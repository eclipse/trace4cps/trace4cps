/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.tl.cmd.test;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.eclipse.trace4cps.tl.cmd.Verify;
import org.junit.Test;

public class VerifyTest {
    @Test
    public void testMain0() {
        int exitCode = verify("./src/test/resources/spec0.etl", "./src/test/resources/trace.etf");
        assertEquals(0, exitCode);
    }

    @Test
    public void testMain1() {
        int exitCode = verify("./src/test/resources/spec1.etl", "./src/test/resources/trace.etf");
        assertEquals(3, exitCode);
    }

    @Test
    public void testMainSyntaxError() {
        int exitCode = verify("./src/test/resources/spec2.etl", "./src/test/resources/trace.etf");
        assertEquals(-1, exitCode);
    }

    @Test
    public void testValidationError() {
        int exitCode = verify("./src/test/resources/spec3.etl", "./src/test/resources/trace.etf");
        assertEquals(-1, exitCode);
    }

    private int verify(String specFile, String traceFile) {
        String[] args = {new File(specFile).getAbsolutePath(), new File(traceFile).getAbsolutePath()};
        return Verify.doMain(args);
    }
}
