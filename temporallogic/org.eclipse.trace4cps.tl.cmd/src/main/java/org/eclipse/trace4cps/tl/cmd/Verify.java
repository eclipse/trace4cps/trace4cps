/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.tl.cmd;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.trace4cps.analysis.mtl.InformativePrefix;
import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.io.ParseException;
import org.eclipse.trace4cps.core.io.TraceReader;
import org.eclipse.trace4cps.tl.EtlStandaloneSetup;
import org.eclipse.trace4cps.tl.FormulaBuilderException;
import org.eclipse.trace4cps.tl.VerificationHelper;
import org.eclipse.trace4cps.tl.VerificationResult;
import org.eclipse.trace4cps.tl.etl.EtlModel;
import org.eclipse.xtext.diagnostics.Severity;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.util.CancelIndicator;
import org.eclipse.xtext.validation.CheckMode;
import org.eclipse.xtext.validation.IResourceValidator;
import org.eclipse.xtext.validation.Issue;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;

public class Verify {
    @Inject
    private Provider<ResourceSet> resourceSetProvider;

    private static int exitError(String msg) {
        System.out.println(msg);
        return -1;
    }

    private static void showUsage() {
        System.out.println("Usage: verify <.etl spec file> <.etf trace file>");
    }

    public static void main(String[] args) {
        int exitCode = doMain(args);
        System.exit(exitCode);
    }

    // We have this method for JUnit testing (we don't want to call System.exit during unit tests)
    public static int doMain(String[] args) {
        if (args.length != 2) {
            showUsage();
            return exitError("Please provide the proper arguments.");
        }

        File specFile = new File(args[0]);
        File traceFile = new File(args[1]);

        if (!specFile.exists() || specFile.isDirectory()) {
            showUsage();
            return exitError("The provided specification file does not exist.");
        }

        if (!traceFile.exists() || traceFile.isDirectory()) {
            showUsage();
            return exitError("The provided trace file does not exist.");
        }

        // do this only once per application
        Injector injector = new EtlStandaloneSetup().createInjectorAndDoEMFRegistration();

        Verify verify = injector.getInstance(Verify.class);
        try {
            int exitCode = verify.run(specFile, traceFile);
            if (exitCode < 0) {
                return exitError("Syntax errors or validation errors in model.");
            }
            return exitCode;
        } catch (Exception e) {
            e.printStackTrace();
            return exitError(e.getMessage());
        }
    }

    /**
     * @return 0 iff all properties satisfied, n>0 iff n properties not satisfied, <0 iff syntax error or validation
     *     error in spec file
     */
    private int run(File specFile, File traceFile)
            throws FormulaBuilderException, InterruptedException, ExecutionException, ParseException, IOException
    {
        ITrace trace = TraceReader.readTrace(traceFile);

        Resource resource = getValidatedResource(specFile.getAbsolutePath());
        if (resource == null) {
            return -1; // syntax errors or validation errors in given model
        }
        EtlModel model = (EtlModel)resource.getContents().get(0);

        VerificationHelper vh = new VerificationHelper(trace, model);
        return printResult(vh.run());
    }

    private Resource getValidatedResource(String filename) {
        ResourceSet resourceSet = resourceSetProvider.get();
        Resource resource = resourceSet.getResource(URI.createFileURI(filename), true);
        // Validation
        IResourceValidator validator = ((XtextResource)resource).getResourceServiceProvider().getResourceValidator();
        List<Issue> issues = validator.validate(resource, CheckMode.ALL, CancelIndicator.NullImpl);
        for (Issue issue: issues) {
            if (issue.getSeverity() == Severity.ERROR) {
                return null;
            }
        }
        return resource;
    }

    /**
     * @return the number of unsatisfied formula
     */
    private int printResult(VerificationResult verRes) {
        int unsatCount = 0;
        for (MtlFormula phi: verRes.getChecks()) {
            String index = "";
            if (verRes.isQuantifiedCheck(phi)) {
                index = "[" + verRes.getQuantifierValue(phi) + "]";
            }
            InformativePrefix p = verRes.getResult(phi).informative();
            if (p == InformativePrefix.BAD) {
                System.out.println(verRes.getName(phi) + index + " : " + p);
                unsatCount++;
            }
        }
        System.out.println("#UNSAT = " + unsatCount);
        return unsatCount;
    }
}
