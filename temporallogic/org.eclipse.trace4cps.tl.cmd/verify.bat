@REM
@REM Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
@REM
@REM This program and the accompanying materials are made
@REM available under the terms of the Eclipse Public License 2.0
@REM which is available at https://www.eclipse.org/legal/epl-2.0/
@REM
@REM SPDX-License-Identifier: EPL-2.0
@REM

@echo off

set DIRNAME=%~dp0
if "%DIRNAME%" == "" set DIRNAME=.
set APP_BASE_NAME=%~n0
set APP_HOME=%DIRNAME%..

for %%i in ("%APP_HOME%") do set APP_HOME=%%~fi

set CMD_LINE_ARGS=%*

java -jar %APP_HOME%\\lib\\${project.artifactId}-${project.version}.jar %CMD_LINE_ARGS%

