/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.tl.tests;

import org.eclipse.trace4cps.tl.validation.CycleDetector;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CycleDetectorTest {
    @Test
    public void testSimpleCycle() {
        CycleDetector d = new CycleDetector();
        d.addEdge("A", "A");
        Assertions.assertTrue(d.hasCycle());
    }

    @Test
    public void testCycle() {
        CycleDetector d = new CycleDetector();
        d.addEdge("A", "B");
        d.addEdge("A", "C");
        d.addEdge("B", "C");
        d.addEdge("B", "D");
        d.addEdge("D", "E");
        d.addEdge("E", "B");
        Assertions.assertTrue(d.hasCycle());
    }

    @Test
    public void testNoCycle() {
        CycleDetector d = new CycleDetector();
        d.addEdge("low", "med");
        d.addEdge("med", "high");
        d.addEdge("low", "high");
        Assertions.assertFalse(d.hasCycle());
    }

    @Test
    public void testNoCycle2() {
        CycleDetector d = new CycleDetector();
        d.addEdge("A", "B");
        d.addEdge("A", "C");
        d.addEdge("B", "C");
        Assertions.assertFalse(d.hasCycle());
    }
}
