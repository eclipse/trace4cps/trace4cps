/**
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.tl.tests

import com.google.inject.Inject
import java.io.File
import org.eclipse.trace4cps.analysis.mtl.InformativePrefix
import org.eclipse.trace4cps.core.ITrace
import org.eclipse.trace4cps.core.impl.Claim
import org.eclipse.trace4cps.core.impl.Resource
import org.eclipse.trace4cps.core.impl.Trace
import org.eclipse.trace4cps.core.io.TraceReader
import org.eclipse.trace4cps.tl.VerificationHelper
import org.eclipse.trace4cps.tl.etl.EtlModel
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

import static org.junit.jupiter.api.Assertions.*

@ExtendWith(InjectionExtension)
@InjectWith(EtlInjectorProvider)
class VerificationHelperTest {

    @Inject extension ParseHelper<EtlModel>

    @Test
    def void verifyODSETest() {
        var model = ('''
signal tp_per_s : throughput of id per s over 10.0 ms

signal work_in_progress : wip of id

signal latency_ms : latency of id in ms

def processing_starts(i) : start { 'name'='A', 'id'=i}

def processing_ends(i) : end {'name'='G', 'id'=i}

def work_done_within_100ms : during [100.0, Infty) ms work_in_progress == 0.0


check latency_discrete : forall (i : 0 ... 199)
                            globally
                                if processing_starts(i) then within [0.0, 50.0) ms processing_ends(i)

check latency_cont : globally latency_ms <= 40.0

check pipeline_depth : within [0.0, 100.0) ms
                            globally
                                ( work_in_progress >= 1.0 or work_done_within_100ms )

check productivity: within [0.0, 100.0) ms
                        globally
                            ( tp_per_s <= 200.0 or work_done_within_100ms ) 

check productivity2: within [0.0, 100.0) ms
                        globally
                            ( tp_per_s >= 40.0 or work_done_within_100ms ) 
        ''').parse;
        assertNoParseErrors(model)
        assertNoValidationIssues(model)
        val trace = TraceReader.readTrace(new File("../../trace-examples/odse/model-bf.etf"));
        val vr = new VerificationHelper(trace, model).run
        for (var i = 0; i < 200; i++) {
            assertTrue(vr.getResult("latency_discrete", i).informative == InformativePrefix.NON_INFORMATIVE)
        }
        assertTrue(vr.getResult("latency_cont").informative == InformativePrefix.GOOD)
        assertTrue(vr.getResult("pipeline_depth").informative == InformativePrefix.GOOD)
        assertTrue(vr.getResult("productivity").informative == InformativePrefix.BAD)
        assertTrue(vr.getResult("productivity2").informative == InformativePrefix.GOOD)
    }

    @Test
    def void verifyInstantaneousTP() {
        var model = ('''
            signal tp : throughput of id
            
            check check1: ( finally tp>=199.99  and globally tp<=200.01 ) // duration of a claim is 5 ms
        ''').parse;
        assertNoParseErrors(model)
        assertNoValidationIssues(model)
        val trace = createTestTrace
        val vr = new VerificationHelper(trace, model).run
        assertTrue(vr.getResult("check1").informative == InformativePrefix.GOOD)
    }

    @Test
    def void verifyScaledInstantaneousTP() {
        var model = ('''
            signal tp : throughput of id per min
            
            // duration of a claim is 5 ms: 1/0.005 = 200 times 60
            check check1: ( finally tp>=12000.0  and globally tp<=12000.01 )
        ''').parse;
        assertNoParseErrors(model)
        assertNoValidationIssues(model)
        val trace = createTestTrace
        val vr = new VerificationHelper(trace, model).run
        assertTrue(vr.getResult("check1").informative == InformativePrefix.GOOD)
    }

    @Test
    def void verifyTP() {
        var model = ('''
            signal tp : throughput of id over 100.0 ms
            
            signal work_in_progress : wip of id
            def work_almost_done : during [1.0, Infty) s work_in_progress == 0.0
            
            // duration of a claim is 5 ms, but period is 20 ms: 1/0.020 = 50
            check check1: within [0.0, 1.0) s
                                    globally
                                        ( tp >= 49.99 or work_almost_done )
        ''').parse;
        assertNoParseErrors(model)
        assertNoValidationIssues(model)
        val trace = createTestTrace
        val vr = new VerificationHelper(trace, model).run
        assertTrue(vr.getResult("check1").informative == InformativePrefix.GOOD)
    }

    @Test
    def void verifyEventBasedTP() {
        var model = ('''
            signal tp1 : throughput of {"name"="A"} over 3.0 s
            signal tp2 : throughput of start {"name"="A"} over 3.0 s

            signal work_in_progress : wip of id
            def work_almost_done : during [3.0, Infty) s work_in_progress == 0.0

            // duration of a claim is 5 ms, but period is 20 ms: 1/0.020 = 50
            check check1: within [0.0, 3.0) s
                                    globally
                                        ( (tp1 >= 99.0 and tp1 <= 101.0) or work_almost_done )
            check check2: within [0.0, 3.0) s
                                    globally
                                        ( (tp2 >= 49.0 and tp2 <= 51.0) or work_almost_done )
        ''').parse;
        assertNoParseErrors(model)
        assertNoValidationIssues(model)
        val trace = createTestTrace
        val vr = new VerificationHelper(trace, model).run
        assertTrue(vr.getResult("check1").informative == InformativePrefix.GOOD)
        assertTrue(vr.getResult("check2").informative == InformativePrefix.GOOD)
    }

    @Test
    def void verifyScaledTP() {
        var model = ('''
            signal tp : throughput of id per hr over 100.0 ms
            
            signal work_in_progress : wip of id
            def work_almost_done : during [1.0, Infty) s work_in_progress == 0.0
            
            // duration of a claim is 5 ms, but period is 20 ms: 1/0.020 = 50 * 3600 = 180000
            check check1: within [0.0, 1.0) s
                                    globally
                                        ( ( tp >= 179999.9 and tp <= 180000.1) or work_almost_done )
        ''').parse;
        assertNoParseErrors(model)
        assertNoValidationIssues(model)
        val trace = createTestTrace
        val vr = new VerificationHelper(trace, model).run
        assertTrue(vr.getResult("check1").informative == InformativePrefix.GOOD)
    }

    @Test
    def void verifyInstantaneousWip() {
        var model = ('''
            signal w : wip of id
            
            check check1: ( finally w>=1.0  and globally w<=1.0 )
        ''').parse;
        assertNoParseErrors(model)
        assertNoValidationIssues(model)
        val trace = createTestTrace
        val vr = new VerificationHelper(trace, model).run
        assertTrue(vr.getResult("check1").informative == InformativePrefix.GOOD)
    }

    @Test
    def void verifyWip() {
        var model = ('''
            signal w : wip of id over 100.0 ms
            
            signal work_in_progress : wip of id
            def work_almost_done : during [1.0, Infty) s work_in_progress == 0.0
            
            // every 20 ms we have a claim of 5 ms: wip = 1/4
            check check1: within [0.0, 1.0) s
                                    globally
                                        ( (w>=0.2499 and w <= 0.2501) or work_almost_done )
        ''').parse;
        assertNoParseErrors(model)
        assertNoValidationIssues(model)
        val trace = createTestTrace
        val vr = new VerificationHelper(trace, model).run
        assertTrue(vr.getResult("check1").informative == InformativePrefix.GOOD)
    }

    @Test
    def void verifyLatency() {
        var model = ('''
            signal lat : latency of id over 100.0 ms
            
            signal work_in_progress : wip of id
            def work_almost_done : during [1.0, Infty) s work_in_progress == 0.0
            
            // every 20 ms we have a claim of 5 ms
            check check1: within [0.0, 1.0) s
                                    globally
                                        ( (lat>=0.001249 and lat<=0.001251) or work_almost_done )
        ''').parse;
        assertNoParseErrors(model)
        assertNoValidationIssues(model)
        val trace = createTestTrace
        val vr = new VerificationHelper(trace, model).run
        assertTrue(vr.getResult("check1").informative == InformativePrefix.GOOD)
    }

    @Test
    def void verifyScaledLatency() {
        var model = ('''
            signal lat : latency of id in ms over 100.0 ms
            
            signal work_in_progress : wip of id
            def work_almost_done : during [1.0, Infty) s work_in_progress == 0.0
            
            // every 20 ms we have a claim of 5 ms
            check check1: within [0.0, 1.0) s
                                    globally
                                        ( (lat>=1.249 and lat<=1.251) or work_almost_done )
        ''').parse;
        assertNoParseErrors(model)
        assertNoValidationIssues(model)
        val trace = createTestTrace
        val vr = new VerificationHelper(trace, model).run
        assertTrue(vr.getResult("check1").informative == InformativePrefix.GOOD)
    }

    @Test
    def void verifyTraceDomainIntersection() {
        var model = ('''            
            check latency_discrete :
                            globally
                                if start {'name'='A'} then within [0.0, 15.0) s end {'name'='B'}
        ''').parse;
        assertNoParseErrors(model)
        assertNoValidationIssues(model)
        val trace = createTestTrace2
        val vr = new VerificationHelper(trace, model).run
        assertTrue(vr.getResult("latency_discrete").informative == InformativePrefix.BAD)
    }

    private def ITrace createTestTrace() { // every 20 ms an execution of A with duration 5 ms
        var trace = new Trace
        var res = new Resource(1, false)
        val period = 0.020; // 20 ms
        for (var i = 0; i < 1000; i++) {
            var claim = new Claim(i * period, i * period + 0.005, res, 1)
            claim.setAttribute("name", "A")
            claim.setAttribute("id", Integer.toString(i))
            trace.add(claim)
        }
        trace
    }

    private def ITrace createTestTrace2() {
        var trace = new Trace
        var res = new Resource(1, false)
        var claim = new Claim(0, 10, res, 1)
        claim.setAttribute("name", "A")
        trace.add(claim)
        claim = new Claim(10, 20, res, 1)
        claim.setAttribute("name", "B")
        trace.add(claim)
        trace
    }

    private def void assertNoParseErrors(EtlModel model) {
        Assertions.assertNotNull(model)
        val errors = model.eResource.errors
        Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: �errors.join(", ")�''')
    }

    private def void assertNoValidationIssues(EtlModel model) {
        val issues = new ValidationTestHelper().validate(model)
        Assertions.assertTrue(issues.isEmpty)
    }
}
