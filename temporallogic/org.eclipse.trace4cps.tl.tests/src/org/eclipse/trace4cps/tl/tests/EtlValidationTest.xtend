/**
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.tl.tests

import com.google.inject.Inject
import org.eclipse.trace4cps.tl.etl.EtlModel
import org.eclipse.xtext.diagnostics.Severity
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.eclipse.xtext.validation.Issue
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

import static org.junit.jupiter.api.Assertions.*

@ExtendWith(InjectionExtension)
@InjectWith(EtlInjectorProvider)
class EtlValidationTest {

    @Inject extension ParseHelper<EtlModel>

    @Test
    def void duplicateSignalNameTest() {
        var model = ('''
            signal s1 : { 'name'='some name' }
            signal s1 : { 'name'='some name', 'type' = 'some type' }
        ''').parse;
        assertNoParseErrors(model)
        assertEquals(2, count(model, Severity.ERROR))
    }

    @Test
    def void throughputEventSignalTest() {
        var model = ('''
            signal s1 : throughput of {"name"="A", "type"= i + "k"} per hr over 2.0 us
        ''').parse;
        assertNoParseErrors(model)
        assertEquals(1, count(model, Severity.ERROR))
    }

    @Test
    def void parameterNotDeclaredTest1() {
        var model = ('''
            def ap1 (i) : { 'name'='A', 'id'=j}
        ''').parse;
        assertNoParseErrors(model)
        assertEquals(2, count(model, Severity.ERROR))
    }

    @Test
    def void parameterNotDeclaredTest2() {
        var model = ('''
            def ap1 (i) : { 'name'='A', 'id'=i}
            check c1 : forall(i : 0 ... 99) finally ap1(j)
        ''').parse;
        assertNoParseErrors(model)
        assertEquals(2, count(model, Severity.ERROR))
    }

    @Test
    def void parameterNotDeclaredTest3() {
        var model = ('''
            check c1 : forall(i : 0 ... 99) finally { 'name'='A', 'id'=j}
        ''').parse;
        assertNoParseErrors(model)
        assertEquals(2, count(model, Severity.ERROR))
    }

    @Test
    def void parameterizedCheckUsesParameter() {
        var model = ('''
            check c1 : forall(i : 0 ... 99) finally { 'name'='A'}
        ''').parse;
        assertNoParseErrors(model)
        assertEquals(1, count(model, Severity.ERROR))
    }

    @Test
    def void parameterizedDefUsesParameterTest() {
        var model = ('''
            def ap1 (i) : { 'name'='A'}
        ''').parse;
        assertNoParseErrors(model)
        assertEquals(1, count(model, Severity.ERROR))
    }

    @Test
    def void signalUsesParameterTest() {
        var model = ('''
            signal s1 : { 'name'='some name', 'id'=i }
        ''').parse;
        assertNoParseErrors(model)
        assertEquals(1, count(model, Severity.ERROR))
    }

    @Test
    def void cyclicDefTest1() {
        var model = ('''
            def ap1 : ( { 'name'='A' } and ap1)
        ''').parse;
        assertNoParseErrors(model)
        assertEquals(1, count(model, Severity.ERROR))
    }

    @Test
    def void cyclicDefTest2() {
        var model = ('''
            def ap1 : ( { 'name'='A' } and ap2)
            def ap2 : ( { 'name'='B' } or ap1)
        ''').parse;
        assertNoParseErrors(model)
        assertEquals(2, count(model, Severity.ERROR))
    }

    @Test
    def void cyclicDefTest3() {
        var model = ('''
            def height_measurement : {'signal'='Td_Collet AT_PUSHUP', 'color'='green', 'type'='spike' }
            def no_pickup_activity_occurs : not {'signal'='Pickup'}

            def m1: height_measurement
            def m2: (height_measurement and by (0.0, 5.0] s m1 and until then no_pickup_activity_occurs)
        ''').parse;
        assertNoParseErrors(model)
        assertEquals(0, count(model, Severity.ERROR))        
    }

    @Test
    def void intervalSSValidationTest1() {
        var model = ('''
            def ap1 : within (-1.0, 10.0) s { 'name'='A' }
        ''').parse;
        assertNoParseErrors(model)
        assertEquals(1, count(model, Severity.ERROR))
    }

    @Test
    def void intervalSSValidationTest2() {
        var model = ('''
            def ap1 : within (10.0, 10.0) s { 'name'='A' }
        ''').parse;
        assertNoParseErrors(model)
        assertEquals(1, count(model, Severity.ERROR))
    }

    @Test
    def void intervalSSValidationTest3() {
        var model = ('''
            def ap1 : within (20.0, 10.0) s { 'name'='A' }
        ''').parse;
        assertNoParseErrors(model)
        assertEquals(1, count(model, Severity.ERROR))
    }

    @Test
    def void intervalSSValidationTest4() {
        var model = ('''
            def ap1 : within (-10.0, Infty) s { 'name'='A' }
        ''').parse;
        assertNoParseErrors(model)
        assertEquals(1, count(model, Severity.ERROR))
    }

    @Test
    def void intervalSNValidationTest1() {
        var model = ('''
            def ap1 : within (-1.0, 10.0] s { 'name'='A' }
        ''').parse;
        assertNoParseErrors(model)
        assertEquals(1, count(model, Severity.ERROR))
    }

    @Test
    def void intervalSNValidationTest2() {
        var model = ('''
            def ap1 : within (10.0, 10.0] s { 'name'='A' }
        ''').parse;
        assertNoParseErrors(model)
        assertEquals(1, count(model, Severity.ERROR))
    }

    @Test
    def void intervalSNValidationTest3() {
        var model = ('''
            def ap1 : within (20.0, 10.0] s { 'name'='A' }
        ''').parse;
        assertNoParseErrors(model)
        assertEquals(1, count(model, Severity.ERROR))
    }

    @Test
    def void intervalNSValidationTest1() {
        var model = ('''
            def ap1 : within [-1.0, 10.0) s { 'name'='A' }
        ''').parse;
        assertNoParseErrors(model)
        assertEquals(1, count(model, Severity.ERROR))
    }

    @Test
    def void intervalNSValidationTest2() {
        var model = ('''
            def ap1 : within [10.0, 10.0) s { 'name'='A' }
        ''').parse;
        assertNoParseErrors(model)
        assertEquals(1, count(model, Severity.ERROR))
    }

    @Test
    def void intervalNSValidationTest3() {
        var model = ('''
            def ap1 : within [20.0, 10.0) s { 'name'='A' }
        ''').parse;
        assertNoParseErrors(model)
        assertEquals(1, count(model, Severity.ERROR))
    }

    @Test
    def void intervalNSValidationTest4() {
        var model = ('''
            def ap1 : within [-10.0, Infty) s { 'name'='A' }
        ''').parse;
        assertNoParseErrors(model)
        assertEquals(1, count(model, Severity.ERROR))
    }

    @Test
    def void intervalNNValidationTest1() {
        var model = ('''
            def ap1 : within [-1.0, 10.0] s { 'name'='A' }
        ''').parse;
        assertNoParseErrors(model)
        assertEquals(1, count(model, Severity.ERROR))
    }

    @Test
    def void intervalNNValidationTest2() {
        var model = ('''
            def ap1 : within [10.0, 10.0] s { 'name'='A' }
        ''').parse;
        assertNoParseErrors(model)
        assertEquals(0, count(model, Severity.ERROR))
    }

    @Test
    def void intervalNNValidationTest3() {
        var model = ('''
            def ap1 : within [20.0, 10.0] s { 'name'='A' }
        ''').parse;
        assertNoParseErrors(model)
        assertEquals(1, count(model, Severity.ERROR))
    }

    private def void assertNoParseErrors(EtlModel model) {
        Assertions.assertNotNull(model)
        val errors = model.eResource.errors
        Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: �errors.join(", ")�''')
    }

    private def int count(EtlModel model, Severity s) {
        var c = 0
        for (Issue issue : new ValidationTestHelper().validate(model)) {
            if (issue.severity == s) {
                c++
            }
        }
        c
    }
}
