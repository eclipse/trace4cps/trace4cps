/**
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.tl.tests

import com.google.inject.Inject
import org.eclipse.trace4cps.tl.etl.EtlModel
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

@ExtendWith(InjectionExtension)
@InjectWith(EtlInjectorProvider)
class EtlParsingTest {
    @Inject
    ParseHelper<EtlModel> parseHelper

    @Test
    def void parseTraceSignal() {
        val result = parseHelper.parse('''
            signal s1 : { 'name'='some name' }
            signal s2 : { 'name'='some name', 'type' = 'some type' }
        ''')
        assertNoParseErrors(result)
    }

    @Test
    def void parseThroughputSignal() {
        val result = parseHelper.parse('''
            signal s1 : throughput of id
            signal s2 : throughput of id per ns
            signal s3 : throughput of id per us
            signal s4 : throughput of id per ms
            signal s5 : throughput of id per s
            signal s6 : throughput of id per min
            signal s7 : throughput of id per hr
            
            signal s8 : throughput of id over 2.0 us
            signal s9 : throughput of id per hr over 2.0 us
        ''')
        assertNoParseErrors(result)
    }

    @Test
    def void parseThroughputSignalEvents() {
        val result = parseHelper.parse('''
            signal s1 : throughput of {"name"="A"}
            signal s2 : throughput of start {"name"="A", "type"= "k"} per ns
            signal s3 : throughput of end {"name"="A", "type"= "k"} over 2.0 us
            signal s4 : throughput of {"name"="A", "type"= "k"} per hr over 2.0 us
        ''')
        assertNoParseErrors(result)
    }

    @Test
    def void parseLatencySignal() {
        val result = parseHelper.parse('''
            signal s1 : latency of id
            signal s2 : latency of id in ns
            signal s3 : latency of id in us over 2.0 hr
            signal s4 : latency of id over 1.0 s
        ''')
        assertNoParseErrors(result)
    }

    @Test
    def void parseWipSignal() {
        val result = parseHelper.parse('''
            signal s1 : wip of id
            signal s2 : wip of id over 1.0 s
        ''')
        assertNoParseErrors(result)
    }

    @Test
    def void parseResourceAmountSignal() {
        val result = parseHelper.parse('''
            signal s1 : resource-amount {'name'='CPU'} 
            signal s2 : resource-amount {'name'='CPU'} over 1.0 s
        ''')
        assertNoParseErrors(result)
    }

    @Test
    def void parseResourceClientSignal() {
        val result = parseHelper.parse('''
            signal s1 : resource-clients {'name'='CPU'} 
            signal s2 : resource-clients {'name'='CPU'} over 1.0 s
        ''')
        assertNoParseErrors(result)
    }

    @Test
    def void parseEmptyKeyVal() {
        val result = parseHelper.parse('''
            signal s1 : {  }
        ''')
        Assertions.assertNotNull(result)
        val errors = result.eResource.errors
        Assertions.assertFalse(errors.isEmpty)
    }

    @Test
    def void parseAtomicProp() {
        val result = parseHelper.parse('''
            // MTL:
            def ap1 : { 'name'='A', 'id'='0' }
            def ap2 : start { 'name'='A', 'id'='0' }
            def ap3 : end { 'name'='A', 'id'='0' }
            // STL:
            def ap4 : s1 <= 2.0
            def ap5 : s1 == 2.0
            def ap6 : s1 >= 2.0
            def ap7 : d s1 / dt >= 2.0
        ''')
        assertNoParseErrors(result)
    }

    @Test
    def void parseParameterizedDef() {
        val result = parseHelper.parse('''
            def ap1 (i) : { 'name'='A', 'id'=i }
            def ap2 (i) : { 'name'='A', 'id'= 'id' + i }
            def ap3 (i) : { 'name'='A', 'id'= i + 'id' }
            def ap4 (i) : { 'name'='A', 'id'= 'a' + i + 'b' }
            def ap5 (i) : { 'name'='A', 'a' + i + 'b' = i}
        ''')
        assertNoParseErrors(result)
    }

    @Test
    def void parseParameterizedRef() {
        val result = parseHelper.parse('''
            def ap1 (i) : { 'name'='A', 'id'=i }
            def ap2 (i) : ap1(i)
            def ap3 (i) : ap1(i + 4)
        ''')
        assertNoParseErrors(result)
    }

    @Test
    def void parseIntervals() {
        val result = parseHelper.parse('''
            def ap1 : { 'name'='A'}
            
            def f1 : within (0.0, 1.0) ms ap1
            def f2 : within (0.0, Infty) ms ap1
            def f3 : within (0.0, 2.0] ms ap1
            def f4 : within [0.0, 1.9) ms ap1
            def f5 : within [0.0, Infty) ms ap1
            def f6 : within [2.0, 4.0] ms ap1
        ''')
        assertNoParseErrors(result)
    }

    @Test
    def void parseFormulas() {
        val result = parseHelper.parse('''
            def ap1 : { 'name'='A'}
            def ap2 : s1 <= 2.0
            
            def f1 : not ap1
            def f2 : ( ap1 and ap2 )
            def f3 : ( ap1 or f2 )
            def f4 : if ap1 then f2
            
            def f5 : globally ap1
            def f6 : finally ap1
            
            def f7 : during (1.2, 5.8] hr ap1
            def f8 : within [3.2, Infty) us ap1
            
            def f9 : until ap2 we have that ap1
            def f10 : by [0.0, 2.0] min ap2 and until then ap1
        ''')
        assertNoParseErrors(result)
    }

    @Test
    def void parseChecks() {
        val result = parseHelper.parse('''
            check c1 : finally { 'name'='A'}
            
            check c2 : forall(i : 0 ... 99) finally { 'name'='A', 'id'=i}
        ''')
        assertNoParseErrors(result)
    }

    @Test
    def void parseingOrder() {
        val result = parseHelper.parse('''
            check c1 : finally { 'name'='A'}
            def ap1 : { 'name'='A'}
            signal s1 : resource-clients {'name'='CPU'}
        ''')
        assertNoParseErrors(result)
    }

    private def void assertNoParseErrors(EtlModel model) {
        Assertions.assertNotNull(model)
        val errors = model.eResource.errors
        Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: �errors.join(", ")�''')
    }
}
