/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.tl.ui.contentassist;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.trace4cps.tl.FormulaBuilder;
import org.eclipse.trace4cps.tl.FormulaHelper;
import org.eclipse.trace4cps.tl.etl.EtlModel;
import org.eclipse.trace4cps.tl.etl.SignalDef;
import org.eclipse.trace4cps.tl.etl.impl.EtlModelImpl;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext;
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor;

/**
 * See https://www.eclipse.org/Xtext/documentation/310_eclipse_support.html#content-assist on how to customize the
 * content assistant.
 */
public class EtlProposalProvider extends AbstractEtlProposalProvider {
    @Override
    public void completeKeyword(Keyword keyword, ContentAssistContext contentAssistContext,
            ICompletionProposalAcceptor acceptor)
    {
    }
//
//    @Override
//    public void completeFormula_(EObject model, RuleCall ruleCall, ContentAssistContext context,
//            ICompletionProposalAcceptor acceptor) {
//        EtlModel m = FormulaHelper.findContainer(model, EtlModelImpl.class);
//        for (Def def : FormulaBuilder.get(m, Def.class)) {
//            if (def.getParam() != null) {
//                // FIXME: change i to the variable/parameter of the enclosing Def/Check
//                acceptor.accept(createCompletionProposal(def.getName() + "(i)", context));
//            } else {
//                acceptor.accept(createCompletionProposal(def.getName(), context));
//            }
//        }
//    }

    @Override
    public void complete_AttributeFilter(EObject model, RuleCall ruleCall, ContentAssistContext context,
            ICompletionProposalAcceptor acceptor)
    {
        acceptor.accept(createCompletionProposal("{'name'='<name>', ...}", context));
    }

    @Override
    public void complete_StlAp(EObject model, RuleCall ruleCall, ContentAssistContext context,
            ICompletionProposalAcceptor acceptor)
    {
        EtlModel m = FormulaHelper.findContainer(model, EtlModelImpl.class);
        for (SignalDef signalEl: FormulaBuilder.get(m, SignalDef.class)) {
            acceptor.accept(createCompletionProposal(signalEl.getName() + " >= 0.0", context));
        }
    }

    @Override
    public void complete_IntervalSS(EObject model, RuleCall ruleCall, ContentAssistContext context,
            ICompletionProposalAcceptor acceptor)
    {
        acceptor.accept(createCompletionProposal("(0.0, 1.0)", context));
    }

    @Override
    public void complete_IntervalSN(EObject model, RuleCall ruleCall, ContentAssistContext context,
            ICompletionProposalAcceptor acceptor)
    {
        acceptor.accept(createCompletionProposal("(0.0, 1.0]", context));
    }

    @Override
    public void complete_IntervalNS(EObject model, RuleCall ruleCall, ContentAssistContext context,
            ICompletionProposalAcceptor acceptor)
    {
        acceptor.accept(createCompletionProposal("[0.0, 1.0)", context));
    }

    @Override
    public void complete_IntervalNN(EObject model, RuleCall ruleCall, ContentAssistContext context,
            ICompletionProposalAcceptor acceptor)
    {
        acceptor.accept(createCompletionProposal("[0.0, 1.0]", context));
    }

    @Override
    public void complete_TimeUnitEnum(EObject model, RuleCall ruleCall, ContentAssistContext context,
            ICompletionProposalAcceptor acceptor)
    {
        acceptor.accept(createCompletionProposal("ns|us|ms|s|min|hr", context));
    }

    @Override
    public void complete_INT_T(EObject model, RuleCall ruleCall, ContentAssistContext context,
            ICompletionProposalAcceptor acceptor)
    {
        acceptor.accept(createCompletionProposal("0", context));
    }

    @Override
    public void complete_DOUBLE_T(EObject model, RuleCall ruleCall, ContentAssistContext context,
            ICompletionProposalAcceptor acceptor)
    {
        acceptor.accept(createCompletionProposal("0.0", context));
    }

    @Override
    public void complete_Signal(EObject model, RuleCall ruleCall, ContentAssistContext context,
            ICompletionProposalAcceptor acceptor)
    {
        acceptor.accept(createCompletionProposal("signal ID : ", context));
    }

    public void complete_ConvSpec(EObject model, RuleCall ruleCall, ContentAssistContext context,
            ICompletionProposalAcceptor acceptor)
    {
        acceptor.accept(createCompletionProposal("over 10.0 ms", context));
    }
//
//    @Override
//    public void complete_NotFormula(EObject model, RuleCall ruleCall, ContentAssistContext context,
//            ICompletionProposalAcceptor acceptor) {
//        acceptor.accept(createCompletionProposal("not ", context));
//    }
//
//    @Override
//    public void complete_AndOrFormula(EObject model, RuleCall ruleCall, ContentAssistContext context,
//            ICompletionProposalAcceptor acceptor) {
//        acceptor.accept(createCompletionProposal("( f1 and/or f2 )", context));
//    }
//
//    @Override
//    public void complete_IfThenFormula(EObject model, RuleCall ruleCall, ContentAssistContext context,
//            ICompletionProposalAcceptor acceptor) {
//        acceptor.accept(createCompletionProposal("if f1 then f2", context));
//    }
//
//    @Override
//    public void complete_Finally0(EObject model, RuleCall ruleCall, ContentAssistContext context,
//            ICompletionProposalAcceptor acceptor) {
//        acceptor.accept(createCompletionProposal("finally f", context));
//    }
//
//    @Override
//    public void complete_Finally1(EObject model, RuleCall ruleCall, ContentAssistContext context,
//            ICompletionProposalAcceptor acceptor) {
//        acceptor.accept(createCompletionProposal("within [0.0, 1.0) ms f", context));
//    }
//
//    @Override
//    public void complete_Globally0(EObject model, RuleCall ruleCall, ContentAssistContext context,
//            ICompletionProposalAcceptor acceptor) {
//        acceptor.accept(createCompletionProposal("globally f", context));
//    }
//
//    @Override
//    public void complete_Globally1(EObject model, RuleCall ruleCall, ContentAssistContext context,
//            ICompletionProposalAcceptor acceptor) {
//        acceptor.accept(createCompletionProposal("during [0.0, 1.0) ms f", context));
//    }
//
//    @Override
//    public void complete_Until0(EObject model, RuleCall ruleCall, ContentAssistContext context,
//            ICompletionProposalAcceptor acceptor) {
//        acceptor.accept(createCompletionProposal("until f2 we have that f1", context));
//    }
//
//    @Override
//    public void complete_Until1(EObject model, RuleCall ruleCall, ContentAssistContext context,
//            ICompletionProposalAcceptor acceptor) {
//        acceptor.accept(createCompletionProposal("by [0.0, 1.0) ms f2 and until then f1", context));
//    }

    @Override
    public void complete_EtlModel(EObject model, RuleCall ruleCall, ContentAssistContext context,
            ICompletionProposalAcceptor acceptor)
    {
    }

    @Override
    public void complete_Formula(EObject model, RuleCall ruleCall, ContentAssistContext context,
            ICompletionProposalAcceptor acceptor)
    {
    }

    @Override
    public void complete_KeyVal(EObject model, RuleCall ruleCall, ContentAssistContext context,
            ICompletionProposalAcceptor acceptor)
    {
    }

    @Override
    public void complete_IdString(EObject model, RuleCall ruleCall, ContentAssistContext context,
            ICompletionProposalAcceptor acceptor)
    {
    }

    @Override
    public void complete_Interval(EObject model, RuleCall ruleCall, ContentAssistContext context,
            ICompletionProposalAcceptor acceptor)
    {
    }
}
