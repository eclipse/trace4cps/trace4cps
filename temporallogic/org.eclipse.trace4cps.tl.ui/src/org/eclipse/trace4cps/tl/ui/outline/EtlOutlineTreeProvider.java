/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.tl.ui.outline;

import org.eclipse.trace4cps.tl.etl.ApFormula;
import org.eclipse.trace4cps.tl.etl.AttributeFilter;
import org.eclipse.trace4cps.tl.etl.Interval;
import org.eclipse.trace4cps.tl.etl.KeyVal;
import org.eclipse.trace4cps.tl.etl.MtlAp;
import org.eclipse.trace4cps.tl.etl.ResourceAmountSignal;
import org.eclipse.trace4cps.tl.etl.ResourceClientSignal;
import org.eclipse.trace4cps.tl.etl.TraceSignal;
import org.eclipse.xtext.ui.editor.outline.IOutlineNode;
import org.eclipse.xtext.ui.editor.outline.impl.DefaultOutlineTreeProvider;

/**
 * Customization of the default outline structure.
 *
 * <p>
 * See https://www.eclipse.org/Xtext/documentation/310_eclipse_support.html#outline
 * </p>
 */
public class EtlOutlineTreeProvider extends DefaultOutlineTreeProvider {
//
    private void createAttributeFilter(IOutlineNode parentNode, AttributeFilter f) {
        for (KeyVal kv: f.getKeyVals()) {
            createNode(parentNode, kv);
        }
    }

    protected void _createChildren(IOutlineNode parentNode, TraceSignal e) {
        createAttributeFilter(parentNode, e.getFilter());
    }

    protected void _createChildren(IOutlineNode parentNode, MtlAp e) {
        createAttributeFilter(parentNode, e.getFilter());
    }

    protected void _createChildren(IOutlineNode parentNode, ResourceAmountSignal e) {
        createAttributeFilter(parentNode, e.getFilter());
    }

    protected void _createChildren(IOutlineNode parentNode, ResourceClientSignal e) {
        createAttributeFilter(parentNode, e.getFilter());
    }

    protected void _createChildren(IOutlineNode parentNode, KeyVal e) {
        // KeyVal are displayed by the parent KeyVals
    }

    protected void _createChildren(IOutlineNode parentNode, ApFormula e) {
        if (e.getMtlAP() != null) {
            createNode(parentNode, e.getMtlAP());
        } else {
            createNode(parentNode, e.getStlAP());
        }
    }

    protected void _createChildren(IOutlineNode parentNode, Interval i) {
    }
//
//    protected void _createChildren(IOutlineNode parentNode, ReferenceFormula f) {
//    }
//
//    protected void _createChildren(IOutlineNode parentNode, KeyVal m) {
//    }
//
//    protected void _createChildren(IOutlineNode parentNode, GloballyUntimedFormula f) {
//        Formula fc = f.getFormula();
//        createFormulaInstanceNode(parentNode, fc);
//    }
//
//    protected void _createChildren(IOutlineNode parentNode, GloballyFormula f) {
//        Formula fc = f.getFormula();
//        createFormulaInstanceNode(parentNode, fc);
//    }
//
//    protected void _createChildren(IOutlineNode parentNode, FinallyUntimedFormula f) {
//        Formula fc = f.getFormula();
//        createFormulaInstanceNode(parentNode, fc);
//    }
//
//    protected void _createChildren(IOutlineNode parentNode, FinallyFormula f) {
//        Formula fc = f.getFormula();
//        createFormulaInstanceNode(parentNode, fc);
//    }
//
//    protected void _createChildren(IOutlineNode parentNode, UntilUntimedFormula f) {
//        createFormulaInstanceNode(parentNode, f.getLeft());
//        createFormulaInstanceNode(parentNode, f.getRight());
//    }
//
//    protected void _createChildren(IOutlineNode parentNode, UntilFormula f) {
//        createFormulaInstanceNode(parentNode, f.getLeft());
//        createFormulaInstanceNode(parentNode, f.getRight());
//    }
//
//    protected void _createChildren(IOutlineNode parentNode, AndOrFormula f) {
//        createFormulaInstanceNode(parentNode, f.getLeft());
//        createFormulaInstanceNode(parentNode, f.getRight());
//    }
//
//    protected void _createChildren(IOutlineNode parentNode, IfThenFormula f) {
//        createFormulaInstanceNode(parentNode, f.getLeft());
//        createFormulaInstanceNode(parentNode, f.getRight());
//    }
//
//    protected void _createChildren(IOutlineNode parentNode, NotFormula f) {
//        createFormulaInstanceNode(parentNode, f.getFormula());
//    }
//
//    protected void _createChildren(IOutlineNode parentNode, Def def) {
//        Formula f = def.getFormula();
//        createFormulaInstanceNode(parentNode, f);
//    }
//
//    protected void _createChildren(IOutlineNode parentNode, Check c) {
//        Formula f = c.getFormula();
//        createFormulaInstanceNode(parentNode, f);
//    }
//
//    protected void _createChildren(IOutlineNode parentNode, TraceSignal s) {
//        // skip the KeyVals node:
//        for (KeyVal v : s.getFilter().getKeyVals()) {
//            createNode(parentNode, v);
//        }
//    }
//
//    protected void _createChildren(IOutlineNode parentNode, ResourceAmountSignal s) {
//        // skip the KeyVals node:
//        for (KeyVal v : s.getFilter().getKeyVals()) {
//            createNode(parentNode, v);
//        }
//    }
//
//    protected void _createChildren(IOutlineNode parentNode, ResourceClientSignal s) {
//        // skip the KeyVals node:
//        for (KeyVal v : s.getFilter().getKeyVals()) {
//            createNode(parentNode, v);
//        }
//    }
//
//    protected void _createChildren(IOutlineNode parentNode, ThroughputSignal s) {
//    }
//
//    protected void _createChildren(IOutlineNode parentNode, LatencySignal s) {
//    }
//
//    protected void _createChildren(IOutlineNode parentNode, WipSignal s) {
//    }
//
//    /**
//     * For filtering out the "Formula" nodes of the meta model in the outline.
//     */
//    private void createFormulaInstanceNode(IOutlineNode parentNode, Formula f) {
//        if (f == null) {
//            return;
//        }
//        switch (FormulaHelper.getType(f)) {
//        case REF:
//            createNode(parentNode, f.getRef());
//            break;
//        case MTL_AP:
//            if (f.getMtlAP() instanceof StartKeyVals || f.getMtlAP() instanceof StopKeyVals) {
//                createNode(parentNode, f.getMtlAP());
//            } else {
//                for (KeyVal k : f.getMtlAP().getKeyVals()) {
//                    createNode(parentNode, k);
//                }
//            }
//            break;
//        case STL_AP:
//            createNode(parentNode, f.getStlAP());
//            break;
//        case AND:
//            createNode(parentNode, f.getAndOrFormula());
//            break;
//        case OR:
//            createNode(parentNode, f.getAndOrFormula());
//            break;
//        case IMPLY:
//            createNode(parentNode, f.getIfThenFormule());
//            break;
//        case NOT:
//            createNode(parentNode, f.getNotFormula());
//            break;
//        case FIN0:
//            createNode(parentNode, f.getFinally0());
//            break;
//        case FIN1:
//            createNode(parentNode, f.getFinally1());
//            break;
//        case GLOB0:
//            createNode(parentNode, f.getGlobally0());
//            break;
//        case GLOB1:
//            createNode(parentNode, f.getGlobally1());
//            break;
//        case UNTIL0:
//            createNode(parentNode, f.getUntil0());
//            break;
//        case UNTIL1:
//            createNode(parentNode, f.getUntil1());
//            break;
//        default:
//            break;
//        }
//    }
//
}
