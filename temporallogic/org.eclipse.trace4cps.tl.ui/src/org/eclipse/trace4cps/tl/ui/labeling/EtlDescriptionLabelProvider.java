/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.tl.ui.labeling;

import org.eclipse.xtext.ui.label.DefaultDescriptionLabelProvider;

/**
 * Provides labels for IEObjectDescriptions and IResourceDescriptions.
 *
 * <p>
 * See https://www.eclipse.org/Xtext/documentation/310_eclipse_support.html#label-provider
 * </p>
 */
public class EtlDescriptionLabelProvider extends DefaultDescriptionLabelProvider {

    // Labels and icons can be computed like this:
//    @Override
//    public String text(IEObjectDescription ele) {
//        return ele.getName().toString();
//    }
//
//    @Override
//    public String image(IEObjectDescription ele) {
//        return ele.getEClass().getName() + ".gif";
//    }
}
