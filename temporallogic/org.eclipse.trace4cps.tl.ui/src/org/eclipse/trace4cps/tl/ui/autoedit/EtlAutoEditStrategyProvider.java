/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.tl.ui.autoedit;

import java.util.List;

import org.eclipse.jface.text.IAutoEditStrategy;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.xtext.ui.editor.autoedit.DefaultAutoEditStrategyProvider;

import com.google.common.collect.Lists;

public class EtlAutoEditStrategyProvider extends DefaultAutoEditStrategyProvider {
    /**
     * Just remove all strategies... I don't know how to only remove auto-editing of the parentheses and square brackets
     * (for the time intervals).
     *
     * {@inheritDoc}
     */
    @Override
    public List<IAutoEditStrategy> getStrategies(final ISourceViewer sourceViewer, final String contentType) {
        return Lists.newArrayList();
    }
}
