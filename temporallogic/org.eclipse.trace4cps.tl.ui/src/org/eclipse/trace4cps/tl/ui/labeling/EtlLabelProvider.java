/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.tl.ui.labeling;

import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.trace4cps.tl.FormulaHelper;
import org.eclipse.trace4cps.tl.etl.AndOrFormula;
import org.eclipse.trace4cps.tl.etl.Check;
import org.eclipse.trace4cps.tl.etl.ConvSpec;
import org.eclipse.trace4cps.tl.etl.Def;
import org.eclipse.trace4cps.tl.etl.EtlModel;
import org.eclipse.trace4cps.tl.etl.FinallyFormula;
import org.eclipse.trace4cps.tl.etl.FinallyUntimedFormula;
import org.eclipse.trace4cps.tl.etl.Formula;
import org.eclipse.trace4cps.tl.etl.GloballyFormula;
import org.eclipse.trace4cps.tl.etl.GloballyUntimedFormula;
import org.eclipse.trace4cps.tl.etl.IfThenFormula;
import org.eclipse.trace4cps.tl.etl.Interval;
import org.eclipse.trace4cps.tl.etl.KeyVal;
import org.eclipse.trace4cps.tl.etl.LatencySignal;
import org.eclipse.trace4cps.tl.etl.MtlAp;
import org.eclipse.trace4cps.tl.etl.MtlApEnd;
import org.eclipse.trace4cps.tl.etl.MtlApStart;
import org.eclipse.trace4cps.tl.etl.NotFormula;
import org.eclipse.trace4cps.tl.etl.ReferenceFormula;
import org.eclipse.trace4cps.tl.etl.ResourceAmountSignal;
import org.eclipse.trace4cps.tl.etl.ResourceClientSignal;
import org.eclipse.trace4cps.tl.etl.SignalDef;
import org.eclipse.trace4cps.tl.etl.StlAp;
import org.eclipse.trace4cps.tl.etl.StlApDeriv;
import org.eclipse.trace4cps.tl.etl.ThroughputSignal;
import org.eclipse.trace4cps.tl.etl.TraceSignal;
import org.eclipse.trace4cps.tl.etl.UntilFormula;
import org.eclipse.trace4cps.tl.etl.UntilUntimedFormula;
import org.eclipse.trace4cps.tl.etl.WipSignal;
import org.eclipse.xtext.ui.label.DefaultEObjectLabelProvider;

import com.google.inject.Inject;

/**
 * Provides labels for EObjects.
 *
 * <p>
 * See https://www.eclipse.org/Xtext/documentation/310_eclipse_support.html#label-provider
 * </p>
 */
public class EtlLabelProvider extends DefaultEObjectLabelProvider {
    @Inject
    public EtlLabelProvider(AdapterFactoryLabelProvider delegate) {
        super(delegate);
    }

    String text(EtlModel m) {
        return "ETL specification";
    }

    String text(SignalDef s) {
        return "signal " + s.getName();
    }

    String text(TraceSignal s) {
        return "provided signal";
    }

    String text(ThroughputSignal s) {
        if (s.getIdAtt() != null) {
            return "throughput of " + s.getIdAtt() + " per " + s.getScale();
        }
        return "throughput per " + s.getScale();
    }

    String text(LatencySignal s) {
        return "latency signal of " + s.getIdAtt() + " in " + s.getScale();
    }

    String text(WipSignal s) {
        return "wip signal of " + s.getIdAtt();
    }

    String text(ResourceAmountSignal s) {
        return "resource amount signal";
    }

    String text(ResourceClientSignal s) {
        return "resource clients signal";
    }

    String text(Formula f) {
        return "formula";
    }

    String text(Def def) {
        String param = def.getParam() != null ? "(" + def.getParam() + ")" : "";
        return "def " + def.getName() + param;
    }

    String text(Check c) {
        String var = c.getVar() != null ? "(" + c.getLb() + "<=" + c.getVar() + "<=" + c.getUb() + ")" : "";
        return "check " + c.getName() + var;
    }

    String text(ConvSpec s) {
        return "convolution window " + s.getWindowWidth() + " " + s.getWindowUnit();
    }

    String text(ReferenceFormula ref) {
        if (ref.getParam() != null) {
            if (ref.getVal() != 0) {
                return ref.getDef().getName() + "(" + ref.getParam() + "+" + ref.getVal() + ")";
            } else {
                return ref.getDef().getName() + "(" + ref.getParam() + ")";
            }
        }
        return ref.getDef().getName();
    }

    String text(GloballyUntimedFormula g) {
        return "G";
    }

    String text(GloballyFormula g) {
        return "G_" + FormulaHelper.toString(g.getInterval());
    }

    String text(FinallyUntimedFormula f) {
        return "F";
    }

    String text(FinallyFormula f) {
        return "F_" + FormulaHelper.toString(f.getInterval());
    }

    String text(UntilUntimedFormula f) {
        return "U";
    }

    String text(UntilFormula f) {
        return "U_" + FormulaHelper.toString(f.getInterval());
    }

    String text(IfThenFormula f) {
        return "imply";
    }

    String text(NotFormula f) {
        return "not";
    }

    String text(AndOrFormula f) {
        return f.getOp().toString().toLowerCase();
    }

    String text(StlAp s) {
        return s.getRef().getName() + s.getCompOp() + s.getVal();
    }

    String text(StlApDeriv s) {
        return "d " + s.getRef().getName() + "/ dt " + s.getCompOp() + s.getVal();
    }

    String text(MtlAp ap) {
        return "event";
    }

    String text(MtlApStart v) {
        return "start of";
    }

    String text(MtlApEnd v) {
        return "end of";
    }

    String text(KeyVal kv) {
        return FormulaHelper.toString(kv.getAtt()) + "=" + FormulaHelper.toString(kv.getVal());
    }

    String text(Interval i) {
        return FormulaHelper.toString(i);
    }
}
