/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.tl;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.mtl.MtlResult;

/**
 * This type represents the result of the verification of a single ETL file with mutiple defs and checks.
 */
public final class VerificationResult {

    // For defs and checks:
    private final Map<MtlFormula, String> formula2name = new HashMap<>();

    // Only for the checks:
    private final Map<MtlFormula, MtlResult> formula2result = new HashMap<>();

    // Only for the qunatified checks:
    private final Map<MtlFormula, Integer> formula2QuantValue = new HashMap<>();

    private final Set<MtlFormula> checks = new HashSet<>();

    public VerificationResult() {
    }

    void add(String name, MtlFormula phi, boolean isCheck) {
        if (formula2name.containsKey(phi)) {
            throw new IllegalStateException();
        }
        formula2name.put(phi, name);
        if (isCheck) {
            checks.add(phi);
        }
    }

    void add(String name, MtlFormula phi, Integer quantValue) {
        if (formula2name.containsKey(phi)) {
            throw new IllegalStateException();
        }
        formula2name.put(phi, name);
        checks.add(phi);
        formula2QuantValue.put(phi, quantValue);
    }

    void setResult(MtlResult r) {
        MtlFormula phi = r.getPhi();
        if (formula2result.containsKey(phi) || !checks.contains(phi)) {
            throw new IllegalStateException();
        }
        formula2result.put(phi, r);
    }

    public Collection<MtlFormula> getChecks() {
        return checks;
    }

    public String getName(MtlFormula phi) {
        return formula2name.get(phi);
    }

    public MtlResult getResult(MtlFormula phi) {
        return formula2result.get(phi);
    }

    public MtlResult getResult(String name) {
        for (Map.Entry<MtlFormula, String> e: formula2name.entrySet()) {
            if (name.equals(e.getValue())) {
                MtlFormula phi = e.getKey();
                if (isQuantifiedCheck(phi)) {
                    throw new IllegalStateException();
                }
                return getResult(e.getKey());
            }
        }
        return null;
    }

    public MtlResult getResult(String name, int i) {
        for (Map.Entry<MtlFormula, String> e: formula2name.entrySet()) {
            if (name.equals(e.getValue())) {
                MtlFormula phi = e.getKey();
                if (!isQuantifiedCheck(phi)) {
                    throw new IllegalStateException();
                } else if (getQuantifierValue(phi) == i) {
                    return getResult(e.getKey());
                }
            }
        }
        return null;
    }

    public Integer getQuantifierValue(MtlFormula phi) {
        return formula2QuantValue.get(phi);
    }

    public boolean isCheck(MtlFormula phi) {
        return formula2result.keySet().contains(phi);
    }

    public boolean isQuantifiedCheck(MtlFormula phi) {
        return formula2QuantValue.keySet().contains(phi);
    }

    public boolean contains(MtlFormula phi) {
        return formula2name.keySet().contains(phi);
    }
}
