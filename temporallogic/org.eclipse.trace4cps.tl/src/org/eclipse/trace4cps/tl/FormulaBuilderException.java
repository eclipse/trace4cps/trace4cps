/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.tl;

import org.eclipse.trace4cps.core.TraceException;

public class FormulaBuilderException extends TraceException {
    private static final long serialVersionUID = 1L;

    public FormulaBuilderException() {
        super();
    }

    public FormulaBuilderException(String msg) {
        super(msg);
    }

    public FormulaBuilderException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
