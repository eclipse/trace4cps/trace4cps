/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.tl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import org.eclipse.trace4cps.analysis.mtl.InformativePrefix;
import org.eclipse.trace4cps.analysis.mtl.MtlChecker;
import org.eclipse.trace4cps.analysis.mtl.MtlFuture;
import org.eclipse.trace4cps.analysis.mtl.MtlResult;
import org.eclipse.trace4cps.analysis.mtl.State;
import org.eclipse.trace4cps.analysis.signal.impl.PsopHelper;
import org.eclipse.trace4cps.core.IInterval;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.impl.Event;
import org.eclipse.trace4cps.core.impl.Interval;
import org.eclipse.trace4cps.tl.etl.EtlModel;
import org.eclipse.trace4cps.tl.etl.Signal;
import org.eclipse.trace4cps.tl.etl.SignalDef;
import org.eclipse.trace4cps.tl.etl.TraceSignal;

public class VerificationHelper {
    private final ITrace trace;

    private final EtlModel model;

    private final VerificationResult verificationResult;

    private final List<State> states;

    private final Map<String, IPsop> signalMap;

    private final IInterval dom;

    private final int numInjectedStates;

    public VerificationHelper(ITrace trace, EtlModel model) throws FormulaBuilderException {
        this.trace = trace;
        this.model = model;
        signalMap = FormulaHelper.createSignalMap(trace, FormulaBuilder.get(model, SignalDef.class), false);
        dom = computeCommonTimeDomain(signalMap.values());
        if (dom.isEmpty()) {
            throw new FormulaBuilderException("Events and signals used have no common time domain");
        }
        addDerivedSignals();
        states = projectDiscreteStates();
        if (states.isEmpty()) {
            throw new FormulaBuilderException("No states on " + dom);
        }

        if (FormulaHelper.hasMixedCheck(model)) {
            numInjectedStates = injectSamplingStates();
        } else {
            numInjectedStates = 0;
        }
//        projectContinuousSignals();
        verificationResult = new FormulaBuilder(trace, signalMap, model).create();
    }

    public IInterval getTimeDomainForVerification() {
        return dom;
    }

    public boolean statesInjected() {
        return numInjectedStates > 0;
    }

    public int getNumInjectedStates() {
        return numInjectedStates;
    }

    /**
     * Runs the verification and writes returns the {@link VerificationResult}
     */
    public VerificationResult run() throws InterruptedException, ExecutionException {
        MtlChecker engine = new MtlChecker();
        Set<InformativePrefix> counterExamples = new HashSet<>();
        counterExamples.add(InformativePrefix.GOOD);
        counterExamples.add(InformativePrefix.BAD);
        counterExamples.add(InformativePrefix.NON_INFORMATIVE);
        MtlFuture f = engine.checkAll(states, verificationResult.getChecks(), counterExamples, true);
        for (MtlResult checkResult: f.get()) {
            verificationResult.setResult(checkResult);
        }
        return verificationResult;
    }

    /**
     * Adds the non-{@link TraceSignal} signals to the signalmap and projects them to dom
     */
    private void addDerivedSignals() throws FormulaBuilderException {
        for (SignalDef signalDef: FormulaBuilder.get(model, SignalDef.class)) {
            Signal signal = signalDef.getSignal();
            if (!(signal instanceof TraceSignal)) {
                IPsop psop = FormulaHelper.getDerivedSignal(trace, signal);
                if (psop != null && !psop.getFragments().isEmpty()) {
                    PsopHelper.projectTo(psop, dom);
                    signalMap.put(signalDef.getName(), psop);
                } else {
                    throw new FormulaBuilderException("Psop for " + signalDef.getName() + " is empty on domain " + dom);
                }
            }
        }
    }

    private List<State> projectDiscreteStates() {
        List<State> states = new ArrayList<State>();
        for (State state: trace.getEvents()) {
            if (dom.contains(state.getTimestamp())) {
                states.add(state);
            }
        }
        return states;
    }

    private int injectSamplingStates() {
        int cnt = 0;
        double domWidth = dom.ub().doubleValue() - dom.lb().doubleValue();
        double stepSize = domWidth / 10000; // ~10.000 dummy states

        int i = 0;
        while (i < states.size() - 1) {
            State si = states.get(i);
            State sj = states.get(i + 1);
            double delta = sj.getTimestamp().doubleValue() - si.getTimestamp().doubleValue();
            if (delta > stepSize) {
                long k = Math.round(Math.floor(delta / stepSize));
                for (int j = 0; j < k; j++) {
                    double t = si.getTimestamp().doubleValue() + ((j + 1) * stepSize);
                    if (t < sj.getTimestamp().doubleValue()) {
                        states.add(i + 1 + j, new Event(t));
                        cnt++;
                    }
                }
                i += k;
            }
            i++;
        }
        return cnt;
    }

    private IInterval computeCommonTimeDomain(Collection<IPsop> psops) {
        Interval dom = new Interval(trace.getEvents().get(0).getTimestamp(), false,
                trace.getEvents().get(trace.getEvents().size() - 1).getTimestamp(), false);
        for (IPsop p: psops) {
            dom = Interval.intersect(dom, PsopHelper.dom(p));
        }
        return dom;
    }
}
