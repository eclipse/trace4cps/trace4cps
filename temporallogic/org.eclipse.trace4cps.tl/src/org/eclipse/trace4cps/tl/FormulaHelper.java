/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.tl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.trace4cps.analysis.signal.SignalModifier;
import org.eclipse.trace4cps.analysis.signal.SignalUtil;
import org.eclipse.trace4cps.analysis.signal.impl.PsopHelper;
import org.eclipse.trace4cps.core.ClaimEventType;
import org.eclipse.trace4cps.core.IClaimEvent;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.impl.TraceHelper;
import org.eclipse.trace4cps.tl.etl.AndOrFormula;
import org.eclipse.trace4cps.tl.etl.ApFormula;
import org.eclipse.trace4cps.tl.etl.AttributeFilter;
import org.eclipse.trace4cps.tl.etl.Check;
import org.eclipse.trace4cps.tl.etl.ConvSpec;
import org.eclipse.trace4cps.tl.etl.EtlModel;
import org.eclipse.trace4cps.tl.etl.FinallyFormula;
import org.eclipse.trace4cps.tl.etl.FinallyUntimedFormula;
import org.eclipse.trace4cps.tl.etl.Formula;
import org.eclipse.trace4cps.tl.etl.GloballyFormula;
import org.eclipse.trace4cps.tl.etl.GloballyUntimedFormula;
import org.eclipse.trace4cps.tl.etl.IdString;
import org.eclipse.trace4cps.tl.etl.IfThenFormula;
import org.eclipse.trace4cps.tl.etl.Interval;
import org.eclipse.trace4cps.tl.etl.KeyVal;
import org.eclipse.trace4cps.tl.etl.LatencySignal;
import org.eclipse.trace4cps.tl.etl.MtlAp;
import org.eclipse.trace4cps.tl.etl.MtlApEnd;
import org.eclipse.trace4cps.tl.etl.MtlApStart;
import org.eclipse.trace4cps.tl.etl.NotFormula;
import org.eclipse.trace4cps.tl.etl.ReferenceFormula;
import org.eclipse.trace4cps.tl.etl.ResourceAmountSignal;
import org.eclipse.trace4cps.tl.etl.ResourceClientSignal;
import org.eclipse.trace4cps.tl.etl.Signal;
import org.eclipse.trace4cps.tl.etl.SignalDef;
import org.eclipse.trace4cps.tl.etl.ThroughputSignal;
import org.eclipse.trace4cps.tl.etl.TimeUnitEnum;
import org.eclipse.trace4cps.tl.etl.TopLevelModelElement;
import org.eclipse.trace4cps.tl.etl.TraceSignal;
import org.eclipse.trace4cps.tl.etl.UntilFormula;
import org.eclipse.trace4cps.tl.etl.UntilUntimedFormula;
import org.eclipse.trace4cps.tl.etl.WipSignal;

public class FormulaHelper {
    public enum FormulaType {
        MTL, STL, MIXED
    }

    private FormulaHelper() {
    }

    public static <T> T findContainer(EObject o, Class<T> clazz) {
        EObject c = o.eContainer();
        while (c != null) {
            if (clazz.isInstance(c)) {
                return clazz.cast(c);
            }
            c = c.eContainer();
        }
        return null;
    }

    public static TimeUnit valueOf(TimeUnitEnum tu) {
        switch (tu) {
            case NS:
                return TimeUnit.NANOSECONDS;
            case US:
                return TimeUnit.MICROSECONDS;
            case MS:
                return TimeUnit.MILLISECONDS;
            case S:
                return TimeUnit.SECONDS;
            case MIN:
                return TimeUnit.MINUTES;
            case HR:
                return TimeUnit.HOURS;
        }
        throw new IllegalArgumentException();
    }

    public static String toString(Interval i) {
        if (i.getInn() != null) {
            return "[" + i.getInn().getLb() + "," + i.getInn().getUb() + "] " + i.getTimeUnit();
        }
        if (i.getIns() != null) {
            return "[" + i.getIns().getLb() + "," + (i.getIns().getInfty() != null ? "Infty" : i.getIns().getUb())
                    + ") " + i.getTimeUnit();
        }
        if (i.getIsn() != null) {
            return "(" + i.getIsn().getLb() + "," + i.getIsn().getUb() + "] " + i.getTimeUnit();
        }
        if (i.getIss() != null) {
            return "(" + i.getIss().getLb() + "," + (i.getIss().getInfty() != null ? "Infty" : i.getIss().getUb())
                    + ") " + i.getTimeUnit();
        }
        throw new IllegalStateException("Unexpected structure in model");
    }

    public static String toString(IdString s) {
        StringBuilder b = new StringBuilder();
        if (s.getLeft() != null) {
            b.append(s.getLeft());
        }
        if (s.getId() != null) {
            b.append(s.getId());
        }
        if (s.getRight() != null) {
            b.append(s.getRight());
        }
        return b.toString();
    }

    public static boolean hasMixedCheck(EtlModel m) {
        for (TopLevelModelElement e: m.getElements()) {
            if (e instanceof Check) {
                if (getFormulaType(((Check)e).getFormula()) == FormulaType.MIXED) {
                    return true;
                }
            }
        }
        return false;
    }

    public static FormulaType getFormulaType(Formula phi) {
        // check whether we have both MTL aps and STL aps
        List<ApFormula> aps = new ArrayList<>();
        visit(phi, aps);
        boolean hasMTL = false;
        boolean hasSTL = false;
        for (ApFormula f: aps) {
            if (f.getMtlAP() != null) {
                hasMTL = true;
            } else {
                hasSTL = true;
            }
        }
        if (hasMTL && !hasSTL) {
            return FormulaType.MTL;
        } else if (!hasMTL && hasSTL) {
            return FormulaType.STL;
        } else if (hasMTL && hasSTL) {
            return FormulaType.MIXED;
        }
        return null;
    }

    private static void visit(Formula f, List<ApFormula> aps) {
        if (f instanceof ApFormula) {
            aps.add((ApFormula)f);
        } else if (f instanceof ReferenceFormula) {
            visit(((ReferenceFormula)f).getDef().getFormula(), aps);
        } else if (f instanceof AndOrFormula) {
            visit(((AndOrFormula)f).getLeft(), aps);
            visit(((AndOrFormula)f).getRight(), aps);
        } else if (f instanceof IfThenFormula) {
            visit(((IfThenFormula)f).getLeft(), aps);
            visit(((IfThenFormula)f).getRight(), aps);
        } else if (f instanceof NotFormula) {
            visit(((NotFormula)f).getFormula(), aps);
        } else if (f instanceof FinallyFormula) {
            visit(((FinallyFormula)f).getFormula(), aps);
        } else if (f instanceof FinallyUntimedFormula) {
            visit(((FinallyUntimedFormula)f).getFormula(), aps);
        } else if (f instanceof GloballyFormula) {
            visit(((GloballyFormula)f).getFormula(), aps);
        } else if (f instanceof GloballyUntimedFormula) {
            visit(((GloballyUntimedFormula)f).getFormula(), aps);
        } else if (f instanceof UntilFormula) {
            visit(((UntilFormula)f).getLeft(), aps);
            visit(((UntilFormula)f).getRight(), aps);
        } else if (f instanceof UntilUntimedFormula) {
            visit(((UntilUntimedFormula)f).getLeft(), aps);
            visit(((UntilUntimedFormula)f).getRight(), aps);
        }
    }

    /**
     * Creates a map from signal names ({@link TraceSignal} instances) to {@link IPsop}s. The Psops from the ITrace data
     * are copied so that we can manipulate them (projection to time domain)
     */
    public static Map<String, IPsop> createSignalMap(ITrace trace, List<SignalDef> signalDefs, boolean includeDerived)
            throws FormulaBuilderException
    {
        Map<String, IPsop> signalMap = new HashMap<String, IPsop>();
        if (signalDefs.isEmpty()) {
            return signalMap;
        }
        for (SignalDef signalDef: signalDefs) {
            Signal s = signalDef.getSignal();
            if (s instanceof TraceSignal) {
                Map<String, String> filter = FormulaBuilder.toMap(((TraceSignal)s).getFilter(), null);
                boolean found = false;
                for (IPsop psop: trace.getSignals()) {
                    if (TraceHelper.matches(filter, psop.getAttributes())) {
                        if (!found) {
                            signalMap.put(signalDef.getName(), PsopHelper.copy(psop));
                            found = true;
                        } else {
                            throw new FormulaBuilderException(
                                    "Filter for " + signalDef.getName() + " matches multiple signals");
                        }
                    }
                }
                if (!found) {
                    throw new FormulaBuilderException(
                            "Filter for " + signalDef.getName() + " does not match any signal");
                }
            } else if (includeDerived) {
                IPsop psop = FormulaHelper.getDerivedSignal(trace, s);
                if (psop != null && !psop.getFragments().isEmpty()) {
                    signalMap.put(signalDef.getName(), psop);
                }
            }
        }
        return signalMap;
    }

    public static IPsop getDerivedSignal(ITrace trace, Signal signal) throws FormulaBuilderException {
        if (signal instanceof ThroughputSignal) {
            return getDerivedTPsignal(trace, (ThroughputSignal)signal);
        } else if (signal instanceof LatencySignal) {
            return getDerivedLatSignal(trace, (LatencySignal)signal);
        } else if (signal instanceof WipSignal) {
            return getDerivedWipSignal(trace, (WipSignal)signal);
        } else if (signal instanceof ResourceAmountSignal) {
            return getDerivedResourceAmountSignal(trace, (ResourceAmountSignal)signal);
        } else if (signal instanceof ResourceClientSignal) {
            return getDerivedResourceClientSignal(trace, (ResourceClientSignal)signal);
        }
        return null;
    }

    private static IPsop getDerivedResourceClientSignal(ITrace trace, ResourceClientSignal signal)
            throws FormulaBuilderException
    {
        SignalModifier mod = getModifier(signal.getConvSpec());
        IResource resource = findResource(trace, signal.getFilter());
        return SignalUtil.getResourceClients(trace, resource, mod);
    }

    private static IPsop getDerivedResourceAmountSignal(ITrace trace, ResourceAmountSignal signal)
            throws FormulaBuilderException
    {
        SignalModifier mod = getModifier(signal.getConvSpec());
        IResource resource = findResource(trace, signal.getFilter());
        return SignalUtil.getResourceAmount(trace, resource, mod);
    }

    private static IPsop getDerivedWipSignal(ITrace trace, WipSignal signal) {
        SignalModifier mod = getModifier(signal.getConvSpec());
        return SignalUtil.getWip(trace, signal.getIdAtt(), mod);
    }

    private static IPsop getDerivedLatSignal(ITrace trace, LatencySignal signal) {
        TimeUnit scaleUnit = signal.getScale() == null ? trace.getTimeUnit() : valueOf(signal.getScale());
        double windowWidth = signal.getConvSpec() == null ? 0d : signal.getConvSpec().getWindowWidth();
        TimeUnit windowTimeUnit = signal.getConvSpec() == null ? trace.getTimeUnit()
                : valueOf(signal.getConvSpec().getWindowUnit());
        return SignalUtil.getLatency(trace, signal.getIdAtt(), scaleUnit, windowWidth, windowTimeUnit);
    }

    private static IPsop getDerivedTPsignal(ITrace trace, ThroughputSignal signal) {
        TimeUnit scaleUnit = signal.getScale() == null ? trace.getTimeUnit() : valueOf(signal.getScale());
        double windowWidth = signal.getConvSpec() == null ? 0d : signal.getConvSpec().getWindowWidth();
        TimeUnit windowTimeUnit = signal.getConvSpec() == null ? trace.getTimeUnit()
                : valueOf(signal.getConvSpec().getWindowUnit());
        if (signal.getIdAtt() != null) {
            return SignalUtil.getTP(trace, signal.getIdAtt(), scaleUnit, windowWidth, windowTimeUnit);
        } else {
            return SignalUtil.getTP(trace.getTimeUnit(), TraceHelper.getDomain(trace),
                    filterEvents(trace, signal.getAp()), scaleUnit, windowWidth, windowTimeUnit);
        }
    }

    private static IResource findResource(ITrace trace, AttributeFilter keyvals) throws FormulaBuilderException {
        Map<String, String> filter = FormulaBuilder.toMap(keyvals, null);
        IResource res = null;
        for (IResource r: trace.getResources()) {
            if (TraceHelper.matches(filter, r.getAttributes())) {
                if (res != null) {
                    throw new FormulaBuilderException("Filter " + filter + " selects multiple resources");
                }
                res = r;
            }
        }
        if (res == null) {
            throw new FormulaBuilderException("Filter " + filter + " selects no resource");
        }
        return res;
    }

    private static SignalModifier getModifier(ConvSpec cs) {
        double windowWidth = 0d;
        TimeUnit wtu = TimeUnit.SECONDS; // doesn't matter if width=0
        if (cs != null) {
            windowWidth = cs.getWindowWidth();
            wtu = FormulaHelper.valueOf(cs.getWindowUnit());
        }
        return new SignalModifier(1d, windowWidth, wtu);
    }

    private static List<IEvent> filterEvents(ITrace trace, MtlAp kvs) {
        Map<String, String> filter = new HashMap<String, String>();
        for (KeyVal kv: kvs.getFilter().getKeyVals()) {
            filter.put(kv.getAtt().getLeft(), kv.getVal().getLeft());
        }
        if (kvs instanceof MtlApStart) {
            List<IEvent> events = new ArrayList<>();
            for (IEvent e: trace.getEvents()) {
                if (e instanceof IClaimEvent) {
                    if (((IClaimEvent)e).getType() == ClaimEventType.START
                            && TraceHelper.matches(filter, e.getAttributes()))
                    {
                        events.add(e);
                    }
                }
            }
            return events;
        } else if (kvs instanceof MtlApEnd) {
            List<IEvent> events = new ArrayList<>();
            for (IEvent e: trace.getEvents()) {
                if (e instanceof IClaimEvent) {
                    if (((IClaimEvent)e).getType() == ClaimEventType.END
                            && TraceHelper.matches(filter, e.getAttributes()))
                    {
                        events.add(e);
                    }
                }
            }
            return events;
        } else {
            return TraceHelper.filter(trace.getEvents(), filter);
        }
    }

    public static org.eclipse.trace4cps.core.impl.Interval from(TimeUnit traceTimeUnit, Interval i) {
        TimeUnitEnum timeUnit = i.getTimeUnit();
        double factor = getFactor(traceTimeUnit, timeUnit);
        if (i.getInn() != null) {
            return new org.eclipse.trace4cps.core.impl.Interval(i.getInn().getLb() * factor, false,
                    i.getInn().getUb() * factor, false);
        }
        if (i.getIns() != null) {
            if (i.getIns().getInfty() != null) {
                return new org.eclipse.trace4cps.core.impl.Interval(i.getIns().getLb() * factor, false,
                        Double.POSITIVE_INFINITY, true);
            }
            return new org.eclipse.trace4cps.core.impl.Interval(i.getIns().getLb() * factor, false,
                    i.getIns().getUb() * factor, true);
        }
        if (i.getIsn() != null) {
            return new org.eclipse.trace4cps.core.impl.Interval(i.getIsn().getLb() * factor, true,
                    i.getIsn().getUb() * factor, false);
        }
        if (i.getIss() != null) {
            if (i.getIss().getInfty() != null) {
                return new org.eclipse.trace4cps.core.impl.Interval(i.getIss().getLb() * factor, true,
                        Double.POSITIVE_INFINITY, true);
            }
            return new org.eclipse.trace4cps.core.impl.Interval(i.getIss().getLb() * factor, true,
                    i.getIss().getUb() * factor, true);
        }
        throw new IllegalStateException("unsupported interval");
    }

    public static double getFactor(TimeUnit traceTimeUnit, TimeUnitEnum specTimeUnit) {
        switch (specTimeUnit) {
            case NS:
                return SignalUtil.convert(traceTimeUnit, 1d, TimeUnit.NANOSECONDS);
            case US:
                return SignalUtil.convert(traceTimeUnit, 1d, TimeUnit.MICROSECONDS);
            case MS:
                return SignalUtil.convert(traceTimeUnit, 1d, TimeUnit.MILLISECONDS);
            case S:
                return SignalUtil.convert(traceTimeUnit, 1d, TimeUnit.SECONDS);
            case MIN:
                return SignalUtil.convert(traceTimeUnit, 1d, TimeUnit.MINUTES);
            case HR:
                return SignalUtil.convert(traceTimeUnit, 1d, TimeUnit.HOURS);
        }
        throw new UnsupportedOperationException();
    }
}
