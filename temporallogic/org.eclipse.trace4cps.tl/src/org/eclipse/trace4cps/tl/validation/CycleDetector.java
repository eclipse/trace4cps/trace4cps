/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.tl.validation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CycleDetector {
    private final Set<String> vertices = new HashSet<>();

    private final Map<String, Set<String>> successors = new HashMap<>();

    private void addVertex(String name) {
        vertices.add(name);
        if (!successors.containsKey(name)) {
            successors.put(name, new HashSet<>());
        }
    }

    public void addEdge(String src, String dst) {
        addVertex(src);
        addVertex(dst);
        successors.get(src).add(dst);
    }

    private boolean dfs(String v, Set<String> white, Set<String> grey, Set<String> black) {
        white.remove(v);
        grey.add(v);
        for (String w: successors.get(v)) {
            if (black.contains(w)) {
                continue;
            }
            if (grey.contains(w)) {
                return true;
            }
            if (dfs(w, white, grey, black)) {
                return true;
            }
        }
        grey.remove(v);
        black.add(v);
        return false;
    }

    public boolean hasCycle() {
        Set<String> white = new HashSet<String>();
        Set<String> grey = new HashSet<String>();
        Set<String> black = new HashSet<String>();

        white.addAll(vertices);
        while (!white.isEmpty()) {
            for (String v: vertices) {
                if (white.contains(v)) {
                    if (dfs(v, white, grey, black)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
