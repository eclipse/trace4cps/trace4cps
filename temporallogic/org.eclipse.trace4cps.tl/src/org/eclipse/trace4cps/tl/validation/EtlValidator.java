/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.tl.validation;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.trace4cps.tl.FormulaHelper;
import org.eclipse.trace4cps.tl.FormulaHelper.FormulaType;
import org.eclipse.trace4cps.tl.etl.Def;
import org.eclipse.trace4cps.tl.etl.EtlModel;
import org.eclipse.trace4cps.tl.etl.EtlPackage;
import org.eclipse.trace4cps.tl.etl.IdString;
import org.eclipse.trace4cps.tl.etl.IntervalNN;
import org.eclipse.trace4cps.tl.etl.IntervalNS;
import org.eclipse.trace4cps.tl.etl.IntervalSN;
import org.eclipse.trace4cps.tl.etl.IntervalSS;
import org.eclipse.trace4cps.tl.etl.ReferenceFormula;
import org.eclipse.trace4cps.tl.etl.TopLevelModelElement;
import org.eclipse.trace4cps.tl.etl.UntilFormula;
import org.eclipse.trace4cps.tl.etl.UntilUntimedFormula;
import org.eclipse.xtext.validation.Check;
import org.eclipse.xtext.validation.CheckType;

/**
 * This class contains custom validation rules.
 *
 * <p>
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#validation
 * </p>
 */
public class EtlValidator extends AbstractEtlValidator {
    @Check(CheckType.FAST)
    public void infoSTLmixFormulas(org.eclipse.trace4cps.tl.etl.Check c) {
        if (FormulaHelper.getFormulaType(c.getFormula()) == FormulaType.MIXED) {
            info("Mixed formula: be aware of sampling semantics", EtlPackage.Literals.TOP_LEVEL_MODEL_ELEMENT__NAME);
        }
    }

    @Check(CheckType.FAST)
    public void infoSTLgeneralUntilFormulas(UntilFormula f) {
        boolean leftIsSTL = FormulaHelper.getFormulaType(f.getLeft()) == FormulaType.STL;
        boolean rightIsSTL = FormulaHelper.getFormulaType(f.getRight()) == FormulaType.STL;
        if (leftIsSTL && rightIsSTL) {
            info("General until not supported for STL: reverting to sampling semantics",
                    EtlPackage.Literals.UNTIL_FORMULA__LEFT);
        }
    }

    @Check(CheckType.FAST)
    public void infoSTLgeneralUntilFormulas2(UntilUntimedFormula f) {
        boolean leftIsSTL = FormulaHelper.getFormulaType(f.getLeft()) == FormulaType.STL;
        boolean rightIsSTL = FormulaHelper.getFormulaType(f.getRight()) == FormulaType.STL;
        if (leftIsSTL && rightIsSTL) {
            info("General until not supported for STL: reverting to sampling semantics",
                    EtlPackage.Literals.UNTIL_UNTIMED_FORMULA__LEFT);
        }
    }

    @Check(CheckType.FAST)
    public void checkUniqueTopLevelIDs(TopLevelModelElement e) {
        EtlModel m = (EtlModel)e.eContainer();
        for (TopLevelModelElement e2: m.getElements()) {
            if (!e.equals(e2) && e.getName().equals(e2.getName())) {
                error("Duplicate identifier", EtlPackage.Literals.TOP_LEVEL_MODEL_ELEMENT__NAME);
            }
        }
    }

    @Check(CheckType.FAST)
    public void checkParamDefUsesParameter(Def d) {
        String param = d.getParam();
        if (param != null && !paramUsed(param, d)) {
            error("Parameter " + param + " is not used", EtlPackage.Literals.DEF__PARAM);
        }
    }

    @Check(CheckType.FAST)
    public void checkParamCheckUsesParameter(org.eclipse.trace4cps.tl.etl.Check c) {
        String param = c.getVar();
        if (param != null && !paramUsed(param, c)) {
            error("Variable " + param + " is not used", EtlPackage.Literals.CHECK__VAR);
        }
    }

    @Check(CheckType.FAST)
    public void checkParameterDeclared(IdString id) {
        String param = id.getId();
        if (param != null && !paramDeclared(param, id)) {
            error("Parameter " + param + " is not declared", EtlPackage.Literals.ID_STRING__ID);
        }
    }

    @Check(CheckType.FAST)
    public void checkParameterDeclared(ReferenceFormula ref) {
        String param = ref.getParam();
        if (param != null && !paramDeclared(param, ref)) {
            error("Parameter " + param + " is not declared", EtlPackage.Literals.REFERENCE_FORMULA__PARAM);
        }
    }

    @Check(CheckType.FAST)
    public void checkCycles(EtlModel model) {
        CycleDetector det = new CycleDetector();
        Map<String, Def> map = new HashMap<>();
        for (TreeIterator<EObject> it = model.eAllContents(); it.hasNext();) {
            EObject o = it.next();
            if (o instanceof ReferenceFormula) {
                ReferenceFormula ref = (ReferenceFormula)o;
                Def src = FormulaHelper.findContainer(ref, Def.class);
                if (src != null) {
                    det.addEdge(src.getName(), ref.getDef().getName());
                    map.put(src.getName(), src);
                    map.put(ref.getDef().getName(), ref.getDef());
                }
            }
        }
        if (det.hasCycle()) {
            for (Map.Entry<String, Def> e: map.entrySet()) {
//                if (det.hasCycle(e.getKey())) {
                error("Cycle in definitions", e.getValue(), EtlPackage.Literals.TOP_LEVEL_MODEL_ELEMENT__NAME);
//                }
            }
        }
    }

    @Check(CheckType.FAST)
    public void checkNonEmptyIntervalSS(IntervalSS i) {
        if (i.getLb() < 0) {
            error("Lower bound must be at least 0", EtlPackage.Literals.INTERVAL_SS__LB);
        }
        if (i.getInfty() == null && i.getLb() >= i.getUb()) {
            error("Upper bound must be larger than lower bound", EtlPackage.Literals.INTERVAL_SS__UB);
        }
    }

    @Check(CheckType.FAST)
    public void checkNonEmptyIntervalSN(IntervalSN i) {
        if (i.getLb() < 0) {
            error("Lower bound must be at least 0", EtlPackage.Literals.INTERVAL_SN__LB);
        }
        if (i.getLb() >= i.getUb()) {
            error("Upper bound must be larger than lower bound", EtlPackage.Literals.INTERVAL_SN__UB);
        }
    }

    @Check(CheckType.FAST)
    public void checkNonEmptyIntervalNS(IntervalNS i) {
        if (i.getLb() < 0) {
            error("Lower bound must be at least 0", EtlPackage.Literals.INTERVAL_NS__LB);
        }
        if (i.getInfty() == null && i.getLb() >= i.getUb()) {
            error("Upper bound must be larger than lower bound", EtlPackage.Literals.INTERVAL_NS__UB);
        }
    }

    @Check(CheckType.FAST)
    public void checkNonEmptyIntervalNN(IntervalNN i) {
        if (i.getLb() < 0) {
            error("Lower bound must be at least 0", EtlPackage.Literals.INTERVAL_NN__LB);
        }
        if (i.getLb() > i.getUb()) {
            error("Upper bound must be at least lower bound", EtlPackage.Literals.INTERVAL_NN__UB);
        }
    }

    private boolean paramDeclared(String param, EObject obj) {
        Def def = FormulaHelper.findContainer(obj, Def.class);
        if (def != null) {
            return def.getParam() != null && param.equals(def.getParam());
        }
        org.eclipse.trace4cps.tl.etl.Check check = FormulaHelper.findContainer(obj,
                org.eclipse.trace4cps.tl.etl.Check.class);
        if (check != null) {
            return check.getVar() != null && param.equals(check.getVar());
        }
        return false;
    }

    private boolean paramUsed(String param, EObject root) {
        for (TreeIterator<Object> it = EcoreUtil.getAllContents(Collections.singletonList(root)); it.hasNext();) {
            Object next = it.next();
            if (next instanceof IdString) {
                IdString id = (IdString)next;
                if (param.equals(id.getId())) {
                    return true;
                }
            } else if (next instanceof ReferenceFormula) {
                ReferenceFormula r = (ReferenceFormula)next;
                if (param.equals(r.getParam())) {
                    return true;
                }
            }
        }
        return false;
    }
}
