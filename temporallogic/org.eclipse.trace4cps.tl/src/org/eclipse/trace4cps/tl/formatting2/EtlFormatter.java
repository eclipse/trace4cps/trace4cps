/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.tl.formatting2;

import org.eclipse.trace4cps.tl.etl.EtlModel;
import org.eclipse.trace4cps.tl.etl.SignalDef;
import org.eclipse.trace4cps.tl.etl.TopLevelModelElement;
import org.eclipse.xtext.formatting2.AbstractJavaFormatter;
import org.eclipse.xtext.formatting2.IFormattableDocument;

public class EtlFormatter extends AbstractJavaFormatter {
    protected void format(EtlModel etlModel, IFormattableDocument doc) {
        // TODO: format HiddenRegions around keywords, attributes, cross references, etc.
        for (TopLevelModelElement topLevelModelElement: etlModel.getElements()) {
            doc.format(topLevelModelElement);
            doc.append(topLevelModelElement, i -> i.setNewLines(2));
        }
    }

    protected void format(SignalDef signalDef, IFormattableDocument doc) {
        // TODO: format HiddenRegions around keywords, attributes, cross references, etc.
        doc.format(signalDef.getSignal());
    }

    // TODO: implement for Def, Check, TraceSignal, ThroughputSignal, LatencySignal, WipSignal, ResourceAmountSignal,
    // ResourceClientSignal, ApFormula, NotFormula, AndOrFormula, IfThenFormula, GloballyUntimedFormula,
    // GloballyFormula, FinallyUntimedFormula, FinallyFormula, UntilUntimedFormula, UntilFormula, MtlAp,
    // AttributeFilter, KeyVal, Interval
}
