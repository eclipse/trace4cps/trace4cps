@REM
@REM Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
@REM
@REM This program and the accompanying materials are made
@REM available under the terms of the Eclipse Public License 2.0
@REM which is available at https://www.eclipse.org/legal/epl-2.0/
@REM
@REM SPDX-License-Identifier: EPL-2.0
@REM

@echo on

mvn -Dtycho.pomless.aggregator.names=releng,temporallogic,trace,jfreechart %*


