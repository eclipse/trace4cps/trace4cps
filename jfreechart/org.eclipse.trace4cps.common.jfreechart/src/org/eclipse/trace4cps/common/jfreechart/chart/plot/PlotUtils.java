/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.chart.plot;

import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.CombinedRangeXYPlot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.general.DatasetUtils;
import org.jfree.data.xy.XYDataset;

/**
 * Some utility methods related to the plot classes.
 */
public class PlotUtils {
    /**
     * Private constructor prevents object creation.
     */
    private PlotUtils() {
        // Empty for utility classes
    }

    /**
     * Returns <code>true</code> if all the {@link XYPlot#getDatasets() data sets} belonging to the specified plot are
     * empty or <code>null</code>, and <code>false</code> otherwise.
     *
     * @param plot the plot (<code>null</code> permitted).
     *
     * @return A boolean.
     */
    public static boolean isEmptyOrNull(XYPlot plot) {
        return plot == null || plot.getDatasets().values().stream().allMatch(DatasetUtils::isEmptyOrNull);
    }

    /**
     * Returns the cumulative amount of items for all datasets in the plot.
     *
     * @param plot the plot (<code>null</code> not permitted).
     * @return the cumulative amount of items for all datasets in the plot.
     */
    public static int getItemCount(XYPlot plot) {
        int itemCount = 0;
        for (int d = 0; d < plot.getDatasetCount(); d++) {
            XYDataset dataset = plot.getDataset(d);
            for (int s = 0; s < dataset.getSeriesCount(); s++) {
                itemCount += dataset.getItemCount(s);
            }
        }
        return itemCount;
    }

    /**
     * Returns all {@link XYPlot#getRenderers() renderers} of {@code plot}, including the renderers of its sub-plots if
     * {@code plot} is either a {@link CombinedRangeXYPlot} or {@link CombinedDomainXYPlot}.
     *
     * @param plot the plot (<code>null</code> not permitted).
     * @return the renderers of the plot and its sub-plots (if any).
     */
    public static List<XYItemRenderer> getAllRenderers(XYPlot plot) {
        ArrayList<XYItemRenderer> renderers = new ArrayList<>(plot.getRenderers().values());
        if (plot instanceof CombinedRangeXYPlot) {
            ((CombinedRangeXYPlot)plot).getSubplots().forEach(p -> renderers.addAll(getAllRenderers(p)));
        } else if (plot instanceof CombinedDomainXYPlot) {
            ((CombinedDomainXYPlot)plot).getSubplots().forEach(p -> renderers.addAll(getAllRenderers(p)));
        }
        return renderers;
    }
}
