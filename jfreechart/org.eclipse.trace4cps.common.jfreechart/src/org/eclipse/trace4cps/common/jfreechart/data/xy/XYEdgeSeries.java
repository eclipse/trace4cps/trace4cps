/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.data.xy;

import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.util.Args;
import org.jfree.data.general.Series;

/**
 * A list of {@link XYEdgeDataItem}.
 *
 * @see XYEdgeSeriesCollection
 */
public class XYEdgeSeries extends Series {
    private static final long serialVersionUID = 7175710875342448296L;

    private final List<XYEdgeDataItem> items = new ArrayList<>();

    public XYEdgeSeries(Comparable<?> key) {
        super(key);
    }

    public XYEdgeSeries(Comparable<?> key, String description) {
        super(key, description);
    }

    public void add(XYEdgeDataItem item, boolean notify) {
        Args.nullNotPermitted(item, "item");
        items.add(item);
        if (notify) {
            fireSeriesChanged();
        }
    }

    public void removeAllSeries() {
        items.clear();
        fireSeriesChanged();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public XYEdgeDataItem getDataItem(int item) {
        if ((item < 0) || (item >= getItemCount())) {
            throw new IllegalArgumentException("Series index out of bounds");
        }
        return items.get(item);
    }

    public Number getX(int item) {
        XYEdgeDataItem dataItem = getDataItem(item);
        return (dataItem.getX0Value() * 0.5) + (dataItem.getX1Value() * 0.5);
    }

    public Number getY(int item) {
        XYEdgeDataItem dataItem = getDataItem(item);
        return (dataItem.getY0Value() * 0.5) + (dataItem.getY1Value() * 0.5);
    }

    public Number getStartX(int item) {
        return getDataItem(item).getX0Value();
    }

    public Number getEndX(int item) {
        return getDataItem(item).getX1Value();
    }

    public Number getStartY(int item) {
        return getDataItem(item).getY0Value();
    }

    public Number getEndY(int item) {
        return getDataItem(item).getY1Value();
    }
}
