/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.chart;

import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import org.jfree.chart.ChartPanel;

/**
 * This handler handles zooming, using the mouse wheel.
 */
public class ZoomMouseWheelHandler implements MouseWheelListener {
    @Override
    public void mouseWheelMoved(MouseWheelEvent event) {
        Object source = event.getSource();
        if (!(source instanceof ChartPanel)) {
            return;
        }
        ChartPanel chartPanel = (ChartPanel)source;
        if (event.isControlDown() && event.isShiftDown()) {
            if (event.getWheelRotation() > 0) {
                chartPanel.zoomOutRange(event.getX(), event.getY());
            } else {
                chartPanel.zoomInRange(event.getX(), event.getY());
            }
            event.consume();
        } else if (event.isControlDown()) {
            if (event.getWheelRotation() > 0) {
                chartPanel.zoomOutDomain(event.getX(), event.getY());
            } else {
                chartPanel.zoomInDomain(event.getX(), event.getY());
            }
            event.consume();
        }
    }
}
