/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.chart.axis;

import java.io.Serializable;

import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.TickUnit;
import org.jfree.chart.axis.TickUnitSource;

public class BaseExponentTickUnitSource implements TickUnitSource, Serializable {
    private static final long serialVersionUID = -1558705155970046957L;

    private final double base;

    private final int minExponent;

    private final int maxExponent;

    public BaseExponentTickUnitSource(double base) {
        this(base, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    public BaseExponentTickUnitSource(double base, int minExponent) {
        this(base, minExponent, Integer.MAX_VALUE);
    }

    public BaseExponentTickUnitSource(double base, int minExponent, int maxExponent) {
        this.base = base;
        this.minExponent = minExponent;
        this.maxExponent = maxExponent;
    }

    @Override
    public TickUnit getLargerTickUnit(TickUnit unit) {
        double size = unit.getSize();
        double exp = Math.floor(Math.log(size) / Math.log(base));
        double fac = size / Math.pow(base, exp);
        if (exp < minExponent) {
            exp = minExponent;
            fac = 1;
        } else if (exp > maxExponent) {
            exp = maxExponent;
            fac = 5;
        } else if (fac < 1) {
            fac = 1;
        } else if (fac < 2) {
            fac = 2;
        } else if (fac < 3) {
            fac = 3;
        } else if (fac < 5 || exp == maxExponent) {
            fac = 5;
        } else {
            exp = exp + 1;
            fac = 1;
        }
        return new NumberTickUnit(fac * Math.pow(base, exp));
    }

    @Override
    public TickUnit getCeilingTickUnit(TickUnit unit) {
        return getCeilingTickUnit(unit.getSize());
    }

    @Override
    public TickUnit getCeilingTickUnit(double size) {
        double exp = Math.floor(Math.log(size) / Math.log(base));
        double fac = size / Math.pow(base, exp);
        if (exp < minExponent) {
            exp = minExponent;
            fac = 1;
        } else if (exp > maxExponent) {
            exp = maxExponent;
            fac = 5;
        } else if (fac <= 1) {
            fac = 1;
        } else if (fac <= 2) {
            fac = 2;
        } else if (fac <= 3) {
            fac = 3;
        } else if (fac <= 5 || exp == maxExponent) {
            fac = 5;
        } else {
            exp = exp + 1;
            fac = 1;
        }
        return new NumberTickUnit(fac * Math.pow(base, exp));
    }
}
