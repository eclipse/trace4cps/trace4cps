/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.chart.geom;

import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

/**
 * A pointy arrow shape.
 */
public abstract class Arrow2D implements Shape {
    protected final Path2D arrow;

    protected Arrow2D(Path2D arrow) {
        this.arrow = arrow;
    }

    /**
     * An arrow with double precision and center anchor.
     */
    public static class Double extends Arrow2D {
        /**
         * Creates an instance of {@link Arrow2D} with {@link java.lang.Double double} precision.<br>
         * Using a positive <code>width</code> value creates a right pointing arrow and a negative <code>width</code>
         * value creates a left pointing arrow.
         */
        public Double(double x, double y, double width, double height) {
            super(new Path2D.Double());
            arrow.moveTo(x + width, y + (height / 2));
            arrow.lineTo(x, y + height);
            arrow.lineTo(x + (width / 2), y + (height / 2));
            arrow.lineTo(x, y);
            arrow.closePath();
        }
    }

    @Override
    public Rectangle2D getBounds2D() {
        return arrow.getBounds2D();
    }

    @Override
    public final Rectangle getBounds() {
        return arrow.getBounds();
    }

    @Override
    public final boolean contains(double x, double y) {
        return arrow.contains(x, y);
    }

    @Override
    public final boolean contains(Point2D p) {
        return arrow.contains(p);
    }

    @Override
    public final boolean contains(double x, double y, double w, double h) {
        return arrow.contains(x, y, w, h);
    }

    @Override
    public final boolean contains(Rectangle2D r) {
        return arrow.contains(r);
    }

    @Override
    public final boolean intersects(double x, double y, double w, double h) {
        return arrow.intersects(x, y, w, h);
    }

    @Override
    public final boolean intersects(Rectangle2D r) {
        return arrow.intersects(r);
    }

    @Override
    public PathIterator getPathIterator(AffineTransform at) {
        return arrow.getPathIterator(at);
    }

    @Override
    public final PathIterator getPathIterator(AffineTransform at, double flatness) {
        return arrow.getPathIterator(at, flatness);
    }
}
