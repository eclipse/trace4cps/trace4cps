/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.chart.renderer.xy;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

import org.eclipse.trace4cps.common.jfreechart.chart.geom.Arrow2D;
import org.jfree.chart.LegendItem;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.EntityCollection;
import org.jfree.chart.plot.CrosshairState;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.AbstractXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRendererState;
import org.jfree.chart.ui.RectangleEdge;
import org.jfree.chart.util.BooleanList;
import org.jfree.chart.util.LineUtils;
import org.jfree.chart.util.ShapeList;
import org.jfree.chart.util.ShapeUtils;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYDataset;

/**
 * Renders edges (i.e. lines) on an {@link XYPlot}, requires an {@link IntervalXYDataset}. Lines can be configured to
 * have begin and/or end shapes, e.g. an {@link Arrow2D}.
 */
public class XYEdgeRenderer extends AbstractXYItemRenderer implements XYItemRenderer {
    private static final long serialVersionUID = -754074656456575380L;

    public static final boolean DEFAULT_SHAPE_ROTATE = true;

    public static final boolean DEFAULT_DRAW_PLOT_CROSSING_LINES = true;

    private final ShapeList beginShapes = new ShapeList();

    private final BooleanList beginShapesRotate = new BooleanList();

    private final ShapeList endShapes = new ShapeList();

    private final BooleanList endShapesRotate = new BooleanList();

    private final BooleanList drawPlotCrossingLines = new BooleanList();

    private Shape defaultBeginShape;

    private Boolean defaultBeginShapeRotate;

    private Shape defaultEndShape;

    private Boolean defaultEndShapeRotate;

    private boolean defaultShapeRotate;

    private boolean defaultDrawPlotCrossingLines;

    public XYEdgeRenderer() {
        this.defaultShapeRotate = DEFAULT_SHAPE_ROTATE;
        this.defaultDrawPlotCrossingLines = DEFAULT_DRAW_PLOT_CROSSING_LINES;
        setDefaultShape(new Rectangle2D.Double(-4.0, -4.0, 8.0, 8.0));
        setAutoPopulateSeriesShape(false);
        setDataBoundsIncludesVisibleSeriesOnly(false);
    }

    public void setDefaultDrawPlotCrossingLines(boolean drawCrossingLines, boolean notify) {
        this.defaultDrawPlotCrossingLines = drawCrossingLines;
        if (notify) {
            fireChangeEvent();
        }
    }

    /**
     * @return true to render lines which start and end outside, but cross the visible area of the plot
     */
    public boolean getDefaultDrawPlotCrossingLines() {
        return this.defaultDrawPlotCrossingLines;
    }

    /**
     * @see #getDefaultDrawPlotCrossingLines()
     */
    public boolean getSeriesDrawPlotCrossingLines(int series) {
        Boolean drawCrossingLines = this.drawPlotCrossingLines.getBoolean(series);
        return null == drawCrossingLines ? getDefaultDrawPlotCrossingLines() : drawCrossingLines;
    }

    public void setSeriesDrawPlotCrossingLines(int series, boolean drawCrossingLines, boolean notify) {
        this.drawPlotCrossingLines.setBoolean(series, drawCrossingLines);
        if (notify) {
            fireChangeEvent();
        }
    }

    public void setDefaultShapeRotate(boolean baseShapeRotate) {
        this.defaultShapeRotate = baseShapeRotate;
    }

    public boolean getDefaultShapeRotate(boolean baseShapeRotate) {
        return this.defaultShapeRotate;
    }

    public void setDefaultBeginShape(Shape baseBeginShape, boolean notify) {
        this.defaultBeginShape = baseBeginShape;
        if (notify) {
            fireChangeEvent();
        }
    }

    public Shape getDefaultBeginShape() {
        return defaultBeginShape;
    }

    public void setSeriesBeginShape(int series, Shape shape, boolean notify) {
        this.beginShapes.setShape(series, shape);
        if (notify) {
            fireChangeEvent();
        }
    }

    public Shape getSeriesBeginShape(int series) {
        Shape shape = this.beginShapes.getShape(series);
        return null == shape ? this.defaultBeginShape : shape;
    }

    public void setSeriesBeginShapeRotate(int series, boolean rotate, boolean notify) {
        this.beginShapesRotate.setBoolean(series, rotate);
        if (notify) {
            fireChangeEvent();
        }
    }

    public void setDefaultBeginShapeRotate(Boolean baseBeginShapeRotate, boolean notify) {
        this.defaultBeginShapeRotate = baseBeginShapeRotate;
        if (notify) {
            fireChangeEvent();
        }
    }

    public Boolean getDefaultBeginShapeRotate() {
        return null == this.defaultBeginShapeRotate ? this.defaultShapeRotate : this.defaultBeginShapeRotate;
    }

    public boolean getSeriesBeginShapeRotate(int series) {
        Boolean rotate = this.beginShapesRotate.getBoolean(series);
        return null == rotate ? getDefaultBeginShapeRotate() : rotate;
    }

    public void setDefaultEndShape(Shape baseEndShape, boolean notify) {
        this.defaultEndShape = baseEndShape;
        if (notify) {
            fireChangeEvent();
        }
    }

    public Shape getDefaultEndShape() {
        return defaultEndShape;
    }

    public void setSeriesEndShape(int series, Shape shape, boolean notify) {
        this.endShapes.setShape(series, shape);
        if (notify) {
            fireChangeEvent();
        }
    }

    public Shape getSeriesEndShape(int series) {
        Shape shape = this.endShapes.getShape(series);
        return null == shape ? this.defaultEndShape : shape;
    }

    public void setDefaultEndShapeRotate(Boolean baseEndShapeRotate, boolean notify) {
        this.defaultEndShapeRotate = baseEndShapeRotate;
        if (notify) {
            fireChangeEvent();
        }
    }

    public Boolean getDefaultEndShapeRotate() {
        return null == this.defaultEndShapeRotate ? this.defaultShapeRotate : this.defaultEndShapeRotate;
    }

    public void setSeriesEndShapeRotate(int series, boolean rotate, boolean notify) {
        this.endShapesRotate.setBoolean(series, rotate);
        if (notify) {
            fireChangeEvent();
        }
    }

    public boolean getSeriesEndShapeRotate(int series) {
        Boolean rotate = this.endShapesRotate.getBoolean(series);
        return null == rotate ? getDefaultEndShapeRotate() : rotate;
    }

    @Override
    public LegendItem getLegendItem(int datasetIndex, int series) {
        LegendItem item = super.getLegendItem(datasetIndex, series);
        item.setShape(lookupLegendShape(series));
        return item;
    }

    @Override
    public Shape lookupSeriesShape(int series) {
        Shape shape = getSeriesEndShape(series);
        if (null == shape) {
            shape = getSeriesBeginShape(series);
        }
        if (null == shape) {
            shape = super.lookupSeriesShape(series);
        }
        return shape;
    }

    @Override
    public XYItemRendererState initialise(Graphics2D g2, Rectangle2D dataArea, XYPlot plot, XYDataset data,
            PlotRenderingInfo info)
    {
        XYItemRendererState state = super.initialise(g2, dataArea, plot, data, info);
        // disable visible items optimization - it doesn't work for this renderer...
        state.setProcessVisibleItemsOnly(false);
        return state;
    }

    @Override
    public void drawItem(Graphics2D g2, XYItemRendererState state, Rectangle2D dataArea, PlotRenderingInfo info,
            XYPlot plot, ValueAxis domainAxis, ValueAxis rangeAxis, XYDataset dataset, int series, int item,
            CrosshairState crosshairState, int pass)
    {
        if (!getItemVisible(series, item) || !(dataset instanceof IntervalXYDataset)) {
            return;
        }

        IntervalXYDataset intervalDataset = (IntervalXYDataset)dataset;
        double x0 = intervalDataset.getStartXValue(series, item);
        double y0 = intervalDataset.getStartYValue(series, item);
        double x1 = intervalDataset.getEndXValue(series, item);
        double y1 = intervalDataset.getEndYValue(series, item);
        // only draw if we have good values
        if (Double.isNaN(x0) || Double.isNaN(y0) || Double.isNaN(x1) || Double.isNaN(y1)) {
            return;
        }

        RectangleEdge xAxisLocation = plot.getDomainAxisEdge();
        RectangleEdge yAxisLocation = plot.getRangeAxisEdge();

        double transX0 = domainAxis.valueToJava2D(x0, dataArea, xAxisLocation);
        double transY0 = rangeAxis.valueToJava2D(y0, dataArea, yAxisLocation);
        double transX1 = domainAxis.valueToJava2D(x1, dataArea, xAxisLocation);
        double transY1 = rangeAxis.valueToJava2D(y1, dataArea, yAxisLocation);

        // only draw if we have good values
        if (Double.isNaN(transX0) || Double.isNaN(transY0) || Double.isNaN(transX1) || Double.isNaN(transY1)) {
            return;
        }

        PlotOrientation orientation = plot.getOrientation();
        if (orientation == PlotOrientation.HORIZONTAL) {
            state.workingLine.setLine(transY0, transX0, transY1, transX1);
        } else if (orientation == PlotOrientation.VERTICAL) {
            state.workingLine.setLine(transX0, transY0, transX1, transY1);
        }
        boolean edgeVisible = drawEdge(g2, state, dataArea, dataset, series, item);
        if (edgeVisible && isItemLabelVisible(series, item)) {
            double centerX = transX0 * 0.5 + transX1 * 0.5;
            double centerY = transY0 * 0.5 + transY1 * 0.5;
            drawItemLabel(g2, orientation, dataset, series, item, centerX, centerY, false);
        }
    }

    protected boolean drawEdge(Graphics2D g2, XYItemRendererState state, Rectangle2D dataArea, XYDataset dataset,
            int series, int item)
    {
        Line2D line = state.workingLine;

        boolean beginVisible = dataArea.contains(line.getP1());
        boolean endVisible = dataArea.contains(line.getP2());
        if (!beginVisible && !endVisible && !getSeriesDrawPlotCrossingLines(series)) {
            return false;
        }

        // Copy the line before clipping as we need it to create begin/end shapes
        Line2D fullLine = (Line2D)line.clone();
        boolean lineVisible = LineUtils.clipLine(line, dataArea);
        if (!lineVisible) {
            return false;
        }

        Stroke itemStroke = getItemStroke(series, item);
        g2.setStroke(itemStroke);
        g2.setPaint(getItemPaint(series, item));
        g2.draw(line);

        Shape beginShape = null;
        if (beginVisible) {
            beginShape = createBeginShape(fullLine, series);
            if (null != beginShape) {
                g2.fill(beginShape);
            }
        }
        Shape endShape = null;
        if (endVisible) {
            endShape = createEndShape(fullLine, series);
            if (null != endShape) {
                g2.fill(endShape);
            }
        }

        EntityCollection entities = state.getEntityCollection();
        if (entities != null) {
            Area hotspot = new Area(ShapeUtils.createLineRegion(line, calculateLineWidth(itemStroke)));
            if (null != beginShape) {
                hotspot.add(new Area(beginShape));
            }
            if (null != endShape) {
                hotspot.add(new Area(endShape));
            }
            hotspot.intersect(new Area(dataArea));
            addEntity(entities, hotspot, dataset, series, item, 0.0, 0.0);
        }
        return true;
    }

    protected Shape createBeginShape(Line2D line, int series) {
        Shape shape = getSeriesBeginShape(series);
        if (null == shape) {
            return null;
        }
        double tx = shape.getBounds2D().getCenterX();
        double ty = shape.getBounds2D().getCenterY();
        AffineTransform aff = new AffineTransform();
        aff.translate(line.getX1() - tx, line.getY1() - ty);
        if (getSeriesBeginShapeRotate(series)) {
            double angle = Math.atan2(line.getY2() - line.getY1(), line.getX2() - line.getX1());
            aff.rotate(angle, tx, ty);
        }
        return aff.createTransformedShape(shape);
    }

    protected Shape createEndShape(Line2D line, int series) {
        Shape shape = getSeriesEndShape(series);
        if (null == shape) {
            return null;
        }
        double tx = shape.getBounds2D().getCenterX();
        double ty = shape.getBounds2D().getCenterY();
        AffineTransform aff = new AffineTransform();
        aff.translate(line.getX2() - tx, line.getY2() - ty);
        if (getSeriesEndShapeRotate(series)) {
            double angle = Math.atan2(line.getY2() - line.getY1(), line.getX2() - line.getX1());
            aff.rotate(angle, tx, ty);
        }
        return aff.createTransformedShape(shape);
    }

    protected float calculateLineWidth(Stroke stroke) {
        return (float)stroke.createStrokedShape(new Line2D.Double(0, 0, 100, 0)).getBounds2D().getHeight();
    }
}
