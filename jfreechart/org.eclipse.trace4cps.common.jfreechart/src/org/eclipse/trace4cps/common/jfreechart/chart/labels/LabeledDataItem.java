/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.chart.labels;

public interface LabeledDataItem {
    /**
     * Returns the label to be used for this data item or {@code null} if no label has been explicitly set. In case of
     * {@code null}, the default label will be used. An {@link String#isEmpty() empty string} should be returned to
     * suppress the label for this data item.
     *
     * @return the label to use for this data item.
     */
    String getItemLabel();
}
