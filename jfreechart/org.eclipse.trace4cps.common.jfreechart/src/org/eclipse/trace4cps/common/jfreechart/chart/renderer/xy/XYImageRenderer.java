/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.chart.renderer.xy;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.EntityCollection;
import org.jfree.chart.plot.CrosshairState;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.AbstractXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRendererState;
import org.jfree.chart.ui.RectangleAnchor;
import org.jfree.chart.ui.RectangleEdge;
import org.jfree.chart.ui.Size2D;
import org.jfree.data.xy.XYDataset;

/**
 * Renders a {@link BufferedImage} at for every item in an {@link XYDataset}.
 */
public class XYImageRenderer extends AbstractXYItemRenderer {
    private static final long serialVersionUID = 3581068448737775534L;

    private BufferedImage defaultImage = null;

    private RectangleAnchor defaultAnchor = RectangleAnchor.CENTER;

    public void setDefaultImage(BufferedImage baseImage) {
        this.defaultImage = baseImage;
    }

    public BufferedImage getDefaultImage() {
        return defaultImage;
    }

    public RectangleAnchor getDefaultAnchor() {
        return defaultAnchor;
    }

    public void setDefaultAnchor(RectangleAnchor baseAnchor) {
        this.defaultAnchor = baseAnchor;
    }

    public BufferedImage getItemImage(int row, int column) {
        return defaultImage;
    }

    public RectangleAnchor getItemAnchor(int row, int column) {
        return defaultAnchor;
    }

    @Override
    public void drawItem(Graphics2D g2, XYItemRendererState state, Rectangle2D dataArea, PlotRenderingInfo info,
            XYPlot plot, ValueAxis domainAxis, ValueAxis rangeAxis, XYDataset dataset, int series, int item,
            CrosshairState crosshairState, int pass)
    {
        // do nothing if item is not visible
        if (!getItemVisible(series, item)) {
            return;
        }

        // get the data point...
        double x = dataset.getXValue(series, item);
        double y = dataset.getYValue(series, item);
        BufferedImage image = getItemImage(series, item);
        RectangleAnchor anchor = getItemAnchor(series, item);
        if (Double.isNaN(x) || Double.isNaN(y) || null == image || null == anchor) {
            return;
        }

        RectangleEdge xAxisLocation = plot.getDomainAxisEdge();
        RectangleEdge yAxisLocation = plot.getRangeAxisEdge();
        int transX = (int)Math.round(domainAxis.valueToJava2D(x, dataArea, xAxisLocation));
        int transY = (int)Math.round(rangeAxis.valueToJava2D(y, dataArea, yAxisLocation));

        Rectangle2D imageRectangle = null;
        PlotOrientation orientation = plot.getOrientation();
        if (orientation == PlotOrientation.HORIZONTAL) {
            imageRectangle = RectangleAnchor.createRectangle(new Size2D(image.getWidth(), image.getHeight()), transY,
                    transX, anchor);
        } else if (orientation == PlotOrientation.VERTICAL) {
            imageRectangle = RectangleAnchor.createRectangle(new Size2D(image.getWidth(), image.getHeight()), transX,
                    transY, anchor);
        }
        if (null != imageRectangle) {
            int imageX = (int)Math.round(imageRectangle.getMinX());
            int imageY = (int)Math.round(imageRectangle.getMinY());
            g2.drawImage(image, null, imageX, imageY);
        }

        int datasetIndex = plot.indexOf(dataset);
        updateCrosshairValues(crosshairState, x, y, datasetIndex, transX, transY, orientation);

        EntityCollection entities = state.getEntityCollection();
        if (entities != null) {
            addEntity(entities, imageRectangle, dataset, series, item, 0.0, 0.0);
        }
    }
}
