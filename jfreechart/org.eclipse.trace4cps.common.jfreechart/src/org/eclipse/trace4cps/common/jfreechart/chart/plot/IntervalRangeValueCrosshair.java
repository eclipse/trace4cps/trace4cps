/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.chart.plot;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.geom.Rectangle2D;
import java.util.function.DoubleFunction;

import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.panel.CrosshairOverlay;
import org.jfree.chart.plot.Crosshair;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.ui.RectangleAnchor;
import org.jfree.chart.ui.RectangleEdge;
import org.jfree.data.xy.IntervalXYDataset;

public class IntervalRangeValueCrosshair extends CrosshairOverlay {
    private static final long serialVersionUID = -5903584362110518312L;

    private final Crosshair crosshair;

    public static IntervalRangeValueCrosshair connectTo(ChartPanel chartPanel) {
        return connectTo(chartPanel, String::valueOf);
    }

    public static IntervalRangeValueCrosshair connectTo(ChartPanel chartPanel, DoubleFunction<String> labelGenerator) {
        return new IntervalRangeValueCrosshair(chartPanel, labelGenerator);
    }

    private IntervalRangeValueCrosshair(ChartPanel chartPanel, DoubleFunction<String> labelGenerator) {
        crosshair = new Crosshair(Double.NaN, Color.DARK_GRAY, new BasicStroke(1.0F));
        crosshair.setLabelVisible(true);
        crosshair.setLabelGenerator(ch -> labelGenerator.apply(ch.getValue()));
        crosshair.setLabelAnchor(RectangleAnchor.TOP_LEFT);
        crosshair.setLabelBackgroundPaint(new Color(192, 192, 192, 192));
        crosshair.setLabelOutlinePaint(Color.DARK_GRAY);
        crosshair.setLabelPaint(Color.BLACK);
        addRangeCrosshair(crosshair);
        chartPanel.addOverlay(this);

        chartPanel.addChartMouseListener(new ChartMouseListener() {
            @Override
            public void chartMouseMoved(ChartMouseEvent event) {
                final Rectangle2D screenDataArea = chartPanel.getScreenDataArea();
                final XYPlot xyPlot = event.getChart().getXYPlot();
                final ValueAxis domainAxis = xyPlot.getDomainAxis();
                final RectangleEdge domainAxisEdge = xyPlot.getDomainAxisEdge();
                final IntervalXYDataset dataset = (IntervalXYDataset)xyPlot.getDataset();
                double x = domainAxis.java2DToValue(event.getTrigger().getX(), screenDataArea, domainAxisEdge);
                double y = Double.NaN;
                if (domainAxis.getRange().contains(x)) {
                    for (int i = 0; i < dataset.getSeriesCount() && Double.isNaN(y); i++) {
                        y = findYValue(dataset, i, x);
                    }
                }
                crosshair.setVisible(!Double.isNaN(y));
                crosshair.setValue(y);
            }

            @Override
            public void chartMouseClicked(ChartMouseEvent arg0) {
                // Empty
            }
        });
    }

    public Crosshair getCrosshair() {
        return crosshair;
    }

    private double findYValue(IntervalXYDataset dataset, int series, double x) {
        for (int item = 0; item < dataset.getItemCount(series); item++) {
            double startX = dataset.getStartXValue(series, item);
            double endX = dataset.getEndXValue(series, item);
            if (x >= startX && x <= endX) {
                return dataset.getYValue(series, item);
            }
        }
        return Double.NaN;
    }
}
