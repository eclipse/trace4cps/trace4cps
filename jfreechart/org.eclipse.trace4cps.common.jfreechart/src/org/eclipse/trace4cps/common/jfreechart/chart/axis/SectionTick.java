/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.chart.axis;

import org.jfree.chart.axis.TickType;
import org.jfree.chart.axis.ValueTick;
import org.jfree.chart.ui.TextAnchor;

/**
 * A labeled tick with the option for tooltip text.
 */
public class SectionTick extends ValueTick {
    private static final long serialVersionUID = 4133486505758570577L;

    private String tooltipText;

    /**
     * Creates a new tick.
     *
     * @param tickType the tick type.
     * @param value the value.
     * @param label the label.
     * @param textAnchor the part of the label that is aligned with the anchor point.
     * @param rotationAnchor defines the rotation point relative to the text.
     * @param angle the rotation angle (in radians).
     */
    public SectionTick(TickType tickType, double value, String label, TextAnchor textAnchor, TextAnchor rotationAnchor,
            double angle)
    {
        super(tickType, value, label, textAnchor, rotationAnchor, angle);
    }

    /**
     * Returns the tool tip text for the tick. This will be displayed in a {@link org.jfree.chart.ChartPanel} when the
     * mouse pointer hovers over the tick.
     *
     * @return The tool tip text (possibly {@code null}).
     *
     * @see #setToolTipText(String)
     */
    public String getTooltipText() {
        return tooltipText;
    }

    /**
     * Sets the tool tip text for the tick.
     *
     * @param text the tool tip text ({@code null} permitted).
     *
     * @see #getToolTipText()
     */
    public void setTooltipText(String tooltipText) {
        this.tooltipText = tooltipText;
    }
}
