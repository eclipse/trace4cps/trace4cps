/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.chart.axis;

import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.ValueAxisPlot;
import org.jfree.data.Range;
import org.jfree.data.RangeType;

public class BoundedNumberAxis extends NumberAxis {
    private static final long serialVersionUID = 4541966206816909874L;

    private double lowerBound = Double.NEGATIVE_INFINITY;

    private boolean autoRangeIncludesLowerBound;

    private double upperBound = Double.POSITIVE_INFINITY;

    private boolean autoRangeIncludesUpperBound;

    public BoundedNumberAxis() {
        super();
    }

    public BoundedNumberAxis(String label) {
        super(label);
    }

    @Override
    public void setLowerBound(double min) {
        if (min > this.upperBound) {
            this.upperBound = min + 1.0;
        }
        this.lowerBound = min;
        if (isAutoRange()) {
            autoAdjustRange();
        } else {
            setRange(this.lowerBound, this.upperBound);
        }
    }

    @Override
    public double getLowerBound() {
        return this.lowerBound;
    }

    public void setAutoRangeIncludesLowerBound(boolean autoRangeIncludesLowerBound) {
        this.autoRangeIncludesLowerBound = autoRangeIncludesLowerBound;
        fireChangeEvent();
    }

    public boolean getAutoRangeIncludesLowerBound() {
        return autoRangeIncludesLowerBound;
    }

    @Override
    public void setUpperBound(double max) {
        if (max < this.lowerBound) {
            this.lowerBound = max - 1.0;
        }
        this.upperBound = max;
        if (isAutoRange()) {
            autoAdjustRange();
        } else {
            setRange(this.lowerBound, this.upperBound);
        }
    }

    @Override
    public double getUpperBound() {
        return this.upperBound;
    }

    public void setAutoRangeIncludesUpperBound(boolean autoRangeIncludesUpperBound) {
        this.autoRangeIncludesUpperBound = autoRangeIncludesUpperBound;
        fireChangeEvent();
    }

    public boolean getAutoRangeIncludesUpperBound() {
        return autoRangeIncludesUpperBound;
    }

    @Override
    public Range calculateAutoRange(boolean adhereToMax) {
        Plot plot = getPlot();
        if (plot == null) {
            return null; // no plot, no data
        }

        if (plot instanceof ValueAxisPlot) {
            ValueAxisPlot vap = (ValueAxisPlot)plot;

            Range r = vap.getDataRange(this);
            if (r == null) {
                r = getDefaultAutoRange();
            }

            double upper = r.getUpperBound();
            double lower = r.getLowerBound();
            if (getRangeType() == RangeType.POSITIVE) {
                lower = Math.max(0.0, lower);
                upper = Math.max(0.0, upper);
            } else if (getRangeType() == RangeType.NEGATIVE) {
                lower = Math.min(0.0, lower);
                upper = Math.min(0.0, upper);
            }

            if (getAutoRangeIncludesZero()) {
                lower = Math.min(lower, 0.0);
                upper = Math.max(upper, 0.0);
            }
            if (getAutoRangeIncludesLowerBound()) {
                lower = Math.min(lower, getLowerBound());
                upper = Math.max(upper, getLowerBound());
            }
            if (getAutoRangeIncludesUpperBound()) {
                lower = Math.min(lower, getUpperBound());
                upper = Math.max(upper, getUpperBound());
            }
            double range = upper - lower;

            // if fixed auto range, then derive lower bound...
            double fixedAutoRange = getFixedAutoRange();
            if (adhereToMax && fixedAutoRange > 0.0) {
                Range aligned = getAutoRangeAlign().align(new Range(lower, upper), fixedAutoRange);
                lower = aligned.getLowerBound();
                upper = aligned.getUpperBound();
            } else {
                // ensure the autorange is at least <minRange> in size...
                double minRange = getAutoRangeMinimumSize();
                if (range < minRange) {
                    double expand = (minRange - range) / 2;
                    upper = upper + expand;
                    lower = lower - expand;
                    if (lower == upper) { // see bug report 1549218
                        double adjust = Math.abs(lower) / 10.0;
                        lower = lower - adjust;
                        upper = upper + adjust;
                    }
                    if (getRangeType() == RangeType.POSITIVE) {
                        if (lower < 0.0) {
                            upper = upper - lower;
                            lower = 0.0;
                        }
                    } else if (getRangeType() == RangeType.NEGATIVE) {
                        if (upper > 0.0) {
                            lower = lower - upper;
                            upper = 0.0;
                        }
                    }
                }

                if (getAutoRangeStickyZero()) {
                    if (upper <= 0.0) {
                        upper = Math.min(0.0, upper + getUpperMargin() * range);
                    } else {
                        upper = upper + getUpperMargin() * range;
                    }
                    if (lower >= 0.0) {
                        lower = Math.max(0.0, lower - getLowerMargin() * range);
                    } else {
                        lower = lower - getLowerMargin() * range;
                    }
                } else {
                    upper = upper + getUpperMargin() * range;
                    lower = lower - getLowerMargin() * range;
                }
            }

            return new Range(lower, upper);
        }
        return null;
    }
}
