/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.chart.labels;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;

import org.jfree.chart.labels.AbstractXYItemLabelGenerator;
import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.chart.util.PublicCloneable;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYDataset;

/**
 * Formats tooltip according to <code>formatString</code> with following arguments:<br>
 * <code>
 * Object[] result = new Object[7];<br>
 * result[0] = String.valueOf(dataset.getSeriesKey(series));<br>
 * result[1] = formatXValue(intervalXYDataset.getX(series, item));<br>
 * result[2] = formatYValue(intervalXYDataset.getY(series, item));<br>
 * result[3] = formatXValue(intervalXYDataset.getStartX(series, item));<br>
 * result[4] = formatYValue(intervalXYDataset.getStartY(series, item));<br>
 * result[5] = formatXValue(intervalXYDataset.getEndX(series, item));<br>
 * result[6] = formatYValue(intervalXYDataset.getEndY(series, item));<br>
 * </code>
 *
 * @see MessageFormat
 */
public class StandardIntervalXYToolTipGenerator extends AbstractXYItemLabelGenerator
        implements XYToolTipGenerator, Cloneable, PublicCloneable, Serializable
{
    private static final long serialVersionUID = -2173467562605711571L;

    /** The default tooltip format. */
    public static final String DEFAULT_TOOL_TIP_FORMAT = "{0}: ({3} - {5}, {2})";

    public StandardIntervalXYToolTipGenerator() {
        this(DEFAULT_TOOL_TIP_FORMAT, NumberFormat.getNumberInstance(), NumberFormat.getNumberInstance());
    }

    public StandardIntervalXYToolTipGenerator(String formatString, NumberFormat xFormat, NumberFormat yFormat) {
        super(formatString, xFormat, yFormat);
    }

    public StandardIntervalXYToolTipGenerator(String formatString, DateFormat xFormat, NumberFormat yFormat) {
        super(formatString, xFormat, yFormat);
    }

    public StandardIntervalXYToolTipGenerator(String formatString, NumberFormat xFormat, DateFormat yFormat) {
        super(formatString, xFormat, yFormat);
    }

    public StandardIntervalXYToolTipGenerator(String formatString, DateFormat xFormat, DateFormat yFormat) {
        super(formatString, xFormat, yFormat);
    }

    @Override
    public String generateToolTip(XYDataset dataset, int series, int item) {
        return generateLabelString(dataset, series, item);
    }

    @Override
    protected Object[] createItemArray(XYDataset dataset, int series, int item) {
        if (dataset instanceof IntervalXYDataset) {
            IntervalXYDataset intervalXYDataset = (IntervalXYDataset)dataset;
            Object[] result = new Object[7];
            result[0] = String.valueOf(dataset.getSeriesKey(series));
            result[1] = formatXValue(intervalXYDataset.getX(series, item));
            result[2] = formatYValue(intervalXYDataset.getY(series, item));
            result[3] = formatXValue(intervalXYDataset.getStartX(series, item));
            result[4] = formatYValue(intervalXYDataset.getStartY(series, item));
            result[5] = formatXValue(intervalXYDataset.getEndX(series, item));
            result[6] = formatYValue(intervalXYDataset.getEndY(series, item));
            return result;
        } else {
            return super.createItemArray(dataset, series, item);
        }
    }

    protected String formatXValue(Number xValue) {
        if (null != getXDateFormat()) {
            return getXDateFormat().format(xValue);
        } else {
            return getXFormat().format(xValue);
        }
    }

    protected String formatYValue(Number yValue) {
        if (null == yValue || Double.isNaN(yValue.doubleValue())) {
            return getNullYString();
        } else if (null != getYDateFormat()) {
            return getYDateFormat().format(yValue);
        } else {
            return getYFormat().format(yValue);
        }
    }

    /**
     * Tests this object for equality with an arbitrary object.
     *
     * @param obj the other object (<code>null</code> permitted).
     *
     * @return A boolean.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof StandardIntervalXYToolTipGenerator)) {
            return false;
        }
        return super.equals(obj);
    }
}
