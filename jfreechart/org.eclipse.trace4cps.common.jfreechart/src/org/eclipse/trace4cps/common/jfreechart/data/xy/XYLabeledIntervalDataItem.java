/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.data.xy;

import org.eclipse.trace4cps.common.jfreechart.chart.labels.LabeledDataItem;
import org.jfree.data.xy.XYIntervalDataItem;

public class XYLabeledIntervalDataItem extends XYIntervalDataItem implements LabeledDataItem {
    private static final long serialVersionUID = 2278894211820843418L;

    private String itemLabel;

    /**
     * Creates a new instance of {@code XYLabeledIntervalDataItem}.
     *
     * @param x the x-value.
     * @param xLow the lower bound of the x-interval.
     * @param xHigh the upper bound of the x-interval.
     * @param y the y-value.
     * @param yLow the lower bound of the y-interval.
     * @param yHigh the upper bound of the y-interval.
     */
    public XYLabeledIntervalDataItem(double x, double xLow, double xHigh, double y, double yLow, double yHigh) {
        super(x, xLow, xHigh, y, yLow, yHigh);
    }

    /**
     * Sets the label to be used for this data item or null if no label has been explicitly set. In case of null, the
     * default label will be used. An empty string should be set to suppress the label for this data item.
     *
     * @param itemLabel The itemLabel to set.
     */
    public void setItemLabel(String itemLabel) {
        this.itemLabel = itemLabel;
    }

    @Override
    public String getItemLabel() {
        return itemLabel;
    }
}
