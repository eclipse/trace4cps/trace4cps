/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.chart.labels;

import java.util.function.Function;

import org.eclipse.trace4cps.common.jfreechart.data.xy.XYDataItemResolver;
import org.jfree.chart.labels.XYItemLabelGenerator;
import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.data.xy.XYDataset;

public class XYDataItemLabelGenerator implements XYItemLabelGenerator, XYToolTipGenerator {
    public static final XYDataItemLabelGenerator DEFAULT = new XYDataItemLabelGenerator(
            XYDataItemLabelGenerator::toString, XYDataItemResolver.DEFAULT);

    private final Function<Object, String> labelSupplier;

    private final XYDataItemResolver xyDataItemResolver;

    public XYDataItemLabelGenerator(Function<Object, String> labelSupplier) {
        this(labelSupplier, XYDataItemResolver.DEFAULT);
    }

    public XYDataItemLabelGenerator(Function<Object, String> labelSupplier, XYDataItemResolver xyDataItemResolver) {
        this.labelSupplier = labelSupplier;
        this.xyDataItemResolver = xyDataItemResolver;
    }

    @Override
    public String generateLabel(XYDataset dataset, int series, int item) {
        return nullIfEmpty(generateLabel(xyDataItemResolver.resolveDataItem(dataset, series, item)));
    }

    @Override
    public String generateToolTip(XYDataset dataset, int series, int item) {
        return nullIfEmpty(generateLabel(xyDataItemResolver.resolveDataItem(dataset, series, item)));
    }

    protected String generateLabel(Object dataItem) {
        if (dataItem == null) {
            return null;
        }
        if (dataItem instanceof LabeledDataItem) {
            String itemLabel = ((LabeledDataItem)dataItem).getItemLabel();
            if (itemLabel != null) {
                return itemLabel;
            }
        }
        return labelSupplier.apply(dataItem);
    }

    /**
     * Returns the {@link Object#toString() string} representation of {@code o} if not {@code null}, {@code null}
     * otherwise.
     *
     * @param o the object
     * @return the {@link Object#toString() string} representation of {@code o} if not {@code null}, {@code null}
     *     otherwise.
     */
    public static String toString(Object o) {
        return o == null ? null : o.toString();
    }

    /**
     * Returns string {@code s} if not {@code null} and not {@link String#isEmpty() empty}, {@code null} otherwise.
     *
     * @param s the string
     * @return string {@code s} if not {@code null} and not {@link String#isEmpty() empty}, {@code null} otherwise.
     */
    public static String nullIfEmpty(String s) {
        return s == null || s.isEmpty() ? null : s;
    }
}
