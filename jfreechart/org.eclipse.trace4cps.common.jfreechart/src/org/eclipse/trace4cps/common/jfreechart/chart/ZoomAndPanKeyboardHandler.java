/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.chart;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.plot.Pannable;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.Zoomable;

/**
 * This handler enables panning and zooming via the keyboard.
 */
public class ZoomAndPanKeyboardHandler extends KeyAdapter {
    @Override
    public void keyPressed(KeyEvent event) {
        Object source = event.getSource();
        if (!(source instanceof ChartPanel)) {
            return;
        }
        ChartPanel chartPanel = (ChartPanel)source;

        final double centerX = chartPanel.getBounds().getCenterX();
        final double centerY = chartPanel.getBounds().getCenterY();
        final Plot plot = chartPanel.getChart().getPlot();
        final Pannable pannable = plot instanceof Pannable ? (Pannable)plot : null;
        final Zoomable zoomable = plot instanceof Zoomable ? (Zoomable)plot : null;

        switch (event.getKeyCode()) {
            case KeyEvent.VK_KP_UP:
            case KeyEvent.VK_NUMPAD8:
            case KeyEvent.VK_UP: {
                if (event.isControlDown() && null != zoomable) {
                    if (zoomable.getOrientation() == PlotOrientation.VERTICAL && zoomable.isRangeZoomable()) {
                        chartPanel.zoomInRange(centerX, centerY);
                        event.consume();
                    } else if (zoomable.isDomainZoomable()) {
                        chartPanel.zoomInDomain(centerX, centerY);
                        event.consume();
                    }
                } else if (null != pannable) {
                    if (pannable.getOrientation() == PlotOrientation.VERTICAL && pannable.isRangePannable()) {
                        chartPanel.panRange(chartPanel.getDefaultPanFactor(), centerX, centerY);
                        event.consume();
                    } else if (pannable.isDomainPannable()) {
                        chartPanel.panDomain(chartPanel.getDefaultPanFactor(), centerX, centerY);
                        event.consume();
                    }
                }
                break;
            }
            case KeyEvent.VK_KP_RIGHT:
            case KeyEvent.VK_NUMPAD6:
            case KeyEvent.VK_RIGHT: {
                if (event.isControlDown() && null != zoomable) {
                    if (zoomable.getOrientation() == PlotOrientation.HORIZONTAL && zoomable.isRangeZoomable()) {
                        chartPanel.zoomInRange(centerX, centerY);
                        event.consume();
                    } else if (zoomable.isDomainZoomable()) {
                        chartPanel.zoomInDomain(centerX, centerY);
                        event.consume();
                    }
                } else if (null != pannable) {
                    if (pannable.getOrientation() == PlotOrientation.HORIZONTAL && pannable.isRangePannable()) {
                        chartPanel.panRange(chartPanel.getDefaultPanFactor(), centerX, centerY);
                        event.consume();
                    } else if (pannable.isDomainPannable()) {
                        chartPanel.panDomain(chartPanel.getDefaultPanFactor(), centerX, centerY);
                        event.consume();
                    }
                }
                break;
            }
            case KeyEvent.VK_KP_DOWN:
            case KeyEvent.VK_NUMPAD2:
            case KeyEvent.VK_DOWN: {
                if (event.isControlDown() && null != zoomable) {
                    if (zoomable.getOrientation() == PlotOrientation.VERTICAL && zoomable.isRangeZoomable()) {
                        chartPanel.zoomOutRange(centerX, centerY);
                        event.consume();
                    } else if (zoomable.isDomainZoomable()) {
                        chartPanel.zoomOutDomain(centerX, centerY);
                        event.consume();
                    }
                } else if (null != pannable) {
                    if (pannable.getOrientation() == PlotOrientation.VERTICAL && pannable.isRangePannable()) {
                        chartPanel.panRange(-chartPanel.getDefaultPanFactor(), centerX, centerY);
                        event.consume();
                    } else if (pannable.isDomainPannable()) {
                        chartPanel.panDomain(-chartPanel.getDefaultPanFactor(), centerX, centerY);
                        event.consume();
                    }
                }
                break;
            }
            case KeyEvent.VK_KP_LEFT:
            case KeyEvent.VK_NUMPAD4:
            case KeyEvent.VK_LEFT: {
                if (event.isControlDown() && null != zoomable) {
                    if (zoomable.getOrientation() == PlotOrientation.HORIZONTAL && zoomable.isRangeZoomable()) {
                        chartPanel.zoomOutRange(centerX, centerY);
                        event.consume();
                    } else if (zoomable.isDomainZoomable()) {
                        chartPanel.zoomOutDomain(centerX, centerY);
                        event.consume();
                    }
                } else if (null != pannable) {
                    if (pannable.getOrientation() == PlotOrientation.HORIZONTAL && pannable.isRangePannable()) {
                        chartPanel.panRange(-chartPanel.getDefaultPanFactor(), centerX, centerY);
                        event.consume();
                    } else if (pannable.isDomainPannable()) {
                        chartPanel.panDomain(-chartPanel.getDefaultPanFactor(), centerX, centerY);
                        event.consume();
                    }
                }
                break;
            }
            // Also add browser style zooming
            case KeyEvent.VK_ADD:
            case KeyEvent.VK_EQUALS: {
                if (event.isControlDown()) {
                    chartPanel.zoomInBoth(centerX, centerY);
                    event.consume();
                }
                break;
            }
            case KeyEvent.VK_SUBTRACT:
            case KeyEvent.VK_MINUS: {
                if (event.isControlDown()) {
                    chartPanel.zoomOutBoth(centerX, centerY);
                    event.consume();
                }
                break;
            }
            case KeyEvent.VK_NUMPAD0: {
                if (event.isControlDown()) {
                    chartPanel.restoreAutoBounds();
                    event.consume();
                }
                break;
            }
            default:
                // Empty
                break;
        }
    }
}
