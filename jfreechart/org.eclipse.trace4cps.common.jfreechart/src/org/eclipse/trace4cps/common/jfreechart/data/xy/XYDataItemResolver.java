/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.data.xy;

import java.util.Objects;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.TimePeriodValuesCollection;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.time.ohlc.OHLCSeriesCollection;
import org.jfree.data.xy.VectorSeriesCollection;
import org.jfree.data.xy.XIntervalSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYIntervalSeriesCollection;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.data.xy.YIntervalSeriesCollection;

/**
 * Utility class to resolve data items from an {@link XYDataset}. {@link Object}s are returned as there is no common
 * interface defined for data items.
 */
public class XYDataItemResolver {
    public static final XYDataItemResolver DEFAULT = new XYDataItemResolver();

    /**
     * Resolves {@link #resolveAllDataItems(XYDataset) all data items} from {@link XYPlot#getDatasets() all data sets}
     * of the specified {@code plot}.
     */
    public Stream<Object> resolveAllDataItems(XYPlot plot) {
        return plot.getDatasets().values().stream().flatMap(this::resolveAllDataItems);
    }

    /**
     * Resolves all {@link XYDataItemResolver#resolveDataItem(XYDataset, int, int) data items} from the specified
     * {@code dataset}.
     */
    public Stream<Object> resolveAllDataItems(XYDataset dataset) {
        Stream<Integer> seriesStream = IntStream.range(0, dataset.getSeriesCount()).boxed();
        return seriesStream.flatMap(series -> {
            IntStream itemStream = IntStream.range(0, dataset.getItemCount(series));
            return itemStream.mapToObj(item -> resolveDataItem(dataset, series, item)).filter(Objects::nonNull);
        });
    }

    public Object resolveDataItem(XYDataset dataset, int series, int item) {
        if (series >= dataset.getSeriesCount() || item >= dataset.getItemCount(series)) {
            // Avoiding java.lang.IndexOutOfBoundsException
            return null;
        }
        if (dataset instanceof XYIntervalSeriesCollection) {
            return ((XYIntervalSeriesCollection)dataset).getSeries(series).getDataItem(item);
        }
        if (dataset instanceof XYSeriesCollection) {
            return ((XYSeriesCollection)dataset).getSeries(series).getDataItem(item);
        }
        if (dataset instanceof XYEdgeSeriesCollection) {
            return ((XYEdgeSeriesCollection)dataset).getSeries(series).getDataItem(item);
        }
        if (dataset instanceof XIntervalSeriesCollection) {
            return ((XIntervalSeriesCollection)dataset).getSeries(series).getDataItem(item);
        }
        if (dataset instanceof YIntervalSeriesCollection) {
            return ((YIntervalSeriesCollection)dataset).getSeries(series).getDataItem(item);
        }
        if (dataset instanceof OHLCSeriesCollection) {
            return ((OHLCSeriesCollection)dataset).getSeries(series).getDataItem(item);
        }
        if (dataset instanceof TimePeriodValuesCollection) {
            return ((TimePeriodValuesCollection)dataset).getSeries(series).getDataItem(item);
        }
        if (dataset instanceof TimeSeriesCollection) {
            return ((TimeSeriesCollection)dataset).getSeries(series).getDataItem(item);
        }
        if (dataset instanceof VectorSeriesCollection) {
            return ((VectorSeriesCollection)dataset).getSeries(series).getDataItem(item);
        }
        return null;
    }
}
