/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.data.xy;

import org.eclipse.trace4cps.common.jfreechart.chart.labels.LabeledDataItem;
import org.jfree.data.xy.XYDataItem;

public class XYLabeledDataItem extends XYDataItem implements LabeledDataItem {
    private static final long serialVersionUID = 2278894211820843418L;

    private String itemLabel;

    /**
     * Constructs a new data item.
     *
     * @param x the x-value.
     * @param y the y-value.
     */
    public XYLabeledDataItem(double x, double y) {
        super(x, y);
    }

    /**
     * Constructs a new data item.
     *
     * @param x the x-value ({@code null} NOT permitted).
     * @param y the y-value ({@code null} permitted).
     */
    public XYLabeledDataItem(Number x, Number y) {
        super(x, y);
    }

    /**
     * Sets the label to be used for this data item or null if no label has been explicitly set. In case of null, the
     * default label will be used. An empty string should be set to suppress the label for this data item.
     *
     * @param itemLabel The itemLabel to set.
     */
    public void setItemLabel(String itemLabel) {
        this.itemLabel = itemLabel;
    }

    @Override
    public String getItemLabel() {
        return itemLabel;
    }
}
