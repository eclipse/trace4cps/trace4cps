/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.data.xy;

import java.io.Serializable;

import org.eclipse.trace4cps.common.jfreechart.chart.labels.LabeledDataItem;
import org.jfree.chart.util.Args;

/**
 * An item representing data as two points in the form (x0, y0 and x1, y1).
 */
public class XYEdgeDataItem implements LabeledDataItem, Serializable {
    private static final long serialVersionUID = -7116745427210808035L;

    private final Number x0;

    private final Number y0;

    private final Number x1;

    private final Number y1;

    private String itemLabel;

    public XYEdgeDataItem(Number x0, Number y0, Number x1, Number y1) {
        Args.nullNotPermitted(x0, "x0");
        Args.nullNotPermitted(x1, "x1");
        Args.nullNotPermitted(y0, "y0");
        Args.nullNotPermitted(y1, "y1");
        this.x0 = x0;
        this.y0 = y0;
        this.x1 = x1;
        this.y1 = y1;
    }

    public boolean isNaN() {
        return Double.isNaN(getX0Value()) || Double.isNaN(getX1Value()) || Double.isNaN(getY0Value())
                || Double.isNaN(getY1Value());
    }

    public Number getX0() {
        return x0;
    }

    public Number getY0() {
        return y0;
    }

    public Number getX1() {
        return x1;
    }

    public Number getY1() {
        return y1;
    }

    public double getX0Value() {
        return x0.doubleValue();
    }

    public double getY0Value() {
        return y0.doubleValue();
    }

    public double getX1Value() {
        return x1.doubleValue();
    }

    public double getY1Value() {
        return y1.doubleValue();
    }

    /**
     * Sets the label to be used for this data item or null if no label has been explicitly set. In case of null, the
     * default label will be used. An empty string should be set to suppress the label for this data item.
     *
     * @param itemLabel The itemLabel to set.
     */
    public void setItemLabel(String itemLabel) {
        this.itemLabel = itemLabel;
    }

    @Override
    public String getItemLabel() {
        return itemLabel;
    }
}
