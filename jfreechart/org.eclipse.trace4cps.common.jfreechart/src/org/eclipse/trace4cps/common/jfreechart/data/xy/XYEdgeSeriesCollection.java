/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.data.xy;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.io.Serializable;
import java.util.ArrayList;

import org.jfree.chart.util.Args;
import org.jfree.chart.util.PublicCloneable;
import org.jfree.data.general.DatasetChangeEvent;
import org.jfree.data.xy.AbstractIntervalXYDataset;
import org.jfree.data.xy.IntervalXYDataset;

/**
 * Represents a collection of {@link XYEdgeSeries} objects that can be used as a dataset.
 */
public class XYEdgeSeriesCollection extends AbstractIntervalXYDataset
        implements IntervalXYDataset, VetoableChangeListener, PublicCloneable, Serializable
{
    private static final long serialVersionUID = -371610910545736704L;

    private final ArrayList<XYEdgeSeries> data = new ArrayList<>();

    public XYEdgeSeriesCollection() {
        // Empty
    }

    public XYEdgeSeriesCollection(XYEdgeSeries series) {
        addSeries(series);
    }

    /**
     * Adds a series to the collection and sends a {@link DatasetChangeEvent} to all registered listeners.
     *
     * @param series the series ({@code null} not permitted).
     *
     * @throws IllegalArgumentException if the key for the series is null or not unique within the dataset.
     */
    public void addSeries(XYEdgeSeries series) {
        Args.nullNotPermitted(series, "series");
//        if (getSeriesIndex(series.getKey()) >= 0) {
//            throw new IllegalArgumentException(
//                    "This dataset already contains a series with the key " + series.getKey());
//        }
        this.data.add(series);
        series.addChangeListener(this);
        series.addVetoableChangeListener(this);
        fireDatasetChanged();
    }

    public void removeAllSeries() {
        data.forEach(s -> s.removeChangeListener(this));
        data.clear();
        fireDatasetChanged();
    }

    @Override
    public int getSeriesCount() {
        return data.size();
    }

    public XYEdgeSeries getSeries(int series) {
        if ((series < 0) || (series >= getSeriesCount())) {
            throw new IllegalArgumentException("Series index out of bounds");
        }
        return data.get(series);
    }

    /**
     * Returns the index of the series with the specified key, or -1 if no series has that key.
     *
     * @param key the key ({@code null} not permitted).
     *
     * @return The index.
     */
    public int getSeriesIndex(Comparable<?> key) {
        Args.nullNotPermitted(key, "key");
        int seriesCount = getSeriesCount();
        for (int i = 0; i < seriesCount; i++) {
            XYEdgeSeries series = this.data.get(i);
            if (key.equals(series.getKey())) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public Comparable<?> getSeriesKey(int series) {
        return getSeries(series).getKey();
    }

    @Override
    public int getItemCount(int series) {
        return getSeries(series).getItemCount();
    }

    @Override
    public Number getX(int series, int item) {
        return getSeries(series).getX(item);
    }

    @Override
    public Number getY(int series, int item) {
        return getSeries(series).getY(item);
    }

    @Override
    public Number getStartX(int series, int item) {
        return getSeries(series).getStartX(item);
    }

    @Override
    public Number getEndX(int series, int item) {
        return getSeries(series).getEndX(item);
    }

    @Override
    public Number getStartY(int series, int item) {
        return getSeries(series).getStartY(item);
    }

    @Override
    public Number getEndY(int series, int item) {
        return getSeries(series).getEndY(item);
    }

    @Override
    public void vetoableChange(PropertyChangeEvent e) throws PropertyVetoException {
        // if it is not the series name, then we have no interest
        if (!"Key".equals(e.getPropertyName())) {
            return;
        }

        // to be defensive, let's check that the source series does in fact
        // belong to this collection
        XYEdgeSeries s = (XYEdgeSeries)e.getSource();
        if (getSeriesIndex(s.getKey()) == -1) {
            throw new IllegalStateException(
                    "Receiving events from a series " + "that does not belong to this collection.");
        }
        // check if the new series name already exists for another series
        Comparable<?> key = (Comparable<?>)e.getNewValue();
        if (getSeriesIndex(key) >= 0) {
            throw new PropertyVetoException("Duplicate key2", e);
        }
    }
}
