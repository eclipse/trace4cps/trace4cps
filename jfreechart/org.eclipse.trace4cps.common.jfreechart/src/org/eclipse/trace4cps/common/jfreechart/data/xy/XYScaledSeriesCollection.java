/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.data.xy;

import java.util.List;

import org.eclipse.trace4cps.common.jfreechart.chart.axis.Section;
import org.eclipse.trace4cps.common.jfreechart.chart.axis.SectionAxis;
import org.jfree.data.general.DatasetChangeEvent;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 * A special {@link XYSeriesCollection} that allows to scale its values to fit a certain range. This dataset can be used
 * to bound values to a specific {@link Section} on a {@link SectionAxis}.
 *
 * @see SectionAxis
 * @see XYScaledSeries
 */
public class XYScaledSeriesCollection extends XYSeriesCollection {
    private static final long serialVersionUID = 9200187261351790160L;

    public XYScaledSeriesCollection() {
        super();
    }

    public XYScaledSeriesCollection(XYScaledSeries series) {
        super(series);
    }

    /**
     * @throws ClassCastException if {@code series} is not instance of {@link XYScaledSeries}
     * @deprecated use {@link #addSeries(XYScaledSeries)}
     */
    @Override
    @Deprecated
    public void addSeries(XYSeries series) throws ClassCastException {
        super.addSeries(XYScaledSeries.class.cast(series));
    }

    /**
     * Adds a series to the collection and sends a {@link DatasetChangeEvent} to all registered listeners.
     *
     * @param series the series ({@code null} not permitted).
     *
     * @throws IllegalArgumentException if the key for the series is null or not unique within the dataset.
     * @see XYSeriesCollection#addSeries(XYSeries)
     */
    public void addSeries(XYScaledSeries series) {
        super.addSeries(series);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<XYScaledSeries> getSeries() {
        return super.getSeries();
    }

    @Override
    public XYScaledSeries getSeries(int series) {
        return (XYScaledSeries)super.getSeries(series);
    }

    @Override
    @SuppressWarnings("rawtypes")
    public XYScaledSeries getSeries(Comparable key) {
        return (XYScaledSeries)super.getSeries(key);
    }

    /**
     * Returns the unscaled (a.k.a. original) x-value for the specified series and item.
     *
     * @param series the series (zero-based index).
     * @param item the item (zero-based index).
     *
     * @return The unscaled (a.k.a. original) value.
     */
    public Number getUnscaledX(int series, int item) {
        return getSeries(series).getUnscaledX(item);
    }

    /**
     * Returns the unscaled (a.k.a. original) y-value for the specified series and item.
     *
     * @param series the series (zero-based index).
     * @param item the item (zero-based).
     *
     * @return The unscaled (a.k.a. original) value (possibly {@code null}).
     */
    public Number getUnscaledY(int series, int item) {
        return getSeries(series).getUnscaledY(item);
    }
}
