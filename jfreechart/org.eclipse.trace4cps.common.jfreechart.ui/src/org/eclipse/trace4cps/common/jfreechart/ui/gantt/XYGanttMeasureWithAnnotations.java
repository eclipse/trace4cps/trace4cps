/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.ui.gantt;

import java.awt.event.MouseEvent;

import org.eclipse.trace4cps.common.jfreechart.ui.gantt.XYGanttMeasurement.Snap;
import org.eclipse.trace4cps.common.jfreechart.ui.viewers.BackReferenceProvider;
import org.eclipse.trace4cps.common.jfreechart.ui.viewers.BackReferenceResolver;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.annotations.XYAnnotation;
import org.jfree.chart.annotations.XYMeasurementAnnotation;
import org.jfree.chart.annotations.XYMeasurementAnnotation.Orientation;
import org.jfree.chart.geom.Point2DNumber;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYMeasureWithAnnotations;
import org.jfree.chart.plot.XYPlot;

public class XYGanttMeasureWithAnnotations extends XYMeasureWithAnnotations {
    private static final long serialVersionUID = -6750408735004577024L;

    private final MeasurementAxis measurementAxis;

    private BackReferenceResolver backReferenceResolver = BackReferenceResolver.DEFAULT;

    /**
     * Registers this utility to a {@link ChartPanel chartPanel} for a specific {@link MeasurementAxis axis}.
     *
     * @param chartPanel the chart panel to register to.
     * @param axis the axis to measure.
     * @return the created {@link XYGanttMeasureWithAnnotations measure utility} that was registered to
     *     {@code chartPanel}
     */
    public static XYGanttMeasureWithAnnotations addToChartPanel(ChartPanel chartPanel, MeasurementAxis axis) {
        XYGanttMeasureWithAnnotations instance = new XYGanttMeasureWithAnnotations(axis);
        instance.addToChartPanel(chartPanel);
        return instance;
    }

    public XYGanttMeasureWithAnnotations(MeasurementAxis measurementAxis) {
        super(measurementAxis);
        this.measurementAxis = measurementAxis;
    }

    protected MeasurementAxis getMeasurementAxis() {
        return measurementAxis;
    }

    public BackReferenceResolver getBackReferenceResolver() {
        return backReferenceResolver;
    }

    public void setBackReferenceResolver(BackReferenceResolver backReferenceResolver) {
        this.backReferenceResolver = backReferenceResolver;
    }

    @Override
    protected Point2DNumber getCrosshairPoint(ChartMouseEvent event) {
        BackReferenceProvider<?> backReferenceProvider = backReferenceResolver.resolveBackReferenceProvider(event);
        if (backReferenceProvider instanceof XYGanttDataItem<?>) {
            return new XYGanttPoint((XYGanttDataItem<?>)backReferenceProvider, calculateCrosshairPoint(event));
        }
        return null;
    }

    /**
     * Calculates the point to snap the measurement to, returns {@code null} if not applicable.
     *
     * @param event contains the current mouse location
     * @return the point to snap the measurement to
     * @see XYMeasureWithAnnotations#getCrosshairPoint(ChartMouseEvent)
     */
    protected Point2DNumber calculateCrosshairPoint(ChartMouseEvent event) {
        return super.getCrosshairPoint(event);
    }

    @Override
    protected void addAnnotation(XYPlot plot, Point2DNumber first, Point2DNumber second, MouseEvent event) {
        String label;
        XYMeasurementAnnotation.Orientation orientation;
        if (measurementAxis == MeasurementAxis.DOMAIN) {
            label = createMeasurementLabel(first.getX(), second.getX(), event);
            orientation = plot.getOrientation() == PlotOrientation.VERTICAL
                    ? Orientation.HORIZONTAL
                    : Orientation.VERTICAL;
        } else {
            label = createMeasurementLabel(first.getY(), second.getY(), event);
            orientation = plot.getOrientation() == PlotOrientation.VERTICAL
                    ? Orientation.VERTICAL
                    : Orientation.HORIZONTAL;
        }
        XYAnnotation measurementAnnotation = createMeasurementAnnotation(orientation, label, first, second);
        if (measurementAnnotation != null) {
            plot.addAnnotation(measurementAnnotation);
        }
    }

    @Override
    protected XYAnnotation createMeasurementAnnotation(Orientation orientation, String label, Point2DNumber first,
            Point2DNumber second)
    {
        if (first instanceof XYGanttPoint && second instanceof XYGanttPoint) {
            final XYGanttDataItem<?> firstDataItem = ((XYGanttPoint)first).getDataItem();
            final Snap firstSnap = getSnap((XYGanttPoint)first);
            final XYGanttDataItem<?> secondDataItem = ((XYGanttPoint)second).getDataItem();
            final Snap secondSnap = getSnap((XYGanttPoint)second);
            return createMeasurementAnnotation(orientation, label, firstDataItem, firstSnap, secondDataItem,
                    secondSnap);
        }
        return super.createMeasurementAnnotation(orientation, label, first, second);
    }

    protected XYAnnotation createMeasurementAnnotation(Orientation orientation, String label,
            XYGanttDataItem<?> firstDataItem, Snap firstSnap, XYGanttDataItem<?> secondDataItem, Snap secondSnap)
    {
        if (label == null) {
            return null;
        }
        return new XYGanttMeasurementAnnotation<>(orientation, label, firstDataItem, firstSnap, secondDataItem,
                secondSnap);
    }

    protected Snap getSnap(XYGanttPoint xyGanttPoint) {
        final boolean snapToStart = measurementAxis == MeasurementAxis.DOMAIN
                ? xyGanttPoint.getX() == xyGanttPoint.getDataItem().getXLowValue()
                : xyGanttPoint.getY() == xyGanttPoint.getDataItem().getYLowValue();
        return snapToStart ? Snap.START : Snap.END;
    }

    protected static class XYGanttPoint extends Point2DNumber {
        private final XYGanttDataItem<?> dataItem;

        public XYGanttPoint(XYGanttDataItem<?> dataItem, Point2DNumber point) {
            super(point.x, point.y);
            this.dataItem = dataItem;
        }

        public XYGanttDataItem<?> getDataItem() {
            return dataItem;
        }
    }
}
