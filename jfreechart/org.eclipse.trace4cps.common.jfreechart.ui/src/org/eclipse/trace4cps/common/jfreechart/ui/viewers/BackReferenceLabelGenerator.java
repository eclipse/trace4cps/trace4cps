/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.ui.viewers;

import java.util.function.Function;

import org.eclipse.trace4cps.common.jfreechart.chart.labels.XYDataItemLabelGenerator;
import org.eclipse.trace4cps.common.jfreechart.data.xy.XYDataItemResolver;

public class BackReferenceLabelGenerator extends XYDataItemLabelGenerator {
    public static final BackReferenceLabelGenerator DEFAULT = new BackReferenceLabelGenerator(
            XYDataItemLabelGenerator::toString, XYDataItemResolver.DEFAULT);

    public BackReferenceLabelGenerator(Function<Object, String> labelSupplier) {
        this(labelSupplier, XYDataItemResolver.DEFAULT);
    }

    public BackReferenceLabelGenerator(Function<Object, String> labelSupplier, XYDataItemResolver xyDataItemResolver) {
        super(o -> generateLabel(o, labelSupplier), xyDataItemResolver);
    }

    private static String generateLabel(Object dataItem, Function<Object, String> labelSupplier) {
        if (dataItem instanceof BackReferenceProvider<?>) {
            final Object backReference = ((BackReferenceProvider<?>)dataItem).getBackReference();
            if (backReference != null) {
                final String label = labelSupplier.apply(backReference);
                if (label != null) {
                    return label;
                }
            }
        }
        return labelSupplier.apply(dataItem);
    }
}
