/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.ui.gantt;

import static org.jfree.chart.plot.PlotOrientation.VERTICAL;

import java.awt.geom.Rectangle2D;

import org.eclipse.trace4cps.common.jfreechart.ui.data.XYBackreferenceIntervalDataItem;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.Range;

public class XYGanttDataItem<T> extends XYBackreferenceIntervalDataItem<T> {
    private static final long serialVersionUID = 3004235510100651118L;

    public static final double DEFAULT_OCCUPATION = 0.9;

    public static final double DEFAULT_OFFSET = 0.05;

    public static final double DEFAULT_RESOURCE_LENGTH = 1;

    public XYGanttDataItem(Number resourceIndex, Number start, Number end) {
        this(calculateResourceRange(resourceIndex), start, end);
    }

    public XYGanttDataItem(Number resourceIndex, Number start, Number end, T backReference) {
        this(calculateResourceRange(resourceIndex), start, end, backReference);
    }

    public XYGanttDataItem(Number resourceIndex, Number resourceOccupation, Number resourceOffset, Number start,
            Number end, PlotOrientation orientation, T backReference)
    {
        this(calculateResourceRange(resourceIndex), resourceOccupation, resourceOffset, start, end, orientation,
                backReference);
    }

    public XYGanttDataItem(Range resourceRange, Number start, Number end) {
        this(resourceRange, DEFAULT_OCCUPATION, DEFAULT_OFFSET, start, end, VERTICAL, null);
    }

    public XYGanttDataItem(Range resourceRange, Number start, Number end, T backReference) {
        this(resourceRange, DEFAULT_OCCUPATION, DEFAULT_OFFSET, start, end, VERTICAL, backReference);
    }

    public XYGanttDataItem(Range resourceRange, Number resourceOccupation, Number resourceOffset, Number start,
            Number end, PlotOrientation orientation, T backReference)
    {
        this(calculateBounds(resourceRange, resourceOccupation, resourceOffset, start, end, orientation),
                backReference);
    }

    private XYGanttDataItem(Rectangle2D bounds, T backReference) {
        super(bounds.getCenterX(), bounds.getMinX(), bounds.getMaxX(), bounds.getCenterY(), bounds.getMinY(),
                bounds.getMaxY(), backReference);
    }

    private static Range calculateResourceRange(Number resourceIndex) {
        double centralValue = resourceIndex.doubleValue();
        double lower = centralValue - (DEFAULT_RESOURCE_LENGTH / 2);
        double upper = centralValue + (DEFAULT_RESOURCE_LENGTH / 2);
        return new Range(lower, upper);
    }

    private static Rectangle2D calculateBounds(Range resourceRange, Number occupation, Number offset, Number start,
            Number end, PlotOrientation orientation)
    {
        double x = start.doubleValue();
        double w = end.doubleValue() - start.doubleValue();
        double y = resourceRange.getLowerBound() + (offset.doubleValue() * resourceRange.getLength());
        double h = occupation.doubleValue() * resourceRange.getLength();
        if (PlotOrientation.VERTICAL == orientation) {
            return new Rectangle2D.Double(x, y, w, h);
        } else {
            return new Rectangle2D.Double(y, x, h, w);
        }
    }
}
