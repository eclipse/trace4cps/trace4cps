/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.ui.data;

import org.eclipse.trace4cps.common.jfreechart.data.xy.XYLabeledDataItem;
import org.eclipse.trace4cps.common.jfreechart.ui.viewers.BackReferenceProvider;

public class XYBackreferenceDataItem<T> extends XYLabeledDataItem implements BackReferenceProvider<T> {
    private static final long serialVersionUID = 2278894211820843418L;

    private final transient T backReference;

    /**
     * Constructs a new data item.
     *
     * @param x the x-value.
     * @param y the y-value.
     * @param backReference the back reference, possibly {@code null}.
     */
    public XYBackreferenceDataItem(double x, double y, T backReference) {
        super(x, y);
        this.backReference = backReference;
    }

    /**
     * Constructs a new data item.
     *
     * @param x the x-value ({@code null} NOT permitted).
     * @param y the y-value ({@code null} permitted).
     */
    public XYBackreferenceDataItem(Number x, Number y, T backReference) {
        super(x, y);
        this.backReference = backReference;
    }

    @Override
    public T getBackReference() {
        return backReference;
    }
}
