/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.ui.gantt;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYItemRendererState;
import org.jfree.data.xy.XYDataset;

public class XYGanttRenderer extends XYBarRenderer {
    private static final long serialVersionUID = 4578003575487129237L;

    /**
     * The default constructor.
     */
    public XYGanttRenderer() {
        super();
        setUseYInterval(true);
        setDataBoundsIncludesVisibleSeriesOnly(false);
    }

    /**
     * Constructs a new renderer.
     *
     * @param margin the percentage amount to trim from the width of each bar.
     */
    public XYGanttRenderer(double margin) {
        super(margin);
    }

    @Override
    public XYItemRendererState initialise(Graphics2D g2, Rectangle2D dataArea, XYPlot plot, XYDataset dataset,
            PlotRenderingInfo info)
    {
        XYItemRendererState state = super.initialise(g2, dataArea, plot, dataset, info);
        // We can't support processing of visible data items only
        state.setProcessVisibleItemsOnly(false);
        return state;
    }
}
