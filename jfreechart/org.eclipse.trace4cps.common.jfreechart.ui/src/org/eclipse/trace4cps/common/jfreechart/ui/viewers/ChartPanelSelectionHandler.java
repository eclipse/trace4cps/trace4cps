/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.ui.viewers;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.PlatformUI;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.annotations.XYAnnotation;
import org.jfree.chart.annotations.XYAnnotationBoundsInfo;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.panel.AbstractOverlay;
import org.jfree.chart.panel.Overlay;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.ui.RectangleEdge;
import org.jfree.chart.util.Args;
import org.jfree.data.Range;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYDataset;

public class ChartPanelSelectionHandler {
    public static enum SelectionType {
        Single, Multi, ReadOnly, None
    }

    public static enum Axis {
        X, Y, BOTH
    }

    private final MouseSelectionListener mouseSelectionListener = new MouseSelectionListener();

    private final SelectionOverlay selectionOverlay = new SelectionOverlay();

    private final ChartPanel chartPanel;

    private final ISelectionProvider selectionProvider;

    private BackReferenceResolver backReferenceResolver = BackReferenceResolver.DEFAULT;

    private double zoomToSelectionMargin = 0;

    private SelectionType selectionType;

    public static ChartPanelSelectionHandler connectSelection(ChartPanel chartPanel,
            ISelectionProvider selectionProvider, SelectionType selectionType)
    {
        return new ChartPanelSelectionHandler(chartPanel, selectionProvider, selectionType);
    }

    public ChartPanelSelectionHandler(ChartPanel chartPanel, ISelectionProvider selectionProvider,
            SelectionType selectionType)
    {
        Args.nullNotPermitted(chartPanel, "chartPanel");
        Args.nullNotPermitted(selectionProvider, "selectionProvider");
        this.chartPanel = chartPanel;
        this.selectionProvider = selectionProvider;
        setSelectionType(selectionType);
    }

    public ChartPanel getChartPanel() {
        return chartPanel;
    }

    public ISelectionProvider getSelectionProvider() {
        return selectionProvider;
    }

    public SelectionType getSelectionType() {
        return selectionType;
    }

    /**
     * Changing the selection type will not affect the current selection.
     */
    public void setSelectionType(SelectionType selectionType) {
        Args.nullNotPermitted(selectionType, "selectionType");
        // Deregistering all listeners
        this.chartPanel.removeOverlay(selectionOverlay);
        this.selectionProvider.removeSelectionChangedListener(selectionOverlay);
        this.chartPanel.removeChartMouseListener(mouseSelectionListener);

        this.selectionType = selectionType;

        switch (selectionType) {
            case Single:
            case Multi:
                this.chartPanel.addChartMouseListener(mouseSelectionListener);
                // No break, continue next case
            case ReadOnly:
                this.selectionProvider.addSelectionChangedListener(selectionOverlay);
                this.chartPanel.addOverlay(selectionOverlay);
                break;
            case None:
                // Do nothing, already deregistered
        }
    }

    public Paint getSelectionPaint() {
        return selectionOverlay.selectionPaint;
    }

    public void setSelectionPaint(Paint selectionPaint) {
        selectionOverlay.selectionPaint = selectionPaint;
    }

    public Stroke getSelectionStroke() {
        return selectionOverlay.selectionStroke;
    }

    public void setSelectionStroke(Stroke selectionStroke) {
        selectionOverlay.selectionStroke = selectionStroke;
    }

    public double getZoomToSelectionMargin() {
        return zoomToSelectionMargin;
    }

    public void setZoomToSelectionMargin(double zoomToSelectionMargin) {
        this.zoomToSelectionMargin = zoomToSelectionMargin;
    }

    public BackReferenceResolver getBackReferenceResolver() {
        return backReferenceResolver;
    }

    public void setBackReferenceResolver(BackReferenceResolver backReferenceResolver) {
        this.backReferenceResolver = backReferenceResolver;
    }

    public void zoomToSelection() {
        zoomToSelection(Axis.BOTH);
    }

    public void zoomToSelection(Axis zoomType) {
        Map<Object, Rectangle2D> selectionBounds = getSelectionBounds();
        if (selectionBounds.isEmpty()) {
            return; // Nothing to do
        }

        // Calculate bounds which includes whole selection
        double minX = Double.POSITIVE_INFINITY;
        double minY = Double.POSITIVE_INFINITY;
        double maxX = Double.NEGATIVE_INFINITY;
        double maxY = Double.NEGATIVE_INFINITY;
        for (Rectangle2D bounds: selectionBounds.values()) {
            minX = Math.min(minX, bounds.getMinX());
            minY = Math.min(minY, bounds.getMinY());
            maxX = Math.max(maxX, bounds.getMaxX());
            maxY = Math.max(maxY, bounds.getMaxY());
        }

        // Add margins
        double marginX = zoomToSelectionMargin * (maxX - minX);
        double marginY = zoomToSelectionMargin * (maxY - minY);
        minX -= marginX;
        minY -= marginY;
        maxX += marginX;
        maxY += marginY;

        // Keep zoom if specified
        Rectangle2D dataArea = chartPanel.getChartRenderingInfo().getPlotInfo().getDataArea();
        switch (zoomType) {
            case X:
                minY = dataArea.getMinY();
                maxY = dataArea.getMaxY();
                break;
            case Y:
                minX = dataArea.getMinX();
                maxX = dataArea.getMaxX();
                break;
            default:
                // Nothing to do
                break;
        }

        // Zooming with a width or height of 0 will be ignored, hence max with
        // minimal value
        double width = Math.max(maxX - minX, Double.MIN_VALUE);
        double height = Math.max(maxY - minY, Double.MIN_VALUE);

        chartPanel.zoom(new Rectangle2D.Double(minX, minY, width, height));
    }

    @SuppressWarnings("unchecked")
    protected Map<Object, Rectangle2D> getSelectionBounds() {
        ISelection selection = selectionProvider.getSelection();
        Plot plot = chartPanel.getChart().getPlot();
        if (!(plot instanceof XYPlot) || !(selection instanceof StructuredSelection) || selection.isEmpty()) {
            return Collections.emptyMap();
        }
        List<?> selectionItems = ((StructuredSelection)selection).toList();
        XYPlot xyPlot = (XYPlot)plot;

        Rectangle2D dataArea = chartPanel.getChartRenderingInfo().getPlotInfo().getDataArea();
        Map<Object, Rectangle2D> selectionBounds = new HashMap<>(selectionItems.size());
        for (int d = 0; d < xyPlot.getDatasetCount(); d++) {
            XYDataset ds = xyPlot.getDataset(d);
            if (!(ds instanceof IntervalXYDataset)) {
                continue;
            }
            IntervalXYDataset dataset = (IntervalXYDataset)ds;

            ValueAxis domainAxis = xyPlot.getDomainAxisForDataset(d);
            ValueAxis rangeAxis = xyPlot.getRangeAxisForDataset(d);
            XYItemRenderer renderer = xyPlot.getRendererForDataset(ds);
            if (null != renderer) {
                for (XYAnnotation xyAnnotation: (List<XYAnnotation>)renderer.getAnnotations()) {
                    Object selectionItem = getSelectionItem(xyAnnotation);
                    if (null != selectionItem && selectionItems.contains(selectionItem)
                            && xyAnnotation instanceof XYAnnotationBoundsInfo)
                    {
                        final Range xRange = ((XYAnnotationBoundsInfo)xyAnnotation).getXRange();
                        final Range yRange = ((XYAnnotationBoundsInfo)xyAnnotation).getYRange();
                        Rectangle2D j2DBounds = valueToJava2D(xRange.getLowerBound(), yRange.getLowerBound(),
                                xRange.getUpperBound(), yRange.getUpperBound(), xyPlot, dataArea, domainAxis,
                                rangeAxis);
                        addBounds(selectionBounds, selectionItem, j2DBounds);
                    }
                }
            }

            for (int s = 0; s < dataset.getSeriesCount(); s++) {
                final int itemCount = dataset.getItemCount(s);
                for (int i = 0; i < itemCount; i++) {
                    Object selectionItem = getSelectionItem(dataset, s, i);
                    if (null != selectionItem && selectionItems.contains(selectionItem)) {
                        double xStart = dataset.getStartXValue(s, i);
                        double yStart = dataset.getStartYValue(s, i);
                        double xEnd = dataset.getEndXValue(s, i);
                        double yEnd = dataset.getEndYValue(s, i);
                        Rectangle2D j2DBounds = valueToJava2D(xStart, yStart, xEnd, yEnd, xyPlot, dataArea, domainAxis,
                                rangeAxis);
                        addBounds(selectionBounds, selectionItem, j2DBounds);
                    }
                }
            }
        }

        for (XYAnnotation xyAnnotation: xyPlot.getAnnotations()) {
            Object selectionItem = getSelectionItem(xyAnnotation);
            if (null != selectionItem && selectionItems.contains(selectionItem)
                    && xyAnnotation instanceof XYAnnotationBoundsInfo)
            {
                final Range xRange = ((XYAnnotationBoundsInfo)xyAnnotation).getXRange();
                final Range yRange = ((XYAnnotationBoundsInfo)xyAnnotation).getYRange();
                Rectangle2D j2DBounds = valueToJava2D(xRange.getLowerBound(), yRange.getLowerBound(),
                        xRange.getUpperBound(), yRange.getUpperBound(), xyPlot, dataArea, xyPlot.getDomainAxis(),
                        xyPlot.getRangeAxis());
                addBounds(selectionBounds, selectionItem, j2DBounds);
            }
        }

        return selectionBounds;
    }

    protected Rectangle2D valueToJava2D(double x1, double y1, double x2, double y2, XYPlot xyPlot, Rectangle2D dataArea,
            ValueAxis domainAxis, ValueAxis rangeAxis)
    {
        RectangleEdge domainAxisEdge = xyPlot.getDomainAxisEdge(xyPlot.getDomainAxisIndex(domainAxis));
        RectangleEdge rangeAxisEdge = xyPlot.getRangeAxisEdge(xyPlot.getRangeAxisIndex(rangeAxis));

        boolean isVerticalOrientation = xyPlot.getOrientation() == PlotOrientation.VERTICAL;
        ValueAxis xAxis = isVerticalOrientation ? domainAxis : rangeAxis;
        RectangleEdge xAxisEdge = isVerticalOrientation ? domainAxisEdge : rangeAxisEdge;
        ValueAxis yAxis = isVerticalOrientation ? rangeAxis : domainAxis;
        RectangleEdge yAxisEdge = isVerticalOrientation ? rangeAxisEdge : domainAxisEdge;

        double j2DX1 = xAxis.valueToJava2D(x1, dataArea, xAxisEdge);
        double j2DY1 = yAxis.valueToJava2D(y1, dataArea, yAxisEdge);
        double j2DX2 = xAxis.valueToJava2D(x2, dataArea, xAxisEdge);
        double j2DY2 = yAxis.valueToJava2D(y2, dataArea, yAxisEdge);
        return new Rectangle2D.Double(Math.min(j2DX1, j2DX2), Math.min(j2DY1, j2DY2), Math.abs(j2DX2 - j2DX1),
                Math.abs(j2DY2 - j2DY1));
    }

    /**
     * Selection items might be represented as multiple items on the screen
     */
    private void addBounds(Map<Object, Rectangle2D> map, Object o, Rectangle2D bounds) {
        map.compute(o, (k, v) -> null == v ? bounds : bounds.createUnion(v));
    }

    protected Object getSelectionItem(ChartEntity chartEntity) {
        BackReferenceProvider<?> backReferenceProvider = backReferenceResolver
                .resolveBackReferenceProvider(chartEntity);
        return null == backReferenceProvider ? null : backReferenceProvider.getBackReference();
    }

    protected Object getSelectionItem(XYDataset dataset, int seriesIndex, int item) {
        BackReferenceProvider<?> backReferenceProvider = backReferenceResolver.resolveBackReferenceProvider(dataset,
                seriesIndex, item);
        return null == backReferenceProvider ? null : backReferenceProvider.getBackReference();
    }

    protected Object getSelectionItem(XYAnnotation annotation) {
        BackReferenceProvider<?> backReferenceProvider = backReferenceResolver.resolveBackReferenceProvider(annotation);
        return null == backReferenceProvider ? null : backReferenceProvider.getBackReference();
    }

    private class MouseSelectionListener implements ChartMouseListener {
        @Override
        public void chartMouseMoved(ChartMouseEvent event) {
            // Empty
        }

        @Override
        public void chartMouseClicked(ChartMouseEvent event) {
            if (event.getTrigger().isAltDown() || event.getTrigger().isAltGraphDown()
                    || event.getTrigger().isMetaDown())
            {
                return;
            }
            Object selectionItem = getSelectionItem(event.getEntity());
            if (null == selectionItem) {
                // Entity doesn't hold a selection item
                return;
            }
            final StructuredSelection selection;
            if (selectionType == SelectionType.Multi && event.getTrigger().isControlDown()
                    && !selectionProvider.getSelection().isEmpty())
            {
                @SuppressWarnings("unchecked")
                ArrayList<Object> newSelection = new ArrayList<Object>(
                        ((StructuredSelection)selectionProvider.getSelection()).toList());
                if (!newSelection.remove(selectionItem)) {
                    newSelection.add(selectionItem);
                }
                selection = new StructuredSelection(newSelection);
            } else {
                selection = new StructuredSelection(selectionItem);
            }
            PlatformUI.getWorkbench().getDisplay().asyncExec(() -> selectionProvider.setSelection(selection));
        }
    }

    private class SelectionOverlay extends AbstractOverlay implements Overlay, ISelectionChangedListener {
        /** The border paint for the selected bar. */
        private Paint selectionPaint = new Color(255, 255, 0, 200);

        /** The stroke for the selected bar. */
        private Stroke selectionStroke = new BasicStroke(2f);

        @Override
        public void selectionChanged(SelectionChangedEvent event) {
            fireOverlayChanged();
        }

        @Override
        public void paintOverlay(Graphics2D g2, ChartPanel chartPanel) {
            final ISelection selection = selectionProvider.getSelection();
            if (selection == null || selection.isEmpty()) {
                return;
            }

            Graphics2D g = (Graphics2D)g2.create();
            g.clip(chartPanel.getScreenDataArea());
            g.setStroke(selectionStroke);
            g.setPaint(selectionPaint);

            for (ChartEntity chartEntity: getSelectedChartEntities(selection)) {
                AffineTransform transform = new AffineTransform();
                transform.scale(chartPanel.getScaleX(), chartPanel.getScaleY());
                Shape selectionShape = transform.createTransformedShape(chartEntity.getArea());
                g.draw(selectionStroke.createStrokedShape(selectionShape));
            }
        }

        private Collection<ChartEntity> getSelectedChartEntities(ISelection selection) {
            if (selection.isEmpty() || !(selection instanceof StructuredSelection)) {
                return Collections.emptyList();
            }

            List<?> selectionItems = ((StructuredSelection)selection).toList();
            Collection<ChartEntity> result = new ArrayList<>(selectionItems.size());
            for (Object chartEntity: chartPanel.getChartRenderingInfo().getEntityCollection().getEntities()) {
                Object selectionItem = getSelectionItem((ChartEntity)chartEntity);
                if (selectionItems.contains(selectionItem)) {
                    result.add((ChartEntity)chartEntity);
                }
            }
            return result;
        }
    }
}
