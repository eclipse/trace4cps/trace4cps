/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.ui.widgets;

import java.beans.PropertyChangeListener;
import java.util.LinkedList;
import java.util.Objects;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.Action;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.event.ChartChangeEventType;
import org.jfree.chart.event.PlotChangeListener;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.Range;

/**
 * This class records the visible areas and provides back/forward buttons to navigate in the view history.
 */
public class ChartPanelCompositeHistory {
    private final Action backAction = new Action("Back") {
        {
            setImageDescriptor(
                    PlatformUI.getWorkbench().getSharedImages().getImageDescriptor(ISharedImages.IMG_TOOL_BACK));
            setDisabledImageDescriptor(PlatformUI.getWorkbench().getSharedImages()
                    .getImageDescriptor(ISharedImages.IMG_TOOL_BACK_DISABLED));
        }

        @Override
        public void run() {
            back();
        }
    };

    private final Action forwardAction = new Action("Forward") {
        {
            setImageDescriptor(
                    PlatformUI.getWorkbench().getSharedImages().getImageDescriptor(ISharedImages.IMG_TOOL_FORWARD));
            setDisabledImageDescriptor(PlatformUI.getWorkbench().getSharedImages()
                    .getImageDescriptor(ISharedImages.IMG_TOOL_FORWARD_DISABLED));
        }

        @Override
        public void run() {
            forward();
        }
    };

    private final PropertyChangeListener chartChangeListener = e -> {
        final JFreeChart chart = (JFreeChart)e.getNewValue();
        setPlot(chart == null ? null : chart.getPlot());
    };

    private final LinkedList<XYPlotHistoryElement> history = new LinkedList<>();

    private final LinkedList<XYPlotHistoryElement> future = new LinkedList<>();

    private final PlotChangeListener plotChangeListener;

    private final Job updateJob;

    private XYPlot xyPlot = null;

    public ChartPanelCompositeHistory(ChartPanelComposite chartPanelComposite) {
        this(chartPanelComposite, 50, 1000);
    }

    public ChartPanelCompositeHistory(ChartPanelComposite chartPanelComposite, int historySize, long quietTime) {
        updateJob = new Job("ChartPanelCompositeHistory update") {
            {
                setSystem(true);
            }

            @Override
            protected IStatus run(IProgressMonitor monitor) {
                XYPlotHistoryElement current = createHistoryElement(xyPlot);
                if (history.isEmpty() || !Objects.equals(current, history.peek())) {
                    history.push(current);
                    while (history.size() > historySize + 1) {
                        history.removeLast();
                    }
                    future.clear();
                    updateActions();
                }
                return Status.OK_STATUS;
            }
        };

        plotChangeListener = e -> {
            if (e.getType() != ChartChangeEventType.GENERAL) {
                // Clear the history when the dataset of the plot changes as we cannot rely
                // anymore that the saved ranges are still within the data range.
                clearHistory();
            }
            updateJob.cancel();
            updateJob.schedule(quietTime);
        };

        chartPanelComposite.getChartPanel().addPropertyChangeListener(ChartPanel.PROPERTY_CHART, chartChangeListener);
        chartPanelComposite.addDisposeListener(e -> {
            ((ChartPanelComposite)e.getSource()).getChartPanel().removePropertyChangeListener(ChartPanel.PROPERTY_CHART,
                    chartChangeListener);
            dispose();
        });

        final JFreeChart chart = chartPanelComposite.getChart();
        setPlot(chart == null ? null : chart.getPlot());
    }

    public void dispose() {
        setPlot(null);
    }

    public Action getBackAction() {
        return backAction;
    }

    public Action getForwardAction() {
        return forwardAction;
    }

    public void clearHistory() {
        history.clear();
        future.clear();
        updateActions();
    }

    protected synchronized void setPlot(Plot plot) {
        if (xyPlot == plot) {
            // Ignore
            return;
        }
        if (xyPlot != null) {
            xyPlot.removeChangeListener(plotChangeListener);
        }
        clearHistory();
        if (plot instanceof XYPlot) {
            xyPlot = (XYPlot)plot;
            xyPlot.addChangeListener(plotChangeListener);
        } else {
            xyPlot = null;
        }
    }

    protected synchronized void back() {
        if (history.size() <= 1) {
            return;
        }
        future.push(history.pop());
        history.peek().apply(xyPlot);
        updateActions();
    }

    protected synchronized void forward() {
        if (future.isEmpty()) {
            return;
        }
        history.push(future.pop());
        history.peek().apply(xyPlot);
        updateActions();
    }

    protected void updateActions() {
        backAction.setEnabled(history.size() > 1);
        forwardAction.setEnabled(!future.isEmpty());
    }

    protected XYPlotHistoryElement createHistoryElement(XYPlot xyPlot) {
        return new XYPlotHistoryElement(xyPlot);
    }

    protected static class XYPlotHistoryElement {
        private final Range rangeAxisRange;

        private final Range domainAxisRange;

        public XYPlotHistoryElement(XYPlot xyPlot) {
            final ValueAxis rangeAxis = null == xyPlot ? null : xyPlot.getRangeAxis();
            rangeAxisRange = null == rangeAxis || rangeAxis.isAutoRange() ? null : rangeAxis.getRange();
            final ValueAxis domainAxis = null == xyPlot ? null : xyPlot.getDomainAxis();
            domainAxisRange = null == domainAxis || domainAxis.isAutoRange() ? null : domainAxis.getRange();
        }

        public Range getRangeAxisRange() {
            return rangeAxisRange;
        }

        public Range getDomainAxisRange() {
            return domainAxisRange;
        }

        public void apply(XYPlot xyPlot) {
            final boolean notify = xyPlot.isNotify();
            xyPlot.setNotify(false);
            try {
                final ValueAxis rangeAxis = xyPlot.getRangeAxis();
                if (null != rangeAxis) {
                    if (null == rangeAxisRange) {
                        rangeAxis.setAutoRange(true, true);
                    } else {
                        rangeAxis.setRange(rangeAxisRange, true, true);
                    }
                }
                final ValueAxis domainAxis = xyPlot.getDomainAxis();
                if (null != domainAxis) {
                    if (null == domainAxisRange) {
                        domainAxis.setAutoRange(true, true);
                    } else {
                        domainAxis.setRange(domainAxisRange, true, true);
                    }
                }
            } finally {
                xyPlot.setNotify(notify);
            }
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((domainAxisRange == null) ? 0 : domainAxisRange.hashCode());
            result = prime * result + ((rangeAxisRange == null) ? 0 : rangeAxisRange.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            XYPlotHistoryElement other = (XYPlotHistoryElement)obj;
            if (domainAxisRange == null) {
                if (other.domainAxisRange != null) {
                    return false;
                }
            } else if (!domainAxisRange.equals(other.domainAxisRange)) {
                return false;
            }
            if (rangeAxisRange == null) {
                if (other.rangeAxisRange != null) {
                    return false;
                }
            } else if (!rangeAxisRange.equals(other.rangeAxisRange)) {
                return false;
            }
            return true;
        }
    }
}
