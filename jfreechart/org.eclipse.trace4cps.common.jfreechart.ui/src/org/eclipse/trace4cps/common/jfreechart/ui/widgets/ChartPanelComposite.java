/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.ui.widgets;

import static org.eclipse.core.runtime.IStatus.ERROR;
import static org.eclipse.trace4cps.common.jfreechart.ui.JFreeChartUIPlugin.PLUGIN_ID;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JApplet;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.trace4cps.common.jfreechart.ui.internal.ChartPanelFactory;
import org.eclipse.trace4cps.common.jfreechart.ui.internal.ChartPanelScrollbarHandler;
import org.eclipse.trace4cps.common.jfreechart.ui.internal.SWT_AWT_PATCH_377104;
import org.eclipse.ui.statushandlers.StatusManager;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.util.Args;

public class ChartPanelComposite extends Composite {
    private final Frame baseFrame;

    private final ChartPanel chartPanel;

    private final ChartPanelScrollbarHandler scrollbarHandler;

    public ChartPanelComposite(Composite parent, int style) {
        this(ChartPanelFactory.createChartPanel(null), parent, style);
    }

    public ChartPanelComposite(ChartPanel chartPanel, Composite parent, int style) {
        super(parent, SWT.EMBEDDED | style);
        Args.nullNotPermitted(chartPanel, "chartPanel");
        this.baseFrame = SWT_AWT_PATCH_377104.new_Frame(this);
        this.chartPanel = chartPanel;
        this.scrollbarHandler = new ChartPanelScrollbarHandler(this);

        final Color background = toAwtColor(getDisplay().getSystemColor(SWT.COLOR_INFO_BACKGROUND));
        try {
            invokeAndWait(() -> createAWTComponent(baseFrame, background));
        } catch (InvocationTargetException | InterruptedException e) {
            IStatus status = new Status(ERROR, PLUGIN_ID, "Failed to create AWT component: " + e.getLocalizedMessage(),
                    e);
            StatusManager.getManager().handle(status);
        }
    }

    private void createAWTComponent(Frame baseFrame, Color background) {
        chartPanel.setBackground(background);
        chartPanel.addMouseListener(new SWTMenuActivator());

        JApplet chartApplet = new JApplet();
        chartApplet.add(chartPanel);
        baseFrame.add(chartApplet);
    }

    @Override
    public void dispose() {
        EventQueue.invokeLater(baseFrame::dispose);
        scrollbarHandler.dispose();
        super.dispose();
    }

    @Override
    public boolean setFocus() {
        final boolean gotFocus = super.setFocus();
        if (gotFocus) {
            EventQueue.invokeLater(chartPanel::requestFocusInWindow);
        }
        return gotFocus;
    }

    public boolean isNotify() {
        final JFreeChart chart = getChart();
        return chart != null && chart.isNotify();
    }

    public void setNotify(boolean notify) {
        final JFreeChart chart = getChart();
        if (chart != null && (chart.isNotify() != notify || chart.getPlot().isNotify() != notify)) {
            chart.getPlot().setNotify(notify);
            chart.setNotify(notify);
        }
    }

    /**
     * @return the {@link #getChart() chart's} plot as {@link XYPlot}.
     * @throws ClassCastException if {@link JFreeChart#getPlot() plot} is not an instance of {@link XYPlot}.
     * @see JFreeChart#getXYPlot()
     */
    public XYPlot getXYPlot() throws ClassCastException {
        return (XYPlot)getPlot();
    }

    /**
     * @return the {@link #getChart() chart's} plot.
     * @see JFreeChart#getPlot()
     */
    public Plot getPlot() {
        final JFreeChart chart = getChart();
        return null == chart ? null : chart.getPlot();
    }

    /**
     * @return the {@link #getChartPanel() chart panel's} chart.
     * @see ChartPanelComposite#getChartPanel()
     */
    public JFreeChart getChart() {
        return chartPanel.getChart();
    }

    /**
     * @return the chart panel for this composite.
     */
    public ChartPanel getChartPanel() {
        return chartPanel;
    }

    public void setChart(JFreeChart chart) {
        try {
            invokeAndWait(() -> chartPanel.setChart(chart));
        } catch (InvocationTargetException | InterruptedException e) {
            IStatus status = new Status(ERROR, PLUGIN_ID, "Failed to set chart: " + e.getLocalizedMessage(), e);
            StatusManager.getManager().handle(status);
        }
    }

    @Override
    public void setMenu(Menu menu) {
        if (null == menu) {
            // Restore the original Swing popup trigger
            chartPanel.setPopupPredicate(MouseEvent::isPopupTrigger);
        } else {
            // Disable the Swing popup menu
            // We have added our own SWTMenuActivator to the chart panel
            chartPanel.setPopupPredicate(e -> false);
        }
        super.setMenu(menu);
    }

    private static java.awt.Color toAwtColor(org.eclipse.swt.graphics.Color color) {
        return new java.awt.Color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    /**
     * Causes the {@link Runnable#run() run method of runnable} to be called in the {@link #isDispatchThread dispatch
     * thread} of {@link Toolkit#getSystemEventQueue the system EventQueue}. This will happen after all pending events
     * are processed. The call blocks until this has happened. When this method is called from the
     * {@link #isDispatchThread event dispatcher thread}, it simply invokes the {@link Runnable#run() run method of
     * runnable} This will happen after all pending events are processed.
     *
     * @param runnable the <code>Runnable</code> whose <code>run</code> method should be executed synchronously in the
     *     {@link #isDispatchThread event dispatch thread} of {@link Toolkit#getSystemEventQueue the system EventQueue}
     * @exception InterruptedException if any thread has interrupted this thread
     * @exception InvocationTargetException if an throwable is thrown when running <code>runnable</code>
     * @see EventQueue#invokeAndWait(Runnable)
     */
    private static void invokeAndWait(Runnable runnable) throws InvocationTargetException, InterruptedException {
        if (EventQueue.isDispatchThread()) {
            try {
                runnable.run();
            } catch (Exception e) {
                throw new InvocationTargetException(e);
            }
        } else {
            EventQueue.invokeAndWait(runnable);
        }
    }

    private class SWTMenuActivator extends MouseAdapter {
        @Override
        public void mouseReleased(MouseEvent e) {
            if (e.isPopupTrigger() && !e.isConsumed() && !isDisposed() && !getDisplay().isDisposed()) {
                getDisplay().syncExec(() -> {
                    final Menu menu = getMenu();
                    if (null != menu) {
                        menu.setVisible(true);
                        e.consume();
                    }
                });
            }
        }
    }
}
