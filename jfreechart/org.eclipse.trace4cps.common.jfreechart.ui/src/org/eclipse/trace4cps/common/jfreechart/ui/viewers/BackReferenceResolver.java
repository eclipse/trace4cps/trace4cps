/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.ui.viewers;

import org.eclipse.trace4cps.common.jfreechart.data.xy.XYDataItemResolver;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.annotations.Annotation;
import org.jfree.chart.entity.CategoryItemEntity;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.PieSectionEntity;
import org.jfree.chart.entity.XYAnnotationEntity;
import org.jfree.chart.entity.XYItemEntity;
import org.jfree.data.xy.XYDataset;

public class BackReferenceResolver extends XYDataItemResolver {
    public static final BackReferenceResolver DEFAULT = new BackReferenceResolver();

    public BackReferenceProvider<?> resolveBackReferenceProvider(ChartMouseEvent event) {
        return doResolveBackReferenceProvider(event.getEntity());
    }

    public BackReferenceProvider<?> resolveBackReferenceProvider(ChartEntity chartEntity) {
        return doResolveBackReferenceProvider(chartEntity);
    }

    public BackReferenceProvider<?> resolveBackReferenceProvider(Annotation annotation) {
        return doResolveBackReferenceProvider(annotation);
    }

    public BackReferenceProvider<?> resolveBackReferenceProvider(XYDataset dataset, int series, int item) {
        return doResolveBackReferenceProvider(resolveDataItem(dataset, series, item));
    }

    protected BackReferenceProvider<?> doResolveBackReferenceProvider(Object o) {
        BackReferenceProvider<?> backReferenceProvider = null;
        if (o instanceof BackReferenceProvider<?>) {
            backReferenceProvider = (BackReferenceProvider<?>)o;
        } else if (o instanceof XYItemEntity) {
            final XYItemEntity entity = (XYItemEntity)o;
            backReferenceProvider = resolveBackReferenceProvider(entity.getDataset(), entity.getSeriesIndex(),
                    entity.getItem());
        } else if (o instanceof XYAnnotationEntity) {
            final XYAnnotationEntity entity = (XYAnnotationEntity)o;
            backReferenceProvider = doResolveBackReferenceProvider(entity.getAnnotation());
        } else if (o instanceof PieSectionEntity) {
            final PieSectionEntity entity = (PieSectionEntity)o;
            backReferenceProvider = doResolveBackReferenceProvider(entity.getSectionKey());
        } else if (o instanceof CategoryItemEntity) {
            final CategoryItemEntity entity = (CategoryItemEntity)o;
            backReferenceProvider = doResolveBackReferenceProvider(entity.getColumnKey());
            if (backReferenceProvider == null) {
                backReferenceProvider = doResolveBackReferenceProvider(entity.getRowKey());
            }
        }
        return backReferenceProvider;
    }
}
