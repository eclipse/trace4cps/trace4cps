/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.ui.gantt;

import java.math.BigDecimal;

import org.eclipse.trace4cps.common.jfreechart.ui.viewers.BackReferenceProvider;
import org.jfree.chart.annotations.XYAnnotationBoundsInfo;
import org.jfree.chart.annotations.XYMeasurementAnnotation;
import org.jfree.data.Range;

public class XYGanttMeasurementAnnotation<T> extends XYMeasurementAnnotation implements XYGanttMeasurement<T>,
        BackReferenceProvider<XYGanttMeasurementAnnotationDescriptor<T>>, XYAnnotationBoundsInfo
{
    private static final long serialVersionUID = -5836943007613577145L;

    private final XYGanttMeasurementAnnotationDescriptor<T> descriptor = new XYGanttMeasurementAnnotationDescriptor<T>(
            this);

    private final XYGanttDataItem<? extends T> item1;

    private final Snap snap1;

    private final XYGanttDataItem<? extends T> item2;

    private final Snap snap2;

    public XYGanttMeasurementAnnotation(Orientation orientation, String label, XYGanttDataItem<? extends T> item1,
            Snap snap1, XYGanttDataItem<? extends T> item2, Snap snap2)
    {
        super(orientation, label, snap1 == Snap.START ? item1.getXLowValue() : item1.getXHighValue(), item1.getYValue(),
                snap2 == Snap.START ? item2.getXLowValue() : item2.getXHighValue(), item2.getYValue());
        setBaseCreateEntity(true);
        this.item1 = item1;
        this.snap1 = snap1;
        this.item2 = item2;
        this.snap2 = snap2;
    }

    @Override
    public XYGanttMeasurementAnnotationDescriptor<T> getBackReference() {
        return descriptor;
    }

    @Override
    public T getBackReference1() {
        return item1.getBackReference();
    }

    @Override
    public T getBackReference2() {
        return item2.getBackReference();
    }

    @Override
    public Snap getSnap1() {
        return snap1;
    }

    @Override
    public Snap getSnap2() {
        return snap2;
    }

    @Override
    public Number getTime1() {
        return getX();
    }

    @Override
    public Number getTime2() {
        return getX2();
    }

    @Override
    public Number getDuration() {
        // Try to avoid rounding errors as much as possible
        return BigDecimal.valueOf(getX2()).subtract(BigDecimal.valueOf(getX())).abs();
    }

    @Override
    public Range getXRange() {
        if (getX() > getX2()) {
            return new Range(getX2(), getX());
        } else {
            return new Range(getX(), getX2());
        }
    }

    @Override
    public Range getYRange() {
        if (getY() > getY2()) {
            return new Range(getY2(), getY());
        } else {
            return new Range(getY(), getY2());
        }
    }

    @Override
    public boolean getIncludeInDataBounds() {
        // Represents a measurement between existing points in a data set,
        // hence already included in the data bounds.
        return false;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((item1 == null) ? 0 : item1.hashCode());
        result = prime * result + ((item2 == null) ? 0 : item2.hashCode());
        result = prime * result + ((snap1 == null) ? 0 : snap1.hashCode());
        result = prime * result + ((snap2 == null) ? 0 : snap2.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        XYGanttMeasurementAnnotation<?> other = (XYGanttMeasurementAnnotation<?>)obj;
        if (item1 == null) {
            if (other.item1 != null)
                return false;
        } else if (!item1.equals(other.item1))
            return false;
        if (item2 == null) {
            if (other.item2 != null)
                return false;
        } else if (!item2.equals(other.item2))
            return false;
        if (snap1 != other.snap1)
            return false;
        if (snap2 != other.snap2)
            return false;
        return true;
    }
}
