/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.ui;

import static org.eclipse.core.runtime.IStatus.ERROR;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.trace4cps.common.jfreechart.ui.theme.ChartThemeSupplier;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.jfree.chart.ChartFactory;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class JFreeChartUIPlugin extends AbstractUIPlugin {

    // The plug-in ID
    public static final String PLUGIN_ID = "org.eclipse.trace4cps.common.jfreechart.ui"; //$NON-NLS-1$

    private static final String EXTENSION_CHART_THEME_SUPPLIER = PLUGIN_ID + ".chartthemesupplier";

    // The shared instance
    private static JFreeChartUIPlugin plugin;

    /**
     * The constructor
     */
    public JFreeChartUIPlugin() {
    }

    @Override
    public void start(BundleContext context) throws Exception {
        super.start(context);
        plugin = this;

        configureLookAndFeel();
        configureChartThemeSupplier();
    }

    private void configureLookAndFeel() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException
                | UnsupportedLookAndFeelException e)
        {
            IStatus status = new Status(ERROR, PLUGIN_ID,
                    "Failed to install system look-and-feel: " + e.getLocalizedMessage(), e);
            getLog().log(status);
        }
    }

    private void configureChartThemeSupplier() {
        IConfigurationElement[] elements = Platform.getExtensionRegistry()
                .getConfigurationElementsFor(EXTENSION_CHART_THEME_SUPPLIER);
        int highestRank = Integer.MIN_VALUE;
        IConfigurationElement highestElement = null;
        for (IConfigurationElement element: elements) {
            if (!"chart_theme_supplier".equals(element.getName())) {
                continue;
            }
            String rankStr = element.getAttribute("rank");
            if (null == rankStr) {
                IStatus status = new Status(ERROR, element.getContributor().getName(),
                        "Chart theme supplier is missing mandatory attribute 'rank'");
                getLog().log(status);
            }
            try {
                int rank = Integer.parseInt(rankStr);
                if (rank >= highestRank) {
                    highestRank = rank;
                    highestElement = element;
                }
            } catch (NumberFormatException e) {
                IStatus status = new Status(ERROR, element.getContributor().getName(),
                        "Chart theme supplier attribute 'rank' not a valid integer", e);
                getLog().log(status);
            }
        }

        if (null != highestElement) {
            try {
                ChartThemeSupplier supplier = (ChartThemeSupplier)highestElement.createExecutableExtension("class");
                ChartFactory.setChartTheme(supplier.getChartTheme());
            } catch (CoreException e) {
                IStatus status = new Status(ERROR, highestElement.getContributor().getName(),
                        "Chart theme supplier could not be instantiated", e);
                getLog().log(status);
            }
        }
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        plugin = null;
        super.stop(context);
    }

    /**
     * Returns the shared instance
     *
     * @return the shared instance
     */
    public static JFreeChartUIPlugin getDefault() {
        return plugin;
    }
}
