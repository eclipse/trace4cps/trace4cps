/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.ui.internal;

import java.awt.event.InputEvent;

import org.eclipse.trace4cps.common.jfreechart.chart.ZoomAndPanKeyboardHandler;
import org.eclipse.trace4cps.common.jfreechart.chart.ZoomMouseWheelHandler;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.OfflineRenderingChartPanel;

/**
 * TODO: This is not the best place to store this default configuration
 */
public class ChartPanelFactory {
    private ChartPanelFactory() {
        // Empty for utility classes
    }

    public static ChartPanel createChartPanel(JFreeChart chart) {
        return configureChartPanel(new OfflineRenderingChartPanel(chart));
    }

    public static ChartPanel configureChartPanel(ChartPanel chartPanel) {
        chartPanel.setZoomAroundAnchor(true);
        chartPanel.setZoomOutFactor(1.25);
        chartPanel.setZoomInFactor(0.8);
        chartPanel.setDefaultPanFactor(0.1);
        chartPanel.setPanPredicate(InputEvent::isAltDown);
        chartPanel.setZoomDomainPredicate(e -> e.isControlDown() && !e.isShiftDown());
        chartPanel.setZoomRangePredicate(e -> e.isControlDown() && e.isShiftDown());
        chartPanel.addMouseWheelListener(new ZoomMouseWheelHandler());
        chartPanel.addKeyListener(new ZoomAndPanKeyboardHandler());
        return chartPanel;
    }
}
