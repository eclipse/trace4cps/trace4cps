/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.ui.gantt;

import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertySource;
import org.eclipse.ui.views.properties.PropertyDescriptor;
import org.eclipse.ui.views.properties.TextPropertyDescriptor;

class XYGanttMeasurementAnnotationDescriptor<T> implements IPropertySource, XYGanttMeasurement<T> {
    private enum Properties {
        Label, Duration
    }

    private final XYGanttMeasurementAnnotation<T> annotation;

    private IPropertyDescriptor[] propertyDescriptors;

    public XYGanttMeasurementAnnotationDescriptor(XYGanttMeasurementAnnotation<T> annotation) {
        this.annotation = annotation;
    }

    @Override
    public Object getEditableValue() {
        return this;
    }

    @Override
    public IPropertyDescriptor[] getPropertyDescriptors() {
        if (null == propertyDescriptors) {
            TextPropertyDescriptor labelProperty = new TextPropertyDescriptor(Properties.Label,
                    Properties.Label.name());
            PropertyDescriptor durationProperty = new PropertyDescriptor(Properties.Duration,
                    Properties.Duration.name());
            propertyDescriptors = new IPropertyDescriptor[] {labelProperty, durationProperty};
        }
        return propertyDescriptors;
    }

    @Override
    public Object getPropertyValue(Object id) {
        switch ((XYGanttMeasurementAnnotationDescriptor.Properties)id) {
            case Label:
                return annotation.getText();
            case Duration:
                return annotation.getDuration();
        }
        return null;
    }

    @Override
    public boolean isPropertySet(Object id) {
        return true;
    }

    @Override
    public void resetPropertyValue(Object id) {
        if (id == Properties.Label) {
            annotation.setText("");
        }
    }

    @Override
    public void setPropertyValue(Object id, Object value) {
        if (id == Properties.Label) {
            annotation.setText(String.valueOf(value));
        }
    }

    @Override
    public T getBackReference1() {
        return annotation.getBackReference1();
    }

    @Override
    public Snap getSnap1() {
        return annotation.getSnap1();
    }

    @Override
    public Number getTime1() {
        return annotation.getTime1();
    }

    @Override
    public T getBackReference2() {
        return annotation.getBackReference2();
    }

    @Override
    public Snap getSnap2() {
        return annotation.getSnap2();
    }

    @Override
    public Number getTime2() {
        return annotation.getTime2();
    }

    @Override
    public Number getDuration() {
        return annotation.getDuration();
    }
}
