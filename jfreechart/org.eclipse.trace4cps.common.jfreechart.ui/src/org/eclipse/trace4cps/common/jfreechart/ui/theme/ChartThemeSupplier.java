/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.ui.theme;

import org.jfree.chart.ChartTheme;

public interface ChartThemeSupplier {
    /**
     * @return the {@link ChartTheme} to use for all charts in the application
     */
    ChartTheme getChartTheme();
}
