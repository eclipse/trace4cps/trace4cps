/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.ui.viewers;

import org.eclipse.jface.viewers.ContentViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.trace4cps.common.jfreechart.ui.viewers.ChartPanelSelectionHandler.SelectionType;
import org.eclipse.trace4cps.common.jfreechart.ui.widgets.ChartPanelComposite;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.util.Args;

public abstract class ChartPanelContentViewer extends ContentViewer implements ChartPanelViewer {
    private final Composite parentComposite;

    private ISelection selection = StructuredSelection.EMPTY;

    private boolean preserveSelection = false;

    private ChartPanelControlTuple controlTuple = null;

    public ChartPanelContentViewer(Composite parent) {
        parentComposite = parent;
    }

    @Override
    public Control getControl() {
        return getControlTuple().getControl();
    }

    /**
     * @see ChartPanelControlTuple#getChartPanelComposite()
     */
    @Override
    public ChartPanelComposite getChartPanelComposite() {
        return getControlTuple().getChartPanelComposite();
    }

    /**
     * @see ChartPanelControlTuple#getChartPanelSelectionHandler()
     */
    @Override
    public ChartPanelSelectionHandler getChartPanelSelectionHandler() {
        return getControlTuple().getChartPanelSelectionHandler();
    }

    private ChartPanelControlTuple getControlTuple() {
        if (null == controlTuple) {
            controlTuple = createControl(parentComposite, SWT.H_SCROLL | SWT.V_SCROLL, SelectionType.Multi);
            configureChartPanel();
        }
        return controlTuple;
    }

    /**
     * Creates a {@link ChartPanelComposite} for this viewer.<br>
     * <b>NOTE:</b> This method should not call any other method of {@link ChartPanelContentViewer}, except for its
     * super method, override {@link #configureChartPanel(ChartPanel)} instead.
     *
     * @param parent the composite to add the {@link ChartPanelComposite} to
     * @param style the style to apply to the {@link ChartPanelComposite}
     * @param selectionType the selection type to apply to this {@link ChartPanelContentViewer}
     * @return a tuple containing both the {@link ChartPanelComposite} and the {@link Control} it is added to which may
     *     be the {@link ChartPanelComposite} itself.
     */
    protected ChartPanelControlTuple createControl(Composite parent, int style, SelectionType selectionType) {
        final ChartPanelComposite composite = new ChartPanelComposite(parent, style);
        final ChartPanelSelectionHandler selectionHandler = new ChartPanelSelectionHandler(composite.getChartPanel(),
                this, selectionType);
        return new ChartPanelControlTuple(composite, composite, selectionHandler);
    }

    /**
     * This method can be used to create a {@link JFreeChart} and {@link ChartPanel#setChart(JFreeChart) set it} on the
     * {@link #getChartPanel() chart panel}. This method is called only once, directly after the control has been
     * created by {@link #createControl(Composite, int, SelectionType)}. In the body of this method, all other methods
     * of this {@link ChartPanelContentViewer} can be called, as opposed to
     * {@link #createControl(Composite, int, SelectionType)}.
     */
    protected void configureChartPanel() {
        // Empty
    }

    protected boolean isPreserveSelection() {
        return preserveSelection;
    }

    protected void setPreserveSelection(boolean preserveSelection) {
        this.preserveSelection = preserveSelection;
    }

    @Override
    protected void inputChanged(Object input, Object oldInput) {
        refresh();
    }

    @Override
    public void refresh() {
        internalRefresh();
    }

    protected final void internalRefresh() {
        final JFreeChart chart = getChart();
        if (null == chart) {
            // No chart, no refresh
            return;
        }
        // disable notification during update
        getChartPanelComposite().setNotify(false);
        try {
            ISelection selection = StructuredSelection.EMPTY;
            if (preserveSelection) {
                selection = getSelection();
            }

            synchronized (chart) {
                refreshChart();
            }

            setSelection(selection);
        } finally {
            // enable notification again
            getChartPanelComposite().setNotify(true);
        }
    }

    /**
     * Refreshes this viewer.
     *
     * <p>
     * This method is internal to the framework; subclasses should not call this method directly, use {@link #refresh()}
     * instead.
     * </p>
     */
    protected abstract void refreshChart();

    @Override
    public ISelection getSelection() {
        return selection;
    }

    @Override
    public void setSelection(ISelection selection, boolean reveal) {
        this.selection = selection;
        fireSelectionChanged(new SelectionChangedEvent(this, selection));
    }

    protected final class ChartPanelControlTuple {
        private final Control control;

        private final ChartPanelComposite chartPanelComposite;

        private final ChartPanelSelectionHandler chartPanelSelectionHandler;

        /**
         * @param control the SWT control which displays this viewer's content
         * @param chartPanelComposite the SWT composite which displays this viewer's {@link JFreeChart}
         * @param chartPanelSelectionHandler the selection handler to use for this viewer
         */
        public ChartPanelControlTuple(Control control, ChartPanelComposite chartPanelComposite,
                ChartPanelSelectionHandler chartPanelSelectionHandler)
        {
            Args.nullNotPermitted(control, "control");
            Args.nullNotPermitted(chartPanelComposite, "chartPanelComposite");
            Args.nullNotPermitted(chartPanelSelectionHandler, "chartPanelSelectionHandler");
            this.control = control;
            this.chartPanelComposite = chartPanelComposite;
            this.chartPanelSelectionHandler = chartPanelSelectionHandler;
        }

        /**
         * Returns the primary control associated with this viewer.
         *
         * @return the SWT control which displays this viewer's content
         * @see ContentViewer#getControl()
         */
        public Control getControl() {
            return control;
        }

        /**
         * Returns the chart-panel composite associated with this viewer.
         *
         * @return the SWT composite which displays this viewer's {@link JFreeChart}
         */
        public ChartPanelComposite getChartPanelComposite() {
            return chartPanelComposite;
        }

        /**
         * Returns the selection handler for this viewer
         *
         * @return the selection handler for this viewer
         */
        public ChartPanelSelectionHandler getChartPanelSelectionHandler() {
            return chartPanelSelectionHandler;
        }

        /**
         * By default a {@link ChartPanelContentViewer}'s control is expected to be the
         * {@link #getChartPanelComposite()}. If additional layers of {@link Composite}s are added, this control tuple
         * should be wrapped with the root of that composite tree.
         *
         * @param control the SWT control which displays this viewer's content, see
         *     {@link ChartPanelContentViewer#getControl()}.
         * @return an updated control tuple to be returned by
         *     {@link ChartPanelContentViewer#createControl(Composite, int, SelectionType)}.
         */
        public ChartPanelControlTuple wrapControl(Control control) {
            return new ChartPanelControlTuple(control, chartPanelComposite, chartPanelSelectionHandler);
        }
    }
}
