/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.ui.data;

import org.eclipse.trace4cps.common.jfreechart.data.xy.XYLabeledIntervalDataItem;
import org.eclipse.trace4cps.common.jfreechart.ui.viewers.BackReferenceProvider;

public class XYBackreferenceIntervalDataItem<T> extends XYLabeledIntervalDataItem implements BackReferenceProvider<T> {
    private static final long serialVersionUID = 2278894211820843418L;

    private final transient T backReference;

    /**
     * Creates a new instance of {@code XYLabeledIntervalDataItem}.
     *
     * @param x the x-value.
     * @param xLow the lower bound of the x-interval.
     * @param xHigh the upper bound of the x-interval.
     * @param y the y-value.
     * @param yLow the lower bound of the y-interval.
     * @param yHigh the upper bound of the y-interval.
     * @param backReference the back reference, possibly {@code null}.
     */
    public XYBackreferenceIntervalDataItem(double x, double xLow, double xHigh, double y, double yLow, double yHigh,
            T backReference)
    {
        super(x, xLow, xHigh, y, yLow, yHigh);
        this.backReference = backReference;
    }

    @Override
    public T getBackReference() {
        return backReference;
    }
}
