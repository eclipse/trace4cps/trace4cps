/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.ui.theme;

import java.awt.Color;
import java.awt.Font;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.util.Collection;

import org.eclipse.jface.resource.FontRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Display;
import org.eclipse.trace4cps.common.jfreechart.chart.axis.SectionAxis;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.annotations.XYAnnotation;
import org.jfree.chart.annotations.XYMeasurementAnnotation;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelClip;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.plot.DefaultDrawingSupplier;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.plot.PieLabelLinkStyle;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.AbstractRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.chart.title.Title;
import org.jfree.chart.ui.Layer;
import org.jfree.chart.ui.RectangleInsets;
import org.jfree.chart.ui.TextAnchor;
import org.jfree.chart.util.DefaultShadowGenerator;

public class DefaultChartTheme extends StandardChartTheme {
    private static final long serialVersionUID = -6128499423748683998L;

    public DefaultChartTheme() {
        this(DefaultChartTheme.class.getName());
    }

    protected DefaultChartTheme(String name) {
        this(name, false);
    }

    protected DefaultChartTheme(String name, boolean shadow) {
        super(name, shadow);
        setDrawingSupplier(new DefaultDrawingSupplier());
        setChartBackgroundPaint(Color.WHITE);
        setPlotBackgroundPaint(new Color(240, 240, 245));
        setGridBandPaint(new Color(200, 200, 205, 64));
        setPlotOutlinePaint(new Color(100, 100, 100));
        setDomainGridlinePaint(new Color(160, 160, 160));
        setRangeGridlinePaint(Color.WHITE);
        setBarPainter(new StandardBarPainter());
        setXYBarPainter(new StandardXYBarPainter());
        setShadowVisible(false);

        if (PlatformUI.isWorkbenchRunning()) {
            IWorkbench workbench = PlatformUI.getWorkbench();
            Display display = workbench.getDisplay();
            FontRegistry fontRegistry = workbench.getThemeManager().getCurrentTheme().getFontRegistry();

            // JFace doesn't define 'small' font, so derive it from the regular font
            Font regularFont = toAwtFont(display, fontRegistry.getFontData(JFaceResources.DIALOG_FONT)[0], true);
            Font smallFont = regularFont.deriveFont(Math.max(regularFont.getSize() - 1.0f, 1));

            setExtraLargeFont(toAwtFont(display, fontRegistry.getFontData(JFaceResources.HEADER_FONT)[0], true));
            setLargeFont(toAwtFont(display, fontRegistry.getFontData(JFaceResources.BANNER_FONT)[0], true));
            setRegularFont(regularFont);
            setSmallFont(smallFont);
        } else {
            setExtraLargeFont(new Font("Segoe UI", Font.BOLD, 16));
            setLargeFont(new Font("Segoe UI", Font.BOLD, 13));
            setRegularFont(new Font("Segoe UI", Font.PLAIN, 11));
            setSmallFont(new Font("Segoe UI", Font.PLAIN, 10));
        }
    }

    @Override
    protected void applyToPlot(Plot plot) {
        super.applyToPlot(plot);
        if (null == plot.getNoDataMessage()) {
            // Only set message when developer didn't set it yet
            plot.setNoDataMessage("No data available");
        }
    }

    @Override
    @SuppressWarnings("rawtypes")
    protected void applyToPiePlot(PiePlot plot) {
        super.applyToPiePlot(plot);

        plot.setBackgroundPaint(Color.WHITE);
        plot.setDefaultSectionOutlinePaint(Color.WHITE);
        plot.setOutlineVisible(false);
        plot.setCircular(true);
        plot.setLabelLinkStyle(PieLabelLinkStyle.STANDARD);
        plot.setLabelBackgroundPaint(new Color(255, 252, 216));
        plot.setLabelOutlinePaint(new Color(255, 252, 216).darker());
        plot.setShadowGenerator(new DefaultShadowGenerator());
    }

    @Override
    protected void applyToXYPlot(XYPlot plot) {
        super.applyToXYPlot(plot);

        for (int i = 0; i < plot.getRendererCount(); i++) {
            applyToMarkers(plot.getRangeMarkers(i, Layer.BACKGROUND));
            applyToMarkers(plot.getRangeMarkers(i, Layer.FOREGROUND));
            applyToMarkers(plot.getDomainMarkers(i, Layer.BACKGROUND));
            applyToMarkers(plot.getDomainMarkers(i, Layer.FOREGROUND));
        }
    }

    private void applyToMarkers(Collection<Marker> markers) {
        if (null == markers) {
            return;
        }
        markers.forEach(this::applyToMarker);
    }

    protected void applyToMarker(Marker marker) {
        marker.setLabelFont(getSmallFont());
    }

    @Override
    protected void applyToAbstractRenderer(AbstractRenderer renderer) {
        super.applyToAbstractRenderer(renderer);

        renderer.setDataBoundsIncludesVisibleSeriesOnly(false);
        renderer.setItemLabelInsets(new RectangleInsets(1, 5, 1, 5));
        renderer.setDefaultPositiveItemLabelPosition(
                new ItemLabelPosition(ItemLabelAnchor.CENTER, TextAnchor.CENTER, ItemLabelClip.FIT));
    }

    @Override
    protected void applyToXYItemRenderer(XYItemRenderer renderer) {
        super.applyToXYItemRenderer(renderer);

        if (renderer instanceof XYBarRenderer) {
            applyToXYBarRenderer((XYBarRenderer)renderer);
        }
    }

    protected void applyToXYBarRenderer(XYBarRenderer renderer) {
        renderer.setDrawBarOutline(true);
        if (renderer.getAutoPopulateSeriesPaint()) {
            renderer.setAutoPopulateSeriesOutlinePaint(true);
            renderer.setComputeItemLabelContrastColor(true);
        }

        // Prevent to draw label in small bars
        final Font font = renderer.getDefaultItemLabelFont();
        final FontRenderContext fontRenderContext = new FontRenderContext(font.getTransform(), true, true);
        final Rectangle2D minimumLabelBounds = font.getStringBounds("MMM", fontRenderContext);
        renderer.setMinimumLabelSize(minimumLabelBounds.getBounds().getSize());
        // Draw the labels in the visible part of the bar (show as much as possible)
        renderer.setShowLabelInsideVisibleBar(true);
        // If label does not fit draw it to the left of the bar and clip the label
        renderer.setPositiveItemLabelPositionFallback(
                new ItemLabelPosition(ItemLabelAnchor.INSIDE9, TextAnchor.CENTER_LEFT, ItemLabelClip.CLIP));
    }

    @Override
    protected void applyToXYAnnotation(XYAnnotation annotation) {
        super.applyToXYAnnotation(annotation);
        if (annotation instanceof XYMeasurementAnnotation) {
            applyToXYMeasurementAnnotation((XYMeasurementAnnotation)annotation);
        }
    }

    protected void applyToXYMeasurementAnnotation(XYMeasurementAnnotation annotation) {
        annotation.setFont(getRegularFont());
        annotation.setPaint(Color.RED);
    }

    @Override
    protected void applyToValueAxis(ValueAxis axis) {
        super.applyToValueAxis(axis);
        if (axis instanceof SectionAxis) {
            applyToSectionsAxis((SectionAxis)axis);
        }
    }

    protected void applyToSectionsAxis(SectionAxis axis) {
        axis.setDefaultGridBandPaint(getGridBandPaint());
        axis.setDefaultGridBandAlternatePaint(getGridBandAlternatePaint());
        axis.setTickLabelMaxLength(40);
    }

    @Override
    protected void applyToTitle(Title title) {
        super.applyToTitle(title);

        if (title instanceof LegendTitle) {
            title.setFrame(BlockBorder.NONE);
        }
    }

    private static final String AZ = "ABCpqr";

    private static final FontRenderContext DEFAULT_FONT_RENDER_CONTEXT = new FontRenderContext(null, true, true);

    /**
     * Create an awt font by converting as much information as possible from the provided swt <code>FontData</code>.
     *
     * <p>
     * Generally speaking, given a font size, an swt font will display differently on the screen than the corresponding
     * awt one. Because the SWT toolkit use native graphical ressources whenever it is possible, this fact is platform
     * dependent. To address this issue, it is possible to enforce the method to return an awt font with the same height
     * as the swt one.
     * </p>
     *
     * @param device The swt device being drawn on (display or gc device).
     * @param fontData The swt font to convert.
     * @param ensureSameSize A boolean used to enforce the same size (in pixels) between the swt font and the newly
     *     created awt font.
     * @return An awt font converted from the provided swt font.
     * @see org.jfree.experimental.swt.SwtUtils#toAwtFont
     */
    public static Font toAwtFont(Device device, FontData fontData, boolean ensureSameSize) {
        Font font = new Font(fontData.getName(), fontData.getStyle(), fontData.getHeight());
        if (ensureSameSize) {
            GC tmpGC = new GC(device);
            org.eclipse.swt.graphics.Font tmpFont = new org.eclipse.swt.graphics.Font(device, fontData);
            tmpGC.setFont(tmpFont);

            int swtHeight = tmpGC.textExtent(AZ).y;
            float awtHeight = (float)font.getStringBounds(AZ, DEFAULT_FONT_RENDER_CONTEXT).getHeight();
            float scaledSize = (swtHeight * fontData.getHeight()) / awtHeight;
            font = font.deriveFont(scaledSize);

            tmpFont.dispose();
            tmpGC.dispose();
        }
        return font;
    }
}
