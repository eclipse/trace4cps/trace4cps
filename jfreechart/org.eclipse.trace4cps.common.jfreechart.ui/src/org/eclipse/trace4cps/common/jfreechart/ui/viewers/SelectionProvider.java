/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.ui.viewers;

import org.eclipse.core.runtime.ListenerList;
import org.eclipse.jface.util.SafeRunnable;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.jfree.chart.util.Args;

/**
 * Convenience class to keep track of an {@link ISelection}
 */
public class SelectionProvider implements ISelectionProvider {
    private final ListenerList<ISelectionChangedListener> selectionChangedListeners = new ListenerList<>();

    private final ISelection emptySelection;

    private ISelection selection;

    public SelectionProvider() {
        this(StructuredSelection.EMPTY);
    }

    /**
     * Use this constructor if your selection provider requires another type than {@link IStructuredSelection} for its
     * empty selection.
     *
     * @param emptySelection An {@link ISelection#isEmpty() empty} selection that is used to {@link #clearSelection()}.
     * @throws IllegalArgumentException if {@link ISelection#isEmpty() emptySelection.isEmpty()} returns {@code false}.
     */
    public SelectionProvider(ISelection emptySelection) {
        Args.nullNotPermitted(emptySelection, "emptySelection");
        if (!emptySelection.isEmpty()) {
            throw new IllegalArgumentException("emptySelection should return true on isEmpty()");
        }
        this.emptySelection = emptySelection;
        this.selection = emptySelection;
    }

    public void clearSelection() {
        setSelection(emptySelection);
    }

    @Override
    public void setSelection(ISelection selection) {
        this.selection = selection == null ? emptySelection : selection;
        fireSelectionChanged(new SelectionChangedEvent(this, this.selection));
    }

    @Override
    public ISelection getSelection() {
        return selection;
    }

    @Override
    public void addSelectionChangedListener(ISelectionChangedListener listener) {
        selectionChangedListeners.add(listener);
    }

    @Override
    public void removeSelectionChangedListener(ISelectionChangedListener listener) {
        selectionChangedListeners.remove(listener);
    }

    /**
     * Notifies any selection changed listeners that the viewer's selection has changed. Only listeners registered at
     * the time this method is called are notified.
     *
     * @param event a selection changed event
     *
     * @see ISelectionChangedListener#selectionChanged
     */
    protected void fireSelectionChanged(final SelectionChangedEvent event) {
        for (ISelectionChangedListener l: selectionChangedListeners) {
            SafeRunnable.run(new SafeRunnable() {
                @Override
                public void run() {
                    l.selectionChanged(event);
                }
            });
        }
    }
}
