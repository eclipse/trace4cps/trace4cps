/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.ui.gantt;

public interface XYGanttMeasurement<T> {
    public enum Snap {
        START, END
    }

    /**
     * We do not assume measurements going from left to right or low to high, etc. We only know the first and second
     * item which where selected for this measurement, hence the name of this method.
     *
     * @return a reference to the first measurement item
     */
    T getBackReference1();

    /**
     * @return if the measurement is either from/to the start or the end of the first measurement item.
     * @see #getBackReference1()
     */
    Snap getSnap1();

    /**
     * @return the time of the first selection.
     */
    Number getTime1();

    /**
     * We do not assume measurements going from left to right or low to high, etc. We only know the first and second
     * item which where selected for this measurement, hence the name of this method.
     *
     * @return a reference to the second measurement item
     */
    T getBackReference2();

    /**
     * @return if the measurement is either from/to the start or the end of the second measurement item.
     * @see #getBackReference2()
     */
    Snap getSnap2();

    /**
     * @return the time of the second selection.
     */
    Number getTime2();

    /**
     * @return the absolute duration between the two measurement points.
     */
    Number getDuration();

    /**
     * @return the text that describes this measurement
     */
    default String getText() {
        return String.valueOf(getDuration());
    }
}
