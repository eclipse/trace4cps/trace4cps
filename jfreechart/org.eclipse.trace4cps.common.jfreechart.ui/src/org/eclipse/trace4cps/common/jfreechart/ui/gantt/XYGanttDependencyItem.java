/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.ui.gantt;

import org.eclipse.trace4cps.common.jfreechart.data.xy.XYEdgeDataItem;
import org.eclipse.trace4cps.common.jfreechart.ui.viewers.BackReferenceProvider;

public class XYGanttDependencyItem<T> extends XYEdgeDataItem implements BackReferenceProvider<T> {
    private static final long serialVersionUID = -2205873637869057094L;

    public enum DependencyType {
        START_START(true, true), START_END(true, false), END_START(false, true), END_END(false, false);

        private final boolean fromStart;

        private final boolean toStart;

        private DependencyType(boolean fromStart, boolean toStart) {
            this.fromStart = fromStart;
            this.toStart = toStart;
        }

        public boolean isFromStart() {
            return fromStart;
        }

        public boolean isToStart() {
            return toStart;
        }
    }

    private final transient T backReference;

    public XYGanttDependencyItem(XYGanttDataItem<?> from, XYGanttDataItem<?> to, DependencyType type) {
        this(from, to, type, null);
    }

    public XYGanttDependencyItem(XYGanttDataItem<?> from, XYGanttDataItem<?> to, DependencyType type, T backReference) {
        super(type.isFromStart() ? from.getXLowValue() : from.getXHighValue(), from.getYValue(),
                type.isToStart() ? to.getXLowValue() : to.getXHighValue(), to.getYValue());
        this.backReference = backReference;
    }

    @Override
    public T getBackReference() {
        return backReference;
    }
}
