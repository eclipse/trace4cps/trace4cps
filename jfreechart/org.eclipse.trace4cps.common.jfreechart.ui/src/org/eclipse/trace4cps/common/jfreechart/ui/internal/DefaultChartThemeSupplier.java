/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.ui.internal;

import org.eclipse.trace4cps.common.jfreechart.ui.theme.ChartThemeSupplier;
import org.eclipse.trace4cps.common.jfreechart.ui.theme.DefaultChartTheme;
import org.jfree.chart.ChartTheme;

public class DefaultChartThemeSupplier implements ChartThemeSupplier {
    @Override
    public ChartTheme getChartTheme() {
        return new DefaultChartTheme();
    }
}
