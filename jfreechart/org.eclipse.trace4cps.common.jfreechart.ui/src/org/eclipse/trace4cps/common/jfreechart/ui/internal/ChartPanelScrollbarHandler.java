/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.ui.internal;

import static org.eclipse.core.runtime.IStatus.WARNING;
import static org.eclipse.trace4cps.common.jfreechart.ui.JFreeChartUIPlugin.PLUGIN_ID;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Map;
import java.util.Objects;
import java.util.WeakHashMap;
import java.util.function.Supplier;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.trace4cps.common.jfreechart.ui.JFreeChartUIPlugin;
import org.eclipse.trace4cps.common.jfreechart.ui.widgets.ChartPanelComposite;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.event.PlotChangeEvent;
import org.jfree.chart.event.PlotChangeListener;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.Range;

public class ChartPanelScrollbarHandler extends SelectionAdapter implements PropertyChangeListener, PlotChangeListener {

    // Numbers bigger than this lead to unwanted scrollbar behavior
    // i.e. sometimes the scroll direction changes when using the mouse wheel
    private static final int SCROLLBAR_LENGTH = 50_000_000;

    private final Map<ValueAxis, Range> axesAutoRanges = new WeakHashMap<>();

    private final ChartPanelComposite chartPanelComposite;

    private double incrementFactor = 0.1;

    private double pageIncrementFactor = 0.9;

    private XYPlot currentPlot;

    private Range lastDomainRange;

    private Range lastRangeRange;

    public ChartPanelScrollbarHandler(ChartPanelComposite composite) {
        chartPanelComposite = composite;

        boolean scroll = false;
        if (null != chartPanelComposite.getHorizontalBar()) {
            chartPanelComposite.getHorizontalBar().addSelectionListener(this);
            scroll = true;
        }
        if (null != chartPanelComposite.getVerticalBar()) {
            chartPanelComposite.getVerticalBar().addSelectionListener(this);
            scroll = true;
        }
        if (scroll) {
            // No need to add listeners if not scrolling anyway
            setChart(chartPanelComposite.getChartPanel().getChart());
            chartPanelComposite.getChartPanel().addPropertyChangeListener(ChartPanel.PROPERTY_CHART, this);
        }
    }

    public void dispose() {
        boolean scroll = false;
        if (null != chartPanelComposite.getHorizontalBar()) {
            chartPanelComposite.getHorizontalBar().removeSelectionListener(this);
            scroll = true;
        }
        if (null != chartPanelComposite.getVerticalBar()) {
            chartPanelComposite.getVerticalBar().removeSelectionListener(this);
            scroll = true;
        }
        if (scroll) {
            setChart(null);
            chartPanelComposite.getChartPanel().removePropertyChangeListener(ChartPanel.PROPERTY_CHART, this);
        }
    }

    public double getIncrementFactor() {
        return incrementFactor;
    }

    public void setIncrementFactor(double incrementFactor) {
        this.incrementFactor = incrementFactor;
    }

    public double getPageIncrementFactor() {
        return pageIncrementFactor;
    }

    public void setPageIncrementFactor(double pageIncrementFactor) {
        this.pageIncrementFactor = pageIncrementFactor;
    }

    @Override
    public void propertyChange(PropertyChangeEvent event) {
        if (ChartPanel.PROPERTY_CHART.equals(event.getPropertyName())) {
            setChart((JFreeChart)event.getNewValue());
        }
    }

    private void setChart(JFreeChart chart) {
        if (null != currentPlot) {
            currentPlot.removeChangeListener(this);
        }
        if (null != chart && chart.getPlot() instanceof XYPlot) {
            currentPlot = (XYPlot)chart.getPlot();
            currentPlot.addChangeListener(this);
        } else {
            currentPlot = null;
        }
        currentPlotChanged();
    }

    @Override
    public void plotChanged(PlotChangeEvent event) {
        if (event.getPlot() == currentPlot) {
            currentPlotChanged();
        }
    }

    private void currentPlotChanged() {
        boolean autoRangesChanged = cacheAutoRanges();

        ValueAxis domainAxis = null == currentPlot ? null : currentPlot.getDomainAxis();
        if (null != domainAxis) {
            final Range domainRange = domainAxis.getRange();
            if (autoRangesChanged || !Objects.equals(domainRange, lastDomainRange)) {
                lastDomainRange = domainRange;
                final Range domainAutoRange = axesAutoRanges.get(domainAxis);
                if (null != domainAutoRange) {
                    updateScrollBar(this::getDomainBar, domainRange, domainAutoRange);
                } else {
                    IStatus status = new Status(WARNING, PLUGIN_ID,
                            "Skipped update: Unknown auto range for domain axis");
                    JFreeChartUIPlugin.getDefault().getLog().log(status);
                }
            }
        } else {
            disableScrollBar(this::getDomainBar);
        }

        ValueAxis rangeAxis = null == currentPlot ? null : currentPlot.getRangeAxis();
        if (null != rangeAxis) {
            final Range rangeRange = rangeAxis.getRange();
            if (autoRangesChanged || !Objects.equals(rangeRange, lastRangeRange)) {
                lastRangeRange = rangeRange;
                final Range rangeAutoRange = axesAutoRanges.get(rangeAxis);
                if (null != rangeAutoRange) {
                    updateScrollBar(this::getRangeBar, rangeRange, rangeAutoRange);
                } else {
                    IStatus status = new Status(WARNING, PLUGIN_ID,
                            "Skipped update: Unknown auto range for range axis");
                    JFreeChartUIPlugin.getDefault().getLog().log(status);
                }
            }
        } else {
            disableScrollBar(this::getRangeBar);
        }
    }

    private void disableScrollBar(final Supplier<ScrollBar> scrollBarSupplier) {
        chartPanelComposite.getDisplay().asyncExec(() -> {
            ScrollBar scrollBar = scrollBarSupplier.get();
            if (scrollBar != null && !scrollBar.isDisposed() && scrollBar.isEnabled()) {
                scrollBar.setEnabled(false);
            }
        });
    }

    private void updateScrollBar(final Supplier<ScrollBar> scrollBarSupplier, Range viewRange, Range autoRange) {
        chartPanelComposite.getDisplay().asyncExec(() -> {
            ScrollBar scrollBar = scrollBarSupplier.get();
            if (null == scrollBar || scrollBar.isDisposed()) {
                return;
            }

            double thumbPercentage = viewRange.getLength() / autoRange.getLength();
            double thumb = thumbPercentage * SCROLLBAR_LENGTH;
            boolean enabled = thumb >= 1 && thumb < SCROLLBAR_LENGTH;
            scrollBar.setEnabled(enabled);
            if (!enabled) {
                return;
            }

            double increment = Math.max(thumb * incrementFactor, 1);
            double pageIncrement = Math.max(thumb * pageIncrementFactor, 1);
            double selectionPercentage = (viewRange.getLowerBound() - autoRange.getLowerBound())
                    / autoRange.getLength();
            double selection = selectionPercentage * SCROLLBAR_LENGTH;
            if (selection < 0) {
                selection = 0;
            } else if (selection > SCROLLBAR_LENGTH) {
                selection = SCROLLBAR_LENGTH;
            }
            if ((scrollBar.getStyle() & SWT.VERTICAL) != 0) {
                // Invert value
                selection = SCROLLBAR_LENGTH - selection - thumb;
            }

            scrollBar.setValues(round(selection), 0, SCROLLBAR_LENGTH, round(thumb), round(increment),
                    round(pageIncrement));
        });
    }

    private int round(double value) {
        final Long longValue = Math.round(value);
        return longValue.intValue();
    }

    @Override
    public void widgetSelected(SelectionEvent event) {
        if (null == currentPlot || !currentPlot.isNotify()) {
            return;
        }

        final Object source = event.getSource();
        if (source == getDomainBar()) {
            for (int i = 0; i < currentPlot.getDomainAxisCount(); i++) {
                final ValueAxis domainAxis = currentPlot.getDomainAxis(i);
                if (domainAxis != null) {
                    Range axisRange = calculateAxisRange((ScrollBar)source, domainAxis);
                    if (i == 0) {
                        lastDomainRange = axisRange;
                    }
                    if (!Objects.equals(domainAxis.getRange(), axisRange)) {
                        domainAxis.setRange(axisRange, true, true);
                    }
                }
            }
        } else if (source == getRangeBar()) {
            for (int i = 0; i < currentPlot.getRangeAxisCount(); i++) {
                final ValueAxis rangeAxis = currentPlot.getRangeAxis(i);
                if (rangeAxis != null) {
                    Range axisRange = calculateAxisRange((ScrollBar)source, rangeAxis);
                    if (i == 0) {
                        lastRangeRange = axisRange;
                    }
                    if (!Objects.equals(rangeAxis.getRange(), axisRange)) {
                        rangeAxis.setRange(axisRange, true, true);
                    }
                }
            }
        }
        // Maybe an old scroll bar? Not interested
    }

    private Range calculateAxisRange(ScrollBar scrollBar, ValueAxis axis) {
        if (null == scrollBar || scrollBar.isDisposed()) {
            return axis.getRange();
        }
        Range autoRange = axesAutoRanges.get(axis);
        if (null == autoRange) {
            IStatus status = new Status(WARNING, PLUGIN_ID,
                    String.format("Skipped update: Unknown auto range for axis: %s (type: %s)", axis.getLabel(),
                            axis.getClass().getName()));
            JFreeChartUIPlugin.getDefault().getLog().log(status);
            return axis.getRange();
        }

        double length = scrollBar.getMaximum();
        double selection = scrollBar.getSelection();
        double thumb = scrollBar.getThumb();
        if ((scrollBar.getStyle() & SWT.VERTICAL) != 0) {
            // Invert value
            selection = length - (selection + thumb);
        }

        double selectionPercentage = selection / length;
        double thumbPercentage = thumb / length;
        double lower = autoRange.getLowerBound() + (selectionPercentage * autoRange.getLength());
        double upper = lower + (thumbPercentage * autoRange.getLength());
        return new Range(lower, upper);
    }

    /**
     * @return true if one of the auto ranges changed
     */
    protected boolean cacheAutoRanges() {
        if (null == currentPlot) {
            boolean changed = !axesAutoRanges.isEmpty();
            axesAutoRanges.clear();
            return changed;
        } else {
            boolean changed = false;
            for (int i = 0; i < currentPlot.getRangeAxisCount(); i++) {
                if (cacheAutoRange(currentPlot.getRangeAxis(i))) {
                    changed = true;
                }
            }
            for (int i = 0; i < currentPlot.getDomainAxisCount(); i++) {
                if (cacheAutoRange(currentPlot.getDomainAxis(i))) {
                    changed = true;
                }
            }
            return changed;
        }
    }

    /**
     * @param axis the axis for which its auto-range should be cached
     * @return true if the auto range changed
     * @see ValueAxis#calculateAutoRange(boolean)
     */
    protected boolean cacheAutoRange(ValueAxis axis) {
        Range newRange = null == axis ? null : axis.calculateAutoRange(false);
        Range oldRange = null == newRange ? axesAutoRanges.remove(axis) : axesAutoRanges.put(axis, newRange);
        return !Objects.equals(oldRange, newRange);
    }

    protected ScrollBar getDomainBar() {
        if (null == currentPlot || chartPanelComposite.isDisposed()) {
            return null;
        }
        return currentPlot.getOrientation() == PlotOrientation.VERTICAL ? chartPanelComposite.getHorizontalBar()
                : chartPanelComposite.getVerticalBar();
    }

    protected ScrollBar getRangeBar() {
        if (null == currentPlot || chartPanelComposite.isDisposed()) {
            return null;
        }
        return currentPlot.getOrientation() == PlotOrientation.VERTICAL ? chartPanelComposite.getVerticalBar()
                : chartPanelComposite.getHorizontalBar();
    }
}
