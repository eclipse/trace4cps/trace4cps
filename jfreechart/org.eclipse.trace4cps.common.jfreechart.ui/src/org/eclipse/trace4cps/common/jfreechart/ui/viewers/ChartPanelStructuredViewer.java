/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.ui.viewers;

import java.util.Collections;
import java.util.List;

import org.eclipse.jface.viewers.ContentViewer;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.trace4cps.common.jfreechart.ui.viewers.ChartPanelSelectionHandler.SelectionType;
import org.eclipse.trace4cps.common.jfreechart.ui.widgets.ChartPanelComposite;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.event.PlotChangeEvent;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.util.Args;

public abstract class ChartPanelStructuredViewer extends StructuredViewer implements ChartPanelViewer {
    private final Composite parentComposite;

    @SuppressWarnings("rawtypes")
    private List widgetSelection = Collections.EMPTY_LIST;

    private ChartPanelControlTuple controlTuple = null;

    public ChartPanelStructuredViewer(Composite parent) {
        parentComposite = parent;
    }

    @Override
    public Control getControl() {
        return getControlTuple().getControl();
    }

    /**
     * @see ChartPanelControlTuple#getChartPanelComposite()
     */
    @Override
    public ChartPanelComposite getChartPanelComposite() {
        return getControlTuple().getChartPanelComposite();
    }

    /**
     * @see ChartPanelControlTuple#getChartPanelSelectionHandler()
     */
    @Override
    public ChartPanelSelectionHandler getChartPanelSelectionHandler() {
        return getControlTuple().getChartPanelSelectionHandler();
    }

    private ChartPanelControlTuple getControlTuple() {
        if (null == controlTuple) {
            controlTuple = createControl(parentComposite, SWT.H_SCROLL | SWT.V_SCROLL, SelectionType.Multi);
            configureChartPanel();
        }
        return controlTuple;
    }

    /**
     * Creates a {@link ChartPanelComposite} for this viewer.<br>
     * <b>NOTE:</b> This method should not call any other method of {@link ChartPanelStructuredViewer}, except for its
     * super method, override {@link #configureChartPanel(ChartPanel)} instead.
     *
     * @param parent the composite to add the {@link ChartPanelComposite} to
     * @param style the style to apply to the {@link ChartPanelComposite}
     * @param selectionType the selection type to apply to this {@link ChartPanelStructuredViewer}
     * @return a tuple containing both the {@link ChartPanelComposite} and the {@link Control} it is added to which may
     *     be the {@link ChartPanelComposite} itself.
     */
    protected ChartPanelControlTuple createControl(Composite parent, int style, SelectionType selectionType) {
        final ChartPanelComposite composite = new ChartPanelComposite(parent, style);
        final ChartPanelSelectionHandler selectionHandler = new ChartPanelSelectionHandler(composite.getChartPanel(),
                this, selectionType);
        return new ChartPanelControlTuple(composite, composite, selectionHandler);
    }

    /**
     * This method can be used to create a {@link JFreeChart} and {@link ChartPanel#setChart(JFreeChart) set it} on the
     * {@link #getChartPanel() chart panel}. This method is called only once, directly after the control has been
     * created by {@link #createControl(Composite, int, SelectionType)}. In the body of this method, all other methods
     * of this {@link ChartPanelStructuredViewer} can be called, as opposed to
     * {@link #createControl(Composite, int, SelectionType)}.
     */
    protected void configureChartPanel() {
        // Empty
    }

    @Override
    protected void inputChanged(Object input, Object oldInput) {
        refresh();
    }

    @Override
    public final void refresh(boolean updateLabels) {
        refresh();
    }

    @Override
    public final void refresh(Object element, boolean updateLabels) {
        refresh(element);
    }

    @Override
    public final void refresh() {
        refresh(getRoot());
    }

    @Override
    protected final void internalRefresh(Object element, boolean updateLabels) {
        super.internalRefresh(element, updateLabels);
    }

    @Override
    protected final void internalRefresh(Object element) {
        final JFreeChart chart = getChart();
        if (null == chart) {
            // No chart, no refresh
            return;
        }
        // disable notification during update
        getChartPanelComposite().setNotify(false);
        try {
            synchronized (chart) {
                refreshChart(element);
            }
        } finally {
            // enable notification again
            getChartPanelComposite().setNotify(true);
        }
    }

    /**
     * Refreshes this viewer starting at the given element.
     *
     * <p>
     * This method is internal to the framework; subclasses should not call this method directly, use {@link #refresh()}
     * instead.
     * </p>
     *
     * @param element the element
     */
    protected abstract void refreshChart(Object element);

    @Override
    @SuppressWarnings("rawtypes")
    protected List getSelectionFromWidget() {
        return widgetSelection;
    }

    @Override
    @SuppressWarnings("rawtypes")
    protected void setSelectionToWidget(List selection, boolean reveal) {
        widgetSelection = selection;
    }

    @Override
    protected Widget doFindInputItem(Object element) {
        return getChartPanelComposite();
    }

    @Override
    protected Widget doFindItem(Object element) {
        return doFindInputItem(element);
    }

    @Override
    protected void doUpdateItem(Widget item, Object element, boolean fullMap) {
        if (item instanceof ChartPanelComposite) {
            final Plot plot = ((ChartPanelComposite)item).getPlot();
            if (plot != null) {
                // Causes plot to be re-rendered
                plot.notifyListeners(new PlotChangeEvent(plot));
            }
        }
    }

    @Override
    public void reveal(Object element) {
        // Empty, subclasses may override
    }

    protected final class ChartPanelControlTuple {
        private final Control control;

        private final ChartPanelComposite chartPanelComposite;

        private final ChartPanelSelectionHandler chartPanelSelectionHandler;

        /**
         * @param control the SWT control which displays this viewer's content
         * @param chartPanelComposite the SWT composite which displays this viewer's {@link JFreeChart}
         * @param chartPanelSelectionHandler the selection handler to use for this viewer
         */
        public ChartPanelControlTuple(Control control, ChartPanelComposite chartPanelComposite,
                ChartPanelSelectionHandler chartPanelSelectionHandler)
        {
            Args.nullNotPermitted(control, "control");
            Args.nullNotPermitted(chartPanelComposite, "chartPanelComposite");
            Args.nullNotPermitted(chartPanelSelectionHandler, "chartPanelSelectionHandler");
            this.control = control;
            this.chartPanelComposite = chartPanelComposite;
            this.chartPanelSelectionHandler = chartPanelSelectionHandler;
        }

        /**
         * Returns the primary control associated with this viewer.
         *
         * @return the SWT control which displays this viewer's content
         * @see ContentViewer#getControl()
         */
        public Control getControl() {
            return control;
        }

        /**
         * Returns the chart-panel composite associated with this viewer.
         *
         * @return the SWT composite which displays this viewer's {@link JFreeChart}
         */
        public ChartPanelComposite getChartPanelComposite() {
            return chartPanelComposite;
        }

        /**
         * Returns the selection handler for this viewer
         *
         * @return the selection handler for this viewer
         */
        public ChartPanelSelectionHandler getChartPanelSelectionHandler() {
            return chartPanelSelectionHandler;
        }

        /**
         * By default a {@link ChartPanelStructuredViewer}'s control is expected to be the
         * {@link #getChartPanelComposite()}. If additional layers of {@link Composite}s are added, this control tuple
         * should be wrapped with the root of that composite tree.
         *
         * @param control the SWT control which displays this viewer's content, see
         *     {@link ChartPanelStructuredViewer#getControl()}.
         * @return an updated control tuple to be returned by
         *     {@link ChartPanelStructuredViewer#createControl(Composite, int, SelectionType)}.
         */
        public ChartPanelControlTuple wrapControl(Control control) {
            return new ChartPanelControlTuple(control, chartPanelComposite, chartPanelSelectionHandler);
        }
    }
}
