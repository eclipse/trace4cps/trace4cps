/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.common.jfreechart.ui.viewers;

import org.eclipse.jface.viewers.IInputSelectionProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.widgets.Control;
import org.eclipse.trace4cps.common.jfreechart.ui.widgets.ChartPanelComposite;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.XYPlot;

/**
 * Common interface for all {@link ChartPanel} viewers.<br>
 * Implementers of this class are expected to extend {@link Viewer}.
 */
public interface ChartPanelViewer extends IInputSelectionProvider {
    /**
     * Returns the control that contains the {@link ChartPanel}.
     *
     * @return the control that contains the {@link ChartPanel}.
     */
    ChartPanelComposite getChartPanelComposite();

    /**
     * @see ChartPanelComposite#getXYPlot()
     */
    default XYPlot getXYPlot() throws ClassCastException {
        return getChartPanelComposite().getXYPlot();
    }

    /**
     * @see ChartPanelComposite#getPlot()
     */
    default Plot getPlot() {
        return getChartPanelComposite().getPlot();
    }

    /**
     * @see ChartPanelComposite#getChart()
     */
    default JFreeChart getChart() {
        return getChartPanelComposite().getChart();
    }

    /**
     * @see ChartPanelComposite#getChartPanel()
     */
    default ChartPanel getChartPanel() {
        return getChartPanelComposite().getChartPanel();
    }

    /**
     * Returns the selection handler for this viewer
     *
     * @return the selection handler for this viewer
     */
    ChartPanelSelectionHandler getChartPanelSelectionHandler();

    /**
     * Returns the primary control associated with this viewer.
     *
     * @return the SWT control which displays this viewer's content.
     */
    Control getControl();

    /**
     * Refreshes this viewer completely with information freshly obtained from this viewer's model.
     */
    void refresh();
}
