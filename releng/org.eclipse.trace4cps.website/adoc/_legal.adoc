////
  // Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
  //
  // This program and the accompanying materials are made
  // available under the terms of the Eclipse Public License 2.0
  // which is available at https://www.eclipse.org/legal/epl-2.0/
  //
  // SPDX-License-Identifier: EPL-2.0
////

include::_initCommon.adoc[]

indexterm:[legal]

[[legal]]
== Legal

The material in this documentation is Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation.

{trace} and TRACE4CPS(TM) are trademarks of the Eclipse Foundation.
Eclipse, and the Eclipse Logo are registered trademarks of the Eclipse Foundation.
Other names may be trademarks of their respective owners.

*License*

The Eclipse Foundation makes available all content in this document ("Content").
Unless otherwise indicated below, the Content is provided to you under the terms of the Eclipse Public License 2.0 which is available at https://www.eclipse.org/legal/epl-2.0/
For purposes of the EPL-2.0 License, "Software" will mean the Content.

If you did not receive this Content directly from the Eclipse Foundation, the Content is being redistributed by another party ("Redistributor") and different terms and conditions may apply to your use of any object code in the Content.
Check the Redistributor's license that was provided with the Content.
If no such license exists, contact the Redistributor.
Unless otherwise indicated below, the terms and conditions of the EPL-2.0 License still apply to any source code in the Content and such source code may be obtained at link:https://www.eclipse.org[].
