/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.vis.jfree.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.vis.jfree.TraceViewConfiguration;
import org.eclipse.trace4cps.vis.jfree.TraceViewConfiguration.DisjunctionFilter;
import org.eclipse.trace4cps.vis.jfree.TraceViewConfiguration.SimpleFilter;
import org.eclipse.trace4cps.vis.jfree.TraceViewConfigurationIO;
import org.jfree.data.Range;
import org.junit.jupiter.api.Test;

public class TraceViewConfigurationIOTest {
    @Test
    public void writeRead() throws IOException, TraceException {
        TraceViewConfiguration cfg = new TraceViewConfiguration();
        cfg.setShowDependencies(false);
        cfg.setShowSignalMarkers(true);
        cfg.setRange(new Range(100, 1000));
        DisjunctionFilter df = new DisjunctionFilter();
        df.add(new SimpleFilter("A}", Arrays.asList("v1", "{v2")));
        df.add(new SimpleFilter("B", Arrays.asList("w;1", "w2")));
        cfg.addFilter(TracePart.CLAIM, df);
        cfg.addFilter(TracePart.CLAIM, "C", Arrays.asList("x"));
        File tmp = new File("./target/viewconfig-io-test.txt");
        TraceViewConfigurationIO.toFile(cfg, tmp);

        TraceViewConfiguration cfg2 = TraceViewConfigurationIO.fromFile(tmp);
        assertEqualPersistentConfig(cfg, cfg2);
    }

    private void assertEqualPersistentConfig(TraceViewConfiguration c1, TraceViewConfiguration c2) {
        assertEquals(c1.isActivityView(), c2.isActivityView());
        assertEquals(c1.getRange(), c2.getRange());
        assertEquals(c1.showClaims(), c2.showClaims());
        assertEquals(c1.showEvents(), c2.showEvents());
        assertEquals(c1.showClaimEvents(), c2.showClaimEvents());
        assertEquals(c1.showDependencies(), c2.showDependencies());
        assertEquals(c1.showSignals(), c2.showSignals());
        assertEquals(c1.getShowSignalMarkers(), c2.getShowSignalMarkers());
        // filters
        assertEquals(c1.getFilters(TracePart.CLAIM), c2.getFilters(TracePart.CLAIM));
        assertEquals(c1.getFilters(TracePart.EVENT), c2.getFilters(TracePart.EVENT));
        assertEquals(c1.getFilters(TracePart.DEPENDENCY), c2.getFilters(TracePart.DEPENDENCY));
        assertEquals(c1.getFilters(TracePart.SIGNAL), c2.getFilters(TracePart.SIGNAL));
        // grouping
        assertEquals(c1.getGroupingAttributes(TracePart.CLAIM), c2.getGroupingAttributes(TracePart.CLAIM));
        assertEquals(c1.getGroupingAttributes(TracePart.EVENT), c2.getGroupingAttributes(TracePart.EVENT));
        assertEquals(c1.getGroupingAttributes(TracePart.DEPENDENCY), c2.getGroupingAttributes(TracePart.DEPENDENCY));
        assertEquals(c1.getGroupingAttributes(TracePart.SIGNAL), c2.getGroupingAttributes(TracePart.SIGNAL));
        // coloring
        assertEquals(c1.getColoringAttributes(TracePart.CLAIM), c2.getColoringAttributes(TracePart.CLAIM));
        assertEquals(c1.getColoringAttributes(TracePart.EVENT), c2.getColoringAttributes(TracePart.EVENT));
        assertEquals(c1.getColoringAttributes(TracePart.DEPENDENCY), c2.getColoringAttributes(TracePart.DEPENDENCY));
        assertEquals(c1.getColoringAttributes(TracePart.SIGNAL), c2.getColoringAttributes(TracePart.SIGNAL));
    }
}
