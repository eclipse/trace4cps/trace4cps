/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.vis.jfree.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.impl.Claim;
import org.eclipse.trace4cps.core.impl.Resource;
import org.eclipse.trace4cps.core.io.ParseException;
import org.eclipse.trace4cps.vis.jfree.ClaimScaling;
import org.eclipse.trace4cps.vis.jfree.impl.ClaimPartition;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PartitionTest {
    @Test
    public void testPartition_RESOURCE_ClaimDurationZero() {
        IResource r1 = new Resource(10, false);
        IResource r2 = new Resource(10, false);
        r1.setAttribute("name", "r1");
        r2.setAttribute("name", "r2");

        IClaim c1 = new Claim(0, 0, r1, 1);
        IClaim c2 = new Claim(0, 1, r2, 1);
        c1.setAttribute("name", "c1");
        c2.setAttribute("name", "c2");

        List<IClaim> claims = new ArrayList<>();
        claims.add(c1);
        claims.add(c2);

        ClaimPartition p = ClaimPartition.createResourcePartition(claims, Collections.singletonList("name"));
        Assertions.assertEquals(2, p.size());
        Assertions.assertEquals(1, p.get(0).size());
        Assertions.assertEquals(1, p.get(1).size());

        p.scaleClaims(ClaimScaling.RESOURCE_AMOUNT);
        Assertions.assertEquals(2, p.size());
        Assertions.assertEquals(1, p.get(0).size());
        Assertions.assertEquals(1, p.get(1).size());
    }

    @Test
    public void testPartition_FULL_ClaimDurationZero() {
        IResource r1 = new Resource(10, false);
        IResource r2 = new Resource(10, false);
        r1.setAttribute("name", "r1");
        r2.setAttribute("name", "r2");

        IClaim c1 = new Claim(0, 10, r1, 1);
        IClaim c2 = new Claim(5, 5, r2, 1);
        c1.setAttribute("name", "A");
        c2.setAttribute("name", "A");

        List<IClaim> claims = new ArrayList<>();
        claims.add(c1);
        claims.add(c2);

        ClaimPartition p = ClaimPartition.createActivityPartition(claims, Collections.singletonList("name"));
        Assertions.assertEquals(1, p.size());
        Assertions.assertEquals(2, p.get(0).size());

        p.scaleClaims(ClaimScaling.FULL);
        Assertions.assertEquals(1, p.size());
        Assertions.assertEquals(3, p.get(0).size());
    }

    @Test
    public void testPartitionFragment_FULL_frags() {
        IResource r1 = new Resource(10, false);
        IResource r2 = new Resource(10, false);
        r1.setAttribute("name", "r1");
        r2.setAttribute("name", "r2");

        IClaim c1 = new Claim(0, 10, r1, 1);
        IClaim c2 = new Claim(4, 7, r2, 1);
        c1.setAttribute("name", "A");
        c2.setAttribute("name", "A");

        List<IClaim> claims = new ArrayList<>();
        claims.add(c1);
        claims.add(c2);

        ClaimPartition p = ClaimPartition.createActivityPartition(claims, Collections.singletonList("name"));
        Assertions.assertEquals(1, p.size());
        Assertions.assertEquals(2, p.get(0).size());

        p.scaleClaims(ClaimScaling.FULL);
        Assertions.assertEquals(1, p.size());
        Assertions.assertEquals(4, p.get(0).size());
    }

    @Test
    public void testPartitionFragment_FULL_stacked() throws ParseException, IOException {
        IResource r1 = new Resource(10, false);
        IResource r2 = new Resource(10, false);
        r1.setAttribute("name", "r1");
        r2.setAttribute("name", "r2");

        IClaim c1 = new Claim(0, 10, r1, 1);
        c1.setAttribute("name", "A");
        IClaim c2 = new Claim(0, 10, r2, 1);
        c2.setAttribute("name", "A");

        IClaim c3 = new Claim(11, 20, r1, 1);
        c3.setAttribute("name", "B");
        IClaim c4 = new Claim(11, 20, r2, 1);
        c4.setAttribute("name", "B");

        List<IClaim> claims = new ArrayList<>();
        claims.add(c1);
        claims.add(c2);
        claims.add(c3);
        claims.add(c4);

        ClaimPartition p = ClaimPartition.createActivityPartition(claims, Collections.singletonList("name"));
        Assertions.assertEquals(2, p.size());
        Assertions.assertEquals(2, p.get(0).size());
        Assertions.assertEquals(2, p.get(1).size());

        p.scaleClaims(ClaimScaling.FULL);
        Assertions.assertEquals(2, p.size());
        Assertions.assertEquals(2, p.get(0).size());
        Assertions.assertEquals(2, p.get(1).size());
    }

    @Test
    public void testPartitionFragment_FULL_consecutive() throws ParseException, IOException {
        IResource r1 = new Resource(10, false);
        IResource r2 = new Resource(10, false);
        r1.setAttribute("name", "r1");
        r2.setAttribute("name", "r2");

        IClaim c1 = new Claim(0, 10, r1, 1);
        IClaim c2 = new Claim(10, 20, r2, 1);
        c1.setAttribute("name", "A");
        c2.setAttribute("name", "A");

        List<IClaim> claims = new ArrayList<>();
        claims.add(c1);
        claims.add(c2);

        ClaimPartition p = ClaimPartition.createActivityPartition(claims, Collections.singletonList("name"));
        Assertions.assertEquals(1, p.size());
        Assertions.assertEquals(2, p.get(0).size());

        p.scaleClaims(ClaimScaling.FULL);
        Assertions.assertEquals(1, p.size());
        Assertions.assertEquals(2, p.get(0).size());
    }
}
