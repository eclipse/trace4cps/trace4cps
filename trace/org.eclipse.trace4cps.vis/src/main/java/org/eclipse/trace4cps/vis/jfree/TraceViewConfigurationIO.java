/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.vis.jfree;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.vis.jfree.TraceViewConfiguration.DisjunctionFilter;
import org.eclipse.trace4cps.vis.jfree.TraceViewConfiguration.SimpleFilter;
import org.jfree.data.Range;

public class TraceViewConfigurationIO {
    private static final String RANGE = "range";

    private static final String SHOW_CLAIMS = "showClaims";

    private static final String SHOW_EVENTS = "showEvents";

    private static final String SHOW_CLAIM_EVENTS = "showClaimEvents";

    private static final String SHOW_DEPENDENCIES = "showDependencies";

    private static final String SHOW_SIGNALS = "showSignals";

    private static final String SHOW_SIGNAL_MARKERS = "showSignalMarkers";

    private static final String SHOW_CLAIM_LABELS = "showClaimLabels";

    private static final String ACTIVITY_VIEW = "activityView";

    private static final String CLAIM_SCALING = "claimScaling";

    private static final String GROUPING_CLAIMS = "claimGrouping";

    private static final String GROUPING_EVENTS = "eventGrouping";

    private static final String COLORING_CLAIMS = "claimColoring";

    private static final String COLORING_EVENTS = "eventColoring";

    private static final String COLORING_DEPENDENCIES = "dependencyColoring";

    private static final String FILTERING_CLAIMS = "claimFiltering";

    private static final String FILTERING_EVENTS = "eventFiltering";

    private static final String FILTERING_DEPENDENCIES = "dependencyFiltering";

    private static final String FILTERING_RESOURCES = "resourceFiltering";

    private static final String FILTERING_SIGNALS = "signalFiltering";

    private static final String DESCRIBING_CLAIMS = "claimDescribing";

    private static final String DESCRIBING_EVENTS = "eventDescribing";

    private static final String DESCRIBING_DEPENDENCIES = "dependencyDescribing";

    private static final String DESCRIBING_RESOURCES = "resourceDescribing";

    private static final String DESCRIBING_SIGNALS = "signalDescribing";

    private TraceViewConfigurationIO() {
    }

    public static void toFile(TraceViewConfiguration cfg, File out) throws IOException {
        BufferedWriter w = new BufferedWriter(new FileWriter(out));
        try {
            w.write(SHOW_CLAIMS + " : " + cfg.showClaims() + "\n");
            w.write(SHOW_EVENTS + " : " + cfg.showEvents() + "\n");
            w.write(SHOW_CLAIM_EVENTS + " : " + cfg.showClaimEvents() + "\n");
            w.write(SHOW_DEPENDENCIES + " : " + cfg.showDependencies() + "\n");
            w.write(SHOW_SIGNALS + " : " + cfg.showSignals() + "\n");
            w.write(SHOW_SIGNAL_MARKERS + " : " + cfg.getShowSignalMarkers() + "\n");
            w.write(ACTIVITY_VIEW + " : " + cfg.isActivityView() + "\n");
            w.write(CLAIM_SCALING + " : " + cfg.getClaimScaling() + "\n");
            w.write(SHOW_CLAIM_LABELS + " : " + cfg.getShowClaimLabels() + "\n");
            writeCollection(cfg, w, GROUPING_CLAIMS, cfg.getGroupingAttributes(TracePart.CLAIM));
            writeCollection(cfg, w, GROUPING_EVENTS, cfg.getGroupingAttributes(TracePart.EVENT));
            writeCollection(cfg, w, COLORING_CLAIMS, cfg.getColoringAttributes(TracePart.CLAIM));
            writeCollection(cfg, w, COLORING_EVENTS, cfg.getColoringAttributes(TracePart.EVENT));
            writeCollection(cfg, w, COLORING_DEPENDENCIES, cfg.getColoringAttributes(TracePart.DEPENDENCY));
            writeCollection(cfg, w, DESCRIBING_CLAIMS, cfg.getDescribingAttributes(TracePart.CLAIM));
            writeCollection(cfg, w, DESCRIBING_DEPENDENCIES, cfg.getDescribingAttributes(TracePart.DEPENDENCY));
            writeCollection(cfg, w, DESCRIBING_EVENTS, cfg.getDescribingAttributes(TracePart.EVENT));
            writeCollection(cfg, w, DESCRIBING_RESOURCES, cfg.getDescribingAttributes(TracePart.RESOURCE));
            writeCollection(cfg, w, DESCRIBING_SIGNALS, cfg.getDescribingAttributes(TracePart.SIGNAL));
            writeMap(cfg, w, FILTERING_CLAIMS, cfg.getFilters(TracePart.CLAIM));
            writeMap(cfg, w, FILTERING_EVENTS, cfg.getFilters(TracePart.EVENT));
            writeMap(cfg, w, FILTERING_DEPENDENCIES, cfg.getFilters(TracePart.DEPENDENCY));
            writeMap(cfg, w, FILTERING_RESOURCES, cfg.getFilters(TracePart.RESOURCE));
            writeMap(cfg, w, FILTERING_SIGNALS, cfg.getFilters(TracePart.SIGNAL));
            if (cfg.getRange() != null) {
                w.write(RANGE + " : " + cfg.getRange().getLowerBound() + " ; " + cfg.getRange().getUpperBound() + "\n");
            }
        } finally {
            if (w != null) {
                w.close();
            }
        }
    }

    private static void writeCollection(TraceViewConfiguration cfg, BufferedWriter w, String key,
            Collection<String> atts) throws IOException
    {
        if (atts != null) {
            w.write(key + " : " + toString(atts) + "\n");
        }
    }

    private static void writeMap(TraceViewConfiguration cfg, BufferedWriter w, String key,
            List<DisjunctionFilter> filters) throws IOException
    {
        w.write(key + " : ");
        for (Iterator<DisjunctionFilter> it = filters.iterator(); it.hasNext();) {
            DisjunctionFilter df = it.next();
            w.write(" { ");
            for (Iterator<SimpleFilter> it2 = df.getPersistentFilters().iterator(); it2.hasNext();) {
                SimpleFilter sf = it2.next();
                String attName = sf.getAttName();
                Collection<String> atts = sf.getIncludedValues();
                w.write(escape(attName) + " = " + toString(atts));
                if (it2.hasNext()) {
                    w.write(" ; ");
                }
            }
            w.write(" } ");
            if (it.hasNext()) {
                w.write(",");
            }
        }
        w.write("\n");
    }

    private static String toString(Collection<String> atts) {
        StringBuilder b = new StringBuilder();
        for (Iterator<String> it = atts.iterator(); it.hasNext();) {
            String att = it.next();
            b.append(escape(att));
            if (it.hasNext()) {
                b.append(" , ");
            }
        }
        return b.toString();
    }

    public static TraceViewConfiguration fromFile(File in) throws TraceException, IOException {
        TraceViewConfiguration c = new TraceViewConfiguration();
        BufferedReader r = new BufferedReader(new FileReader(in));
        try {
            String line = r.readLine();
            int lineNumber = 1;
            while (line != null) {
                line = line.trim();
                int splitIdx = line.indexOf(':');
                if (splitIdx <= 0) {
                    throw new TraceException("Syntax error in line " + lineNumber);
                }
                String key = line.substring(0, splitIdx).trim();
                String value = ""; // allow for empty values
                if (splitIdx < line.length() - 1) {
                    value = line.substring(splitIdx + 1).trim();
                }
                processLine(c, lineNumber, key, value);
                line = r.readLine();
                lineNumber++;
            }
            return c;
        } finally {
            if (r != null) {
                r.close();
            }
        }
    }

    private static void processLine(TraceViewConfiguration c, int lineNumber, String key, String value)
            throws TraceException
    {
        try {
            switch (key) {
                case SHOW_CLAIMS:
                    c.setShowClaims(Boolean.parseBoolean(value));
                    break;
                case SHOW_EVENTS:
                    c.setShowEvents(Boolean.parseBoolean(value));
                    break;
                case SHOW_CLAIM_EVENTS:
                    c.setShowClaimEvents(Boolean.parseBoolean(value));
                    break;
                case SHOW_DEPENDENCIES:
                    c.setShowDependencies(Boolean.parseBoolean(value));
                    break;
                case SHOW_SIGNAL_MARKERS:
                    c.setShowSignalMarkers(Boolean.parseBoolean(value));
                    break;
                case SHOW_SIGNALS:
                    c.setShowSignals(Boolean.parseBoolean(value));
                    break;
                case SHOW_CLAIM_LABELS:
                    c.setShowClaimLabels(Boolean.parseBoolean(value));
                    break;
                case ACTIVITY_VIEW:
                    if (Boolean.parseBoolean(value)) {
                        c.setActivityView();
                    } else {
                        c.setResourceView();
                    }
                    break;
                case CLAIM_SCALING:
                    c.setClaimScaling(ClaimScaling.valueOf(value));
                    break;
                case GROUPING_CLAIMS:
                    c.setGroupingAttributes(TracePart.CLAIM, fromString(value, lineNumber));
                    break;
                case GROUPING_EVENTS:
                    c.setGroupingAttributes(TracePart.EVENT, fromString(value, lineNumber));
                    break;
                case COLORING_CLAIMS:
                    c.setColoringAttributes(TracePart.CLAIM, fromString(value, lineNumber));
                    break;
                case COLORING_EVENTS:
                    c.setColoringAttributes(TracePart.EVENT, fromString(value, lineNumber));
                    break;
                case COLORING_DEPENDENCIES:
                    c.setColoringAttributes(TracePart.DEPENDENCY, fromString(value, lineNumber));
                    break;
                case DESCRIBING_CLAIMS:
                    c.setDescribingAttributes(TracePart.CLAIM, fromString(value, lineNumber));
                    break;
                case DESCRIBING_DEPENDENCIES:
                    c.setDescribingAttributes(TracePart.DEPENDENCY, fromString(value, lineNumber));
                    break;
                case DESCRIBING_EVENTS:
                    c.setDescribingAttributes(TracePart.EVENT, fromString(value, lineNumber));
                    break;
                case DESCRIBING_RESOURCES:
                    c.setDescribingAttributes(TracePart.RESOURCE, fromString(value, lineNumber));
                    break;
                case DESCRIBING_SIGNALS:
                    c.setDescribingAttributes(TracePart.SIGNAL, fromString(value, lineNumber));
                    break;
                case FILTERING_CLAIMS:
                    parseFilter(c, TracePart.CLAIM, value, lineNumber);
                    break;
                case FILTERING_EVENTS:
                    parseFilter(c, TracePart.EVENT, value, lineNumber);
                    break;
                case FILTERING_RESOURCES:
                    parseFilter(c, TracePart.RESOURCE, value, lineNumber);
                    break;
                case FILTERING_DEPENDENCIES:
                    parseFilter(c, TracePart.DEPENDENCY, value, lineNumber);
                    break;
                case FILTERING_SIGNALS:
                    parseFilter(c, TracePart.SIGNAL, value, lineNumber);
                    break;
                case RANGE:
                    c.setRange(parseRange(value, lineNumber));
                    break;
                default:
                    throw new TraceException("Unexpected property: " + key + " in line " + lineNumber);
            }
        } catch (IllegalArgumentException e) {
            throw new TraceException(e);
        }
    }

    private static Range parseRange(String s, int lineNumber) throws TraceException {
        String[] tokens = s.split(";");
        if (tokens.length != 2) {
            throw new TraceException("Parse error in line " + lineNumber);
        }
        double lb = Double.parseDouble(tokens[0].trim());
        double ub = Double.parseDouble(tokens[1].trim());
        return new Range(lb, ub);
    }

    private static void parseFilter(TraceViewConfiguration cfg, TracePart part, String s, int lineNumber)
            throws TraceException
    {
        if (s.trim().length() == 0) {
            return;
        }
        boolean done = false;
        while (!done) {
            int i = firstUnescaped(s, 0, '{');
            int j = firstUnescaped(s, 0, '}');
            if (i < 0) {
                done = true;
                break;
            }
            if (j <= i + 1 || j >= s.length()) {
                throw new TraceException("Parse error in line " + lineNumber);
            }
            DisjunctionFilter df = new DisjunctionFilter();
            String simpleFilterString = s.substring(i + 1, j).trim();
            parseSimpleFilters(df, simpleFilterString, lineNumber);
            cfg.addFilter(part, df);
            s = s.substring(j + 1);
        }
    }

    private static void parseSimpleFilters(DisjunctionFilter df, String s, int lineNumber) throws TraceException {
        boolean done = false;
        while (!done) {
            int idx = firstUnescaped(s, 0, ';');
            String simpleFilterString;
            if (idx < 0) {
                done = true;
                simpleFilterString = s;
            } else {
                simpleFilterString = s.substring(0, idx).trim();
                s = s.substring(idx + 1);
            }
            df.add(parseSimpleFilter(simpleFilterString, lineNumber));
        }
    }

    private static SimpleFilter parseSimpleFilter(String s, int lineNumber) throws TraceException {
        int i = firstUnescaped(s, 0, '=');
        if (i <= 0 || i >= s.length() - 1) {
            throw new TraceException("Parse error in line " + lineNumber);
        }
        String attName = unescape(s.substring(0, i).trim());
        s = s.substring(i + 1);
        List<String> includedValues = fromString(s, lineNumber);
        return new SimpleFilter(attName, includedValues);
    }

    private static List<String> fromString(String s, int lineNumber) throws TraceException {
        List<String> r = new ArrayList<String>();
        int i = 0;
        while (i < s.length()) {
            StringBuilder key = new StringBuilder();
            i = readElement(s, i, key, lineNumber);
            r.add(key.toString().trim());
        }
        return r;
    }

    private static int readElement(String line, int i, StringBuilder b, int lineNumber) throws TraceException {
        int j = firstUnescaped(line, i, ',');
        if (j == -1) {
            j = line.length();
        }
        if (j > i) {
            String key = unescape(line.substring(i, j).trim());
            b.append(key.trim());
            return j + 1;
        } else {
            throw new TraceException("Failed to parse attribute list in line " + lineNumber);
        }
    }

    private static int firstUnescaped(String line, int fromIndex, char c) {
        while (fromIndex < line.length()) {
            int r = line.indexOf(c, fromIndex);
            if (r >= 0) {
                if (r > 0 && line.charAt(r - 1) == '\\') {
                    fromIndex = r + 1;
                } else {
                    return r;
                }
            } else {
                return -1;
            }
        }
        return -1;
    }

    private static String unescape(String str) {
        return str.replace("\\,", ",").replace("\\=", "=").replace("\\{", "{").replace("\\}", "}").replace("\\;", ";");
    }

    private static String escape(String str) {
        return str.replace(",", "\\,").replace("=", "\\=").replace("{", "\\{").replace("}", "\\}").replace(";", "\\;");
    }
}
