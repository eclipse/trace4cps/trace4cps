/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.vis.jfree;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.eclipse.trace4cps.analysis.signal.impl.PsopHelper;
import org.eclipse.trace4cps.common.jfreechart.chart.axis.AxisUtils;
import org.eclipse.trace4cps.common.jfreechart.chart.axis.Section;
import org.eclipse.trace4cps.common.jfreechart.chart.axis.SectionAxis;
import org.eclipse.trace4cps.common.jfreechart.chart.geom.Arrow2D;
import org.eclipse.trace4cps.common.jfreechart.chart.plot.PlotUtils;
import org.eclipse.trace4cps.common.jfreechart.chart.renderer.xy.XYEdgeRenderer;
import org.eclipse.trace4cps.common.jfreechart.data.xy.XYEdgeDataItem;
import org.eclipse.trace4cps.common.jfreechart.data.xy.XYEdgeSeries;
import org.eclipse.trace4cps.common.jfreechart.data.xy.XYEdgeSeriesCollection;
import org.eclipse.trace4cps.common.jfreechart.data.xy.XYScaledSeries;
import org.eclipse.trace4cps.common.jfreechart.data.xy.XYScaledSeriesCollection;
import org.eclipse.trace4cps.core.ClaimEventType;
import org.eclipse.trace4cps.core.IAttributeAware;
import org.eclipse.trace4cps.core.IAttributeFilter;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IClaimEvent;
import org.eclipse.trace4cps.core.IDependency;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.IFilteredTrace;
import org.eclipse.trace4cps.core.IInterval;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.IPsopFragment;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.core.impl.ModifiableTrace;
import org.eclipse.trace4cps.core.impl.Trace;
import org.eclipse.trace4cps.core.impl.TraceHelper;
import org.eclipse.trace4cps.vis.jfree.impl.Cell;
import org.eclipse.trace4cps.vis.jfree.impl.ClaimFragment;
import org.eclipse.trace4cps.vis.jfree.impl.ClaimPartition;
import org.eclipse.trace4cps.vis.jfree.impl.Partition;
import org.jfree.chart.ChartColor;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.XYItemLabelGenerator;
import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRendererState;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.ui.RectangleAnchor;
import org.jfree.chart.ui.TextAnchor;
import org.jfree.data.Range;
import org.jfree.data.RangeAlign;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYIntervalDataItem;
import org.jfree.data.xy.XYIntervalSeries;
import org.jfree.data.xy.XYIntervalSeriesCollection;

/**
 * This is the entry point for showing {@link Trace} instances.
 */
public class TracePlotManager implements XYToolTipGenerator, XYItemLabelGenerator {
    private static final int CLAIMS_INDEX = 3;

    private static final int SIGNALS_INDEX = 1;

    private static final int EVENTS_INDEX = 2;

    private static final int DEPENDENCIES_INDEX = 0;

    private final List<ModifiableTrace> traces = new ArrayList<>();

    private TraceViewConfiguration viewConfig;

    private Map<Paint, Integer> highlightToClaimSeries;

    private ValueMarker startMarker;

    private ValueMarker endMarker;

    private DataItemFactory dataItemFactory = new DefaultDataItemFactory();

    private XYToolTipGenerator delegateToolTipGenerator = new DefaultToolTipGenerator(this::getViewConfig);

    public TracePlotManager(ITrace... traces) {
        this(new TraceViewConfiguration(), traces);
    }

    public TracePlotManager(TraceViewConfiguration viewConfig, ITrace... traces) {
        this.viewConfig = viewConfig;
        setTraces(traces);
    }

    public void setDataItemFactory(DataItemFactory dataItemFactory) {
        this.dataItemFactory = dataItemFactory;
    }

    public void setDelegateToolTipGenerator(XYToolTipGenerator delegateToolTipGenerator) {
        this.delegateToolTipGenerator = delegateToolTipGenerator;
    }

    public void setTraces(ITrace... traces) {
        if (traces != null && traces.length > 0) {
            setTraces(Arrays.asList(traces));
        }
    }

    public void setTraces(List<ITrace> traces) {
        if (traces.isEmpty()) {
            throw new IllegalArgumentException();
        }
        TimeUnit tu = traces.get(0).getTimeUnit();
        long offset = traces.get(0).getTimeOffset().longValue();
        for (ITrace trace: traces) {
            if (tu != trace.getTimeUnit() || offset != trace.getTimeOffset().longValue()) {
                throw new IllegalArgumentException("traces must have the same time unit and offset");
            }
            this.traces.add(new ModifiableTrace(trace));
        }
    }

    /**
     * Retrieves the configuration for building the view. Modifications can be made, after which the {@link #update()}
     * method updates the plot.
     *
     * @return the view configuration
     */
    public TraceViewConfiguration getViewConfig() {
        return viewConfig;
    }

    public void setViewConfiguration(TraceViewConfiguration viewConfig) {
        this.viewConfig = viewConfig;
    }

    public List<ModifiableTrace> getTraces() {
        return traces;
    }

    @Override
    public String generateToolTip(XYDataset ds, int series, int item) {
        return delegateToolTipGenerator.generateToolTip(ds, series, item);
    }

    @Override
    public String generateLabel(XYDataset ds, int series, int item) {
        return delegateToolTipGenerator.generateToolTip(ds, series, item);
    }

    /**
     * Initializes a XY plot with datasets for Claims, Events, Signals, and Dependencies and their renderers. To be
     * called once.
     */
    public void initializePlot(XYPlot xyplot) {
        xyplot.setRangePannable(true);
        xyplot.setDomainPannable(true);

        initClaimDatasetAndRenderer(xyplot);
        initSignalDatasetAndRenderer(xyplot);
        initEventDatasetAndRenderer(xyplot);
        initDependencyDatasetAndRenderer(xyplot);
    }

    private void initDependencyDatasetAndRenderer(XYPlot xyplot) {
        XYEdgeSeriesCollection edgesDataset = new XYEdgeSeriesCollection();
        xyplot.setDataset(DEPENDENCIES_INDEX, edgesDataset);
        XYEdgeRenderer edgesRenderer = new XYEdgeRenderer();
        edgesRenderer.setDefaultToolTipGenerator(this);
        edgesRenderer.setDefaultEndShape(getArrowHeadShape(), false);
        edgesRenderer.setAutoPopulateSeriesPaint(false);
        xyplot.setRenderer(DEPENDENCIES_INDEX, edgesRenderer);
    }

    private Shape getArrowHeadShape() {
        return new Arrow2D.Double(0, 0, 8, 8);
    }

    private void initEventDatasetAndRenderer(XYPlot xyplot) {
        XYEdgeSeriesCollection edgesDataset = new XYEdgeSeriesCollection();
        xyplot.setDataset(EVENTS_INDEX, edgesDataset);
        XYEdgeRenderer edgesRenderer = new XYEdgeRenderer();
        edgesRenderer.setDefaultToolTipGenerator(this);
        edgesRenderer.setDefaultEndShape(getArrowHeadShape(), false);
        edgesRenderer.setAutoPopulateSeriesPaint(false);
        edgesRenderer.setAutoPopulateSeriesStroke(false);
        edgesRenderer.setDefaultStroke(new BasicStroke(2.0f), false);
        xyplot.setRenderer(EVENTS_INDEX, edgesRenderer);
    }

    private void initSignalDatasetAndRenderer(XYPlot xyplot) {
        XYScaledSeriesCollection signalDataset = new XYScaledSeriesCollection();
        signalDataset.setIntervalWidth(0);
        xyplot.setDataset(SIGNALS_INDEX, signalDataset);
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer(true, true);
        renderer.setAutoPopulateSeriesPaint(false);
        renderer.setAutoPopulateSeriesShape(false);
        renderer.setDefaultShape(new Rectangle2D.Double(-1, -1, 3, 3));
        renderer.setDefaultToolTipGenerator(this);
        renderer.setDefaultEntityRadius(5);
        xyplot.setRenderer(SIGNALS_INDEX, renderer);
    }

    private void initClaimDatasetAndRenderer(XYPlot xyplot) {
        XYIntervalSeriesCollection claimDataset = new XYIntervalSeriesCollection();
        xyplot.setDataset(CLAIMS_INDEX, claimDataset);
        XYBarRenderer claimRenderer = new XYBarRenderer() {
            private static final long serialVersionUID = 6167536692792897425L;

            @Override
            public XYItemRendererState initialise(Graphics2D g2, Rectangle2D dataArea, XYPlot plot, XYDataset dataset,
                    PlotRenderingInfo info)
            {
                final XYItemRendererState state = super.initialise(g2, dataArea, plot, dataset, info);
                // Avoiding bug of not drawing bars
                state.setProcessVisibleItemsOnly(false);
                return state;
            }
        };
        xyplot.setRenderer(CLAIMS_INDEX, claimRenderer);
        claimRenderer.setUseYInterval(true);
        claimRenderer.setAutoPopulateSeriesPaint(false);
        claimRenderer.setDefaultOutlinePaint(ChartColor.BLACK);
        claimRenderer.setDefaultOutlineStroke(new BasicStroke(1));
        claimRenderer.setShadowVisible(false);
        claimRenderer.setDefaultToolTipGenerator(this);
        claimRenderer.setDefaultItemLabelGenerator(this);
        claimRenderer.setDefaultItemLabelsVisible(false);
        claimRenderer.setComputeItemLabelContrastColor(true);
    }

    /**
     * Initializes the claim, events and dependencies series and sets the paint for the series. Not all series may be
     * used, but that's no problem we assume. If a highlight is set in the view configuration, then the colors used in
     * the highlight are used for the configuration of the claim dataset and renderer.
     */
    private void initSeriesAndColors(XYPlot xyplot) {
        if (!viewConfig.hasHighlight()) {
            initSeriesPaint((XYIntervalSeriesCollection)xyplot.getDataset(CLAIMS_INDEX),
                    xyplot.getRenderer(CLAIMS_INDEX));
        } else {
            initClaimHighlightPaints(xyplot);
        }
        xyplot.getRenderer(CLAIMS_INDEX).setDefaultItemLabelsVisible(viewConfig.getShowClaimLabels());
        initSeriesPaint((XYEdgeSeriesCollection)xyplot.getDataset(EVENTS_INDEX), xyplot.getRenderer(EVENTS_INDEX));
        initSeriesPaint((XYEdgeSeriesCollection)xyplot.getDataset(DEPENDENCIES_INDEX),
                xyplot.getRenderer(DEPENDENCIES_INDEX));
    }

    private void initSeriesPaint(XYIntervalSeriesCollection ds, XYItemRenderer r) {
        for (ColorManager color: ColorManager.values()) {
            ds.addSeries(new XYIntervalSeries(color, false, true));
            r.setSeriesPaint(color.ordinal(), color.getColor());
        }
    }

    private void initSeriesPaint(XYEdgeSeriesCollection ds, XYItemRenderer r) {
        for (ColorManager color: ColorManager.values()) {
            ds.addSeries(new XYEdgeSeries(color));
            r.setSeriesPaint(color.ordinal(), color.getColor());
        }
    }

    /**
     * Initializes the series paints for highlighted claims
     */
    private void initClaimHighlightPaints(XYPlot xyplot) {
        XYItemRenderer renderer = xyplot.getRenderer(CLAIMS_INDEX);
        XYIntervalSeriesCollection ds = (XYIntervalSeriesCollection)xyplot.getDataset(CLAIMS_INDEX);

        // Keeps highlight Paint -> Series index mapping
        highlightToClaimSeries = new HashMap<>();
        Set<Paint> paints = new HashSet<>();
        paints.addAll(viewConfig.getHighlightMap().values());
        // Create the map from paint to series index and set the series paint in the renderer
        renderer.setSeriesPaint(0, ColorManager.NON_HIGHLIGHT.getColor());
        highlightToClaimSeries.put(ColorManager.NON_HIGHLIGHT.getColor(), 0);
        ds.addSeries(new XYIntervalSeries("0", false, true));
        int i = 1;
        for (Paint p: paints) {
            highlightToClaimSeries.put(p, i);
            ds.addSeries(new XYIntervalSeries("" + i, false, true));
            renderer.setSeriesPaint(i, p);
            i++;
        }
    }

    /**
     * Gets the series index for a TRACE item. The series is used for coloring.
     *
     * @param a the claim or event instance
     * @return a series index
     */

    private int getSeriesIndex(IClaim a) {
        // Unwrap ClaimFragments:
        if (a instanceof ClaimFragment) {
            a = ((ClaimFragment)a).getWrapped();
        }
        if (!viewConfig.hasHighlight()) {
            return getSeriesIndex(a, TracePart.CLAIM);
        } else {
            // Highlight defined
            Paint p = viewConfig.getHighlightMap().get(a);
            if (p == null) {
                p = ColorManager.NON_HIGHLIGHT.getColor(); // no highlight
            }
            return highlightToClaimSeries.get(p);
        }
    }

    private int getSeriesIndex(IEvent a) {
        return getSeriesIndex(a, TracePart.EVENT);
    }

    private int getSeriesIndex(IDependency a) {
        return getSeriesIndex(a, TracePart.DEPENDENCY);
    }

    private int getSeriesIndex(IAttributeAware a, TracePart part) {
        // We check whether there is a "color" attribute, and try to use that
        ColorManager color = ColorManager.getColor(a);
        if (color != null) {
            return color.ordinal();
        }
        // We use the coloring attributes
        Collection<String> colAtts = viewConfig.getColoringAttributes(part);
        String colorKey = TraceHelper.getValues(a, colAtts, false);
        return ColorManager.getAutomaticColor(colorKey).ordinal();
    }

    /**
     * Refreshes the plot, typically after the {@link TraceViewConfiguration} has been updated.
     * {@link #initializePlot(XYPlot)} and {@link #setTraces(ITrace...)} must have been called before this method.
     */
    public void update(XYPlot xyplot) {
        applyFilters();

        boolean firstPlot = xyplot.getDomainAxis().getLabel() == null;
        Range oldRangeX = xyplot.getDomainAxis().getRange();

        // Set up the axes of the plot
        SectionAxis rangeAxis = new SectionAxis();
        xyplot.setRangeAxis(rangeAxis);

        NumberAxis domainAxis = new NumberAxis(null);
        domainAxis.setAutoRangeIncludesZero(false);
        domainAxis.setAutoRangeAlign(RangeAlign.LOWER);
        domainAxis.setRangeMinimumSize(10e-6);
        domainAxis.setLabel("Time (" + traces.get(0).getTimeUnit().toString().toLowerCase() + ")");
        xyplot.setDomainAxis(domainAxis);

        // Get, clear and prepare the datasets:
        XYIntervalSeriesCollection claimsDataset = (XYIntervalSeriesCollection)xyplot.getDataset(CLAIMS_INDEX);
        claimsDataset.removeAllSeries();
        XYScaledSeriesCollection signalDataset = (XYScaledSeriesCollection)xyplot.getDataset(SIGNALS_INDEX);
        signalDataset.removeAllSeries();
        XYEdgeSeriesCollection eventsDataset = (XYEdgeSeriesCollection)xyplot.getDataset(EVENTS_INDEX);
        eventsDataset.removeAllSeries();
        XYEdgeSeriesCollection dependenciesDataset = (XYEdgeSeriesCollection)xyplot.getDataset(DEPENDENCIES_INDEX);
        dependenciesDataset.removeAllSeries();

        // Create the colors and series for claims and events (inc highlight)
        initSeriesAndColors(xyplot);

        // A cache for creating the dependency dataset
        Map<IEvent, XYCoordinate> entityMap = new HashMap<>();

        // Fill all datasets:
        for (Iterator<? extends ITrace> it = traces.iterator(); it.hasNext();) {
            ITrace trace = it.next();
            // Create the sections datasets: claims, events, signals
            List<SectionData> data = getSectionsData(trace);
            for (SectionData d: data) {
                if (d instanceof SignalSectionData) {
                    XYLineAndShapeRenderer signalsRenderer = (XYLineAndShapeRenderer)xyplot.getRenderer(SIGNALS_INDEX);
                    ((SignalSectionData)d).add(rangeAxis, signalDataset, signalsRenderer);
                } else if (d instanceof ClaimSectionData) {
                    ((ClaimSectionData)d).add(rangeAxis, claimsDataset, entityMap);
                } else if (d instanceof EventSectionData) {
                    ((EventSectionData)d).add(rangeAxis, eventsDataset, entityMap);
                }
            }
            // Create the dependency dataset:
            if (viewConfig.showDependencies()) {
                addDependencies(trace.getDependencies(), entityMap, dependenciesDataset);
            }
            // Create a separator section between the data of multiple traces
            if (it.hasNext()) {
                rangeAxis.nextSection("");
            }
        }

        addStartEndMarkers(xyplot);
        ChartFactory.getChartTheme().apply(xyplot.getChart());

        // Performance improvements: for large datasets only show a portion
        int itemCount = PlotUtils.getItemCount(xyplot);
        if (itemCount > 50_000) {
            Range range = xyplot.getDataRange(domainAxis);

            // Detected large amount of plot data, so limit time window (based on average) for performance
            domainAxis.setAutoRangeAlign(RangeAlign.LOWER);
            domainAxis.setFixedAutoRange((50_000d / itemCount) * range.getLength());
        } else {
            domainAxis.setFixedAutoRange(0);
        }

        if (rangeAxis.getSections().size() > 30) {
            rangeAxis.setAutoRangeAlign(RangeAlign.LOWER);
            rangeAxis.setFixedAutoRange(30);
        } else {
            rangeAxis.setAutoRange(true);
        }

        if (!firstPlot) {
            domainAxis.setRange(oldRangeX);
        } else if (viewConfig.getRange() != null) {
            domainAxis.setRange(viewConfig.getRange());
        }

        AxisUtils.calculateAndApplyFixedDimension(rangeAxis, xyplot.getRangeAxisEdge());
    }

    private void applyFilters() {
        for (IFilteredTrace trace: traces) {
            trace.clearFilter(TracePart.ALL);
            for (IAttributeFilter f: viewConfig.getFilters(TracePart.RESOURCE)) {
                trace.addFilter(TracePart.RESOURCE, f);
            }
            for (IAttributeFilter f: viewConfig.getFilters(TracePart.CLAIM)) {
                trace.addFilter(TracePart.CLAIM, f);
            }
            for (IAttributeFilter f: viewConfig.getFilters(TracePart.DEPENDENCY)) {
                trace.addFilter(TracePart.DEPENDENCY, f);
            }
            for (IAttributeFilter f: viewConfig.getFilters(TracePart.EVENT)) {
                trace.addFilter(TracePart.EVENT, f);
            }
            for (IAttributeFilter f: viewConfig.getFilters(TracePart.SIGNAL)) {
                trace.addFilter(TracePart.SIGNAL, f);
            }
            trace.recalculate();
        }
    }

    private void addDependencies(List<IDependency> deps, Map<IEvent, XYCoordinate> entityMap,
            XYEdgeSeriesCollection dependenciesDataset)
    {
        for (IDependency dep: deps) {
            int key = getSeriesIndex(dep);
            XYEdgeSeries series = dependenciesDataset.getSeries(key);
            XYCoordinate src = entityMap.get(dep.getSrc());
            XYCoordinate dst = entityMap.get(dep.getDst());
            if (src != null && dst != null) {
                series.add(dataItemFactory.createDependencyDataItem(dep, src.x, src.y, dst.x, dst.y), false);
            } else {
                throw new IllegalStateException("Cannot find src or dst of " + dep);
            }
        }
    }

    private List<SectionData> getSectionsData(ITrace trace) {
        List<SectionData> data = new ArrayList<>();
        if (viewConfig.showClaims()) {
            createClaimSectionData(trace, data);
        }
        if (viewConfig.showEvents()) {
            createEventsSectionData(trace, data);
        }
        if (viewConfig.showSignals()) {
            createSignalSectionData(trace, data);
        }
        Collections.sort(data, Collections.reverseOrder());
        return data;
    }

    private void createSignalSectionData(ITrace trace, List<SectionData> data) {
        for (IPsop p: trace.getSignals()) {
            Color c = ColorManager.getColor(p, ColorManager.DARK_BLUE).getColor();
            data.add(new SignalSectionData(trace, p, c));
        }
    }

    private void createClaimSectionData(ITrace trace, List<SectionData> data) {
        ClaimPartition groupingPartition = null;
        if (viewConfig.isActivityView()) {
            groupingPartition = ClaimPartition.createActivityPartition(trace.getClaims(),
                    viewConfig.getGroupingAttributes(TracePart.CLAIM));
        } else {
            groupingPartition = ClaimPartition.createResourcePartition(trace.getClaims(),
                    viewConfig.getDescribingAttributes(TracePart.RESOURCE));
        }
        groupingPartition.scaleClaims(viewConfig.getClaimScaling());
        for (Cell<IClaim> cell: groupingPartition) {
            data.add(new ClaimSectionData(trace, cell));
        }
    }

    private void createEventsSectionData(ITrace trace, List<SectionData> data) {
        List<IEvent> events = TraceHelper.getEvents(trace, viewConfig.showClaimEvents());
        if (!events.isEmpty()) {
            Partition<IEvent> groupingPartition = Partition.createAttributePartition(events,
                    viewConfig.getGroupingAttributes(TracePart.EVENT));
            for (Cell<IEvent> cell: groupingPartition) {
                data.add(new EventSectionData(trace, cell));
            }
        }
    }

    private void addStartEndMarkers(XYPlot xyplot) {
        double lb = Double.POSITIVE_INFINITY;
        double ub = Double.NEGATIVE_INFINITY;
        for (ITrace trace: traces) {
            IInterval dom = TraceHelper.getDomain(trace);
            lb = Math.min(lb, dom.lb().doubleValue());
            ub = Math.max(ub, dom.ub().doubleValue());
        }
        if (startMarker == null) {
            String startLabel = "Start of trace" + (traces.size() > 1 ? "s" : "");
            startMarker = new ValueMarker(lb, Color.DARK_GRAY, xyplot.getDomainGridlineStroke());
            startMarker.setLabelAnchor(RectangleAnchor.TOP_RIGHT);
            startMarker.setLabelTextAnchor(TextAnchor.TOP_LEFT);
            startMarker.setLabelPaint(Color.DARK_GRAY);
            xyplot.addDomainMarker(startMarker);
            startMarker.setLabel(startLabel);

            String endLabel = "End of trace" + (traces.size() > 1 ? "s" : "");
            endMarker = new ValueMarker(ub, Color.DARK_GRAY, xyplot.getDomainGridlineStroke());
            endMarker.setLabelAnchor(RectangleAnchor.TOP_LEFT);
            endMarker.setLabelTextAnchor(TextAnchor.TOP_RIGHT);
            endMarker.setLabelPaint(Color.DARK_GRAY);
            xyplot.addDomainMarker(endMarker);
            endMarker.setLabel(endLabel);
        } else {
            startMarker.setValue(lb);
            endMarker.setValue(ub);
        }
    }

    private abstract class SectionData implements Comparable<SectionData> {
        protected String traceLabel;

        protected String label;

        public SectionData(String traceLabel, String label) {
            this.traceLabel = traceLabel;
            this.label = label;
        }

        public String getSectionLabel() {
            if (traces.size() > 1) {
                return traceLabel + " : " + label;
            } else {
                return label;
            }
        }

        @Override
        public int compareTo(SectionData o) {
            if (traceLabel != null) {
                int val = traceLabel.compareTo(o.traceLabel);
                if (val == 0) {
                    val = label.compareTo(o.label);
                }
                return val;
            }
            return label.compareTo(o.label);
        }
    }

    private class SignalSectionData extends SectionData {
        private final Paint color;

        private final IPsop signal;

        public SignalSectionData(ITrace trace, IPsop signal, Paint color) {
            super(TraceHelper.getValues(trace, false),
                    TraceHelper.getValues(signal, viewConfig.getDescribingAttributes(TracePart.SIGNAL), false));
            this.signal = signal;
            this.color = color;
        }

        // TODO: create dedicated dataset and renderer for the piecewise polynomials
        public void add(SectionAxis rangeAxis, XYScaledSeriesCollection ds, XYLineAndShapeRenderer renderer) {
            Section s = rangeAxis.nextSection(getSectionLabel(), 2d, 0.1d);
            // make sure that the label, used as a series key, is unique
            String seriesKey = label + UUID.randomUUID().toString();
            XYScaledSeries series = new XYScaledSeries(seriesKey, null, s.getRange(), false, true);
            for (int i = 0; i < signal.getFragments().size(); i++) {
                IPsopFragment f = signal.getFragments().get(i);
                double x = f.dom().lb().doubleValue();
                double y = f.getC().doubleValue();
                series.add(dataItemFactory.createSignalDataItem(signal, f, x, y));
                addAdditionalItemOnJumps(series, f, i);
            }
            s.setGridBandNumberRange(getRange(series.getUnscaledRangeY()), false);
            ds.addSeries(series);
            renderer.setDefaultShapesVisible(getViewConfig().getShowSignalMarkers());
            renderer.setSeriesPaint(ds.getSeriesIndex(seriesKey), color);
        }

        private Range getRange(Range dataRange) {
            if (dataRange.getLength() <= 0d) { // single value on Y axis
                // FIXME: the graph will not be shown: bug in the Section/XYScaledSeries types
                // This Range, however, prevents an unchecked exception fro jFree
                return new Range(dataRange.getLowerBound() - 1d, dataRange.getUpperBound() + 1d);
            } else {
                return dataRange;
            }
        }

        private void addAdditionalItemOnJumps(XYScaledSeries series, IPsopFragment f, int i) {
            boolean lastFragment = i == signal.getFragments().size() - 1;
            IPsopFragment nextF = lastFragment ? null : signal.getFragments().get(i + 1);
            double x2 = f.dom().ub().doubleValue();
            double y2 = PsopHelper.valueAt(f, x2).doubleValue();
            double delta = lastFragment ? 0 : Math.abs(y2 - nextF.getC().doubleValue());
            if (lastFragment || delta > 1e-9) {
                series.add(x2, y2);
            }
        }
    }

    private class EventSectionData extends SectionData {
        private final Cell<IEvent> cell;

        public EventSectionData(ITrace trace, Cell<IEvent> cell) {
            super(TraceHelper.getValues(trace, false), cell.getDescription());
            this.cell = cell;
        }

        public void add(SectionAxis rangeAxis, XYEdgeSeriesCollection ds, Map<IEvent, XYCoordinate> entityMap) {
            Section s = rangeAxis.nextSection(getSectionLabel(), 1, 0.1d);
            for (IEvent event: cell) {
                int key = getSeriesIndex(event);
                XYEdgeSeries series = ds.getSeries(key);
                series.add(createFrom(event, s, entityMap), false);
            }
        }

        private XYEdgeDataItem createFrom(IEvent event, Section s, Map<IEvent, XYCoordinate> entityMap) {
            double x = event.getTimestamp().doubleValue();
            double ymin = s.getRange().getLowerBound() + 0.1;
            double ymax = s.getRange().getUpperBound() - 0.1;
            if (event instanceof IClaimEvent) {
                double length = (s.getRange().getUpperBound() - s.getRange().getLowerBound()) / 2d;
                if (((IClaimEvent)event).getType() == ClaimEventType.START) {
                    ymax = s.getRange().getLowerBound() + length;
                } else {
                    ymin = s.getRange().getLowerBound() + length;
                    ymax = s.getRange().getLowerBound() + 0.1;
                }
            }

            XYCoordinate coord = new XYCoordinate(x, Math.min(ymin, ymax) + (Math.abs(ymax - ymin) / 2d));
            entityMap.put(event, coord);

            return dataItemFactory.createEventDataItem(event, x, ymin, ymax);
        }
    }

    private class ClaimSectionData extends SectionData {
        private final Cell<IClaim> cell;

        public ClaimSectionData(ITrace trace, Cell<IClaim> cell) {
            this(trace, cell.getDescription(), cell);
        }

        public ClaimSectionData(ITrace trace, String description, Cell<IClaim> cell) {
            super(TraceHelper.getValues(trace, false), description);
            this.cell = cell;
        }

        public void add(SectionAxis rangeAxis, XYIntervalSeriesCollection ds, Map<IEvent, XYCoordinate> entityMap) {
            Section s = rangeAxis.nextSection(getSectionLabel(), 1, 0.1d);
            for (IClaim claim: cell) {
                int key = getSeriesIndex(claim);
                XYIntervalSeries series = ds.getSeries(key);
                XYIntervalDataItem entity = createFrom(claim, s);
                administrateEvents(claim, entityMap, entity);
                series.add(entity, false);
            }
        }

        private void administrateEvents(IClaim claim, Map<IEvent, XYCoordinate> entityMap, XYIntervalDataItem entity) {
            double y = entity.getYLowValue() + Math.abs((entity.getYLowValue() - entity.getYHighValue()) / 2d);
            // The Math.abs is needed because the ClaimDataItem can introduce some rounding
            // effects because a rectangle is used...
            if (Math.abs(claim.getStartTime().doubleValue() - entity.getXLowValue()) < 1e-12) {
                // we have the first of the possible fragments
                entityMap.put(claim.getStartEvent(), new XYCoordinate(entity.getXLowValue(), y));
            }
            if (Math.abs(claim.getEndTime().doubleValue() - entity.getXHighValue()) < 1e-12) {
                // we have the last of the possible fragments
                entityMap.put(claim.getEndEvent(), new XYCoordinate(entity.getXHighValue(), y));
            }
        }

        private XYIntervalDataItem createFrom(IClaim claim, Section s) {
            if (claim instanceof ClaimFragment) {
                ClaimFragment frag = (ClaimFragment)claim;
                double xmin = frag.getStartTime().doubleValue();
                double xmax = frag.getEndTime().doubleValue();
                double ymin = s.getRange().getLowerBound() + 0.1 + frag.getSwimLaneOffset();
                double ymax = s.getRange().getLowerBound() + 0.1 + frag.getSwimLaneOffset()
                        + frag.getSwimLaneFraction();
                return dataItemFactory.createClaimDataItem(claim, xmin, xmax, ymin, ymax);
            } else {
                double xmin = claim.getStartTime().doubleValue();
                double xmax = claim.getEndTime().doubleValue();
                double ymin = s.getRange().getLowerBound() + 0.1;
                double ymax = s.getRange().getUpperBound() - 0.1;
                return dataItemFactory.createClaimDataItem(claim, xmin, xmax, ymin, ymax);
            }
        }
    }

    private class XYCoordinate {
        private final double x;

        private final double y;

        public XYCoordinate(double x, double y) {
            this.x = x;
            this.y = y;
        }
    }
}
