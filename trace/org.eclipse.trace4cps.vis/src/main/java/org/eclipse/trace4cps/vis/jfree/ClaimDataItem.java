/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.vis.jfree;

import org.eclipse.trace4cps.core.IClaim;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYIntervalDataItem;
import org.jfree.data.xy.XYIntervalSeries;
import org.jfree.data.xy.XYIntervalSeriesCollection;

public class ClaimDataItem extends XYIntervalDataItem {
    private static final long serialVersionUID = 1L;

    private final transient IClaim claim;

    ClaimDataItem(IClaim claim, double xmin, double xmax, double ymin, double ymax) {
        super((xmin + xmax) / 2, xmin, xmax, (ymin + ymax) / 2, ymin, ymax);
        this.claim = claim;
    }

    public IClaim getClaim() {
        return claim;
    }

    @Override
    public String toString() {
        return "ClaimDataItem[claim=" + claim + "]";
    }

    public static ClaimDataItem getFrom(XYDataset ds, int series, int item) {
        if (ds instanceof XYIntervalSeriesCollection) {
            XYIntervalSeriesCollection sc = (XYIntervalSeriesCollection)ds;
            XYIntervalSeries s = sc.getSeries(series);
            Object o = s.getDataItem(item);
            if (o instanceof ClaimDataItem) {
                return (ClaimDataItem)o;
            }
        }
        return null;
    }
}
