/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.vis.jfree.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.vis.jfree.ClaimScaling;

public class ClaimCell extends Cell<IClaim> {
    ClaimCell(String description) {
        super(description);
    }

    /**
     * Take care of claims with zero time!
     *
     * @param scaling scaling method
     */
    public void scaleClaims(ClaimScaling scaling) {
        if (scaling == ClaimScaling.NONE) {
            return;
        }
        IResource r = null;
        if (scaling == ClaimScaling.RESOURCE_AMOUNT) {
            r = getResource();
            if (r.useOffset()) {
                // Claims might overlap when using an offset,
                // this is the responsibility of the user.
                // Only the offset and amount are correctly scaled.
                for (int i = 0; i < items.size(); i++) {
                    IClaim claim = items.get(i);
                    double offset = (claim.getOffset().doubleValue() / r.getCapacity().doubleValue()) * 0.8d;
                    double fraction = (claim.getAmount().doubleValue() / r.getCapacity().doubleValue()) * 0.8d;
                    ClaimFragment frag = new ClaimFragment(claim, claim.getStartTime().doubleValue(),
                            claim.getEndTime().doubleValue(), offset, fraction);
                    items.set(i, frag);
                }
                return;
            }
        }
        List<Event> events = getEvents();
        items.clear();
        List<IClaim> active = new ArrayList<IClaim>();
        double prevEventTime = Double.NaN;
        int i = 0;
        while (i < events.size()) {
            Event e = events.get(i);
            double now = e.t.doubleValue();
            int endIdx = getNextIndex(events, i);
            createFragmentsForActiveClaims(active, scaling, r, prevEventTime, now);
            updateActiveClaims(events, active, i, endIdx);
            processClaimsWithZeroDuration(events, now, i, endIdx);
            prevEventTime = now;
            i = endIdx;
        }
    }

    private void processClaimsWithZeroDuration(List<Event> events, double now, int i, int endIdx) {
        for (int j = i; j < endIdx; j++) {
            Event ej = events.get(j);
            if (ej.isStart && ej.parent.getEndTime().doubleValue() == now) {
                ClaimFragment frag = new ClaimFragment(ej.parent, now, now, 0.1d, 0.8d);
                items.add(frag);
            }
        }
    }

    private void updateActiveClaims(List<Event> events, List<IClaim> active, int i, int endIdx) {
        for (int j = i; j < endIdx; j++) {
            Event ej = events.get(j);
            if (!ej.isStart) {
                active.remove(ej.parent);
            } else {
                active.add(ej.parent);
            }
        }
    }

    /**
     * @return first index of the next event with a strictly larger timestamp
     */
    private int getNextIndex(List<Event> events, int i) {
        double tsi = events.get(i).t.doubleValue();
        int j = i + 1;
        while (j < events.size()) {
            double tsj = events.get(j).t.doubleValue();
            if (tsj > tsi) {
                return j;
            }
            j++;
        }
        return events.size();
    }

    private void createFragmentsForActiveClaims(List<IClaim> active, ClaimScaling scaling, IResource r,
            double prevEventTime, double now)
    {
        if (!Double.isNaN(prevEventTime)) {
            switch (scaling) {
                case FULL:
                    createAndAddFragments(active, prevEventTime, now);
                    break;
                case RESOURCE_AMOUNT:
                    createAndAddFragments(active, prevEventTime, now, r);
                    break;
                default:
                    throw new IllegalStateException();
            }
        }
    }

    private void createAndAddFragments(List<IClaim> active, double prevEventTime, double now) {
        double offset = 0d;
        double fraction = 0.8d / active.size();
        for (IClaim a: active) {
            ClaimFragment frag = new ClaimFragment(a, prevEventTime, now, offset, fraction);
            items.add(frag);
            offset += fraction;
        }
    }

    private void createAndAddFragments(List<IClaim> active, double prevEventTime, double now, IResource r) {
        if (r.useOffset()) {
            for (IClaim a: active) {
                double offset = (a.getOffset().doubleValue() / r.getCapacity().doubleValue()) * 0.8d;
                double fraction = (a.getAmount().doubleValue() / r.getCapacity().doubleValue()) * 0.8d;
                ClaimFragment frag = new ClaimFragment(a, prevEventTime, now, offset, fraction);
                items.add(frag);
            }
        } else {
            double offset = 0d;
            for (IClaim a: active) {
                double fraction = (a.getAmount().doubleValue() / r.getCapacity().doubleValue()) * 0.8d;
                ClaimFragment frag = new ClaimFragment(a, prevEventTime, now, offset, fraction);
                items.add(frag);
                offset += fraction;
            }
        }
    }

    private IResource getResource() {
        IResource r = null;
        for (IClaim c: items) {
            if (r != null && !r.equals(c.getResource())) {
                throw new IllegalStateException(
                        "cannot apply RESOURCE_AMOUNT scaling to a swim lane with multiple resources");
            }
            r = c.getResource();
        }
        return r;
    }

    /**
     * Ordering ascending by timestamp, and start events before end events if same timestamp
     *
     * @return a list of events
     */
    private List<Event> getEvents() {
        List<Event> events = new ArrayList<Event>();
        for (IClaim claim: items) {
            events.add(new Event(claim, true));
            events.add(new Event(claim, false));
        }
        Collections.sort(events);
        return events;
    }

    private static class Event implements Comparable<Event> {
        private final boolean isStart;

        private final Number t;

        private final IClaim parent;

        public Event(IClaim parent, boolean isStart) {
            this.parent = parent;
            this.isStart = isStart;
            if (isStart) {
                this.t = parent.getStartTime();
            } else {
                this.t = parent.getEndTime();
            }
        }

        @Override
        public int compareTo(Event e) {
            int c = Double.compare(t.doubleValue(), e.t.doubleValue());
            if (c != 0) {
                return c;
            }
            // start events before end events if same timestamp
            return Boolean.compare(e.isStart, isStart);
        }
    }
}
