/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.vis.jfree.impl;

import java.util.Map;

import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IClaimEvent;
import org.eclipse.trace4cps.core.IResource;

/**
 * A UI specific claim implementation.
 */
public class ClaimFragment implements IClaim {
    private final IClaim wrapped;

    private final double t0;

    private final double t1;

    private final double fraction;

    private final double offset;

    public ClaimFragment(IClaim wrapped, double t0, double t1, double offset, double fraction) {
        this.wrapped = wrapped;
        this.t0 = t0;
        this.t1 = t1;
        this.offset = offset;
        this.fraction = fraction;
    }

    @Override
    public Number getStartTime() {
        return t0;
    }

    @Override
    public Number getEndTime() {
        return t1;
    }

    @Override
    public IResource getResource() {
        return wrapped.getResource();
    }

    @Override
    public Number getAmount() {
        return wrapped.getAmount();
    }

    @Override
    public String getAttributeValue(String key) {
        return wrapped.getAttributeValue(key);
    }

    @Override
    public Map<String, String> getAttributes() {
        return wrapped.getAttributes();
    }

    @Override
    public void setAttribute(String key, String value) {
        wrapped.setAttribute(key, value);
    }

    @Override
    public void clearAttributes() {
        wrapped.clearAttributes();
    }

    public IClaim getWrapped() {
        return wrapped;
    }

    @Override
    public Number getOffset() {
        return wrapped.getOffset();
    }

    public double getSwimLaneOffset() {
        return offset;
    }

    public double getSwimLaneFraction() {
        return fraction;
    }

    @Override
    public String toString() {
        return "ClaimFragment [t0=" + t0 + ", t1=" + t1 + ", wrapped=" + wrapped + "]";
    }

    @Override
    public IClaimEvent getStartEvent() {
        return wrapped.getStartEvent();
    }

    @Override
    public IClaimEvent getEndEvent() {
        return wrapped.getEndEvent();
    }
}
