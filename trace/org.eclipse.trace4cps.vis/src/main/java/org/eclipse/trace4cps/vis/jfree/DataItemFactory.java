/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.vis.jfree;

import org.eclipse.trace4cps.common.jfreechart.data.xy.XYEdgeDataItem;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IDependency;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.IPsopFragment;
import org.jfree.data.xy.XYDataItem;
import org.jfree.data.xy.XYIntervalDataItem;

public interface DataItemFactory {
    XYIntervalDataItem createClaimDataItem(IClaim claim, Number xmin, Number xmax, Number ymin, Number ymax);

    XYEdgeDataItem createEventDataItem(IEvent event, Number x, Number y0, Number y1);

    XYEdgeDataItem createDependencyDataItem(IDependency dep, Number x0, Number y0, Number x1, Number y1);

    XYDataItem createSignalDataItem(IPsop p, IPsopFragment f, Number x, Number y);
}
