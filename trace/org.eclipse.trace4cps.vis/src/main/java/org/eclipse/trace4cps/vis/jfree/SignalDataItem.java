/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.vis.jfree;

import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.IPsopFragment;
import org.jfree.data.xy.XYDataItem;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class SignalDataItem extends XYDataItem {
    private static final long serialVersionUID = 4016661192502965523L;

    private final IPsopFragment f;

    SignalDataItem(double x, double y, IPsop p, IPsopFragment f) {
        super(x, y);
        this.f = f;
    }

    public IPsopFragment getFragment() {
        return f;
    }

    public static SignalDataItem getFrom(XYDataset ds, int series, int item) {
        if (ds instanceof XYSeriesCollection) {
            XYSeriesCollection sc = (XYSeriesCollection)ds;
            XYSeries s = sc.getSeries(series);
            Object o = s.getDataItem(item);
            if (o instanceof SignalDataItem) {
                return (SignalDataItem)o;
            }
        }
        return null;
    }
}
