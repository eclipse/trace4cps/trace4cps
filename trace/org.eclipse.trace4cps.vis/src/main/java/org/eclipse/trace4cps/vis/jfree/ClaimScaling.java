/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.vis.jfree;

/**
 * Determines how overlapping claims in swim lanes are handled. None means that Full means that Resource amount means
 * that the
 */
public enum ClaimScaling {
    /**
     * Each claim gets the full height of the swim lane and thus overlaps are not easily visible. This is the default
     * and computationally cheapest mode.
     */
    NONE,

    /**
     * Overlapping claims each get an equal part of the swimlane height so that the full swim lane height is used.
     */
    FULL,

    /**
     * This only makes sense if all claims in a swimlane use the same resource. Overlapping claims each get an amount of
     * the height corresponding to the amount of resource that they have claimed. The height of the swimlane corresponds
     * to the capacity of the resource.
     */
    RESOURCE_AMOUNT;
}
