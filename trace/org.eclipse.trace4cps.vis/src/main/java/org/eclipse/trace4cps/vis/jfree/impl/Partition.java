/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.vis.jfree.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.trace4cps.core.IAttributeAware;
import org.eclipse.trace4cps.core.impl.TraceHelper;

public class Partition<T extends IAttributeAware> implements Iterable<Cell<T>> {
    protected final List<Cell<T>> cells = new ArrayList<>();

    public static <T extends IAttributeAware> Partition<T> createAttributePartition(List<T> aa,
            Collection<String> atts)
    {
        Partition<T> partition = new Partition<>();
        Map<String, Cell<T>> m = new HashMap<>();
        for (T a: aa) {
            String attVals = TraceHelper.getValues(a, atts, false);
            Cell<T> cell = m.get(attVals);
            if (cell == null) {
                cell = partition.createCell(attVals);
                m.put(attVals, cell);
            }
            cell.add(a);
        }
        return partition;
    }

    private Cell<T> createCell(String description) {
        Cell<T> c = new Cell<T>(description);
        cells.add(c);
        return c;
    }

    protected Partition() {
    }

    public int size() {
        return cells.size();
    }

    public Cell<T> get(int i) {
        return cells.get(i);
    }

    public int getIndex(Cell<T> c) {
        return cells.indexOf(c);
    }

    public void sortCells() {
        Collections.sort(cells, new Comparator<Cell<T>>() {
            @Override
            public int compare(Cell<T> o1, Cell<T> o2) {
                return o2.getDescription().compareTo(o1.getDescription());
            }
        });
    }

    @Override
    public Iterator<Cell<T>> iterator() {
        return cells.iterator();
    }
}
