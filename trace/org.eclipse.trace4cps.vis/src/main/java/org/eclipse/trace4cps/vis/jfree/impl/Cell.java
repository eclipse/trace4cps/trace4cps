/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.vis.jfree.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.trace4cps.core.IAttributeAware;

public class Cell<T extends IAttributeAware> implements Iterable<T> {
    private final String description;

    protected final List<T> items = new ArrayList<>();

    Cell(String description) {
        this.description = description;
    }

    @Override
    public Iterator<T> iterator() {
        return items.iterator();
    }

    public int size() {
        return items.size();
    }

    public String getDescription() {
        return description;
    }

    public void add(T a) {
        items.add(a);
    }

    public T get(int i) {
        return items.get(i);
    }
}
