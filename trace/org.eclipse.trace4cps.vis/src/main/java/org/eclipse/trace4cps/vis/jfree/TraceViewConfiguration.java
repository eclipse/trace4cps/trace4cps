/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.vis.jfree;

import java.awt.Paint;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.trace4cps.core.IAttributeAware;
import org.eclipse.trace4cps.core.IAttributeFilter;
import org.eclipse.trace4cps.core.TracePart;
import org.jfree.data.Range;

/**
 * Defines the view on the trace data that is created by the {@link TracePlotManager}.
 */
public class TraceViewConfiguration {
    private boolean showClaims = true;

    private boolean showEvents = true;

    private boolean showClaimEvents = false;

    private boolean showDependencies = true;

    private boolean showSignals = true;

    private boolean showSignalMarkers = false;

    private boolean showClaimLabels = false;

    private final List<DisjunctionFilter> claimFilters = new ArrayList<>();

    private final List<DisjunctionFilter> eventFilters = new ArrayList<>();

    private final List<DisjunctionFilter> resourceFilters = new ArrayList<>();

    private final List<DisjunctionFilter> dependencyFilters = new ArrayList<>();

    private final List<DisjunctionFilter> signalFilters = new ArrayList<>();

    private boolean isResourceView = false;

    private ClaimScaling claimScaling = ClaimScaling.NONE;

    private Map<TracePart, Collection<String>> describingAttributes = new HashMap<>();

    private Map<TracePart, Collection<String>> groupingAttributes = new HashMap<>();

    private Map<TracePart, Collection<String>> colorAttributes = new HashMap<>();

    private Range range = null;

    private Map<IAttributeAware, Paint> highlightMap = new HashMap<>();

    public void setRange(Range r) {
        this.range = r;
    }

    public Range getRange() {
        return range;
    }

    public boolean isActivityView() {
        return !isResourceView;
    }

    public void setActivityView() {
        claimScaling = ClaimScaling.NONE;
        isResourceView = false;
    }

    public void setResourceView() {
        claimScaling = ClaimScaling.RESOURCE_AMOUNT;
        isResourceView = true;
    }

    public boolean showClaims() {
        return showClaims;
    }

    public void setShowClaims(boolean showClaims) {
        this.showClaims = showClaims;
    }

    public boolean showEvents() {
        return showEvents;
    }

    public void setShowEvents(boolean showEvents) {
        this.showEvents = showEvents;
    }

    public boolean showClaimEvents() {
        return showClaimEvents;
    }

    public void setShowClaimEvents(boolean showClaimEvents) {
        this.showClaimEvents = showClaimEvents;
    }

    public boolean showDependencies() {
        return showDependencies;
    }

    public void setShowDependencies(boolean showDependencies) {
        this.showDependencies = showDependencies;
    }

    public boolean showSignals() {
        return showSignals;
    }

    public void setShowSignals(boolean showSignals) {
        this.showSignals = showSignals;
    }

    public boolean getShowSignalMarkers() {
        return showSignalMarkers;
    }

    public void setShowSignalMarkers(boolean b) {
        showSignalMarkers = b;
    }

    public boolean getShowClaimLabels() {
        return showClaimLabels;
    }

    public void setShowClaimLabels(boolean b) {
        showClaimLabels = b;
    }

    public Collection<String> getDescribingAttributes(TracePart part) {
        return describingAttributes.get(part);
    }

    public void setDescribingAttributes(TracePart part, Collection<String> atts) {
        this.describingAttributes.put(part, atts);
    }

    public Collection<String> getGroupingAttributes(TracePart part) {
        return groupingAttributes.get(part);
    }

    public void setGroupingAttributes(TracePart part, Collection<String> atts) {
        this.groupingAttributes.put(part, atts);
    }

    public Collection<String> getColoringAttributes(TracePart part) {
        return colorAttributes.get(part);
    }

    public void setColoringAttributes(TracePart part, Collection<String> atts) {
        this.colorAttributes.put(part, atts);
    }

    public boolean hasHighlight() {
        return highlightMap != null && !highlightMap.isEmpty();
    }

    /**
     * @return a mapping of IClaims and IEvents to colors
     */
    public Map<IAttributeAware, Paint> getHighlightMap() {
        return highlightMap;
    }

    public void setHighlightMap(Map<IAttributeAware, Paint> highlightMap) {
        this.highlightMap = highlightMap;
    }

    public void clearHighlight() {
        highlightMap = new HashMap<>();
    }

    public ClaimScaling getClaimScaling() {
        return claimScaling;
    }

    public void setClaimScaling(ClaimScaling claimScaling) {
        this.claimScaling = claimScaling;
    }

    public void clearFilters(TracePart part) {
        switch (part) {
            case ALL:
                claimFilters.clear();
                eventFilters.clear();
                resourceFilters.clear();
                dependencyFilters.clear();
                signalFilters.clear();
                break;
            case CLAIM:
                claimFilters.clear();
                break;
            case EVENT:
                eventFilters.clear();
                break;
            case RESOURCE:
                resourceFilters.clear();
                break;
            case DEPENDENCY:
                dependencyFilters.clear();
                break;
            case SIGNAL:
                signalFilters.clear();
                break;
            default:
                throw new IllegalStateException();
        }
    }

    /**
     * Adds a filter that cannot be persisted by the {@link TraceViewConfigurationIO} class. Typically, results of
     * analysis methods use this to filter an analysis result.
     */
    public void addNonPersistentFilter(TracePart part, IAttributeFilter f) {
        DisjunctionFilter df = new DisjunctionFilter();
        df.addNonPersistent(f);
        addFilter(part, df);
    }

    public void addFilter(TracePart part, String att, List<String> includedValues) {
        DisjunctionFilter f = new DisjunctionFilter();
        f.add(new SimpleFilter(att, includedValues));
        addFilter(part, f);
    }

    public void addFilter(TracePart part, DisjunctionFilter f) {
        switch (part) {
            case CLAIM:
                claimFilters.add(f);
                break;
            case EVENT:
                eventFilters.add(f);
                break;
            case RESOURCE:
                resourceFilters.add(f);
                break;
            case DEPENDENCY:
                dependencyFilters.add(f);
                break;
            case SIGNAL:
                signalFilters.add(f);
                break;
            default:
                throw new IllegalStateException();
        }
    }

    public boolean hasFilters() {
        return !claimFilters.isEmpty() || !eventFilters.isEmpty() || !resourceFilters.isEmpty()
                || !dependencyFilters.isEmpty() || !signalFilters.isEmpty();
    }

    public List<DisjunctionFilter> getFilters(TracePart part) {
        switch (part) {
            case CLAIM:
                return claimFilters;
            case EVENT:
                return eventFilters;
            case RESOURCE:
                return resourceFilters;
            case DEPENDENCY:
                return dependencyFilters;
            case SIGNAL:
                return signalFilters;
            default:
                throw new IllegalStateException();
        }
    }

    /**
     * A simple filter that has an attribute name and a set of allowed values.
     */
    public static final class SimpleFilter implements IAttributeFilter {
        private final String attName;

        private final Set<String> includedValues = new HashSet<>();

        public SimpleFilter(String attName, Collection<String> includedValues) {
            this.attName = attName;
            this.includedValues.addAll(includedValues);
        }

        public String getAttName() {
            return attName;
        }

        public Set<String> getIncludedValues() {
            return includedValues;
        }

        @Override
        public boolean include(IAttributeAware a) {
            String val = a.getAttributeValue(attName);
            return val != null && includedValues.contains(val);
        }

        @Override
        public String toString() {
            return attName + "=" + includedValues;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((attName == null) ? 0 : attName.hashCode());
            result = prime * result + ((includedValues == null) ? 0 : includedValues.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            SimpleFilter other = (SimpleFilter)obj;
            if (attName == null) {
                if (other.attName != null)
                    return false;
            } else if (!attName.equals(other.attName))
                return false;
            if (includedValues == null) {
                if (other.includedValues != null)
                    return false;
            } else if (!includedValues.equals(other.includedValues))
                return false;
            return true;
        }
    }

    /**
     * This filter aggregates a number of {@link SimpleFilter} instances. The filter returns {@code true} if and only if
     * one of the {@link SimpleFilter} instances returns {@code true} for a given {@link IAttributeAware} instance.
     */
    public static final class DisjunctionFilter implements IAttributeFilter {
        private final Set<SimpleFilter> filters = new HashSet<>();

        private final Set<IAttributeFilter> nonPersistentFilters = new HashSet<>();

        public void add(SimpleFilter f) {
            filters.add(f);
        }

        public void addNonPersistent(IAttributeFilter f) {
            nonPersistentFilters.add(f);
        }

        public Set<SimpleFilter> getPersistentFilters() {
            return filters;
        }

        @Override
        public boolean include(IAttributeAware a) {
            for (IAttributeFilter f: filters) {
                if (f.include(a)) {
                    return true;
                }
            }
            for (IAttributeFilter f: nonPersistentFilters) {
                if (f.include(a)) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public String toString() {
            return "filters=" + filters + ", nonPersistent=" + nonPersistentFilters;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((filters == null) ? 0 : filters.hashCode());
            result = prime * result + ((nonPersistentFilters == null) ? 0 : nonPersistentFilters.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            DisjunctionFilter other = (DisjunctionFilter)obj;
            if (filters == null) {
                if (other.filters != null)
                    return false;
            } else if (!filters.equals(other.filters))
                return false;
            if (nonPersistentFilters == null) {
                if (other.nonPersistentFilters != null)
                    return false;
            } else if (!nonPersistentFilters.equals(other.nonPersistentFilters))
                return false;
            return true;
        }
    }
}
