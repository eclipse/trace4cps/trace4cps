/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.vis.jfree.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.impl.TraceHelper;
import org.eclipse.trace4cps.vis.jfree.ClaimScaling;

public class ClaimPartition extends Partition<IClaim> {
    public static ClaimPartition createActivityPartition(List<IClaim> claims, Collection<String> atts) {
        ClaimPartition partition = new ClaimPartition();
        Map<String, ClaimCell> m = new HashMap<>();
        for (IClaim a: claims) {
            String attVals = TraceHelper.getValues(a, atts, false);
            ClaimCell cell = m.get(attVals);
            if (cell == null) {
                cell = partition.createCell(attVals);
                m.put(attVals, cell);
            }
            cell.add(a);
        }
        return partition;
    }

    public static ClaimPartition createResourcePartition(List<IClaim> claims, Collection<String> atts) {
        ClaimPartition partition = new ClaimPartition();
        Map<IResource, ClaimCell> m = new HashMap<>();
        for (IClaim claim: claims) {
            IResource r = claim.getResource();
            String attVals = TraceHelper.getValues(r, atts, false);
            ClaimCell cell = m.get(r);
            if (cell == null) {
                cell = partition.createCell(attVals);
                m.put(r, cell);
            }
            cell.add(claim);
        }
        return partition;
    }

    private ClaimPartition() {
    }

    private ClaimCell createCell(String description) {
        ClaimCell c = new ClaimCell(description);
        cells.add(c);
        return c;
    }

    public void scaleClaims(ClaimScaling scaling) {
        if (scaling == ClaimScaling.NONE) {
            return;
        }
        for (Cell<IClaim> cell: cells) {
            ((ClaimCell)cell).scaleClaims(scaling);
        }
    }
}
