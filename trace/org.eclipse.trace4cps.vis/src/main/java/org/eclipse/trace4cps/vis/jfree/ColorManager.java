/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.vis.jfree;

import java.awt.Color;

import org.eclipse.trace4cps.core.IAttributeAware;
import org.jfree.chart.ChartColor;

/**
 * A color palette, consisting of 36 automatic colors 10 shades of red and 6 manual colors.
 */
public enum ColorManager {
    // Automatic colors
    RED(new Color(0xFF, 0x55, 0x55)), BLUE(new Color(0x55, 0x55, 0xFF)), GREEN(new Color(0x55, 0xFF, 0x55)),
    YELLOW(new Color(0xFF, 0xFF, 0x55)), MAGENTA(new Color(0xFF, 0x55, 0xFF)), CYAN(new Color(0x55, 0xFF, 0xFF)),
    PINK(Color.PINK), DARK_RED(ChartColor.DARK_RED), DARK_BLUE(ChartColor.DARK_BLUE), DARK_GREEN(ChartColor.DARK_GREEN),
    DARK_YELLOW(ChartColor.DARK_YELLOW), DARK_MAGENTA(ChartColor.DARK_MAGENTA), DARK_CYAN(ChartColor.DARK_CYAN),
    LIGHT_RED(ChartColor.LIGHT_RED), LIGHT_BLUE(ChartColor.LIGHT_BLUE), LIGHT_GREEN(ChartColor.LIGHT_GREEN),
    LIGHT_YELLOW(ChartColor.LIGHT_YELLOW), LIGHT_MAGENTA(ChartColor.LIGHT_MAGENTA), LIGHT_CYAN(ChartColor.LIGHT_CYAN),
    VERY_DARK_RED(ChartColor.VERY_DARK_RED), VERY_DARK_BLUE(ChartColor.VERY_DARK_BLUE),
    VERY_DARK_GREEN(ChartColor.VERY_DARK_GREEN), VERY_DARK_YELLOW(ChartColor.VERY_DARK_YELLOW),
    VERY_DARK_MAGENTA(ChartColor.VERY_DARK_MAGENTA), VERY_DARK_CYAN(ChartColor.VERY_DARK_CYAN),
    VERY_LIGHT_RED(ChartColor.VERY_LIGHT_RED), VERY_LIGHT_BLUE(ChartColor.VERY_LIGHT_BLUE),
    VERY_LIGHT_GREEN(ChartColor.VERY_LIGHT_GREEN), VERY_LIGHT_YELLOW(ChartColor.VERY_LIGHT_YELLOW),
    VERY_LIGHT_MAGENTA(ChartColor.VERY_LIGHT_MAGENTA), VERY_LIGHT_CYAN(ChartColor.VERY_LIGHT_CYAN),
    TURQUOISE(new Color(0x40, 0xE0, 0xD0)), ORANGE(new Color(0xFF, 0xA5, 0x00)), SIENNA(new Color(0xA0, 0x52, 0x2D)),
    PURPLE(new Color(0x80, 0x00, 0x80)), SAND(new Color(0xFF, 0xC0, 0x70)), DARK_GRAY(Color.DARK_GRAY),

    // Ten shades of red, going from close to grey to bright red.
    RED_SHADE_00(new Color(0x69, 0x69, 0x82)), RED_SHADE_01(new Color(0x78, 0x5F, 0x75)),
    RED_SHADE_02(new Color(0x87, 0x55, 0x68)), RED_SHADE_03(new Color(0x96, 0x4B, 0x5B)),
    RED_SHADE_04(new Color(0xA5, 0x41, 0x4E)), RED_SHADE_05(new Color(0xB4, 0x37, 0x41)),
    RED_SHADE_06(new Color(0xC3, 0x2D, 0x34)), RED_SHADE_07(new Color(0xD2, 0x23, 0x27)),
    RED_SHADE_08(new Color(0xE1, 0x19, 0x1A)), RED_SHADE_09(new Color(0xF0, 0x0F, 0x0D)),
    RED_SHADE_10(new Color(0xFF, 0x05, 0x00)),

    // Manual colors
    // TODO: This color is a duplicate of 'light_blue', is this required?
    HALF_BLUE(ChartColor.LIGHT_BLUE), RED_PURE(Color.RED), LIGHT_GRAY(Color.LIGHT_GRAY), GRAY(Color.GRAY),
    BLACK(Color.BLACK), WHITE(Color.WHITE);

    public static final String COLOR_ATTRIBUTE = "color";

    /** A special constant mapping {@link #NON_HIGHLIGHT} to {@link #RED_SHADE_00}. */
    public static final ColorManager NON_HIGHLIGHT = ColorManager.RED_SHADE_00;

    private static final int NUM_AUTOMATIC_COLORS = ColorManager.DARK_GRAY.ordinal() + 1;

    private final Color color;

    private ColorManager(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public static ColorManager getAutomaticColor(String colorKey) {
        return getAutomaticColor(colorKey.hashCode());
    }

    public static ColorManager getAutomaticColor(int colorIndex) {
        return values()[Math.abs(colorIndex) % NUM_AUTOMATIC_COLORS];
    }

    /**
     * Returns a shade of red of which its intensity depends on a {@code weight} in the range of (0.0 - 1.0).
     *
     * @param weight a value in the range of (0.0 - 1.0)
     * @return one of {@link #RED_SHADE_00} to {@link #RED_SHADE_10}
     */
    public static ColorManager getShadeOfRed(float weight) {
        int offset = Math.round(weight * 10);
        if (offset < 0) {
            offset = 0;
        }
        if (offset > 10) {
            offset = 10;
        }
        return values()[RED_SHADE_00.ordinal() + offset];
    }

    public static ColorManager getColor(IAttributeAware iaa) {
        return getColor(iaa, null);
    }

    public static ColorManager getColor(IAttributeAware iaa, ColorManager defaultColor) {
        String color = iaa.getAttributeValue(COLOR_ATTRIBUTE);
        if (color == null) {
            return defaultColor;
        }
        try {
            return Enum.valueOf(ColorManager.class, color.toUpperCase());
        } catch (IllegalArgumentException e) {
            return defaultColor;
        }
    }
}
