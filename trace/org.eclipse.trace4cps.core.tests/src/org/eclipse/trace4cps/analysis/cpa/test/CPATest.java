/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.cpa.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.trace4cps.analysis.constraintgraph.ConstraintConfig;
import org.eclipse.trace4cps.analysis.cpa.CpaResult;
import org.eclipse.trace4cps.analysis.cpa.CriticalPathAnalysis;
import org.eclipse.trace4cps.analysis.cpa.DependencyProvider;
import org.eclipse.trace4cps.analysis.cpa.PositiveCycleException;
import org.eclipse.trace4cps.core.IDependency;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.impl.Claim;
import org.eclipse.trace4cps.core.impl.Resource;
import org.eclipse.trace4cps.core.impl.Trace;
import org.junit.jupiter.api.Test;

public final class CPATest {
    @Test
    public void testCPAsimple() throws PositiveCycleException {
        ITrace trace = createDummyModel();
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setAddSourceAndSink(true);
        cfg.setApplyOrderingHeuristic(0);
        checkCritical(CriticalPathAnalysis.run(trace, cfg), "A", "B", "D", "E");
    }

    @Test
    public void testCPAsimpleBlocked() throws PositiveCycleException {
        ITrace trace = createDummyModel();
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setAddSourceAndSink(true);
        cfg.setApplyOrderingHeuristic(0);
        CpaResult result = CriticalPathAnalysis.run(trace, cfg);
        checkCritical(result, "A", "B", "D", "E");

        DependencyProvider p = new DependencyProvider() {
            @Override
            public boolean isApplicationDependency(IEvent src, IEvent dst) {
                if (src.getAttributeValue("name").equals("A") && dst.getAttributeValue("name").equals("C")) {
                    return true;
                }
                if (src.getAttributeValue("name").equals("B") && dst.getAttributeValue("name").equals("D")) {
                    return true;
                }
                if (src.getAttributeValue("name").equals("D") && dst.getAttributeValue("name").equals("E")) {
                    return true;
                }
                return false;
            }
        };
        checkBlocked(result, p, "D");
    }

    @Test
    public void testCPAsimpleBlocked2() throws PositiveCycleException {
        ITrace trace = createDummyModel7();
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setAddSourceAndSink(true);
        cfg.setApplyOrderingHeuristic(0);
        CpaResult result = CriticalPathAnalysis.run(trace, cfg);
        checkCritical(result, "A", "B", "C", "D");

        DependencyProvider p = new DependencyProvider() {
            @Override
            public boolean isApplicationDependency(IEvent src, IEvent dst) {
                if (src.getAttributeValue("name").equals("B") && dst.getAttributeValue("name").equals("D")) {
                    return true;
                }
                if (src.getAttributeValue("name").equals("C") && dst.getAttributeValue("name").equals("D")) {
                    return true;
                }
                return false;
            }
        };
        checkBlocked(result, p, "B", "C");
    }

    @Test
    public void testCPAsimple2_epsilon() throws PositiveCycleException {
        ITrace trace = createDummyModel2();
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setApplyOrderingHeuristic(1);
        cfg.setAddSourceAndSink(true);
        checkCritical(CriticalPathAnalysis.run(trace, cfg), "A", "D", "E");
    }

    @Test
    public void testCPAsimple3_strict() throws PositiveCycleException {
        ITrace trace = createDummyModel3();
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setApplyOrderingHeuristic(0);
        cfg.setAddSourceAndSink(true);
        checkCritical(CriticalPathAnalysis.run(trace, cfg), "B", "E");
    }

    @Test
    public void testCPAsimple3_relaxed() throws PositiveCycleException {
        ITrace trace = createDummyModel3();
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setApplyOrderingHeuristic(Double.POSITIVE_INFINITY);
        cfg.setAddSourceAndSink(true);
        checkCritical(CriticalPathAnalysis.run(trace, cfg), "B", "D", "E");
    }

    @Test
    public void testCPAsimple3_epsilon() throws PositiveCycleException {
        ITrace trace = createDummyModel3();
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setApplyOrderingHeuristic(3);
        cfg.setAddSourceAndSink(true);
        checkCritical(CriticalPathAnalysis.run(trace, cfg), "B", "D", "E");
    }

    @Test
    public void testCPA_nonZeroStartTime() throws PositiveCycleException {
        ITrace trace = createDummyModel4();
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setApplyOrderingHeuristic(0);
        cfg.setAddSourceAndSink(true);
        checkCritical(CriticalPathAnalysis.run(trace, cfg), "A", "B");
    }

    @Test
    public void testCPA_nonRelatedTasks() throws PositiveCycleException {
        ITrace trace = createDummyModel5();
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setApplyOrderingHeuristic(0);
        cfg.setAddSourceAndSink(true);
        checkCritical(CriticalPathAnalysis.run(trace, cfg), "A", "C");
    }

    @Test
    public void testCPA_startDelay() throws PositiveCycleException {
        ITrace trace = createDummyModel6();
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setApplyOrderingHeuristic(0);
        cfg.setAddSourceAndSink(true);
        checkCritical(CriticalPathAnalysis.run(trace, cfg), "A", "C");
    }

    @Test
    public void testCPA_startDelay2() throws PositiveCycleException {
        ITrace trace = createDummyModel6();
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setApplyOrderingHeuristic(100);
        cfg.setAddSourceAndSink(true);
        checkCritical(CriticalPathAnalysis.run(trace, cfg), "A", "C");
    }

    @Test
    public void testCPA_cyclesStartEnd() throws PositiveCycleException {
        ITrace trace = createDummyModel8();
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setApplyOrderingHeuristic(0);
        cfg.setAddSourceAndSink(true);
        checkCritical(CriticalPathAnalysis.run(trace, cfg), "A", "B", "C", "D", "E");
    }

    private void checkCritical(CpaResult r, String... names) {
        Set<String> critical = new HashSet<String>();
        Set<String> given = new HashSet<String>();
        for (IDependency dep: r.getCriticalDeps()) {
            critical.add(dep.getSrc().getAttributeValue("name"));
            critical.add(dep.getDst().getAttributeValue("name"));
        }
        for (String n: names) {
            given.add(n);
        }
        assertEquals(given, critical);
    }

    private void checkBlocked(CpaResult r, DependencyProvider p, String... names) {
        Set<String> blocked = new HashSet<String>();
        Set<String> given = new HashSet<String>();
        for (IDependency d: r.getCriticalDeps()) {
            if (CpaResult.isNonAppDependency(d, p)) {
                blocked.add(d.getDst().getAttributeValue("name"));
            }
        }
        for (String n: names) {
            given.add(n);
        }
        assertEquals(given, blocked);
    }

    public static ITrace createDummyModel() {
        Trace trace = new Trace();
        IResource r = new Resource(10, false);
        trace.add(new Claim(0, 5, r, 1, from("name", "A")));
        trace.add(new Claim(0, 5, r, 1, from("name", "B")));
        trace.add(new Claim(5, 8, r, 1, from("name", "C")));
        trace.add(new Claim(5, 10, r, 1, from("name", "D")));
        trace.add(new Claim(10, 15, r, 1, from("name", "E")));
        return trace;
    }

    public static ITrace createDummyModel2() {
        Trace trace = new Trace();
        IResource r = new Resource(10, false);
        trace.add(new Claim(1, 6, r, 1, from("name", "A")));
        trace.add(new Claim(0, 5, r, 1, from("name", "B")));
        trace.add(new Claim(7, 10, r, 1, from("name", "C")));
        trace.add(new Claim(7, 12, r, 1, from("name", "D")));
        trace.add(new Claim(13, 18, r, 1, from("name", "E")));
        return trace;
    }

    public static ITrace createDummyModel3() {
        Trace trace = new Trace();
        IResource r = new Resource(10, false);
        trace.add(new Claim(2, 6, r, 1, from("name", "A")));
        trace.add(new Claim(0, 5, r, 1, from("name", "B")));
        trace.add(new Claim(7, 8, r, 1, from("name", "C")));
        trace.add(new Claim(7, 11, r, 1, from("name", "D")));
        trace.add(new Claim(14, 19, r, 1, from("name", "E")));
        return trace;
    }

    public static ITrace createDummyModel4() {
        Trace trace = new Trace();
        IResource r = new Resource(10, false);
        trace.add(new Claim(10, 12, r, 1, from("name", "A")));
        trace.add(new Claim(12, 15, r, 1, from("name", "B")));
        trace.add(new Claim(11, 14, r, 1, from("name", "C")));
        return trace;
    }

    public static ITrace createDummyModel5() {
        Trace trace = new Trace();
        IResource r = new Resource(10, false);
        trace.add(new Claim(2, 5, r, 1, from("name", "A")));
        trace.add(new Claim(8, 9, r, 1, from("name", "B")));
        trace.add(new Claim(11, 14, r, 1, from("name", "C")));
        return trace;
    }

    public static ITrace createDummyModel6() {
        Trace trace = new Trace();
        IResource r = new Resource(10, false);
        trace.add(new Claim(10, 20, r, 1, from("name", "A")));
        trace.add(new Claim(15, 20, r, 1, from("name", "B")));
        trace.add(new Claim(20, 30, r, 1, from("name", "C")));
        return trace;
    }

    public static ITrace createDummyModel7() {
        Trace trace = new Trace();
        IResource r = new Resource(10, false);
        trace.add(new Claim(0, 5, r, 1, from("name", "A")));
        trace.add(new Claim(5, 10, r, 1, from("name", "B")));
        trace.add(new Claim(5, 10, r, 1, from("name", "C")));
        trace.add(new Claim(10, 15, r, 1, from("name", "D")));
        return trace;
    }

    public static ITrace createDummyModel8() {
        Trace trace = new Trace();
        IResource r = new Resource(10, false);
        trace.add(new Claim(10, 10, r, 1, from("name", "A")));
        trace.add(new Claim(10, 10, r, 1, from("name", "B")));
        trace.add(new Claim(10, 30, r, 1, from("name", "C")));
        trace.add(new Claim(30, 30, r, 1, from("name", "D")));
        trace.add(new Claim(30, 30, r, 1, from("name", "E")));
        return trace;
    }

    private static Map<String, String> from(String... ss) {
        if (ss == null || ss.length == 0 || ss.length % 2 != 0) {
            throw new IllegalArgumentException();
        }
        Map<String, String> r = new HashMap<String, String>();
        for (int i = 0; i < ss.length; i += 2) {
            r.put(ss[i], ss[i + 1]);
        }
        return r;
    }
}
