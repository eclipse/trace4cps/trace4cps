/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.behavior.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.trace4cps.analysis.behavior.impl.FilteringClaimRepresentation;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.impl.Claim;
import org.eclipse.trace4cps.core.impl.Resource;
import org.junit.jupiter.api.Test;

public class FilteringClaimRepresentationTest {
    private static final IResource R = new Resource(100, false);

    private static final Map<Integer, Integer> M1 = new HashMap<Integer, Integer>();

    private static final Set<String> U0 = new HashSet<String>();

    private static final Set<String> U1 = new HashSet<String>();

    static {
        R.setAttribute("name", "CPU");

        M1.put(1, 23);
        M1.put(2, 25);
        M1.put(3, 38);

        U1.add("type");
    }

    @Test
    public void testRemap() {
        FilteringClaimRepresentation f = new FilteringClaimRepresentation("id", U0, M1);
        IClaim claim = new Claim(0, 10, R, 10, attMap("id", "1"));
        assertEquals("id=23;res={name=CPU};s", f.represent(claim.getStartEvent()));
    }

    @Test
    public void testUniqueness1() {
        FilteringClaimRepresentation f = new FilteringClaimRepresentation("id", U0, M1);
        IClaim claim = new Claim(0, 10, R, 10, attMap("id", "3", "type", "LOW"));
        assertEquals("id=38;res={name=CPU};e", f.represent(claim.getEndEvent()));
    }

    @Test
    public void testUniqueness2() {
        FilteringClaimRepresentation f = new FilteringClaimRepresentation("id", U1, M1);
        IClaim claim = new Claim(0, 10, R, 10, attMap("id", "3", "type", "LOW"));
        assertEquals("id=38;type=LOW;res={name=CPU};s", f.represent(claim.getStartEvent()));
    }

    private Map<String, String> attMap(String... strings) {
        Map<String, String> map = new HashMap<String, String>();
        if (strings != null) {
            for (int i = 0; i + 1 < strings.length; i += 2) {
                map.put(strings[i], strings[i + 1]);
            }
        }
        return map;
    }
}
