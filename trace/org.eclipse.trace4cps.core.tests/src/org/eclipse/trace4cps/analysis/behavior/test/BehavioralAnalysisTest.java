/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.behavior.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.trace4cps.analysis.behavior.BehavioralAnalysis;
import org.eclipse.trace4cps.analysis.behavior.BehavioralPartition;
import org.eclipse.trace4cps.analysis.behavior.HistogramEntry;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.impl.Claim;
import org.eclipse.trace4cps.core.impl.Resource;
import org.eclipse.trace4cps.core.impl.Trace;
import org.junit.jupiter.api.Test;

public class BehavioralAnalysisTest {
    @Test
    public void testPartition1() {
        // Identity of claims is decided by id, type: the three behaviors are not equal.
        BehavioralPartition p = BehavioralAnalysis.partition(getTrace1(), "id", Collections.singleton("type"));
        assertEquals(3, p.size());
    }

    @Test
    public void testPartition2() {
        // Identity of claims is decided by id and name only: two behaviors are
        // equal, and the third is different because A and B overlap
        BehavioralPartition p = BehavioralAnalysis.partition(getTrace1(), "id", Collections.singleton("name"));
        assertEquals(2, p.size());
    }

    @Test
    public void testHistogram1() {
        // Identity of claims is decided by id, type and resource: the three behaviors are not equal.
        BehavioralPartition p = BehavioralAnalysis.partition(getTrace1(), "id", Collections.singleton("type"));
        List<HistogramEntry> h = BehavioralAnalysis.createBehavioralHistogram(p);
        assertEquals(3, h.size());
        HistogramEntry e0 = h.get(0);
        HistogramEntry e1 = h.get(1);
        HistogramEntry e2 = h.get(2);
        // One of the entries: id, the other id and type
        assertTrue(e0.getCommonAttributeValues().size() > 0);
        assertTrue(e1.getCommonAttributeValues().size() > 0);
        assertTrue(e2.getCommonAttributeValues().size() > 0);
        assertEquals(5, e0.getCommonAttributeValues().size() + e1.getCommonAttributeValues().size()
                + e2.getCommonAttributeValues().size());
    }

    @Test
    public void testHistogram2() {
        // Identity of claims is decided by id and name: two behaviors are equal and one is different
        BehavioralPartition p = BehavioralAnalysis.partition(getTrace1(), "id", Collections.singleton("name"));
        List<HistogramEntry> h = BehavioralAnalysis.createBehavioralHistogram(p);
        assertEquals(2, h.size());
        HistogramEntry e0 = h.get(0);
        assertEquals(0, e0.getCommonAttributeValues().size());
        assertEquals(2, e0.size());
        HistogramEntry e1 = h.get(1);
        // id and type are equals
        assertEquals(1, e1.size());
        assertEquals(2, e1.getCommonAttributeValues().size());
    }

    /**
     * Creates a trace with three behaviors, and for each behavior 2 tasks.
     */
    private List<IEvent> getTrace1() {
        Trace trace = new Trace();
        IResource r1 = new Resource(10, false);
        IResource r2 = new Resource(10, false);

        trace.add(createODSEclaim(trace, "A", 3, "HIGH", 0, 5, r1));
        trace.add(createODSEclaim(trace, "B", 3, "HIGH", 5, 10, r2));

        trace.add(createODSEclaim(trace, "A", 8, "LOW", 12, 15, r1));
        trace.add(createODSEclaim(trace, "B", 8, "MED", 15, 20, r2));

        trace.add(createODSEclaim(trace, "A", 19, "LOW", 45, 55, r1));
        trace.add(createODSEclaim(trace, "B", 19, "LOW", 50, 60, r2));

        return trace.getEvents();
    }

    private IClaim createODSEclaim(Trace trace, String name, int id, String type, double t0, double t1, IResource r) {
        Map<String, String> m1 = new HashMap<String, String>();
        m1.put("name", name);
        m1.put("id", Integer.toString(id));
        m1.put("type", type);
        Claim c = new Claim(t0, t1, r, 1);
        c.setAttributes(m1);
        return c;
    }
}
