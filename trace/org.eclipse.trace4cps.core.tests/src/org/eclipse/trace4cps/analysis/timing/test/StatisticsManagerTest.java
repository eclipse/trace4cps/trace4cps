/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.timing.test;

import java.util.concurrent.TimeUnit;

import org.eclipse.trace4cps.analysis.timing.StatisticsManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StatisticsManagerTest {
    @Test
    public void testScale1() {
        StatisticsManager m = new StatisticsManager(TimeUnit.SECONDS);
        m.addSample(2d);
        m.scaleTo(TimeUnit.MILLISECONDS);
        Assertions.assertEquals(2000d, m.getSamples().get(0), 1e-12);
        Assertions.assertEquals(TimeUnit.MILLISECONDS, m.getTimeUnit());
    }

    @Test
    public void testScale2() {
        StatisticsManager m = new StatisticsManager(TimeUnit.MILLISECONDS);
        m.addSample(2000d);
        m.scaleTo(TimeUnit.SECONDS);
        Assertions.assertEquals(2d, m.getSamples().get(0), 1e-12);
        Assertions.assertEquals(TimeUnit.SECONDS, m.getTimeUnit());
    }

    @Test
    public void testAutoScale1() {
        StatisticsManager m = new StatisticsManager(TimeUnit.MILLISECONDS);
        m.addSample(120000d);
        m.autoScale();
        Assertions.assertEquals(120d, m.getSamples().get(0), 1e-12);
        Assertions.assertEquals(TimeUnit.SECONDS, m.getTimeUnit());
    }

    @Test
    public void testAutoScale2() {
        StatisticsManager m = new StatisticsManager(TimeUnit.MILLISECONDS);
        m.addSample(1200000d);
        m.autoScale();
        Assertions.assertEquals(20d, m.getSamples().get(0), 1e-12);
        Assertions.assertEquals(TimeUnit.MINUTES, m.getTimeUnit());
    }
}
