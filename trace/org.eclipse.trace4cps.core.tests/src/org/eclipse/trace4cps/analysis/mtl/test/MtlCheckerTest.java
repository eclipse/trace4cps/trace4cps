/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl.test;

import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.F;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.G;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.IMPLY;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.NOT;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.OR;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.U;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.eclipse.trace4cps.analysis.mtl.AtomicProposition;
import org.eclipse.trace4cps.analysis.mtl.DefaultAtomicProposition;
import org.eclipse.trace4cps.analysis.mtl.InformativePrefix;
import org.eclipse.trace4cps.analysis.mtl.MtlBuilder;
import org.eclipse.trace4cps.analysis.mtl.MtlChecker;
import org.eclipse.trace4cps.analysis.mtl.MtlException;
import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.mtl.MtlFuture;
import org.eclipse.trace4cps.analysis.mtl.MtlResult;
import org.eclipse.trace4cps.analysis.mtl.State;
import org.eclipse.trace4cps.analysis.mtl.check.test.DefaultState;
import org.eclipse.trace4cps.analysis.stl.StlBuilder;
import org.eclipse.trace4cps.analysis.stl.StlFormula;
import org.eclipse.trace4cps.core.ClaimEventType;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.impl.Interval;
import org.eclipse.trace4cps.core.impl.Psop;
import org.eclipse.trace4cps.core.impl.PsopFragment;
import org.eclipse.trace4cps.core.io.ParseException;
import org.eclipse.trace4cps.core.io.TraceReader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MtlCheckerTest {
    private MtlChecker c;

    @BeforeEach
    public void setup() {
        c = new MtlChecker(1);
    }

    @Test
    public void test1() throws MtlException {
        List<State> trace = new ArrayList<State>();
        trace.add(new DefaultState(0));
        trace.add(new DefaultState(1, "a", "a"));
        trace.add(new DefaultState(2, "a", "a"));
        trace.add(new DefaultState(3, "b", "b"));
        trace.add(new DefaultState(4, "a", "a"));
        MtlFormula phi = G(IMPLY(new DefaultAtomicProposition("a", "a"),
                F(new DefaultAtomicProposition("b", "b"), new Interval(0, false, 3, false))));
        assertEquals(InformativePrefix.BAD, c.check(trace, phi, null, false).informative());
    }

    @Test
    public void test2() throws MtlException {
        List<State> trace = new ArrayList<State>();
        trace.add(new DefaultState(0));
        trace.add(new DefaultState(1, "a", "a"));
        trace.add(new DefaultState(2, "a", "a"));
        trace.add(new DefaultState(3, "b", "b"));
        trace.add(new DefaultState(4, "a", "a"));
        MtlFormula phi = G(IMPLY(new DefaultAtomicProposition("a", "a"),
                F(new DefaultAtomicProposition("b", "b"), new Interval(0, false, 1, false))));
        assertEquals(InformativePrefix.BAD, c.check(trace, phi, null, false).informative());
    }

    @Test
    public void test3() throws MtlException {
        List<State> trace = new ArrayList<State>();
        trace.add(new DefaultState(0));
        trace.add(new DefaultState(1, "a", "a"));
        trace.add(new DefaultState(2, "a", "a"));
        trace.add(new DefaultState(3, "b", "b"));
        trace.add(new DefaultState(4, "a", "a"));
        trace.add(new DefaultState(10, "c", "c"));
        MtlFormula phi = F(new DefaultAtomicProposition("c", "c"), new Interval(0, false, 12, false));
        assertEquals(InformativePrefix.GOOD, c.check(trace, phi, null, false).informative());
    }

    @Test
    public void test4() throws MtlException {
        List<State> trace = new ArrayList<State>();
        trace.add(new DefaultState(0));
        trace.add(new DefaultState(1, "g", "g"));
        trace.add(new DefaultState(2));
        trace.add(new DefaultState(3, "g", "g"));
        trace.add(new DefaultState(4));
        AtomicProposition p = new DefaultAtomicProposition("g", "g");
        MtlFormula phi1 = G(IMPLY(p, F(p, new Interval(0, true, 2, false))));
        MtlFormula phi2 = G(IMPLY(p, OR(F(p, new Interval(0, true, 2, false)),
                G(NOT(p), new Interval(0, true, Double.POSITIVE_INFINITY, true)))));
        assertEquals(InformativePrefix.BAD, c.check(trace, phi1, null, false).informative());
        assertEquals(InformativePrefix.GOOD, c.check(trace, phi2, null, false).informative());
    }

    @Test
    public void testExplain1() throws MtlException {
        List<State> trace = new ArrayList<State>();
        trace.add(new DefaultState(0));
        trace.add(new DefaultState(1, "b", "b"));
        trace.add(new DefaultState(7, "a", "a"));
        AtomicProposition p = new DefaultAtomicProposition("a", "a");
        MtlFormula phi = F(G(NOT(p), new Interval(0, true, Double.POSITIVE_INFINITY, true)),
                new Interval(0, false, 5, false));
        MtlResult r = c.check(trace, phi, Collections.singleton(InformativePrefix.BAD), false);
        assertEquals(InformativePrefix.BAD, r.informative());
    }

    @Test
    public void testF1() throws MtlException {
        List<State> trace = new ArrayList<State>();
        trace.add(new DefaultState(0, "a"));
        trace.add(new DefaultState(3, "b"));
        AtomicProposition p = new DefaultAtomicProposition("b", "b");
        MtlFormula phi1 = F(p);
        assertEquals(InformativePrefix.GOOD, c.check(trace, phi1, null, false).informative());
    }

    @Test
    public void testF2() throws MtlException {
        List<State> trace = new ArrayList<State>();
        trace.add(new DefaultState(0, "a"));
        trace.add(new DefaultState(3, "b"));
        AtomicProposition p = new DefaultAtomicProposition("b", "b");
        MtlFormula phi1 = F(p, new Interval(2, false, 3, true));
        assertEquals(InformativePrefix.BAD, c.check(trace, phi1, null, false).informative());
    }

    @Test
    public void testF3() throws MtlException {
        List<State> trace = new ArrayList<State>();
        trace.add(new DefaultState(0, "a"));
        trace.add(new DefaultState(3, "b"));
        AtomicProposition p = new DefaultAtomicProposition("b", "b");
        MtlFormula phi1 = F(p, new Interval(3, true, 5, true));
        assertEquals(InformativePrefix.BAD, c.check(trace, phi1, null, false).informative());
    }

    @Test
    public void testUntil1() throws MtlException {
        List<State> trace = new ArrayList<State>();
        trace.add(new DefaultState(0, "a"));
        trace.add(new DefaultState(1, "a"));
        trace.add(new DefaultState(2, "a"));
        trace.add(new DefaultState(3, "b"));
        trace.add(new DefaultState(4, "b"));
        AtomicProposition a = new DefaultAtomicProposition("a", "a");
        AtomicProposition b = new DefaultAtomicProposition("b", "b");
        MtlFormula phi1 = U(a, b, Interval.trivial());
        assertEquals(InformativePrefix.GOOD, c.check(trace, phi1, null, false).informative());
    }

    @Test
    public void testUntil2() throws MtlException {
        List<State> trace = new ArrayList<State>();
        trace.add(new DefaultState(0, "a"));
        trace.add(new DefaultState(1, "a"));
        trace.add(new DefaultState(2, "a"));
        trace.add(new DefaultState(3, "b"));
        trace.add(new DefaultState(4, "b"));
        AtomicProposition a = new DefaultAtomicProposition("a", "a");
        AtomicProposition b = new DefaultAtomicProposition("b", "b");
        MtlFormula phi1 = U(a, b, new Interval(0, false, 3, true));
        assertEquals(InformativePrefix.BAD, c.check(trace, phi1, null, false).informative());
    }

    @Test
    public void testUntil3() throws MtlException {
        List<State> trace = new ArrayList<State>();
        trace.add(new DefaultState(0, "a"));
        trace.add(new DefaultState(1, "a"));
        trace.add(new DefaultState(2, "a"));
        trace.add(new DefaultState(3, "b"));
        trace.add(new DefaultState(4, "b"));
        AtomicProposition a = new DefaultAtomicProposition("a", "a");
        AtomicProposition b = new DefaultAtomicProposition("b", "b");
        MtlFormula phi1 = U(a, b, new Interval(2, false, 3, false));
        MtlResult r = c.check(trace, phi1, null, false);
        assertEquals(InformativePrefix.GOOD, r.informative());
    }

    @Test
    public void testUntil4() throws MtlException {
        List<State> trace = new ArrayList<State>();
        trace.add(new DefaultState(0, "a"));
        trace.add(new DefaultState(1, "a"));
        trace.add(new DefaultState(2));
        trace.add(new DefaultState(3, "b"));
        trace.add(new DefaultState(4, "b"));
        AtomicProposition a = new DefaultAtomicProposition("a", "a");
        AtomicProposition b = new DefaultAtomicProposition("b", "b");
        MtlFormula phi1 = U(a, b, new Interval(2, false, 3, false));
        assertEquals(InformativePrefix.BAD, c.check(trace, phi1, null, false).informative());
    }

    @Test
    public void testInformativePrefix1() throws MtlException {
        List<State> trace = new ArrayList<State>();
        trace.add(new DefaultState(0, "a"));
        trace.add(new DefaultState(1, "a"));
        AtomicProposition a = new DefaultAtomicProposition("a", "a");
        MtlFormula phi1 = G(a);
        assertEquals(InformativePrefix.NON_INFORMATIVE, c.check(trace, phi1, null, true).informative());
    }

    @Test
    public void testMix1() throws MtlException, ParseException {
        List<State> states = new ArrayList<State>();
        states.add(new DefaultState(0, "c", "c"));
        states.add(new DefaultState(1, "d", "d"));
        states.add(new DefaultState(2, "a", "a"));
        states.add(new DefaultState(3, "b", "b"));
        states.add(new DefaultState(4, "a", "a"));
        Psop signal = new Psop();
        signal.add(new PsopFragment(3, 1, 0, new Interval(0, 2.5)));
        signal.add(new PsopFragment(5.5, -1, 0, new Interval(2.5, 4)));

        StlFormula phi1 = StlBuilder.F(StlBuilder.GEQ(signal, 5), 0, 2);
        AtomicProposition event = new DefaultAtomicProposition("d", "d");
        MtlFormula phi = MtlBuilder.G(MtlBuilder.IMPLY(event, phi1));

        assertEquals(InformativePrefix.GOOD, c.check(states, phi, null, false).informative());
    }

    @Test
    public void testMix2() throws MtlException, ParseException {
        List<State> trace = new ArrayList<State>();
        trace.add(new DefaultState(0, "c", "c"));
        trace.add(new DefaultState(1, "d", "d"));
        trace.add(new DefaultState(2, "a", "a"));
        trace.add(new DefaultState(3, "b", "b"));
        trace.add(new DefaultState(4, "a", "a"));

        Psop signal = new Psop();
        signal.add(new PsopFragment(3, 1, 0, new Interval(0, 2.5)));
        signal.add(new PsopFragment(5.5, -1, 0, new Interval(2.5, 4)));

        StlFormula phi1 = StlBuilder.F(StlBuilder.GEQ(signal, 5), 0, 1);

        AtomicProposition event = new DefaultAtomicProposition("c", "c");
        MtlFormula phi = MtlBuilder.G(MtlBuilder.IMPLY(event, phi1));

        assertEquals(InformativePrefix.BAD, c.check(trace, phi, null, false).informative());
    }

    @Test
    public void testLatencyVerification()
            throws IOException, MtlException, InterruptedException, ExecutionException, ParseException
    {
        ITrace trace = TraceReader.readTrace(new File("../../trace-examples/odse/reference.etf"));

        List<MtlFormula> phis = new ArrayList<>();
        for (int i = 0; i < 200; i++) {
            AtomicProposition ap1 = new DefaultAtomicProposition(ClaimEventType.START, "name", "A", "id",
                    Integer.toString(i));
            AtomicProposition ap2 = new DefaultAtomicProposition(ClaimEventType.END, "name", "G", "id",
                    Integer.toString(i));
            MtlFormula phi = G(IMPLY(ap1, F(ap2, new Interval(0, false, 100, false))));
            phis.add(phi);
        }
        MtlFuture f = new MtlChecker().checkAll(trace.getEvents(), phis, Collections.emptySet(), false);
        List<MtlResult> results = f.get();
        assertEquals(200, results.size());
        for (MtlResult r: results) {
            assertEquals(InformativePrefix.GOOD, r.informative());
        }
    }
}
