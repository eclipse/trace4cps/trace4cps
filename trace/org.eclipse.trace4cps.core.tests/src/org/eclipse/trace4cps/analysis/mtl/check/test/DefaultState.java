/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl.check.test;

import java.util.Map;

import org.eclipse.trace4cps.analysis.mtl.AtomicProposition;
import org.eclipse.trace4cps.analysis.mtl.DefaultAtomicProposition;
import org.eclipse.trace4cps.analysis.mtl.State;
import org.eclipse.trace4cps.core.impl.AttributeAware;
import org.eclipse.trace4cps.core.impl.TraceHelper;

/**
 * A base implementation of the {@link State} interface. Properties are specified by a key-value mapping of strings.
 * This class is intended to work with {@link DefaultAtomicProposition} instances.
 */
public class DefaultState extends AttributeAware implements State {
    private final double ts;

    public DefaultState(double ts) {
        this.ts = ts;
    }

    public DefaultState(double ts, String keyValue) {
        this.ts = ts;
        setAttribute(keyValue, keyValue);
    }

    public DefaultState(double ts, String key, String value) {
        this.ts = ts;
        setAttribute(key, value);
    }

    public DefaultState(double ts, Map<String, String> props) {
        this.ts = ts;
        for (Map.Entry<String, String> e: props.entrySet()) {
            setAttribute(e.getKey(), e.getValue());
        }
    }

    public boolean satisfies(AtomicProposition p) {
        return TraceHelper.matches(p, this);
    }

    public Number getTimestamp() {
        return ts;
    }

    @Override
    public String toString() {
        return "DefaultState [ts=" + ts + ", props=" + getAttributes() + "]";
    }
}
