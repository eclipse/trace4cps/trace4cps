/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl.check.test;

import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.F;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.G;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.IMPLY;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.OR;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.TRUE;
import static org.eclipse.trace4cps.analysis.mtl.check.TabularExplanationTable.MAYBE;
import static org.eclipse.trace4cps.analysis.mtl.check.TabularExplanationTable.UNKNOWN;
import static org.eclipse.trace4cps.analysis.mtl.check.TabularExplanationTable.YES;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.trace4cps.analysis.mtl.AtomicProposition;
import org.eclipse.trace4cps.analysis.mtl.DefaultAtomicProposition;
import org.eclipse.trace4cps.analysis.mtl.ExplanationTable.Region;
import org.eclipse.trace4cps.analysis.mtl.MtlBuilder;
import org.eclipse.trace4cps.analysis.mtl.MtlException;
import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.mtl.MtlUtil;
import org.eclipse.trace4cps.analysis.mtl.State;
import org.eclipse.trace4cps.analysis.mtl.check.CompactExplanationTableImpl;
import org.eclipse.trace4cps.analysis.mtl.check.TabularExplanationTable;
import org.eclipse.trace4cps.core.impl.Interval;
import org.junit.jupiter.api.Test;

public class CompactExplanationTableImplTest {
    @Test
    public void testSetGet() throws MtlException {
        List<State> trace = new ArrayList<State>();
        trace.add(new DefaultState(1, "a"));
        trace.add(new DefaultState(2, "b"));
        trace.add(new DefaultState(3, "c"));
        trace.add(new DefaultState(4, "d"));

        AtomicProposition a = new DefaultAtomicProposition("a", "a");
        AtomicProposition b = new DefaultAtomicProposition("b", "b");
        MtlFormula phi = G(IMPLY(a, OR(F(b, new Interval(0, false, 3, false)), F(G(MtlBuilder.TRUE())))));

        Map<MtlFormula, Integer> idMap = getIdMap(phi);
        assertEquals(12, idMap.size() - 1);

        CompactExplanationTableImpl t = new CompactExplanationTableImpl(trace, idMap);

        int trueIndex = idMap.get(MtlBuilder.TRUE());
        // Verify that the table initially is empty, except for the true formula:
        for (int state = 0; state < 4; state++) {
            for (int i = 0; i < 13; i++) {
                if (trueIndex != i) {
                    assertEquals(UNKNOWN, t.get(i, state), "failed for state=" + state + ", i=" + i);
                } else {
                    assertEquals(YES, t.get(i, state), "failed for state=" + state + ", i=" + i);
                }
            }
        }

        for (int state = 0; state < 4; state++) {
            for (int i = 0; i < 13; i++) {
                t.put(i, state, MAYBE);
                if (i != trueIndex) {
                    assertEquals(MAYBE, t.get(i, state), "failed for state=" + state + ", i=" + i);
                }
            }
        }
    }

    @Test
    public void testGetRegions() throws MtlException {
        List<State> trace = new ArrayList<State>();
        for (int i = 0; i < 10; i++) {
            trace.add(new DefaultState(i + 5, "ap" + i));
        }
        AtomicProposition a = new DefaultAtomicProposition("a", "a");
        AtomicProposition b = new DefaultAtomicProposition("b", "b");
        MtlFormula phi = G(IMPLY(a, OR(F(b, new Interval(0, false, 3, false)), F(G(TRUE())))));

        final int formulaIndex = 0;
        Map<MtlFormula, Integer> idMap = Collections.singletonMap(phi, formulaIndex);
        CompactExplanationTableImpl t = new CompactExplanationTableImpl(trace, idMap);
        // Set some values:
        for (int i = 0; i < 4; i++) {
            t.put(formulaIndex, i, TabularExplanationTable.NO);
        }
        for (int i = 4; i < 7; i++) {
            t.put(formulaIndex, i, TabularExplanationTable.MAYBE);
        }
        for (int i = 7; i < 10; i++) {
            t.put(formulaIndex, i, TabularExplanationTable.YES);
        }
        List<Region> regions = t.getRegions(phi);
        assertEquals(3, regions.size());
        Region r = regions.get(0);
        assertEquals(0, r.getStartIndex());
        assertEquals(5d, r.getStartTime(), 0.000);
        assertEquals(3, r.getEndIndex());
        assertEquals(8d, r.getEndTime(), 0.000);
        r = regions.get(1);
        assertEquals(4, r.getStartIndex());
        assertEquals(9d, r.getStartTime(), 0.000);
        assertEquals(6, r.getEndIndex());
        assertEquals(11d, r.getEndTime(), 0.000);
        r = regions.get(2);
        assertEquals(7, r.getStartIndex());
        assertEquals(12d, r.getStartTime(), 0.000);
        assertEquals(9, r.getEndIndex());
        assertEquals(14d, r.getEndTime(), 0.000);
    }

    private Map<MtlFormula, Integer> getIdMap(MtlFormula phi) {
        Map<MtlFormula, Integer> idMap = new HashMap<MtlFormula, Integer>();
        List<MtlFormula> sub = MtlUtil.getSubformulas(phi);
        idMap.clear();
        int i = 0;
        for (MtlFormula f: sub) {
            idMap.put(f, i);
            i++;
        }
        return idMap;
    }
}
