/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.constraintgraph.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.trace4cps.analysis.constraintgraph.ConstraintConfig;
import org.eclipse.trace4cps.analysis.constraintgraph.impl.ClaimNode;
import org.eclipse.trace4cps.analysis.constraintgraph.impl.Constraint;
import org.eclipse.trace4cps.analysis.constraintgraph.impl.ConstraintGraph;
import org.eclipse.trace4cps.analysis.constraintgraph.impl.ConstraintGraphNode;
import org.eclipse.trace4cps.analysis.cpa.PositiveCycleException;
import org.eclipse.trace4cps.analysis.cpa.test.CPATest;
import org.eclipse.trace4cps.analysis.mtl.DefaultAtomicProposition;
import org.eclipse.trace4cps.core.ClaimEventType;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.impl.Claim;
import org.eclipse.trace4cps.core.impl.Dependency;
import org.eclipse.trace4cps.core.impl.Resource;
import org.eclipse.trace4cps.core.impl.Trace;
import org.junit.jupiter.api.Test;

public class ConstraintGraphTest {
    @Test
    public void testSimple() {
        ConstraintGraph cg = new ConstraintGraph(createDummyModel());
        assertEquals(10, cg.size());
        assertEquals(10, cg.edgeSize());
        assertClaimDurationConstraint(cg, "A", 5);
        assertClaimDurationConstraint(cg, "B", 5);
        assertClaimDurationConstraint(cg, "C", 3);
        assertClaimDurationConstraint(cg, "D", 5);
        assertClaimDurationConstraint(cg, "E", 5);
    }

    @Test
    public void testSimple_relaxedRelation() {
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setApplyOrderingHeuristic(Double.POSITIVE_INFINITY);
        ConstraintGraph cg = new ConstraintGraph(createDummyModel(), cfg);
        assertEquals(10, cg.size());
        assertEquals(16, cg.edgeSize());
        assertESConstraint(cg, "A", "C", 0);
        assertESConstraint(cg, "A", "D", 0);
        assertESConstraint(cg, "B", "C", 0);
        assertESConstraint(cg, "B", "D", 0);
        assertESConstraint(cg, "C", "E", 0);
        assertESConstraint(cg, "D", "E", 0);
    }

    @Test
    public void testSimple_strictRelation() {
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setApplyOrderingHeuristic(0);
        ConstraintGraph cg = new ConstraintGraph(createDummyModel(), cfg);
        assertEquals(10, cg.size());
        assertEquals(15, cg.edgeSize());
        assertESConstraint(cg, "A", "C", 0);
        assertESConstraint(cg, "A", "D", 0);
        assertESConstraint(cg, "B", "C", 0);
        assertESConstraint(cg, "B", "D", 0);
        assertESConstraint(cg, "D", "E", 0);
    }

    @Test
    public void testSimple_epsilonRelation() {
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setApplyOrderingHeuristic(3);
        ConstraintGraph cg = new ConstraintGraph(createDummyModel(), cfg);
        assertEquals(10, cg.size());
        assertEquals(16, cg.edgeSize());
        assertESConstraint(cg, "A", "C", 0);
        assertESConstraint(cg, "A", "D", 0);
        assertESConstraint(cg, "B", "C", 0);
        assertESConstraint(cg, "B", "D", 0);
        assertESConstraint(cg, "C", "E", 0);
        assertESConstraint(cg, "D", "E", 0);
    }

    @Test
    public void testSimple2() {
        ConstraintGraph cg = new ConstraintGraph(createDummyModel2());
        assertEquals(10, cg.size());
        assertEquals(10, cg.edgeSize());
        assertClaimDurationConstraint(cg, "A", 5);
        assertClaimDurationConstraint(cg, "B", 5);
        assertClaimDurationConstraint(cg, "C", 3);
        assertClaimDurationConstraint(cg, "D", 5);
        assertClaimDurationConstraint(cg, "E", 5);
    }

    @Test
    public void testSimple2_relaxedRelation() {
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setApplyOrderingHeuristic(Double.POSITIVE_INFINITY);
        ConstraintGraph cg = new ConstraintGraph(createDummyModel2(), cfg);
        assertEquals(10, cg.size());
        assertEquals(16, cg.edgeSize());
        assertESConstraint(cg, "A", "C", 0);
        assertESConstraint(cg, "A", "D", 0);
        assertESConstraint(cg, "B", "C", 0);
        assertESConstraint(cg, "B", "D", 0);
        assertESConstraint(cg, "C", "E", 0);
        assertESConstraint(cg, "D", "E", 0);
    }

    @Test
    public void testSimple2_strictRelation() {
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setApplyOrderingHeuristic(0);
        ConstraintGraph cg = new ConstraintGraph(createDummyModel2(), cfg);
        assertEquals(10, cg.size());
        assertEquals(10, cg.edgeSize());
    }

    @Test
    public void testSimple2_epsilonRelation() {
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setApplyOrderingHeuristic(1);
        ConstraintGraph cg = new ConstraintGraph(createDummyModel2(), cfg);
        assertEquals(10, cg.size());
        assertEquals(13, cg.edgeSize());
        assertESConstraint(cg, "A", "C", 0);
        assertESConstraint(cg, "A", "D", 0);
        assertESConstraint(cg, "D", "E", 0);
    }

    @Test
    public void testSimpleGrouping() {
        Map<String, String> filter = new HashMap<String, String>();
        filter.put("type", "transport");
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setApplyOrderingHeuristic(0);
        cfg.setApplyNonElasticityHeuristic(filter, "id");
        ConstraintGraph cg = new ConstraintGraph(getSimpleGroupTrace(), cfg);
        assertESConstraint(cg, "B", "C", 0);
        assertSEConstraint(cg, "C", "B", 0);
        assertESConstraint(cg, "C", "D", 0);
        assertSEConstraint(cg, "D", "C", 0);
        assertESConstraint(cg, "A", "D", 0);
        assertESConstraint(cg, "B", "E", 0);
    }

    @Test
    public void testSimpleCPAtest() {
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setApplyOrderingHeuristic(Double.POSITIVE_INFINITY);
        ConstraintGraph cg = new ConstraintGraph(CPATest.createDummyModel(), cfg);
        assertEquals(10, cg.size());
        assertEquals(16, cg.edgeSize());
    }

    @Test
    public void testSimpleCPAtest2() {
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setApplyOrderingHeuristic(Double.POSITIVE_INFINITY);
        ConstraintGraph cg = new ConstraintGraph(CPATest.createDummyModel4(), cfg);
        assertEquals(6, cg.size());
        assertEquals(6 + 1, cg.edgeSize());
    }

    @Test
    public void testSimpleClaimEventStateMatching() {
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setAddClaimDurations(false);
        ConstraintGraph cg = new ConstraintGraph(createDummyModel(), cfg);
        assertEquals(10, cg.size());
        DefaultAtomicProposition matchStart = new DefaultAtomicProposition(ClaimEventType.START);
        DefaultAtomicProposition matchEnd = new DefaultAtomicProposition(ClaimEventType.END);
        for (ConstraintGraphNode e: cg.getNodes()) {
            if (e instanceof ClaimNode) {
                if (((ClaimNode)e).isClaimStart()) {
                    assertTrue(((ClaimNode)e).getEvent().satisfies(matchStart));
                } else {
                    assertTrue(((ClaimNode)e).getEvent().satisfies(matchEnd));
                }
            }
        }
    }

    @Test
    public void testClaimZeroDuration() throws PositiveCycleException {
        ITrace trace = createModelZeroClaim();
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setAddSourceAndSink(true);
        cfg.setUseDependencies(true);
        ConstraintGraph cg = new ConstraintGraph(trace, cfg);

        assertEquals(10, cg.size());
        assertEquals(27, cg.edgeSize());

        assertClaimDurationConstraint(cg, "A", 5);
        assertClaimDurationConstraint(cg, "B", 0);
        assertClaimDurationConstraint(cg, "C", 0);
        assertClaimDurationConstraint(cg, "D", 5);

        assertESConstraint(cg, "A", "B", 0);
        assertESConstraint(cg, "B", "C", 0);
        assertESConstraint(cg, "C", "D", 0);
    }

    private void assertClaimDurationConstraint(ConstraintGraph cg, String name, double weigth) {
        ConstraintGraphNode src = find(cg, name, true);
        ConstraintGraphNode dst = find(cg, name, false);
        assertTrue(src != null && dst != null);
        assertConstraint(src, weigth, dst);
        assertConstraint(dst, -weigth, src);
    }

    private void assertESConstraint(ConstraintGraph cg, String nameSrc, String nameDst, double weigth) {
        ConstraintGraphNode src = find(cg, nameSrc, false);
        ConstraintGraphNode dst = find(cg, nameDst, true);
        assertTrue(src != null && dst != null);
        assertConstraint(src, weigth, dst);
    }

    private void assertSEConstraint(ConstraintGraph cg, String nameSrc, String nameDst, double weigth) {
        ConstraintGraphNode src = find(cg, nameSrc, true);
        ConstraintGraphNode dst = find(cg, nameDst, false);
        assertTrue(src != null && dst != null);
        assertConstraint(src, weigth, dst);
    }

    private void assertConstraint(ConstraintGraphNode src, double weigth, ConstraintGraphNode dst) {
        boolean found = false;
        for (Constraint c: src.constraints()) {
            if (c.getDst() == dst && c.weight() == weigth) {
                found = true;
                break;
            }
        }
        assertTrue(found);
    }

    private ConstraintGraphNode find(ConstraintGraph cg, String name, boolean isStart) {
        for (ConstraintGraphNode ce: cg.getNodes()) {
            if (ce instanceof ClaimNode) {
                IClaim c = ((ClaimNode)ce).getClaim();
                if (((ClaimNode)ce).isClaimStart() == isStart && name.equals(c.getAttributeValue("name"))) {
                    return ce;
                }
            }
        }
        return null;
    }

    private ITrace createDummyModel() {
        Trace t = new Trace();
        IResource r = new Resource(10, false);
        t.add(new Claim(0, 5, r, 1, from("name", "A")));
        t.add(new Claim(0, 5, r, 1, from("name", "B")));
        t.add(new Claim(5, 8, r, 1, from("name", "C")));
        t.add(new Claim(5, 10, r, 1, from("name", "D")));
        t.add(new Claim(10, 15, r, 1, from("name", "E")));
        return t;
    }

    private ITrace createDummyModel2() {
        Trace t = new Trace();
        IResource r = new Resource(10, false);
        t.add(new Claim(1, 6, r, 1, from("name", "A")));
        t.add(new Claim(0, 5, r, 1, from("name", "B")));
        t.add(new Claim(7, 10, r, 1, from("name", "C")));
        t.add(new Claim(7, 12, r, 1, from("name", "D")));
        t.add(new Claim(13, 18, r, 1, from("name", "E")));
        return t;
    }

    public static ITrace createModelZeroClaim() {
        Trace trace = new Trace();
        IResource r = new Resource(10, false);
        IClaim c1 = new Claim(0, 5, r, 1, from("name", "A"));
        IClaim c2 = new Claim(10, 10, r, 1, from("name", "B"));
        IClaim c3 = new Claim(20, 20, r, 1, from("name", "C"));
        IClaim c4 = new Claim(30, 35, r, 1, from("name", "D"));
        trace.add(c1);
        trace.add(c2);
        trace.add(c3);
        trace.add(c4);
        trace.add(new Dependency(c1.getEndEvent(), c2.getStartEvent()));
        trace.add(new Dependency(c2.getEndEvent(), c3.getStartEvent()));
        trace.add(new Dependency(c3.getEndEvent(), c4.getStartEvent()));
        return trace;
    }

    private static Map<String, String> from(String... ss) {
        if (ss == null || ss.length == 0 || ss.length % 2 != 0) {
            throw new IllegalArgumentException();
        }
        Map<String, String> r = new HashMap<String, String>();
        for (int i = 0; i < ss.length; i += 2) {
            r.put(ss[i], ss[i + 1]);
        }
        return r;
    }

    private ITrace getSimpleGroupTrace() {
        Trace trace = new Trace();
        IResource r = new Resource(10, false);
        IClaim ca = new Claim(0, 20, r, 1, from("name", "A", "id", "1"));
        IClaim cb = new Claim(2, 15, r, 1, from("name", "B", "id", "2", "type", "transport"));
        IClaim cc = new Claim(15, 20, r, 1, from("name", "C", "id", "2", "type", "transport"));
        IClaim cd = new Claim(20, 23, r, 1, from("name", "D", "id", "2", "type", "transport"));
        IClaim ce = new Claim(15, 30, r, 1, from("name", "E", "id", "1"));
        trace.add(ca);
        trace.add(cb);
        trace.add(cc);
        trace.add(cd);
        trace.add(ce);
        return trace;
    }
}
