/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.stl.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.eclipse.trace4cps.analysis.signal.impl.PsopHelper;
import org.eclipse.trace4cps.analysis.signal.impl.PsopHelper.MinMaxResult;
import org.eclipse.trace4cps.core.IPsopFragment;
import org.eclipse.trace4cps.core.Shape;
import org.eclipse.trace4cps.core.impl.Interval;
import org.eclipse.trace4cps.core.impl.PsopFragment;
import org.junit.jupiter.api.Test;

public class PsopFragmentTest {
    @Test
    public void testCtorInvalid1() {
        assertThrows(IllegalArgumentException.class, () -> { new PsopFragment(0d, 1, -2, null); });
    }

    @Test
    public void testCtorInvalid2() {
        assertThrows(IllegalArgumentException.class, () -> { new PsopFragment(0d, 1, -2, new Interval(10, 5)); });
    }

    @Test
    public void testShape() {
        IPsopFragment f = new PsopFragment(0d, 1, -2, new Interval(0, 10));
        assertEquals(Shape.PARABOLA_CAP, f.getShape());
        f = new PsopFragment(0d, -1, 2, new Interval(0, 10));
        assertEquals(Shape.PARABOLA_CUP, f.getShape());
        f = new PsopFragment(0d, 1, 2, new Interval(0, 10));
        assertEquals(Shape.INCREASING, f.getShape());
        f = new PsopFragment(0d, -1, -1, new Interval(0, 10));
        assertEquals(Shape.DECREASING, f.getShape());
        f = new PsopFragment(10d, 0d, 0d, new Interval(0, 10));
        assertEquals(Shape.CONSTANT, f.getShape());
    }

    @Test
    public void testCopy() {
        PsopFragment f = new PsopFragment(0d, 1, -2, new Interval(0, 10));
        PsopFragment f2 = PsopHelper.copy(f);
        assertTrue(f.getA().equals(f2.getA()));
        assertTrue(f.getB().equals(f2.getB()));
        assertTrue(f.getC().equals(f2.getC()));
        assertTrue(f.getOrder() == f2.getOrder());
        assertTrue(f.getShape().equals(f2.getShape()));
    }

    @Test
    public void testValueXAtInvalid() {
        assertThrows(IllegalArgumentException.class, () -> {
            IPsopFragment f = new PsopFragment(0d, 1, -2, new Interval(0, 10));
            PsopHelper.valueAt(f, 12);
        });
    }

    @Test
    public void testValueAt() {
        IPsopFragment f = new PsopFragment(3d, 2, 0.5 * 1.5, new Interval(5, 10));
        assertEquals(3d, PsopHelper.valueAt(f, 5).doubleValue(), 0.000001);
        assertEquals(3d + 5 * 2 + 0.5 * 1.5 * 25, PsopHelper.valueAt(f, 10).doubleValue(), 0.000001);
    }

    @Test
    public void testTimeZeroSlope() {
        // t=2 => v=0
        // t=4 => v=-2, x = 2*4 + 0.5 * -1 * 16 = 0
        IPsopFragment f = new PsopFragment(0d, 2, 0.5 * -1, new Interval(0, 4));
        assertEquals(2d, PsopHelper.argZeroSlope(f).doubleValue(), 0.000001);
    }

    @Test
    public void testTimeZeroSlopeIllegal() {
        assertThrows(IllegalArgumentException.class,
                () -> { PsopHelper.argZeroSlope(new PsopFragment(0d, 2, 0d, new Interval(0, 4))); });
    }

    @Test
    public void testMinMax() {
        // t=2 => v=0, x = 0 + 2*2 + 0.5 *-1 * 2*2 = 4 + -2 = 2
        // t=4 => v=-2, x = 2*4 + 0.5 * -1 * 16 = 0
        IPsopFragment f = new PsopFragment(0d, 2, -1 * 0.5, new Interval(0, 4));
        MinMaxResult r = PsopHelper.computeMinMax(f);
        assertEquals(0d, r.getMin(), 0.000001);
        assertEquals(2d, r.getMax(), 0.000001);
    }

    @Test
    public void testMinMax2() {
        // t=2 => v=0, x = 0 + 2*2 + 0.5 *-1 * 2*2 = 4 + -2 = 2
        // t=4 => v=-2, x = 2*4 + 0.5 * -1 * 16 = 0
        // t=6 => x = 0 + 2 * 6 + 0.5 * -1 * 36 = 12 - 18 = -6
        IPsopFragment f = new PsopFragment(0d, 2, -1 * 0.5, new Interval(0, 6));
        MinMaxResult r = PsopHelper.computeMinMax(f);
        assertEquals(-6d, r.getMin(), 0.000001);
        assertEquals(2d, r.getMax(), 0.000001);
    }

    @Test
    public void testIntersectionsInvalid() {
        assertThrows(IllegalArgumentException.class, () -> {
            PsopFragment f1 = new PsopFragment(2d, 0, 0.2, new Interval(1, 4));
            PsopFragment f2 = new PsopFragment(2d, 0, 0, new Interval(1, 3));
            PsopHelper.computeIntersections(f1, f2);
        });
    }

    @Test
    public void testIntersections1() {
        // t=0 => x = 0
        // t=2 => x = 2
        // t=4 => x = 0
        // t=6 => x = -6
        PsopFragment f = new PsopFragment(0d, 2, -1 * 0.5, new Interval(0, 6));
        PsopFragment f1 = new PsopFragment(0d, 0, 0d, new Interval(0, 6));
        PsopFragment f2 = new PsopFragment(-3d, 1, 0, new Interval(0, 6));
        List<Double> is = PsopHelper.computeIntersections(f, f1);
        assertEquals(2, is.size());
        assertEquals(0d, is.get(0), 0.00001);
        assertEquals(4d, is.get(1), 0.00001);
        is = PsopHelper.computeIntersections(f, f2);
        assertEquals(1, is.size());
        // f = f2 <=> -0.5x^2 + 2x = x - 3
        // <=> -0.5x^2 + x + 3 = 0
        // a*x^2 + bx + c = 0
        double a = -0.5;
        double b = 1;
        double c = 3;
        double x1 = (-b - Math.sqrt(b * b - 4 * a * c)) / (2 * a);
        double x2 = (-b + Math.sqrt(b * b - 4 * a * c)) / (2 * a);
        if (f.dom().contains(x1)) {
            is.contains(x1);
        }
        if (f.dom().contains(x2)) {
            is.contains(x2);
        }
    }
}
