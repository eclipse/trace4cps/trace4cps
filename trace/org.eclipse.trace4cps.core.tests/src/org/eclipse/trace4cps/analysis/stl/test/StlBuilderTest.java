/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.stl.test;

import static org.eclipse.trace4cps.analysis.stl.StlBuilder.AND;
import static org.eclipse.trace4cps.analysis.stl.StlBuilder.F;
import static org.eclipse.trace4cps.analysis.stl.StlBuilder.G;
import static org.eclipse.trace4cps.analysis.stl.StlBuilder.GEQ;
import static org.eclipse.trace4cps.analysis.stl.StlBuilder.IMPLY;
import static org.eclipse.trace4cps.analysis.stl.StlBuilder.LEQ;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.eclipse.trace4cps.analysis.signal.SignalModifier;
import org.eclipse.trace4cps.analysis.signal.SignalUtil;
import org.eclipse.trace4cps.analysis.stl.StlFormula;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.impl.TraceHelper;
import org.eclipse.trace4cps.core.io.ParseException;
import org.eclipse.trace4cps.core.io.TraceReader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class StlBuilderTest {
    private ITrace traceOdse;

    @BeforeEach
    public void setup() throws IOException, ParseException {
        traceOdse = TraceReader.readTrace(new File("../../trace-examples/odse/reference.etf"));
    }

    @Test
    public void testResourceAmountODSE() {
        IResource m2 = TraceHelper.filter(traceOdse.getResources(), "name", "M2").get(0);
        IPsop p = SignalUtil.getResourceAmount(traceOdse, m2, new SignalModifier(1, 10, TimeUnit.MILLISECONDS));
        // We know the trace time domain is approx [0, 1600] ms
        StlFormula phi = G(AND(LEQ(p, 90), GEQ(p, 10)), 100, 1500);
        assertTrue(phi.getRho() > 0d);
        // The signal drops below 20 somewhere
        StlFormula phi2 = F(LEQ(p, 20), 100, 1500);
        assertTrue(phi2.getRho() > 0d);
        // If the signal drops below 20, then within 10 ms it is again larger than 20
        StlFormula phi3 = G(IMPLY(LEQ(p, 20), F(GEQ(p, 20), 0, 10)), 100, 1500);
        assertTrue(phi3.getRho() > 0d);
    }

    @Test
    public void testResourceClientODSE() {
        IResource m2 = TraceHelper.filter(traceOdse.getResources(), "name", "M2").get(0);
        IPsop p = SignalUtil.getResourceClients(traceOdse, m2, new SignalModifier(1, 10, TimeUnit.MILLISECONDS));
        // We know the trace time domain is approx [0, 1600] ms
        StlFormula phi = G(AND(LEQ(p, 6), GEQ(p, 1)), 100, 1500);
        assertTrue(phi.getRho() > 0d);
    }

    @Test
    public void testWipODSE() {
        IPsop p = SignalUtil.getWip(traceOdse, "id", new SignalModifier(1, 100, TimeUnit.MILLISECONDS));
        StlFormula phi = G(AND(LEQ(p, 5), GEQ(p, 2)), 100, 1500);
        assertTrue(phi.getRho() > 0d);
    }

    @Test
    public void testLatencyODSE() {
        IPsop p = SignalUtil.getLatency(traceOdse, "id", TimeUnit.MILLISECONDS, 100, TimeUnit.MILLISECONDS);
        StlFormula phi = G(AND(LEQ(p, 35), GEQ(p, 25)), 100, 1500);
        assertTrue(phi.getRho() > 0d);
    }

    @Test
    public void testPaperPathTP() {
        IPsop p = SignalUtil.getTP(traceOdse, "id", TimeUnit.SECONDS, 10, TimeUnit.MILLISECONDS);
        // The tp is evetually >= 10 images/s for at least 100 mseconds
        StlFormula phi = F(G(GEQ(p, 10), 0, 0.100));
        assertTrue(phi.getRho() > 0d);
    }
}
