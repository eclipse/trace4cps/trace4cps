/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.stl.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.eclipse.trace4cps.analysis.signal.impl.PsopHelper;
import org.eclipse.trace4cps.analysis.signal.impl.PsopHelper.ShapeSegment;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IInterval;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.IPsopFragment;
import org.eclipse.trace4cps.core.Shape;
import org.eclipse.trace4cps.core.impl.Interval;
import org.eclipse.trace4cps.core.impl.Psop;
import org.eclipse.trace4cps.core.impl.PsopFragment;
import org.junit.jupiter.api.Test;

public class PsopTest {
    @Test
    public void testDomInvalid() {
        assertThrows(IllegalStateException.class, () -> { PsopHelper.dom(new Psop()); });
    }

    @Test
    public void testDom() {
        Psop f = getTestFunction1();
        IInterval dom = PsopHelper.dom(f);
        assertEquals(dom.lb().doubleValue(), 0, 1e-9);
        assertEquals(dom.ub().doubleValue(), 11.5, 1e-9);
    }

    @Test
    public void testSize() {
        Psop f = getTestFunction1();
        assertEquals(7, PsopHelper.size(f));
        assertEquals(0, PsopHelper.size(new Psop()));
    }

    @Test
    public void testValueAtInvalid() {
        assertThrows(IllegalArgumentException.class, () -> { PsopHelper.valueAt(getTestFunction1(), 100); });
    }

    @Test
    public void testAddInvalid() {
        assertThrows(IllegalArgumentException.class, () -> {
            Psop f = getTestFunction1();
            IInterval dom = PsopHelper.dom(f);
            Interval d = new Interval(dom.lb().doubleValue() - 10, dom.ub().doubleValue() - 5);
            f.addAtBegin(new PsopFragment(0, 1, 2, d));
        });
    }

    @Test
    public void testValueAt1() {
        assertEquals(0.2 + 4 * 0.4, PsopHelper.valueAt(getTestFunction1(), 11.5).doubleValue(), 1e-9);
    }

    @Test
    public void testValueAt2() {
        assertEquals(0.2, PsopHelper.valueAt(getTestFunction1(), 7.5).doubleValue(), 1e-9);
    }

    @Test
    public void testProjectTo1() {
        Psop f = getTestFunction0();
        PsopHelper.projectTo(f, new Interval(0, 3));
        assertTimeDomains(f, 0.0, 1.0, 3.0);
    }

    @Test
    public void testProjectTo2() {
        Psop f = getTestFunction0();
        PsopHelper.projectTo(f, new Interval(0.5, 1.5));
        assertTimeDomains(f, 0.5, 1.0, 1.5);
    }

    @Test
    public void testProjectTo3() {
        Psop f = getTestFunction0();
        PsopHelper.projectTo(f, new Interval(0.5, 1.0));
        assertTimeDomains(f, 0.5, 1.0);
    }

    @Test
    public void testProjectTo4() {
        Psop f = getTestFunction0();
        PsopHelper.projectTo(f, new Interval(0.5, 0.8));
        assertTimeDomains(f, 0.5, 0.8);
    }

    @Test
    public void testProjectTo5() {
        Psop f = getTestFunction0();
        PsopHelper.projectTo(f, new Interval(-0.5, 1.5));
        assertTimeDomains(f, 0, 1, 1.5);
    }

    @Test
    public void testProjectTo6() {
        Psop f = getTestFunction0();
        PsopHelper.projectTo(f, new Interval(4, 6));
        assertEquals(0, PsopHelper.size(f));
    }

    @Test
    public void testProjectTo7() {
        Psop f = getTestFunction0();
        PsopHelper.projectTo(f, new Interval(-5, -2));
        assertEquals(0, PsopHelper.size(f));
    }

    @Test
    public void testAlignWith1() {
        Psop f = getTestFunction0();
        PsopBuilder g = new PsopBuilder("g", 2.0, 0d, -0.5d).addFragment(0.1, 1).addFragment(0.2, 1);
        PsopHelper.alignWith(f, g.get());
        assertTimeDomains(f, 0.0, 0.5, 1.0, 1.5);
    }

    @Test
    public void testAlignWith2() {
        Psop f = getTestFunction0();
        PsopBuilder g = new PsopBuilder("g", 2.0, 0d, -0.5d).addFragment(0.1, 10);
        PsopHelper.alignWith(f, g.get());
        assertTimeDomains(f, 0.0, 1.0, 3.0);
    }

    @Test
    public void testAlignWith3() {
        Psop f = getTestFunction0();
        PsopBuilder g = new PsopBuilder("g", 2.0, 0d, 0.5).addFragment(0.1, 0.25);
        PsopHelper.alignWith(f, g.get());
        assertTimeDomains(f, 0.5, 0.75);
    }

    @Test
    public void testMin() {
        Psop p1 = new Psop();
        p1.add(new PsopFragment(0d, 1d, 0d, new Interval(0d, 2d)));
        Psop p2 = new Psop();
        p2.add(new PsopFragment(2d, -1d, 0d, new Interval(-1d, 1d)));

        IPsop p = PsopHelper.min(p1, p2);
        assertEquals(2, p.getFragments().size());
        assertFragment(p.getFragments().get(0), 0d, 0.5, 0d, 1d, 0d);
        assertFragment(p.getFragments().get(1), 0.5, 1.0, 0.5, -1d, 0d);
    }

    @Test
    public void testMax() {
        Psop p1 = new Psop();
        p1.add(new PsopFragment(0d, 1d, 0d, new Interval(0d, 2d)));
        Psop p2 = new Psop();
        p2.add(new PsopFragment(2d, -1d, 0d, new Interval(-1d, 1d)));

        IPsop p = PsopHelper.max(p1, p2);
        assertEquals(2, p.getFragments().size());
        assertFragment(p.getFragments().get(0), 0d, 0.5, 1d, -1d, 0d);
        assertFragment(p.getFragments().get(1), 0.5, 1.0, 0.5, 1d, 0d);
    }

    @Test
    public void testAdd() {
        Psop p1 = new Psop();
        p1.add(new PsopFragment(0d, 1d, 0d, new Interval(0d, 2d)));
        Psop p2 = new Psop();
        p2.add(new PsopFragment(2d, -1d, 0d, new Interval(-1d, 1d)));

        IPsop p = PsopHelper.add(p1, p2);
        assertEquals(1, p.getFragments().size());
        assertFragment(p.getFragments().get(0), 0d, 1d, 1d, 0d, 0d);
    }

    @Test
    public void testSub() {
        Psop p1 = new Psop();
        p1.add(new PsopFragment(0d, 1d, 0d, new Interval(0d, 2d)));
        Psop p2 = new Psop();
        p2.add(new PsopFragment(2d, -1d, 0d, new Interval(-1d, 1d)));

        IPsop pa = PsopHelper.sub(p1, p2);
        assertEquals(1, pa.getFragments().size());
        assertFragment(pa.getFragments().get(0), 0d, 1d, -1d, 2d, 0d);

        IPsop pb = PsopHelper.sub(p2, p1);
        assertEquals(1, pb.getFragments().size());
        assertFragment(pb.getFragments().get(0), 0d, 1d, 1d, -2d, 0d);
    }

    @Test
    public void testCreateMonotonicSegmentations() {
        Psop f = getTestFunction1();
        List<ShapeSegment> l = PsopHelper.createMonotonicSegmentation(f);
        assertEquals(3, l.size());
        assertShapeSegment(l.get(0), 0, 3, Shape.INCREASING);
        assertShapeSegment(l.get(1), 4, 6, Shape.DECREASING);
        assertShapeSegment(l.get(2), 7, 8, Shape.INCREASING);
    }

    @Test
    public void testCreateMonotonicSegmentations2() {
        Psop f = new Psop();
        f.add(new PsopFragment(3, 0, 0, new Interval(0d, 1d)));
        f.add(new PsopFragment(4, 1, -0.5, new Interval(1d, 3d)));

        List<ShapeSegment> l = PsopHelper.createMonotonicSegmentation(f);
        assertEquals(3, l.size());
        assertShapeSegment(l.get(0), 0, 0, Shape.CONSTANT);
        assertShapeSegment(l.get(1), 1, 1, Shape.INCREASING);
        assertShapeSegment(l.get(2), 2, 2, Shape.DECREASING);
    }

    @Test
    public void testCreateClaimRepresentationSatisfaction1() {
        Psop f = new Psop();
        f.add(new PsopFragment(3, 0, 0, new Interval(0d, 1d)));
        f.add(new PsopFragment(-2, 0, 0, new Interval(1d, 3d)));
        f.add(new PsopFragment(3, 0, 0, new Interval(3d, 4d)));
        List<IClaim> claims = PsopHelper.createClaimRepresentationOfSatisfaction(f, "test");
        assertEquals(3, claims.size());
    }

    @Test
    public void testCreateClaimRepresentationSatisfaction2() {
        Psop f = new Psop();
        f.add(new PsopFragment(-3, 0, 0, new Interval(0d, 1d)));
        f.add(new PsopFragment(-3, 2, 0, new Interval(1d, 3d)));
        f.add(new PsopFragment(1, 0, 0, new Interval(3d, 4d)));
        List<IClaim> claims = PsopHelper.createClaimRepresentationOfSatisfaction(f, "test");
        assertEquals(2, claims.size());
    }

    @Test
    public void testCreateClaimRepresentationSatisfaction3() {
        Psop f = new Psop();
        f.add(new PsopFragment(-1, 1, 0, new Interval(0d, 1d)));
        f.add(new PsopFragment(0, 0, 0, new Interval(1d, 3d)));
        f.add(new PsopFragment(0, 1, 0, new Interval(3d, 4d)));
        List<IClaim> claims = PsopHelper.createClaimRepresentationOfSatisfaction(f, "test");
        assertEquals(2, claims.size());
    }

    private void assertShapeSegment(ShapeSegment s, int lb, int ub, Shape shape) {
        assertEquals(lb, s.getBeginFragment());
        assertEquals(ub, s.getEndFragment());
        assertEquals(shape, s.getShape());
    }

    public static void assertTimeDomains(IPsop f, double... ts) {
        int i = 0;
        for (IPsopFragment frag: f.getFragments()) {
            assertEquals(ts[i], frag.dom().lb().doubleValue(), 1e-9);
            assertEquals(ts[i + 1], frag.dom().ub().doubleValue(), 1e-9);
            i++;
        }
    }

    public static void assertFragment(IPsopFragment f, double t0, double t1, double c, double b, double a) {
        assertEquals(f.getC().doubleValue(), c, 1e-9);
        assertEquals(f.getB().doubleValue(), b, 1e-9);
        assertEquals(f.getA().doubleValue(), a, 1e-9);
        assertEquals(f.dom().lb().doubleValue(), t0, 1e-9);
        assertEquals(f.dom().ub().doubleValue(), t1, 1e-9);
    }

    /**
     * @return dom = [0,1)[1,3)
     */
    private Psop getTestFunction0() {
        PsopBuilder b = new PsopBuilder("test_function_0", 2.0, 0d);
        b.addFragment(0, 1);
        b.addFragment(0.2, 2);
        Psop f = b.get();
        return f;
    }

    private Psop getTestFunction1() {
        PsopBuilder b = new PsopBuilder("test_function_1", 2.0, 0d);
        b.addFragment(0, 1);
        b.addFragment(0.2, 2);
        b.addFragment(0, 1);
        b.addFragment(-2, 1);
        b.addFragment(0, 0.5);
        b.addFragment(1, 2);
        b.addFragment(0, 4);
        return b.get();
    }
}
