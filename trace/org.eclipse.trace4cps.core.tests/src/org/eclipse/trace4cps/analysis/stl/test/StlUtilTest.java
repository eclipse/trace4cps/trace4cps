/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.stl.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.eclipse.trace4cps.analysis.signal.impl.PsopHelper;
import org.eclipse.trace4cps.analysis.stl.impl.STLUtil;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.impl.Interval;
import org.eclipse.trace4cps.core.impl.Psop;
import org.eclipse.trace4cps.core.impl.PsopFragment;
import org.junit.jupiter.api.Test;

public class StlUtilTest {
    /**
     * CAV 2013 paper, Donze, Ferrere, Maler.
     *
     * @throws Exception
     */
    @Test
    public void testEventuallySTLpaperMaler() throws Exception {
        Psop f = new Psop();
        f.add(new PsopFragment(0d, 0d, 0d, new Interval(0, 5)));
        f.add(new PsopFragment(0d, -1d, 0d, new Interval(5, 6)));
        f.add(new PsopFragment(-1d, 0d, 0d, new Interval(6, 7)));
        f.add(new PsopFragment(-1d, 1d, 0d, new Interval(7, 8)));
        IPsop g = STLUtil.signal_eventually(f, 0, 2);
        IPsop h = STLUtil.signal_eventually(f, 2, 4);
        IPsop i = STLUtil.signalAnd(g, h);
        IPsop j1 = STLUtil.signalNegate(i);
        IPsop j2 = STLUtil.signal_eventually(j1, 0d, 0.5d);
        IPsop j = STLUtil.signalNegate(j2);

        assertEquals(8, PsopHelper.size(j));
        PsopTest.assertFragment(j.getFragments().get(0), 0, 2.5, 0d, 0d, 0d);
        PsopTest.assertFragment(j.getFragments().get(1), 2.5, 3, 0d, -1d, 0d);
        PsopTest.assertFragment(j.getFragments().get(2), 3, 3.5, -0.5, 0d, 0d);
        PsopTest.assertFragment(j.getFragments().get(3), 3.5, 4, -0.5, 1d, 0d);
        PsopTest.assertFragment(j.getFragments().get(4), 4, 4.5, 0d, 0d, 0d);
        PsopTest.assertFragment(j.getFragments().get(5), 4.5, 5, 0d, -1d, 0d);
        PsopTest.assertFragment(j.getFragments().get(6), 5, 5.5, -0.5, 0d, 0d);
        PsopTest.assertFragment(j.getFragments().get(7), 5.5, 6, -0.5, 1d, 0d);
    }

    @Test
    public void testUntimedEventually() throws Exception {
        PsopBuilder b = new PsopBuilder("f", 2.0, 2.0);
        b.addFragment(-2, 2);
        b.addFragment(1.5, 2);
        b.addFragment(-5, 0.5);
        Psop f = b.get();
        IPsop g = STLUtil.signal_eventually(f, 0, Double.POSITIVE_INFINITY);
        assertEquals(3d, PsopHelper.getStartValue(g).doubleValue(), 1e-9);
    }

    @Test
    public void testUntimedEventuallyCAV13_stair_signal() throws Exception {
        Psop f = new Psop();
        f.add(new PsopFragment(1, 0, 0, new Interval(0, 1)));
        f.add(new PsopFragment(2, 0, 0, new Interval(1, 2)));
        f.add(new PsopFragment(3, 0, 0, new Interval(2, 3)));
        IPsop g = STLUtil.signal_eventually(f, 0, Double.POSITIVE_INFINITY);
        assertEquals(3d, PsopHelper.getStartValue(g).doubleValue(), 1e-9);
    }

    @Test
    public void testTimedEventuallyNonMonotone() throws Exception {
        Psop f = new Psop();
        f.add(new PsopFragment(10, 2, -0.5, new Interval(0, 10)));
        assertEquals(10d, PsopHelper.getStartValue(f).doubleValue(), 1e-9);
        assertEquals(-20d, PsopHelper.getMinValue(f).doubleValue(), 1e-9);
        assertEquals(12d, PsopHelper.getMaxValue(f).doubleValue(), 1e-9);

        IPsop g1 = STLUtil.signal_eventually(f, 0, 20);
        assertEquals(12d, PsopHelper.getMaxValue(g1).doubleValue(), 1e-9);
        IPsop g2 = STLUtil.signal_eventually(f, 5, Double.POSITIVE_INFINITY);
        assertEquals(7.5d, PsopHelper.getMaxValue(g2).doubleValue(), 1e-9);
        IPsop g3 = STLUtil.signal_eventually(f, 9, Double.POSITIVE_INFINITY);
        assertEquals(-12.5d, PsopHelper.getMaxValue(g3).doubleValue(), 1e-9);
    }
}
