/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.stl.test;

import org.eclipse.trace4cps.core.impl.Interval;
import org.eclipse.trace4cps.core.impl.Psop;
import org.eclipse.trace4cps.core.impl.PsopFragment;

public class PsopBuilder {
    private double x0;

    private double v0;

    private double t;

    private final Psop f;

    public PsopBuilder(String id, double x0, double v0) {
        this(id, x0, v0, 0d);
    }

    public PsopBuilder(String id, double x0, double v0, double t0) {
        f = new Psop();
        this.x0 = x0;
        this.v0 = v0;
        this.t = t0;
    }

    public PsopBuilder addFragment(double acc, double dt) {
        f.add(new PsopFragment(x0, v0, 0.5 * acc, new Interval(t, t + dt)));
        x0 = x0 + v0 * dt + 0.5 * acc * dt * dt;
        v0 = v0 + acc * dt;
        t = t + dt;
        return this;
    }

    public Psop get() {
        return f;
    }
}
