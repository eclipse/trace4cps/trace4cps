/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.signal.test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.eclipse.trace4cps.analysis.signal.SignalModifier;
import org.eclipse.trace4cps.analysis.signal.SignalUtil;
import org.eclipse.trace4cps.analysis.signal.impl.PsopHelper;
import org.eclipse.trace4cps.core.ClaimEventType;
import org.eclipse.trace4cps.core.IClaimEvent;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.IPsopFragment;
import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.impl.Claim;
import org.eclipse.trace4cps.core.impl.Interval;
import org.eclipse.trace4cps.core.impl.Resource;
import org.eclipse.trace4cps.core.impl.Trace;
import org.junit.jupiter.api.Test;

public class SignalUtilTest {
    @Test
    public void testInstThroughputSignal() throws Exception {
        ITrace trace = createTestTrace();
        IPsop inst_tp = SignalUtil.getTP(trace, "id", TimeUnit.SECONDS, 0, TimeUnit.SECONDS);
        assertMaxValue(inst_tp, 200);
        assertMinValue(inst_tp, 0);
    }

    @Test
    public void testThroughputSignal() throws Exception {
        ITrace trace = createTestTrace();
        IPsop tp_per_s = SignalUtil.getTP(trace, "id", TimeUnit.SECONDS, 3, TimeUnit.SECONDS);
        assertMiddleValue(tp_per_s, 50);
        assertMaxValue(tp_per_s, 50);
        assertMinValue(tp_per_s, 0);

        IPsop tp_per_min = SignalUtil.getTP(trace, "id", TimeUnit.MINUTES, 1, TimeUnit.SECONDS);
        assertMiddleValue(tp_per_min, 50 * 60);
        assertMaxValue(tp_per_min, 50 * 60);
        assertMinValue(tp_per_min, 0);
    }

    @Test
    public void testThroughputSignalEvents() throws Exception {
        List<IEvent> events = getStartEvents(); // every 20 ms an event: 50 events per second
        IPsop tp = SignalUtil.getTP(TimeUnit.SECONDS, new Interval(0, 20), events, TimeUnit.SECONDS, 3001,
                TimeUnit.MILLISECONDS);
        // At most 151 events in the window of 3001 ms
        assertMaxValue(tp, 151d / 3.001);
        assertMinValue(tp, 0);
    }

    @Test
    public void testInstWipSignal() throws Exception {
        ITrace trace = createTestTrace();
        IPsop wip = SignalUtil.getWip(trace, "id", SignalModifier.NONE);
        assertMaxValue(wip, 1);
        assertMinValue(wip, 0);
    }

    @Test
    public void testWipSignal() throws Exception {
        ITrace trace = createTestTrace();
        IPsop wip = SignalUtil.getWip(trace, "id", new SignalModifier(1d, 3, TimeUnit.SECONDS));
        // 5 ms busy, 15 ms idle -> wip = 5/(5+15) = 0.25
        assertMiddleValue(wip, 0.25);
        assertMaxValue(wip, 0.25);
        assertMinValue(wip, 0);
    }

    @Test
    public void testInstLatenctySignal() throws Exception {
        ITrace trace = createTestTrace();
        IPsop p = SignalUtil.getLatency(trace, "id", TimeUnit.MILLISECONDS, 0, TimeUnit.SECONDS);
        // check that every fragment is valid
        for (IPsopFragment f: p.getFragments()) {
            double a = f.getA().doubleValue();
            double b = f.getB().doubleValue();
            double c = f.getC().doubleValue();
            assertFalse(Double.isNaN(a));
            assertFalse(Double.isNaN(b));
            assertFalse(Double.isNaN(c));
        }
        // inst latency equals inst wip / inst tp = 1 / 200 s^-1 = 5 ms
        assertMaxValue(p, 5);
        assertMinValue(p, 0);
    }

    @Test
    public void testLatenctySignal() throws Exception {
        ITrace trace = createTestTrace();
        IPsop p = SignalUtil.getLatency(trace, "id", TimeUnit.MILLISECONDS, 3, TimeUnit.SECONDS);
        // check that every fragment is valid
        for (IPsopFragment f: p.getFragments()) {
            double a = f.getA().doubleValue();
            double b = f.getB().doubleValue();
            double c = f.getC().doubleValue();
            assertFalse(Double.isNaN(a));
            assertFalse(Double.isNaN(b));
            assertFalse(Double.isNaN(c));
        }
        // inst latency equals inst wip / inst tp = 1 / 200 s^-1 = 5 ms
        // but that happens only 1/4 of the time (A duration 5ms, period 20ms)
        // Thus: 5 ms * 1/4
        assertMiddleValue(p, 5d / 4d);
        assertMaxValue(p, 5d / 4d);
        assertMinValue(p, 0);
    }

    @Test
    public void testInstResourceAmountSignal() throws Exception {
        ITrace trace = createTestTrace();
        IResource r = trace.getResources().get(0);
        IPsop amount = SignalUtil.getResourceAmount(trace, r, SignalModifier.NONE);
        assertMaxValue(amount, 75);
        assertMinValue(amount, 0);
    }

    @Test
    public void testResourceAmountSignal() throws Exception {
        ITrace trace = createTestTrace();
        IResource r = trace.getResources().get(0);
        IPsop amount = SignalUtil.getResourceAmount(trace, r, new SignalModifier(1d, 3, TimeUnit.SECONDS));
        // 75 during 1/4 of the time:
        assertMiddleValue(amount, 75d / 4d);
        assertMaxValue(amount, 75d / 4d);
        assertMinValue(amount, 0);
    }

    @Test
    public void testInstResourceClientSignal() throws Exception {
        ITrace trace = createTestTrace();
        IResource r = trace.getResources().get(0);
        IPsop client = SignalUtil.getResourceClients(trace, r, SignalModifier.NONE);
        assertMaxValue(client, 1);
        assertMinValue(client, 0);
    }

    @Test
    public void testResourceClientSignal() throws Exception {
        ITrace trace = createTestTrace();
        IResource r = trace.getResources().get(0);
        IPsop amount = SignalUtil.getResourceClients(trace, r, new SignalModifier(1d, 3, TimeUnit.SECONDS));
        // 1 during 1/4 of the time:
        assertMiddleValue(amount, 1d / 4d);
        assertMaxValue(amount, 1d / 4d);
        assertMinValue(amount, 0);
    }

    /**
     * @return a test trace with a single repetitive task A with a duration of 5 ms and a period of 20 ms
     */
    private ITrace createTestTrace() {
        Trace trace = new Trace();
        Resource res = new Resource(100, false);
        res.setAttribute("name", "R");
        double period = 0.020; // 20 ms
        for (int i = 0; i < 1000; i++) {
            Claim claim = new Claim(i * period, i * period + 0.005, res, 75);
            claim.setAttribute("id", Integer.toString(i));
            claim.setAttribute("name", "A");
            trace.add(claim);
        }
        return trace;
    }

    private List<IEvent> getStartEvents() {
        List<IEvent> events = new ArrayList<>();
        for (IEvent e: createTestTrace().getEvents()) {
            if (((IClaimEvent)e).getType() == ClaimEventType.START) {
                events.add(e);
            }
        }
        return events;
    }

    private void assertMiddleValue(IPsop p, double expected) {
        double v = p.getFragments().get(p.getFragments().size() / 2).getC().doubleValue();
        assertTrue(expected - 1e-9 < v && v < expected + 1e-9);
    }

    private void assertMaxValue(IPsop p, double expected) {
        double v = PsopHelper.getMaxValue(p).doubleValue();
        assertTrue(expected - 1e-9 < v && v < expected + 1e-9);
    }

    private void assertMinValue(IPsop p, double expected) {
        double v = PsopHelper.getMinValue(p).doubleValue();
        assertTrue(expected - 1e-9 < v && v < expected + 1e-9);
    }
}
