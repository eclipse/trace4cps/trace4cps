/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.signal.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.trace4cps.analysis.signal.impl.EventSignalUtil;
import org.eclipse.trace4cps.analysis.signal.impl.LinearSignalFragment;
import org.eclipse.trace4cps.analysis.signal.impl.PsopHelper;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.impl.Interval;
import org.junit.jupiter.api.Test;

public class EventSignalUtilTest {
    @Test
    public void testInstantaneous() {
        List<LinearSignalFragment> es = new ArrayList<LinearSignalFragment>();
        es.add(new LinearSignalFragment(3.7, 3.9, 5));
        es.add(new LinearSignalFragment(6.4, 6.6, 10));
        IPsop p = EventSignalUtil.toIPsop(EventSignalUtil.computeInstantaneous(es, 1d, new Interval(0, 10)));
        assertEquals(10d, PsopHelper.getMaxValue(p).doubleValue(), 1e-9);
        assertEquals(0d, PsopHelper.valueAt(p, 0d).doubleValue(), 1e-9);
        assertEquals(5d, PsopHelper.valueAt(p, 3.8).doubleValue(), 1e-9);
        assertEquals(10d, PsopHelper.valueAt(p, 6.45).doubleValue(), 1e-9);
        assertEquals(0d, PsopHelper.valueAt(p, 9).doubleValue(), 1e-9);
    }

    @Test
    public void testConvolution_simple1() {
        List<LinearSignalFragment> es = new ArrayList<LinearSignalFragment>();
        es.add(new LinearSignalFragment(3.7, 3.9, 5));
        IPsop p = EventSignalUtil.convoluteScaleAndProject(es, 1, 1, new Interval(0, 10));
        assertEquals(1d, PsopHelper.getMaxValue(p).doubleValue(), 1e-9);
        assertEquals(0d, PsopHelper.valueAt(p, 3d).doubleValue(), 1e-9);
        assertEquals(0d, PsopHelper.valueAt(p, 3.7d).doubleValue(), 1e-9);
        assertEquals(0.5d, PsopHelper.valueAt(p, 3.8d).doubleValue(), 1e-9);
        assertEquals(1d, PsopHelper.valueAt(p, 3.9d).doubleValue(), 1e-9);
        assertEquals(1d, PsopHelper.valueAt(p, 4.7d).doubleValue(), 1e-9);
        assertEquals(0.5d, PsopHelper.valueAt(p, 4.8d).doubleValue(), 1e-9);
        assertEquals(0d, PsopHelper.valueAt(p, 4.9d).doubleValue(), 1e-9);
        assertEquals(0d, PsopHelper.valueAt(p, 10d).doubleValue(), 1e-9);
//            System.out.println(p);
    }

    @Test
    public void testConvolution_simple2() {
        List<LinearSignalFragment> es = new ArrayList<LinearSignalFragment>();
        es.add(new LinearSignalFragment(3.7, 3.9, 5));
        IPsop p = EventSignalUtil.convoluteScaleAndProject(es, 1, 2, new Interval(0, 10));
//      System.out.println(p);
        assertEquals(0.5d, PsopHelper.getMaxValue(p).doubleValue(), 1e-9);
        assertEquals(0d, PsopHelper.valueAt(p, 3.7d).doubleValue(), 1e-9);
        assertEquals(0.25d, PsopHelper.valueAt(p, 3.8d).doubleValue(), 1e-9);
        assertEquals(0.5d, PsopHelper.valueAt(p, 3.9d).doubleValue(), 1e-9);
        assertEquals(0.5d, PsopHelper.valueAt(p, 5.7d).doubleValue(), 1e-9);
        assertEquals(0.25d, PsopHelper.valueAt(p, 5.8d).doubleValue(), 1e-9);
        assertEquals(0d, PsopHelper.valueAt(p, 5.9d).doubleValue(), 1e-9);
        assertEquals(0d, PsopHelper.valueAt(p, 10d).doubleValue(), 1e-9);
    }

    @Test
    public void testConvolution_simple3() {
        List<LinearSignalFragment> es = new ArrayList<LinearSignalFragment>();
        es.add(new LinearSignalFragment(3.7, 3.9, 5));
        IPsop p = EventSignalUtil.convoluteScaleAndProject(es, 1, 0.1, new Interval(0, 10));
        assertEquals(5d, PsopHelper.getMaxValue(p).doubleValue(), 1e-9);
        assertEquals(0d, PsopHelper.valueAt(p, 3.7d).doubleValue(), 1e-9);
        assertEquals(2.5d, PsopHelper.valueAt(p, 3.75d).doubleValue(), 1e-9);
        assertEquals(5d, PsopHelper.valueAt(p, 3.8d).doubleValue(), 1e-9);
        assertEquals(5d, PsopHelper.valueAt(p, 3.9d).doubleValue(), 1e-9);
        assertEquals(2.5d, PsopHelper.valueAt(p, 3.95d).doubleValue(), 1e-9);
        assertEquals(0d, PsopHelper.valueAt(p, 4d).doubleValue(), 1e-9);
    }

    @Test
    public void testConvolution_large() {
        List<LinearSignalFragment> es = new ArrayList<LinearSignalFragment>();
        for (int i = 0; i < 1000; i++) {
            es.add(new LinearSignalFragment(i, (double)i + 0.2, 5));
        }
        IPsop p = EventSignalUtil.convoluteScaleAndProject(es, 60, 30, new Interval(0, 1030));
        assertEquals(0d, PsopHelper.getDomainLowerBound(p).doubleValue(), 1e-9);
        assertEquals(1030d, PsopHelper.getDomainUpperBound(p).doubleValue(), 1e-9);
        // Why is 60 correct? Between i and i + 0.2, one page is printed: this is approx
        // 60 pages per
        // 60 time units (normalization equals 60)...
        assertEquals(60d, PsopHelper.valueAt(p, 200).doubleValue(), 1e-9);
    }

    @Test
    public void testConvolution_Dirac() {
        List<LinearSignalFragment> es = new ArrayList<LinearSignalFragment>();
        es.add(new LinearSignalFragment(10, 1));
        es.add(new LinearSignalFragment(20, 1));
        IPsop p = EventSignalUtil.convoluteScaleAndProject(es, 1, 3, new Interval(0, 100));
        assertEquals(1d / 3d, PsopHelper.getMaxValue(p).doubleValue(), 1e-9);
        assertEquals(0d, PsopHelper.valueAt(p, 8d).doubleValue(), 1e-9);
        assertEquals(1d / 3d, PsopHelper.valueAt(p, 10d).doubleValue(), 1e-9);
        assertEquals(1d / 3d, PsopHelper.valueAt(p, 12.99d).doubleValue(), 1e-9);
        assertEquals(0d, PsopHelper.valueAt(p, 13d).doubleValue(), 1e-9);
        assertEquals(1d / 3d, PsopHelper.valueAt(p, 20d).doubleValue(), 1e-9);
        assertEquals(1d / 3d, PsopHelper.valueAt(p, 22.99d).doubleValue(), 1e-9);
        assertEquals(0d, PsopHelper.valueAt(p, 23d).doubleValue(), 1e-9);
//        System.out.println(p);
    }

    @Test
    public void testInstantaneous_Dirac() {
        assertThrows(UnsupportedOperationException.class, () -> {
            List<LinearSignalFragment> es = new ArrayList<LinearSignalFragment>();
            es.add(new LinearSignalFragment(1, 1));
            EventSignalUtil.computeInstantaneous(es, 1, new Interval(0, 10));
        });
    }
}
