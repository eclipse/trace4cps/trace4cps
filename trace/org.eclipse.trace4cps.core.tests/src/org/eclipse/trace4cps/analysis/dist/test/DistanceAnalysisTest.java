/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.dist.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.eclipse.trace4cps.analysis.cpa.test.CPATest;
import org.eclipse.trace4cps.analysis.dist.DefaultRepresentation;
import org.eclipse.trace4cps.analysis.dist.DistanceAnalysis;
import org.eclipse.trace4cps.analysis.dist.DistanceResult;
import org.eclipse.trace4cps.core.ITrace;
import org.junit.jupiter.api.Test;

public final class DistanceAnalysisTest {
    @Test
    public void testSimple() {
        ITrace reference = CPATest.createDummyModel();
        ITrace traceToAnnotate = CPATest.createDummyModel4();
        DistanceResult diff = DistanceAnalysis.distance(reference, traceToAnnotate, new DefaultRepresentation(), false);
        assertEquals(15, diff.getDistance());
        assertEquals(3, diff.getInterestCount().size());
    }
}
