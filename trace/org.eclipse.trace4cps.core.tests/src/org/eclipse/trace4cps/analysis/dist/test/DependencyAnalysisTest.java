/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.dist.test;

import java.util.List;

import org.eclipse.trace4cps.analysis.dist.DependencyInclusionCheck;
import org.eclipse.trace4cps.core.IDependency;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.impl.Dependency;
import org.eclipse.trace4cps.core.impl.Event;
import org.eclipse.trace4cps.core.impl.Trace;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DependencyAnalysisTest {
    private static final String W = "weight";

    @Test
    public void testCheck1() {
        List<IDependency> deps = DependencyInclusionCheck.check(getModelTrace(), getSystemTrace1(), W);
        Assertions.assertEquals(0, deps.size());
    }

    @Test
    public void testCheck2() {
        List<IDependency> deps = DependencyInclusionCheck.check(getModelTrace(), getSystemTrace2(), W);
        Assertions.assertEquals(2, deps.size());
    }

    @Test
    public void testCheck3() {
        List<IDependency> deps = DependencyInclusionCheck.check(getModelTrace(), getSystemTrace2(), null);
        Assertions.assertEquals(0, deps.size());
    }

    @Test
    public void testCheck4() {
        List<IDependency> deps = DependencyInclusionCheck.check(getModelTrace(), getSystemTrace3(), null);
        Assertions.assertEquals(1, deps.size());
    }

    private ITrace getModelTrace() {
        Trace t = new Trace();
        IEvent e1 = create(t, 0, "e1");
        IEvent e2 = create(t, 3, "e2");
        IEvent e3 = create(t, 10, "e3");
        create(t, e1, e2, 2.5);
        create(t, e2, e3, 6);
        create(t, e1, e3, 10);
        return t;
    }

    private ITrace getSystemTrace1() {
        Trace t = new Trace();
        create(t, 0, "e1");
        create(t, 3, "e2");
        create(t, 10, "e3");
        return t;
    }

    private ITrace getSystemTrace2() {
        Trace t = new Trace();
        create(t, 0, "e1");
        create(t, 2, "e2");
        create(t, 8, "e3");
        return t;
    }

    private ITrace getSystemTrace3() {
        Trace t = new Trace();
        create(t, 0, "e1");
        create(t, -2, "e2");
        create(t, 8, "e3");
        return t;
    }

    private IEvent create(Trace trace, double t, String name) {
        IEvent e = new Event(t);
        e.setAttribute("name", name);
        trace.add(e);
        return e;
    }

    private IDependency create(Trace trace, IEvent src, IEvent dst, double w) {
        IDependency d = new Dependency(src, dst);
        d.setAttribute(W, Double.toString(w));
        trace.add(d);
        return d;
    }
}
