/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.impl.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.concurrent.TimeUnit;

import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.impl.Claim;
import org.eclipse.trace4cps.core.impl.Resource;
import org.eclipse.trace4cps.core.impl.Trace;
import org.junit.jupiter.api.Test;

public class TraceTest {
    @Test
    public void testTraceTimeUnit() {
        Trace t = new Trace();
        assertEquals(TimeUnit.SECONDS, t.getTimeUnit());
        assertEquals(0L, t.getTimeOffset());
    }

    @Test
    public void testTimeOffset() {
        Trace t = new Trace();
        t.setTimeUnit(TimeUnit.MICROSECONDS);
        t.setOffset(-10L);
        assertEquals(TimeUnit.MICROSECONDS, t.getTimeUnit());
        assertEquals(-10L, t.getTimeOffset());
    }

    @Test
    public void addClaimsOrdering() {
        IResource r1 = new Resource(1, false);
        Claim c1 = new Claim(0, 10, r1, 0.2);
        IResource r2 = new Resource(2, false);
        Claim c2 = new Claim(5, 15, r2, 0.2);

        Trace trace = new Trace();
        trace.add(c2);
        assertEquals(c2.getStartEvent(), trace.getEvents().get(0));
        assertEquals(c2.getEndEvent(), trace.getEvents().get(1));

        trace.add(c1);
        assertEquals(c1, trace.getClaims().get(0));
        assertEquals(c2, trace.getClaims().get(1));

        assertEquals(c1.getStartEvent(), trace.getEvents().get(0));
        assertEquals(c2.getStartEvent(), trace.getEvents().get(1));
        assertEquals(c1.getEndEvent(), trace.getEvents().get(2));
        assertEquals(c2.getEndEvent(), trace.getEvents().get(3));
    }
}
