/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.impl.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.eclipse.trace4cps.core.impl.Resource;
import org.junit.jupiter.api.Test;

public class ResourceTest {
    @Test
    public void testNormal() {
        Resource r = new Resource(10, true);
        assertEquals(10, r.getCapacity());
        assertEquals(true, r.useOffset());
    }

    @Test
    public void testInvalid() {
        assertThrows(IllegalArgumentException.class, () -> { new Resource(0d, true); });
    }
}
