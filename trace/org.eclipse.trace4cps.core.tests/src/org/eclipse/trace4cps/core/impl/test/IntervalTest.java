/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.impl.test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.trace4cps.core.impl.Interval;
import org.junit.jupiter.api.Test;

public class IntervalTest {
    @Test
    public void testIntervalConstruction1() {
        assertNotNull(new Interval(0, 10));
        assertNotNull(new Interval(-2, 10));
        assertNotNull(new Interval(-8, -2));
    }

    @Test
    public void testEmpty() {
        assertTrue(new Interval(4, 3).isEmpty());
        assertTrue(new Interval(4, 4).isEmpty());
        assertFalse(new Interval(4, false, 4, false).isEmpty());
    }

    @Test
    public void testContains() {
        Interval i = new Interval(5, 10);
        assertTrue(i.contains(6));
        assertTrue(i.contains(5));
        assertFalse(i.contains(10));
        assertFalse(i.contains(3));
        assertFalse(i.contains(11));
    }
}
