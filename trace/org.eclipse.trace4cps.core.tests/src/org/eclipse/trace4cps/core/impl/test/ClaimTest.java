/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.impl.test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.impl.Claim;
import org.eclipse.trace4cps.core.impl.Resource;
import org.junit.jupiter.api.Test;

public class ClaimTest {
    @Test
    public void testClaimCtor1() {
        IResource r = new Resource(1, false);
        Claim c = new Claim(0, 10, r, 0.2);
        assertNotNull(c);
    }

    @Test
    public void testClaimCtor2() {
        IResource r = new Resource(1, true);
        Claim c = new Claim(0, 10, r, 0.1, 0.2);
        assertNotNull(c);
    }

    @Test
    public void testClaimCtorInvalid1() {
        IResource r = new Resource(1, true);
        assertThrows(IllegalArgumentException.class, () -> { new Claim(0, 10, r, 0.2); });
    }

    @Test
    public void testClaimCtorInvalid2() {
        IResource r = new Resource(1, false);
        assertThrows(IllegalArgumentException.class, () -> { new Claim(0, 10, r, 0.1, 0.2); });
    }

    @Test
    public void testClaimCtorInvalid3() {
        IResource r = new Resource(1, false);
        assertThrows(IllegalArgumentException.class, () -> { new Claim(20, 10, r, 0.2); });
    }

    @Test
    public void testClaimCtorInvalid4() {
        IResource r = new Resource(1, true);
        assertThrows(IllegalArgumentException.class, () -> { new Claim(10, 20, r, 2.3, 0.2); });
    }

    @Test
    public void testClaimEvents() {
        IResource r = new Resource(1, true);
        Claim c = new Claim(0, 10, r, 0.1, 0.2);
        assertNotNull(c.getStartEvent());
        assertNotNull(c.getEndEvent());
    }
}
