/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.impl.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.trace4cps.analysis.mtl.DefaultAtomicProposition;
import org.eclipse.trace4cps.core.ClaimEventType;
import org.eclipse.trace4cps.core.IClaimEvent;
import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.core.impl.Claim;
import org.eclipse.trace4cps.core.impl.Resource;
import org.junit.jupiter.api.Test;

public class ClaimEventTest {
    @Test
    public void testGetType() throws TraceException {
        IResource r = new Resource(10, false);
        Claim c = new Claim(0, 10, r, 0.2);
        IClaimEvent e1 = c.getStartEvent();
        IClaimEvent e2 = c.getEndEvent();
        assertEquals(ClaimEventType.START, e1.getType());
        assertEquals(ClaimEventType.END, e2.getType());
    }

    @Test
    public void testGetTimestamp() throws TraceException {
        IResource r = new Resource(10, false);
        Claim c = new Claim(0, 10, r, 0.2);
        IClaimEvent e1 = c.getStartEvent();
        IClaimEvent e2 = c.getEndEvent();
        assertEquals(0d, e1.getTimestamp().doubleValue());
        assertEquals(10d, e2.getTimestamp().doubleValue());
    }

    @Test
    public void testClaim() throws TraceException {
        IResource r = new Resource(10, false);
        Claim c = new Claim(0, 10, r, 0.2);
        IClaimEvent e1 = c.getStartEvent();
        IClaimEvent e2 = c.getEndEvent();
        assertEquals(c, e1.getClaim());
        assertEquals(c, e2.getClaim());
    }

    @Test
    public void testAttributeDelegation() throws TraceException {
        IResource r = new Resource(10, false);
        Claim c = new Claim(0, 10, r, 0.2);
        IClaimEvent e1 = c.getStartEvent();
        IClaimEvent e2 = c.getEndEvent();

        e1.setAttribute("e1", "e1");
        e2.setAttribute("e2", "e2");
        c.setAttribute("c", "c");

        Map<String, String> expected = new HashMap<>();
        expected.put("c", "c");
        expected.put("e1", "e1");
        expected.put("e2", "e2");

        assertEquals(expected, c.getAttributes());
        assertEquals(expected, e1.getAttributes());
        assertEquals(expected, e2.getAttributes());
    }

    @Test
    public void testSatisfies() throws TraceException {
        IResource r = new Resource(10, false);
        Claim c = new Claim(0, 10, r, 0.2);
        IClaimEvent e1 = c.getStartEvent();
        IClaimEvent e2 = c.getEndEvent();
        c.setAttribute("c", "c");

        assertTrue(e1.satisfies(new DefaultAtomicProposition()));
        assertTrue(e1.satisfies(new DefaultAtomicProposition("c", "c")));
        assertFalse(e1.satisfies(new DefaultAtomicProposition("c", "d")));
        assertTrue(e1.satisfies(new DefaultAtomicProposition(ClaimEventType.START, "c", "c")));
        assertFalse(e1.satisfies(new DefaultAtomicProposition(ClaimEventType.END, "c", "c")));
        assertFalse(e2.satisfies(new DefaultAtomicProposition(ClaimEventType.START, "c", "c")));
        assertTrue(e2.satisfies(new DefaultAtomicProposition(ClaimEventType.END, "c", "c")));
    }
}
