/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.impl.test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.trace4cps.analysis.mtl.DefaultAtomicProposition;
import org.eclipse.trace4cps.core.ClaimEventType;
import org.eclipse.trace4cps.core.impl.Event;
import org.junit.jupiter.api.Test;

public class EventTest {
    @Test
    public void testSatisfies() {
        Event e = new Event(10d);
        e.setAttribute("c", "c");
        assertTrue(e.satisfies(new DefaultAtomicProposition()));
        assertTrue(e.satisfies(new DefaultAtomicProposition("c", "c")));
        assertFalse(e.satisfies(new DefaultAtomicProposition("c", "d")));
        assertFalse(e.satisfies(new DefaultAtomicProposition(ClaimEventType.END, "c", "c")));
        assertFalse(e.satisfies(new DefaultAtomicProposition(ClaimEventType.START, "c", "c")));
    }
}
