/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.impl.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.trace4cps.core.impl.AttributeAware;
import org.junit.jupiter.api.Test;

public class AttributeAwareTest {
    @Test
    public void testSetGet() {
        AttributeAware a = new AttributeAware();
        a.setAttribute("a", "1");
        a.setAttribute("b", "2");
        assertEquals("1", a.getAttributeValue("a"));
        assertEquals("2", a.getAttributeValue("b"));
    }

    @Test
    public void testOverwrite() {
        AttributeAware a = new AttributeAware();
        a.setAttribute("a", "1");
        assertEquals("1", a.getAttributeValue("a"));
        a.setAttribute("a", "2");
        assertEquals("2", a.getAttributeValue("a"));
    }

    @Test
    public void testSetAllGetAll() {
        AttributeAware a = new AttributeAware();
        a.setAttribute("a", "1");
        a.setAttribute("b", "2");
        Map<String, String> newAtts = new HashMap<>();
        newAtts.put("c", "3");
        a.setAttributes(newAtts);
        assertEquals("3", a.getAttributeValue("c"));
        assertEquals(1, a.getAttributes().size());
    }

    @Test
    public void testNewLineValue() {
        AttributeAware a = new AttributeAware();
        a.setAttribute("a", "1\n2");
        assertEquals("1 2", a.getAttributeValue("a"));
    }
}
