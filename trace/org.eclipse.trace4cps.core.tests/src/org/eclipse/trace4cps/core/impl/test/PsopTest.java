/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.impl.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.eclipse.trace4cps.core.impl.Interval;
import org.eclipse.trace4cps.core.impl.Psop;
import org.eclipse.trace4cps.core.impl.PsopFragment;
import org.junit.jupiter.api.Test;

public class PsopTest {
    @Test
    public void testAdd() {
        Psop p = new Psop();
        p.add(new PsopFragment(10, 20, 30, new Interval(0, 1)));
        p.add(new PsopFragment(10, 20, 30, new Interval(1, 2)));
        assertEquals(2, p.getFragments().size());
    }

    @Test
    public void testAddBegin() {
        Psop p = new Psop();
        p.add(new PsopFragment(10, 20, 30, new Interval(0, 1)));
        p.addAtBegin(new PsopFragment(10, 20, 30, new Interval(-1, 0)));
        assertEquals(2, p.getFragments().size());
    }

    @Test
    public void testAddInvalid1() {
        Psop p = new Psop();
        p.add(new PsopFragment(10, 20, 30, new Interval(0, 1)));
        assertThrows(IllegalArgumentException.class,
                () -> { p.add(new PsopFragment(10, 20, 30, new Interval(2, 3))); });
    }

    @Test
    public void testAddInvalid2() {
        Psop p = new Psop();
        p.add(new PsopFragment(10, 20, 30, new Interval(0, 1)));
        assertThrows(IllegalArgumentException.class,
                () -> { p.addAtBegin(new PsopFragment(10, 20, 30, new Interval(-2, -1))); });
    }

    @Test
    public void testMergeConstant() {
        Psop p = new Psop();
        p.add(new PsopFragment(10, 0, 0, new Interval(0, 1)));
        p.add(new PsopFragment(10, 0, 0, new Interval(1, 2)));
        p.merge();
        assertEquals(1, p.getFragments().size());
        assertEquals(10, p.getFragments().get(0).getC());
        assertEquals(0, p.getFragments().get(0).getB());
        assertEquals(0, p.getFragments().get(0).getA());
        assertEquals(0, p.getFragments().get(0).dom().lb());
        assertEquals(2, p.getFragments().get(0).dom().ub());
    }

    @Test
    public void testMergeLinear() {
        Psop p = new Psop();
        p.add(new PsopFragment(10, 1, 0, new Interval(0, 1)));
        p.add(new PsopFragment(11, 1, 0, new Interval(1, 2)));
        p.merge();
        assertEquals(1, p.getFragments().size());
        assertEquals(10, p.getFragments().get(0).getC());
        assertEquals(1, p.getFragments().get(0).getB());
        assertEquals(0, p.getFragments().get(0).getA());
        assertEquals(0, p.getFragments().get(0).dom().lb());
        assertEquals(2, p.getFragments().get(0).dom().ub());
    }

    @Test
    public void testMergeQuadratic() {
        Psop p = new Psop();
        p.add(new PsopFragment(10, 0, 2, new Interval(0, 1)));
        p.add(new PsopFragment(12, 0, 2, new Interval(1, 2)));
        p.merge();
        assertEquals(2, p.getFragments().size()); // not merged
    }
}
