/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.impl.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.eclipse.trace4cps.core.Shape;
import org.eclipse.trace4cps.core.impl.Interval;
import org.eclipse.trace4cps.core.impl.PsopFragment;
import org.junit.jupiter.api.Test;

public class PsopFragmentTest {
    @Test
    public void testCtor() {
        PsopFragment f = new PsopFragment(10, 20, 30, new Interval(0, 1));
        assertNotNull(f);
    }

    @Test
    public void testCtorInvalid1() {
        assertThrows(IllegalArgumentException.class,
                () -> { new PsopFragment(10, 20, 30, new Interval(0, true, 1, true)); });
    }

    @Test
    public void testCtorInvalid2() {
        assertThrows(IllegalArgumentException.class, () -> { new PsopFragment(null, 20, 30, new Interval(0, 1)); });
    }

    @Test
    public void testCtorInvalid3() {
        assertThrows(IllegalArgumentException.class, () -> { new PsopFragment(10, null, 30, new Interval(0, 1)); });
    }

    @Test
    public void testCtorInvalid4() {
        assertThrows(IllegalArgumentException.class, () -> { new PsopFragment(10, 20, null, new Interval(0, 1)); });
    }

    @Test
    public void testCtorInvalid5() {
        assertThrows(IllegalArgumentException.class, () -> { new PsopFragment(10, 20, 30, new Interval(0, -1)); });
    }

    @Test
    public void testOrder() {
        assertEquals(0, new PsopFragment(10, 0, 0, new Interval(0, 1)).getOrder());
        assertEquals(1, new PsopFragment(10, 20, 0, new Interval(0, 1)).getOrder());
        assertEquals(2, new PsopFragment(10, 20, 30, new Interval(0, 1)).getOrder());
    }

    @Test
    public void testShape() {
        assertEquals(Shape.CONSTANT, new PsopFragment(10, 0, 0, new Interval(0, 1)).getShape());
        assertEquals(Shape.INCREASING, new PsopFragment(10, 20, 0, new Interval(0, 1)).getShape());
        assertEquals(Shape.INCREASING, new PsopFragment(10, 0, 0.1, new Interval(0, 1)).getShape());
        assertEquals(Shape.DECREASING, new PsopFragment(10, -2, 0, new Interval(0, 1)).getShape());
        assertEquals(Shape.DECREASING, new PsopFragment(10, 0, -0.1, new Interval(0, 1)).getShape());

        assertEquals(Shape.PARABOLA_CAP, new PsopFragment(10, 2, -2, new Interval(0, 1)).getShape());
        assertEquals(Shape.PARABOLA_CUP, new PsopFragment(10, -2, 2, new Interval(0, 1)).getShape());
    }
}
