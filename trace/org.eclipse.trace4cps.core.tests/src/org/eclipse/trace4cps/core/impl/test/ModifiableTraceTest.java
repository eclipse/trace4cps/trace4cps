/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.impl.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.trace4cps.core.IAttributeAware;
import org.eclipse.trace4cps.core.IAttributeFilter;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IDependency;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.core.impl.Claim;
import org.eclipse.trace4cps.core.impl.Dependency;
import org.eclipse.trace4cps.core.impl.Event;
import org.eclipse.trace4cps.core.impl.Interval;
import org.eclipse.trace4cps.core.impl.ModifiableTrace;
import org.eclipse.trace4cps.core.impl.Psop;
import org.eclipse.trace4cps.core.impl.PsopFragment;
import org.eclipse.trace4cps.core.impl.Resource;
import org.eclipse.trace4cps.core.impl.Trace;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ModifiableTraceTest {
    private IEvent ex1, ex2;

    private ModifiableTrace t;

    @BeforeEach
    public void createTestTrace() {
        Trace t = new Trace();

        Resource r1 = new Resource(1, false);
        r1.setAttribute("name", "R1");
        Resource r2 = new Resource(1, false);
        r2.setAttribute("name", "R2");

        Claim c1 = new Claim(0, 10, r1, 1);
        c1.setAttribute("name", "C1");
        t.add(c1);
        Claim c2 = new Claim(5, 12, r2, 1);
        c2.setAttribute("name", "C2");
        t.add(c2);

        Event e1 = new Event(54);
        e1.setAttribute("name", "E1");
        t.add(e1);
        Event e2 = new Event(67);
        e2.setAttribute("name", "E2");
        t.add(e2);

        Dependency d1 = new Dependency(c1.getEndEvent(), e1);
        d1.setAttribute("name", "D1");
        t.add(d1);
        Dependency d2 = new Dependency(c2.getStartEvent(), e2);
        d2.setAttribute("name", "D2");
        t.add(d2);

        Psop s1 = new Psop();
        s1.add(new PsopFragment(0, 1, 2, new Interval(0, 10)));
        s1.setAttribute("name", "S1");
        t.add(s1);
        Psop s2 = new Psop();
        s2.add(new PsopFragment(20, 1, 2, new Interval(0, 60)));
        s2.setAttribute("name", "S2");
        t.add(s2);

        this.ex1 = c1.getStartEvent();
        this.ex2 = c2.getEndEvent();
        this.t = new ModifiableTrace(t);
    }

    @Test
    public void testNoFilter() {
        assertNothingFiltered();
    }

    @Test
    public void testNullFilter() {
        t.addFilterAndRecalculate(TracePart.EVENT, null);
        assertEquals(6, t.getEvents().size()); // also start and end events of claims
    }

    @Test
    public void testSignalFilter() {
        t.addFilterAndRecalculate(TracePart.SIGNAL, new NameFilter("S2"));
        assertEquals(1, t.getSignals().size());
        assertName(t.getSignals(), "S2");
        t.addFilterAndRecalculate(TracePart.SIGNAL, new NameFilter("S1"));
        assertEquals(0, t.getSignals().size());
    }

    @Test
    public void testEventFilter() {
        t.addFilterAndRecalculate(TracePart.EVENT, new NameFilter("E1"));
        assertEquals(1, t.getEvents().size());
        assertName(t.getEvents(), "E1");
        t.addFilterAndRecalculate(TracePart.EVENT, new NameFilter("E2"));
        assertEquals(0, t.getEvents().size());
    }

    @Test
    public void testResourceFilter1() {
        t.addFilterAndRecalculate(TracePart.RESOURCE, new NameFilter("R1"));
        assertEquals(1, t.getResources().size());
        assertName(t.getResources(), "R1");
        assertEquals(1, t.getClaims().size());
        assertName(t.getClaims(), "C1");
        assertEquals(1, t.getDependencies().size());
        assertName(t.getDependencies(), "D1");
        assertEquals(4, t.getEvents().size());
        assertEquals(2, t.getSignals().size());
    }

    @Test
    public void testResourceFilter2() {
        t.addFilterAndRecalculate(TracePart.RESOURCE, new NameFilter("R1"));
        t.addFilterAndRecalculate(TracePart.RESOURCE, new NameFilter("R2"));
        assertEquals(0, t.getResources().size());
    }

    @Test
    public void testClaimFilter() {
        t.addFilterAndRecalculate(TracePart.CLAIM, new NameFilter("C2"));
        assertEquals(1, t.getClaims().size());
        assertName(t.getClaims(), "C2");
        assertEquals(1, t.getDependencies().size());
        assertName(t.getDependencies(), "D2");
        assertEquals(4, t.getEvents().size());
        assertEquals(2, t.getSignals().size());
    }

    @Test
    public void testResourceAndClaimFilter1() {
        t.addFilterAndRecalculate(TracePart.RESOURCE, new NameFilter("R1"));
        t.addFilterAndRecalculate(TracePart.RESOURCE, new NameFilter("R2"));
        assertEquals(0, t.getClaims().size());
        assertEquals(2, t.getEvents().size());
    }

    @Test
    public void testResourceAndClaimFilter2() {
        t.addFilterAndRecalculate(TracePart.RESOURCE, new NameFilter("R1"));
        t.addFilterAndRecalculate(TracePart.CLAIM, new NameFilter("C2"));
        assertEquals(1, t.getResources().size());
        assertEquals(0, t.getClaims().size());
        assertEquals(2, t.getEvents().size());
        assertEquals(0, t.getDependencies().size());
        assertEquals(2, t.getSignals().size());
    }

    @Test
    public void testClearAll() {
        t.addFilterAndRecalculate(TracePart.RESOURCE, new NameFilter("R1"));
        t.addFilterAndRecalculate(TracePart.CLAIM, new NameFilter("C2"));
        t.clearFilter(TracePart.ALL);
        assertNothingFiltered();
    }

    @Test
    public void testExtension_none() {
        assertFalse(t.hasExtension(TracePart.SIGNAL));
        assertFalse(t.hasExtension(TracePart.DEPENDENCY));
        assertFalse(t.hasExtension(TracePart.CLAIM));
        assertFalse(t.hasExtension(TracePart.EVENT));
        assertFalse(t.hasExtension(TracePart.RESOURCE));
        assertFalse(t.hasExtension(TracePart.ALL));
    }

    @Test
    public void testExtension_signal_add() {
        IPsop ex = new Psop();
        ex.setAttribute("name", "S3");
        t.addSignal(ex);
        assertTrue(t.hasExtension(TracePart.SIGNAL));
        assertEquals(3, t.getSignals().size());
    }

    @Test
    public void testExtension_signal_filter() {
        IPsop ex = new Psop();
        ex.setAttribute("name", "S3");
        t.addSignal(ex);
        t.addFilterAndRecalculate(TracePart.SIGNAL, new NameFilter("S2"));
        assertEquals(1, t.getSignals().size());
        t.clearFilter(TracePart.ALL);
        t.addFilterAndRecalculate(TracePart.SIGNAL, new NameFilter("S3"));
        assertEquals(1, t.getSignals().size());
    }

    @Test
    public void testExtension_dependency_add() {
        IDependency ex = new Dependency(ex1, ex2);
        ex.setAttribute("name", "D3");
        t.addDependencies(Collections.singletonList(ex));
        assertTrue(t.hasExtension(TracePart.DEPENDENCY));
        assertEquals(3, t.getDependencies().size());
    }

    @Test
    public void testExtension_dependency_filter1() {
        IDependency ex = new Dependency(ex1, ex2);
        ex.setAttribute("name", "D3");
        t.addDependencies(Collections.singletonList(ex));
        t.addFilterAndRecalculate(TracePart.DEPENDENCY, new NameFilter("D3"));
        assertEquals(1, t.getDependencies().size());
    }

    @Test
    public void testExtension_dependency_filter2() {
        IDependency ex = new Dependency(ex1, ex2);
        ex.setAttribute("name", "D3");
        t.addDependencies(Collections.singletonList(ex));
        assertEquals(3, t.getDependencies().size());
        t.addFilterAndRecalculate(TracePart.CLAIM, new NameFilter("C1"));
        assertEquals(1, t.getDependencies().size());
    }

    @Test
    public void testExtension_event_add() {
        assertFalse(t.hasExtension(TracePart.EVENT));
        IEvent e1 = new Event(-100);
        IEvent e2 = new Event(100);
        e1.setAttribute("name", "A");
        e2.setAttribute("name", "B");
        t.addEvents(Arrays.asList(e1, e2));
        assertTrue(t.hasExtension(TracePart.EVENT));
        assertEquals(8, t.getEvents().size());
        assertEquals("A", t.getEvents().get(0).getAttributeValue("name"));
        assertEquals("B", t.getEvents().get(7).getAttributeValue("name"));
        t.clearExtension(TracePart.EVENT);
        assertEquals(6, t.getEvents().size());
    }

    @Test
    public void testExtension_event_filter() {
        assertFalse(t.hasExtension(TracePart.EVENT));
        IEvent e1 = new Event(-100);
        IEvent e2 = new Event(100);
        e1.setAttribute("name", "A");
        e2.setAttribute("name", "B");
        t.addEvents(Arrays.asList(e1, e2));
        t.addFilterAndRecalculate(TracePart.EVENT, new NameFilter("A"));
        assertEquals(1, t.getEvents().size());
        assertEquals("A", t.getEvents().get(0).getAttributeValue("name"));
        t.clearExtension(TracePart.EVENT);
        assertEquals(0, t.getEvents().size());
        t.clearFilter(TracePart.EVENT);
        assertEquals(6, t.getEvents().size());
    }

    @Test
    public void testExtension_claim_add() {
        IResource r = new Resource(10, false);
        assertFalse(t.hasExtension(TracePart.CLAIM));
        IClaim c1 = new Claim(20, 30, r, 1);
        IClaim c2 = new Claim(50, 55, r, 1);
        c1.setAttribute("name", "A");
        c2.setAttribute("name", "B");

        t.addClaims(Arrays.asList(c1, c2));
        assertTrue(t.hasExtension(TracePart.CLAIM));
        assertEquals(4, t.getClaims().size());
        assertEquals(10, t.getEvents().size());
        assertEquals("A", t.getEvents().get(4).getAttributeValue("name"));
        assertEquals("B", t.getEvents().get(6).getAttributeValue("name"));

        t.clearExtension(TracePart.EVENT);
        assertEquals(10, t.getEvents().size()); // the IClaim extension is leading!

        t.clearExtension(TracePart.CLAIM);
        assertEquals(2, t.getClaims().size());
        assertEquals(6, t.getEvents().size());
    }

    @Test
    public void testExtension_claim_filter() {
        IResource r = new Resource(10, false);
        assertFalse(t.hasExtension(TracePart.CLAIM));
        IClaim c1 = new Claim(20, 30, r, 1);
        IClaim c2 = new Claim(50, 55, r, 1);
        c1.setAttribute("name", "A");
        c2.setAttribute("name", "B");
        t.addClaims(Arrays.asList(c1, c2));
        assertTrue(t.hasExtension(TracePart.CLAIM));
        assertEquals(4, t.getClaims().size());
        assertEquals(10, t.getEvents().size());

        t.addFilterAndRecalculate(TracePart.CLAIM, new NameFilter("A"));
        assertEquals(1, t.getClaims().size());
        assertEquals(4, t.getEvents().size()); // also the two regular events

        t.clearFilter(TracePart.CLAIM);
        assertEquals(4, t.getClaims().size());
        assertEquals(10, t.getEvents().size());

        t.clearExtension(TracePart.CLAIM);
        assertEquals(2, t.getClaims().size());
        assertEquals(6, t.getEvents().size());
    }

    private void assertNothingFiltered() {
        assertEquals(2, t.getResources().size());
        assertEquals(2, t.getClaims().size());
        assertEquals(6, t.getEvents().size()); // also start and end events of claims
        assertEquals(2, t.getDependencies().size());
        assertEquals(2, t.getSignals().size());
    }

    private void assertName(List<? extends IAttributeAware> l, String name) {
        assertEquals(name, l.get(0).getAttributeValue("name"));
    }

    private static final class NameFilter implements IAttributeFilter {
        private final String value;

        public NameFilter(String v) {
            this.value = v;
        }

        @Override
        public boolean include(IAttributeAware a) {
            return value.equals(a.getAttributeValue("name"));
        }
    }
}
