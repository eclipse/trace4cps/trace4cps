/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.io.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.eclipse.trace4cps.core.IAttributeAware;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IDependency;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.IPsopFragment;
import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.impl.ClaimEvent;
import org.eclipse.trace4cps.core.impl.Event;
import org.eclipse.trace4cps.core.impl.TraceHelper;
import org.eclipse.trace4cps.core.io.ParseException;
import org.eclipse.trace4cps.core.io.TraceReader;
import org.eclipse.trace4cps.core.io.TraceWriter;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class TraceIOTest {
    private static ITrace t;

    @BeforeAll
    public static void setup() throws Exception {
        t = TraceReader.readTrace(new File("../../trace-examples/test.etf"));
    }

    @Test
    public void testSimple() throws Exception {
        assertNotNull(t);
    }

    @Test
    public void testTimeUnitAndOffset() throws Exception {
        assertEquals(TimeUnit.MILLISECONDS, t.getTimeUnit());
        assertEquals(10283L, t.getTimeOffset());
    }

    @Test
    public void testTraceAttributes() throws Exception {
        Map<String, String> atts = t.getAttributes();
        assertEquals(3, atts.size());
        assertEquals("A", atts.get("some att"));
        assertEquals("some type", atts.get("type"));
        assertEquals("test.etf", t.getAttributeValue(TraceReader.FILENAME_ATT));
    }

    @Test
    public void testSimpleResources() throws Exception {
        assertResources(t);
    }

    @Test
    public void testSimpleClaims() throws Exception {
        assertClaims(t);
    }

    @Test
    public void testSimpleEvents() throws Exception {
        assertEvents(t);
    }

    @Test
    public void testSimpleDependencies() throws Exception {
        assertDependencies(t);
    }

    @Test
    public void testSimpleSignals() throws Exception {
        assertSignals(t);
    }

    @Test
    public void testSimpleAttributes() throws Exception {
        IClaim c1 = TraceHelper.get(t.getClaims(), "task", "B");
        assertAtributes(c1, "task", "B", "task's,name", "some", "\\", "\'");
    }

    @Test
    public void testWriteRead() throws Exception {
        File tmp = File.createTempFile("TraceIOTest", "tmp");
        tmp.deleteOnExit();
        TraceWriter.writeTrace(t, tmp);
        ITrace t2 = TraceReader.readTrace(tmp);
        assertResources(t2);
        assertClaims(t2);
        assertEvents(t2);
        assertDependencies(t2);
        assertSignals(t2);
        IClaim c0 = TraceHelper.get(t.getClaims(), "task", "B");
        assertAtributes(c0, "task", "B", "task's,name", "some", "\\", "\'");
        IClaim c1 = TraceHelper.get(t2.getClaims(), "task", "B");
        assertAtributes(c1, "task", "B", "task's,name", "some", "\\", "\'");
        assertAtributes(t2, "some att", "A", "type", "some type", TraceReader.FILENAME_ATT, tmp.getName());
    }

    @Test
    public void testWriteIllegalAttributes() throws Exception {
        Event e = new Event(10);
        e.setAttribute("illegal", "some\nmulti-line value");
        String val = e.getAttributeValue("illegal");
        assertEquals("some multi-line value", val);
    }

    @Test
    public void testReadInvalidPsopFragment() throws Exception {
        assertThrows(ParseException.class, () -> {
            String testPsop = "S  0;signal=S0 \nF  0  14.5  14.4  6.0 0.0 0.0\n";
            TraceReader.readTrace(testPsop);
        });
    }

    @Test
    public void testReadPsopFragment() throws Exception {
        String testPsop = "S  0;signal=S0 \nF  0  14.5  14.5  6.0 0.0 0.0\nF  0  14.5  14.6  6.0 0.0 0.0\n";
        ITrace trace = TraceReader.readTrace(testPsop);
        assertEquals(1, trace.getSignals().size());
        IPsop psop = trace.getSignals().get(0);
        assertEquals(1, psop.getFragments().size());
    }

    @Test
    public void testReadEmptyPsop() throws Exception {
        String testPsop = "S  0;signal=S0 \n";
        ITrace trace = TraceReader.readTrace(testPsop);
        assertEquals(0, trace.getSignals().size());
    }

    @Test
    public void testReadPsopNonConsecutiveFragments() {
        assertThrows(ParseException.class, () -> {
            String testPsop = "S  0;signal=S0 \nF  0  14.5  14.6  6.0 0.0 0.0\nF  0  14.7  14.8  6.0 0.0 0.0\n";
            TraceReader.readTrace(testPsop);
        });
    }

    private void assertResources(ITrace trace) {
        List<IResource> res = trace.getResources();
        assertEquals(2, res.size());
        assertResource(trace, "name", "CPU", 100d, false);
        assertResource(trace, "name", "RAM", 512d, true);
    }

    private void assertResource(ITrace trace, String key, String value, double capacity, boolean offset) {
        List<IResource> r = TraceHelper.filter(trace.getResources(), key, value);
        assertEquals(1, r.size());
        IResource resource = r.get(0);
        assertEquals(capacity, resource.getCapacity().doubleValue(), 1e-9);
        assertTrue(resource.useOffset() == offset);
    }

    private void assertClaims(ITrace trace) {
        List<IClaim> claims = trace.getClaims();
        assertEquals(2, claims.size());
        assertClaim(claims.get(0), 0.2, 13.2, TraceHelper.get(trace.getResources(), "name", "CPU"), Double.NaN, 100);
        assertClaim(claims.get(1), 0.4, 0.6, TraceHelper.get(trace.getResources(), "name", "RAM"), 128, 256);
    }

    private void assertClaim(IClaim claim, double t0, double t1, IResource r, double offset, double amount,
            String... att)
    {
        assertEquals(r, claim.getResource());
        assertEquals(t0, claim.getStartTime().doubleValue(), 1e-9);
        assertEquals(t1, claim.getEndTime().doubleValue(), 1e-9);
        assertEquals(offset, claim.getOffset().doubleValue(), 1e-9);
        assertEquals(amount, claim.getAmount().doubleValue(), 1e-9);
        for (int i = 0; i < att.length - 1; i += 2) {
            String k = att[i];
            String v = att[i + 1];
            String val = claim.getAttributeValue(k);
            assertEquals(v, val);
        }
    }

    private void assertAtributes(IAttributeAware a, String... att) {
        assertEquals(a.getAttributes().size(), att.length / 2);
        for (int i = 0; i < att.length - 1; i += 2) {
            String k = att[i];
            String v = att[i + 1];
            String val = a.getAttributeValue(k);
            assertEquals(v, val);
        }
    }

    private void assertEvents(ITrace trace) {
        List<IEvent> events = trace.getEvents();
        assertEquals(6, events.size());
        IEvent e0 = events.get(0);
        IEvent e1 = events.get(1);
        IEvent e2 = events.get(2);
        IEvent e3 = events.get(3);
        IEvent e4 = events.get(4);
        IEvent e5 = events.get(5);
        assertEquals(0.2, e0.getTimestamp().doubleValue(), 1e-9);
        assertEquals(0.4, e1.getTimestamp().doubleValue(), 1e-9);
        assertEquals(0.6, e2.getTimestamp().doubleValue(), 1e-9);
        assertEquals(13.2, e3.getTimestamp().doubleValue(), 1e-9);
        assertEquals(42.4, e4.getTimestamp().doubleValue(), 1e-9);
        assertEquals(50.0, e5.getTimestamp().doubleValue(), 1e-9);
        assertTrue(e0 instanceof ClaimEvent);
        assertTrue(e1 instanceof ClaimEvent);
        assertTrue(e2 instanceof ClaimEvent);
        assertTrue(e3 instanceof ClaimEvent);
        assertFalse(e4 instanceof ClaimEvent);
        assertFalse(e5 instanceof ClaimEvent);
        assertEquals(trace.getClaims().get(0), ((ClaimEvent)e0).getClaim());
        assertEquals(trace.getClaims().get(1), ((ClaimEvent)e1).getClaim());
        assertEquals(trace.getClaims().get(1), ((ClaimEvent)e2).getClaim());
        assertEquals(trace.getClaims().get(0), ((ClaimEvent)e3).getClaim());
    }

    private void assertDependencies(ITrace t) {
        List<IDependency> deps = t.getDependencies();
        List<IClaim> claims = t.getClaims();
        List<IEvent> events = t.getEvents();
        assertEquals(3, deps.size());
        IDependency d0 = TraceHelper.get(deps, "name", "A");
        IDependency d1 = TraceHelper.get(deps, "name", "B");
        IDependency d2 = TraceHelper.get(deps, "name", "C");
        IClaim c0 = TraceHelper.get(claims, "task", "A");
        IClaim c1 = TraceHelper.get(claims, "task", "B");
        IEvent e0 = TraceHelper.get(events, "name", "E");
        IEvent e1 = TraceHelper.get(events, "name", "B");

        assertTrue(d0.getSrc() instanceof ClaimEvent);
        assertTrue(d0.getDst() instanceof ClaimEvent);
        assertEquals(((ClaimEvent)d0.getSrc()).getClaim(), c0);
        assertEquals(((ClaimEvent)d0.getDst()).getClaim(), c1);

        assertFalse(d1.getSrc() instanceof ClaimEvent);
        assertFalse(d1.getDst() instanceof ClaimEvent);
        assertEquals(d1.getSrc(), e0);
        assertEquals(d1.getDst(), e1);

        assertTrue(d2.getSrc() instanceof ClaimEvent);
        assertFalse(d2.getDst() instanceof ClaimEvent);
        assertEquals(((ClaimEvent)d2.getSrc()).getClaim(), c0);
        assertEquals(d2.getDst(), e0);
    }

    private void assertSignals(ITrace t) {
        List<IPsop> signals = t.getSignals();
        assertEquals(1, signals.size());
        IPsop s = signals.get(0);
        assertEquals(2, s.getFragments().size());
        IPsopFragment f0 = s.getFragments().get(0);
        IPsopFragment f1 = s.getFragments().get(1);
        assertFragment(f0, 0, 2, 3, 1.2, -0.4);
        assertFragment(f1, 2, 2.5, 4, -0.3, 5);
    }

    private void assertFragment(IPsopFragment f, double t0, double t1, double c, double b, double a) {
        assertEquals(t0, f.dom().lb().doubleValue(), 1e-9);
        assertEquals(t1, f.dom().ub().doubleValue(), 1e-9);
        assertEquals(c, f.getC().doubleValue(), 1e-9);
        assertEquals(b, f.getB().doubleValue(), 1e-9);
        assertEquals(a, f.getA().doubleValue(), 1e-9);
    }
}
