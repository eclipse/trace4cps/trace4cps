<!--

    Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation

    This program and the accompanying materials are made
    available under the terms of the Eclipse Public License 2.0
    which is available at https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 5.0 Transitional//EN">

<html>
<head>
    <link rel="stylesheet" type="text/css" href="../css/style.css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title>TRACE - File Format</title>
</head>
<div>

<h1>TRACE File Format</h1>
The default TRACE file format is a simple text-based and human-readable format. It consists of
several elements to specify the events, claims, signals and dependencies.
This is a brief overview of the patterns that can be used to specify an
execution trace (more detailed explanation and constraints follow below):

<div class="code"># Comments start with a #
#
# The following three patterns are used to specify general
# properties of the execution trace:
#
'TU' &lt;time unit of time stamps in trace&gt;
'O'  &lt;offset of time stamps in trace&gt; 
'T'  &lt;attributes&gt;*

#
# Events are defined using the pattern:
#
'E'  &lt;event id&gt; &lt;t&gt; ';' &lt;attributes&gt;*

#
# The following two patterns are involved with
# defining claims and resources:
#
'R'  &lt;resource id&gt; &lt;capacity&gt; &lt;uses offset&gt; ';' &lt;attributes&gt;*
'C'  &lt;claim id> &lt;t0&gt; &lt;t1&gt; &lt;resource id&gt; &lt;offset&gt;? &lt;amount&gt; ';' &lt;attributes&gt;*

#
# Dependencies between events and or start and end of claims
# are defined with the following pattern:
#
'D'  &lt;dep id&gt; &lt;type&gt; &lt;src id&gt; &lt;dst id&gt; ';' &lt;attributes&gt;*

#
# Continuous signals are specified by piecewise second-order polynomials
# using the following patterns:
#
'S'  &lt;signal id&gt; ';' &lt;attributes&gt;*
'F'  &lt;signal id&gt; &lt;t0&gt; &lt;t1&gt; &lt;c&gt; &lt;b&gt; &lt;a&gt;
</div>

<h2>Specifying general trace properties</h2> 
<p>
General trace properties are specified by the <tt>TU</tt>, <tt>O</tt> and <tt>T</tt> lines.
The <tt>TU</tt> line specifies the time unit of all the time stamps that appear in the
execution trace. It must be specified at most once per trace file. If it is not
specified, then by default every time stamp is interpreted as a value in seconds.
The time unit specification can take one of the following values:
<div class="code">NANOSECONDS, MICROSECONDS, MILLISECONDS, SECONDS, MINUTES, HOURS
</div>
</p>

<p>
The <tt>O</tt> line specifies the offset of the time stamps in the trace with respect to
the Unix epoch, i.e., January 1970 00:00:00 UTC. The value should be an integer in
milliseconds. It must be specified at most once per trace file. If it is not specified,
then an offset of 0 is assumed.  
</p>

<p>
The <tt>T</tt> line specifies general attributes of the execution trace.
&lt;attributes&gt; define a (possibly empty) key-value mapping in which
each key-value pair consists of a string for the key, a '=' character and 
a string for the value. Pairs are separated by ',' characters.
Special characters thus are '=' and ','. If one of these is to be used in a key or
value string, then it must be escaped by pre-fixing the character with a '\'.
Key and value strings are trimmed, i.e., leading and trailing whitespace is removed.
An example is the following:
<div class="code">T name = experiment 1, origin = prototype X, date = Jan 12\, 2020
</div>
This line specifies that the trace has three general attributes: name,
origin and date. Note that the value of the date attribute
contains a ',' which thus is escaped.
</p>

<h2>Specifying events</h2> 
Events are specified by the <tt>E</tt> lines. An event has an identifier (unique within the events),
a time stamp and a number of attributes. An example is the following:
<div class="code">E   0   50.0 ; name=E1
E   1   42.4 ; name=E2, att = E2's name \= E2
</div>
Here two events are specified, and note the attributes of the second event in which the = is escaped
and thus part of the attribute value.

<h2>Specifying claims on resources</h2> 
<p>
Claims on resources are specified by a combination of <tt>R</tt> and <tt>C</tt>
lines. An <tt>R</tt> line specifies a resource, and a <tt>C</tt> line specifies
a claim of a certain resource.
A resource has:
<ul>
<li> an identifier (natural number; unique within the resources of the trace),</li>
<li> a capacity (positive real number),</li>
<li> a boolean that indicates whether claims on the resource specify an offset, and</li>
<li> a number of attributes.</li>
</ul>
A claim has:
<ul>
<li> an identifier (natural number; unique within the claims of the trace),</li>
<li> start and end time stamps (real numbers), a reference to a resource using the
resource's identifier,</li>
<li>an offset (real number) if and only if the resource has declared to use an offset,</li>
<li>an amount (positive real number), and</li>
<li> a number of attributes.</li>
</ul> 
An example is the following:
<div class="code">R   0   100.0   false ; name = CPU, unit = %
R   1   512     true  ; name = RAM, unit = MB

C   0   0.2   13.2    0   100.0      ; task=A
C   1   0.4   0.6     1   128    256 ; task=B
</div>
</p>
This example specifies two resources. The CPU resource has a capacity of 100
and does not use an offset.
The RAM resource has a capacity of 512 (MB as the attributes suggest) and
does require claims to specify an offset.
Also two claims are specified. The first claim, for task A, starts at 0.2 and ends at 13.2
time units. It claims 100.0 of the CPU resource (it specifies resource identifier
0, which is the identifier of the CPU resource).
The second claim, for task B, is from 0.4 to 0.6 time units and takes 256 of the RAM
resources, starting at an offset of 128. The offset in combination with the amount
thus specifies that the middle half of the memory is claimed.

<h2>Specifying dependencies</h2> 
Dependencies are specified by the <tt>D</tt> lines. A dependency has an identifier
(unique within the dependencies), a type (one of 0..8), a source (a claim or event identifier),
a destination (a claim or event identifier) and a number of attributes.
There are nine types of dependencies:
<ul>
<li>0 : claim start - claim start : both src and dst id refer to claims</li>
<li>1 : claim start - claim end : both src and dst id refer to claims</li>
<li>2 : claim end - claim start : both src and dst id refer to claims</li>
<li>3 : claim end - claim end : both src and dst id refer to claims</li>
<li>4 : event - event : both src and dst id refer to events</li>
<li>5 : claim start - event : src id refers to a claim and dst id refers to an event</li>
<li>6 : claim end - event : src id refers to a claim and dst id refers to an event</li>
<li>7 : event - claim start : src id refers to an event and dst id refers to a claim</li>
<li>8 : event - claim end : src id refers to an event and dst id refers to a claim</li>
</ul>
The following is an example that uses the claims and events from the examples above:
<div class="code">D   0   0   0   1 ; type=start-start
D   1   4   0   1 ; type=application
D   2   6   0   0 ; type=application
</div>
The first dependency has type 0 
and specifies a dependency from the start of task A to the start of task B.
The second dependency has type 4 
and specifies a dependency from event E1 to event E2.
The second dependency has type 6 
and specifies a dependency from the end of task A to event E1.
The attributes are used to indicate the type of the dependency.

<h2>Specifying signals</h2> 
Signals are specified by the <tt>S</tt> and <tt>F</tt> lines. There is exactly one
<tt>S</tt> line for each signal, and at least one <tt>F</tt> line for each signal.
The <tt>S</tt> line specifies a signal identifier (unique within the signals of the trace),
and a number of attributes for the signal.
The <tt>F</tt> lines then each encode a piecewise second-order polynomial fragment of
the signal. An <tt>F</tt> line has an signal identifier that specifies the signal to which
the fragment belongs. It then has five real numbers. The first two are the start and
end time stamps of the time domain of the fragment. The last three are the coefficients
for the second order polynomial (coefficient 0, 1 and 2 respectively). For instance:

<div class="code">S   0 ; name = x position
F   0   0    2.2   3   1.2   -0.4
F   0   2.2  2.5   4  -0.3    5
</div>
This encodes a single signal with two fragments and gives a piecewise polynomial on
the time domain [0, 2.5) with two fragments.
The first fragment defines the polynomial on the time domain [0, 2.2) as:
<div class="code2">c + b*(t-t0) + a*(t-t0)^2 = 3 + 1.2*t - 0.4*t^2</div>
The second fragment defines the polynomial on the time domain [2.2, 2.5) as:
<div class="code2">c + b*(t-t0) + a*(t-t0)^2 = 4 - 0.3*(t-2.2) + 5*(t - 2.2)^2</div>

The time domains of the fragments of a signal as they appear in the file must be
consecutive, i.e., no overlap between fragment domains and no gaps between fragment domains.
This means that the boundary values must be equal.

</div>
</html>