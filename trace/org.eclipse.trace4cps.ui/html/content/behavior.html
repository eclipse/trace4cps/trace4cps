<!--

    Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation

    This program and the accompanying materials are made
    available under the terms of the Eclipse Public License 2.0
    which is available at https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 5.0 Transitional//EN">

<html>
<head>
    <link rel="stylesheet" type="text/css" href="../css/style.css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title>TRACE - Behavioral analysis</title>
</head>
<div>
<h1>TRACE Behavioral analysis</h1>

<p>
<b>NOTE: Analysis is applied to the current filtered view</b>
</p>

<p>
Many systems exhibit more or less repetitive behavior, e.g., video-processing pipelines and manufacturing machines
such as professional printers and wafer scanners.
Execution traces of such systems hence also are more or less repetitive.
The <a href="example.html">running example</a> of a video-processing system, for instance, gives highly
repetitive traces, e.g., the one in Figure 1 which shows the processing of 11 frames.
Disturbances in the repetitive
patterns in an execution trace can hint at (performance) problems.
The behavioral analysis that TRACE supports aims to find such disturbances.
</p>

<!-- da-more-mem-2-cores.etf -->
<figure>
<a target="_blank" href="../img/beh-1.png"><img src="../img/beh-1.png" class="open600" /> </a>
<figcaption>Figure 1: A trace in which 11 images are processed.</figcaption>
</figure>

<p>
The behavioral analysis works on claims.
It assumed that the trace comes from a system that processes identifiable <i>objects</i>.
The objects, for example, can be frames, sheets of paper or wafers, each with a unique identifier.
The analysis thus requires that each claim has an attribute with an integer value that is the
identifier of the object that is being processed.
This is the <i>object identifier</i> attribute.
In the trace of Figure 1, each claim has an attribute "id" with an integer value. The coloring is
according to this "id" attribute, and "id" serves as the object identifier attribute.
</p>
<p>
The analysis first creates a partition of the claims according to the object identifier attribute: claims
with the same value for the object identifier attribute are placed in the same cell of the partition.
Such a cell is called a <i>behavior</i>.
In Figure 1, all claims with identical color are a behavior (because the coloring is according to the
object identifier attribute).
</p>
<p>
The analysis then compares the behaviors in a trace with each other.
This is done with the pseudo metric (distance) explained <a href="dist.html">here</a>.
To compare a behavior B<sub>1</sub> with another behavior B<sub>2</sub>, the claims of B<sub>1</sub> are used 
to create a trace T<sub>1</sub> and the claims of B<sub>2</sub> are used 
to create a trace T<sub>2</sub>, which are input to the distance computation.
The distance computation uses as a basis for equality of vertices in T<sub>1</sub> and T<sub>2</sub>
(i) a subset of the attributes (<i>uniqueness attributes</i>), and (ii) the resource that a claim uses.
These <i>uniqueness attributes</i> are a second parameter of the analysis, next to the object identifier.
In our running example, see Figure 1, we could use the "name" attribute as the uniqueness attribute.
This, however, is not necessary as each claim in a behavior uses a unique resource.
</p>
<p>
The pseudo metric induces an equivalence relation on behaviors.
Two behaviors are equivalent if and only if their distance equals 0.
This informally means that the order of the processing steps that are modeled by the claims
in the two behaviors are equal.
This equivalence relation is used to create the <i>behavioral partition</i>.
For instance, when we compare the first (cyan) behavior with the second (red) behavior in Figure 1,
we get distance 0. These two behaviors are thus part of the same cell in the behavioral partition.
</p>
<p>
The behavioral analysis can be started with the menu (<img src="../../icons/anomaly.png" />)
and has three items:
<ul>
<li> <i>Behavioral classes</i> : This analysis gives the behaviors in each cell of the behavioral partition
a unique color.
An example is shown in Figure 2. Apparently, the trace in Figure 1 containts three kinds of behaviors.
Note that the difference is whether activity with name F is present or not, and if it is present,
whether it overlaps with the activity with name E.
</li>  
<li> <i>Behavioral representatives</i> : This analysis adds a filter to show only  a single (random)
behavior from each cell in the behavioral partition. Figure 3, for instance, shows
three representatives of the different behaviors that occur in the trace.
</li>  
<li> <i>Behavioral histogram</i> : This analysis  shows a histogram of the sizes of the various
cells in the behavioral partition. The histogram is opened in a separate view, see Figure 4. 
</li>  
</ul>
</p>

<figure>
<a target="_blank" href="../img/beh-2.png"><img src="../img/beh-2.png" class="open600" /> </a>
<figcaption>Figure 2: Highlight of the three behavioral classes.</figcaption>
</figure>

<figure>
<a target="_blank" href="../img/beh-3.png"><img src="../img/beh-3.png" class="open600" /> </a>
<figcaption>Figure 3: Filtering to show only on representative per behavioral class.</figcaption>
</figure>

<figure>
<a target="_blank" href="../img/beh-4.png"><img src="../img/beh-4.png" class="open" /> </a>
<figcaption>Figure 4: A histogram that shows the sizes of the behavioral classes.</figcaption>
</figure>

</div>
</html>