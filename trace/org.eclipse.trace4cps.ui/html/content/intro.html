<!--

    Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation

    This program and the accompanying materials are made
    available under the terms of the Eclipse Public License 2.0
    which is available at https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 5.0 Transitional//EN">

<html>
<head>
    <link rel="stylesheet" type="text/css" href="../css/style.css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title>TRACE - Introduction</title>
</head>
<div>
<h1>TRACE Introduction</h1>

<h2>Understanding system performance</h2>
There are many reasons why a system�s (time-based) performance can become difficult to understand, or
worse, confusing. This is often due to the cross-cutting nature of system performance.
An example is a situation in
which many concurrent activities share resources. Unforeseen interactions may arise due to the
specific timing of the activities. Moreover, if the timing of the activities changes (e.g. due
to an upgrade to the computational platform), the interactions may also change, which could
result in significantly different behavior. However, insight into the hows and whys of a
system�s behavior over time is of paramount importance for making effective (design) choices
and trade-offs in all phases of the system lifecycle, from the design of a new system to the
maintenance of an old legacy system.
The TRACE tool can help with this.

<h2>Execution traces to capture behavior over time</h2>
The TRACE tool works with <i>execution traces</i>.
An execution trace is a model of system behavior over time.
TRACE supports several concepts that can be used within an execution trace:
<ul>
<li>time-stamped <i>events</i>,</li>
<li><i>claims</i> of a resource by an activity (with start and end time stamps),</li>
<li>sampled or continuous <i>signals</i>, and</li>
<li><i>dependencies</i> between events or claims.</li> 
</ul>
Each element of an execution trace (event, claim, signal or dependency) has a number of user-defined
attributes (e.g. the name of the activity) which typically are tailored to a specific problem domain.
This concept of an execution trace is very generic, which makes TRACE widely applicable:
<ul>
<li>All levels of abstraction: the TRACE format can capture all levels of abstraction, from
low-level embedded activities to system-level activities.</li>
<li>Domain-independent: the TRACE format is domain-independent, but nevertheless has the means to
be tailored to a domain via user-defined attributes.</li>
<li>Source-independent: TRACE input can be created from any source, e.g. from the log files
of legacy systems or from a discrete-event simulation model.</li>
</ul>
The TRACE tool is, however, meant for performance engineering of cyber-physical systems. This
is most clearly reflected in the ideas that underlie the claim concept, and in the analysis
methods that are provided by the TRACE tool. 
</p>

<h2>TRACE methodology</h2>

<figure>
<a target="_blank" href="../img/method.png"><img class="open" src="../img/method.png" /></a>
<figcaption>Figure 1: TRACE method.</figcaption>
</figure>

<p>
The TRACE methodology for performance engineering of cyber-physical systems consists of three parts:
the method, techniques and formalisms.
Figure 1 shows the method. First, an execution trace is acquired, for instance, from the
logging data of a component, prototype or deployed system.
Second, the execution trace is assessed using the TRACE techniques. Third, action is
undertaken based on the outcome of the assessment. For instance, a performance
bottleneck has been identified and can now be addressed. 
Because of TRACE's source independence,
the TRACE methodology can be applied during every lifecycle phase of a system. During design (left-hand
side of the well-known V model of system development), for instance, a discrete-event simulation
model can produce execution traces. On the right-hand side of the V, the TRACE tool can be
used to analyze the performance of prototypes; the execution trace then is extracted from the
logging. TRACE can also be applied to deployed systems by creating execution traces from the
system's logging. This can be useful in the contexts of online optimization and digital twinning.
</p>

<figure>
<a target="_blank" href="../img/workflow.png"><img class="open" src="../img/workflow.png" /></a>
<figcaption>Figure 2: TRACE workflow.</figcaption>
</figure>

<p>
The techniques that TRACE offers fall in two categories: <i>visualization</i> and <i>analysis</i>:
see Figure 2.
Visualization is based on a Gantt-chart view of the claims, extended with visual elements for
the events, signals and dependencies. The TRACE tool offers functionality to pan and zoom.
Furthermore, coloring, filtering and grouping of elements can be done based on the user-defined
attributes. This is explained in more detail <a href="vis.html">here</a>.
TRACE offers several analysis methods, all tailored to performance engineering of cyber-physical
systems. These are:
<a href="resource.html">resource-usage analysis</a>,
<a href="behavior.html">behavioral analysis</a>,
<a href="ll.html">Little's-Law-based analysis</a>,
<a href="dist.html">distance analysis</a>,
<a href="cpa.html">critical-path analysis</a>, and
<a href="tl.html">runtime verification</a>.
</p>
<p>
Application of the TRACE method and techniques starts with a formalism to define execution traces.
A simple human-readable text-based format is supported by default (see <a href="format.html">here</a>). 
Furthermore, TRACE provides a language to formally specify temporal-logic-based properties of
execution traces for runtime verification (see <a href="tl.html">here</a>).
All the TRACE aspects that are mentioned above are explained in more detail in the following
sections.
</p>

</div>
</html>