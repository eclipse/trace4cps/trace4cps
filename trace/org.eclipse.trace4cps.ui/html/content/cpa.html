<!--

    Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation

    This program and the accompanying materials are made
    available under the terms of the Eclipse Public License 2.0
    which is available at https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

-->
<!DOCTYPE HTML PUBLC "-//W3C//DTD HTML 5.0 Transitional//EN">

<html>
<head>
    <link rel="stylesheet" type="text/css" href="../css/style.css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title>TRACE - Critical-path analysis</title>
</head>
<div>
<h1>TRACE Critical-path analysis</h1>

<p>
<b>NOTE: Analysis is applied to the current filtered view</b>
</p>

<p>
Critical path analysis (CPA) is a technique to find <i>bottleneck</i> activities and resources.
The technique implemented in the TRACE tool constructs a <i>constraint graph</i> from the claims in the trace.
A constraint graph is a weighted directed graph in which the weights on the edges encode timing
constraints. For instance, if we have two claims, <i>c<sub>1</sub></i> and <i>c<sub>2</sub></i>,
and an edge from the end of <i>c<sub>1</sub></i> to the start of <i>c<sub>2</sub></i> with weight <i>w</i>,
then this
means that <i>c<sub>2</sub></i> always starts at least <i>w</i> time units after <i>c<sub>1</sub></i> ends.
An edge from the start of <i>c<sub>2</sub></i> to the end of <i>c<sub>1</sub></i> with weight <i>-w</i>
means that <i>c<sub>2</sub></i> always starts at most <i>w</i> time units after <i>c<sub>1</sub></i> ends.
These weights can thus be used to specify lower and upper bounds on the time duration between the start
and end of claims.
</p>
<p>
Note that there may be cycles in the graph, but these must have a negative weight. A cycle with a positive
weight indicates inconsistent constraints.
Critical-path analysis computes the longest paths through the constraint graph.
The claims on the longest paths are <i>critical</i> and their improvement
(execution time) can improve the performance of the system.
</p>

<p>
Critical-path analysis is available when a single trace is viewed through the
<img src="../../icons/cpa.png" /> menu. The parameters for the analysis are the following:
<ul>
<li>
    <i>Use of specified dependencies</i> :
    <ul>
    <li>
    The <i>Full</i> option means that the edges in the constraint graph come from the dependencies of the trace,
    using weight 0, which means that the a dependency does not give a quantitative constraint, but rather an
    order constraint. 
    Two constraints also are added between the start and end of each claim that expresses that the claim takes exactly
    the specified time.
    </li>
    <li>
    The <i>None</i> options means that no dependencies from the trace are used for constraints. Instead the
    <i>&epsilon; heuristic</i> is used to create constraints. This heuristic adds a constraint with weight 0
    between the end of a claim <i>c<sub>1</sub></i> and the start of another claim <i>c<sub>2</sub></i>
    if and only if <i>c<sub>2</sub></i> starts within &epsilon; time units after <i>c<sub>1</sub></i> ends.
    Furthermore, the intra-claim dependencies as described above are added.
    The &epsilon; parameter is defined by the user (see below).
    </li>
    <li>
    Last, the <i>As application dependencies</i> option means that the &epsilon; heuristic is used again to create
    the constraint graph.
    If a critical edge in the constraint graph does not conform to a dependency, then this indicates that
    blocking on non-application aspects occurs. An example is an activity that must wait for resources to become available.
    </li>
    </ul> 
</li>
<li>
    <i>Use default &epsilon;</i> :
    The default allowable gap between claims in order to relate them in the graph representation equals 0.
    This usually is a proper value for traces that have been produced by simulation models, as the timing
    in such models is <i>perfect</i> in some sense. Traces that have been produced by real systems typically
    have timing overhead between claims. The &epsilon; parameter should be as least as large as the maximum gap.
    Of course, this is very difficult to determine.
</li>
</ul>
</p>
<p>
Consider the trace shown in Figure 1, which has claims and dependencies between claims.
It is a view on the the trace "model-bf.etf" from the <a href="example.html">running example</a>.
We have filtered out all but the memory resources M1 and M2, and also restricted the id attribute to
values 0 to 5.
Note that the dependencies are the data dependencies between processing tasks of the ODSE system.
During the execution of these tasks (which resulted in this trace), however,
there are probably more dependencies because of limited memory.
</p>

<figure>
<a target="_blank" href="../img/trace-1.png"><img src="../img/trace-1.png" class="open600" /> </a>
<figcaption>Figure 1: A trace in the standard activity view.</figcaption>
</figure>

<p>
Figure 2 shows the activity view after CPA with <i>Full</i> usage of the dependencies.
The critical tasks are marked with a red color, and the critical dependencies are added
to the trace. A filter is applied to only show these critical dependencies. 
Note that the critical tasks are consecutive except for the those at the top in in the
middle of the figure. This gap indicates that this is not the true critical path as the red task
in the second section (named B) is apparantly waiting for something else than the end of the
red task in first section (named A). 
</p>

<figure>
<a target="_blank" href="../img/trace-1-cpa-full.png"><img src="../img/trace-1-cpa-full.png" class="open600" /> </a>
<figcaption>Figure 2: The activity view after CPA with dependency-usage option <i>Full</i>.</figcaption>
</figure>

<p>
Figure 3 shows the activity view after CPA with <i>None</i> usage of the dependencies.
The critical tasks are marked with a dull red color, and the critical dependencies are added
to the trace. Again, a filter is applied to only show these critical dependencies. 
Note that the critical tasks are consecutive now due to the &epsilon; heuristic
with &epsilon; = 0.
The critical path shows that the B task is waiting for a G task to finish (G releases
memory which B needs).
</p>

<figure>
<a target="_blank" href="../img/trace-1-cpa-none.png"><img src="../img/trace-1-cpa-none.png" class="open600" /> </a>
<figcaption>Figure 3: The resource view after CPA with dependency-usage option <i>None</i>.</figcaption>
</figure>

<p>
Finally, figure 4 shows the activity view after CPA with use of the dependencies <i>As application dependencies</i>.
The critical tasks are marked with a dull red color, and the critical dependencies are added
to the trace. Again, a filter is applied to only show these critical dependencies. 
Note that the critical task B that was blocked because it had to wait for memory to be released by G
now is marked bright red. This indicates that task B is not blocked on an application dependency,
which can indicate resource contention.
</p>

<figure>
<a target="_blank" href="../img/trace-1-cpa-app.png"><img src="../img/trace-1-cpa-app.png" class="open600" /> </a>
<figcaption>Figure 4: The resource view after CPA with dependency-usage option <i>As application dependencies</i>.</figcaption>
</figure>

</div>
</html>