/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui;

import org.eclipse.debug.ui.IDebugUIConstants;
import org.eclipse.trace4cps.ui.view.ChartView;
import org.eclipse.trace4cps.ui.view.timing.TimingAnalysisView;
import org.eclipse.trace4cps.ui.view.verify.VerificationResultView;
import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class TracePerspectiveFactory implements IPerspectiveFactory {
    public static final String PERSPECTIVE_ID = "org.eclipse.trace4cps.ui.perspective";

    /**
     * {@inheritDoc}
     */
    @Override
    public void createInitialLayout(IPageLayout layout) {
        defineActions(layout);
        defineLayout(layout);
    }

    private void defineActions(IPageLayout layout) {
        layout.addShowViewShortcut(IPageLayout.ID_PROJECT_EXPLORER);
        layout.addShowViewShortcut(IPageLayout.ID_PROP_SHEET);
        layout.addShowViewShortcut(IPageLayout.ID_OUTLINE);
        layout.addShowViewShortcut(VerificationResultView.VIEW_ID);

        layout.addActionSet(IDebugUIConstants.LAUNCH_ACTION_SET);
    }

    private void defineLayout(IPageLayout layout) {
        String editorArea = layout.getEditorArea();
        layout.setEditorAreaVisible(true);

        IFolderLayout left = layout.createFolder("left", IPageLayout.LEFT, (float)0.15, editorArea);
        left.addView(IPageLayout.ID_PROJECT_EXPLORER);

        IFolderLayout right = layout.createFolder("right", IPageLayout.RIGHT, (float)0.85, editorArea);
        right.addView(VerificationResultView.VIEW_ID);
        right.addView(TimingAnalysisView.VIEW_ID);

        IFolderLayout leftBottom = layout.createFolder("leftBottom", IPageLayout.BOTTOM, (float)0.7, "left");
        leftBottom.addView(IPageLayout.ID_OUTLINE);

        IFolderLayout bottom = layout.createFolder("bottom", IPageLayout.BOTTOM, 0.7f, editorArea);
        bottom.addView(IPageLayout.ID_PROP_SHEET);
        bottom.addView("org.eclipse.ui.console.ConsoleView");

        IFolderLayout rightBottom = layout.createFolder("rightBottom", IPageLayout.RIGHT, (float)0.5, "bottom");
        rightBottom.addPlaceholder(ChartView.VIEW_ID + ":*");
    }
}
