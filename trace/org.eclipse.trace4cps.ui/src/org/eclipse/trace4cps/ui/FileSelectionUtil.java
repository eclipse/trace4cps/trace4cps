/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.model.WorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.eclipse.ui.views.navigator.ResourceComparator;

public class FileSelectionUtil {
    private FileSelectionUtil() {
    }

    public static String browseForFileInWorkspace(Shell parent, String[] ext) {
        List<String> r = browseForFilesInWorkspace(parent, ext, false);
        if (r == null || r.isEmpty()) {
            return null;
        }
        return r.get(0);
    }

    public static List<String> browseForFilesInWorkspace(Shell parent, String[] ext) {
        return browseForFilesInWorkspace(parent, ext, true);
    }

    private static List<String> browseForFilesInWorkspace(Shell parent, String[] ext, boolean allowMultiple) {
        ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(parent, new WorkbenchLabelProvider(),
                new WorkbenchContentProvider());
        dialog.setTitle("Select file");
        dialog.setInput(ResourcesPlugin.getWorkspace().getRoot());
        dialog.setComparator(new ResourceComparator(ResourceComparator.NAME));
        dialog.setAllowMultiple(allowMultiple);
        if (ext != null && ext.length > 0) {
            dialog.addFilter(new ViewerFilter() {
                @Override
                public boolean select(Viewer viewer, Object parent, Object element) {
                    if (element instanceof IFile) {
                        IFile f = (IFile)element;
                        for (String extension: ext) {
                            if (f.getName().endsWith(extension)) {
                                return true;
                            }
                        }
                        return false; // IFile, but with inappropriate extension
                    }
                    return true;
                }
            });
        }
        if (dialog.open() == IDialogConstants.OK_ID) {
            List<String> result = new ArrayList<>();
            for (Object r: dialog.getResult()) {
                IResource resource = (IResource)r;
                result.add(resource.getLocation().toString());
            }
            return result;
        }
        return null;
    }

    public static String browseForFile(Shell parent, String[] ext) {
        File workspaceDir = new File(ResourcesPlugin.getWorkspace().getRoot().getLocation().toString());
        FileDialog fd = new FileDialog(parent, SWT.OPEN);
        fd.setText("Browse");
        if (ext != null && ext.length > 0) {
            String[] filter = new String[ext.length];
            for (int i = 0; i < ext.length; i++) {
                filter[i] = "*" + ext[i];
            }
            fd.setFilterExtensions(filter);
        }
        fd.setFilterPath(workspaceDir.getAbsolutePath());
        return fd.open();
    }

    public static String browseForDir(Shell parent) {
        File workspaceDir = new File(ResourcesPlugin.getWorkspace().getRoot().getLocation().toString());
        DirectoryDialog fd = new DirectoryDialog(parent, SWT.OPEN);
        fd.setText("Browse");
        fd.setFilterPath(workspaceDir.getAbsolutePath());
        return fd.open();
    }
}
