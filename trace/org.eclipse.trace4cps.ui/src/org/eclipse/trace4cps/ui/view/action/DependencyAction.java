/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.trace4cps.analysis.dist.DependencyInclusionCheck;
import org.eclipse.trace4cps.core.IDependency;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.core.impl.TraceHelper;
import org.eclipse.trace4cps.ui.ConsoleUtil;
import org.eclipse.trace4cps.ui.dialog.DependencyAnalysisWizard;
import org.eclipse.trace4cps.ui.view.TraceView;

public class DependencyAction extends AbstractTraceViewAction {
    public DependencyAction(TraceView view) {
        super(view);
        setText("Dependency analysis");
    }

    @Override
    public boolean isEnabled() {
        return view.getNumTraces() == 2;
    }

    @Override
    protected void doRun() throws TraceException {
        DependencyAnalysisWizard wizard = new DependencyAnalysisWizard(view);
        if (new WizardDialog(view.getEditorSite().getShell(), wizard).open() == Dialog.CANCEL) {
            return;
        }
        int modelTraceIdx = wizard.getModelTraceIdx();
        int systemTraceIdx = wizard.getSystemTraceIdx();
        ITrace modelTrace = view.getTrace(modelTraceIdx);
        ITrace systemTrace = view.getTrace(systemTraceIdx);
        String weightAttribute = wizard.getWeightAttribute();
        List<IDependency> deps = DependencyInclusionCheck.check(modelTrace, systemTrace, weightAttribute);

        ConsoleUtil.log("Unsatisfied dependencies of " + TraceHelper.getValues(modelTrace, false) + " in "
                + TraceHelper.getValues(systemTrace, false) + " : " + deps.size());

        for (IDependency d: deps) {
            d.setAttribute("analysis_type", getClass().getName());
        }
        view.addDependencyExtension(deps, systemTraceIdx);
        view.update();
    }
}
