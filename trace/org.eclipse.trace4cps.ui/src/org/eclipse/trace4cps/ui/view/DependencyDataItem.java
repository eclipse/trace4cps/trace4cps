/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view;

import org.eclipse.trace4cps.common.jfreechart.data.xy.XYEdgeDataItem;
import org.eclipse.trace4cps.common.jfreechart.data.xy.XYEdgeSeriesCollection;
import org.eclipse.trace4cps.common.jfreechart.ui.viewers.BackReferenceProvider;
import org.eclipse.trace4cps.core.IDependency;
import org.jfree.data.xy.XYDataset;

public class DependencyDataItem extends XYEdgeDataItem implements BackReferenceProvider<EclipseSelectionWrapper> {
    private static final long serialVersionUID = 1L;

    private final EclipseSelectionWrapper dep;

    public DependencyDataItem(IDependency dep, Number x0, Number y0, Number x1, Number y1) {
        super(x0, y0, x1, y1);
        this.dep = new EclipseSelectionWrapper(dep);
    }

    @Override
    public EclipseSelectionWrapper getBackReference() {
        return dep;
    }

    public static DependencyDataItem getFrom(XYDataset ds, int series, int item) {
        if (ds instanceof XYEdgeSeriesCollection) {
            XYEdgeSeriesCollection edgesDs = (XYEdgeSeriesCollection)ds;
            XYEdgeDataItem di = edgesDs.getSeries(series).getDataItem(item);
            if (di instanceof DependencyDataItem) {
                return (DependencyDataItem)di;
            }
        }
        return null;
    }
}
