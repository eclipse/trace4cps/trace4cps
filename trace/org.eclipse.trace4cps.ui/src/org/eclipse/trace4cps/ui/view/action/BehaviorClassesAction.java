/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import java.awt.Paint;
import java.util.Map;
import java.util.Set;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.trace4cps.core.IAttributeAware;
import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.ui.AnalysisUtil;
import org.eclipse.trace4cps.ui.dialog.BehaviorWizard;
import org.eclipse.trace4cps.ui.view.TraceView;

public class BehaviorClassesAction extends AbstractTraceViewAction {
    public BehaviorClassesAction(TraceView view) {
        super(view);
        setText("Behavioral classes");
    }

    @Override
    public boolean isEnabled() {
        return view.getNumTraces() == 1 && view.hasClaims(true);
    }

    @Override
    protected void doRun() throws TraceException {
        BehaviorWizard wizard = new BehaviorWizard(view);
        if (new WizardDialog(view.getEditorSite().getShell(), wizard).open() == Dialog.CANCEL) {
            return;
        }
        String idAtt = wizard.getIdAttribute();
        Set<String> uniqueness = wizard.getUniqueness();

        Map<IAttributeAware, Paint> hl = AnalysisUtil.computeAnomalyHistogramHighlight(view.getTrace(), idAtt,
                uniqueness);
        viewCfg.setHighlightMap(hl);
        view.update();
    }
}
