/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.ui.view.TraceView;

public class ClearFilterAction extends AbstractTraceViewAction {
    private final TracePart part;

    public ClearFilterAction(TraceView view, TracePart part) {
        super(view);
        setText("Clear " + part.toString().toLowerCase() + " filter");
        this.part = part;
    }

    @Override
    public boolean isEnabled() {
        if (part == TracePart.ALL) {
            return viewCfg.hasFilters();
        }
        return !viewCfg.getFilters(part).isEmpty();
    }

    @Override
    protected void doRun() throws TraceException {
        viewCfg.clearFilters(part);
        view.update();
    }
}
