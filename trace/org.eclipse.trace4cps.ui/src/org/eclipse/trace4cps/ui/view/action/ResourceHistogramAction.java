/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.ui.AnalysisUtil;
import org.eclipse.trace4cps.ui.dialog.ResourceClientHistogramDialog;
import org.eclipse.trace4cps.ui.view.ChartView;
import org.eclipse.trace4cps.ui.view.TraceView;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;

public class ResourceHistogramAction extends AbstractTraceViewAction {
    public ResourceHistogramAction(TraceView view) {
        super(view);
        setText("Resource-usage histogram");
    }

    @Override
    public boolean isEnabled() {
        return view.getNumTraces() == 1 && view.hasClaims(true);
    }

    @Override
    protected void doRun() throws TraceException {
        ResourceClientHistogramDialog dialog = new ResourceClientHistogramDialog(view.getEditorSite().getShell(),
                view.getTrace().getResources(), true);
        if (dialog.open() == Dialog.CANCEL) {
            return;
        }
        JFreeChart chart = AnalysisUtil.createResourceUsageHistogramPlot(view.getTrace(), dialog.getSelected(),
                viewCfg.getDescribingAttributes(TracePart.RESOURCE), dialog.cumulative());
        ChartFactory.getChartTheme().apply(chart);
        ChartView.showChart(chart, "Resource-usage histogram", "Resource-usage histogram");
    }
}
