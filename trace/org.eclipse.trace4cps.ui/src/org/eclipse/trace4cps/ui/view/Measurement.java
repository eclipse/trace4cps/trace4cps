/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.concurrent.TimeUnit;

import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertySource;
import org.eclipse.ui.views.properties.PropertyDescriptor;

public class Measurement implements Serializable, IPropertySource {
    private enum Properties {
        Duration, Productivity
    }

    private static final long serialVersionUID = -8418765797403500502L;

    private static final DecimalFormat FORMAT = new DecimalFormat("0.###");

    private final double duration;

    private final TimeUnit timeUnit;

    private transient IPropertyDescriptor[] propertyDescriptors;

    public Measurement(double duration, TimeUnit timeUnit) {
        this.duration = duration;
        this.timeUnit = timeUnit;
    }

    /**
     * @return The time.
     */
    public double getDuration() {
        return duration;
    }

    /**
     * @return The timeUnit.
     */
    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    @Override
    public Object getEditableValue() {
        return this;
    }

    @Override
    public IPropertyDescriptor[] getPropertyDescriptors() {
        if (null == propertyDescriptors) {
            PropertyDescriptor durationProperty = new PropertyDescriptor(Properties.Duration,
                    Properties.Duration.name());
            PropertyDescriptor productivityProperty = new PropertyDescriptor(Properties.Productivity,
                    Properties.Productivity.name());
            propertyDescriptors = new IPropertyDescriptor[] {durationProperty, productivityProperty};
        }
        return propertyDescriptors;
    }

    @Override
    public Object getPropertyValue(Object id) {
        switch ((Measurement.Properties)id) {
            case Duration:
                return toString();
            case Productivity:
                return toProductivityString();
        }
        return null;
    }

    @Override
    public boolean isPropertySet(Object id) {
        return true;
    }

    @Override
    public void resetPropertyValue(Object id) {
        // Empty
    }

    @Override
    public void setPropertyValue(Object id, Object value) {
        // Empty
    }

    public Measurement scale() {
        if (duration < 1) {
            for (int index = timeUnit.ordinal() - 1; index >= 0; index--) {
                TimeUnit scaledUnit = TimeUnit.values()[index];
                double scaledDuration = scaleTime(scaledUnit);
                if (scaledDuration >= 1) {
                    return new Measurement(scaledDuration, scaledUnit);
                }
            }
        } else {
            for (int index = timeUnit.ordinal() + 1; index < TimeUnit.values().length; index++) {
                if (scaleTime(TimeUnit.values()[index]) < 1) {
                    return scale(TimeUnit.values()[index - 1]);
                }
            }
        }
        return this;
    }

    public Measurement scale(TimeUnit scaledUnit) {
        if (scaledUnit == timeUnit) {
            return this;
        } else {
            return new Measurement(scaleTime(scaledUnit), scaledUnit);
        }
    }

    private double scaleTime(TimeUnit scaledUnit) {
        if (scaledUnit == timeUnit) {
            return duration;
        } else if (scaledUnit.compareTo(timeUnit) > 0) {
            return duration / timeUnit.convert(1, scaledUnit);
        } else {
            return duration * scaledUnit.convert(1, timeUnit);
        }
    }

    public String toProductivityString() {
        TimeUnit productivityTimeUnit;
        switch (timeUnit) {
            case NANOSECONDS:
            case MICROSECONDS:
                productivityTimeUnit = TimeUnit.SECONDS;
                break;
            case MILLISECONDS:
                productivityTimeUnit = TimeUnit.MINUTES;
                break;
            case SECONDS:
                productivityTimeUnit = TimeUnit.HOURS;
                break;
            default:
                productivityTimeUnit = TimeUnit.DAYS;
                break;
        }
        double productivity = timeUnit.convert(1, productivityTimeUnit) / duration;
        return FORMAT.format(productivity) + " per " + asLongString(productivityTimeUnit);
    }

    @Override
    public String toString() {
        return FORMAT.format(duration) + " " + asShortString(timeUnit);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(duration);
        result = prime * result + (int)(temp ^ (temp >>> 32));
        result = prime * result + ((timeUnit == null) ? 0 : timeUnit.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Measurement other = (Measurement)obj;
        if (Double.doubleToLongBits(duration) != Double.doubleToLongBits(other.duration)) {
            return false;
        }
        if (timeUnit != other.timeUnit) {
            return false;
        }
        return true;
    }

    protected String asShortString(TimeUnit timeUnit) {
        switch (timeUnit) {
            case NANOSECONDS:
                return "ns";
            case MICROSECONDS:
                return "us";
            case MILLISECONDS:
                return "ms";
            case SECONDS:
                return "s";
            case MINUTES:
                return "min";
            case HOURS:
                return "hr";
            case DAYS:
                return "day";
            default:
                throw new IllegalStateException();
        }
    }

    protected String asLongString(TimeUnit timeUnit) {
        switch (timeUnit) {
            case NANOSECONDS:
                return "nanosecond";
            case MICROSECONDS:
                return "microsecond";
            case MILLISECONDS:
                return "millisecond";
            case SECONDS:
                return "second";
            case MINUTES:
                return "minute";
            case HOURS:
                return "hour";
            case DAYS:
                return "day";
            default:
                throw new IllegalStateException();
        }
    }
}
