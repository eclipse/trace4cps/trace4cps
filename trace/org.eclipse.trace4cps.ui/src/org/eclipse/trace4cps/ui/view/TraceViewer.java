/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view;

import java.awt.event.MouseEvent;

import javax.swing.SwingUtilities;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.trace4cps.common.jfreechart.chart.axis.SectionAxis;
import org.eclipse.trace4cps.common.jfreechart.ui.viewers.ChartPanelContentViewer;
import org.eclipse.trace4cps.vis.jfree.TracePlotManager;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.geom.Point2DNumber;
import org.jfree.chart.plot.XYMeasureWithAnnotations;
import org.jfree.chart.plot.XYMeasureWithAnnotations.MeasurementAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.Range;
import org.jfree.data.RangeAlign;

/**
 * This is a wrapper around the {@link TracePlotManager} that embeds it in the Eclipse UI.
 */
public class TraceViewer extends ChartPanelContentViewer {
    private final TracePlotManager plotMgr;

    public TraceViewer(Composite parent) {
        super(parent);
        setPreserveSelection(true);
        plotMgr = new TracePlotManager();
        plotMgr.setDataItemFactory(new EclipseDataItemFactory());
        plotMgr.setDelegateToolTipGenerator(new EclipseToolTipGenerator(plotMgr::getViewConfig));
    }

    public TracePlotManager getPlotManager() {
        return plotMgr;
    }

    @Override
    protected void configureChartPanel() {
        super.configureChartPanel();

        // Measuring
        XYMeasureWithAnnotations measurer = new XYMeasureWithAnnotations(MeasurementAxis.DOMAIN) {
            private static final long serialVersionUID = 5406861194841750122L;

            @Override
            protected void addAnnotation(XYPlot plot, Point2DNumber first, Point2DNumber second, MouseEvent event) {
                Measurement measurement = new Measurement(Math.abs(second.getX() - first.getX()),
                        plotMgr.getTraces().get(0).getTimeUnit());
                plot.addAnnotation(new MeasurementAnnotation(measurement.scale(),
                        SwingUtilities.isMiddleMouseButton(event), first, second));
            }
        };
        measurer.addToChartPanel(getChartPanel());

        XYPlot xyPlot = new XYPlot();
        NumberAxis domainAxis = new NumberAxis();
        domainAxis.setAutoRangeIncludesZero(false);
        domainAxis.setAutoRangeAlign(RangeAlign.LOWER);
        // Set a maximum zoom level to prevent display issues
        domainAxis.setRangeMinimumSize(10e-6);
        xyPlot.setDomainAxis(domainAxis);

        SectionAxis rangeAxis = new SectionAxis();
        rangeAxis.setRangeMinimumSize(1);
        xyPlot.setRangeAxis(rangeAxis);

        // Setup the datasets and coloring etc of the plot:
        plotMgr.initializePlot(xyPlot);

        JFreeChart chart = new JFreeChart(null, null, xyPlot, false);
        ChartFactory.getChartTheme().apply(chart);
        getChartPanel().setChart(chart);
    }

    @Override
    protected void refreshChart() {
        plotMgr.update(getXYPlot());
    }

    public Range getRange() {
        return getXYPlot().getDomainAxis().getRange();
    }
}
