/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import java.awt.Paint;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.trace4cps.core.IAttributeAware;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.core.impl.TraceHelper;
import org.eclipse.trace4cps.ui.AnalysisUtil;
import org.eclipse.trace4cps.ui.ConsoleUtil;
import org.eclipse.trace4cps.ui.dialog.DistanceDialog;
import org.eclipse.trace4cps.ui.view.TraceView;

public class DistanceAction extends AbstractTraceViewAction {
    public DistanceAction(TraceView view) {
        super(view);
        setText("Order analysis");
    }

    @Override
    public boolean isEnabled() {
        return view.getNumTraces() > 1 && view.hasEvents(true, true);
    }

    @Override
    protected void doRun() throws TraceException {
        if (view.getNumTraces() == 2) {
            Map<IAttributeAware, Paint> highlight = new HashMap<>();
            computeDistance(highlight, view.getTrace(0), view.getTrace(1));
            viewCfg.setHighlightMap(highlight);
        } else {
            Map<String, Integer> traceKeyToIndex = new HashMap<>();
            for (int i = 0; i < view.getNumTraces(); i++) {
                ITrace trace = view.getTrace(i);
                traceKeyToIndex.put(TraceHelper.getValues(trace, false), i);
            }
            DistanceDialog dialog = new DistanceDialog(view.getEditorSite().getShell(), traceKeyToIndex);
            if (dialog.open() == IStatus.OK) {
                int refIdx = dialog.getReferenceIndex();
                ITrace[] modelTraces = new ITrace[view.getNumTraces() - 1];
                for (int i = 0, j = 0; i < view.getNumTraces(); i++) {
                    if (i != refIdx) {
                        modelTraces[j] = view.getTrace(i);
                        j++;
                    }
                }
                Map<IAttributeAware, Paint> highlight = new HashMap<>();
                ITrace reference = view.getTrace(refIdx);
                for (ITrace modelTrace: modelTraces) {
                    computeDistance(highlight, reference, modelTrace);
                }
                viewCfg.setHighlightMap(highlight);
            }
        }
        view.update();
    }

    private void computeDistance(Map<IAttributeAware, Paint> highlight, ITrace reference, ITrace modelTrace) {
        long dist = AnalysisUtil.computeDiffHighlights(reference, modelTrace, false, highlight);
        ConsoleUtil.log("Distance from " + TraceHelper.getValues(reference, false) + " to "
                + TraceHelper.getValues(modelTrace, false) + " : " + dist);
    }
}
