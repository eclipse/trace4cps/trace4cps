/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.trace4cps.core.IDependency;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.IPsopFragment;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.core.impl.TraceHelper;
import org.eclipse.trace4cps.core.io.ParseException;
import org.eclipse.trace4cps.core.io.TraceReader;
import org.eclipse.trace4cps.ui.TracePerspectiveFactory;
import org.eclipse.trace4cps.ui.action.OpenTraceView;
import org.eclipse.trace4cps.ui.view.action.BehaviorClassesAction;
import org.eclipse.trace4cps.ui.view.action.BehaviorHistogramAction;
import org.eclipse.trace4cps.ui.view.action.BehaviorRepresentativesAction;
import org.eclipse.trace4cps.ui.view.action.ClaimDescriptionToggleAction;
import org.eclipse.trace4cps.ui.view.action.ClearExtensionAction;
import org.eclipse.trace4cps.ui.view.action.ClearFilterAction;
import org.eclipse.trace4cps.ui.view.action.ColoringAction;
import org.eclipse.trace4cps.ui.view.action.CriticalPathAction;
import org.eclipse.trace4cps.ui.view.action.DependencyAction;
import org.eclipse.trace4cps.ui.view.action.DescribingAction;
import org.eclipse.trace4cps.ui.view.action.DistanceAction;
import org.eclipse.trace4cps.ui.view.action.DropDownAggregateAction;
import org.eclipse.trace4cps.ui.view.action.FilterAction;
import org.eclipse.trace4cps.ui.view.action.GroupingAction;
import org.eclipse.trace4cps.ui.view.action.HideAction;
import org.eclipse.trace4cps.ui.view.action.LatencyAction;
import org.eclipse.trace4cps.ui.view.action.ResourceHistogramAction;
import org.eclipse.trace4cps.ui.view.action.ResourceUsageAction;
import org.eclipse.trace4cps.ui.view.action.SaveViewConfigurationAction;
import org.eclipse.trace4cps.ui.view.action.SeparatorAction;
import org.eclipse.trace4cps.ui.view.action.ThroughputEventAction;
import org.eclipse.trace4cps.ui.view.action.ThroughputIdAction;
import org.eclipse.trace4cps.ui.view.action.TimingAnalysisAction;
import org.eclipse.trace4cps.ui.view.action.ToggleClaimScaleAction;
import org.eclipse.trace4cps.ui.view.action.ToggleSignalAction;
import org.eclipse.trace4cps.ui.view.action.ToggleViewAction;
import org.eclipse.trace4cps.ui.view.action.VerifyAction;
import org.eclipse.trace4cps.ui.view.action.WipAction;
import org.eclipse.trace4cps.vis.jfree.TraceViewConfiguration;
import org.eclipse.trace4cps.vis.jfree.TraceViewConfigurationIO;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.IPersistableElement;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.part.EditorPart;
import org.jfree.data.Range;

/**
 * A {@link TraceView} is either opened from the {@code OpenTraceView} action via the
 * {@link #showView(List, boolean, boolean)} method, or restored from a previous session through the
 * {@link #init(IViewSite, IMemento)} method.
 */
public class TraceView extends EditorPart {
    public static final String VIEW_ID = "org.eclipse.trace4cps.ui.TraceView";

    public static final String MEMENTO_FILE_KEY = "TRACE_FILE";

    private TraceViewer traceViewer;

    private ToolBarManager mgr;

    private List<File> files = new ArrayList<>();

    private List<ITrace> unfilteredTraces = new ArrayList<>();

    public TraceViewer getViewer() {
        return traceViewer;
    }

    public TraceViewConfiguration getViewConfiguration() {
        return traceViewer.getPlotManager().getViewConfig();
    }

    /**
     * @return whether any of the traces has claims
     */
    public boolean hasClaims(boolean filtered) {
        for (int i = 0; i < getNumTraces(); i++) {
            ITrace trace = getTrace(i, filtered);
            if (!trace.getClaims().isEmpty()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return whether any of the traces has non-claim events
     */
    public boolean hasEvents(boolean includeClaimEvents, boolean filtered) {
        for (int i = 0; i < getNumTraces(); i++) {
            ITrace trace = getTrace(i, filtered);
            if (includeClaimEvents) {
                if (!trace.getEvents().isEmpty()) {
                    return true;
                }
            } else {
                if (TraceHelper.hasNonClaimEvents(trace)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return whether any of the traces has dependencies
     */
    public boolean hasDependencies(boolean filtered) {
        for (int i = 0; i < getNumTraces(); i++) {
            ITrace trace = getTrace(i, filtered);
            if (!trace.getDependencies().isEmpty()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return whether any of the traces has signals or there are derived signals
     */
    public boolean hasSignals(boolean filtered) {
        for (int i = 0; i < getNumTraces(); i++) {
            ITrace trace = getTrace(i, filtered);
            if (!trace.getSignals().isEmpty()) {
                return true;
            }
        }
        return false;
    }

    public Range getDomainAxisRange() {
        return traceViewer.getRange();
    }

    public int getNumTraces() {
        return unfilteredTraces.size();
    }

    public ITrace getTrace() {
        return getTrace(0, true);
    }

    public File getTraceFile(int index) {
        return files.get(index);
    }

    public File getViewConfigFile(int index) {
        File traceFile = files.get(index);
        return new File(traceFile.getParentFile(), "." + traceFile.getName() + ".view");
    }

    public ITrace getTrace(int index) {
        return getTrace(index, true);
    }

    public ITrace getTrace(int index, boolean filtered) {
        if (filtered) {
            return traceViewer.getPlotManager().getTraces().get(index);
        }
        return unfilteredTraces.get(index);
    }

    public Set<String> getAttributeNames(TracePart part) {
        Set<String> result = new HashSet<>();
        for (ITrace trace: traceViewer.getPlotManager().getTraces()) {
            switch (part) {
                case ALL:
                    result.addAll(TraceHelper.getAttributeNames(trace));
                    break;
                case CLAIM:
                    result.addAll(TraceHelper.getAttributeNames(trace.getClaims()));
                    break;
                case RESOURCE:
                    result.addAll(TraceHelper.getAttributeNames(trace.getResources()));
                    break;
                case EVENT:
                    result.addAll(TraceHelper.getAttributeNames(trace.getEvents()));
                    break;
                case DEPENDENCY:
                    result.addAll(TraceHelper.getAttributeNames(trace.getDependencies()));
                    break;
                case SIGNAL:
                    result.addAll(TraceHelper.getAttributeNames(trace.getSignals()));
                    break;
                default:
                    throw new IllegalStateException();
            }
        }
        return result;
    }

    public Set<String> getAllAttributeValuesFor(String att, TracePart part) {
        Set<String> result = new HashSet<>();
        for (ITrace trace: traceViewer.getPlotManager().getTraces()) {
            switch (part) {
                case ALL:
                    result.addAll(TraceHelper.getAttributeValues(trace, att));
                    break;
                case CLAIM:
                    result.addAll(TraceHelper.getAttributeValues(trace.getClaims(), att));
                    break;
                case RESOURCE:
                    result.addAll(TraceHelper.getAttributeValues(trace.getResources(), att));
                    break;
                case EVENT:
                    result.addAll(TraceHelper.getAttributeValues(trace.getEvents(), att));
                    break;
                case DEPENDENCY:
                    result.addAll(TraceHelper.getAttributeValues(trace.getDependencies(), att));
                    break;
                case SIGNAL:
                    result.addAll(TraceHelper.getAttributeValues(trace.getSignals(), att));
                    break;
                default:
                    throw new IllegalStateException();
            }
        }
        return result;
    }

    public void addExtension(IPsop s, int traceIdx) {
        traceViewer.getPlotManager().getTraces().get(traceIdx).addSignal(s);
    }

    public void addDependencyExtension(List<IDependency> deps, int traceIdx) {
        traceViewer.getPlotManager().getTraces().get(traceIdx).addDependencies(deps);
    }

    public void addEventExtension(List<IEvent> events, int traceIdx) {
        traceViewer.getPlotManager().getTraces().get(traceIdx).addEvents(events);
    }

    public boolean hasExtension(TracePart part) {
        for (int i = 0; i < getNumTraces(); i++) {
            if (traceViewer.getPlotManager().getTraces().get(i).hasExtension(part)) {
                return true;
            }
        }
        return false;
    }

    public void clearExtensions(TracePart part) {
        for (int i = 0; i < getNumTraces(); i++) {
            traceViewer.getPlotManager().getTraces().get(i).clearExtension(part);
        }
    }

    @Override
    public void init(IEditorSite site, IEditorInput input) throws PartInitException {
        setSite(site);
        setInput(input);

        List<File> inputFiles = new ArrayList<>();
        if (input instanceof IFileEditorInput) {
            IFileEditorInput in = (IFileEditorInput)input;
            IPath location = in.getFile().getLocation();
            if (location != null) {
                inputFiles.add(location.toFile());
            } else {
                throw new PartInitException("Failed to get File for " + in.getFile());
            }
        } else if (input instanceof MultiFileEditorInput) {
            inputFiles.addAll(((MultiFileEditorInput)input).getFiles());
        }

        try {
            setInput(inputFiles);
        } catch (ParseException | IOException e) {
            throw new PartInitException("Failed to initialize Trace view", e);
        }
    }

    /**
     * @param inputFiles the TRACE input files, to be read via the {@link TraceReader} type
     */
    private void setInput(List<File> inputFiles) throws ParseException, IOException, PartInitException {
        if (inputFiles.isEmpty()) {
            throw new PartInitException("no input files specified");
        }
        if (inputFiles.size() > 1) {
            setPartName("Trace comparison");
        } else {
            setPartName(inputFiles.get(0).getName());
        }
        for (File file: inputFiles) {
            ITrace trace = TraceReader.readTrace(file);
            unfilteredTraces.add(trace);
            files.add(file);
        }
        check2ndOrderSignals();
    }

    private void check2ndOrderSignals() {
        // See https://ci.tno.nl/gitlab/trace/trace-feature/-/issues/44
        for (ITrace trace: unfilteredTraces) {
            for (IPsop signal: trace.getSignals()) {
                for (IPsopFragment f: signal.getFragments()) {
                    if (f.getOrder() == 2) {
                        final IWorkbench workbench = PlatformUI.getWorkbench();
                        workbench.getDisplay().asyncExec(new Runnable() {
                            @Override
                            public void run() {
                                MessageDialog.openWarning(null, "Warning: visualization incomplete",
                                        "Currently, the visualization of 2nd order signals is not correct. "
                                                + "A 2nd order fragments is approximated by a single first-order fragement. "
                                                + "The temporal logic analysis based on these signals, however, is correct.");
                            }
                        });
                        return;
                    }
                }
            }
        }
    }

    /**
     * To be called when the {@link TraceViewConfiguration} has been updated; see {@link #getViewConfiguration()}.
     */
    public void update() {
        traceViewer.refresh();
        mgr.update(true);
    }

    @Override
    public void createPartControl(Composite parent) {
        parent.setLayout(new GridLayout());

        mgr = new ToolBarManager(SWT.HORIZONTAL);
        mgr.createControl(parent);
        GridDataFactory.fillDefaults().align(SWT.END, SWT.CENTER).grab(true, false).applyTo(mgr.getControl());

        traceViewer = new TraceViewer(parent);
        GridDataFactory.fillDefaults().align(SWT.FILL, SWT.FILL).grab(true, true).applyTo(traceViewer.getControl());

        // Try to read the trace view configurations
        if (files.size() == 1) {
            try {
                TraceViewConfiguration viewConfig = TraceViewConfigurationIO.fromFile(getViewConfigFile(0));
                traceViewer.getPlotManager().setViewConfiguration(viewConfig);
            } catch (TraceException | IOException e) {
                // ignore
            }
        }
        traceViewer.getPlotManager().setTraces(unfilteredTraces);
        fillToolBar(mgr);
        getSite().setSelectionProvider(traceViewer);
        update();
    }

    private void fillToolBar(IToolBarManager mgr) {
        // Viewing
        mgr.add(new ToggleViewAction(this));
        mgr.add(new ToggleClaimScaleAction(this));
        mgr.add(new ToggleSignalAction(this));
        createDescribingMenu(mgr);
        createColoringMenu(mgr);
        createGroupingMenu(mgr);
        createHideMenu(mgr);
        createFilterMenu(mgr);
        // Analysis
        mgr.add(new Separator("Analysis"));
        mgr.add(new CriticalPathAction(this));
        mgr.add(new VerifyAction(this));
        mgr.add(new TimingAnalysisAction(this));
        createLittlesLawMenu(mgr);
        createResourceMenu(mgr);
        createBehaviorMenu(mgr);
        createComparisonMenu(mgr);
        // General
        mgr.add(new Separator("General"));
        createClearExtensionMenu(mgr);
        mgr.add(new SaveViewConfigurationAction(this));
    }

    private void createDescribingMenu(IToolBarManager mgr) {
        Action[] actions = new Action[6];
        actions[0] = new ClaimDescriptionToggleAction(this);
        actions[1] = new DescribingAction(this, TracePart.CLAIM);
        actions[2] = new DescribingAction(this, TracePart.RESOURCE);
        actions[3] = new DescribingAction(this, TracePart.EVENT);
        actions[4] = new DescribingAction(this, TracePart.DEPENDENCY);
        actions[5] = new DescribingAction(this, TracePart.SIGNAL);
        mgr.add(new DropDownAggregateAction(this, 0, "Describing", "/icons/xml_text_node.png", actions));
    }

    private void createColoringMenu(IToolBarManager mgr) {
        Action[] actions = new Action[3];
        actions[0] = new ColoringAction(this, TracePart.CLAIM);
        actions[1] = new ColoringAction(this, TracePart.EVENT);
        actions[2] = new ColoringAction(this, TracePart.DEPENDENCY);
        mgr.add(new DropDownAggregateAction(this, 0, "Coloring", "/icons/palette_view.gif", actions));
    }

    private void createGroupingMenu(IToolBarManager mgr) {
        Action[] actions = new Action[2];
        actions[0] = new GroupingAction(this, TracePart.CLAIM);
        actions[1] = new GroupingAction(this, TracePart.EVENT);
        mgr.add(new DropDownAggregateAction(this, 0, "Grouping", "/icons/category_obj.png", actions));
    }

    private void createFilterMenu(IToolBarManager mgr) {
        Action[] actions = new Action[5];
        actions[0] = new FilterAction(this, TracePart.CLAIM);
        actions[1] = new FilterAction(this, TracePart.RESOURCE);
        actions[2] = new FilterAction(this, TracePart.EVENT);
        actions[3] = new FilterAction(this, TracePart.DEPENDENCY);
        actions[4] = new FilterAction(this, TracePart.SIGNAL);
        mgr.add(new DropDownAggregateAction(this, 0, "Filter", "/icons/filter_ps.png", actions));
    }

    private void createLittlesLawMenu(IToolBarManager mgr) {
        Action[] actions = new Action[4];
        actions[0] = new ThroughputEventAction(this);
        actions[1] = new ThroughputIdAction(this);
        actions[2] = new LatencyAction(this);
        actions[3] = new WipAction(this);
        mgr.add(new DropDownAggregateAction(this, 1, "Little's Law analysis", "/icons/change_obj.png", actions));
    }

    private void createResourceMenu(IToolBarManager mgr) {
        Action[] actions = new Action[2];
        actions[0] = new ResourceUsageAction(this);
        actions[1] = new ResourceHistogramAction(this);
        mgr.add(new DropDownAggregateAction(this, 1, "Resource-usage analysis", "/icons/resource.png", actions));
    }

    private void createBehaviorMenu(IToolBarManager mgr) {
        Action[] actions = new Action[3];
        actions[0] = new BehaviorClassesAction(this);
        actions[1] = new BehaviorRepresentativesAction(this);
        actions[2] = new BehaviorHistogramAction(this);
        mgr.add(new DropDownAggregateAction(this, 1, "Behavioral analysis", "/icons/anomaly.png", actions));
    }

    private void createComparisonMenu(IToolBarManager mgr) {
        Action[] actions = new Action[2];
        actions[0] = new DistanceAction(this);
        actions[1] = new DependencyAction(this);
        mgr.add(new DropDownAggregateAction(this, 2, "Trace comparison", "/icons/diff.png", actions));
    }

    private void createHideMenu(IToolBarManager mgr) {
        Action[] actions = new Action[4];
        actions[0] = new HideAction(this, TracePart.CLAIM);
        actions[1] = new HideAction(this, TracePart.EVENT);
        actions[2] = new HideAction(this, TracePart.DEPENDENCY);
        actions[3] = new HideAction(this, TracePart.SIGNAL);
        mgr.add(new DropDownAggregateAction(this, 0, "Hide", "/icons/pin_view.png", actions));
    }

    private void createClearExtensionMenu(IToolBarManager mgr) {
        Action[] actions = new Action[12];
        actions[0] = new ClearFilterAction(this, TracePart.CLAIM);
        actions[1] = new ClearFilterAction(this, TracePart.RESOURCE);
        actions[2] = new ClearFilterAction(this, TracePart.EVENT);
        actions[3] = new ClearFilterAction(this, TracePart.DEPENDENCY);
        actions[4] = new ClearFilterAction(this, TracePart.SIGNAL);
        actions[5] = new ClearFilterAction(this, TracePart.ALL);
        actions[6] = new SeparatorAction();
        actions[7] = new ClearExtensionAction(this, TracePart.CLAIM);
        actions[8] = new ClearExtensionAction(this, TracePart.EVENT);
        actions[9] = new ClearExtensionAction(this, TracePart.DEPENDENCY);
        actions[10] = new ClearExtensionAction(this, TracePart.SIGNAL);
        actions[11] = new ClearExtensionAction(this, TracePart.ALL);
        mgr.add(new DropDownAggregateAction(this, 0, "Clear filters or extensions", "/icons/delete.png", actions));
    }

    @Override
    public void setFocus() {
        PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().activate(this);
        traceViewer.getControl().setFocus();
    }

    @Override
    public void dispose() {
        getSite().setSelectionProvider(null);
        super.dispose();
    }

    @Override
    public void doSave(IProgressMonitor monitor) {
    }

    @Override
    public void doSaveAs() {
    }

    @Override
    public boolean isDirty() {
        return false;
    }

    @Override
    public boolean isSaveAsAllowed() {
        return false;
    }

    public static void showView(List<File> files, boolean openTracePerspective) {
        final IWorkbench workbench = PlatformUI.getWorkbench();
        workbench.getDisplay().asyncExec(new Runnable() {
            @Override
            public void run() {
                try {
                    IWorkbenchWindow wbw = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
                    IWorkbenchPage page = wbw.getActivePage();
                    if (openTracePerspective) {
                        PlatformUI.getWorkbench().showPerspective(TracePerspectiveFactory.PERSPECTIVE_ID, wbw);
                    }
                    page.openEditor(new MultiFileEditorInput(files), TraceView.VIEW_ID);
                } catch (WorkbenchException e) {
                    IStatus status = new Status(IStatus.ERROR, OpenTraceView.class, e.getMessage(), e);
                    ErrorDialog.openError(null, "Error", "Failed to open TRACE view", status);
                }
            }
        });
    }

    private static final class MultiFileEditorInput implements IEditorInput {
        private final List<File> files;

        private MultiFileEditorInput(List<File> files) {
            this.files = files;
        }

        public List<File> getFiles() {
            return files;
        }

        @Override
        public <T> T getAdapter(Class<T> adapter) {
            return null;
        }

        @Override
        public boolean exists() {
            return false;
        }

        @Override
        public ImageDescriptor getImageDescriptor() {
            return null;
        }

        @Override
        public String getName() {
            return "MultiFileEditorInput";
        }

        @Override
        public IPersistableElement getPersistable() {
            return null;
        }

        @Override
        public String getToolTipText() {
            return "MultiFileEditorInput";
        }
    }
}
