/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import java.util.Collection;

import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.ui.view.TraceView;

public class GroupingAction extends AbstractPartitionAction {
    private final TracePart part;

    public GroupingAction(TraceView view, TracePart part) {
        super(view);
        this.part = part;
        setText(part.toString().toLowerCase() + " grouping");
    }

    @Override
    public boolean isEnabled() {
        if (!viewCfg.isActivityView()) {
            return false;
        }
        switch (part) {
            case CLAIM:
                return view.hasClaims(true);
            case EVENT:
                return view.hasEvents(false, true);
            default:
                throw new IllegalStateException();
        }
    }

    @Override
    protected TracePart getPart() {
        return part;
    }

    @Override
    protected String getDialogMessage() {
        return "Select the attributes for grouping the swimlanes";
    }

    @Override
    protected Collection<String> getSelectedAttributes() throws TraceException {
        return viewCfg.getGroupingAttributes(part);
    }

    @Override
    protected void updateSelection(Collection<String> newSelection) throws TraceException {
        viewCfg.setGroupingAttributes(part, newSelection);
    }
}
