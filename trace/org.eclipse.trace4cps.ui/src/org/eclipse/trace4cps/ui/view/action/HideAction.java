/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import org.eclipse.jface.action.Action;
import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.ui.view.TraceView;

public class HideAction extends AbstractTraceViewAction {
    private final TracePart part;

    public HideAction(TraceView view, TracePart part) {
        super(view);
        this.part = part;
        setText("Show " + part.toString().toLowerCase());
    }

    @Override
    public int getStyle() {
        return Action.AS_CHECK_BOX;
    }

    @Override
    public boolean isEnabled() {
        switch (part) {
            case CLAIM:
                return view.hasClaims(true);
            case EVENT:
                return view.hasEvents(false, true);
            case DEPENDENCY:
                return view.hasDependencies(true);
            case SIGNAL:
                return view.hasSignals(true);
            default:
                throw new IllegalStateException();
        }
    }

    @Override
    public boolean isChecked() {
        switch (part) {
            case CLAIM:
                return viewCfg.showClaims();
            case EVENT:
                return viewCfg.showEvents();
            case DEPENDENCY:
                return viewCfg.showDependencies();
            case SIGNAL:
                return viewCfg.showSignals();
            default:
                throw new IllegalStateException();
        }
    }

    @Override
    protected void doRun() throws TraceException {
        switch (part) {
            case CLAIM:
                viewCfg.setShowClaims(!viewCfg.showClaims());
                break;
            case EVENT:
                viewCfg.setShowEvents(!viewCfg.showEvents());
                break;
            case DEPENDENCY:
                viewCfg.setShowDependencies(!viewCfg.showDependencies());
                break;
            case SIGNAL:
                viewCfg.setShowSignals(!viewCfg.showSignals());
                break;
            default:
                throw new IllegalStateException();
        }
        view.update();
    }
}
