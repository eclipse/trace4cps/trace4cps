/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.ui.view.TraceView;
import org.eclipse.trace4cps.vis.jfree.ClaimScaling;

public class ToggleClaimScaleAction extends AbstractTraceViewAction {
    public ToggleClaimScaleAction(TraceView view) {
        super(view, "/icons/keygroups_obj.png");
    }

    @Override
    public String getToolTipText() {
        switch (viewCfg.getClaimScaling()) {
            case FULL:
                return "Switch claim scaling to NONE";
            case NONE:
                return "Switch claim scaling to FULL";
            default:
                return "Switch claim scaling";
        }
    }

    @Override
    public boolean isEnabled() {
        return viewCfg.isActivityView() && view.hasClaims(true);
    }

    @Override
    protected void doRun() throws TraceException {
        switch (viewCfg.getClaimScaling()) {
            case FULL:
                viewCfg.setClaimScaling(ClaimScaling.NONE);
                break;
            case NONE:
                viewCfg.setClaimScaling(ClaimScaling.FULL);
                break;
            default:
                throw new IllegalStateException();
        }
        view.update();
    }
}
