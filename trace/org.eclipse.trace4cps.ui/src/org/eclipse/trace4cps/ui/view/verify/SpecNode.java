/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.verify;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.trace4cps.analysis.mtl.InformativePrefix;
import org.eclipse.trace4cps.tl.VerificationResult;
import org.eclipse.trace4cps.ui.view.TraceView;

public final class SpecNode {
    private final TraceView view;

    private final File specFile;

    private final List<ResultNode> results = new ArrayList<>();

    public SpecNode(File f, TraceView v) {
        specFile = f;
        view = v;
        results.add(new ResultNode(InformativePrefix.BAD, view));
        results.add(new ResultNode(InformativePrefix.GOOD, view));
        results.add(new ResultNode(InformativePrefix.NON_INFORMATIVE, view));
    }

    public File getSpecFile() {
        return specFile;
    }

    public List<ResultNode> getResults() {
        return results;
    }

    public void add(VerificationResult vr) {
        for (ResultNode r: results) {
            r.add(vr);
        }
    }

    public boolean refreshSpecNode(long lastModified) {
        lastModified = Math.max(lastModified, specFile.lastModified());
        boolean allEmpty = true;
        for (ResultNode rn: results) {
            boolean emptyResultNode = rn.refresh(lastModified);
            allEmpty = emptyResultNode & allEmpty;
        }
        return allEmpty;
    }
}
