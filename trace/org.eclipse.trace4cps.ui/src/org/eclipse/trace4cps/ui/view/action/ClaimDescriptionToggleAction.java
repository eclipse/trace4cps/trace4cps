/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import org.eclipse.jface.action.IAction;
import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.ui.view.TraceView;

public class ClaimDescriptionToggleAction extends AbstractTraceViewAction {
    public ClaimDescriptionToggleAction(TraceView view) {
        super(view, null);
        setText("show claim descriptions");
        setChecked(viewCfg.getShowClaimLabels());
    }

    @Override
    public int getStyle() {
        return IAction.AS_CHECK_BOX;
    }

    @Override
    protected void doRun() throws TraceException {
        viewCfg.setShowClaimLabels(isChecked());
        view.update();
    }
}
