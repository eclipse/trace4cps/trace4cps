/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view;

import java.awt.geom.Point2D;

import org.eclipse.trace4cps.common.jfreechart.ui.viewers.BackReferenceProvider;
import org.jfree.chart.annotations.XYMeasurementAnnotation;

public class MeasurementAnnotation extends XYMeasurementAnnotation implements BackReferenceProvider<Measurement> {
    private static final long serialVersionUID = -5323934653535661111L;

    private final Measurement measurement;

    public MeasurementAnnotation(Measurement measurement, boolean productivity, Point2D p1, Point2D p2) {
        super(Orientation.HORIZONTAL,
                " " + (productivity ? measurement.toProductivityString() : measurement.toString()) + " ", p1, p2);
        this.measurement = measurement;
    }

    @Override
    public Measurement getBackReference() {
        return measurement;
    }
}
