/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.timing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LabeledNode implements Comparable<LabeledNode> {
    private final String label;

    private final LabeledNode parent;

    private final List<LabeledNode> children = new ArrayList<>();

    public LabeledNode(LabeledNode parent, String label) {
        this.parent = parent;
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public LabeledNode getParent() {
        return parent;
    }

    public void addChild(LabeledNode n) {
        children.add(n);
        Collections.sort(children);
    }

    public List<? extends LabeledNode> getChildren() {
        return children;
    }

    @Override
    public int compareTo(LabeledNode o) {
        return getLabel().compareTo(o.getLabel());
    }
}
