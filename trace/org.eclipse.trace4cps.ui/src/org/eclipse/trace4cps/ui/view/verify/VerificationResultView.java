/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.verify;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.trace4cps.analysis.mtl.ExplanationTable;
import org.eclipse.trace4cps.analysis.mtl.ExplanationTable.Region;
import org.eclipse.trace4cps.analysis.mtl.InformativePrefix;
import org.eclipse.trace4cps.analysis.mtl.MtlBuilder;
import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.mtl.MtlUtil;
import org.eclipse.trace4cps.analysis.mtl.State;
import org.eclipse.trace4cps.analysis.signal.impl.PsopHelper;
import org.eclipse.trace4cps.analysis.stl.StlFormula;
import org.eclipse.trace4cps.analysis.stl.impl.StlNeg;
import org.eclipse.trace4cps.analysis.stl.impl.StlTrue;
import org.eclipse.trace4cps.analysis.stl.impl.StlUntil;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.core.impl.Event;
import org.eclipse.trace4cps.core.impl.ModifiableTrace;
import org.eclipse.trace4cps.tl.VerificationResult;
import org.eclipse.trace4cps.ui.view.TraceView;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.osgi.framework.Bundle;

public class VerificationResultView extends ViewPart
        implements IResourceChangeListener, IDoubleClickListener, IPartListener
{
    public static final String VIEW_ID = "org.eclipse.trace4cps.ui.view.verify.VerificationResultView";

    private ResultTree tree = new ResultTree();

    private TreeViewer viewer;

    public static void showView(String specFile, String traceFile, VerificationResult results, TraceView traceView) {
        final IWorkbench workbench = PlatformUI.getWorkbench();
        workbench.getDisplay().asyncExec(new Runnable() {
            @Override
            public void run() {
                try {
                    IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
                    IViewPart view = page.showView(VerificationResultView.VIEW_ID);
                    ((VerificationResultView)view).setResult(specFile, traceFile, results, traceView);
                } catch (CoreException e) {
                    ErrorDialog.openError(null, "ETL Verification Error", "Failed to update result view",
                            e.getStatus());
                }
            }
        });
    }

    private void setResult(String specPath, String tracePath, VerificationResult res, TraceView view) {
        tree.refresh();
        tree.add(tracePath, specPath, res, view);
        tree.refresh();
        viewer.refresh();
        viewer.expandToLevel(4);
    }

    @Override
    public void partActivated(IWorkbenchPart part) {
    }

    @Override
    public void partBroughtToTop(IWorkbenchPart part) {
    }

    @Override
    public void partClosed(IWorkbenchPart part) {
        tree.partClosed(part);
        viewer.refresh();
    }

    @Override
    public void partDeactivated(IWorkbenchPart part) {
    }

    @Override
    public void partOpened(IWorkbenchPart part) {
    }

    @Override
    public void createPartControl(Composite c) {
        IWorkspace workspace = ResourcesPlugin.getWorkspace();
        workspace.addResourceChangeListener(this);
        GridLayoutFactory.swtDefaults().numColumns(1).applyTo(c);
        viewer = new TreeViewer(c, SWT.SINGLE);
        GridDataFactory.swtDefaults().applyTo(viewer.getTree());
        GridDataFactory.fillDefaults().grab(true, true).applyTo(viewer.getTree());
        viewer.setContentProvider(new TreeContentProvider());
        viewer.setLabelProvider(new TreeLabelProvider());
        viewer.setInput(tree);
        viewer.addDoubleClickListener(this);
        final IWorkbench workbench = PlatformUI.getWorkbench();
        workbench.getActiveWorkbenchWindow().getActivePage().addPartListener(this);
    }

    @Override
    public void dispose() {
        IWorkspace workspace = ResourcesPlugin.getWorkspace();
        workspace.removeResourceChangeListener(this);
        final IWorkbench workbench = PlatformUI.getWorkbench();
        workbench.getActiveWorkbenchWindow().getActivePage().removePartListener(this);
        super.dispose();
    }

    @Override
    public void setFocus() {
    }

    @Override
    public void resourceChanged(IResourceChangeEvent event) {
        tree.refresh();
        // We may not be on the UI thread
        final IWorkbench workbench = PlatformUI.getWorkbench();
        workbench.getDisplay().asyncExec(new Runnable() {
            @Override
            public void run() {
                viewer.refresh();
            }
        });
    }

    @Override
    public void doubleClick(DoubleClickEvent event) {
        IStructuredSelection selection = (IStructuredSelection)event.getSelection();
        if (selection == null || selection.isEmpty()) {
            return;
        }
        Object sel = selection.getFirstElement();
        if (sel instanceof CheckNode) {
            CheckNode cn = (CheckNode)sel;
            if (!cn.isParameterized() && cn.getMTLresult().getExplanation() != null) {
                createCounterExampleTrace(cn.getResult(), cn.getFormula(), cn.getTraceView());
            }
        }
        if (sel instanceof ParameterizedCheckNode) {
            ParameterizedCheckNode cn = (ParameterizedCheckNode)sel;
            if (cn.getMTLresult().getExplanation() != null) {
                createCounterExampleTrace(cn.getResult(), cn.getFormula(), cn.getTraceView());
            }
        }
    }

    private void createCounterExampleTrace(VerificationResult r, MtlFormula formula, TraceView view) {
        ModifiableTrace trace = (ModifiableTrace)view.getTrace();

        ExplanationTable ex = r.getResult(formula).getExplanation();
        List<? extends State> states = ex.getTrace();

        addMtlFormula(r, formula, trace, ex, states);
        setGrouping(view);
    }

    private void setGrouping(TraceView view) {
        Set<String> groupAtts = new HashSet<>();
        if (view.getViewConfiguration().getGroupingAttributes(TracePart.EVENT) != null) {
            groupAtts.addAll(view.getViewConfiguration().getGroupingAttributes(TracePart.EVENT));
        }
        groupAtts.add("phi");
        groupAtts.add("sat");
        view.getViewConfiguration().setGroupingAttributes(TracePart.EVENT, groupAtts);

        Set<String> groupAtts2 = new HashSet<>();
        if (view.getViewConfiguration().getGroupingAttributes(TracePart.CLAIM) != null) {
            groupAtts2.addAll(view.getViewConfiguration().getGroupingAttributes(TracePart.CLAIM));
        }
        groupAtts2.add("phi");
        view.getViewConfiguration().setGroupingAttributes(TracePart.CLAIM, groupAtts2);
        view.update();
    }

    private void addMtlFormula(VerificationResult r, MtlFormula formula, ModifiableTrace trace, ExplanationTable ex,
            List<? extends State> states)
    {
        for (MtlFormula phi: MtlUtil.getSubformulas(formula)) {
            if (!phi.equals(MtlBuilder.TRUE()) && r.contains(phi)) {
                if (phi instanceof StlFormula) {
                    addStlExplanationClaims(r, (StlFormula)phi, trace);
                } else {
                    addMtlExplanationEvents(phi, states, r, trace, ex);
                }
            }
        }
    }

    private void addMtlExplanationEvents(MtlFormula phi, List<? extends State> states, VerificationResult r,
            ModifiableTrace trace, ExplanationTable ex)
    {
        for (Region region: ex.getRegions(phi)) {
            if (region.getValue() != InformativePrefix.NOT_COMPUTED) {
                String color = "black";
                switch (region.getValue()) {
                    case BAD:
                        color = "dark_red";
                        break;
                    case GOOD:
                        color = "dark_green";
                        break;
                    case NON_INFORMATIVE:
                        color = "dark_blue";
                        break;
                    case NOT_COMPUTED: // should never occur
                        color = "gray";
                        break;
                }
                List<IEvent> events = new ArrayList<>();
                for (int i = region.getStartIndex(); i <= region.getEndIndex(); i++) {
                    // Add an event at every state in the region
                    Event e = new Event(states.get(i).getTimestamp());
                    e.setAttribute("phi", r.getName(phi));
                    e.setAttribute("color", color);
                    e.setAttribute("type", "STL");
                    e.setAttribute("sat", region.getValue().toString());
                    events.add(e);
                }
                trace.addEvents(events);
            }
        }
    }

    private void addStlExplanationClaims(VerificationResult r, StlFormula phi, ModifiableTrace trace) {
        if (!isGlobally(phi)) {
            IPsop p = phi.getSignal();
            List<IClaim> claims = PsopHelper.createClaimRepresentationOfSatisfaction(p, r.getName(phi));
            trace.addClaims(claims);
        }
    }

    private boolean isGlobally(StlFormula phi) {
        if (phi instanceof StlNeg) {
            StlFormula phi2 = ((StlNeg)phi).getFormula();
            if (phi2 instanceof StlUntil) {
                StlFormula f1 = ((StlUntil)phi2).getLeft();
                if (f1 instanceof StlTrue && ((StlUntil)phi2).isUntimed()) {
                    return true;
                }
            }
        }
        return false;
    }

    private final class TreeContentProvider implements ITreeContentProvider {
        @Override
        public void dispose() {
        }

        @Override
        public void inputChanged(Viewer arg0, Object arg1, Object arg2) {
        }

        @Override
        public Object[] getChildren(Object o) {
            if (o instanceof FileNode) {
                return ((FileNode)o).getSpecs().toArray();
            }
            if (o instanceof SpecNode) {
                return ((SpecNode)o).getResults().toArray();
            }
            if (o instanceof ResultNode) {
                return ((ResultNode)o).getChecks().toArray();
            }
            if (o instanceof CheckNode) {
                return ((CheckNode)o).getSubChecks().toArray();
            }
            return new Object[0];
        }

        @Override
        public Object[] getElements(Object input) {
            return tree.traces.toArray();
        }

        @Override
        public Object getParent(Object o) {
            return null;
        }

        @Override
        public boolean hasChildren(Object o) {
            if (o instanceof FileNode) {
                return !((FileNode)o).getSpecs().isEmpty();
            }
            if (o instanceof SpecNode) {
                return !((SpecNode)o).getResults().isEmpty();
            }
            if (o instanceof ResultNode) {
                return !((ResultNode)o).getChecks().isEmpty();
            }
            if (o instanceof CheckNode) {
                return ((CheckNode)o).isParameterized();
            }
            return false;
        }
    }

    private final class TreeLabelProvider implements ILabelProvider {
        private Image good;

        private Image bad;

        private Image neutral;

        public TreeLabelProvider() {
            Bundle bundle = Platform.getBundle("org.eclipse.trace4cps.ui");
            URL fullPathString = FileLocator.find(bundle, new Path("icons/passed.png"), null);
            ImageDescriptor imageDesc = ImageDescriptor.createFromURL(fullPathString);
            good = imageDesc.createImage();

            fullPathString = FileLocator.find(bundle, new Path("icons/delete.png"), null);
            imageDesc = ImageDescriptor.createFromURL(fullPathString);
            bad = imageDesc.createImage();

            fullPathString = FileLocator.find(bundle, new Path("icons/help_contents.png"), null);
            imageDesc = ImageDescriptor.createFromURL(fullPathString);
            neutral = imageDesc.createImage();
        }

        @Override
        public void addListener(ILabelProviderListener arg0) {
        }

        @Override
        public void dispose() {
            good.dispose();
            bad.dispose();
            neutral.dispose();
        }

        @Override
        public boolean isLabelProperty(Object arg0, String arg1) {
            return false;
        }

        @Override
        public void removeListener(ILabelProviderListener arg0) {
        }

        @Override
        public Image getImage(Object o) {
            if (o instanceof ResultNode) {
                ResultNode r = (ResultNode)o;
                if (r.getType() == InformativePrefix.GOOD) {
                    return good;
                } else if (r.getType() == InformativePrefix.BAD) {
                    return bad;
                } else {
                    return neutral;
                }
            }
            ISharedImages im = PlatformUI.getWorkbench().getSharedImages();
            return im.getImage(ISharedImages.IMG_OBJ_FILE);
        }

        @Override
        public String getText(Object o) {
            if (o instanceof FileNode) {
                return ((FileNode)o).getTraceFile().getName();
            }
            if (o instanceof SpecNode) {
                return ((SpecNode)o).getSpecFile().getName();
            }
            if (o instanceof ResultNode) {
                ResultNode rn = (ResultNode)o;
                return rn.getType().toString();
            }
            if (o instanceof CheckNode) {
                CheckNode cn = (CheckNode)o;
                if (cn.isParameterized()) {
                    return cn.getCheckName() + " [" + cn.getSubChecks().size() + "]";
                }
                return cn.getCheckName();
            }
            if (o instanceof ParameterizedCheckNode) {
                return ((ParameterizedCheckNode)o).getCheckName();
            }
            return o.toString();
        }
    }
}
