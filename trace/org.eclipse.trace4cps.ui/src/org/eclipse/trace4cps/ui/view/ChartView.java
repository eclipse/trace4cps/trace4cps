/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view;

import java.awt.Frame;
import java.lang.reflect.InvocationTargetException;

import javax.swing.SwingUtilities;

import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;

public class ChartView extends ViewPart {
    public static final String VIEW_ID = ChartView.class.getName() + ".id";

    private Frame frame;

    @Override
    public void createPartControl(Composite parent) {
        frame = SWT_AWT.new_Frame(new Composite(parent, SWT.EMBEDDED | SWT.NO_BACKGROUND));
    }

    @Override
    public void setFocus() {
    }

    @Override
    public void dispose() {
        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    frame.dispose();
                }
            });
        } catch (InvocationTargetException | InterruptedException e) {
        }
        super.dispose();
    }

    private void setResult(JFreeChart chart) {
        frame.add(new ChartPanel(chart));
    }

    public static synchronized void showChart(JFreeChart chart, String title, String tip) {
        final IWorkbench workbench = PlatformUI.getWorkbench();
        workbench.getDisplay().asyncExec(new Runnable() {
            @Override
            public void run() {
                try {
                    IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
                    ChartView view = (ChartView)page.showView(ChartView.VIEW_ID,
                            Long.toHexString(System.currentTimeMillis()), IWorkbenchPage.VIEW_ACTIVATE);
                    view.setPartName(title);
                    view.setTitleToolTip(tip);
                    view.setResult(chart);
                } catch (PartInitException e) {
                    ErrorDialog.openError(null, "Error", "Failed to open chart view", e.getStatus());
                }
            }
        });
    }
}
