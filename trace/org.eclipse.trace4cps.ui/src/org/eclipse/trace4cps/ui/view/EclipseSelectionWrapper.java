/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.eclipse.trace4cps.core.IAttributeAware;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IDependency;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.IPsopFragment;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertySource;
import org.eclipse.ui.views.properties.PropertyDescriptor;

/**
 * This type helps integrates the trace selection mechanism with the Eclipse properties view.
 */
public class EclipseSelectionWrapper implements IPropertySource {
    private static final String ATTS = "Attributes";

    private static final String CLAIM_ATTS = "Claim attributes";

    private static final String EVENT_ATTS = "Event attributes";

    private static final String DEP_ATTS = "Dependency attributes";

    private static final String SIGNAL_ATTS = "Signal attributes";

    private static final String FRAGMENT = "Fragment properties";

    private static final String CLAIM = "Claim properties";

    private static final String EVENT = "Event properties";

    private static final String DEPENDENCY = "Dependency properties";

    private static final String RESOURCE = "Resource properties";

    private static final String RESOURCE_ATTS = "Resource attributes";

    private final Map<String, Map<String, String>> catProps = new HashMap<>();

    private final Map<String, String> id2Value = new HashMap<>();

    private final Object item;

    public EclipseSelectionWrapper(IPsop psop, IPsopFragment item) {
        this.item = item;
        addAttributes(SIGNAL_ATTS, psop);
        Map<String, String> p = new LinkedHashMap<>();
        p.put("start time", item.dom().lb().toString());
        p.put("end time", item.dom().ub().toString());
        p.put("duration", Double.toString(item.dom().ub().doubleValue() - item.dom().lb().doubleValue()));
        p.put("a", item.getA().toString());
        p.put("b", item.getB().toString());
        p.put("c", item.getC().toString());
        p.put("shape", item.getShape().toString());
        catProps.put(FRAGMENT, p);
    }

    public EclipseSelectionWrapper(IAttributeAware item) {
        this.item = item;
        if (item instanceof IClaim) {
            IClaim claim = (IClaim)item;
            // Claim stuff:
            addAttributes(CLAIM_ATTS, claim);
            Map<String, String> claimProps = new LinkedHashMap<>();
            claimProps.put("start time", Double.toString(claim.getStartTime().doubleValue()));
            claimProps.put("end time", Double.toString(claim.getEndTime().doubleValue()));
            claimProps.put("duration",
                    Double.toString(claim.getEndTime().doubleValue() - claim.getStartTime().doubleValue()));
            claimProps.put("amount", claim.getAmount().toString());
            if (claim.getResource().useOffset()) {
                claimProps.put("offset", claim.getOffset().toString());
            }
            catProps.put(CLAIM, claimProps);
            // Resource stuff:
            addAttributes(RESOURCE_ATTS, claim.getResource());
            Map<String, String> resProps = new LinkedHashMap<>();
            resProps.put("uses offset", Boolean.toString(claim.getResource().useOffset()));
            resProps.put("capacity", claim.getResource().getCapacity().toString());
            catProps.put(RESOURCE, resProps);
        } else if (item instanceof IEvent) {
            IEvent event = (IEvent)item;
            addAttributes(EVENT_ATTS, event);
            Map<String, String> eventProps = new LinkedHashMap<>();
            eventProps.put("start time", Double.toString(event.getTimestamp().doubleValue()));
            catProps.put(EVENT, eventProps);
        } else if (item instanceof IDependency) {
            IDependency dep = (IDependency)item;
            addAttributes(DEP_ATTS, dep);
            Map<String, String> depProps = new LinkedHashMap<>();
            depProps.put("start time", dep.getSrc().getTimestamp().toString());
            depProps.put("end time", dep.getDst().getTimestamp().toString());
            depProps.put("duration", Double
                    .toString(dep.getDst().getTimestamp().doubleValue() - dep.getSrc().getTimestamp().doubleValue()));
            catProps.put(DEPENDENCY, depProps);
        } else {
            addAttributes(ATTS, item);
        }
    }

    public Object getSelectedTraceItem() {
        return item;
    }

    private void addAttributes(String cat, IAttributeAware a) {
        Map<String, String> atts = new TreeMap<>(); // keys ordered
        atts.putAll(a.getAttributes());
        catProps.put(cat, atts);
    }

    @Override
    public Object getEditableValue() {
        return this;
    }

    @Override
    public IPropertyDescriptor[] getPropertyDescriptors() {
        List<IPropertyDescriptor> r = new ArrayList<>();
        for (Map.Entry<String, Map<String, String>> e: catProps.entrySet()) {
            String cat = e.getKey();
            for (Map.Entry<String, String> e2: e.getValue().entrySet()) {
                String id = cat + "_" + e2.getKey();
                r.add(new ClaimPropertyDescriptor(cat, id, e2.getKey()));
                id2Value.put(id, e2.getValue());
            }
        }
        return r.toArray(new IPropertyDescriptor[r.size()]);
    }

    @Override
    public Object getPropertyValue(Object id) {
        return id2Value.get(id);
    }

    @Override
    public boolean isPropertySet(Object id) {
        return false;
    }

    @Override
    public void resetPropertyValue(Object id) {
    }

    @Override
    public void setPropertyValue(Object id, Object value) {
    }

    private static final class ClaimPropertyDescriptor extends PropertyDescriptor {
        public ClaimPropertyDescriptor(String cat, String id, String displayName) {
            super(id, displayName);
            setCategory(cat);
            setAlwaysIncompatible(true);
        }
    }
}
