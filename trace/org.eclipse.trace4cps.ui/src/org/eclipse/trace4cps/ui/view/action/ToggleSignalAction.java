/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.ui.view.TraceView;

public class ToggleSignalAction extends AbstractTraceViewAction {
    public ToggleSignalAction(TraceView view) {
        super(view, "/icons/XSDSequence.gif");
    }

    @Override
    public String getToolTipText() {
        if (viewCfg.getShowSignalMarkers()) {
            return "Hide signal markers";
        } else {
            return "Show signal markers";
        }
    }

    @Override
    public boolean isEnabled() {
        return view.hasSignals(true);
    }

    @Override
    protected void doRun() throws TraceException {
        viewCfg.setShowSignalMarkers(!viewCfg.getShowSignalMarkers());
        view.update();
    }
}
