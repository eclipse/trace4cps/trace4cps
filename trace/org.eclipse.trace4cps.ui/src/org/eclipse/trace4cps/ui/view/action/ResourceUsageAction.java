/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.trace4cps.analysis.signal.SignalModifier;
import org.eclipse.trace4cps.analysis.signal.SignalUtil;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.ui.dialog.ResourceClientHistogramDialog;
import org.eclipse.trace4cps.ui.view.TraceView;

public class ResourceUsageAction extends AbstractTraceViewAction {
    public ResourceUsageAction(TraceView view) {
        super(view);
        setText("Resource-usage graph");
    }

    @Override
    public boolean isEnabled() {
        return view.getNumTraces() == 1 && view.hasClaims(true);
    }

    @Override
    protected void doRun() throws TraceException {
        ResourceClientHistogramDialog dialog = new ResourceClientHistogramDialog(view.getEditorSite().getShell(),
                view.getTrace().getResources(), false);
        if (dialog.open() == Dialog.CANCEL) {
            return;
        }

        double width = dialog.getPositiveDoubleValue();
        SignalModifier mod = SignalModifier.NONE;
        if (width != 0d) {
            mod = new SignalModifier(1d, width, TimeUnit.SECONDS);
        }

        String type = dialog.useAmount() ? "amount" : "#clients";
        for (IResource r: dialog.getSelected()) {
            IPsop p;
            if (dialog.useAmount()) {
                p = SignalUtil.getResourceAmount(view.getTrace(), r, mod);
            } else {
                p = SignalUtil.getResourceClients(view.getTrace(), r, mod);
            }
            for (Map.Entry<String, String> e: r.getAttributes().entrySet()) {
                p.setAttribute(e.getKey(), e.getValue());
            }
            p.setAttribute("resource-usage type", type);
            p.setAttribute("convolution width",
                    Double.toString(dialog.getPositiveDoubleValue()) + " " + TimeUnit.SECONDS);
            view.addExtension(p, 0);
        }
        view.update();
    }
}
