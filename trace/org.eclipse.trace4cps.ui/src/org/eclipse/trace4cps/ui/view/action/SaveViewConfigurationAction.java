/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.swing.SwingUtilities;

import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.ui.view.TraceView;
import org.eclipse.trace4cps.vis.jfree.TraceViewConfigurationIO;

public class SaveViewConfigurationAction extends AbstractTraceViewAction {
    public SaveViewConfigurationAction(TraceView view) {
        super(view, "/icons/save_edit.png");
        setToolTipText("Save view configuration");
    }

    @Override
    public boolean isEnabled() {
        return view.getNumTraces() == 1;
    }

    @Override
    protected void doRun() throws TraceException {
        File file = view.getViewConfigFile(0);
        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    viewCfg.setRange(view.getDomainAxisRange());
                }
            });
            TraceViewConfigurationIO.toFile(viewCfg, file);
        } catch (IOException | InterruptedException | InvocationTargetException e) {
            throw new TraceException("Failed to write configuration file", e);
        }
    }
}
