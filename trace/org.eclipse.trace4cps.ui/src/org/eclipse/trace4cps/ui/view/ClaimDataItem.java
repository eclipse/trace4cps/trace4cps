/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view;

import org.eclipse.trace4cps.common.jfreechart.ui.viewers.BackReferenceProvider;
import org.eclipse.trace4cps.core.IClaim;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYIntervalDataItem;
import org.jfree.data.xy.XYIntervalSeries;
import org.jfree.data.xy.XYIntervalSeriesCollection;

/**
 * This is a data item for use with the JFreeChart {@code XYIntervalSeriesCollection} and {@code XYIntervalSeries} type.
 * The methods {@code XYIntervalSeriesCollection#getSeries} and {@code XYIntervalSeries#getDataItem} can then be used to
 * retrieve instances of this type.
 */
public class ClaimDataItem extends XYIntervalDataItem implements BackReferenceProvider<EclipseSelectionWrapper> {
    private static final long serialVersionUID = 1L;

    private final EclipseSelectionWrapper backReference;

    public ClaimDataItem(IClaim claim, double xmin, double xmax, double ymin, double ymax) {
        super((xmin + xmax) / 2, xmin, xmax, (ymin + ymax) / 2, ymin, ymax);
        this.backReference = new EclipseSelectionWrapper(claim);
    }

    @Override
    public EclipseSelectionWrapper getBackReference() {
        return backReference;
    }

    @Override
    public String toString() {
        return "ClaimDataItem[claim=" + getBackReference().getSelectedTraceItem() + "]";
    }

    public static ClaimDataItem getFrom(XYDataset ds, int series, int item) {
        if (ds instanceof XYIntervalSeriesCollection) {
            XYIntervalSeriesCollection sc = (XYIntervalSeriesCollection)ds;
            XYIntervalSeries s = sc.getSeries(series);
            Object o = s.getDataItem(item);
            if (o instanceof ClaimDataItem) {
                return (ClaimDataItem)o;
            }
        }
        return null;
    }
}
