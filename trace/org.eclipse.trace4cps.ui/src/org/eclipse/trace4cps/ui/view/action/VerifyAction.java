/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.trace4cps.core.IInterval;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.core.impl.TraceHelper;
import org.eclipse.trace4cps.tl.VerificationHelper;
import org.eclipse.trace4cps.tl.VerificationResult;
import org.eclipse.trace4cps.tl.etl.EtlModel;
import org.eclipse.trace4cps.tl.ui.Util;
import org.eclipse.trace4cps.tl.ui.internal.TlActivator;
import org.eclipse.trace4cps.ui.ConsoleUtil;
import org.eclipse.trace4cps.ui.view.TraceView;
import org.eclipse.trace4cps.ui.view.verify.VerificationResultView;
import org.eclipse.xtext.diagnostics.Severity;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.util.CancelIndicator;
import org.eclipse.xtext.validation.CheckMode;
import org.eclipse.xtext.validation.IResourceValidator;
import org.eclipse.xtext.validation.Issue;

import com.google.inject.Injector;
import com.google.inject.Provider;

public class VerifyAction extends AbstractTraceViewAction {
    public static final String SPEC_FILE_EXTENSION = ".etl";

    public VerifyAction(TraceView view) {
        super(view, "/icons/passed.png");
        setToolTipText("Verify");
    }

    @Override
    public boolean isEnabled() {
        return view.getNumTraces() == 1;
    }

    @Override
    protected void doRun() throws TraceException {
        String specFile = Util.browseForFileInWorkspace(view.getEditorSite().getShell(),
                new String[]
                {SPEC_FILE_EXTENSION});
        if (specFile != null) {
            ITrace trace = view.getTrace();
            Resource resource = getValidatedResource(specFile);
            if (resource == null) {
                return; // syntax errors or validation errors in given model
            }
            EtlModel model = (EtlModel)resource.getContents().get(0);
            VerificationHelper vh = new VerificationHelper(trace, model);
            VerificationResult vr;
            try {
                vr = vh.run();
                ConsoleUtil.log("Checking " + new File(specFile).getName() + " on " + view.getTraceFile(0).getName());
                IInterval dom = vh.getTimeDomainForVerification();
                ConsoleUtil.log(" - Domain for verification: " + dom + " " + trace.getTimeUnit());
                if (vh.statesInjected()) {
                    int injected = vh.getNumInjectedStates();
                    double domWidth = dom.ub().doubleValue() - dom.lb().doubleValue();
                    double stepSize = domWidth / injected;
                    ConsoleUtil.log(" - Injecting 10.000 events: time between events <= " + stepSize + " "
                            + trace.getTimeUnit() + " " + getStepFreq(stepSize));
                }
            } catch (InterruptedException | ExecutionException e) {
                throw new TraceException("Failed to run verification", e);
            }
            VerificationResultView.showView(specFile, TraceHelper.getValues(trace, false), vr, view);
        }
    }

    private String getStepFreq(double stepSize) {
        switch (view.getTrace().getTimeUnit()) {
            case SECONDS:
                return " (" + 1d / stepSize + " Hz)";
            case MILLISECONDS:
                return " (" + 1d / stepSize + " kHz)";
            case MICROSECONDS:
                return " (" + 1d / stepSize + " MHz)";
            default:
                return "";
        }
    }

    private Resource getValidatedResource(String filename) throws TraceException {
        Injector injector = TlActivator.getInstance().getInjector(TlActivator.ORG_ECLIPSE_TRACE4CPS_TL_ETL);
        Provider<ResourceSet> resourceSetProvider = injector.getProvider(ResourceSet.class);
        ResourceSet resourceSet = resourceSetProvider.get();
        Resource resource = resourceSet.getResource(URI.createFileURI(filename), true);
        // Validation
        IResourceValidator validator = ((XtextResource)resource).getResourceServiceProvider().getResourceValidator();
        List<Issue> issues = validator.validate(resource, CheckMode.ALL, CancelIndicator.NullImpl);
        for (Issue issue: issues) {
            if (issue.getSeverity() == Severity.ERROR) {
                throw new TraceException(
                        "Issue in specification: " + issue.getMessage() + " at line " + issue.getLineNumber());
            }
        }
        return resource;
    }
}
