/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IMenuCreator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.trace4cps.ui.view.TraceView;

public class DropDownAggregateAction extends Action implements IMenuCreator {
    private final String imgPath;

    private final TraceView view;

    private final int enabledTraceCnt;

    private Menu menu;

    private final Action[] actions;

    /**
     * @param view the view of this action
     * @param enabledTraceCount <=0 means always enabled, 1 means only enabled if 1 trace is viewed, >1 means only
     *     enabled for multiple traces
     * @param toolTip
     * @param imgPath
     * @param actions
     */
    public DropDownAggregateAction(TraceView view, int enabledTraceCount, String toolTip, String imgPath,
            Action... actions)
    {
        super(null, Action.AS_DROP_DOWN_MENU);
        this.imgPath = imgPath;
        this.actions = actions;
        this.view = view;
        this.enabledTraceCnt = enabledTraceCount;
        setToolTipText(toolTip);
    }

    @Override
    public boolean isEnabled() {
        if (enabledTraceCnt <= 0) {
            return true;
        } else if (enabledTraceCnt == 1) {
            return view.getNumTraces() == 1;
        } else {
            return view.getNumTraces() > 1;
        }
    }

    @Override
    public ImageDescriptor getImageDescriptor() {
        return ImageDescriptor.createFromURL(getClass().getResource(imgPath));
    }

    @Override
    public void runWithEvent(Event event) {
        if (event.widget instanceof ToolItem) {
            ToolItem toolItem = (ToolItem)event.widget;
            Control control = toolItem.getParent();
            Menu menu = getMenu(control);
            Rectangle bounds = toolItem.getBounds();
            Point topLeft = new Point(bounds.x, bounds.y + bounds.height);
            menu.setLocation(control.toDisplay(topLeft));
            menu.setVisible(true);
        }
    }

    @Override
    public Menu getMenu(Menu parent) {
        return null;
    }

    @Override
    public Menu getMenu(Control parent) {
        if (menu != null) {
            menu.dispose();
        }
        menu = new Menu(parent);
        for (Action act: actions) {
            if (act instanceof SeparatorAction) {
                new MenuItem(menu, SWT.SEPARATOR);
            } else {
                ActionContributionItem item = new ActionContributionItem(act);
                item.fill(menu, -1);
            }
        }
        return menu;
    }

    @Override
    public void dispose() {
        if (menu != null) {
            menu.dispose();
            menu = null;
        }
    }
}
