/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import java.util.Collection;

import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.ui.view.TraceView;

public class ColoringAction extends AbstractPartitionAction {
    private final TracePart part;

    public ColoringAction(TraceView view, TracePart part) {
        super(view);
        this.part = part;
        setText(part.toString().toLowerCase() + " coloring");
    }

    @Override
    public boolean isEnabled() {
        switch (part) {
            case CLAIM:
                return view.hasClaims(true);
            case EVENT:
                return view.hasEvents(false, true);
            case DEPENDENCY:
                return view.hasDependencies(true);
            default:
                throw new IllegalStateException();
        }
    }

    @Override
    protected TracePart getPart() {
        return part;
    }

    @Override
    protected String getDialogMessage() {
        return "Select the attributes for coloring";
    }

    @Override
    protected Collection<String> getSelectedAttributes() throws TraceException {
        return viewCfg.getColoringAttributes(part);
    }

    @Override
    protected void updateSelection(Collection<String> newSelection) throws TraceException {
        viewCfg.clearHighlight();
        viewCfg.setColoringAttributes(part, newSelection);
    }
}
