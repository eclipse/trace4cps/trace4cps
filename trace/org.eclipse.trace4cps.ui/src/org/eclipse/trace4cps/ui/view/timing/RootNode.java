/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.timing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.eclipse.trace4cps.analysis.timing.StatisticsManager;

public class RootNode extends LabeledNode {
    private final List<SectionNode> children = new ArrayList<>();

    public RootNode(String label, Map<String, StatisticsManager> timing) {
        super(null, label);
        for (Map.Entry<String, StatisticsManager> e: timing.entrySet()) {
            children.add(new SectionNode(this, e.getKey(), e.getValue()));
        }
        Collections.sort(children, new Comparator<SectionNode>() {
            @Override
            public int compare(SectionNode o1, SectionNode o2) {
                return o1.getLabel().compareTo(o2.getLabel());
            }
        });
    }

    @Override
    public List<SectionNode> getChildren() {
        return children;
    }
}
