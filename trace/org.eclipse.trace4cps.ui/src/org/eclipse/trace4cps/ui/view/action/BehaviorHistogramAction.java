/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import java.util.Set;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.ui.AnalysisUtil;
import org.eclipse.trace4cps.ui.dialog.BehaviorWizard;
import org.eclipse.trace4cps.ui.view.ChartView;
import org.eclipse.trace4cps.ui.view.TraceView;
import org.jfree.chart.JFreeChart;

public class BehaviorHistogramAction extends AbstractTraceViewAction {
    public BehaviorHistogramAction(TraceView view) {
        super(view);
        setText("Behavioral histogram");
    }

    @Override
    public boolean isEnabled() {
        return view.getNumTraces() == 1 && view.hasClaims(true);
    }

    @Override
    protected void doRun() throws TraceException {
        BehaviorWizard wizard = new BehaviorWizard(view);
        if (new WizardDialog(view.getEditorSite().getShell(), wizard).open() == Dialog.CANCEL) {
            return;
        }
        String idAtt = wizard.getIdAttribute();
        Set<String> uniqueness = wizard.getUniqueness();

        JFreeChart chart = AnalysisUtil.computeAnomalyHistogram(view.getTrace(), idAtt, uniqueness);
        ChartView.showChart(chart, "Behavior histogram", "Behavior histogram");
    }
}
