/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.verify;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.trace4cps.tl.VerificationResult;
import org.eclipse.trace4cps.ui.view.TraceView;

public final class FileNode {
    private final File traceFile;

    private final TraceView view;

    private List<SpecNode> specs = new ArrayList<>();

    public FileNode(File f, TraceView v) {
        traceFile = f;
        view = v;
    }

    public File getTraceFile() {
        return traceFile;
    }

    public TraceView getView() {
        return view;
    }

    public List<SpecNode> getSpecs() {
        return specs;
    }

    public void add(String specPath, VerificationResult res) {
        File file = new File(specPath);
        SpecNode n = null;
        for (SpecNode sn: specs) {
            if (sn.getSpecFile().equals(file)) {
                n = sn;
                break;
            }
        }
        if (n == null) {
            n = new SpecNode(file, view);
            specs.add(n);
        }
        n.add(res);
    }

    boolean refresh() {
        List<SpecNode> toRemoveSn = new ArrayList<>();
        for (SpecNode sn: specs) {
            if (sn.refreshSpecNode(traceFile.lastModified())) {
                toRemoveSn.add(sn);
            }
        }
        specs.removeAll(toRemoveSn);
        return specs.isEmpty();
    }
}
