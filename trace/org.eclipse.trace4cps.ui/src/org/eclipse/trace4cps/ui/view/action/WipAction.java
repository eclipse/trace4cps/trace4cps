/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import java.util.concurrent.TimeUnit;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.trace4cps.analysis.signal.SignalModifier;
import org.eclipse.trace4cps.analysis.signal.SignalUtil;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.ui.dialog.LittlesLawDialog;
import org.eclipse.trace4cps.ui.dialog.LittlesLawDialog.LlDialogType;
import org.eclipse.trace4cps.ui.view.TraceView;

public class WipAction extends AbstractTraceViewAction {
    private static final TimeUnit WINDOW_TIMEUNIT = TimeUnit.SECONDS;

    public WipAction(TraceView view) {
        super(view);
        setText("Identifier-based wip");
    }

    @Override
    public boolean isEnabled() {
        return view.getNumTraces() == 1 && view.hasClaims(true);
    }

    @Override
    protected void doRun() throws TraceException {
        LittlesLawDialog dialog = new LittlesLawDialog(view.getEditorSite().getShell(), view, LlDialogType.WIP);
        if (dialog.open() == Dialog.OK) {
            IPsop p = SignalUtil.getWip(view.getTrace(), dialog.getIdAtt(),
                    new SignalModifier(1d, dialog.getPositiveDoubleValue(), WINDOW_TIMEUNIT));
            p.setAttribute("name", "wip");
            p.setAttribute("id attribute", dialog.getIdAtt());
            p.setAttribute("convolution width",
                    Double.toString(dialog.getPositiveDoubleValue()) + " " + WINDOW_TIMEUNIT);
            view.addExtension(p, 0);
            view.update();
        }
    }
}
