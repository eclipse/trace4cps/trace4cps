/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view;

import java.util.Collection;
import java.util.function.Supplier;

import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IDependency;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.IPsopFragment;
import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.core.impl.TraceHelper;
import org.eclipse.trace4cps.vis.jfree.TraceViewConfiguration;
import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.data.xy.XYDataset;

/**
 * Works together with the {@link EclipseDataItemFactory}.
 */
public class EclipseToolTipGenerator implements XYToolTipGenerator {
    private final Supplier<TraceViewConfiguration> viewConfigSupplier;

    public EclipseToolTipGenerator(Supplier<TraceViewConfiguration> viewConfigSupplier) {
        this.viewConfigSupplier = viewConfigSupplier;
    }

    private Collection<String> getDescribingAttributes(TracePart part) {
        return viewConfigSupplier.get().getDescribingAttributes(part);
    }

    @Override
    public String generateToolTip(XYDataset ds, int series, int item) {
        EventDataItem dataItem = EventDataItem.getFrom(ds, series, item);
        if (dataItem != null) {
            IEvent e = (IEvent)dataItem.getBackReference().getSelectedTraceItem();
            return TraceHelper.getValues(e, getDescribingAttributes(TracePart.EVENT), false);
        }
        DependencyDataItem ddataItem = DependencyDataItem.getFrom(ds, series, item);
        if (ddataItem != null) {
            IDependency d = (IDependency)ddataItem.getBackReference().getSelectedTraceItem();
            return TraceHelper.getValues(d, getDescribingAttributes(TracePart.DEPENDENCY), false);
        }
        ClaimDataItem cdataItem = ClaimDataItem.getFrom(ds, series, item);
        if (cdataItem != null) {
            IClaim c = (IClaim)cdataItem.getBackReference().getSelectedTraceItem();
            return TraceHelper.getValues(c, getDescribingAttributes(TracePart.CLAIM), false);
        }
        SignalDataItem sdi = SignalDataItem.getFrom(ds, series, item);
        if (sdi != null) {
            IPsopFragment f = (IPsopFragment)sdi.getBackReference().getSelectedTraceItem();
            return "Signal fragment: " + f.dom() + " : " + f.getC() + ", " + f.getB() + ", " + f.getA() + " ("
                    + f.getShape() + ")";
        }
        return ds + " " + series + " " + item;
    }
}
