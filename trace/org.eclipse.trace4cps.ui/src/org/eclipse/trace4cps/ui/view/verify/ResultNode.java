/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.verify;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.trace4cps.analysis.mtl.InformativePrefix;
import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.tl.VerificationResult;
import org.eclipse.trace4cps.ui.view.TraceView;

public class ResultNode {
    private final TraceView view;

    private final InformativePrefix type;

    private final List<CheckNode> checks = new ArrayList<>();

    public ResultNode(InformativePrefix type, TraceView view) {
        this.type = type;
        this.view = view;
    }

    public InformativePrefix getType() {
        return type;
    }

    public List<CheckNode> getChecks() {
        return checks;
    }

    public void add(VerificationResult res) {
        long timeStamp = System.currentTimeMillis();
        boolean updated = false;
        for (MtlFormula phi: res.getChecks()) {
            if (res.getResult(phi).informative() == type) {
                String name = res.getName(phi);
                CheckNode n = getOrCreateNode(name);
                n.set(timeStamp, phi, res);
                updated = true;
            }
        }
        if (updated) {
            Collections.sort(checks, new Comparator<CheckNode>() {
                @Override
                public int compare(CheckNode o1, CheckNode o2) {
                    return o1.getCheckName().compareTo(o2.getCheckName());
                }
            });
        }
    }

    private CheckNode getOrCreateNode(String name) {
        CheckNode n = null;
        for (CheckNode cn: checks) {
            if (cn.getCheckName().equals(name)) {
                n = cn;
                break;
            }
        }
        if (n == null) {
            n = new CheckNode(view);
            checks.add(n);
        }
        return n;
    }

    public boolean refresh(long lastModified) {
        List<CheckNode> toRemoveCn = new ArrayList<>();
        for (CheckNode cn: checks) {
            if (cn.getCheckTime() <= lastModified) {
                toRemoveCn.add(cn);
            }
        }
        checks.removeAll(toRemoveCn);
        return checks.isEmpty();
    }
}
