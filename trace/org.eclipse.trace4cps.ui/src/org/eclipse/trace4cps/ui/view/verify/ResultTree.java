/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.verify;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.trace4cps.tl.VerificationResult;
import org.eclipse.trace4cps.ui.view.TraceView;
import org.eclipse.ui.IWorkbenchPart;

/**
 * A result tree has three levels:
 * <ol>
 * <li>A number of {@link FileNode} instances for the trace files that have been checked.</li>
 * <li>Each {@link FileNode} has a number of {@link SpecNode} instances, each representing a specification file that has
 * been used on the parent trace file.</li>
 * <li>Each {@link SpecNode} has a number of {@link CheckNode} instances, each representing a check in the parent
 * specification file.</li>
 * </ol>
 */
public final class ResultTree {
    List<FileNode> traces = new ArrayList<>();

    public List<FileNode> getTraces() {
        return traces;
    }

    public void add(String tracePath, String specPath, VerificationResult res, TraceView view) {
        File traceFile = new File(tracePath);
        FileNode n = null;
        for (FileNode fn: traces) {
            if (fn.getTraceFile().equals(traceFile)) {
                n = fn;
                break;
            }
        }
        if (n == null) {
            n = new FileNode(traceFile, view);
            traces.add(n);
        }
        n.add(specPath, res);
    }

    /**
     * Ensures that the timestamps of checks are larger than the timestamps of the spec and the trace files.
     */
    public void refresh() {
        List<FileNode> toRemoveFn = new ArrayList<>();
        for (FileNode fn: traces) {
            if (fn.refresh()) {
                toRemoveFn.add(fn);
            }
        }
        traces.removeAll(toRemoveFn);
    }

    public void partClosed(IWorkbenchPart part) {
        List<FileNode> toRemoveFn = new ArrayList<>();
        for (FileNode fn: traces) {
            if (fn.getView() == part) {
                toRemoveFn.add(fn);
            }
        }
        traces.removeAll(toRemoveFn);
    }
}
