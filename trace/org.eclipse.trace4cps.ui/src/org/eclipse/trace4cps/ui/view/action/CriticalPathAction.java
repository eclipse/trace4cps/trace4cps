/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.trace4cps.analysis.constraintgraph.ConstraintConfig;
import org.eclipse.trace4cps.analysis.cpa.CpaResult;
import org.eclipse.trace4cps.analysis.cpa.CriticalPathAnalysis;
import org.eclipse.trace4cps.analysis.cpa.DependencyProvider;
import org.eclipse.trace4cps.core.IDependency;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.ui.AnalysisUtil;
import org.eclipse.trace4cps.ui.dialog.CPAdialog;
import org.eclipse.trace4cps.ui.view.TraceView;

public class CriticalPathAction extends AbstractTraceViewAction {
    public CriticalPathAction(TraceView view) {
        super(view, "/icons/cpa.png");
        setToolTipText("Critical-path analysis");
    }

    @Override
    public boolean isEnabled() {
        return view.getNumTraces() == 1 && view.hasEvents(true, true);
    }

    @Override
    protected void doRun() throws TraceException {
        CPAdialog dialog = new CPAdialog(view.getEditorSite().getShell());
        if (dialog.open() == IStatus.OK) {
            DependencyProvider p = null;
            if (dialog.useDependencies() == CPAdialog.DependencyUse.APP) {
                p = new Provider(view.getTrace());
            }

            ConstraintConfig config = ConstraintConfig.getDefault();
            config.setAddSourceAndSink(true);
            config.setAddClaimDurations(true);
            if (dialog.useDependencies() != CPAdialog.DependencyUse.FULL) {
                config.setApplyOrderingHeuristic(dialog.getEpsilon());
            } else {
                config.setUseDependencies(true);
            }

            CpaResult cpaResult = CriticalPathAnalysis.run(view.getTrace(), config);
            viewCfg.setHighlightMap(AnalysisUtil.createHighlight(cpaResult, p));
            view.clearExtensions(TracePart.DEPENDENCY);
            view.addDependencyExtension(cpaResult.getCriticalDeps(), 0);
            view.update();
        }
    }

    private static final class Provider implements DependencyProvider {
        private final Map<IEvent, Set<IEvent>> succs = new HashMap<>();

        public Provider(ITrace trace) {
            for (IDependency dep: trace.getDependencies()) {
                IEvent src = dep.getSrc();
                IEvent dst = dep.getDst();
                Set<IEvent> events = succs.get(src);
                if (events == null) {
                    events = new HashSet<>();
                    succs.put(src, events);
                }
                events.add(dst);
            }
        }

        @Override
        public boolean isApplicationDependency(IEvent src, IEvent dst) {
            Set<IEvent> s = succs.get(src);
            if (s != null) {
                return s.contains(dst);
            }
            return false;
        }
    }
}
