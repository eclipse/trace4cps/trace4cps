/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.ui.view.TraceView;

public class ClearExtensionAction extends AbstractTraceViewAction {
    private final TracePart part;

    public ClearExtensionAction(TraceView view, TracePart part) {
        super(view);
        this.part = part;
        setText("Clear " + part.toString().toLowerCase() + " extensions");
    }

    @Override
    public boolean isEnabled() {
        return view.hasExtension(part);
    }

    @Override
    protected void doRun() throws TraceException {
        view.clearExtensions(part);
        view.update();
    }
}
