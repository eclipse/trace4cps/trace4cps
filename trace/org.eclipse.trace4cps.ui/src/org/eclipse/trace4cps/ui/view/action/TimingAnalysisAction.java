/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.trace4cps.analysis.timing.StatisticsManager;
import org.eclipse.trace4cps.analysis.timing.TimingAnalysis;
import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.ui.view.TraceView;
import org.eclipse.trace4cps.ui.view.timing.TimingAnalysisView;

public class TimingAnalysisAction extends AbstractTraceViewAction {
    public TimingAnalysisAction(TraceView view) {
        super(view, "/icons/graph.png");
        setToolTipText("Timing analysis");
    }

    @Override
    public boolean isEnabled() {
        return view.hasClaims(true);
    }

    @Override
    protected void doRun() throws TraceException {
        Map<String, Map<String, StatisticsManager>> timingData = new HashMap<>();
        for (int i = 0; i < view.getNumTraces(); i++) {
            Map<String, StatisticsManager> timing = TimingAnalysis.analyse(view.getTrace(i),
                    viewCfg.getGroupingAttributes(TracePart.CLAIM));
            timingData.put(view.getTraceFile(i).getName(), timing);
        }
        TimingAnalysisView.openView(timingData);
    }
}
