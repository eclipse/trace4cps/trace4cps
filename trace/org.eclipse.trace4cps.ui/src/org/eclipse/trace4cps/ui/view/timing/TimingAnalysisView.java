/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.timing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.trace4cps.analysis.timing.StatisticsManager;
import org.eclipse.trace4cps.ui.view.ChartView;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.statistics.HistogramType;

public class TimingAnalysisView extends ViewPart implements IDoubleClickListener {
    public static final String VIEW_ID = "org.eclipse.trace4cps.ui.view.timing.TimingAnalysisView";

    private TreeViewer viewer;

    private final List<RootNode> rootNodes = new ArrayList<>();

    public static void openView(Map<String, Map<String, StatisticsManager>> timingData) {
        final IWorkbench workbench = PlatformUI.getWorkbench();
        workbench.getDisplay().asyncExec(new Runnable() {
            @Override
            public void run() {
                try {
                    IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
                    IViewPart view = page.showView(TimingAnalysisView.VIEW_ID);
                    TimingAnalysisView tav = (TimingAnalysisView)view;
                    tav.clearResult();
                    for (Map.Entry<String, Map<String, StatisticsManager>> e: timingData.entrySet()) {
                        tav.addResult(e.getKey(), e.getValue());
                    }
                } catch (CoreException e) {
                    ErrorDialog.openError(null, "ETL Verification Error", "Failed to update result view",
                            e.getStatus());
                }
            }
        });
    }

    private void clearResult() {
        rootNodes.clear();
        viewer.setInput(new Object());
        viewer.refresh();
        viewer.expandToLevel(2);
    }

    private void addResult(String rootLabel, Map<String, StatisticsManager> timing) {
        rootNodes.add(new RootNode(rootLabel, timing));
        viewer.refresh();
        viewer.expandToLevel(2);
    }

    @Override
    public void createPartControl(Composite c) {
        GridLayoutFactory.swtDefaults().numColumns(1).applyTo(c);
        viewer = new TreeViewer(c, SWT.SINGLE);
        GridDataFactory.swtDefaults().applyTo(viewer.getTree());
        GridDataFactory.fillDefaults().grab(true, true).applyTo(viewer.getTree());
        viewer.setContentProvider(new TreeContentProvider());
        viewer.setLabelProvider(new TreeLabelProvider());
        viewer.addDoubleClickListener(this);
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    @Override
    public void setFocus() {
    }

    @Override
    public void doubleClick(DoubleClickEvent event) {
        IStructuredSelection selection = (IStructuredSelection)event.getSelection();
        if (selection == null || selection.isEmpty()) {
            return;
        }
        Object sel = selection.getFirstElement();
        if (sel instanceof SectionNode) {
            if (rootNodes.size() == 1) {
                createHistogram(((SectionNode)sel).getLabel(), ((SectionNode)sel).getMgr());
            } else {
                createMultiHistogram(((SectionNode)sel).getLabel());
            }
        }
    }

    private void createHistogram(String key, StatisticsManager mgr) {
        HistogramDataset dataset = new HistogramDataset();
        dataset.setType(HistogramType.RELATIVE_FREQUENCY);
        List<Double> timingData = mgr.getSamples();
        double[] data = timingData.stream().mapToDouble(Double::doubleValue).toArray();
        int bins = (int)(Math.ceil(Math.sqrt(data.length)));
        dataset.addSeries(key, data, bins);

        PlotOrientation orientation = PlotOrientation.VERTICAL;
        String xLabel = "Time (" + mgr.getTimeUnit().toString().toLowerCase() + ")";
        JFreeChart hisChart = ChartFactory.createHistogram(key, xLabel, "Relative frequency", dataset, orientation,
                false, true, false);
        hisChart.getXYPlot().setForegroundAlpha(0.5f);
        ChartView.showChart(hisChart, "Timing - " + key, "");
    }

    private void createMultiHistogram(String key) {
        Map<RootNode, SectionNode> histos = new HashMap<>();
        for (RootNode r: rootNodes) {
            for (SectionNode s: r.getChildren()) {
                if (key.equals(s.getLabel())) {
                    histos.put(r, s);
                    break; // continue with the next rootnode
                }
            }
        }
        HistogramDataset dataset = new HistogramDataset();
        dataset.setType(HistogramType.RELATIVE_FREQUENCY);
        TimeUnit theTimeUnit = null;
        for (Map.Entry<RootNode, SectionNode> e: histos.entrySet()) {
            String datasetKey = e.getKey().getLabel();
            StatisticsManager mgr = e.getValue().getMgr();
            if (theTimeUnit == null) {
                theTimeUnit = mgr.getTimeUnit();
            } else if (theTimeUnit != mgr.getTimeUnit()) {
                mgr.scaleTo(theTimeUnit);
            }
            double[] data = mgr.getSamples().stream().mapToDouble(Double::doubleValue).toArray();
            int bins = (int)(Math.ceil(Math.sqrt(data.length)));
            dataset.addSeries(datasetKey, data, bins);
        }
        PlotOrientation orientation = PlotOrientation.VERTICAL;
        String xLabel = "Time (" + theTimeUnit.toString().toLowerCase() + ")";
        JFreeChart hisChart = ChartFactory.createHistogram(key, xLabel, "Relative frequency", dataset, orientation,
                true, true, false);
        hisChart.getXYPlot().setForegroundAlpha(0.5f);
        ChartView.showChart(hisChart, "Timing - " + key, "");
    }

    private final class TreeContentProvider implements ITreeContentProvider {
        @Override
        public void dispose() {
        }

        @Override
        public void inputChanged(Viewer arg0, Object arg1, Object arg2) {
        }

        @Override
        public Object[] getChildren(Object o) {
            if (o instanceof RootNode) {
                return ((RootNode)o).getChildren().toArray();
            } else if (o instanceof SectionNode) {
                return ((SectionNode)o).getChildren().toArray();
            } else if (o instanceof LabeledNode) {
                return ((LabeledNode)o).getChildren().toArray();
            }
            return new Object[0];
        }

        @Override
        public Object[] getElements(Object input) {
            return rootNodes.toArray();
        }

        @Override
        public Object getParent(Object o) {
            if (o instanceof SectionNode) {
                return ((SectionNode)o).getParent();
            } else if (o instanceof LabeledNode) {
                return ((LabeledNode)o).getParent();
            }
            return null;
        }

        @Override
        public boolean hasChildren(Object o) {
            if (o instanceof RootNode) {
                return !((RootNode)o).getChildren().isEmpty();
            } else if (o instanceof SectionNode) {
                return !((SectionNode)o).getChildren().isEmpty();
            } else if (o instanceof LabeledNode) {
                return !((LabeledNode)o).getChildren().isEmpty();
            }
            return false;
        }
    }

    private final class TreeLabelProvider implements ILabelProvider {
        @Override
        public void addListener(ILabelProviderListener arg0) {
        }

        @Override
        public void dispose() {
        }

        @Override
        public boolean isLabelProperty(Object arg0, String arg1) {
            return false;
        }

        @Override
        public void removeListener(ILabelProviderListener arg0) {
        }

        @Override
        public Image getImage(Object o) {
            return null;
        }

        @Override
        public String getText(Object o) {
            if (o instanceof RootNode) {
                return ((RootNode)o).getLabel();
            } else if (o instanceof SectionNode) {
                return ((SectionNode)o).getLabel();
            } else if (o instanceof LabeledNode) {
                return ((LabeledNode)o).getLabel();
            }
            return o.toString();
        }
    }
}
