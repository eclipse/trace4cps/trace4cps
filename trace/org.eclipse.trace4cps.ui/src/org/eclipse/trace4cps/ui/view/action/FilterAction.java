/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.ui.dialog.FilterWizard;
import org.eclipse.trace4cps.ui.view.TraceView;

public class FilterAction extends AbstractTraceViewAction {
    private final TracePart part;

    public FilterAction(TraceView view, TracePart part) {
        super(view);
        this.part = part;
        setText("Filter " + part.toString().toLowerCase());
    }

    @Override
    public boolean isEnabled() {
        switch (part) {
            case CLAIM:
                return view.hasClaims(true);
            case RESOURCE:
                return view.hasClaims(true);
            case EVENT:
                return view.hasEvents(false, true);
            case DEPENDENCY:
                return view.hasDependencies(true);
            case SIGNAL:
                return view.hasSignals(true);
            default:
                throw new IllegalStateException();
        }
    }

    @Override
    protected void doRun() throws TraceException {
        FilterWizard wizard = new FilterWizard(part, view);
        if (new WizardDialog(view.getEditorSite().getShell(), wizard).open() == Dialog.CANCEL) {
            return; // filters not updated
        }
        view.update();
    }
}
