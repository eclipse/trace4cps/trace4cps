/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view;

import org.eclipse.trace4cps.common.jfreechart.data.xy.XYEdgeDataItem;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IDependency;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.IPsopFragment;
import org.eclipse.trace4cps.vis.jfree.DataItemFactory;
import org.jfree.data.xy.XYDataItem;
import org.jfree.data.xy.XYIntervalDataItem;

/**
 * Works together with the {@link EclipseToolTipGenerator}.
 */
public class EclipseDataItemFactory implements DataItemFactory {
    @Override
    public XYIntervalDataItem createClaimDataItem(IClaim claim, Number xmin, Number xmax, Number ymin, Number ymax) {
        return new ClaimDataItem(claim, xmin.doubleValue(), xmax.doubleValue(), ymin.doubleValue(), ymax.doubleValue());
    }

    @Override
    public XYEdgeDataItem createDependencyDataItem(IDependency dep, Number x0, Number y0, Number x1, Number y1) {
        return new DependencyDataItem(dep, x0, y0, x1, y1);
    }

    @Override
    public XYEdgeDataItem createEventDataItem(IEvent event, Number x, Number y0, Number y1) {
        return new EventDataItem(event, x, y0, y1);
    }

    @Override
    public XYDataItem createSignalDataItem(IPsop p, IPsopFragment f, Number x, Number y) {
        return new SignalDataItem(x.doubleValue(), y.doubleValue(), p, f);
    }
}
