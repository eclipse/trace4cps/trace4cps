/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.verify;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.mtl.MtlResult;
import org.eclipse.trace4cps.tl.VerificationResult;
import org.eclipse.trace4cps.ui.view.TraceView;

public class CheckNode {
    private final TraceView view;

    private long checkTime;

    private VerificationResult result;

    private MtlFormula phi;

    private String name;

    private boolean isParameterized;

    private List<ParameterizedCheckNode> subChecks = new ArrayList<>();

    public CheckNode(TraceView v) {
        view = v;
    }

    public TraceView getTraceView() {
        return view;
    }

    public boolean isParameterized() {
        return isParameterized;
    }

    public String getCheckName() {
        return name;
    }

    public long getCheckTime() {
        return checkTime;
    }

    public MtlFormula getFormula() {
        if (isParameterized()) {
            throw new IllegalStateException();
        }
        return phi;
    }

    public List<ParameterizedCheckNode> getSubChecks() {
        if (!isParameterized()) {
            throw new IllegalStateException();
        }
        return subChecks;
    }

    public MtlResult getMTLresult() {
        if (isParameterized()) {
            throw new IllegalStateException();
        }
        return result.getResult(phi);
    }

    public VerificationResult getResult() {
        if (isParameterized()) {
            throw new IllegalStateException();
        }
        return result;
    }

    public void set(long timeStamp, MtlFormula phi, VerificationResult res) {
        checkTime = timeStamp;
        name = res.getName(phi);
        isParameterized = res.isQuantifiedCheck(phi);
        if (!res.isQuantifiedCheck(phi)) {
            this.phi = phi;
            result = res;
        } else {
            subChecks.add(new ParameterizedCheckNode(phi, res, view));
            Collections.sort(subChecks, new Comparator<ParameterizedCheckNode>() {
                @Override
                public int compare(ParameterizedCheckNode o1, ParameterizedCheckNode o2) {
                    return o1.getCheckName().compareTo(o2.getCheckName());
                }
            });
        }
    }
}
