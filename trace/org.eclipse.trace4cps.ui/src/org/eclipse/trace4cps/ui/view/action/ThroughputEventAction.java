/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.trace4cps.analysis.signal.SignalUtil;
import org.eclipse.trace4cps.core.ClaimEventType;
import org.eclipse.trace4cps.core.IClaimEvent;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.core.impl.TraceHelper;
import org.eclipse.trace4cps.ui.dialog.LittlesLawDialog;
import org.eclipse.trace4cps.ui.dialog.LittlesLawDialog.LlDialogType;
import org.eclipse.trace4cps.ui.view.TraceView;

public class ThroughputEventAction extends AbstractTraceViewAction {
    private static final TimeUnit WINDOW_TIMEUNIT = TimeUnit.SECONDS;

    public ThroughputEventAction(TraceView view) {
        super(view);
        setText("Event-based throughput");
    }

    @Override
    public boolean isEnabled() {
        return view.getNumTraces() == 1 && view.hasEvents(true, true);
    }

    @Override
    protected void doRun() throws TraceException {
        LittlesLawDialog dialog = new LittlesLawDialog(view.getEditorSite().getShell(), view,
                LlDialogType.THROUGHPUT_EVENT);
        if (dialog.open() == Dialog.OK) {
            ITrace trace = view.getTrace();
            Map<String, String> filter = dialog.getFilter();
            List<IEvent> events = new ArrayList<>();
            for (IEvent e: trace.getEvents()) {
                if (matches(e, dialog.getEventType()) && TraceHelper.matches(filter, e.getAttributes())) {
                    events.add(e);
                }
            }
            if (events.size() == 0) {
                throw new TraceException("Filter results in empty event list");
            }

            IPsop p = SignalUtil.getTP(trace.getTimeUnit(), TraceHelper.getDomain(trace), events, dialog.getTimeUnit(),
                    dialog.getPositiveDoubleValue(), WINDOW_TIMEUNIT);
            p.setAttribute("name", "throughput");
            p.setAttribute("type", "event-based");
            p.setAttribute("time unit", dialog.getTimeUnit().toString());
            p.setAttribute("convolution width",
                    Double.toString(dialog.getPositiveDoubleValue()) + " " + WINDOW_TIMEUNIT);
            view.addExtension(p, 0);
            view.update();
        }
    }

    private boolean matches(IEvent e, ClaimEventType t) {
        if (t == null) {
            return true;
        }
        return (e instanceof IClaimEvent) && ((IClaimEvent)e).getType() == t;
    }
}
