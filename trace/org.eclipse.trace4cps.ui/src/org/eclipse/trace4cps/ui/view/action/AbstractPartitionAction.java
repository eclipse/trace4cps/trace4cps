/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.ui.view.TraceView;
import org.eclipse.ui.dialogs.ListSelectionDialog;

public abstract class AbstractPartitionAction extends AbstractTraceViewAction {
    AbstractPartitionAction(TraceView view) {
        super(view);
    }

    AbstractPartitionAction(TraceView view, String imgPath) {
        super(view, imgPath);
    }

    @Override
    protected void doRun() throws TraceException {
        List<String> input = new ArrayList<>();
        input.addAll(view.getAttributeNames(getPart()));
        Collections.sort(input);

        List<String> selection = new ArrayList<>();
        Collection<String> selectedAtts = getSelectedAttributes();
        if (selectedAtts != null) {
            selection.addAll(selectedAtts);
        }

        ListSelectionDialog dialog = ListSelectionDialog.of(input)
                .contentProvider(new ArrayContentProvider())
                .labelProvider(new LabelProvider())
                .message(getDialogMessage())
                .create(view.getEditorSite().getShell());
        dialog.setInitialElementSelections(selection);
        dialog.open();
        if (dialog.getResult() != null) {
            List<String> newSelection = new ArrayList<>();
            for (Object o: dialog.getResult()) {
                newSelection.add(o.toString());
            }
            updateSelection(newSelection);
            view.update();
        }
    }

    protected abstract TracePart getPart();

    protected abstract String getDialogMessage();

    protected abstract Collection<String> getSelectedAttributes() throws TraceException;

    protected abstract void updateSelection(Collection<String> newSelection) throws TraceException;
}
