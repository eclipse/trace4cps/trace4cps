/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.ui.view.TraceView;

public class ToggleViewAction extends AbstractTraceViewAction {
    public ToggleViewAction(TraceView view) {
        super(view, "/icons/synced.png");
    }

    @Override
    public String getToolTipText() {
        if (viewCfg.isActivityView()) {
            return "Switch to resource view";
        } else {
            return "Switch to activity view";
        }
    }

    @Override
    public boolean isEnabled() {
        return view.hasClaims(true);
    }

    @Override
    protected void doRun() throws TraceException {
        if (viewCfg.isActivityView()) {
            viewCfg.setResourceView();
        } else {
            viewCfg.setActivityView();
        }
        view.update();
    }
}
