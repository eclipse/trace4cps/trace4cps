/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.verify;

import java.text.DecimalFormat;
import java.text.Format;

import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.mtl.MtlResult;
import org.eclipse.trace4cps.tl.VerificationResult;
import org.eclipse.trace4cps.ui.view.TraceView;

public class ParameterizedCheckNode {
    private static final Format FORMAT = new DecimalFormat("0000");

    private final TraceView view;

    private final MtlFormula phi;

    private final VerificationResult res;

    private final String name;

    public ParameterizedCheckNode(MtlFormula phi, VerificationResult res, TraceView v) {
        this.phi = phi;
        this.res = res;
        this.view = v;
        this.name = res.getName(phi) + "(" + FORMAT.format(res.getQuantifierValue(phi).intValue()) + ")";
    }

    public TraceView getTraceView() {
        return view;
    }

    public String getCheckName() {
        return name;
    }

    public MtlFormula getFormula() {
        return phi;
    }

    public MtlResult getMTLresult() {
        return res.getResult(phi);
    }

    public VerificationResult getResult() {
        return res;
    }
}
