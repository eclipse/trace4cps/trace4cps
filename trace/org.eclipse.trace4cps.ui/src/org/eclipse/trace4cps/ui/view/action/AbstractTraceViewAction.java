/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.action;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.ui.view.TraceView;
import org.eclipse.trace4cps.vis.jfree.TraceViewConfiguration;

public abstract class AbstractTraceViewAction extends Action {
    private final String imgPath;

    final TraceView view;

    final TraceViewConfiguration viewCfg;

    AbstractTraceViewAction(TraceView view) {
        this(view, null);
    }

    AbstractTraceViewAction(TraceView view, String imgPath) {
        this.view = view;
        this.imgPath = imgPath;
        this.viewCfg = view.getViewConfiguration();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ImageDescriptor getImageDescriptor() {
        if (imgPath != null) {
            return ImageDescriptor.createFromURL(getClass().getResource(imgPath));
        }
        return super.getImageDescriptor();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run() {
        try {
            doRun();
        } catch (TraceException e) {
            IStatus status = new Status(IStatus.ERROR, getClass(), e.getMessage(), e);
            ErrorDialog.openError(null, "Error", "Failed to run action", status);
        }
    }

    protected abstract void doRun() throws TraceException;
}
