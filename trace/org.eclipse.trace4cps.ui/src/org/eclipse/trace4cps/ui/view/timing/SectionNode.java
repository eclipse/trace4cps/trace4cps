/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.view.timing;

import org.eclipse.trace4cps.analysis.timing.NormalStatistics;
import org.eclipse.trace4cps.analysis.timing.StatisticsManager;

public class SectionNode extends LabeledNode {
    private final StatisticsManager mgr;

    public SectionNode(RootNode parent, String label, StatisticsManager mgr) {
        super(parent, label);
        this.mgr = mgr;
        addChild(new LabeledNode(this, "n=" + mgr.getNumSamples()));
        // add the statistics data children
        LabeledNode normal = new LabeledNode(this, "normal distribution");
        NormalStatistics s = mgr.getNormalStatistics();
        normal.addChild(new LabeledNode(normal, "mean = " + s.getMean()));
        normal.addChild(new LabeledNode(normal, "sd = " + s.getSd()));
        addChild(normal);
    }

    public StatisticsManager getMgr() {
        return mgr;
    }
}
