/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui;

import java.awt.Paint;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.eclipse.trace4cps.analysis.AnalysisException;
import org.eclipse.trace4cps.analysis.behavior.BehavioralAnalysis;
import org.eclipse.trace4cps.analysis.behavior.BehavioralPartition;
import org.eclipse.trace4cps.analysis.behavior.HistogramEntry;
import org.eclipse.trace4cps.analysis.cpa.CpaResult;
import org.eclipse.trace4cps.analysis.cpa.DependencyProvider;
import org.eclipse.trace4cps.analysis.dist.DefaultRepresentation;
import org.eclipse.trace4cps.analysis.dist.DistanceAnalysis;
import org.eclipse.trace4cps.analysis.dist.DistanceResult;
import org.eclipse.trace4cps.analysis.mtl.AtomicProposition;
import org.eclipse.trace4cps.analysis.mtl.InformativePrefix;
import org.eclipse.trace4cps.analysis.mtl.MtlChecker;
import org.eclipse.trace4cps.analysis.mtl.MtlException;
import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.mtl.MtlFuture;
import org.eclipse.trace4cps.analysis.mtl.MtlResult;
import org.eclipse.trace4cps.analysis.mtl.MtlUtil;
import org.eclipse.trace4cps.analysis.resource.ResourceAnalysis;
import org.eclipse.trace4cps.analysis.signal.SignalModifier;
import org.eclipse.trace4cps.analysis.signal.SignalUtil;
import org.eclipse.trace4cps.analysis.signal.impl.PsopHelper;
import org.eclipse.trace4cps.core.IAttributeAware;
import org.eclipse.trace4cps.core.IAttributeFilter;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IClaimEvent;
import org.eclipse.trace4cps.core.IDependency;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.IInterval;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.IPsopFragment;
import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.impl.ClaimEvent;
import org.eclipse.trace4cps.core.impl.TraceHelper;
import org.eclipse.trace4cps.vis.jfree.ColorManager;
import org.eclipse.trace4cps.vis.jfree.TracePlotManager;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.CategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 * This is the top-level access point for analysis methods on execution traces. Results can typically be used with the
 * {@link TracePlotManager} for visualization, or are already {@code JFreeChart} instances.
 */
public class AnalysisUtil {
    private AnalysisUtil() {
    }

    public static Map<IAttributeAware, Paint> createAPhighlight(List<IEvent> states, List<MtlResult> results) {
        Map<IAttributeAware, Paint> highlightMap = new HashMap<>();
        for (MtlResult r: results) {
            if (InformativePrefix.BAD == r.informative()) {
                Set<AtomicProposition> aps = MtlUtil.getAtomicPropositions(r.getPhi());
                for (IEvent e: states) {
                    for (AtomicProposition ap: aps) {
                        if (e.satisfies(ap)) {
                            if (e instanceof IClaimEvent) {
                                highlightMap.put(((IClaimEvent)e).getClaim(), ColorManager.RED_PURE.getColor());
                            }
                        }
                    }
                }
            }
        }
        return highlightMap;
    }

    public static Map<IClaim, Paint> createAPhighlight2(List<IEvent> states, List<MtlResult> results) {
        // Create highlight:
        Map<IClaim, Paint> highlightMap = new HashMap<IClaim, Paint>();
        for (MtlResult r: results) {
            if (InformativePrefix.BAD == r.informative()) {
                // Add all claims that match the atomic propositions in this formula
                for (int i: MtlUtil.getAtomicPropIndices(r)) {
                    IEvent e = states.get(i);
                    if (e instanceof IClaimEvent) {
                        highlightMap.put(((IClaimEvent)e).getClaim(), ColorManager.RED_PURE.getColor());
                    }
                }
            }
        }
        return highlightMap;
    }

    public static Map<IAttributeAware, Paint> mtlCheck(ITrace trace, List<MtlFormula> phis, boolean interpretAsPrefix)
            throws MtlException, InterruptedException, ExecutionException
    {
        List<IEvent> states = trace.getEvents();
        MtlFuture f = new MtlChecker().checkAll(states, phis, Collections.emptySet(), interpretAsPrefix);
        List<MtlResult> results = f.get();
        return createAPhighlight(states, results);
    }

    public static Map<IAttributeAware, Paint> createHighlight(CpaResult r, DependencyProvider p) {
        Map<IAttributeAware, Paint> m = new HashMap<>();
        for (IDependency dep: r.getCriticalDeps()) {
            if (dep.getSrc() instanceof IClaimEvent) {
                // don't overwrite RED_1 highlight
                IClaim src = ((IClaimEvent)dep.getSrc()).getClaim();
                if (m.get(src) == null) {
                    m.put(src, ColorManager.DARK_RED.getColor());
                }
            }
            if (dep.getDst() instanceof IClaimEvent) {
                IClaim dst = ((IClaimEvent)dep.getDst()).getClaim();
                if (m.get(dst) == null) {
                    m.put(dst, ColorManager.DARK_RED.getColor());
                }
                // check for blocking on non-app dependency:
                // this is if src->dst is not application dependency
                // and src and dst don't have the same parent claim
                if (CpaResult.isNonAppDependency(dep, p)) {
                    m.put(dst, ColorManager.RED_PURE.getColor());
                }
            }
        }
        return m;
    }

    public static Map<IAttributeAware, Paint> computeDiffHighlights(ITrace referenceTrace, ITrace modelTrace) {
        Map<IAttributeAware, Paint> highlight = new HashMap<>();
        computeDiffHighlights(referenceTrace, modelTrace, true, highlight);
        return highlight;
    }

    public static Map<IAttributeAware, Paint> computeDiffHighlights(ITrace referenceTrace, ITrace... modelTraces) {
        Map<IAttributeAware, Paint> highlight = new HashMap<>();
        for (ITrace modelTrace: modelTraces) {
            computeDiffHighlights(referenceTrace, modelTrace, false, highlight);
        }
        return highlight;
    }

    public static long computeDiffHighlights(ITrace referenceTrace, ITrace modelTrace, boolean annotateReference,
            Map<IAttributeAware, Paint> highlight)
    {
        DistanceResult val = DistanceAnalysis.distance(referenceTrace, modelTrace, new DefaultRepresentation(),
                annotateReference);

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for (Integer v: val.getInterestCount().keySet()) {
            min = Math.min(min, v);
            max = Math.max(max, v);
        }
        // TODO: This new algorithm uses ten shades of red for distance analysis,
        // please test and check if documentation should be updated
        int range = max - min;
        for (Map.Entry<Integer, List<IEvent>> e: val.getInterestCount().entrySet()) {
            Integer level = e.getKey();
            float weight = (float)(level - min) / (float)range;
            ColorManager color = ColorManager.getShadeOfRed(weight);
            if (color != ColorManager.NON_HIGHLIGHT) {
                for (IEvent event: e.getValue()) {
                    if (event instanceof ClaimEvent) {
                        highlight.put(((ClaimEvent)event).getClaim(), color.getColor());
                    }
                }
            }
        }
        return val.getDistance();
    }

    public static IAttributeFilter computeBehavioralRepresentativeFilter(ITrace trace, String idAtt,
            Set<String> uniquenessAtts)
    {
        List<IEvent> events = trace.getEvents();
        BehavioralPartition part = BehavioralAnalysis.partition(events, idAtt, uniquenessAtts);
        return BehavioralAnalysis.computeBehavioralRepresentativeFilter(part);
    }

    public static Map<IAttributeAware, Paint> computeAnomalyHistogramHighlight(ITrace trace, String idAtt,
            Set<String> uniquenessAtts)
    {
        Map<IAttributeAware, Paint> h = new HashMap<>();
        List<IEvent> events = trace.getEvents();
        BehavioralPartition part = BehavioralAnalysis.partition(events, idAtt, uniquenessAtts);
        List<HistogramEntry> hist = BehavioralAnalysis.createBehavioralHistogram(part);
        Map<Integer, Paint> objectIds2Paint = new HashMap<Integer, Paint>();
        int i = 2;
        for (HistogramEntry he: hist) {
            for (Integer id: he.getCentralObjectIds()) {
                objectIds2Paint.put(id, ColorManager.getAutomaticColor(i).getColor());
            }
            i = i + 1;
        }
        for (IEvent event: events) {
            if (event instanceof IClaimEvent) {
                IClaim claim = ((IClaimEvent)event).getClaim();
                String idAttValue = claim.getAttributeValue(idAtt);
                Integer objectId = Integer.parseInt(idAttValue);
                h.put(claim, objectIds2Paint.get(objectId));
            }
        }
        return h;
    }

    public static JFreeChart computeAnomalyHistogram(ITrace trace, String idAtt, Set<String> uniquenessAtts) {
        Iterable<IEvent> claims = trace.getEvents();
        BehavioralPartition part = BehavioralAnalysis.partition(claims, idAtt, uniquenessAtts);
        List<HistogramEntry> hist = BehavioralAnalysis.createBehavioralHistogram(part);
        final List<String> tooltips = new ArrayList<String>();

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        int i = 0;
        for (HistogramEntry h: hist) {
            dataset.addValue(h.size(), "rowKey", "h" + i);
            tooltips.add(getToolTip(idAtt, h));
            i++;
        }
        JFreeChart chart = ChartFactory.createBarChart(null, "", "size", dataset, PlotOrientation.VERTICAL, false, true,
                false);
        CategoryPlot plot = (CategoryPlot)chart.getPlot();
        plot.setRangePannable(false);
        BarRenderer renderer = (BarRenderer)plot.getRenderer();
        renderer.setBarPainter(new StandardBarPainter());
        renderer.setDefaultToolTipGenerator(new CategoryToolTipGenerator() {
            @Override
            public String generateToolTip(CategoryDataset dataset, int r, int c) {
                return tooltips.get(c);
            }
        });
        renderer.setMaximumBarWidth(100d);
        renderer.setMinimumBarLength(0d);
        return chart;
    }

    private static String getToolTip(String idAtt, HistogramEntry e) {
        StringBuilder b = new StringBuilder("<html>" + idAtt + " = {");
        List<Integer> oids = new ArrayList<Integer>();
        oids.addAll(e.getCentralObjectIds());
        Collections.sort(oids);
        int i = 0;
        for (Iterator<Integer> it = oids.iterator(); it.hasNext();) {
            Integer oid = it.next();
            b.append(oid);
            if (it.hasNext()) {
                b.append(",");
            }
            i++;
            if (i % 10 == 0) {
                i = 0;
                b.append("<br/>");
            }
        }
        b.append("}<br/>");
        // add common values
        for (Map.Entry<String, String> entry: e.getCommonAttributeValues().entrySet()) {
            b.append(entry.getKey()).append(" = ").append(entry.getValue()).append("<br/>");
        }
        b.append("</html>");
        return b.toString();
    }

    public static JFreeChart createResourceUsageHistogramPlot(ITrace trace, List<IResource> selected,
            Collection<String> resourceDescriptionAtts, boolean cumulative)
    {
        Map<IResource, TreeMap<Integer, Double>> m = ResourceAnalysis.compute(trace, selected, cumulative);
        DefaultCategoryDataset dataset = createDataset(m, resourceDescriptionAtts);
        JFreeChart chart = ChartFactory.createBarChart(null, "# of concurrent claims on resource", "% of time", dataset,
                PlotOrientation.VERTICAL, true, true, false);
        CategoryPlot plot = (CategoryPlot)chart.getPlot();
        plot.setRangePannable(false);
        BarRenderer renderer = (BarRenderer)plot.getRenderer();
        renderer.setBarPainter(new StandardBarPainter());
        renderer.setMaximumBarWidth(100d);
        renderer.setMinimumBarLength(0d);
        return chart;
    }

    public static JFreeChart createResourceUtilizationPlot(ITrace trace, List<IResource> selected, boolean useAmount,
            SignalModifier mod, boolean alsoShowInst) throws AnalysisException
    {
        String xAxisLabel = "Time (" + getStringFor(trace.getTimeUnit(), false) + ")";
        String yAxisLabel = useAmount ? "Claimed amount" : "Number of parallel claims";
        JFreeChart chart = ChartFactory.createXYLineChart("", xAxisLabel, yAxisLabel, new XYSeriesCollection(),
                PlotOrientation.VERTICAL, true, true, false);

        Map<String, IPsop> usage = new HashMap<String, IPsop>();
        for (IResource r: selected) {
            String rName = TraceHelper.getValues(r, false);
            IPsop p;
            if (useAmount) {
                p = SignalUtil.getResourceAmount(trace, r, mod);
            } else {
                p = SignalUtil.getResourceClients(trace, r, mod);
            }
            String modSpec = "_inst";
            if (mod.appliesConvolution()) {
                modSpec = "[w=" + mod.getWindowWidth() + " " + getStringFor(mod.getWindowTimeUnit(), false) + "]";
            }
            usage.put(rName + modSpec, p);
            if (mod.appliesConvolution() && alsoShowInst) {
                IPsop inst;
                if (useAmount) {
                    inst = SignalUtil.getResourceAmount(trace, r, new SignalModifier(mod.getScale()));
                } else {
                    inst = SignalUtil.getResourceClients(trace, r, new SignalModifier(mod.getScale()));
                }
                usage.put(rName + "_inst", inst);
            }
        }
        ((XYPlot)chart.getPlot()).setDataset(createPsopDataset(usage));
        return chart;
    }

    private static XYDataset createPsopDataset(Map<String, IPsop> usage) {
        XYSeriesCollection dataset = new XYSeriesCollection();
        for (Map.Entry<String, IPsop> e: usage.entrySet()) {
            dataset.addSeries(createSeriesData(e.getKey(), e.getValue(), true, true));
        }
        return dataset;
    }

    private static XYSeries createSeriesData(String id, IPsop p, boolean addZeroStart, boolean addZeroEnd) {
        XYSeries series = new XYSeries(id);
        boolean first = true;
        for (Iterator<IPsopFragment> it = p.getFragments().iterator(); it.hasNext();) {
            IPsopFragment f = it.next();
            double t0 = f.dom().lb().doubleValue();
            double t1 = f.dom().ub().doubleValue();
            if (first && addZeroStart) {
                series.add(t0, 0d);
            }
            first = false;
            series.add(t0, PsopHelper.valueAt(f, t0));
            series.add(t1, PsopHelper.valueAt(f, t1));
            if (!it.hasNext() && addZeroEnd) {
                series.add(t1, 0d);
            }
        }
        return series;
    }

    private static DefaultCategoryDataset createDataset(Map<IResource, TreeMap<Integer, Double>> histos,
            Collection<String> resourceDescriptionAtts)
    {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (Map.Entry<IResource, TreeMap<Integer, Double>> e: histos.entrySet()) {
            TreeMap<Integer, Double> values = e.getValue();
            for (int i = 0; i <= values.lastKey(); i++) {
                Double d = values.get(i);
                if (d == null) {
                    d = 0d;
                }
                String name = TraceHelper.getValues(e.getKey(), resourceDescriptionAtts, false);
                dataset.addValue(d, name, Integer.valueOf(i));
            }
        }
        return dataset;
    }

    public static JFreeChart createLatencyPlot(ITrace trace, String idAttribute, TimeUnit scaleUnit, double windowWidth,
            TimeUnit windowUnit)
    {
        IPsop lat = SignalUtil.getLatency(trace, idAttribute, scaleUnit, windowWidth, windowUnit);
        String yAxisLabel = "Latency (" + getStringFor(scaleUnit, false) + ")";
        String xAxisLabel = "Time (" + getStringFor(trace.getTimeUnit(), false) + ")";
        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(createSeriesData("latency", lat, false, false));
        JFreeChart chart = ChartFactory.createXYLineChart("", xAxisLabel, yAxisLabel, dataset, PlotOrientation.VERTICAL,
                false, true, false);
        return chart;
    }

    public static JFreeChart createWipPlot(ITrace trace, String idAttribute, String valueUnit, SignalModifier mod) {
        IPsop pd = SignalUtil.getWip(trace, idAttribute, mod);
        String yAxisLabel = "Wip (number of " + valueUnit + ")";
        String xAxisLabel = "Time (" + getStringFor(trace.getTimeUnit(), false) + ")";
        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(createSeriesData("wip", pd, true, true));
        JFreeChart chart = ChartFactory.createXYLineChart("", xAxisLabel, yAxisLabel, dataset, PlotOrientation.VERTICAL,
                false, true, false);
        return chart;
    }

    public static JFreeChart createThroughputPlot(ITrace trace, String idAttribute, String valueUnit,
            TimeUnit scaleUnit, double windowWidth, TimeUnit windowUnit)
    {
        return createThroughputPlots(trace, idAttribute, valueUnit, scaleUnit, windowUnit, windowWidth);
    }

    public static JFreeChart createThroughputPlots(ITrace trace, String idAttribute, String valueUnit,
            TimeUnit scaleUnit, TimeUnit windowUnit, double... windowWidth)
    {
        // ensure that all mods have the same scale factor
        IPsop[] tps = new IPsop[windowWidth.length];
        String[] legendLabels = new String[windowWidth.length];
        for (int i = 0; i < windowWidth.length; i++) {
            tps[i] = SignalUtil.getTP(trace, idAttribute, scaleUnit, windowWidth[i], windowUnit);
            if (windowWidth[i] > 0) {
                legendLabels[i] = "tp[w=" + windowWidth[i] + " " + getStringFor(windowUnit, false) + "]";
            } else {
                legendLabels[i] = "tp_inst";
            }
        }
        return createTPplot(trace, idAttribute, valueUnit, scaleUnit, legendLabels, tps);
    }

    public static JFreeChart createChart(TimeUnit tu, String yAxisLabel, String[] legendLabels, IPsop... psops) {
        String xAxisLabel = "Time (" + getStringFor(tu, false) + ")";
        XYSeriesCollection dataset = new XYSeriesCollection();
        for (int i = 0; i < psops.length; i++) {
            dataset.addSeries(createSeriesData(legendLabels[i], psops[i], true, true));
        }
        JFreeChart chart = ChartFactory.createXYLineChart("", xAxisLabel, yAxisLabel, dataset, PlotOrientation.VERTICAL,
                true, true, false);
        return chart;
    }

    private static JFreeChart createTPplot(ITrace trace, String idAttribute, String valueUnit, TimeUnit scaleUnit,
            String[] legendLabels, IPsop[] tps)
    {
        if (valueUnit == null) {
            valueUnit = idAttribute + "s";
        }
        String yAxisLabel = "Throughput (" + valueUnit + " / " + getStringFor(scaleUnit, true) + ")";
        String xAxisLabel = "Time (" + getStringFor(trace.getTimeUnit(), false) + ")";

        XYSeriesCollection dataset = new XYSeriesCollection();
        for (int i = 0; i < tps.length; i++) {
            dataset.addSeries(createSeriesData(legendLabels[i], tps[i], true, true));
        }
        JFreeChart chart = ChartFactory.createXYLineChart("", xAxisLabel, yAxisLabel, dataset, PlotOrientation.VERTICAL,
                true, true, false);
        return chart;
    }

    public static JFreeChart createEventTPplot(TimeUnit traceTimeUnit, List<IEvent> events, IInterval dom,
            String valueUnit, TimeUnit scaleUnit, double windowWidth, TimeUnit windowUnit)
    {
        if (valueUnit == null) {
            valueUnit = "objects";
        }
        String yAxisLabel = "Throughput (" + valueUnit + " / " + getStringFor(scaleUnit, true) + ")";
        String xAxisLabel = "Time (" + getStringFor(traceTimeUnit, false) + ")";
        String legendLabel = "tp_inst";
        if (windowWidth > 0d) {
            legendLabel = "tp[w=" + windowWidth + " " + getStringFor(windowUnit, false) + "]";
        }

        IPsop tp = SignalUtil.getTP(traceTimeUnit, dom, events, scaleUnit, windowWidth, windowUnit);

        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(createSeriesData(legendLabel, tp, true, true));

        JFreeChart chart = ChartFactory.createXYLineChart("", xAxisLabel, yAxisLabel, dataset, PlotOrientation.VERTICAL,
                true, true, false);
        return chart;
    }

    private static String getStringFor(TimeUnit tu, boolean singular) {
        String t = tu.toString().toLowerCase();
        if (singular) {
            return t.substring(0, t.length() - 1);
        }
        return t;
    }
}
