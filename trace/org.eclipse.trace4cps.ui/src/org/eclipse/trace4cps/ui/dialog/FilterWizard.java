/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.dialog;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.ui.view.TraceView;
import org.eclipse.trace4cps.vis.jfree.TraceViewConfiguration.DisjunctionFilter;
import org.eclipse.trace4cps.vis.jfree.TraceViewConfiguration.SimpleFilter;

/**
 * A wizard with a number of pages. First page selects the attribute names for which we want to define a filter. A
 * wizard page is added for each of these attributes to select the value(s) that are included in the filter.
 */
public class FilterWizard extends Wizard {
    private final TraceView traceView;

    private final TracePart part;

    private final List<String> allAttributes = new ArrayList<>();

    private FilterAttributeSelectionWizardPage page1;

    private final Map<String, FilterAttributeValueSelectionWizardPage> idPageMap = new HashMap<>();

    public FilterWizard(TracePart part, TraceView view) {
        this.traceView = view;
        this.part = part;
        this.allAttributes.addAll(traceView.getAttributeNames(part));
        Collections.sort(this.allAttributes);
        setWindowTitle("Filter settings");
        setNeedsProgressMonitor(false);
        TrayDialog.setDialogHelpAvailable(true);
    }

    TracePart getPart() {
        return part;
    }

    TraceView getTraceView() {
        return traceView;
    }

    /**
     * @return the sorted list of attributes
     */
    List<String> getAllAttributes() {
        return allAttributes;
    }

    @Override
    public void addPages() {
        page1 = new FilterAttributeSelectionWizardPage(this);
        addPage(page1);
        // Add pages for all attributes we have; only the ones that are selected on the first page are
        // actually used.
        for (String att: allAttributes) {
            FilterAttributeValueSelectionWizardPage valueSelectionPage = new FilterAttributeValueSelectionWizardPage(
                    this, att);
            addPage(valueSelectionPage);
            idPageMap.put(att, valueSelectionPage);
        }
    }

    @Override
    public IWizardPage getNextPage(IWizardPage page) {
        List<String> selectedAtts = page1.getSelection();
        if (page instanceof FilterAttributeSelectionWizardPage) {
            if (selectedAtts.isEmpty()) {
                return null;
            } else {
                return idPageMap.get(selectedAtts.get(0)); // page of the first selected attribute
            }
        } else {
            FilterAttributeValueSelectionWizardPage p = (FilterAttributeValueSelectionWizardPage)page;
            String currentAtt = p.getAttribute();
            int index = selectedAtts.indexOf(currentAtt);
            if (index + 1 < selectedAtts.size()) {
                return idPageMap.get(selectedAtts.get(index + 1));
            } else {
                return null;
            }
        }
    }

    @Override
    public boolean canFinish() {
        // Check whether all selected attributes have at least a single value selected
        if (!page1.hasSelection()) {
            return false;
        }
        for (String att: page1.getSelection()) {
            FilterAttributeValueSelectionWizardPage page = idPageMap.get(att);
            if (!page.hasSelection()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean performFinish() {
        DisjunctionFilter filter = new DisjunctionFilter();
        for (String att: page1.getSelection()) {
            FilterAttributeValueSelectionWizardPage page = idPageMap.get(att);
            List<String> includedValues = page.getSelection();
            filter.add(new SimpleFilter(att, includedValues));
        }
        traceView.getViewConfiguration().addFilter(part, filter);
        return true;
    }

    private static final class FilterAttributeSelectionWizardPage extends AbstractListSelectionWizardPage {
        public FilterAttributeSelectionWizardPage(FilterWizard wizard) {
            super("FilterAttributeSelectionWizardPage", wizard, SelectionMode.AT_LEAST_ONE);
        }

        @Override
        protected String getPageTitle() {
            return "Attribute filter";
        }

        @Override
        protected String getPageMessage() {
            return "Select the attributes to filter on";
        }

        @Override
        protected Collection<String> getInput() {
            return ((FilterWizard)getWizard()).getAllAttributes();
        }
    }

    private static final class FilterAttributeValueSelectionWizardPage extends AbstractListSelectionWizardPage {
        private final String att;

        private final List<String> attValues;

        public FilterAttributeValueSelectionWizardPage(FilterWizard wizard, String att) {
            super("FilterAttributeValueSelectionWizardPage", wizard, SelectionMode.AT_LEAST_ONE);
            this.att = att;
            Set<String> values = ((FilterWizard)getWizard()).getTraceView().getAllAttributeValuesFor(att,
                    ((FilterWizard)getWizard()).getPart());
            attValues = new ArrayList<>();
            attValues.addAll(values);
            Collections.sort(attValues);
        }

        @Override
        protected String getPageTitle() {
            return "Value selection for: " + att;
        }

        @Override
        protected String getPageMessage() {
            return "Select the attribute values to include";
        }

        @Override
        protected Collection<String> getInput() {
            return attValues;
        }

        protected String getAttribute() {
            return att;
        }
    }
}
