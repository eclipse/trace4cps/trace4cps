/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.dialog;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.impl.TraceHelper;

public final class ResourceClientHistogramDialog extends ParameterDialog {
    private static final String TITLE = "Resource-usage analysis";

    private static final String CUMULATIVE_KEY = "CUMULATIVE";

    private static final String AMOUNT_KEY = "AMOUNT";

    private final boolean isHistoDialog;

    private final Map<String, IResource> resourceMap = new HashMap<>();

    public ResourceClientHistogramDialog(Shell parentShell, Collection<IResource> resources, boolean isHistoDialog) {
        super(parentShell, ResourceClientHistogramDialog.class.getName());
        this.isHistoDialog = isHistoDialog;
        for (IResource r: resources) {
            resourceMap.put(TraceHelper.getValues(r, true), r);
        }
    }

    public boolean cumulative() {
        return get(CUMULATIVE_KEY, Boolean.class);
    }

    public List<IResource> getSelected() {
        List<IResource> r = new ArrayList<>();
        for (String s: getSelection()) {
            r.add(resourceMap.get(s));
        }
        return r;
    }

    public boolean useAmount() {
        return get(AMOUNT_KEY, Boolean.class);
    }

    @Override
    protected String getTitle() {
        return TITLE;
    }

    @Override
    protected void draw(Composite composite) {
        Group gr = new Group(composite, SWT.SHADOW_IN);
        gr.setLayout(new GridLayout(2, true));
        GridData data = new GridData(SWT.FILL, SWT.FILL, true, false);
        gr.setLayoutData(data);
        if (isHistoDialog) {
            addCheckbox(gr, "Use cumulative values", CUMULATIVE_KEY, false);
        } else {
            addCheckbox(gr, "Use resource amount", AMOUNT_KEY, true);
            addPositiveDoubleInput(gr, true);
        }
        addSelectionInput(composite, "Select resources", SelectionType.AT_LEAST_ONE, resourceMap.keySet());
        applyDialogFont(composite);
    }
}
