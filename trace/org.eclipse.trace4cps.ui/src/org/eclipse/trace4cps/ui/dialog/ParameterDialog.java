/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.dialog;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * An abstract class for dialogs that ask parameters from the user.
 */
public abstract class ParameterDialog extends Dialog {
    enum SelectionType {
        EXACTLY_ONE, AT_LEAST_ONE
    }

    private static final Map<String, Map<String, Object>> SETTINGS = new HashMap<>();

    private static final Object LOCK = new Object();

    private final String settingsKey;

    private static final String POSITIVE_DOUBLE_KEY = "POSITIVE_DOUBLE";

    private Text positiveDoubleInput;

    private boolean positiveDoubleOk = true;

    private static final String SELECTION_KEY = "SELECTION";

    private SelectionType selectionType;

    private CheckboxTableViewer cbtv;

    private static final String TIMEUNIT_KEY = "TIMEUNIT";

    protected ParameterDialog(Shell parentShell, String settingsKey) {
        super(parentShell);
        this.settingsKey = settingsKey;
        synchronized (LOCK) {
            Map<String, Object> settings = SETTINGS.get(settingsKey);
            if (settings == null) {
                SETTINGS.put(settingsKey, new HashMap<>());
            }
        }
    }

    @Override
    protected final Control createDialogArea(Composite parent) {
        Composite composite = (Composite)super.createDialogArea(parent);
        composite.getShell().setText(getTitle());
        draw(composite);
        return composite;
    }

    protected abstract String getTitle();

    protected abstract void draw(Composite composite);

    protected void checkOkButton() {
        Control button = getButton(IDialogConstants.OK_ID);
        if (button != null) {
            boolean ok = checkSelection() && checkWindowWidth();
            button.setEnabled(ok && inputIsValid());
        }
    }

    protected boolean inputIsValid() {
        return true;
    }

    protected final boolean checkWindowWidth() {
        return (positiveDoubleInput == null || positiveDoubleOk);
    }

    protected final boolean checkSelection() {
        if (cbtv == null) {
            return true;
        }
        if (selectionType == SelectionType.EXACTLY_ONE) {
            return get(SELECTION_KEY, List.class).size() == 1;
        }
        if (selectionType == SelectionType.AT_LEAST_ONE) {
            return get(SELECTION_KEY, List.class).size() >= 1;
        }
        return false;
    }

    @Override
    protected final void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
        checkOkButton();
    }

    boolean isSet(String key) {
        synchronized (LOCK) {
            return SETTINGS.get(settingsKey).containsKey(key);
        }
    }

    void set(String key, Object value) {
        synchronized (LOCK) {
            SETTINGS.get(settingsKey).put(key, value);
        }
    }

    @SuppressWarnings("unchecked")
    <T> T get(String key, Class<T> clazz) {
        synchronized (LOCK) {
            return (T)SETTINGS.get(settingsKey).get(key);
        }
    }

    public double getPositiveDoubleValue() {
        return get(POSITIVE_DOUBLE_KEY, Double.class);
    }

    @SuppressWarnings("unchecked")
    public List<String> getSelection() {
        return get(SELECTION_KEY, List.class);
    }

    public TimeUnit getTimeUnit() {
        return get(TIMEUNIT_KEY, TimeUnit.class);
    }

    /**
     * Adds 2 elements
     */
    Button addCheckbox(Composite gr, String msg, String key, boolean initialValue) {
        if (!isSet(key)) {
            set(key, initialValue);
        }
        Label lb = new Label(gr, SWT.HORIZONTAL);
        lb.setText(msg);
        final Button cb = new Button(gr, SWT.CHECK);
        cb.setSelection(get(key, Boolean.class));
        cb.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                set(key, cb.getSelection());
            }
        });
        return cb;
    }

    /**
     * Adds 2 elements
     */
    void addTimeUnitDropdown(Composite gr, String msg) {
        if (!isSet(TIMEUNIT_KEY)) {
            set(TIMEUNIT_KEY, TimeUnit.SECONDS);
        }
        Label lb = new Label(gr, SWT.HORIZONTAL);
        lb.setText(msg);
        final Combo cb = new Combo(gr, SWT.DROP_DOWN | SWT.READ_ONLY);
        String[] items = new String[] {TimeUnit.MICROSECONDS.toString(), TimeUnit.MILLISECONDS.toString(),
                TimeUnit.SECONDS.toString(), TimeUnit.MINUTES.toString(), TimeUnit.HOURS.toString(),
                TimeUnit.DAYS.toString()};
        cb.setItems(items);
        String selected = get(TIMEUNIT_KEY, TimeUnit.class).toString();
        for (int i = 0; i < items.length; i++) {
            if (items[i].equals(selected)) {
                cb.select(i);
                break;
            }
        }
        cb.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                int idx = cb.getSelectionIndex();
                String selection = cb.getItem(idx);
                set(TIMEUNIT_KEY, TimeUnit.valueOf(selection));
            }
        });
    }

    /**
     * Adds 2 elements
     */
    void addText(Composite gr, String msg, String key, String defaultValue) {
        if (!isSet(key)) {
            set(key, defaultValue);
        }
        Label lb = new Label(gr, SWT.HORIZONTAL);
        lb.setText(msg);
        final Text text = new Text(gr, SWT.FILL | SWT.BORDER);
        GridData eData = new GridData();
        eData.horizontalIndent = 0;
        eData.horizontalAlignment = SWT.FILL;
        eData.grabExcessHorizontalSpace = true;
        eData.widthHint = 150;
        text.setText(get(key, String.class));
        text.setLayoutData(eData);
        text.addModifyListener(new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent e) {
                set(key, text.getText());
            }
        });
    }

    Composite addPositiveDoubleInput(Composite main, boolean includeZero) {
        return addPositiveDoubleInput(main, "Window width (" + TimeUnit.SECONDS.toString().toLowerCase() + ")",
                includeZero);
    }

    /**
     * Adds 1 element
     */
    Composite addPositiveDoubleInput(Composite gr, String msg, boolean includeZero) {
        if (!isSet(POSITIVE_DOUBLE_KEY)) {
            set(POSITIVE_DOUBLE_KEY, includeZero ? 0d : 1d);
        }
        if (msg != null) {
            Label label = new Label(gr, SWT.HORIZONTAL);
            label.setText(msg);
        }
        // The group for the width input and the error message
        Composite inputGroup = new Composite(gr, SWT.NONE);
        GridLayout inputLayout = new GridLayout(2, true);
        inputLayout.marginWidth = 0;
        inputGroup.setLayout(inputLayout);
        GridData data = new GridData(SWT.FILL, SWT.FILL, true, false);
        inputGroup.setLayoutData(data);

        positiveDoubleInput = new Text(inputGroup, SWT.FILL | SWT.BORDER);
        GridData eData = new GridData();
        eData.horizontalIndent = 0;
        eData.horizontalAlignment = SWT.FILL;
        eData.grabExcessHorizontalSpace = true;
        eData.widthHint = 150;
        positiveDoubleInput.setText(Double.toString(get(POSITIVE_DOUBLE_KEY, Double.class)));
        positiveDoubleInput.setLayoutData(eData);

        final Text errorMsg = new Text(inputGroup, SWT.READ_ONLY | SWT.WRAP);
        GridData errorData = new GridData();
        errorData.horizontalAlignment = GridData.HORIZONTAL_ALIGN_END;
        errorData.grabExcessHorizontalSpace = true;
        errorData.minimumWidth = 150;
        errorMsg.setLayoutData(errorData);
        errorMsg.setBackground(errorMsg.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
        errorMsg.setForeground(errorMsg.getDisplay().getSystemColor(SWT.COLOR_RED));
        errorMsg.setLayoutData(errorData);
        errorMsg.setVisible(false);

        positiveDoubleInput.addModifyListener(new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent e) {
                String s = positiveDoubleInput.getText();
                positiveDoubleOk = true;
                try {
                    double v = Double.parseDouble(s);
                    if (v < 0d || (v == 0d && !includeZero)) {
                        positiveDoubleOk = false;
                    } else {
                        set(POSITIVE_DOUBLE_KEY, v);
                    }
                } catch (NumberFormatException ex) {
                    positiveDoubleOk = false;
                }
                if (positiveDoubleOk) {
                    errorMsg.setVisible(false);
                } else {
                    if (!errorMsg.isDisposed()) {
                        errorMsg.setText("Not a positive real");
                        errorMsg.setVisible(true);
                        errorMsg.getParent().update();
                    }
                }
                checkOkButton();
            }
        });
        return inputGroup;
    }

    void addSelectionInput(Composite main, String title, SelectionType selectionType, Collection<String> items) {
        this.selectionType = selectionType;
        if (!isSet(SELECTION_KEY)) {
            set(SELECTION_KEY, new ArrayList<String>());
        }
        Group gr = new Group(main, SWT.SHADOW_IN);
        gr.setText(title);
        gr.setLayout(new GridLayout(1, true));
        GridData data = new GridData(SWT.FILL, SWT.FILL, true, false);
        gr.setLayoutData(data);

        List<String> input = new ArrayList<>();
        input.addAll(items);
        Collections.sort(input);
        cbtv = CheckboxTableViewer.newCheckList(gr, SWT.BORDER);
        cbtv.getTable().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        cbtv.setContentProvider(new ArrayContentProvider());
        cbtv.setInput(input);
        @SuppressWarnings("unchecked")
        List<String> selection = get(SELECTION_KEY, List.class);
        cbtv.setCheckedElements(selection.toArray());
        cbtv.setLabelProvider(new LabelProvider() {
            @Override
            public String getText(Object element) {
                return element.toString();
            }
        });
        cbtv.addSelectionChangedListener(new ISelectionChangedListener() {
            @Override
            public void selectionChanged(SelectionChangedEvent event) {
                CheckboxTableViewer source = (CheckboxTableViewer)event.getSource();
                List<String> newSelection = new ArrayList<String>();
                for (Object o: source.getCheckedElements()) {
                    newSelection.add(o.toString());
                }
                set(SELECTION_KEY, newSelection);
                checkOkButton();
            }
        });
        addSelectionButtons(main, cbtv);
    }

    private void addSelectionButtons(Composite composite, final CheckboxTableViewer cbtv) {
        Composite buttonComposite = new Composite(composite, SWT.NONE);
        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.marginWidth = 0;
        layout.horizontalSpacing = convertHorizontalDLUsToPixels(IDialogConstants.HORIZONTAL_SPACING);
        buttonComposite.setLayout(layout);
        buttonComposite.setLayoutData(new GridData(SWT.BEGINNING, SWT.TOP, true, false));

        Button selectButton = new Button(buttonComposite, SWT.NULL);
        selectButton.setText("Select all");
        selectButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                cbtv.setAllChecked(true);
                cbtv.setSelection(cbtv.getSelection(), true);
            }
        });

        Button deselectButton = new Button(buttonComposite, SWT.NULL);
        deselectButton.setText("Deselect all");
        deselectButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                cbtv.setAllChecked(false);
                cbtv.setSelection(cbtv.getSelection(), true);
            }
        });
    }
}
