/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.dialog;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.trace4cps.core.TraceException;
import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.ui.view.TraceView;

public class DependencyAnalysisWizard extends Wizard {
    private final TraceView traceView;

    private ModelTraceSelectionPage page1;

    private WeightAttributeSelectionPage page2;

    private int modelTraceIdx;

    private int systemTraceIdx;

    private String weightAttribute;

    public DependencyAnalysisWizard(TraceView view) {
        this.traceView = view;
        setWindowTitle("Dependency analysis settings");
        setNeedsProgressMonitor(false);
        TrayDialog.setDialogHelpAvailable(false);
    }

    @Override
    public void addPages() {
        page1 = new ModelTraceSelectionPage(this);
        addPage(page1);
        page2 = new WeightAttributeSelectionPage(this, traceView.getAttributeNames(TracePart.DEPENDENCY));
        addPage(page2);
    }

    @Override
    public IWizardPage getNextPage(IWizardPage page) {
        if (page instanceof ModelTraceSelectionPage) {
            List<String> modelTrace = page1.getSelection();
            if (modelTrace.isEmpty()) {
                return null;
            } else {
                return page2;
            }
        } else {
            return null;
        }
    }

    @Override
    public boolean performFinish() {
        modelTraceIdx = page1.getModelTraceIdx();
        systemTraceIdx = page1.getSystemTraceIdx();
        weightAttribute = page2.getWeightAttribute();
        return true;
    }

    public int getModelTraceIdx() throws TraceException {
        if (modelTraceIdx < 0) {
            throw new TraceException("No model trace specified");
        }
        return modelTraceIdx;
    }

    public int getSystemTraceIdx() throws TraceException {
        if (systemTraceIdx < 0) {
            throw new TraceException("No system trace found");
        }
        return systemTraceIdx;
    }

    public String getWeightAttribute() {
        return weightAttribute;
    }

    private static final class ModelTraceSelectionPage extends AbstractListSelectionWizardPage {
        private final Map<String, Integer> fileNameToIndex = new HashMap<>();

        private final List<String> fileNames = new ArrayList<>();

        public ModelTraceSelectionPage(DependencyAnalysisWizard wizard) {
            super("ModelTraceSelectionPage", wizard, SelectionMode.EXACTLY_ONE);
            for (int i = 0; i < wizard.traceView.getNumTraces(); i++) {
                File file = wizard.traceView.getTraceFile(i);
                fileNames.add(file.getName());
                fileNameToIndex.put(file.getName(), i);
            }
            Collections.sort(fileNames);
        }

        @Override
        protected String getPageTitle() {
            return "Model trace";
        }

        @Override
        protected String getPageMessage() {
            return "Select the model trace (exactly one)";
        }

        @Override
        protected Collection<String> getInput() {
            return fileNames;
        }

        public int getModelTraceIdx() {
            List<String> selection = getSelection();
            if (selection == null || selection.isEmpty()) {
                return -1;
            }
            return fileNameToIndex.get(selection.get(0));
        }

        public int getSystemTraceIdx() {
            // assumes exactly two traces
            List<String> selection = getSelection();
            if (selection == null || selection.isEmpty()) {
                return -1;
            }
            String modelTraceName = selection.get(0);
            for (Map.Entry<String, Integer> e: fileNameToIndex.entrySet()) {
                if (!modelTraceName.equals(e.getKey())) {
                    return e.getValue();
                }
            }
            return -1;
        }
    }

    private static final class WeightAttributeSelectionPage extends AbstractListSelectionWizardPage {
        private final List<String> depAttributes;

        public WeightAttributeSelectionPage(DependencyAnalysisWizard wizard, Set<String> attributeNames) {
            super("WeightAttributeSelectionPage", wizard, SelectionMode.ALWAYS);
            depAttributes = new ArrayList<>();
            depAttributes.addAll(attributeNames);
            Collections.sort(depAttributes);
        }

        @Override
        protected String getPageTitle() {
            return "Weight attribute";
        }

        @Override
        protected String getPageMessage() {
            return "Select the attribute to interpret as a minimal distance for the dependencies";
        }

        @Override
        protected Collection<String> getInput() {
            return depAttributes;
        }

        public String getWeightAttribute() {
            List<String> selection = getSelection();
            if (selection == null || selection.isEmpty()) {
                return null;
            }
            return selection.get(0);
        }
    }
}
