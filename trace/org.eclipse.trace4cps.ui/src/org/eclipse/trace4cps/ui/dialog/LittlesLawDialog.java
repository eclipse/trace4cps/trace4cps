/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.dialog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.trace4cps.core.ClaimEventType;
import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.ui.view.TraceView;

public final class LittlesLawDialog extends ParameterDialog {
    public enum LlDialogType {
        THROUGHPUT("identifier-based throughput"), THROUGHPUT_EVENT("event-based throughput"),
        LATENCY("identifier-based latency"), WIP("identifier-based wip");

        private final String rep;

        private LlDialogType(String r) {
            this.rep = r;
        }

        public String getRepresentation() {
            return rep;
        }
    }

    private static final String TITLE = "Little's law analysis";

    private final TraceView traceView;

    private final LlDialogType dialogType;

    private static final int N = 5;

    private final String[] keys = new String[N];

    private final String[] vals = new String[N];

    private ClaimEventType eventType = null;

    public LittlesLawDialog(Shell parentShell, TraceView view, LlDialogType type) {
        super(parentShell, LittlesLawDialog.class.getName());
        this.traceView = view;
        this.dialogType = type;
    }

    public String getIdAtt() {
        return getSelection().get(0);
    }

    public Map<String, String> getFilter() {
        Map<String, String> f = new HashMap<>();
        for (int i = 0; i < N; i++) {
            if (keys[i] != null && vals[i] != null) {
                f.put(keys[i], vals[i]);
            }
        }
        return f;
    }

    public ClaimEventType getEventType() {
        return eventType;
    }

    @Override
    protected String getTitle() {
        return TITLE + " - " + dialogType.getRepresentation();
    }

    @Override
    protected boolean inputIsValid() {
        boolean ok = true;
        if (dialogType == LlDialogType.THROUGHPUT_EVENT) {
            // ensure that there is a well-defined filter
            int cnt = 0;
            boolean noPairs = false;
            for (int i = 0; i < N; i++) {
                if (keys[i] != null && vals[i] != null) {
                    cnt++;
                }
                if ((keys[i] != null && vals[i] == null) || (keys[i] == null && vals[i] != null)) {
                    noPairs = true;
                }
            }
            ok = cnt > 0 && !noPairs;
        }
        return ok && super.inputIsValid();
    }

    @Override
    protected void draw(Composite composite) {
        if (dialogType == LlDialogType.THROUGHPUT) {
            drawTP(composite);
        } else if (dialogType == LlDialogType.LATENCY) {
            drawLatency(composite);
        } else if (dialogType == LlDialogType.WIP) {
            drawWip(composite);
        } else if (dialogType == LlDialogType.THROUGHPUT_EVENT) {
            drawTPevent(composite);
        } else {
            throw new IllegalStateException();
        }
        applyDialogFont(composite);
    }

    private void createGrid1(Composite main) {
        GridLayout layout = new GridLayout(1, true);
        main.setLayout(layout);
        GridData data = new GridData(SWT.FILL, SWT.FILL, true, false);
        main.setLayoutData(data);
    }

    private Composite createGrid2(Composite main) {
        Composite gr = new Composite(main, SWT.NONE);
        GridLayout layout = new GridLayout(2, true);
        layout.marginHeight = 0;
        gr.setLayout(layout);
        GridData data = new GridData(SWT.FILL, SWT.FILL, true, false);
        gr.setLayoutData(data);
        return gr;
    }

    private void drawTP(Composite main) {
        createGrid1(main);
        Composite sub = createGrid2(main);

        addPositiveDoubleInput(sub, true);
        addTimeUnitDropdown(sub, "Value scale");

        Set<String> atts = traceView.getAttributeNames(TracePart.CLAIM);
        addSelectionInput(main, "Select exactly one object grouping attribute", SelectionType.EXACTLY_ONE, atts);
    }

    private void drawTPevent(Composite main) {
        createGrid1(main);
        Composite sub = createGrid2(main);

        addPositiveDoubleInput(sub, false);
        addTimeUnitDropdown(sub, "Value scale");

        createFilterInput(main);
    }

    private void createFilterInput(Composite main) {
        Group group1 = new Group(main, SWT.SHADOW_IN);
        group1.setText("Event type");
        group1.setLayout(new GridLayout(3, true));
        group1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
        Button b1 = new Button(group1, SWT.RADIO);
        b1.setText("Any event");
        Button b2 = new Button(group1, SWT.RADIO);
        b2.setText("Start events only");
        Button b3 = new Button(group1, SWT.RADIO);
        b3.setText("End events only");

        if (eventType == null) {
            b1.setSelection(true);
        } else if (eventType == ClaimEventType.START) {
            b2.setSelection(true);
        } else if (eventType == ClaimEventType.END) {
            b3.setSelection(true);
        }

        b1.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                eventType = null;
            }
        });
        b2.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                eventType = ClaimEventType.START;
            }
        });
        b3.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                eventType = ClaimEventType.END;
            }
        });

        Group filterGroup = new Group(main, SWT.NONE);
        filterGroup.setText("Attribute filter");
        filterGroup.setLayout(new GridLayout(3, true));
        filterGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

        Set<String> atts = traceView.getAttributeNames(TracePart.EVENT);
        List<String> input = new ArrayList<>();
        input.add("");
        input.addAll(atts);
        Collections.sort(input);

        for (int i = 0; i < N; i++) {
            final int index = i;
            final Combo keyCombo = new Combo(filterGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
            new Label(filterGroup, SWT.HORIZONTAL).setText("=");
            final Combo valCombo = new Combo(filterGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
            keyCombo.setItems(input.toArray(new String[input.size()]));
            keyCombo.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    int idx = keyCombo.getSelectionIndex();
                    String selection = keyCombo.getItem(idx);
                    if (selection.equals("")) {
                        selection = null;
                    }
                    keys[index] = selection;
                    List<String> possibleVals = new ArrayList<>();
                    possibleVals.add("");
                    possibleVals.addAll(traceView.getAllAttributeValuesFor(selection, TracePart.EVENT));
                    Collections.sort(possibleVals);
                    valCombo.setItems(possibleVals.toArray(new String[possibleVals.size()]));
                    checkOkButton();
                }
            });
            valCombo.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    int idx = valCombo.getSelectionIndex();
                    String selection = valCombo.getItem(idx);
                    if (selection.equals("")) {
                        selection = null;
                    }
                    vals[index] = selection;
                    checkOkButton();
                }
            });
        }
    }

    private void drawLatency(Composite main) {
        createGrid1(main);
        Composite sub = createGrid2(main);

        addPositiveDoubleInput(sub, true);
        addTimeUnitDropdown(sub, "Value scale");

        Set<String> atts = traceView.getAttributeNames(TracePart.CLAIM);
        addSelectionInput(main, "Select exactly one object grouping attribute", SelectionType.EXACTLY_ONE, atts);
    }

    private void drawWip(Composite main) {
        createGrid1(main);
        Composite sub = createGrid2(main);

        addPositiveDoubleInput(sub, true);

        Set<String> atts = traceView.getAttributeNames(TracePart.CLAIM);
        addSelectionInput(main, "Select exactly one object grouping attribute", SelectionType.EXACTLY_ONE, atts);
    }
}
