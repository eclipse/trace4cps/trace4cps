/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.dialog;

import java.util.Map;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

public final class DistanceDialog extends ParameterDialog {
    private static final String TITLE = "Distance analysis";

    private final Map<String, Integer> traceKeyToIndex;

    public DistanceDialog(Shell parentShell, Map<String, Integer> traceKeyToIndex) {
        super(parentShell, ParameterDialog.class.getName());
        this.traceKeyToIndex = traceKeyToIndex;
    }

    public int getReferenceIndex() {
        String selected = getSelection().get(0);
        Integer idx = traceKeyToIndex.get(selected);
        if (idx == null) {
            return -1;
        } else {
            return idx;
        }
    }

    @Override
    protected String getTitle() {
        return TITLE;
    }

    protected void draw(final Composite composite) {
        addSelectionInput(composite, "Select the reference trace", SelectionType.EXACTLY_ONE, traceKeyToIndex.keySet());
        applyDialogFont(composite);
    }
}
