/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.dialog;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

public abstract class AbstractListSelectionWizardPage extends WizardPage {
    public enum SelectionMode {
        EXACTLY_ONE, AT_LEAST_ONE, ALWAYS
    }

    private CheckboxTableViewer listViewer;

    private final SelectionMode mode;

    protected AbstractListSelectionWizardPage(String pageName, Wizard wizard, SelectionMode mode) {
        super(pageName);
        this.mode = mode;
        setWizard(wizard);
    }

    protected List<String> getSelection() {
        List<String> result = new ArrayList<>();
        if (hasSelection()) {
            for (Object sel: listViewer.getCheckedElements()) {
                result.add(sel.toString());
            }
        }
        return result;
    }

    protected boolean hasSelection() {
        return listViewer != null && listViewer.getCheckedElements() != null
                && listViewer.getCheckedElements().length > 0;
    }

    protected abstract Collection<String> getInput();

    protected abstract String getPageTitle();

    protected abstract String getPageMessage();

    @Override
    public void createControl(Composite parent) {
        setTitle(getPageTitle());
        setMessage(getPageMessage());

        Collection<String> input = getInput();
        List<String> initialSelection = new ArrayList<>();
//        List<String> effectiveInput = new ArrayList<>();

        Composite contents = new Composite(parent, SWT.NULL);
        contents.setLayout(new GridLayout());
        contents.setLayoutData(new GridData(GridData.FILL_BOTH));
        if (input != null && input.size() > 0) {
            listViewer = CheckboxTableViewer.newCheckList(contents, SWT.BORDER);
            listViewer.getTable().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
            listViewer.addSelectionChangedListener(new ISelectionChangedListener() {
                @Override
                public void selectionChanged(SelectionChangedEvent event) {
                    CheckboxTableViewer source = (CheckboxTableViewer)event.getSource();
                    updatePageCompletionStatus(source);
                }
            });
            listViewer.setContentProvider(new ArrayContentProvider());
            listViewer.setInput(input);
            listViewer.setCheckedElements(initialSelection.toArray());
            listViewer.setSelection(listViewer.getSelection(), true);
            updatePageCompletionStatus(listViewer);
            addSelectionButtons(contents);
        } else {
            Label l = new Label(contents, SWT.None);
            l.setText("There are no attributes to filter on.");
        }
        setControl(contents);
    }

    private void updatePageCompletionStatus(CheckboxTableViewer source) {
        if (mode == SelectionMode.EXACTLY_ONE) {
            setPageComplete(source.getCheckedElements().length == 1);
        } else if (mode == SelectionMode.AT_LEAST_ONE) {
            setPageComplete(source.getCheckedElements().length >= 1);
        } else if (mode == SelectionMode.ALWAYS) {
            setPageComplete(true);
        }
    }

    protected void updateInputList() {
        listViewer.setInput(getInput());
    }

    /**
     * Add the selection and deselection buttons to the dialog.
     *
     * @param composite org.eclipse.swt.widgets.Composite
     */
    private void addSelectionButtons(Composite composite) {
        Composite buttonComposite = new Composite(composite, SWT.NONE);
        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.marginWidth = 0;
        layout.horizontalSpacing = convertHorizontalDLUsToPixels(IDialogConstants.HORIZONTAL_SPACING);
        buttonComposite.setLayout(layout);
        buttonComposite.setLayoutData(new GridData(SWT.BEGINNING, SWT.TOP, true, false));

        Button selectButton = new Button(buttonComposite, SWT.NULL);
        selectButton.setText("Select All");

        SelectionListener listener = new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                listViewer.setAllChecked(true);
                listViewer.setSelection(listViewer.getSelection(), true);
            }
        };
        selectButton.addSelectionListener(listener);

        Button deselectButton = new Button(buttonComposite, SWT.NULL);
        deselectButton.setText("Deselect All");

        listener = new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                listViewer.setAllChecked(false);
                listViewer.setSelection(listViewer.getSelection(), true);
            }
        };
        deselectButton.addSelectionListener(listener);
    }
}
