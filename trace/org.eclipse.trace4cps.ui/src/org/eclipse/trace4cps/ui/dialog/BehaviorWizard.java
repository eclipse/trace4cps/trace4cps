/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.dialog;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.ui.view.TraceView;

public class BehaviorWizard extends Wizard {
    private final TraceView traceView;

    private String idAtt;

    private Set<String> uniquenessAtts = new HashSet<>();

    private IdPage page1;

    private UniquenessPage page2;

    public BehaviorWizard(TraceView view) {
        this.traceView = view;
        setWindowTitle("Behavior settings");
    }

    public String getIdAttribute() {
        return idAtt;
    }

    public Set<String> getUniqueness() {
        return uniquenessAtts;
    }

    @Override
    public void addPages() {
        List<String> input = new ArrayList<>();
        input.addAll(traceView.getAttributeNames(TracePart.CLAIM));
        Collections.sort(input);
        page1 = new IdPage(this, input);
        page2 = new UniquenessPage(page1, this, input);
        addPage(page1);
        addPage(page2);
    }

    @Override
    public IWizardPage getNextPage(IWizardPage page) {
        if (page == page1 && page1.hasSelection() && page1.getSelection().size() == 1) {
            page2.updateInputList();
            return page2;
        }
        return null;
    }

    @Override
    public boolean canFinish() {
        return page1.hasSelection() && page1.getSelection().size() == 1;
    }

    @Override
    public boolean performFinish() {
        // ensure that every claim has an integer value for the idAtt
        if (validateIdAtt(page1.getSelection().get(0))) {
            idAtt = page1.getSelection().get(0);
            uniquenessAtts.addAll(page2.getSelection());
            return true;
        }
        // Show warning about id att
        MessageBox mb = new MessageBox(getShell(), SWT.ICON_ERROR | SWT.OK);
        mb.setText("Invalid ID attribute");
        mb.setMessage("Some claims have no value for the specified ID attribute, or a non-integer value.");
        mb.open();
        return false;
    }

    private boolean validateIdAtt(String idAtt) {
        for (IClaim claim: traceView.getTrace().getClaims()) {
            String val = claim.getAttributeValue(idAtt);
            if (val == null || !val.matches("-?\\d+")) {
                return false;
            }
        }
        return true;
    }

    private static final class IdPage extends AbstractListSelectionWizardPage {
        private final Collection<String> atts;

        public IdPage(Wizard wizard, Collection<String> atts) {
            super("ID selection", wizard, SelectionMode.EXACTLY_ONE);
            this.atts = atts;
        }

        @Override
        protected Collection<String> getInput() {
            return atts;
        }

        @Override
        protected String getPageTitle() {
            return "ID attribute (1/2)";
        }

        @Override
        protected String getPageMessage() {
            return "Select behavioral ID attribute (exactly 1)";
        }
    }

    private static final class UniquenessPage extends AbstractListSelectionWizardPage {
        private final IdPage idPage;

        private final Collection<String> atts;

        public UniquenessPage(IdPage idPage, Wizard wizard, Collection<String> atts) {
            super("Uniqueness selection", wizard, SelectionMode.ALWAYS);
            this.atts = atts;
            this.idPage = idPage;
        }

        @Override
        protected Collection<String> getInput() {
            List<String> input = new ArrayList<>();
            input.addAll(atts);
            String idAtt = null;
            if (idPage.getSelection() != null && idPage.getSelection().size() > 0) {
                idAtt = idPage.getSelection().get(0);
            }
            if (idAtt != null) {
                input.remove(idAtt);
            }
            return input;
        }

        @Override
        protected String getPageTitle() {
            return "Uniqueness attributes (2/2)";
        }

        @Override
        protected String getPageMessage() {
            return "Select behavioral uniqueness attributes (0 or more)";
        }
    }
}
