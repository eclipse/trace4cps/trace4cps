/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.dialog;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;

public final class CPAdialog extends ParameterDialog {
    private static final String TITLE = "Critical path analysis";

    private static final String USE_DEFAULT_EPSILON_KEY = "USE_DEFAULT_EPS";

    public static enum DependencyUse {
        NONE, FULL, APP;
    }

    private DependencyUse depUse = DependencyUse.FULL;

    public CPAdialog(Shell parentShell) {
        super(parentShell, CPAdialog.class.getName());
    }

    @Override
    protected String getTitle() {
        return TITLE;
    }

    @Override
    protected void checkOkButton() {
        Control button = getButton(IDialogConstants.OK_ID);
        if (button != null) {
            boolean ok = checkSelection() && (checkWindowWidth() || depUse == DependencyUse.FULL);
            button.setEnabled(ok && inputIsValid());
        }
    }

    public double getEpsilon() {
        if (get(USE_DEFAULT_EPSILON_KEY, Boolean.class)) {
            return 0d;
        }
        return getPositiveDoubleValue();
    }

    public DependencyUse useDependencies() {
        return depUse;
    }

    @Override
    protected void draw(final Composite composite) {
        Group useGroup = new Group(composite, SWT.SHADOW_IN);
        useGroup.setText("Use of specified dependencies");
        useGroup.setLayout(new GridLayout(3, true));
        useGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

        Group epsilonGroup = new Group(composite, SWT.SHADOW_IN);
        epsilonGroup.setText("Epsilon dependency heuristic");
        epsilonGroup.setLayout(new GridLayout(3, false));
        GridData data = new GridData(SWT.FILL, SWT.FILL, true, false);
        epsilonGroup.setLayoutData(data);

        epsilonGroup.setVisible(depUse != DependencyUse.FULL);

        Button epsilonButton = addCheckbox(epsilonGroup, "Use default epsilon", USE_DEFAULT_EPSILON_KEY, true);
        Composite epsilonInputGroup = addPositiveDoubleInput(epsilonGroup, null, true);
        epsilonInputGroup.setVisible(!get(USE_DEFAULT_EPSILON_KEY, Boolean.class));
        epsilonButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                boolean useDefaultEpsilon = epsilonButton.getSelection();
                epsilonInputGroup.setVisible(!useDefaultEpsilon);
            }
        });

        Button b2 = new Button(useGroup, SWT.RADIO);
        b2.setText("Full");
        Button b1 = new Button(useGroup, SWT.RADIO);
        b1.setText("None");
        Button b3 = new Button(useGroup, SWT.RADIO);
        b3.setText("As application dependencies");

        if (depUse == DependencyUse.NONE) {
            b1.setSelection(true);
        } else if (depUse == DependencyUse.FULL) {
            b2.setSelection(true);
        } else if (depUse == DependencyUse.APP) {
            b3.setSelection(true);
        }

        b1.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                depUse = DependencyUse.NONE;
                epsilonGroup.setVisible(true);
                checkOkButton();
            }
        });
        b2.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                depUse = DependencyUse.FULL;
                epsilonGroup.setVisible(false);
                checkOkButton();
            }
        });
        b3.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                depUse = DependencyUse.APP;
                epsilonGroup.setVisible(true);
                checkOkButton();
            }
        });

        applyDialogFont(composite);
    }
}
