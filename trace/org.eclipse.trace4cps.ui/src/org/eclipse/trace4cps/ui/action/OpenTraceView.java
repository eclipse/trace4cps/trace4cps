/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.action;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.trace4cps.ui.view.TraceView;
import org.eclipse.ui.IActionDelegate;

public class OpenTraceView implements IActionDelegate {
    private IStructuredSelection selection;

    @Override
    public void run(IAction action) {
        List<File> files = new ArrayList<>();
        for (Object o: selection) {
            IFile file = (IFile)o;
            File f = new File(file.getLocation().toFile().getAbsolutePath());
            files.add(f);
        }
        TraceView.showView(files, true);
    }

    @Override
    public void selectionChanged(IAction action, ISelection selection) {
        if (selection instanceof IStructuredSelection) {
            this.selection = (IStructuredSelection)selection;
        }
    }
}
