/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui;

import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.IConsoleView;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

public class ConsoleUtil {
    public static final String CONSOLE_NAME = "TRACE4CPS";

    private ConsoleUtil() {
    }

    public static void log(String msg) {
        // run on the UI thread:
        final IWorkbench workbench = PlatformUI.getWorkbench();
        workbench.getDisplay().asyncExec(new Runnable() {
            @Override
            public void run() {
                MessageConsole myConsole = findConsole();
                if (myConsole != null) {
                    MessageConsoleStream out = myConsole.newMessageStream();
                    out.println(msg);
                }
            }
        });
    }

    private static MessageConsole findConsole() {
        // Get or create console:
        ConsolePlugin plugin = ConsolePlugin.getDefault();
        if (plugin == null) {
            return null;
        }
        IConsoleManager conMan = plugin.getConsoleManager();
        IConsole[] existing = conMan.getConsoles();
        MessageConsole myConsole = null;
        for (int i = 0; i < existing.length; i++) {
            if (CONSOLE_NAME.equals(existing[i].getName())) {
                myConsole = (MessageConsole)existing[i];
                break;
            }
        }
        if (myConsole == null) {
            myConsole = new MessageConsole(CONSOLE_NAME, null);
            conMan.addConsoles(new IConsole[] {myConsole});
        }
        // Make the console visible:
        try {
            IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
            String id = IConsoleConstants.ID_CONSOLE_VIEW;
            IConsoleView view = (IConsoleView)page.showView(id);
            view.display(myConsole);
        } catch (PartInitException e) {
            // swallow...
            e.printStackTrace();
        }
        return myConsole;
    }
}
