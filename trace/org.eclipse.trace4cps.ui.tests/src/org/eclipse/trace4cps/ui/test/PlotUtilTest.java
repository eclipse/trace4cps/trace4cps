/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.test;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.core.impl.Claim;
import org.eclipse.trace4cps.core.impl.Resource;
import org.eclipse.trace4cps.core.impl.Trace;
import org.eclipse.trace4cps.vis.jfree.ClaimScaling;
import org.eclipse.trace4cps.vis.jfree.TracePlotManager;
import org.eclipse.trace4cps.vis.jfree.TraceViewConfiguration;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYIntervalSeriesCollection;
import org.junit.jupiter.api.Test;

public class PlotUtilTest {
    @Test
    public void testSimpleGantt() {
        TracePlotManager mgr = new TracePlotManager(getTestTrace());
        TraceViewConfiguration cfg = mgr.getViewConfig();
        cfg.setClaimScaling(ClaimScaling.RESOURCE_AMOUNT);
        cfg.setColoringAttributes(TracePart.CLAIM, Collections.singletonList("id"));
        cfg.setResourceView();
        showChart(mgr);
    }

    @Test
    public void testScalability() throws InterruptedException {
        TracePlotManager mgr = new TracePlotManager(getTestTrace("test", 20, 10000));
        TraceViewConfiguration cfg = mgr.getViewConfig();
        cfg.setClaimScaling(ClaimScaling.RESOURCE_AMOUNT);
        cfg.setColoringAttributes(TracePart.CLAIM, Collections.singletonList("id"));
        cfg.setResourceView();
        showChart(mgr);
    }

    public static Trace getTestTrace() {
        Trace t = new Trace();
        Resource cpu = new Resource(100, false);
        cpu.setAttribute("name", "CPU");
        Resource gpu = new Resource(100, false);
        gpu.setAttribute("name", "GPU");
        Resource mem = new Resource(100, true);
        mem.setAttribute("name", "RAM");
        t.add(new Claim(0, 20, cpu, 50, Collections.singletonMap("id", "1")));
        t.add(new Claim(10, 20, gpu, 100, Collections.singletonMap("id", "7")));
        t.add(new Claim(15, 25, cpu, 50, Collections.singletonMap("id", "15")));
        t.add(new Claim(0, 20, mem, 0, 10, Collections.singletonMap("id", "1")));
        t.add(new Claim(10, 20, mem, 15, 25, Collections.singletonMap("id", "7")));
        t.add(new Claim(15, 25, mem, 50, 38, Collections.singletonMap("id", "15")));
        return t;
    }

    public static Trace getTestTrace(String id, int numResources, int numTasks) {
        Trace trace = new Trace();
        Resource[] rs = new Resource[numResources];
        for (int i = 0; i < numResources; i++) {
            rs[i] = new Resource(1, false);
            rs[i].setAttribute("rid", "R" + Integer.toString(i));
        }
        Random rnd = new Random(0L);
        double t = 10;
        for (int i = 0; i < numTasks; i++) {
            double t0 = t + rnd.nextInt(10) - 5;
            double t1 = t0 + rnd.nextInt(10) + 5;
            Resource r = rs[rnd.nextInt(numResources)];
            Map<String, String> atts = new HashMap<String, String>();
            atts.put("id", Integer.toString(i));
            atts.put("id_mod_2", Integer.toString(i % 2));
            atts.put("id_mod_5", Integer.toString(i % 5));
            Claim claim = new Claim(t0, t1, r, rnd.nextDouble(), atts);
            trace.add(claim);
            t += 2;
        }
        return trace;
    }

    public static void showChart(final JFreeChart chart) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                JFrame frame = new JFrame("Test plot");
                frame.setSize(1200, 600);
                frame.setLocationRelativeTo(null);
                frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                frame.setVisible(true);
                ChartPanel panel = new ChartPanel(chart);
                frame.setContentPane(panel);
            }
        });
        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void showChart(final TracePlotManager mgr) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                JFrame frame = new JFrame("Test plot");
                frame.setSize(1200, 600);
                frame.setLocationRelativeTo(null);
                frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                frame.setVisible(true);

                JFreeChart chart = ChartFactory.createXYBarChart(null, null, false, null,
                        new XYIntervalSeriesCollection(), PlotOrientation.VERTICAL, false, true, false);
                XYPlot xyPlot = chart.getXYPlot();

                // Setup the datasets and coloring etc of the plot:
                mgr.initializePlot(xyPlot);
                mgr.update(xyPlot);

                ChartPanel panel = new ChartPanel(chart);

                frame.setContentPane(panel);
            }
        });
        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
