/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.test;

import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.F;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.G;
import static org.eclipse.trace4cps.analysis.mtl.MtlBuilder.IMPLY;
import static org.eclipse.trace4cps.analysis.stl.StlBuilder.GEQ;
import static org.eclipse.trace4cps.analysis.stl.StlBuilder.LEQ;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.awt.Paint;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.eclipse.trace4cps.analysis.AnalysisException;
import org.eclipse.trace4cps.analysis.constraintgraph.ConstraintConfig;
import org.eclipse.trace4cps.analysis.cpa.CpaResult;
import org.eclipse.trace4cps.analysis.cpa.CriticalPathAnalysis;
import org.eclipse.trace4cps.analysis.cpa.DependencyProvider;
import org.eclipse.trace4cps.analysis.mtl.AtomicProposition;
import org.eclipse.trace4cps.analysis.mtl.DefaultAtomicProposition;
import org.eclipse.trace4cps.analysis.mtl.MtlException;
import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.mtl.MtlUtil;
import org.eclipse.trace4cps.analysis.signal.SignalModifier;
import org.eclipse.trace4cps.analysis.signal.SignalUtil;
import org.eclipse.trace4cps.analysis.stl.StlBuilder;
import org.eclipse.trace4cps.analysis.stl.StlFormula;
import org.eclipse.trace4cps.core.ClaimEventType;
import org.eclipse.trace4cps.core.IAttributeAware;
import org.eclipse.trace4cps.core.IAttributeFilter;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.IFilteredTrace;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.core.impl.Interval;
import org.eclipse.trace4cps.core.impl.ModifiableTrace;
import org.eclipse.trace4cps.core.io.ParseException;
import org.eclipse.trace4cps.core.io.TraceReader;
import org.eclipse.trace4cps.ui.AnalysisUtil;
import org.eclipse.trace4cps.vis.jfree.ClaimScaling;
import org.eclipse.trace4cps.vis.jfree.TracePlotManager;
import org.eclipse.trace4cps.vis.jfree.TraceViewConfiguration;
import org.jfree.chart.JFreeChart;
import org.junit.jupiter.api.Test;

public class STTTpaperTest {
    private static final File TEST = new File("../../trace-examples/test.etf");

    private static final File BF = new File("../../trace-examples/odse/model-bf.etf");

    private static final File FF = new File("../../trace-examples/odse/model-ff.etf");

    private static final File REF = new File("../../trace-examples/odse/reference.etf");

    @Test
    public void testVisualizeTest() throws IOException, ParseException {
        ITrace trace1 = TraceReader.readTrace(TEST);
        PlotUtilTest.showChart(new TracePlotManager(trace1));
    }

    @Test
    public void testVisualize_activity2() throws IOException, ParseException {
        ITrace trace1 = TraceReader.readTrace(BF);
        TraceViewConfiguration cfg = getODSEActivityCfg();
        PlotUtilTest.showChart(new TracePlotManager(cfg, trace1));
    }

    @Test
    public void testVisualize_activity_multiple() throws IOException, ParseException {
        IFilteredTrace trace0 = new ModifiableTrace(TraceReader.readTrace(REF));
        IFilteredTrace trace1 = new ModifiableTrace(TraceReader.readTrace(BF));
        IFilteredTrace trace2 = new ModifiableTrace(TraceReader.readTrace(FF));
        IAttributeFilter filter = getODSEfilter();
        trace0.addFilterAndRecalculate(TracePart.CLAIM, filter);
        trace1.addFilterAndRecalculate(TracePart.CLAIM, filter);
        trace2.addFilterAndRecalculate(TracePart.CLAIM, filter);

        TraceViewConfiguration cfg = getODSEActivityCfg();
        PlotUtilTest.showChart(new TracePlotManager(cfg, trace0, trace1, trace2));
    }

    @Test
    public void testVisualize_resource_multiple() throws IOException, ParseException {
        IFilteredTrace trace0 = new ModifiableTrace(TraceReader.readTrace(REF));
        IFilteredTrace trace1 = new ModifiableTrace(TraceReader.readTrace(BF));
        IFilteredTrace trace2 = new ModifiableTrace(TraceReader.readTrace(FF));
        IAttributeFilter filter = getODSEfilter();
        trace0.addFilterAndRecalculate(TracePart.CLAIM, filter);
        trace1.addFilterAndRecalculate(TracePart.CLAIM, filter);
        trace2.addFilterAndRecalculate(TracePart.CLAIM, filter);

        TraceViewConfiguration cfg = getODSEResourceCfg();
        PlotUtilTest.showChart(new TracePlotManager(cfg, trace0, trace1, trace2));
    }

    @Test
    public void testVisualize_resource() throws IOException, ParseException {
        ITrace trace1 = TraceReader.readTrace(BF);
        TraceViewConfiguration cfg = getODSEResourceCfg();
        PlotUtilTest.showChart(new TracePlotManager(cfg, trace1));
    }

    /**
     * One way to show the highlight, using the
     * {@link MtlUtil#getAtomicPropIndices(org.eclipse.trace4cps.analysis.mtl.MtlResult)} does not work because there
     * are cases where only A and not G is then highlighted. The algorithm could conclude that the property could never
     * be satisfied (because of the F_[0,40] bound) without seeing G.
     */
    @Test
    public void testMTL1() throws IOException, MtlException, InterruptedException, ExecutionException, ParseException {
        IFilteredTrace trace = new ModifiableTrace(TraceReader.readTrace(BF));
        List<MtlFormula> phis = new ArrayList<>();
        for (int i = 0; i < 200; i++) {
            AtomicProposition ap1 = new DefaultAtomicProposition(ClaimEventType.START, "name", "A", "id",
                    Integer.toString(i));
            AtomicProposition ap2 = new DefaultAtomicProposition(ClaimEventType.END, "name", "G", "id",
                    Integer.toString(i));
            MtlFormula phi = G(IMPLY(ap1, F(ap2, new Interval(0, false, 35, false))));
            phis.add(phi);
        }
        trace.addFilterAndRecalculate(TracePart.CLAIM, getODSEfilter());
        Map<IAttributeAware, Paint> highlightMap = AnalysisUtil.mtlCheck(trace, phis, false);
        // show plot:
        TraceViewConfiguration cfg = getODSEActivityCfg();
        cfg.setHighlightMap(highlightMap);
        PlotUtilTest.showChart(new TracePlotManager(cfg, trace));
    }

    @Test
    public void testCPA() throws IOException, AnalysisException, ParseException {
        ModifiableTrace trace1 = new ModifiableTrace(TraceReader.readTrace(BF));
        TraceViewConfiguration cfg = getODSEActivityCfg();
        cfg.setClaimScaling(ClaimScaling.FULL);

        trace1.addFilter(TracePart.CLAIM, getODSEMemoryfilter());
        trace1.addFilter(TracePart.CLAIM, getODSEIdfilter(2));
        trace1.recalculate();

        ConstraintConfig config = ConstraintConfig.getDefault();
        config.setAddClaimDurations(true);
        config.setAddSourceAndSink(true);
        config.setApplyOrderingHeuristic(0);
        config.setUseDependencies(false);

        DependencyProvider p = new DependencyProvider() {
            @Override
            public boolean isApplicationDependency(IEvent src, IEvent dst) {
                String sn = src.getAttributeValue("name");
                String dn = dst.getAttributeValue("name");
                if (sn.equals(dn)) {
                    return true;
                }
                if ("A".equals(sn) && "B".equals(dn)) {
                    return true;
                }
                if ("B".equals(sn) && "C".equals(dn)) {
                    return true;
                }
                if ("B".equals(sn) && "F".equals(dn)) {
                    return true;
                }
                if ("C".equals(sn) && "D".equals(dn)) {
                    return true;
                }
                if ("D".equals(sn) && "E".equals(dn)) {
                    return true;
                }
                if ("E".equals(sn) && "G".equals(dn)) {
                    return true;
                }
                if ("F".equals(sn) && "G".equals(dn)) {
                    return true;
                }
                return false;
            }
        };

        CpaResult cpaResult = CriticalPathAnalysis.run(trace1, config);
        trace1.addDependencies(cpaResult.getCriticalDeps());

        cfg.setHighlightMap(AnalysisUtil.createHighlight(cpaResult, p));
        cfg.addNonPersistentFilter(TracePart.DEPENDENCY, CriticalPathAnalysis.getCriticalDependencyFilter());

        TracePlotManager mgr = new TracePlotManager(cfg, trace1);
        PlotUtilTest.showChart(mgr);
    }

    @Test
    public void testDiff1() throws IOException, ParseException {
        IFilteredTrace trace0 = new ModifiableTrace(TraceReader.readTrace(REF));
        IFilteredTrace trace1 = new ModifiableTrace(TraceReader.readTrace(BF));
        IFilteredTrace trace2 = new ModifiableTrace(TraceReader.readTrace(FF));
        IAttributeFilter filter = getODSEfilter();
        trace0.addFilterAndRecalculate(TracePart.CLAIM, filter);
        trace1.addFilterAndRecalculate(TracePart.CLAIM, filter);
        trace2.addFilterAndRecalculate(TracePart.CLAIM, filter);

        TraceViewConfiguration cfg = getODSEResourceCfg();
        cfg.setShowDependencies(false);
        cfg.setHighlightMap(AnalysisUtil.computeDiffHighlights(trace0, trace1, trace2));

        PlotUtilTest.showChart(new TracePlotManager(cfg, trace0, trace1, trace2));
    }

    @Test
    public void testAnomalyHistogram() throws IOException, ParseException {
        IFilteredTrace trace = new ModifiableTrace(TraceReader.readTrace(BF));
        trace.addFilterAndRecalculate(TracePart.EVENT, getODSEfilter());
        JFreeChart chart = AnalysisUtil.computeAnomalyHistogram(trace, "id", Collections.singleton("name"));
        assertNotNull(chart);
        PlotUtilTest.showChart(chart);
    }

    @Test
    public void testAnomalyHighlightCells() throws IOException, ParseException {
        IFilteredTrace trace1 = new ModifiableTrace(TraceReader.readTrace(BF));
        trace1.addFilterAndRecalculate(TracePart.CLAIM, getODSEfilter());
        TraceViewConfiguration cfg = getODSEActivityCfg();
        cfg.setShowDependencies(false);
        cfg.setHighlightMap(AnalysisUtil.computeAnomalyHistogramHighlight(trace1, "id", Collections.singleton("name")));
        PlotUtilTest.showChart(new TracePlotManager(cfg, trace1));
    }

    @Test
    public void testAnomalyRepresentatives() throws IOException, ParseException {
        IFilteredTrace trace1 = new ModifiableTrace(TraceReader.readTrace(BF));
        trace1.addFilterAndRecalculate(TracePart.CLAIM, getODSEfilter());
        IAttributeFilter filter = AnalysisUtil.computeBehavioralRepresentativeFilter(trace1, "id",
                Collections.singleton("name"));
        trace1.clearFilter(TracePart.ALL);
        trace1.addFilterAndRecalculate(TracePart.CLAIM, filter);
        TraceViewConfiguration cfg = getODSEActivityCfg();
        PlotUtilTest.showChart(new TracePlotManager(cfg, trace1));
    }

    @Test
    public void testResourceHistogram() throws IOException, ParseException {
        ITrace trace = TraceReader.readTrace(BF);
        List<IResource> resources = new ArrayList<IResource>();
        resources.add(getResource(trace, "CPU"));
        resources.add(getResource(trace, "M1"));
        resources.add(getResource(trace, "M2"));
        JFreeChart chart = AnalysisUtil.createResourceUsageHistogramPlot(trace, resources,
                Collections.singletonList("name"), false);
        assertNotNull(chart);
        PlotUtilTest.showChart(chart);
    }

    @Test
    public void testResourceUsage() throws IOException, AnalysisException, ParseException {
        ITrace trace = TraceReader.readTrace(BF);
        List<IResource> resources = new ArrayList<IResource>();
        resources.add(getResource(trace, "CPU"));
        JFreeChart chart = AnalysisUtil.createResourceUtilizationPlot(trace, resources, true,
                new SignalModifier(1, 100, TimeUnit.MILLISECONDS), true);
        PlotUtilTest.showChart(chart);
    }

    @Test
    public void testResourceUsage2() throws IOException, AnalysisException, ParseException {
        ITrace trace = TraceReader.readTrace(BF);
        List<IResource> resources = new ArrayList<IResource>();
        resources.add(getResource(trace, "M1"));
        resources.add(getResource(trace, "M2"));
        JFreeChart chart = AnalysisUtil.createResourceUtilizationPlot(trace, resources, true, SignalModifier.NONE,
                true);
        assertNotNull(chart);
        PlotUtilTest.showChart(chart);
    }

    private IResource getResource(ITrace trace, String attributeValue) {
        for (IResource r: trace.getResources()) {
            for (String val: r.getAttributes().values()) {
                if (val.equals(attributeValue)) {
                    return r;
                }
            }
        }
        return null;
    }

    @Test
    public void testWipPlot() throws IOException, ParseException {
        IFilteredTrace trace = new ModifiableTrace(TraceReader.readTrace(BF));
        trace.addFilterAndRecalculate(TracePart.CLAIM, getODSEfilter());
        JFreeChart chart = AnalysisUtil.createWipPlot(trace, "id", "ids", SignalModifier.NONE);
        assertNotNull(chart);
        PlotUtilTest.showChart(chart);
    }

    @Test
    public void testWipPlotConvolution() throws IOException, ParseException {
        IFilteredTrace trace = new ModifiableTrace(TraceReader.readTrace(BF));
        trace.addFilterAndRecalculate(TracePart.CLAIM, getODSEfilter());
        JFreeChart chart = AnalysisUtil.createWipPlot(trace, "id", "ids",
                new SignalModifier(1, 50, TimeUnit.MILLISECONDS));
        assertNotNull(chart);
        PlotUtilTest.showChart(chart);
    }

    @Test
    public void testLatencyPlot() throws IOException, ParseException {
        IFilteredTrace trace = new ModifiableTrace(TraceReader.readTrace(BF));
        trace.addFilterAndRecalculate(TracePart.CLAIM, getODSEfilter());
        JFreeChart chart = AnalysisUtil.createLatencyPlot(trace, "id", TimeUnit.MILLISECONDS, 0d, TimeUnit.SECONDS);
        assertNotNull(chart);
        PlotUtilTest.showChart(chart);
    }

    @Test
    public void testSTL2() throws IOException, AnalysisException, ParseException {
        ITrace trace = TraceReader.readTrace(BF);
        IPsop p = SignalUtil.getResourceAmount(trace, getResource(trace, "CPU"), SignalModifier.NONE);

        Map<String, IPsop> signals = new HashMap<String, IPsop>();
        signals.put("cpu", p);

        StlFormula phi1 = StlBuilder.G(GEQ(p, 80), 0, 10);
        StlFormula phi2 = StlBuilder.F(StlBuilder.G(LEQ(p, 5), 0, 5), 0, 50);
        StlFormula phi3 = StlBuilder.IMPLY(phi1, phi2);
        StlFormula phi4 = StlBuilder.G(phi3);
        System.out.println(phi4.getRho());

        JFreeChart chart = AnalysisUtil.createChart(trace.getTimeUnit(), "rho",
                new String[]
                {"p", "phi1", "phi2", "phi3"}, p, phi1.getSignal(), phi2.getSignal(), phi3.getSignal());
        PlotUtilTest.showChart(chart);
    }

    private TraceViewConfiguration getODSEActivityCfg() {
        TraceViewConfiguration cfg = new TraceViewConfiguration();
        cfg.setClaimScaling(ClaimScaling.FULL);
        cfg.setGroupingAttributes(TracePart.CLAIM, Collections.singletonList("name"));
        cfg.setColoringAttributes(TracePart.CLAIM, Collections.singletonList("id"));
        return cfg;
    }

    private TraceViewConfiguration getODSEResourceCfg() {
        TraceViewConfiguration cfg = new TraceViewConfiguration();
        cfg.setClaimScaling(ClaimScaling.RESOURCE_AMOUNT);
        cfg.setResourceView();
        cfg.setGroupingAttributes(TracePart.CLAIM, Collections.singletonList("name"));
        cfg.setColoringAttributes(TracePart.CLAIM, Collections.singletonList("id"));
        return cfg;
    }

    public static IAttributeFilter getFlowMPfilter() {
        return new IAttributeFilter() {
            @Override
            public boolean include(IAttributeAware a) {
                return "MP".equals(a.getAttributeValue("name"));
            }
        };
    }

    public static IAttributeFilter getODSEfilter() {
        return new IAttributeFilter() {
            @Override
            public boolean include(IAttributeAware a) {
                String name = a.getAttributeValue("name");
                return name != null && (name.equals("A") || name.equals("B") || name.equals("C") || name.equals("D")
                        || name.equals("E") || name.equals("F") || name.equals("G"));
            }
        };
    }

    public static IAttributeFilter getODSEIdfilter(int max) {
        return new IAttributeFilter() {
            @Override
            public boolean include(IAttributeAware a) {
                String idStr = a.getAttributeValue("id");
                if (idStr != null) {
                    return Integer.parseInt(idStr) <= max;
                }
                return false;
            }
        };
    }

    public static IAttributeFilter negationFilter(IAttributeFilter f) {
        return new IAttributeFilter() {
            @Override
            public boolean include(IAttributeAware a) {
                return !f.include(a);
            }
        };
    }

    public static IAttributeFilter getODSEMemoryfilter() {
        return new IAttributeFilter() {
            @Override
            public boolean include(IAttributeAware a) {
                if (!(a instanceof IClaim)) {
                    return false;
                }
                IClaim c = (IClaim)a;
                IResource r = c.getResource();
                String name = r.getAttributeValue("name");
                return "M1".equals(name) || "M2".equals(name);
            }
        };
    }
}
