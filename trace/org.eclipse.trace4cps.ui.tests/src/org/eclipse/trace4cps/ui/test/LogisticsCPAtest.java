/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.ui.test;

import java.awt.Paint;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.trace4cps.analysis.constraintgraph.ConstraintConfig;
import org.eclipse.trace4cps.analysis.cpa.CpaResult;
import org.eclipse.trace4cps.analysis.cpa.CriticalPathAnalysis;
import org.eclipse.trace4cps.analysis.cpa.PositiveCycleException;
import org.eclipse.trace4cps.core.IAttributeAware;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.TracePart;
import org.eclipse.trace4cps.core.impl.Claim;
import org.eclipse.trace4cps.core.impl.Resource;
import org.eclipse.trace4cps.core.impl.Trace;
import org.eclipse.trace4cps.ui.AnalysisUtil;
import org.eclipse.trace4cps.vis.jfree.ClaimScaling;
import org.eclipse.trace4cps.vis.jfree.TracePlotManager;
import org.eclipse.trace4cps.vis.jfree.TraceViewConfiguration;
import org.junit.jupiter.api.Test;

public class LogisticsCPAtest {
    @Test
    public void testViewExample() {
        TraceHighlight hl = getExampleTrace(false);
        showTrace(hl.trace, null);
    }

    @Test
    public void testViewExampleTrue() throws PositiveCycleException {
        TraceHighlight hl = getExampleTrace(true);
        ConstraintConfig config = ConstraintConfig.getDefault();
        config.setAddSourceAndSink(true);
        config.setApplyOrderingHeuristic(0d);
        config.setApplyNonElasticityHeuristic(Collections.singletonMap("type", "transport"), "id");
        CpaResult r = CriticalPathAnalysis.run(hl.trace, config);
        showTrace(hl.trace, AnalysisUtil.createHighlight(r, null));
    }

    @Test
    public void testViewExample2() throws PositiveCycleException {
        Trace t = createPaperPathTrace(20);
        String[] ordering = new String[] {"PIM", "PIM to MP", "MP", "MP to ITS", "ITS", "ITS to DEF", "DEF",
                "DEF to MP", "DEF to FIN", "FIN"};
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setAddSourceAndSink(true);
        cfg.setApplyOrderingHeuristic(0);
        CpaResult r = CriticalPathAnalysis.run(t, cfg);
        showTrace(t, AnalysisUtil.createHighlight(r, null), ClaimScaling.RESOURCE_AMOUNT, Arrays.asList(ordering));
    }

    private Trace createPaperPathTrace(int numSheets) {
        Trace trace = new Trace();
        IResource r = new Resource(12, false);
        IResource r2 = new Resource(1, false);

        final double sheetLength = 210; // A4: 210 mm
        final double speed = 1.250; // mm/ms
        final double releaseDelta = 500; // ms
        // distances:

        // time offset for this sheet:
        double t = 0;
        for (int i = 1; i <= numSheets; i++) {
            createSheetClaims(trace, r, r2, speed, sheetLength + i, t, i);
            t += releaseDelta;
        }

        return trace;
    }

    private void createSheetClaims(Trace trace, IResource r, IResource r2, double speed, double sheetLength, double t,
            int i)
    {
        final double IN_MP = 1200;
        final double MP_ITS = 2300;
        final double ITS_DEF = 4000;
        final double DEF_OUT = 2000;
        final double DEF_MP = 3300;
        // derived:
        double poiPassTime = sheetLength / speed;

        double tt = t;
        // pass IN
        double delta = poiPassTime;
        trace.add(new Claim(tt, tt + delta, r2, 1,
                from("name", "PIM", "id", Integer.toString(i), "pass", "1", "type", "transport")));
        tt += delta;
        // IN -> MP
        delta = IN_MP / speed;
        trace.add(new Claim(tt, tt + delta, r, 1,
                from("name", "PIM to MP", "id", Integer.toString(i), "pass", "1", "type", "transport")));
        tt += delta;
        // pass MP
        delta = poiPassTime;
        trace.add(new Claim(tt, tt + delta, r2, 1,
                from("name", "MP", "id", Integer.toString(i), "pass", "1", "type", "transport")));
        tt += delta;
        // MP1 -> ITS1
        delta = MP_ITS / speed;
        trace.add(new Claim(tt, tt + delta, r, 1,
                from("name", "MP to ITS", "id", Integer.toString(i), "pass", "1", "type", "transport")));
        tt += delta;
        // pass ITS1
        delta = poiPassTime;
        trace.add(new Claim(tt, tt + delta, r2, 1,
                from("name", "ITS", "id", Integer.toString(i), "pass", "1", "type", "transport")));
        tt += delta;
        // ITS1 -> DEF1
        delta = ITS_DEF / speed;
        trace.add(new Claim(tt, tt + delta, r, 1,
                from("name", "ITS to DEF", "id", Integer.toString(i), "pass", "1", "type", "transport")));
        tt += delta;
        // pass DEF1
        delta = poiPassTime;
        trace.add(new Claim(tt, tt + delta, r2, 1,
                from("name", "DEF", "id", Integer.toString(i), "pass", "1", "type", "transport")));
        tt += delta;
        // DEF1 -> MP2
        delta = DEF_MP / speed;
        trace.add(new Claim(tt, tt + delta, r, 1,
                from("name", "DEF to MP", "id", Integer.toString(i), "pass", "1", "type", "transport")));
        tt += delta;
        // pass MP2
        delta = poiPassTime;
        trace.add(new Claim(tt, tt + delta, r2, 1,
                from("name", "MP", "id", Integer.toString(i), "pass", "2", "type", "transport")));
        tt += delta;
        // MP2 -> ITS2
        delta = MP_ITS / speed;
        trace.add(new Claim(tt, tt + delta, r, 1,
                from("name", "MP to ITS", "id", Integer.toString(i), "pass", "2", "type", "transport")));
        tt += delta;
        // pass ITS2
        delta = poiPassTime;
        trace.add(new Claim(tt, tt + delta, r2, 1,
                from("name", "ITS", "id", Integer.toString(i), "pass", "2", "type", "transport")));
        tt += delta;
        // ITS2 -> DEF2
        delta = ITS_DEF / speed;
        trace.add(new Claim(tt, tt + delta, r, 1,
                from("name", "ITS to DEF", "id", Integer.toString(i), "pass", "1", "type", "transport")));
        tt += delta;
        // pass DEF2
        delta = poiPassTime;
        trace.add(new Claim(tt, tt + delta, r2, 1,
                from("name", "DEF", "id", Integer.toString(i), "pass", "2", "type", "transport")));
        tt += delta;
        // DEF2 -> FIN
        delta = DEF_OUT / speed;
        trace.add(new Claim(tt, tt + delta, r, 1,
                from("name", "DEF to FIN", "id", Integer.toString(i), "pass", "1", "type", "transport")));
        tt += delta;

        // pass OUT1
        delta = poiPassTime;
        trace.add(new Claim(tt, tt + delta, r2, 1,
                from("name", "FIN", "id", Integer.toString(i), "pass", "1", "type", "transport")));
        tt += delta;
    }

    private TraceHighlight getExampleTrace(boolean includeConstraints) {
        Trace trace = new Trace();
        IResource r = new Resource(10, false);

        IClaim a1 = new Claim(0, 1, r, 1, from("name", "IN", "id", "1", "type", "transport"));
        IClaim a2 = new Claim(1, 9, r, 1, from("name", "IN_ITS2", "id", "1", "type", "transport"));
        IClaim a3 = new Claim(9, 10, r, 1, from("name", "ITS2", "id", "1", "type", "transport"));
        IClaim a4 = new Claim(10, 19, r, 1, from("name", "ITS2_OUT", "id", "1", "type", "transport"));
        IClaim a5 = new Claim(19, 20, r, 1, from("name", "OUT", "id", "1", "type", "transport"));
        trace.add(a1);
        trace.add(a2);
        trace.add(a3);
        trace.add(a4);
        trace.add(a5);

        IClaim b1 = new Claim(26, 27, r, 1, from("name", "IN", "id", "2", "type", "transport"));
        IClaim b2 = new Claim(27, 35, r, 1, from("name", "IN_ITS2", "id", "2", "type", "transport"));
        IClaim b3 = new Claim(35, 36, r, 1, from("name", "ITS2", "id", "2", "type", "transport"));
        IClaim b4 = new Claim(36, 45, r, 1, from("name", "ITS2_OUT", "id", "2", "type", "transport"));
        IClaim b5 = new Claim(45, 46, r, 1, from("name", "OUT", "id", "2", "type", "transport"));
        trace.add(b1);
        trace.add(b2);
        trace.add(b3);
        trace.add(b4);
        trace.add(b5);

        IClaim c1 = new Claim(52, 53, r, 1, from("name", "IN", "id", "3", "type", "transport"));
        IClaim c2 = new Claim(53, 61, r, 1, from("name", "IN_ITS2", "id", "3", "type", "transport"));
        IClaim c3 = new Claim(61, 62, r, 1, from("name", "ITS2", "id", "3", "type", "transport"));
        IClaim c4 = new Claim(62, 71, r, 1, from("name", "ITS2_OUT", "id", "3", "type", "transport"));
        IClaim c5 = new Claim(71, 72, r, 1, from("name", "OUT", "id", "3", "type", "transport"));
        trace.add(c1);
        trace.add(c2);
        trace.add(c3);
        trace.add(c4);
        trace.add(c5);

        if (includeConstraints) {
            IClaim con0 = new Claim(1, 6, r, 1, from("name", "c0"));
            IClaim con1 = new Claim(10, 45, r, 1, from("name", "c1"));
            IClaim con2 = new Claim(36, 61, r, 1, from("name", "c2"));
            trace.add(con0);
            trace.add(con1);
            trace.add(con2);
            return new TraceHighlight(trace, con1, con2, a1, a2, a3, b4, c3, c4, c5);
        } else {
            return new TraceHighlight(trace);
        }
    }

    private void showTrace(Trace trace, Map<IAttributeAware, Paint> highlight) {
        showTrace(trace, highlight, ClaimScaling.FULL, null);
    }

    private void showTrace(Trace trace, Map<IAttributeAware, Paint> highlight, ClaimScaling scaling,
            List<String> order)
    {
        TracePlotManager mgr = new TracePlotManager(trace);
        TraceViewConfiguration cfg = mgr.getViewConfig();
        cfg.setClaimScaling(scaling);
        cfg.setColoringAttributes(TracePart.CLAIM, Collections.singletonList("id"));
        cfg.setGroupingAttributes(TracePart.CLAIM, Collections.singletonList("name"));
        cfg.setHighlightMap(highlight);
        PlotUtilTest.showChart(mgr);
    }

    private static Map<String, String> from(String... ss) {
        if (ss == null || ss.length == 0 || ss.length % 2 != 0) {
            throw new IllegalArgumentException();
        }
        Map<String, String> r = new HashMap<String, String>();
        for (int i = 0; i < ss.length; i += 2) {
            r.put(ss[i], ss[i + 1]);
        }
        return r;
    }

    private static final class TraceHighlight {
        private final Trace trace;

        private final Set<IClaim> critical = new HashSet<IClaim>();

        public TraceHighlight(Trace trace, IClaim... claims) {
            this.trace = trace;
            for (IClaim c: claims) {
                critical.add(c);
            }
        }
    }
}
