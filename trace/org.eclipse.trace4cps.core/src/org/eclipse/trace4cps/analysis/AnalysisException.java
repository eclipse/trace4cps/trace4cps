/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis;

import org.eclipse.trace4cps.core.TraceException;

/**
 * Top-level exception for the TRACE analysis packages.
 */
public class AnalysisException extends TraceException {
    private static final long serialVersionUID = 1L;

    public AnalysisException() {
    }

    public AnalysisException(String msg) {
        super(msg);
    }

    public AnalysisException(Throwable cause) {
        super(cause);
    }

    public AnalysisException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
