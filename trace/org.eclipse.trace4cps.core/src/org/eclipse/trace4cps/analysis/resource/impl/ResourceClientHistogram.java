/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.resource.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * This class provides a histogram of the time that the resource has <i>n</i> clients.
 */
public final class ResourceClientHistogram {
    private static final Comparator<Event> COMP = new Comparator<Event>() {
        @Override
        public int compare(Event o1, Event o2) {
            return Double.compare(o1.timeStamp, o2.timeStamp);
        }
    };

    private final TreeMap<Integer, Double> histogram = new TreeMap<Integer, Double>();

    private final TreeMap<Integer, Double> normalizedHistogram = new TreeMap<Integer, Double>();

    private final TreeMap<Integer, Double> normalizedCumulHistogram = new TreeMap<Integer, Double>();

    private boolean done = false;

    private final List<Event> events = new ArrayList<Event>();

    /**
     * For building the histogram from a list of claims.
     *
     * @param start start timestamp of a single claim
     * @param end end timestamp of a single claim
     */
    public void claim(double start, double end) {
        if (done) {
            throw new IllegalStateException("client usage has already been called once");
        }
        if (start < end) {
            events.add(new Event(start, true));
            events.add(new Event(end, false));
        }
    }

    /**
     * @return the histogram that gives the absolute time for each client count
     */
    public TreeMap<Integer, Double> getClientUsage() {
        if (!done) {
            throw new IllegalStateException("done has not been called");
        }
        return histogram;
    }

    /**
     * @return the histogram that gives the % time for each client count
     */
    public TreeMap<Integer, Double> getNormalizedClientUsage() {
        if (!done) {
            throw new IllegalStateException("done has not been called");
        }
        return normalizedHistogram;
    }

    /**
     * @return the histogram that gives the cumulative % time for each client count
     */
    public TreeMap<Integer, Double> getNormalizedCumulativeClientUsage() {
        if (!done) {
            throw new IllegalStateException("done has not been called");
        }
        return normalizedCumulHistogram;
    }

    /**
     * Finishes building the histogram. Needs to be called before {@link #getClientUsage()}.
     *
     * @param lastTimeStamp the last timestamp in the trace
     */
    public void done(double firstTimeStamp, double lastTimeStamp) {
        done = true;
        if (events.size() == 0) {
            histogram.put(0, lastTimeStamp - firstTimeStamp);
            normalizedHistogram.put(0, 100d);
            normalizedCumulHistogram.put(0, 100d);
            return;
        }
        Collections.sort(events, COMP);
        int cnt = 0;
        double prevTimeStamp = 0d;
        if (events.size() > 0) {
            prevTimeStamp = events.get(0).timeStamp;
        }
        if (prevTimeStamp < firstTimeStamp) {
            throw new IllegalStateException("timestamps from claims are before the first timestamp");
        }
        process(firstTimeStamp, prevTimeStamp, 0);
        for (Event e: events) {
            process(prevTimeStamp, e.timeStamp, cnt);
            prevTimeStamp = e.timeStamp;
            if (e.isStart) {
                cnt++;
            } else {
                cnt--;
            }
        }
        if (prevTimeStamp > lastTimeStamp) {
            throw new IllegalStateException("timestamps from claims are after the last timestamp");
        }
        process(prevTimeStamp, lastTimeStamp, cnt);
        // Create normalized
        double sum = 0d;
        for (Double t: histogram.values()) {
            sum += t;
        }
        double prevV = 0d;
        for (Map.Entry<Integer, Double> e: histogram.entrySet()) {
            double v = (e.getValue() * 100d) / sum;
            normalizedHistogram.put(e.getKey(), v);
            normalizedCumulHistogram.put(e.getKey(), v + prevV);
            prevV += v;
        }
    }

    private void process(double start, double end, int cnt) {
        if (end > start) {
            Double v = histogram.get(cnt);
            if (v == null) {
                v = 0d;
            }
            histogram.put(cnt, v + (end - start));
        }
    }

    private static final class Event {
        private final double timeStamp;

        private boolean isStart;

        private Event(double t, boolean start) {
            this.timeStamp = t;
            this.isStart = start;
        }
    }
}
