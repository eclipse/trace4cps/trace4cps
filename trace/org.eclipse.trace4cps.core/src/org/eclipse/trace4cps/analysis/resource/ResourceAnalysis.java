/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.eclipse.trace4cps.analysis.resource.impl.ResourceClientHistogram;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.ITrace;

/**
 * This class provides a straightforward resource-usage analysis of a given {@link ITrace}.
 */
public class ResourceAnalysis {
    private ResourceAnalysis() {
    }

    /**
     * Computes the resource-usage of selected resources.
     *
     * @param trace the {@link ITrace}
     * @param selected the {@link IResource} instances to include
     * @param cumulative whether the result is shown as a cumulative value
     * @return a map that for each selected resource gives a map that for a number of concurrent clients on the resource
     *     gives the amount of time (% of the trace time, possible cumulative)
     */
    public static Map<IResource, TreeMap<Integer, Double>> compute(ITrace trace, List<IResource> selected,
            boolean cumulative)
    {
        return convertHistos(getHistos(trace), selected, cumulative);
    }

    private static Map<IResource, ResourceClientHistogram> getHistos(ITrace trace) {
        Map<IResource, ResourceClientHistogram> histos = new HashMap<IResource, ResourceClientHistogram>();
        double min = Double.POSITIVE_INFINITY;
        double max = Double.NEGATIVE_INFINITY;
        for (IClaim c: trace.getClaims()) {
            IResource r = c.getResource();
            ResourceClientHistogram h = histos.get(r);
            if (h == null) {
                h = new ResourceClientHistogram();
                histos.put(r, h);
            }
            h.claim(c.getStartTime().doubleValue(), c.getEndTime().doubleValue());
            // Keep min and max timestamps over all claims
            min = Math.min(min, c.getStartTime().doubleValue());
            max = Math.max(max, c.getEndTime().doubleValue());
        }
        for (ResourceClientHistogram h: histos.values()) {
            h.done(min, max);
        }
        return histos;
    }

    private static Map<IResource, TreeMap<Integer, Double>>
            convertHistos(Map<IResource, ResourceClientHistogram> histos, List<IResource> selected, boolean cumulative)
    {
        Map<IResource, TreeMap<Integer, Double>> h = new HashMap<IResource, TreeMap<Integer, Double>>();
        for (Map.Entry<IResource, ResourceClientHistogram> e: histos.entrySet()) {
            IResource r = e.getKey();
            if (selected.contains(r)) {
                if (cumulative) {
                    h.put(r, e.getValue().getNormalizedCumulativeClientUsage());
                } else {
                    h.put(r, e.getValue().getNormalizedClientUsage());
                }
            }
        }
        return h;
    }
}
