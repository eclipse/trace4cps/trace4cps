/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.stl.impl;

import java.util.Collections;
import java.util.List;

import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.impl.Interval;
import org.eclipse.trace4cps.core.impl.Psop;
import org.eclipse.trace4cps.core.impl.PsopFragment;

public class StlTrue extends AbstractStlFormula {
    @Override
    protected IPsop computeSignal() {
        Psop signal = new Psop();
        signal.setAttribute("id", "TRUE");
        signal.add(new PsopFragment(Double.POSITIVE_INFINITY, 0d, 0d,
                new Interval(Double.NEGATIVE_INFINITY, false, Double.POSITIVE_INFINITY, true)));
        return signal;
    }

    @Override
    public List<MtlFormula> getChildren() {
        return Collections.emptyList();
    }

    @Override
    public String toString() {
        return "TRUE";
    }
}
