/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.stl.impl;

import java.util.Arrays;
import java.util.List;

import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.stl.StlFormula;
import org.eclipse.trace4cps.core.IPsop;

public class StlUntil extends AbstractStlFormula {
    private final StlFormula f1;

    private final StlFormula f2;

    private final double a;

    private final double b;

    public StlFormula getLeft() {
        return f1;
    }

    public StlFormula getRight() {
        return f2;
    }

    public StlUntil(StlFormula f1, StlFormula f2, double a, double b) {
        if (b <= a) {
            throw new IllegalArgumentException("b must be larger than a");
        }
        this.f1 = f1;
        this.f2 = f2;
        this.a = a;
        this.b = b;
    }

    public boolean isUntimed() {
        return a == 0d && b == Double.POSITIVE_INFINITY;
    }

    @Override
    public List<MtlFormula> getChildren() {
        return Arrays.asList(f1, f2);
    }

    @Override
    protected IPsop computeSignal() {
        if (f1 instanceof StlTrue) {
            return STLUtil.signal_eventually(f2.getSignal(), a, b);
        } else {
            throw new UnsupportedOperationException("general Until is not supported");
        }
    }

    @Override
    public String toString() {
        return "(" + f1 + " U_[" + a + "," + b + "] " + f2 + ")";
    }
}
