/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.stl.impl;

import java.util.Arrays;
import java.util.List;

import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.stl.StlFormula;
import org.eclipse.trace4cps.core.IPsop;

public class StlNeg extends AbstractStlFormula {
    private final StlFormula f;

    public StlNeg(StlFormula f) {
        this.f = f;
    }

    @Override
    protected IPsop computeSignal() {
        return STLUtil.signalNegate(f.getSignal());
    }

    public StlFormula getFormula() {
        return f;
    }

    @Override
    public List<MtlFormula> getChildren() {
        return Arrays.asList(f);
    }

    @Override
    public String toString() {
        return "(NOT " + f + ")";
    }
}
