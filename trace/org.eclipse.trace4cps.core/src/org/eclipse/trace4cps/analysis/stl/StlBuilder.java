/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.stl;

import org.eclipse.trace4cps.analysis.stl.impl.StlBinBool;
import org.eclipse.trace4cps.analysis.stl.impl.StlBinBool.BinOp;
import org.eclipse.trace4cps.analysis.stl.impl.StlEq;
import org.eclipse.trace4cps.analysis.stl.impl.StlGeq;
import org.eclipse.trace4cps.analysis.stl.impl.StlLeq;
import org.eclipse.trace4cps.analysis.stl.impl.StlNeg;
import org.eclipse.trace4cps.analysis.stl.impl.StlTrue;
import org.eclipse.trace4cps.analysis.stl.impl.StlUntil;
import org.eclipse.trace4cps.core.IPsop;

/**
 * This utility class provides methods to build {@link StlFormula} instances.
 */
public class StlBuilder {
    private static final StlFormula T = new StlTrue();

    private StlBuilder() {
    }

    /**
     * Creates the STL formula for {@code p(t) = c}.
     */
    public static StlFormula EQ(IPsop p, double c) {
        return new StlEq(p, c);
    }

    /**
     * Creates the STL formula for {@code p(t) >= c}.
     */
    public static StlFormula GEQ(IPsop p, double c) {
        return new StlGeq(p, c);
    }

    /**
     * Creates the STL formula for {@code p(t) < c}.
     */
    public static StlFormula LEQ(IPsop p, double c) {
        return new StlLeq(p, c);
    }

    /**
     * Creates the STL formula for {@code ! f}.
     */
    public static StlFormula NOT(StlFormula f) {
        if (f instanceof StlNeg) {
            return ((StlNeg)f).getFormula();
        }
        return new StlNeg(f);
    }

    /**
     * Creates the STL formula for {@code f1 AND f2}.
     */
    public static StlFormula AND(StlFormula f1, StlFormula f2) {
        return new StlBinBool(f1, BinOp.AND, f2);
    }

    /**
     * Creates the STL formula for {@code f1 OR f2}.
     */
    public static StlFormula OR(StlFormula f1, StlFormula f2) {
        return new StlBinBool(f1, BinOp.OR, f2);
    }

    /**
     * Creates the STL formula for {@code f1 => f2}.
     */
    public static StlFormula IMPLY(StlFormula f1, StlFormula f2) {
        return new StlBinBool(f1, BinOp.IMPLY, f2);
    }

    /**
     * @return constant STL formula for {@code true}
     */
    public static StlFormula TRUE() {
        return T;
    }

    /**
     * Creates the STL formula for {@code F_[0,\infty) f}.
     */
    public static StlFormula F(StlFormula f) {
        return new StlUntil(T, f, 0, Double.POSITIVE_INFINITY);
    }

    /**
     * Creates the STL formula for {@code F_[a,b] f}.
     */
    public static StlFormula F(StlFormula f, double a, double b) {
        return new StlUntil(T, f, a, b);
    }

    /**
     * Creates the STL formula for {@code G_[0,\infty) f}.
     */
    public static StlFormula G(StlFormula f) {
        return NOT(F(NOT(f), 0, Double.POSITIVE_INFINITY));
    }

    /**
     * Creates the STL formula for {@code G_[a, b] f}.
     */
    public static StlFormula G(StlFormula f, double a, double b) {
        return NOT(F(NOT(f), a, b));
    }

    /**
     * Creates the STL formula for {@code f1 U_[0,\infty) f2}.
     *
     * <p>
     * <b> NOTE: general Until (i.e., formulas where {@code f1 != TRUE}) is not supported! </b>
     * </p>
     */
    public static StlFormula U(StlFormula f1, StlFormula f2) {
        return new StlUntil(f1, f2, 0, Double.POSITIVE_INFINITY);
    }

    /**
     * Creates the STL formula for {@code f1 U_[a,b] f2}.
     *
     * <p>
     * <b> NOTE: general Until (i.e., formulas where {@code f1 != TRUE}) is not supported! </b>
     * </p>
     */
    public static StlFormula U(StlFormula f1, StlFormula f2, double a, double b) {
        return new StlUntil(f1, f2, a, b);
    }
}
