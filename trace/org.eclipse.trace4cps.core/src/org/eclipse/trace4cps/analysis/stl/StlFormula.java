/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.stl;

import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.core.IPsop;

/**
 * A Signal Temporal Logic formula. Its space-robustness is given by the {@link #getRho()} method. Instances are created
 * by the {@link StlBuilder} type. The {@link MtlFormula} is extended so that STL formulas can be used as subformulas of
 * an MTL formula. This realizes (a subset of) the logic STL-mx of the FORMATS 2019 paper by T. Ferrere et al.
 */
public interface StlFormula extends MtlFormula {
    /**
     * @return the space robustness of the formula
     */
    double getRho();

    /**
     * @return the {@code rho} signal of this formula
     */
    IPsop getSignal();
}
