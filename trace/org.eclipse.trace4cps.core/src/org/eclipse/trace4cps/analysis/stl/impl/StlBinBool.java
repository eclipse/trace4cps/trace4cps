/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.stl.impl;

import java.util.Arrays;
import java.util.List;

import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.stl.StlFormula;
import org.eclipse.trace4cps.core.IPsop;

public class StlBinBool extends AbstractStlFormula {
    public enum BinOp {
        AND, OR, IMPLY
    }

    private final StlFormula left;

    private final StlFormula right;

    private final BinOp bop;

    public StlBinBool(StlFormula left, BinOp bop, StlFormula right) {
        this.left = left;
        this.right = right;
        this.bop = bop;
    }

    @Override
    public List<MtlFormula> getChildren() {
        return Arrays.asList(left, right);
    }

    @Override
    protected IPsop computeSignal() {
        switch (bop) {
            case AND:
                return STLUtil.signalAnd(left.getSignal(), right.getSignal());
            case OR:
                return STLUtil.signalOr(left.getSignal(), right.getSignal());
            case IMPLY:
                return STLUtil.signalImply(left.getSignal(), right.getSignal());
            default:
                throw new IllegalStateException();
        }
    }

    public StlFormula getLeft() {
        return left;
    }

    public StlFormula getRight() {
        return right;
    }

    @Override
    public String toString() {
        return "(" + left + " " + bop + " " + right + ")";
    }
}
