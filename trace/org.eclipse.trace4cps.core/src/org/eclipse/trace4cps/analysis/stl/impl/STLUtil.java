/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.stl.impl;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.trace4cps.analysis.signal.impl.PsopHelper;
import org.eclipse.trace4cps.analysis.signal.impl.PsopHelper.ShapeSegment;
import org.eclipse.trace4cps.analysis.stl.StlFormula;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.IPsopFragment;
import org.eclipse.trace4cps.core.Shape;
import org.eclipse.trace4cps.core.impl.Interval;
import org.eclipse.trace4cps.core.impl.Psop;
import org.eclipse.trace4cps.core.impl.PsopFragment;

/**
 * All functions create new Psop instances!
 */
public class STLUtil {
    private STLUtil() {
    }

    /**
     * @param phi any {@link StlFormula}
     * @return all subformulas of the given formula (including itself) excluding {@code true}
     */
    public static Set<StlFormula> getFormulas(StlFormula phi) {
        Set<StlFormula> r = new HashSet<StlFormula>();
        getFormulas(phi, r);
        return r;
    }

    private static void getFormulas(StlFormula phi, Set<StlFormula> r) {
        if (!(phi instanceof StlTrue)) {
            r.add(phi);
        }
        if (phi instanceof StlBinBool) {
            getFormulas(((StlBinBool)phi).getLeft(), r);
            getFormulas(((StlBinBool)phi).getRight(), r);
        } else if (phi instanceof StlEq) {
            //
        } else if (phi instanceof StlGeq) {
            //
        } else if (phi instanceof StlLeq) {
            //
        } else if (phi instanceof StlNeg) {
            getFormulas(((StlNeg)phi).getFormula(), r);
        } else if (phi instanceof StlUntil) {
            getFormulas(((StlUntil)phi).getLeft(), r);
            getFormulas(((StlUntil)phi).getRight(), r);
        }
    }

    public static IPsop signal_greaterEqual(IPsop f, double c) {
        Psop r = new Psop();
        for (IPsopFragment frag: f.getFragments()) {
            r.add(new PsopFragment(frag.getC().doubleValue() - c, frag.getB(), frag.getA(), frag.dom()));
        }
        return r;
    }

    public static IPsop signal_lessEqual(IPsop f, double c) {
        Psop r = new Psop();
        for (IPsopFragment frag: f.getFragments()) {
            r.add(new PsopFragment(c - frag.getC().doubleValue(), -frag.getB().doubleValue(),
                    -frag.getA().doubleValue(), frag.dom()));
        }
        return r;
    }

    public static IPsop signal_equal(IPsop f, double c) {
        IPsop f1 = signal_greaterEqual(f, c);
        IPsop f2 = signal_lessEqual(f, c);
        return signalAnd(f1, f2);
    }

    public static Psop signalNegate(IPsop f) {
        Psop r = new Psop();
        for (IPsopFragment frag: f.getFragments()) {
            r.add(new PsopFragment(-frag.getC().doubleValue(), -frag.getB().doubleValue(), -frag.getA().doubleValue(),
                    frag.dom()));
        }
        return r;
    }

    public static IPsop signalImply(IPsop f1, IPsop f2) {
        IPsop notf2 = signalNegate(f2);
        IPsop f1andnotf2 = signalAnd(f1, notf2);
        return signalNegate(f1andnotf2);
    }

    public static IPsop signalAnd(IPsop f1, IPsop f2) {
        return signalAndOr(f1, f2, true);
    }

    public static Psop signalOr(IPsop f1, IPsop f2) {
        return signalAndOr(f1, f2, false);
    }

    private static Psop signalAndOr(IPsop f1, IPsop f2, boolean applyMin) {
        Psop r = new Psop();
        // Align the two position functions:
        IPsop g1 = PsopHelper.copy(f1);
        PsopHelper.alignWith(g1, f2);
        IPsop g2 = PsopHelper.copy(f2);
        PsopHelper.alignWith(g2, f1);
        // Invariant: g1 and g2 have exactly the same fragments w.r.t. time domain
        for (int i = 0; i < g1.getFragments().size(); i++) {
            IPsopFragment frag1 = g1.getFragments().get(i);
            IPsopFragment frag2 = g2.getFragments().get(i);
            signalAndOr(r, frag1, frag2, applyMin);
        }
        return r;
    }

    private static void signalAndOr(Psop r, IPsopFragment frag1, IPsopFragment frag2, boolean applyMin) {
        List<Double> intersects = PsopHelper.computeIntersections(frag1, frag2);
        if (intersects.size() == 0) {
            if ((frag1.getC().doubleValue() < frag2.getC().doubleValue()) == applyMin) {
                r.add(PsopHelper.copy(frag1));
            } else {
                r.add(PsopHelper.copy(frag2));
            }
        } else if (intersects.size() == 1) {
            splitSingleIntersection(r, intersects.get(0), frag1, frag2, applyMin);
        } else if (intersects.size() == 2) {
            Collections.sort(intersects);
            splitDoubleIntersection(r, intersects.get(0), intersects.get(1), frag1, frag2, applyMin);
        } else {
            throw new IllegalStateException();
        }
    }

    private static void splitSingleIntersection(Psop r, double tSplit, IPsopFragment frag1, IPsopFragment frag2,
            boolean applyMin)
    {
        if (tSplit == frag1.dom().lb().doubleValue()) {
            // special case for when the intersect point coincides with a domain boundary
            double size = frag1.dom().ub().doubleValue() - frag1.dom().lb().doubleValue();
            double tHalfway = frag1.dom().lb().doubleValue() + (0.5 * size);
            if ((PsopHelper.valueAt(frag1, tHalfway).doubleValue() < PsopHelper.valueAt(frag2, tHalfway)
                    .doubleValue()) == applyMin)
            {
                r.add(PsopHelper.copy(frag1));
            } else {
                r.add(PsopHelper.copy(frag2));
            }
        } else {
            // split the fragment once
            if ((frag1.getC().doubleValue() < frag2.getC().doubleValue()) == applyMin) {
                split(r, tSplit, frag1, frag2);
            } else {
                split(r, tSplit, frag2, frag1);
            }
        }
    }

    private static void splitDoubleIntersection(Psop r, double tSplit1, double tSplit2, IPsopFragment frag1,
            IPsopFragment frag2, boolean applyMin)
    {
        if (tSplit1 == frag1.dom().lb().doubleValue()) {
            splitSingleIntersection(r, tSplit2, frag1, frag2, applyMin);
            return;
        }
        if ((frag1.getC().doubleValue() < frag2.getC().doubleValue()) == applyMin) {
            Interval d1 = new Interval(frag1.dom().lb(), tSplit1);
            Interval d2 = new Interval(tSplit1, tSplit2);
            Interval d3 = new Interval(tSplit2, frag1.dom().ub());
            r.add(new PsopFragment(frag1.getC(), frag1.getB(), frag1.getA(), d1));
            double x01 = PsopHelper.valueAt(frag2, tSplit1).doubleValue();
            double v01 = PsopHelper.valueDerivativeAt(frag2, tSplit1).doubleValue();
            r.add(new PsopFragment(x01, v01, frag2.getA(), d2));
            double x02 = PsopHelper.valueAt(frag1, tSplit2).doubleValue();
            double v02 = PsopHelper.valueDerivativeAt(frag1, tSplit2).doubleValue();
            r.add(new PsopFragment(x02, v02, frag1.getA(), d3));
        } else {
            Interval d1 = new Interval(frag1.dom().lb(), tSplit1);
            Interval d2 = new Interval(tSplit1, tSplit2);
            Interval d3 = new Interval(tSplit2, frag1.dom().ub());
            r.add(new PsopFragment(frag2.getC(), frag2.getB(), frag2.getA(), d1));
            double x01 = PsopHelper.valueAt(frag1, tSplit1).doubleValue();
            double v01 = PsopHelper.valueDerivativeAt(frag1, tSplit1).doubleValue();
            r.add(new PsopFragment(x01, v01, frag1.getA(), d2));
            double x02 = PsopHelper.valueAt(frag2, tSplit2).doubleValue();
            double v02 = PsopHelper.valueDerivativeAt(frag2, tSplit2).doubleValue();
            r.add(new PsopFragment(x02, v02, frag2.getA(), d3));
        }
    }

    private static void split(Psop r, double tSplit, IPsopFragment frag1, IPsopFragment frag2) {
        Interval d1 = new Interval(frag1.dom().lb(), tSplit);
        r.add(new PsopFragment(frag1.getC(), frag1.getB(), frag1.getA(), d1));
        Interval d2 = new Interval(tSplit, frag1.dom().ub());
        double x02 = PsopHelper.valueAt(frag2, tSplit).doubleValue();
        double v02 = PsopHelper.valueDerivativeAt(frag2, tSplit).doubleValue();
        r.add(new PsopFragment(x02, v02, frag2.getA(), d2));
    }

    public static IPsop signal_eventually(IPsop f, double x, double y) {
        if (f.getFragments().size() == 0) {
            return f;
        }
        if (x >= y) {
            throw new IllegalArgumentException("x must be strictly less than y");
        }
        if (x < 0 || y < 0) {
            throw new IllegalArgumentException("x and y must be at least 0");
        }

        Interval domain = new Interval(PsopHelper.getDomainLowerBound(f),
                PsopHelper.getDomainUpperBound(f).doubleValue() - x);
        Psop r = new Psop();
        r.add(new PsopFragment(Double.NEGATIVE_INFINITY, 0d, 0d, domain));

        f = PsopHelper.copy(f); // overwrite with a copy, because monotonization changes the psop
        List<ShapeSegment> monoseg = PsopHelper.createMonotonicSegmentation(f);
        for (ShapeSegment seg: monoseg) {
            double t0 = f.getFragments().get(seg.getBeginFragment()).dom().lb().doubleValue();
            double t1 = f.getFragments().get(seg.getEndFragment()).dom().ub().doubleValue();
            double valt0 = f.getFragments().get(seg.getBeginFragment()).getC().doubleValue();
            double valt1 = PsopHelper.valueAt(f.getFragments().get(seg.getEndFragment()), t1).doubleValue();
            Psop h = new Psop();
            if (seg.getShape() == Shape.INCREASING) {
                if (y != Double.POSITIVE_INFINITY) {
                    shiftLeft(seg, f, h, y);
                }
                h.add(new PsopFragment(valt1, 0d, 0d, new Interval(t1 - y, t1 - x)));
            } else if (seg.getShape() == Shape.DECREASING) {
                h.add(new PsopFragment(valt0, 0d, 0d, new Interval(t0 - y, t0 - x)));
                shiftLeft(seg, f, h, x);
            } else if (seg.getShape() == Shape.CONSTANT) {
                h.add(new PsopFragment(valt0, 0, 0, new Interval(t0 - y, t1 - x)));
            } else {
                throw new IllegalStateException();
            }
            pad(h, domain, Double.NEGATIVE_INFINITY);
            r = signalOr(r, h);
        }
        String as = Double.toString(x).replace(".", "_");
        String bs = Double.toString(y).replace(".", "_");
        r.setAttribute("id", "F_" + as + "_" + bs + "_" + f.getAttributes().get("id"));
        return r;
    }

    /**
     * Shifts the fragments from src that are specified in the segment b to the left and adds the result to dst.
     */
    private static void shiftLeft(ShapeSegment seg, IPsop src, Psop dst, double b) {
        for (int i = seg.getBeginFragment(); i <= seg.getEndFragment(); i++) {
            shiftLeft(src.getFragments().get(i), dst, b);
        }
    }

    private static void shiftLeft(IPsopFragment frag, Psop dst, double b) {
        // shift the fragments b to the left
        double lb = frag.dom().lb().doubleValue() - b;
        double ub = frag.dom().ub().doubleValue() - b;
        if (lb < ub) { // lb can get equal to ub due to rounding in floating point arithmetic
            PsopFragment shifted = new PsopFragment(frag.getC(), frag.getB(), frag.getA(), new Interval(lb, ub));
            dst.add(shifted);
        }
    }

    private static void pad(Psop f, Interval d, double value) {
        double lb = PsopHelper.getDomainLowerBound(f).doubleValue();
        double ub = PsopHelper.getDomainUpperBound(f).doubleValue();
        boolean needProjection = false;
        if (lb > d.lb().doubleValue()) {
            f.addAtBegin(new PsopFragment(value, 0d, 0d, new Interval(d.lb(), lb)));
        } else if (lb < d.lb().doubleValue()) {
            needProjection = true;
        }
        if (ub < d.ub().doubleValue()) {
            f.add(new PsopFragment(value, 0d, 0d, new Interval(ub, d.ub())));
        } else if (ub > d.ub().doubleValue()) {
            needProjection = true;
        }
        if (needProjection) {
            PsopHelper.projectTo(f, d);
        }
    }
}
