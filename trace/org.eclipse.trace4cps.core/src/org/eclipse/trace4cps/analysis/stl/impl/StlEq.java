/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.stl.impl;

import java.util.Collections;
import java.util.List;

import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.core.IPsop;

public class StlEq extends AbstractStlFormula {
    private final IPsop f;

    private final double c;

    public StlEq(IPsop f, double c) {
        this.f = f;
        this.c = c;
    }

    @Override
    public List<MtlFormula> getChildren() {
        return Collections.emptyList();
    }

    @Override
    protected IPsop computeSignal() {
        return STLUtil.signal_equal(f, c);
    }

    @Override
    public String toString() {
        return f.getAttributes().get("id") + "=" + c;
    }
}
