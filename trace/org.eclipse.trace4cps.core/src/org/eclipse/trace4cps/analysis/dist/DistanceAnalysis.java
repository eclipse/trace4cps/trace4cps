/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.dist;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.trace4cps.analysis.constraintgraph.ConstraintConfig;
import org.eclipse.trace4cps.analysis.constraintgraph.impl.Constraint;
import org.eclipse.trace4cps.analysis.constraintgraph.impl.ConstraintGraph;
import org.eclipse.trace4cps.analysis.constraintgraph.impl.ConstraintGraphNode;
import org.eclipse.trace4cps.analysis.constraintgraph.impl.EventNode;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.ITrace;

/**
 * This class provides methods to compute a distance between sets of {@link IEvent} instances. References:
 * <ul>
 * <li>M. Hendriks, J. Verriet, T. Basten, B. Theelen, M. Brasse, L. Somers. Analyzing Execution Traces - Critical-Path
 * Analysis and Distance Analysis. International Journal on Software Tools for Technology Transfer, STTT. 19(4):487-510,
 * August 2017.</li>
 * </ul>
 *
 * <p>
 * The algorithm needs a {@link Representation} implementation to represents {@link IEvent} instances as strings. This
 * is used by the algorithm to determine equality of events of the different given event sets. It is essential that
 * within each event within a list of events has a unique string representation.
 * </p>
 */
public final class DistanceAnalysis {
    private DistanceAnalysis() {
    }

    /**
     * Computes the distance between two lists of events as a natural number ({@code >= 0}).
     *
     * @param e1 the first list of events
     * @param e2 the second list of events
     * @param r the representation used for equality computation
     * @return the distance between the lists of events ({@code >= 0})
     */
    public static long distance(ITrace e1, ITrace e2, Representation r) {
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setApplyOrderingHeuristic(Double.POSITIVE_INFINITY);
        ConstraintGraph g1 = new ConstraintGraph(e1, cfg);
        ConstraintGraph g2 = new ConstraintGraph(e2, cfg);
        return distance(g1, g2, r);
    }

    public static long distance(List<IEvent> e1, List<IEvent> e2, Representation r) {
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setApplyOrderingHeuristic(Double.POSITIVE_INFINITY);
        ConstraintGraph g1 = new ConstraintGraph(e1, cfg);
        ConstraintGraph g2 = new ConstraintGraph(e2, cfg);
        return distance(g1, g2, r);
    }

    private static long distance(ConstraintGraph cg1, ConstraintGraph cg2, Representation r) {
        long distance = 0;
        Set<VertexClaimWrapper> v1 = getVerticesAsSet(cg1, r);
        Set<VertexClaimWrapper> v2 = getVerticesAsSet(cg2, r);
        Set<VertexClaimWrapper> symdif1 = computeSymDif(v1, v2);
        distance += symdif1.size();
        Set<EdgeClaimWrapper> e1 = getEdges(cg1, r);
        Set<EdgeClaimWrapper> e2 = getEdges(cg2, r);
        Set<EdgeClaimWrapper> symdif2 = computeSymDif(e1, e2);
        distance += symdif2.size();
        return distance;
    }

    /**
     * Computes the distance between two lists of events as a natural number ({@code >=0}). It also annotates the
     * reference list of events through the {@link DistanceResult#getInterestCount()} interest count map.
     *
     * @param e1 a list of events (the "reference")
     * @param e2 a list of events
     * @param r a representation function for the events
     * @param annotateReference whether the reference trace will be included in the annotation of
     *     {@link DistanceResult#getInterestCount()}
     * @return a {@link DistanceResult}
     */
    public static DistanceResult distance(ITrace e1, ITrace e2, Representation r, boolean annotateReference) {
        ConstraintConfig cfg = ConstraintConfig.getDefault();
        cfg.setApplyOrderingHeuristic(Double.POSITIVE_INFINITY);
        ConstraintGraph g1 = new ConstraintGraph(e1, cfg);
        ConstraintGraph g2 = new ConstraintGraph(e2, cfg);
        return run(g1, g2, r, annotateReference);
    }

    private static DistanceResult run(ConstraintGraph g1, ConstraintGraph g2, Representation r,
            boolean annotateReference)
    {
        long distance = 0;
        Map<IEvent, InterestWrapper> interestCount = new HashMap<>();

        // Annotate the vertices
        Map<String, VertexClaimWrapper> v1 = getVertices(g1, r);
        Map<String, VertexClaimWrapper> v2 = getVertices(g2, r);
        Set<VertexClaimWrapper> v1minv2 = computeSetMinus(v1.values(), v2.values());
        Set<VertexClaimWrapper> v2minv1 = computeSetMinus(v2.values(), v1.values());
        distance += (v1minv2.size() + v2minv1.size());
        if (annotateReference) {
            for (VertexClaimWrapper v: v1minv2) {
                interestCount.put(v.node.getEvent(), new InterestWrapper(v.node.getEvent()));
            }
        }
        for (VertexClaimWrapper v: v2minv1) {
            interestCount.put(v.node.getEvent(), new InterestWrapper(v.node.getEvent()));
        }

        // Annotate the edges:
        Set<EdgeClaimWrapper> e1 = getEdges(g1, r);
        Set<EdgeClaimWrapper> e2 = getEdges(g2, r);
        Set<EdgeClaimWrapper> e1mine2 = computeSetMinus(e1, e2);
        Set<EdgeClaimWrapper> e2mine1 = computeSetMinus(e2, e1);
        distance += (e1mine2.size() + e2mine1.size());
        annotateEdges(interestCount, annotateReference, true, e1mine2, v2);
        annotateEdges(interestCount, true, annotateReference, e2mine1, v1);

        // Convert:
        Map<Integer, List<IEvent>> ic = new HashMap<>();
        for (InterestWrapper w: interestCount.values()) {
            List<IEvent> list = ic.get(w.cnt);
            if (list == null) {
                list = new ArrayList<>();
                ic.put(w.cnt, list);
            }
            list.add(w.event);
        }

        return new DistanceResult(distance, ic);
    }

    private static void annotateEdges(Map<IEvent, InterestWrapper> interestCount, boolean annotateV1,
            boolean annotateV2, Set<EdgeClaimWrapper> e1mine2, Map<String, VertexClaimWrapper> v2)
    {
        for (EdgeClaimWrapper e: e1mine2) {
            // e.src and e.dst are in V1, the reference trace:
            if (annotateV1) {
                increment(e.getSrc(), interestCount);
                increment(e.getDst(), interestCount);
            }
            if (annotateV2) {
                // Find equivalent tasks in V2 to annotate a missing edge
                VertexClaimWrapper w = v2.get(e.srcRep);
                if (w != null) {
                    increment(w.node.getEvent(), interestCount);
                }
                w = v2.get(e.dstRep);
                if (w != null) {
                    increment(w.node.getEvent(), interestCount);
                }
            }
        }
    }

    private static void increment(IEvent t, Map<IEvent, InterestWrapper> interestCount) {
        InterestWrapper w = interestCount.get(t);
        if (w == null) {
            interestCount.put(t, new InterestWrapper(t));
        } else {
            w.increment();
        }
    }

    private static Map<String, VertexClaimWrapper> getVertices(ConstraintGraph g, Representation repr) {
        Map<String, VertexClaimWrapper> r = new HashMap<String, VertexClaimWrapper>();
        int dup = 1;
        for (ConstraintGraphNode n: g.getNodes()) {
            VertexClaimWrapper w = new VertexClaimWrapper((EventNode)n, repr);
            if (r.containsKey(w.representation)) {
                // we have a claims/vertices with identical attributes (e.g., due to
                // preemption,...)
                // add a duplication counter: the nodes in CG are ordered according to timestamp
                w = new VertexClaimWrapper((EventNode)n, repr, dup);
                dup++;
            }
            r.put(w.representation, w);
        }
        return r;
    }

    private static Set<VertexClaimWrapper> getVerticesAsSet(ConstraintGraph g, Representation repr) {
        Set<VertexClaimWrapper> r = new HashSet<VertexClaimWrapper>();
        for (ConstraintGraphNode n: g.getNodes()) {
            r.add(new VertexClaimWrapper((EventNode)n, repr));
        }
        return r;
    }

    private static Set<EdgeClaimWrapper> getEdges(ConstraintGraph g, Representation repr) {
        Set<EdgeClaimWrapper> r = new HashSet<EdgeClaimWrapper>();
        for (ConstraintGraphNode e: g.getNodes()) {
            for (Constraint c: e.constraints()) {
                r.add(new EdgeClaimWrapper(c, repr));
            }
        }
        return r;
    }

    private static <T> Set<T> computeSymDif(Set<T> s1, Set<T> s2) {
        Set<T> result = new HashSet<T>();
        for (T o1: s1) {
            if (!s2.contains(o1)) {
                result.add(o1);
            }
        }
        for (T o2: s2) {
            if (!s1.contains(o2)) {
                result.add(o2);
            }
        }
        return result;
    }

    /**
     * @return s1 \ s2
     */
    private static <T> Set<T> computeSetMinus(Collection<T> s1, Collection<T> s2) {
        Set<T> result = new HashSet<T>();
        for (T o1: s1) {
            if (!s2.contains(o1)) {
                result.add(o1);
            }
        }
        return result;
    }

    private static final class VertexClaimWrapper {
        private final String representation;

        private final EventNode node;

        private VertexClaimWrapper(EventNode c, Representation r) {
            this.node = c;
            this.representation = r.represent(c.getEvent());
        }

        private VertexClaimWrapper(EventNode c, Representation r, int dup) {
            this.node = c;
            this.representation = r.represent(c.getEvent()) + ";dup=" + dup;
        }

        @Override
        public int hashCode() {
            return (representation == null) ? 0 : representation.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            VertexClaimWrapper other = (VertexClaimWrapper)obj;
            if (representation == null) {
                if (other.representation != null) {
                    return false;
                }
            } else if (!representation.equals(other.representation)) {
                return false;
            }
            return true;
        }

        @Override
        public String toString() {
            return "VertexClaimWrapper [representation=" + representation + "]";
        }
    }

    private static final class EdgeClaimWrapper {
        private final String representation;

        private final EventNode src;

        private final EventNode dst;

        private final String srcRep;

        private final String dstRep;

        private EdgeClaimWrapper(Constraint c, Representation r) {
            this.src = (EventNode)c.getSrc();
            this.dst = (EventNode)c.getDst();
            this.srcRep = r.represent(src.getEvent());
            this.dstRep = r.represent(dst.getEvent());
            StringBuilder b = new StringBuilder();
            b.append(srcRep).append("->").append(dstRep);
            this.representation = b.toString();
        }

        private IEvent getSrc() {
            return src.getEvent();
        }

        private IEvent getDst() {
            return dst.getEvent();
        }

        @Override
        public int hashCode() {
            return (representation == null) ? 0 : representation.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            EdgeClaimWrapper other = (EdgeClaimWrapper)obj;
            if (representation == null) {
                if (other.representation != null) {
                    return false;
                }
            } else if (!representation.equals(other.representation)) {
                return false;
            }
            return true;
        }

        @Override
        public String toString() {
            return representation;
        }
    }

    private static final class InterestWrapper {
        private int cnt;

        private final IEvent event;

        public InterestWrapper(IEvent e) {
            this.event = e;
            cnt = 1;
        }

        public void increment() {
            cnt++;
        }
    }
}
