/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.dist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.trace4cps.core.IClaimEvent;
import org.eclipse.trace4cps.core.IDependency;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.impl.Dependency;

/**
 * This type provides a check on two traces. One trace typically comes from a model (e.g., an LSAT model) and contains
 * dependencies between claims and/or events. The other trace typically is from a system. The assumption is that a
 * mapping exists between the claims and events in the two traces. This mapping is the equality of attribute-values.
 * E.g., a claim in the model trace is equivalent to a claim in the system trace if and only if they have exactly the
 * same set of attributes and values. Furthermore, we assume that a dependency in the model trace encodes a
 * timing/ordering dependency: the destination of the dependency does not happen before the source of the dependency. We
 * can also quantify the minimal time distance by a special attribute of the dependency that is interpreted as a time
 * duration in the time unit of the traces (which must be equal). The algorithm in this class then checks whether every
 * dependency in the model trace is respected in the system trace.
 */
public class DependencyInclusionCheck {
    private DependencyInclusionCheck() {
    }

    /**
     * @param weightAttribute the attribute to interpret as the minimum time distance for the dependencies (can be
     *     {@code null} in which case a value of 0 is used)
     * @return constraints from the model trace which are not satisfied by the system trace
     */
    public static List<IDependency> check(ITrace modelTrace, ITrace systemTrace, String weightAttribute) {
        if (modelTrace.getTimeUnit() != systemTrace.getTimeUnit()) {
            throw new IllegalArgumentException("traces must use the same time unit");
        }

        List<IDependency> systemDependencies = new ArrayList<>();
        Map<IEvent, IEvent> cache = new HashMap<>();
        for (IDependency d: modelTrace.getDependencies()) {
            IEvent e1 = get(cache, d.getSrc(), systemTrace);
            IEvent e2 = get(cache, d.getDst(), systemTrace);
            if (e1 != null && e2 != null) {
                double weight = getWeightFromAttribute(d, weightAttribute);
                if (e1.getTimestamp().doubleValue() + weight > e2.getTimestamp().doubleValue()) {
                    IDependency annotatedDep = new Dependency(e1, e2);
                    annotatedDep.setAttribute(weightAttribute, Double.toString(weight));
                    systemDependencies.add(annotatedDep);
                }
            }
        }
        return systemDependencies;
    }

    private static double getWeightFromAttribute(IDependency d, String weightAttribute) {
        if (weightAttribute == null) {
            return 0d;
        }
        String w = d.getAttributeValue(weightAttribute);
        if (w != null) {
            try {
                return Double.parseDouble(w);
            } catch (NumberFormatException e) {
                return 0d;
            }
        }
        return 0d;
    }

    /**
     * Gets a system event from the system trace that is the equivalent of the given model event. Already computed pairs
     * are stored in the cache.
     *
     * @param cache the cache of model-system event pairs
     * @param modelEvent the model event for which to find the equivalent system event
     * @param systemTrace the trace with all system events
     * @return the equivalent system event, or {@code null} if it cannot be found
     */
    private static IEvent get(Map<IEvent, IEvent> cache, IEvent modelEvent, ITrace systemTrace) {
        IEvent r = cache.get(modelEvent);
        if (r != null) {
            return r;
        }
        for (IEvent sysEvent: systemTrace.getEvents()) {
            if (eventTypeEquals(modelEvent, sysEvent) && modelEvent.getAttributes().equals(sysEvent.getAttributes())) {
                cache.put(modelEvent, sysEvent);
                return sysEvent;
            }
        }
        return null;
    }

    private static boolean eventTypeEquals(IEvent lsatEvent, IEvent sysEvent) {
        if (lsatEvent instanceof IClaimEvent && !(sysEvent instanceof IClaimEvent)) {
            return false;
        } else if (!(lsatEvent instanceof IClaimEvent) && sysEvent instanceof IClaimEvent) {
            return false;
        } else if (lsatEvent instanceof IClaimEvent && sysEvent instanceof IClaimEvent) {
            if (((IClaimEvent)lsatEvent).getType() != ((IClaimEvent)sysEvent).getType()) {
                return false;
            }
        }
        return true;
    }
}
