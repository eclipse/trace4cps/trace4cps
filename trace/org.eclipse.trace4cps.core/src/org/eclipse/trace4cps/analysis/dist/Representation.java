/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.dist;

import org.eclipse.trace4cps.core.IEvent;

/**
 * Provides a string representation of an {@link IEvent} for the difference analysis.
 */
public interface Representation {
    /**
     * @param e the {@link IEvent} to represent
     * @return the string representation of the event
     */
    String represent(IEvent e);
}
