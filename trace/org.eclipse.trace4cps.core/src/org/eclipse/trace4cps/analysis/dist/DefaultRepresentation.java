/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.dist;

import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.impl.TraceHelper;

/**
 * Default implementation of the {@link Representation} interface. An {@link IEvent} is represented by a string with the
 * keys and values of its attributes (in lexicographical order w.r.t. the keys). Furthermore, {@link IClaimEvent}
 * instances also have the keys and values of the resource attributes, the amount and offset that is claimed, and the
 * type of {@link IClaimEvent}.
 */
public class DefaultRepresentation implements Representation {
    /**
     * {@inheritDoc}
     */
    @Override
    public String represent(IEvent e) {
        return TraceHelper.represent(e, true, true);
    }
}
