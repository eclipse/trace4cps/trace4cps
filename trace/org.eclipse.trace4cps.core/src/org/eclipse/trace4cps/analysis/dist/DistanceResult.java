/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.dist;

import java.util.List;
import java.util.Map;

import org.eclipse.trace4cps.core.IEvent;

/**
 * The result of a {@link DistanceAnalysis} computation.
 */
public final class DistanceResult {
    private final long distance;

    private final Map<Integer, List<IEvent>> interestCount;

    DistanceResult(long distance, Map<Integer, List<IEvent>> interestCount) {
        this.distance = distance;
        this.interestCount = interestCount;
    }

    /**
     * @return the distance between two event lists (this is a pseudo-metric on the set of traces)
     */
    public long getDistance() {
        return distance;
    }

    /**
     * This method returns a map that gives a heuristic <i>interest</i> number to a subset of events. The higher the
     * interest number, the more the event is involved in the distance. This information can, e.g., be used for
     * visualization purposes.
     *
     * @return the interest map
     */
    public Map<Integer, List<IEvent>> getInterestCount() {
        return interestCount;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Value[dist=" + distance + ",map=" + interestCount + "]";
    }
}
