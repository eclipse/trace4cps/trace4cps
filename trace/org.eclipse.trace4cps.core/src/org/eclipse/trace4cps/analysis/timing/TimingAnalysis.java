/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.timing;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.impl.TraceHelper;

public class TimingAnalysis {
    private TimingAnalysis() {
    }

    public static Map<String, StatisticsManager> analyse(ITrace trace, Collection<String> claimGroupingAttributes) {
        Map<String, StatisticsManager> r = new HashMap<>();
        Map<String, List<IClaim>> p = createPartition(trace, claimGroupingAttributes);
        for (Map.Entry<String, List<IClaim>> e: p.entrySet()) {
            String name = e.getKey();
            StatisticsManager stat = new StatisticsManager(trace.getTimeUnit());
            r.put(name, stat);
            List<IClaim> claims = e.getValue();
            for (IClaim c: claims) {
                double t = BigDecimal.valueOf(c.getEndTime().doubleValue())
                        .subtract(BigDecimal.valueOf(c.getStartTime().doubleValue())).doubleValue();
                stat.addSample(t);
            }
            stat.autoScale();
        }
        return r;
    }

    private static Map<String, List<IClaim>> createPartition(ITrace trace, Collection<String> claimGroupingAttributes) {
        Map<String, List<IClaim>> p = new HashMap<>();
        for (IClaim c: trace.getClaims()) {
            String attVals = TraceHelper.getValues(c, claimGroupingAttributes, false);
            List<IClaim> cell = p.get(attVals);
            if (cell == null) {
                cell = new ArrayList<>();
                p.put(attVals, cell);
            }
            cell.add(c);
        }
        return p;
    }
}
