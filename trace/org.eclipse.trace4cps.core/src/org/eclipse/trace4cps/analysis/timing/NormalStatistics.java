/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.timing;

import java.util.List;

public class NormalStatistics {
    private final double mean;

    private final double stddev;

    public NormalStatistics(StatisticsManager mgr) {
        double sum = 0.0;
        double powSum = 0.0;

        List<Double> timings = mgr.getSamples();

        for (double d: timings) {
            sum += d;
        }
        mean = sum / timings.size();

        for (double d: timings) {
            powSum += Math.pow(d - mean, 2);
        }

        stddev = Math.sqrt(powSum / timings.size());
    }

    public double getMean() {
        return mean;
    }

    public double getSd() {
        return stddev;
    }
}
