/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.timing;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class StatisticsManager {
    private TimeUnit timeUnit;

    private final List<Double> samples = new ArrayList<>();

    public StatisticsManager(TimeUnit tu) {
        timeUnit = tu;
    }

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    public void addSample(double v) {
        samples.add(v);
    }

    public int getNumSamples() {
        return samples.size();
    }

    public List<Double> getSamples() {
        return samples;
    }

    public NormalStatistics getNormalStatistics() {
        return new NormalStatistics(this);
    }

    /**
     * Tries to get all samples to the format with at least 1 digit and at most 3 digits before the decimal separator.
     */
    public void autoScale() {
        if (getNumSamples() <= 0) {
            return;
        }
        double mean = getNormalStatistics().getMean();
        if (mean > 1000d) {
            while (mean > 1000d) {
                if (scaleUp()) {
                    mean = getNormalStatistics().getMean();
                } else {
                    return;
                }
            }
        } else if (mean < 1d) {
            while (mean < 1d) {
                if (scaleDown()) {
                    mean = getNormalStatistics().getMean();
                } else {
                    return;
                }
            }
        }
    }

    public void scaleTo(TimeUnit tu) {
        if (tu.compareTo(timeUnit) < 0) {
            scaleSamples(tu.convert(1, timeUnit), true);
            timeUnit = tu;
        } else if (tu.compareTo(timeUnit) > 0) {
            scaleSamples(timeUnit.convert(1, tu), false);
            timeUnit = tu;
        }
    }

    /**
     * uses a larger time unit
     */
    private boolean scaleUp() {
        switch (timeUnit) {
            case NANOSECONDS:
                scaleSamples(1000, false);
                timeUnit = TimeUnit.MICROSECONDS;
                return true;
            case MICROSECONDS:
                scaleSamples(1000, false);
                timeUnit = TimeUnit.MILLISECONDS;
                return true;
            case MILLISECONDS:
                scaleSamples(1000, false);
                timeUnit = TimeUnit.SECONDS;
                return true;
            case SECONDS:
                scaleSamples(60, false);
                timeUnit = TimeUnit.MINUTES;
                return true;
            case MINUTES:
                scaleSamples(60, false);
                timeUnit = TimeUnit.HOURS;
                return true;
            default:
                return false;
        }
    }

    /**
     * uses a smaller time unit
     */
    private boolean scaleDown() {
        switch (timeUnit) {
            case MICROSECONDS:
                scaleSamples(1000, true);
                timeUnit = TimeUnit.NANOSECONDS;
                return true;
            case MILLISECONDS:
                scaleSamples(1000, true);
                timeUnit = TimeUnit.MICROSECONDS;
                return true;
            case SECONDS:
                scaleSamples(1000, true);
                timeUnit = TimeUnit.MILLISECONDS;
                return true;
            case MINUTES:
                scaleSamples(60, true);
                timeUnit = TimeUnit.SECONDS;
                return true;
            case HOURS:
                scaleSamples(60, true);
                timeUnit = TimeUnit.MINUTES;
                return true;
            default:
                return false;
        }
    }

    private void scaleSamples(long f, boolean mult) {
        List<Double> newSamples = new ArrayList<>();
        for (double d: samples) {
            if (!mult) {
                newSamples.add(
                        BigDecimal.valueOf(d).divide(BigDecimal.valueOf(f), RoundingMode.HALF_DOWN).doubleValue());
            } else {
                newSamples.add(BigDecimal.valueOf(d).multiply(BigDecimal.valueOf(f)).doubleValue());
            }
        }
        samples.clear();
        samples.addAll(newSamples);
    }
}
