/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.behavior;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.trace4cps.core.IEvent;

/**
 * A behavioral partition is a partition of a set of {@link Behavior} instances. It is assumed that there is a certain
 * attribute, the {@code idAtt}, for which every {@link IEvent} has an integer value. This can be, for instance, the
 * frame id in an image processing application, the sheet id in a print job, etc. All {@link IEvent} instances with the
 * same value for this {@code idAtt} form a single {@link Behavior} instance. The equivalence between two behaviors,
 * which is used to create the partition, is given by the zero-distance between the sets of {@link IEvent} instances.
 */
public class BehavioralPartition implements Iterable<List<Behavior>> {
    private final String idAtt;

    private final Set<String> uniquenessAtts;

    /**
     * Maps a representative behavior to the other equivalent behaviors.
     */
    private final Map<Behavior, List<Behavior>> partition;

    BehavioralPartition(Iterable<IEvent> events, String idAtt, Set<String> uniquenessAtts) {
        if (!validateObjectId(events, idAtt)) {
            throw new IllegalArgumentException(
                    "not all events have an an attribute named " + idAtt + " with an integer value");
        }
        this.idAtt = idAtt;
        this.uniquenessAtts = uniquenessAtts;
        List<Integer> objectIds = getObjectIds(events);
        List<Behavior> bl = getSingleBehaviors(events, objectIds);
        partition = computeBehavioralPartition(bl);
    }

    /**
     * @return the attribute for this partition
     */
    public String getIdAtt() {
        return idAtt;
    }

    /**
     * @return the number of cells in the partition
     */
    public int size() {
        return partition.size();
    }

    /**
     * @return the {@Link Behavior} instances that represent the equivalence classes in this partition
     */
    public Set<Behavior> getRepresentatives() {
        return partition.keySet();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<List<Behavior>> iterator() {
        return partition.values().iterator();
    }

    private Map<Behavior, List<Behavior>> computeBehavioralPartition(List<Behavior> bl) {
        Map<Behavior, List<Behavior>> freq = new HashMap<Behavior, List<Behavior>>();
        if (bl.isEmpty()) {
            throw new IllegalArgumentException();
        }
        // The first Behavior defines the first cell:
        List<Behavior> cell = new ArrayList<Behavior>();
        Behavior first = bl.get(0);
        cell.add(first);
        freq.put(first, cell);
        // Process the other behaviors:
        for (int i = 1; i < bl.size(); i++) {
            Behavior b = bl.get(i);
            // Find a cell to which it belongs, if exists
            boolean found = false;
            for (Map.Entry<Behavior, List<Behavior>> e: freq.entrySet()) {
                if (b.distanceTo(e.getKey(), uniquenessAtts) == 0) {
                    e.getValue().add(b);
                    found = true;
                    break;
                }
            }
            if (!found) {
                // Create a new cell in the partition for this behavior:
                cell = new ArrayList<Behavior>();
                cell.add(b);
                freq.put(b, cell);
            }
        }
        return freq;
    }

    private List<Behavior> getSingleBehaviors(Iterable<IEvent> events, List<Integer> objectIds) {
        List<Behavior> bl = new ArrayList<Behavior>();
        Map<Integer, Behavior> objectId2Behavior = new HashMap<Integer, Behavior>();
        for (int i = 0; i < objectIds.size(); i++) {
            int theObjectId = objectIds.get(i);
            Behavior b = new Behavior(idAtt, theObjectId);
            bl.add(b);
            objectId2Behavior.put(theObjectId, b);
        }
        // Add the events to the behaviors:
        for (IEvent event: events) {
            Integer id = getId(event);
            Behavior b = objectId2Behavior.get(id);
            b.add(event);
        }
        return bl;
    }

    private boolean validateObjectId(Iterable<IEvent> events, String idAtt) {
        for (IEvent e: events) {
            String val = e.getAttributeValue(idAtt);
            if (val == null || !isInteger(val)) {
                return false;
            }
        }
        return true;
    }

    static boolean isInteger(String val) {
        for (int i = 0; i < val.length(); i++) {
            if (Character.digit(val.charAt(i), 10) < 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param claims all claims that need to be considered
     * @param idAtt the ID attribute
     * @return a list of object ids, sorted ascending
     */
    private List<Integer> getObjectIds(Iterable<IEvent> events) {
        Set<Integer> ids = new HashSet<Integer>();
        for (IEvent c: events) {
            Integer v = getId(c);
            if (v != null) {
                ids.add(v);
            }
        }
        List<Integer> idList = new ArrayList<Integer>(ids.size());
        idList.addAll(ids);
        Collections.sort(idList);
        return idList;
    }

    private Integer getId(IEvent e) {
        String idString = e.getAttributeValue(idAtt);
        return Integer.parseInt(idString);
    }
}
