/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.behavior;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.trace4cps.core.IEvent;

/**
 * A histogram entry for a cell from a behavioral partition. It computes the common attribute-value pairs in all the
 * events of the cell.
 */
public class HistogramEntry {
    private final Map<String, String> common = new HashMap<String, String>();

    private final Set<Integer> objectIds = new HashSet<Integer>();

    private final int size;

    HistogramEntry(List<Behavior> cell) {
        size = cell.size();
        for (Behavior b: cell) {
            objectIds.add(b.getValue());
        }
        // Compute the common attribute-value pairs
        boolean first = true;
        for (Behavior b: cell) {
            for (IEvent e: b.getEvents()) {
                if (first) {
                    common.putAll(e.getAttributes());
                    first = false;
                } else {
                    intersectAttributes(e);
                }
                if (common.isEmpty()) {
                    return;
                }
            }
        }
    }

    private void intersectAttributes(IEvent event) {
        List<String> keysToRemove = new ArrayList<String>();
        for (Map.Entry<String, String> e: common.entrySet()) {
            if (!e.getValue().equals(event.getAttributeValue(e.getKey()))) {
                keysToRemove.add(e.getKey());
            }
        }
        for (String k: keysToRemove) {
            common.remove(k);
        }
    }

    /**
     * @return the attribute-value pairs that all {@link Behavior} instances in this entry have in common
     */
    public Map<String, String> getCommonAttributeValues() {
        return Collections.unmodifiableMap(common);
    }

    /**
     * @return the set of central object ids from the set of behaviors of this entry
     */
    public Set<Integer> getCentralObjectIds() {
        return objectIds;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "HistogramEntry[size=" + size + "]";
    }

    /**
     * @return the number of {@link Behavior} instances in this entry
     */
    public int size() {
        return size;
    }
}
