/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.behavior;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.trace4cps.analysis.behavior.impl.FilteringClaimRepresentation;
import org.eclipse.trace4cps.analysis.dist.DistanceAnalysis;
import org.eclipse.trace4cps.core.IEvent;

/**
 * A behavior is a set of {@link IEvent} instances with the same value for the given {@code idAtt} attribute. Such a set
 * of event models, for instance, the processing of a single image with a certain id.
 */
public final class Behavior {
    private final String idAtt;

    private final int idVal;

    private final List<IEvent> events = new ArrayList<IEvent>();

    Behavior(String idAtt, int idVal) {
        this.idAtt = idAtt;
        this.idVal = idVal;
    }

    void add(IEvent task) {
        events.add(task);
    }

    int getValue() {
        return idVal;
    }

    /**
     * @return the number of {@link IEvent} instances in this behavior
     */
    public int size() {
        return events.size();
    }

    /**
     * @return the {@link IEvent} instances of this behavior
     */
    public List<IEvent> getEvents() {
        return events;
    }

    long distanceTo(Behavior other, Set<String> uniquenessAtts) {
        Map<Integer, Integer> renaming = new HashMap<Integer, Integer>();
        renaming.put(idVal, 0);
        renaming.put(other.idVal, 0);
        FilteringClaimRepresentation rep = new FilteringClaimRepresentation(idAtt, uniquenessAtts, renaming);
        return DistanceAnalysis.distance(events, other.events, rep);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Behavior[" + idAtt + "=" + idVal + "]";
    }
}
