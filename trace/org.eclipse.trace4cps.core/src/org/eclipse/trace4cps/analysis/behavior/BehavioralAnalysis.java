/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.behavior;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.trace4cps.core.IAttributeAware;
import org.eclipse.trace4cps.core.IAttributeFilter;
import org.eclipse.trace4cps.core.IClaimEvent;
import org.eclipse.trace4cps.core.IEvent;

/**
 * This is the top-level accessor for the functionality in this package.
 */
public class BehavioralAnalysis {
    private BehavioralAnalysis() {
    }

    /**
     * Creates a partition of the behavior for the attribute {@code idAtt}. It is assumed that each {@link IEvent} has
     * an integer value for this attribute. Furthermore, some sequential process is assumed. Events with lower values
     * for the attribute happen before (more or less) events with a higher value for the attribute. An example is image
     * processing in which each event is associated with processing a frame with a certain (increasing) id.
     *
     * @param events the events to consider for the partition
     * @param idAtt the ID attribute: each event should have an integer value for it
     * @param uniquenessAtts the attributes that together with the {@code idAtt} and the resource (in case of
     *     {@link IClaimEvent} instances) make events unique
     * @return a {@link BehavioralPartition}
     */
    public static BehavioralPartition partition(Iterable<IEvent> events, String idAtt, Set<String> uniquenessAtts) {
        return new BehavioralPartition(events, idAtt, uniquenessAtts);
    }

    /**
     * Creates a histogram for a given behavioral partition. The histogram entries are sorted w.r.t. size (decreasing).
     *
     * @param partition the behavioral partition for which to create a histogram
     * @return the histogram
     */
    public static List<HistogramEntry> createBehavioralHistogram(BehavioralPartition partition) {
        List<HistogramEntry> entries = new ArrayList<HistogramEntry>();
        for (List<Behavior> cell: partition) {
            entries.add(new HistogramEntry(cell));
        }
        // sort by cell size (largest first)
        Collections.sort(entries, new Comparator<HistogramEntry>() {
            @Override
            public int compare(HistogramEntry o1, HistogramEntry o2) {
                return Integer.compare(o2.size(), o1.size());
            }
        });
        return entries;
    }

    /**
     * Computes a filter that only shows the representative events for a partition.
     *
     * @param partition a behavioral partition
     * @return a filter for {@link IEvent} instances that only shows one representative for each cell in the given
     *     partition
     */
    public static IAttributeFilter computeBehavioralRepresentativeFilter(BehavioralPartition partition) {
        if (partition.size() == 0) {
            return null;
        }
        final Set<Integer> objectIds = new HashSet<Integer>();
        for (Behavior b: partition.getRepresentatives()) {
            objectIds.add(b.getValue());
        }
        final String idAtt = partition.getIdAtt();
        return new IAttributeFilter() {
            @Override
            public boolean include(IAttributeAware a) {
                String id = a.getAttributeValue(idAtt);
                if (id != null && BehavioralPartition.isInteger(id)) {
                    return objectIds.contains(Integer.parseInt(id));
                } else {
                    return false;
                }
            }
        };
    }
}
