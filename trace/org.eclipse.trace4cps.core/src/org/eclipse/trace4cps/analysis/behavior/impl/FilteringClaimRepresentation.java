/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.behavior.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.trace4cps.analysis.dist.Representation;
import org.eclipse.trace4cps.core.ClaimEventType;
import org.eclipse.trace4cps.core.IClaimEvent;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.impl.ClaimEvent;
import org.eclipse.trace4cps.core.impl.TraceHelper;

/**
 * This representation replaces the value of the attribute given on construction according to the given map.
 * Furthermore, the string representation includes the id attribute (with the newly mapped value), the resource
 * attributes, and the uniqueness attributes.
 */
public final class FilteringClaimRepresentation implements Representation {
    private final String idAtt;

    private final Set<String> uniquenessAtts;

    private final Map<Integer, Integer> idRemapping;

    private static final Comparator<Map.Entry<String, String>> COMP = new Comparator<Map.Entry<String, String>>() {
        @Override
        public int compare(Entry<String, String> o1, Entry<String, String> o2) {
            return o1.getKey().compareTo(o2.getKey());
        }
    };

    public FilteringClaimRepresentation(String idAtt, Set<String> uniquenessAtts, Map<Integer, Integer> idRemapping) {
        this.idAtt = idAtt;
        this.uniquenessAtts = uniquenessAtts;
        this.idRemapping = idRemapping;
    }

    @Override
    public String represent(IEvent event) {
        StringBuilder b = new StringBuilder();
        // ensure that the attributes are represented lexicographically
        List<Map.Entry<String, String>> le = new ArrayList<Map.Entry<String, String>>();
        le.addAll(event.getAttributes().entrySet());
        Collections.sort(le, COMP);
        for (Map.Entry<String, String> e: le) {
            String a = e.getKey();
            String v = e.getValue();
            if (!a.equals(idAtt) && (uniquenessAtts == null || uniquenessAtts.contains(a))) {
                b.append(a).append("=").append(v).append(";");
            } else if (a.equals(idAtt)) {
                Integer vi = Integer.parseInt(v);
                b.append(a).append("=").append(idRemapping.get(vi)).append(";");
            }
        }
        if (event instanceof IClaimEvent) {
            ClaimEvent claimEvent = (ClaimEvent)event;
            b.append("res={");
            b.append(TraceHelper.getValues(claimEvent.getClaim().getResource(), true));
            b.append("};");
            if (claimEvent.getType() == ClaimEventType.START) {
                b.append("s");
            } else if (claimEvent.getType() == ClaimEventType.END) {
                b.append("e");
            }
        }
        return b.toString();
    }
}
