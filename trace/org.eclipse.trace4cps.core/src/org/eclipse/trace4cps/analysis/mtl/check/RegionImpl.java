/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl.check;

import org.eclipse.trace4cps.analysis.mtl.ExplanationTable.Region;
import org.eclipse.trace4cps.analysis.mtl.InformativePrefix;

public final class RegionImpl implements Region {
    private double startTime = -1;

    private double endTime = -1;

    private int startIndex = -1;

    private int endIndex = -1;

    private final InformativePrefix val;

    public RegionImpl(InformativePrefix v) {
        this.val = v;
    }

    public void setStart(int index, double timeStamp) {
        this.startIndex = index;
        this.startTime = timeStamp;
    }

    public void setEnd(int index, double timeStamp) {
        this.endIndex = index;
        this.endTime = timeStamp;
    }

    @Override
    public int getStartIndex() {
        return startIndex;
    }

    @Override
    public int getEndIndex() {
        return endIndex;
    }

    @Override
    public double getStartTime() {
        return startTime;
    }

    @Override
    public double getEndTime() {
        return endTime;
    }

    @Override
    public InformativePrefix getValue() {
        return val;
    }

    @Override
    public String toString() {
        return "Region [startIndex=" + startIndex + ", endIndex=" + endIndex + ", " + val + "]";
    }
}
