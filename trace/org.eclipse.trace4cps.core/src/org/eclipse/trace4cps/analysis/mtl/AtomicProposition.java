/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl;

import org.eclipse.trace4cps.core.ClaimEventType;
import org.eclipse.trace4cps.core.IAttributeAware;
import org.eclipse.trace4cps.core.IClaimEvent;
import org.eclipse.trace4cps.core.IEvent;

/**
 * An atomic proposition is a core building block for {@link MtlFormula}. It is specified by the set of attributes and
 * the (possibly null) {@link ClaimEventType}. The atomic proposition is designed to work together with the
 * {@link IEvent} and {@link IClaimEvent} instances, which both extend the {@link State} interface. Implementations of
 * the {@link State} have a {@link State#satisfies(AtomicProposition)} method that typically must the attributes and the
 * type of the atomic proposition. I.e., a state {@code s} satisfies an atomic proposition {@code p} if and only if
 * <ul>
 * <li>all attribute-value pairs of the atomic proposition {@code p} are also present in the state {@code s}, and</li>
 * <li>{@code p.}{@link #getType()}{@code == null} or state {@code s} has the same type as
 * {@code p.}{@link #getType()}}</li>
 * </ul>
 *
 */
public interface AtomicProposition extends MtlFormula, IAttributeAware {
    /**
     * Maybe {@code null} to specify any event type
     *
     * @return the type of this atomic proposition
     */
    ClaimEventType getType();
}
