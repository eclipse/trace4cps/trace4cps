/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl;

import org.eclipse.trace4cps.core.IAttributeAware;
import org.eclipse.trace4cps.core.IEvent;

/**
 * Temporal-logic checking works on a sequence of {@link State} instances. Note that {@link IEvent} extends the
 * {@link State} type.
 *
 * <p>
 * Each state has a timestamp, a number of attributes, and a method that gives the truth-value of a given
 * {@link AtomicProposition}. Both {@link State} and {@link AtomicProposition} are {@link IAttributeAware}. The
 * satisfaction relation uses the attributes, and also the {@link AtomicProposition#getType()} method.
 * </p>
 */
public interface State extends IAttributeAware {
    /**
     * @return the timestamp of this state
     */
    Number getTimestamp();

    /**
     * @param p the atomic proposition
     * @return whether this state satisfied the given atomic proposition
     */
    boolean satisfies(AtomicProposition p);
}
