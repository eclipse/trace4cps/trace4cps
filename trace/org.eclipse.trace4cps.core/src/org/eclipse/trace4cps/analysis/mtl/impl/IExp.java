/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl.impl;

import java.util.Map;

public interface IExp {
    public enum Iop {
        PLUS("+"), MINUS("-"), MULT("*"), DIV("/"), MOD("%");

        private final String rep;

        private Iop(String rep) {
            this.rep = rep;
        }

        @Override
        public String toString() {
            return rep;
        }
    }

    long evaluateLong(Map<String, Long> valuation);

    double evaluate(Map<String, Long> valuation);

    /**
     * A variable can only come from a top-level conjunction and is a natural number
     *
     * @return whether a variable is used in the expression
     */
    boolean hasVariable();

    boolean isNaturalNumber();
}
