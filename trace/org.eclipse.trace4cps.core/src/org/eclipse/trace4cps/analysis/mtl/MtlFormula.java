/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl;

import java.util.List;

/**
 * Top-level interface for metric-temporal logic formulas.
 */
public interface MtlFormula {
    /**
     * @return sub formulas of this formula
     */
    List<MtlFormula> getChildren();
}
