/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl;

/**
 * The result of a computation of the {@link MtlChecker}.
 */
public final class MtlResult {
    private final MtlFormula phi;

    private final InformativePrefix informativePrefix;

    private final ExplanationTable proof;

    MtlResult(MtlFormula phi, InformativePrefix sat, ExplanationTable proof) {
        this.phi = phi;
        this.informativePrefix = sat;
        this.proof = proof;
    }

    /**
     * @return the formula that has been checked
     */
    public MtlFormula getPhi() {
        return phi;
    }

    /**
     * @return the result of the check
     */
    public InformativePrefix informative() {
        return informativePrefix;
    }

    /**
     * An explanation is only available when it is explicitly asked to the {@link MtlChecker}. The
     * {@link ExplanationTable} is a representation of the data structure that has been used to check the formula.
     *
     * @return an explanation, or {@code null} if no explanation has been asked
     */
    public ExplanationTable getExplanation() {
        return proof;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "MTLcheckerResult [phi=" + phi + ", informativePrefix=" + informativePrefix + "]";
    }
}
