/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.trace4cps.analysis.mtl.ExplanationTable.Region;
import org.eclipse.trace4cps.analysis.stl.StlFormula;

/**
 * A utility class for handling MTL-related things.
 */
public final class MtlUtil {
    private MtlUtil() {
    }

    public static boolean isMtl(MtlFormula phi) {
        for (MtlFormula sub: getSubformulas(phi)) {
            if (sub instanceof StlFormula) {
                return false;
            }
        }
        return true;
    }

    public static boolean isStl(MtlFormula phi) {
        return phi instanceof StlFormula;
    }

    public static boolean isStlMx(MtlFormula phi) {
        return !(isStl(phi) || isMtl(phi));
    }

    public static Collection<Integer> getAtomicPropIndices(MtlResult r) {
        Set<AtomicProposition> aps = getAtomicPropositions(r.getPhi());
        ExplanationTable tab = r.getExplanation();
        Set<Integer> result = new HashSet<Integer>();
        for (AtomicProposition ap: aps) {
            for (Region region: tab.getRegions(ap)) {
                if (region.getValue() == InformativePrefix.GOOD) {
                    for (int i = region.getStartIndex(); i <= region.getEndIndex(); i++) {
                        result.add(i);
                    }
                }
            }
        }
        return result;
    }

    /**
     * Returns all subformulas of a given formula in a depth-first order (including the formula itself).
     *
     * @param phi the formula
     * @return a list of subformulas
     */
    public static List<MtlFormula> getSubformulas(MtlFormula phi) {
        List<MtlFormula> r = new ArrayList<MtlFormula>();
        depthFirstSub(phi, r);
        return r;
    }

    public static Set<AtomicProposition> getAtomicPropositions(MtlFormula phi) {
        Set<AtomicProposition> r = new HashSet<AtomicProposition>();
        depthFirstAP(phi, r);
        return r;
    }

    private static void depthFirstAP(MtlFormula phi, Set<AtomicProposition> r) {
        if (!phi.getChildren().isEmpty()) {
            for (MtlFormula c: phi.getChildren()) {
                depthFirstAP(c, r);
            }
        } else if (phi instanceof AtomicProposition) {
            r.add((AtomicProposition)phi);
        }
    }

    private static void depthFirstSub(MtlFormula phi, List<MtlFormula> r) {
        if (!phi.getChildren().isEmpty()) {
            for (MtlFormula c: phi.getChildren()) {
                depthFirstSub(c, r);
            }
        }
        if (!r.contains(phi)) {
            r.add(phi);
        }
    }
}
