/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl.impl;

import org.eclipse.trace4cps.analysis.mtl.MtlFormula;

public class MTLor extends AbstractBinopMTLformula {
    public MTLor(MtlFormula a, MtlFormula b) {
        super(a, b);
    }

    @Override
    public String toString() {
        return "(" + getLeft() + " or " + getRight() + ")";
    }
}
