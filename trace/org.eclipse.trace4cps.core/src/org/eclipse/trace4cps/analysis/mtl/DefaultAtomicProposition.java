/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.trace4cps.analysis.mtl.impl.AbstractMTLformula;
import org.eclipse.trace4cps.core.ClaimEventType;

/**
 * A default implementation of an {@link AtomicProposition} based on a key-value mapping of strings.
 */
public class DefaultAtomicProposition extends AbstractMTLformula implements AtomicProposition {
    private final Map<String, String> properties = new HashMap<>();

    private final ClaimEventType type;

    public DefaultAtomicProposition(String... properties) {
        this(null, properties);
    }

    public DefaultAtomicProposition(ClaimEventType type, String... properties) {
        this.type = type;
        if (properties != null) {
            for (int i = 0; i < properties.length; i += 2) {
                this.properties.put(properties[i], properties[i + 1]);
            }
        }
    }

    public DefaultAtomicProposition(Map<String, String> properties) {
        this(null, properties);
    }

    public DefaultAtomicProposition(ClaimEventType type, Map<String, String> properties) {
        this.type = type;
        this.properties.putAll(properties);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, String> getAttributes() {
        return Collections.unmodifiableMap(properties);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAttribute(String key, String value) {
        properties.put(key, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAttributeValue(String key) {
        return properties.get(key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clearAttributes() {
        properties.clear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ClaimEventType getType() {
        return type;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        if (type == null) {
            return "DefaultAtomicProposition [" + properties.toString() + "]";
        } else {
            return "DefaultAtomicProposition [" + properties.toString() + ", type=" + type + "]";
        }
    }
}
