/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl.impl;

import java.util.Map;

public class VarIExp implements IExp {
    private final String value;

    public VarIExp(String value) {
        this.value = value;
    }

    public double evaluate(Map<String, Long> valuation) {
        Long l = valuation.get(value);
        if (l == null) {
            throw new IllegalStateException("No value for free variable \"" + value + "\"");
        }
        return (double)l;
    }

    public long evaluateLong(Map<String, Long> valuation) {
        Long l = valuation.get(value);
        if (l == null) {
            throw new IllegalStateException("No value for free variable \"" + value + "\"");
        }
        return l;
    }

    public boolean isNaturalNumber() {
        return true;
    }

    public boolean hasVariable() {
        return true;
    }

    @Override
    public String toString() {
        return value;
    }
}
