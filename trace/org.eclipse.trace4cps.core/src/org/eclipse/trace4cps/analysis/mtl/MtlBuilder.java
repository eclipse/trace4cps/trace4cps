/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl;

import org.eclipse.trace4cps.analysis.mtl.impl.MTLand;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLimply;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLnot;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLor;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLtrue;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLuntil;
import org.eclipse.trace4cps.core.impl.Interval;

/**
 * A utility class for building {@link MtlFormula} instances.
 */
public final class MtlBuilder {
    private static final MTLtrue MTL_TRUE = new MTLtrue();

    private MtlBuilder() {
    }

    public static MtlFormula TRUE() {
        return MTL_TRUE;
    }

    public static MtlFormula NOT(MtlFormula f) {
        if (f instanceof MTLnot) {
            return ((MTLnot)f).getChild();
        } else {
            return new MTLnot(f);
        }
    }

    public static MtlFormula AND(MtlFormula l, MtlFormula r) {
        return new MTLand(l, r);
    }

    public static MtlFormula OR(MtlFormula l, MtlFormula r) {
        return new MTLor(l, r);
    }

    public static MtlFormula IMPLY(MtlFormula l, MtlFormula r) {
        return new MTLimply(l, r);
    }

    public static MtlFormula U(MtlFormula l, MtlFormula r, Interval i) {
        return new MTLuntil(l, r, i);
    }

    public static MtlFormula F(MtlFormula p) {
        return new MTLuntil(TRUE(), p, Interval.trivial());
    }

    public static MtlFormula F(MtlFormula p, Interval i) {
        return new MTLuntil(TRUE(), p, i);
    }

    public static MtlFormula G(MtlFormula p) {
        return NOT(F(NOT(p)));
    }

    public static MtlFormula G(MtlFormula p, Interval i) {
        return NOT(F(NOT(p), i));
    }
}
