/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl;

/**
 * An enumeration of the possible satisfaction values that result from MTL checking.
 */
public enum InformativePrefix {

    /**
     * The formula is satisfied. If the state sequence is interpreted as a prefix, then any extension of the state
     * sequence will not invalidate the formula.
     */
    GOOD,

    /**
     * This value can only occur when the state sequence is interpreted as a prefix. The state sequence does not give
     * evidence that the formula is not satisfied. An extension of the state sequence may or may not invalidate the
     * formula.
     */
    NON_INFORMATIVE,

    /**
     * The state sequence contains a counter example for the formula. If the state sequence is interpreted as a prefix,
     * then any extension of the state sequence will not satisfy the formula.
     */
    BAD,

    /**
     * The algorithm did not compute a value.
     */
    NOT_COMPUTED;
}
