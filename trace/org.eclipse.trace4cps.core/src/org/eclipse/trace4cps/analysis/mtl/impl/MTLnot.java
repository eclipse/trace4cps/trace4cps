/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl.impl;

import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.core.impl.Interval;

public class MTLnot extends AbstractMTLformula {
    public MTLnot(MtlFormula p) {
        super(p);
    }

    public MtlFormula getChild() {
        return getChildren().get(0);
    }

    @Override
    public String toString() {
        if (getChild() instanceof MTLuntil) {
            MTLuntil u = (MTLuntil)getChild();
            Interval i = u.getInterval();
            String interval = "";
            if (!i.isTrivial()) {
                interval = "_" + i;
            }
            if (u.getLeft() instanceof MTLtrue) {
                if (u.getRight() instanceof MTLnot) {
                    MTLnot n = (MTLnot)u.getRight();
                    return "G" + interval + " " + n.getChild();
                } else {
                    return "G" + interval + " !" + u.getRight();
                }
            }
        }
        return "not " + getChild();
    }
}
