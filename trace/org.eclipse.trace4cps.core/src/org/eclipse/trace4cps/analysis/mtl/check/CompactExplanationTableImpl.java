/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl.check;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.trace4cps.analysis.mtl.InformativePrefix;
import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.mtl.State;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLtrue;

/**
 * We need 4 bits per index,formula. Optimization for locality in Until computation: put all formulas together
 */
public final class CompactExplanationTableImpl implements TabularExplanationTable {
    private final List<? extends State> trace;

    private final Map<MtlFormula, Integer> subForms;

    private int trueIndex = -1;

    // Number of 32-bit ints to encode the truth values of 1 state
    private final int p;

    private final int[] table;

    public CompactExplanationTableImpl(List<? extends State> trace, Map<MtlFormula, Integer> subForms) {
        this.trace = trace;
        this.subForms = subForms;
        for (Map.Entry<MtlFormula, Integer> e: subForms.entrySet()) {
            if (e.getKey() instanceof MTLtrue) {
                trueIndex = e.getValue();
            }
        }
        // We don't encode the true atom:
        this.p = (subForms.size() / 8) + 1;
        this.table = new int[p * trace.size()];
        for (int i = 0; i < p * trace.size(); i++) {
            table[i] = 0;
        }
    }

    @Override
    public List<? extends State> getTrace() {
        return trace;
    }

    public int get(int formulaIndex, int stateIndex) {
        // don't have an explicit table row for MTLtrue
        if (formulaIndex == trueIndex) {
            return YES;
        }
        // unpack value for formula:
//        return (table[stateIndex * P + formulaIndex / 8] >> (4 * (formulaIndex % 8))) & 15;
        return (table[(stateIndex * p) + (formulaIndex >> 3)] >> ((formulaIndex & 7) << 2)) & 15;
    }

    public int put(int formulaIndex, int stateIndex, int c) {
//        table[stateIndex * P + formulaIndex / 8] |= (c << (4 * (formulaIndex % 8)));
        table[stateIndex * p + (formulaIndex >> 3)] |= (c << ((formulaIndex & 7) << 2));
        return c;
    }

    public List<Region> getRegions(MtlFormula phi) {
        final int formulaIndex = subForms.get(phi);
        List<Region> regions = new ArrayList<Region>();
        // Init the start region:
        InformativePrefix current = getValue(formulaIndex, 0);
        RegionImpl r = new RegionImpl(current);
        r.setStart(0, trace.get(0).getTimestamp().doubleValue());
        // iterate over the table until the value changes:
        for (int i = 1; i < trace.size(); i++) {
            InformativePrefix v = getValue(formulaIndex, i);
            if (v != current) {
                // Close current region:
                r.setEnd(i - 1, trace.get(i - 1).getTimestamp().doubleValue());
                regions.add(r);
                // Open new region:
                current = v;
                r = new RegionImpl(current);
                r.setStart(i, trace.get(i).getTimestamp().doubleValue());
            }
        }
        // Add the last open region:
        r.setEnd(trace.size() - 1, trace.get(trace.size() - 1).getTimestamp().doubleValue());
        regions.add(r);
        return regions;
    }

    public InformativePrefix getValue(MtlFormula phi, int index) {
        return getValue(subForms.get(phi), index);
    }

    public InformativePrefix getValue(int formulaIndex, int index) {
        switch (get(formulaIndex, index)) {
            case YES:
                return InformativePrefix.GOOD;
            case NO:
                return InformativePrefix.BAD;
            case MAYBE:
                return InformativePrefix.NON_INFORMATIVE;
            case UNKNOWN:
                return InformativePrefix.NOT_COMPUTED;
            default:
                throw new IllegalStateException();
        }
    }
}
