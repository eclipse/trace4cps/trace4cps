/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl.check;

import static org.eclipse.trace4cps.analysis.mtl.check.TabularExplanationTable.MAYBE;
import static org.eclipse.trace4cps.analysis.mtl.check.TabularExplanationTable.NO;
import static org.eclipse.trace4cps.analysis.mtl.check.TabularExplanationTable.UNKNOWN;
import static org.eclipse.trace4cps.analysis.mtl.check.TabularExplanationTable.YES;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.trace4cps.analysis.mtl.AtomicProposition;
import org.eclipse.trace4cps.analysis.mtl.ExplanationTable;
import org.eclipse.trace4cps.analysis.mtl.InformativePrefix;
import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.analysis.mtl.MtlUtil;
import org.eclipse.trace4cps.analysis.mtl.State;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLand;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLimply;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLnot;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLor;
import org.eclipse.trace4cps.analysis.mtl.impl.MTLuntil;
import org.eclipse.trace4cps.analysis.signal.impl.PsopHelper;
import org.eclipse.trace4cps.analysis.stl.StlFormula;
import org.eclipse.trace4cps.core.impl.Interval;

public final class RecursiveMemoizationChecker implements SingleFormulaChecker {
    private List<? extends State> trace;

    private Set<InformativePrefix> log;

    private boolean interpretAsPrefix;

    private TabularExplanationTable l;

    private final Map<MtlFormula, Integer> idMap = new HashMap<MtlFormula, Integer>();

    private final Map<MtlFormula, Integer> depthMap = new HashMap<MtlFormula, Integer>();

    @Override
    public InformativePrefix check(List<? extends State> trace, MtlFormula phi, Set<InformativePrefix> log,
            boolean interpretAsPrefix)
    {
        this.trace = trace;
        if (log != null) {
            this.log = log;
        } else {
            this.log = Collections.emptySet();
        }
        this.interpretAsPrefix = interpretAsPrefix;
        List<MtlFormula> sub = MtlUtil.getSubformulas(phi);
        idMap.clear();
        int i = 0;
        for (MtlFormula f: sub) {
            idMap.put(f, i);
            i++;
        }
        fillDepthMap(phi);
        this.l = new CompactExplanationTableImpl(trace, idMap);
        // Now compute the truth value of the top-level formula depth first
        // filling needed table entries on-the-fly:
        fillTable(phi, 0);
        InformativePrefix result = l.getValue(phi, 0);
        if (!this.log.contains(result)) {
            // throw away the explanation
            l = null;
        }
        return result;
    }

    private int fillDepthMap(MtlFormula phi) {
        int max = 0;
        for (MtlFormula child: phi.getChildren()) {
            max = Math.max(max, fillDepthMap(child));
        }
        depthMap.put(phi, max);
        return max;
    }

    private int getDepth(MtlFormula phi) {
        Integer depth = depthMap.get(phi);
        if (depth != null) {
            return depth;
        }
        throw new IllegalStateException(phi + " not present in depth map");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExplanationTable getExplanation() {
        return l;
    }

    private int fillTable(MtlFormula f, int i) {
        if (f instanceof AtomicProposition) {
            return fillAP((AtomicProposition)f, i);
        } else if (f instanceof MTLnot) {
            return fillNot((MTLnot)f, i);
        } else if (f instanceof MTLand) {
            return fillAnd((MTLand)f, i);
        } else if (f instanceof MTLimply) {
            return fillImply((MTLimply)f, i);
        } else if (f instanceof MTLor) {
            return fillOr((MTLor)f, i);
        } else if (f instanceof MTLuntil) {
            return fillUntil((MTLuntil)f, i);
        } else if (f instanceof StlFormula) {
            return fillStlSubformula((StlFormula)f, i);
        } else {
            throw new IllegalStateException("use of " + f.getClass().toString() + " constructs unsupported!");
        }
    }

    private int fillAP(AtomicProposition f, int i) {
        int formulaIndex = idMap.get(f);
        if (trace.get(i).satisfies(f)) {
            l.put(formulaIndex, i, YES);
            return YES;
        } else {
            l.put(formulaIndex, i, NO);
            return NO;
        }
    }

    private int fillStlSubformula(StlFormula f, int i) {
        int formulaIndex = idMap.get(f);
        double t = trace.get(i).getTimestamp().doubleValue();
        if (!PsopHelper.dom(f.getSignal()).contains(t)) {
            throw new IllegalStateException("incompatible time domain");
        }
        double rho = PsopHelper.valueAt(f.getSignal(), t).doubleValue();
        if (rho >= 0) {
            l.put(formulaIndex, i, YES);
            return YES;
        } else {
            l.put(formulaIndex, i, NO);
            return NO;
        }
    }

    private int fillNot(MTLnot f, int i) {
        int childIndex = idMap.get(f.getChild());
        int formulaIndex = idMap.get(f);
        int v = l.get(childIndex, i);
        if (v == UNKNOWN) {
            v = fillTable(f.getChild(), i);
        }
        return l.put(formulaIndex, i, not(v));
    }

    private int fillOr(MTLor f, int i) {
        MtlFormula f1 = f.getLeft();
        MtlFormula f2 = f.getRight();
        int v1 = l.get(idMap.get(f1), i);
        int v2 = l.get(idMap.get(f2), i);
        // First check to compute unkowns with least depth first:
        if (v1 == UNKNOWN && v2 == UNKNOWN) {
            if (getDepth(f1) <= getDepth(f2)) {
                v1 = fillTable(f1, i);
            } else {
                v2 = fillTable(f2, i);
            }
        }
        // Check whether we need to fill the other unknown:
        if (v1 == UNKNOWN && v2 != YES) {
            v1 = fillTable(f1, i);
        } else if (v2 == UNKNOWN && v1 != YES) {
            v2 = fillTable(f2, i);
        }
        return l.put(idMap.get(f), i, or(v1, v2));
    }

    private int fillAnd(MTLand f, int i) {
        MtlFormula f1 = f.getLeft();
        MtlFormula f2 = f.getRight();
        int v1 = l.get(idMap.get(f1), i);
        int v2 = l.get(idMap.get(f2), i);
        // First check to compute unkowns with least depth first:
        if (v1 == UNKNOWN && v2 == UNKNOWN) {
            if (getDepth(f1) <= getDepth(f2)) {
                v1 = fillTable(f1, i);
            } else {
                v2 = fillTable(f2, i);
            }
        }
        if (v1 == UNKNOWN && v2 != NO) {
            v1 = fillTable(f1, i);
        } else if (v2 == UNKNOWN && v1 != NO) {
            v2 = fillTable(f2, i);
        }
        return l.put(idMap.get(f), i, and(v1, v2));
    }

    private int fillImply(MTLimply f, int i) {
        MtlFormula f1 = f.getLeft();
        MtlFormula f2 = f.getRight();
        int v1 = l.get(idMap.get(f1), i);
        int v2 = l.get(idMap.get(f2), i);
        // First check to compute unkowns with least depth first:
        if (v1 == UNKNOWN && v2 == UNKNOWN) {
            if (getDepth(f1) <= getDepth(f2)) {
                v1 = fillTable(f1, i);
            } else {
                v2 = fillTable(f2, i);
            }
        }
        if (v1 == UNKNOWN && v2 != NO) {
            v1 = fillTable(f1, i);
        } else if (v2 == UNKNOWN && v1 != NO) {
            v2 = fillTable(f2, i);
        }
        return l.put(idMap.get(f), i, implies(v1, v2));
    }

    private static int not(int v) {
        if (v == YES) {
            return NO;
        } else if (v == NO) {
            return YES;
        }
        return MAYBE;
    }

    private static int or(int v1, int v2) {
        if (v1 == YES || v2 == YES) {
            return YES;
        } else if (v1 == NO && v2 == NO) {
            return NO;
        }
        return MAYBE;
    }

    private static int and(int v1, int v2) {
        if (v1 == YES && v2 == YES) {
            return YES;
        } else if (v1 == NO || v2 == NO) {
            return NO;
        }
        return MAYBE;
    }

    private static int implies(int v1, int v2) {
        return or(not(v1), v2);
    }

    private int fillUntil(MTLuntil f, int i) {
        if (interpretAsPrefix) {
            return fillUntilEntry(f, i);
        } else {
            return fillUntilEntryFinite(f, i);
        }
    }

    private int fillUntilEntry(MTLuntil f, int i) {
        double ti = trace.get(i).getTimestamp().doubleValue();
        double tn = trace.get(trace.size() - 1).getTimestamp().doubleValue();
        Interval interval = f.getInterval();

        int formulaIndex = idMap.get(f);
        int leftIndex = idMap.get(f.getLeft());
        int rightIndex = idMap.get(f.getRight());

        boolean c1 = false;
        boolean c11 = true;
        boolean c2 = true;
        boolean c22 = false;
        boolean c3 = interval.strictlySmallerThan(tn - ti);
        for (int j = i; j < trace.size(); j++) {
            double tj = trace.get(j).getTimestamp().doubleValue();
            int r2 = l.get(rightIndex, j);
            if (r2 == UNKNOWN) {
                r2 = fillTable(f.getRight(), j);
            }
            c1 = c1 || (r2 == YES && interval.contains(tj - ti) && c11);
            if (c1) {
                return l.put(formulaIndex, i, YES);
            }
            int r1 = l.get(leftIndex, j);
            if (r1 == UNKNOWN) {
                r1 = fillTable(f.getLeft(), j);
            }
            c2 = c2 && (r2 == NO || !interval.contains(tj - ti) || c22);
            c3 = c3 || r1 == NO;
            c11 = c11 && (r1 == YES);
            c22 = c22 || (r1 == NO);
            if ((c22 || j == trace.size() - 1 || interval.strictlySmallerThan(tj - ti)) && c2 && c3) {
                return l.put(formulaIndex, i, NO);
            }
            if ((!c11 || interval.strictlySmallerThan(tj - ti)) && !c1 && !c2) {
                return l.put(formulaIndex, i, MAYBE);
            }
        }
        return l.put(formulaIndex, i, MAYBE);
    }

    private int fillUntilEntryFinite(MTLuntil f, int i) {
        double ti = trace.get(i).getTimestamp().doubleValue();
        Interval interval = f.getInterval();

        int formulaIndex = idMap.get(f);
        int leftIndex = idMap.get(f.getLeft());
        int rightIndex = idMap.get(f.getRight());

        boolean c1 = false;
        boolean c11 = true;
        for (int j = i; j < trace.size(); j++) {
            double tj = trace.get(j).getTimestamp().doubleValue();
            int r2 = l.get(rightIndex, j);
            if (r2 == UNKNOWN) {
                r2 = fillTable(f.getRight(), j);
            }
            c1 = c1 || (r2 == YES && interval.contains(tj - ti) && c11);
            if (c1) {
                return l.put(formulaIndex, i, YES);
            }
            int r1 = l.get(leftIndex, j);
            if (r1 == UNKNOWN) {
                r1 = fillTable(f.getLeft(), j);
            }
            c11 = c11 && (r1 == YES);
            if ((!c11 || j == trace.size() - 1 || interval.strictlySmallerThan(tj - ti)) && !c1) {
                return l.put(formulaIndex, i, NO);
            }
        }
        throw new IllegalStateException();
    }
}
