/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.eclipse.trace4cps.analysis.mtl.check.RecursiveMemoizationChecker;
import org.eclipse.trace4cps.analysis.mtl.check.SingleFormulaChecker;
import org.eclipse.trace4cps.analysis.stl.StlFormula;
import org.eclipse.trace4cps.core.ITrace;

/**
 * This class is the main entry point for runtime verification of STL-mx specifications on traces. Use the
 * {@link MtlBuilder} class for construction of {@link MtlFormula} instances. The list of states typically is provided
 * by {@link ITrace#getEvents()}}.
 *
 * <p>
 * References:
 * <ul>
 * <li>M. Hendriks, M. Geilen, A. R. B. Behrouzian, T. Basten, H. Alizadeh, and D. Goswami. Checking metric temporal
 * logic with trace. In 2016 16th International Conference on Application of Concurrency to System Design (ACSD), pages
 * 19-24, June 2016.</li>
 * <li>M. Hendriks, M. Geilen, A. R. B. Behrouzian, T. Basten, H. Alizadeh, and D. Goswami. Checking metric temporal
 * logic with trace. ESR-2016-01, 11 April 2016. (<a href=
 * "http://www.es.ele.tue.nl/esreports/esr-2016-01.pdf">http://www.es.ele.tue.nl/esreports/esr-2016-01.pdf</a>)</li>
 * <li>T. Ferrere, O. Maler, D. Nickovic. Mixed-Time Signal Temporal Logic. In 2019 International Conference on Formal
 * Modeling and Analysis of Timed Systems (FORMATS), 2019.</li>
 * </ul>
 * </p>
 */
public final class MtlChecker {
    private final int numThreads;

    /**
     * A checker that uses all available processors.
     */
    public MtlChecker() {
        this(Runtime.getRuntime().availableProcessors());
    }

    /**
     * A checker that uses at most a given number of threads.
     *
     * @param numThreads maximum number of threads to use
     */
    public MtlChecker(int numThreads) {
        this.numThreads = numThreads;
    }

    /**
     * Synchronously checks an STL-mx formula (i.e., an {@link MtlFormula} which may have {@link StlFormula}
     * subformulas). The trace is interpreted as a prefix if and only if the given formula does not contain
     * {@link StlFormula} parts.
     *
     * @param trace the execution trace
     * @param phi the formula to check
     * @return the result
     */
    public MtlResult check(List<? extends State> trace, MtlFormula phi) {
        boolean interpretAsPrefix = MtlUtil.isMtl(phi);
        return check(trace, phi, new HashSet<InformativePrefix>(), interpretAsPrefix);
    }

    /**
     * Synchronously checks an STL-mx formula (i.e., an {@link MtlFormula} which may have {@link StlFormula}
     * subformulas). The result will contain an explanation of the formulas whose satisfaction value is contained in the
     * given {@code explain} set. The trace can be interpreted as a prefix if and only if the given formula does not
     * contain {@link StlFormula} parts.
     *
     * @param trace the execution trace
     * @param phi the formula to check
     * @param explain the set of truth values for which to generate an explanation (typically only BAD)
     * @param interpretAsPrefix whether to interpret the trace as a prefix (only for pure MTL formulas!)
     * @return the result
     */
    public MtlResult check(List<? extends State> trace, MtlFormula phi, Set<InformativePrefix> explain,
            boolean interpretAsPrefix)
    {
        if (interpretAsPrefix && !MtlUtil.isMtl(phi)) {
            throw new IllegalArgumentException("trace can only be interpreted as a prefix for pure MTL formulas");
        }
        SingleFormulaChecker c = new RecursiveMemoizationChecker();
        InformativePrefix r = c.check(trace, phi, explain, interpretAsPrefix);
        return new MtlResult(phi, r, c.getExplanation());
    }

    /**
     * A-synchronously checks a list of STL-mx formula (i.e., an {@link MtlFormula} which may have {@link StlFormula}
     * subformulas). The trace is interpreted as a prefix if the given formula does not contain {@link StlFormula}
     * parts.
     *
     * @param trace the execution trace
     * @param phis the formulas to check
     * @return the result
     */
    public MtlFuture checkAll(List<? extends State> trace, List<MtlFormula> phis) {
        return checkAll(trace, phis, new HashSet<InformativePrefix>(), true);
    }

    /**
     * A-synchronously checks a list of STL-mx formula (i.e., an {@link MtlFormula} which may have {@link StlFormula}
     * subformulas). The result will contain an explanation of the formulas whose satisfaction value is contained in the
     * given {@code explain} set.The trace is interpreted as a prefix if the given formula does not contain
     * {@link StlFormula} parts and {@code interpretAsPrefix = true}.
     *
     * @param trace the execution trace
     * @param phis the formulas to check
     * @param explain the set of truth values for which to generate an explanation (typically only BAD)
     * @param tryInterpretAsPrefix whether to interpret the trace as a prefix (if true, then this is only applied if the
     *     formula is a pure MTL formula)
     * @return the result
     */
    public MtlFuture checkAll(List<? extends State> trace, Collection<MtlFormula> phis, Set<InformativePrefix> explain,
            boolean tryInterpretAsPrefix)
    {
        List<State> safeList = new CopyOnWriteArrayList<State>(trace);
        ExecutorService exec = Executors.newFixedThreadPool(numThreads);
        MtlFuture future = new MtlFuture(phis.size(), exec);
        for (MtlFormula phi: phis) {
            boolean pref = tryInterpretAsPrefix && MtlUtil.isMtl(phi);
            exec.submit(new CheckTask(safeList, phi, future, explain, pref));
        }
        return future;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "MtlChecker[#threads=" + numThreads + "]";
    }

    private static final class CheckTask implements Runnable {
        private final List<State> trace;

        private final MtlFormula phi;

        private final MtlFuture result;

        private final Set<InformativePrefix> explain;

        private final boolean interpretAsPrefix;

        public CheckTask(List<State> trace, MtlFormula phi, MtlFuture result, Set<InformativePrefix> explain,
                boolean interpretAsPrefix)
        {
            this.trace = trace;
            this.phi = phi;
            this.result = result;
            this.explain = explain;
            this.interpretAsPrefix = interpretAsPrefix;
        }

        @Override
        public void run() {
            try {
                SingleFormulaChecker c = new RecursiveMemoizationChecker();
                InformativePrefix r = c.check(trace, phi, explain, interpretAsPrefix);
                result.addToResult(phi, r, c.getExplanation());
            } catch (Throwable th) {
//               th.printStackTrace();
                result.addError(th);
            }
        }
    }
}
