/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl;

import java.util.List;

/**
 * An explanation table gives for all subformulas of some {@link MtlFormula} the indices in the trace on which it has
 * been checked and the truth value ({@link InformativePrefix}) on that index.
 */
public interface ExplanationTable {
    /**
     * @return the list of states to which this table belongs
     */
    List<? extends State> getTrace();

    /**
     * @param phi the formula for which to get the satisfaction regions
     * @return the regions for the given formula
     */
    List<? extends Region> getRegions(MtlFormula phi);

    /**
     * A region gives for parts of the trace (list of states) the computed satisfaction value.
     */
    public interface Region {
        /**
         * @return the start index of this region (inclusive)
         */
        int getStartIndex();

        /**
         * @return the end index of this region (inclusive)
         */
        int getEndIndex();

        /**
         * @return the start time of this region
         */
        double getStartTime();

        /**
         * @return the end time of this region
         */
        double getEndTime();

        /**
         * @return the satisfaction value for all states in this regions
         */
        InformativePrefix getValue();
    }
}
