/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl.impl;

import java.util.Map;

public class IntIExp implements IExp {
    private final long value;

    public IntIExp(long value) {
        this.value = value;
    }

    public double evaluate(Map<String, Long> valuation) {
        return value;
    }

    public long evaluateLong(Map<String, Long> valuation) {
        return value;
    }

    public boolean isNaturalNumber() {
        return true;
    }

    public boolean hasVariable() {
        return false;
    }

    @Override
    public String toString() {
        return Double.toString(value);
    }
}
