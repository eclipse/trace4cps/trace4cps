/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl.impl;

import org.eclipse.trace4cps.analysis.mtl.MtlFormula;
import org.eclipse.trace4cps.core.impl.Interval;

public class MTLuntil extends AbstractBinopMTLformula {
    private final Interval interval;

    public MTLuntil(MtlFormula a, MtlFormula b, Interval i) {
        super(a, b);
        interval = i;
    }

    public Interval getInterval() {
        return interval;
    }

    @Override
    public String toString() {
        String i = "";
        if (!interval.isTrivial()) {
            i = "_" + interval.toString();
        }
        if (getLeft() instanceof MTLtrue) {
            return "F" + i + " " + getRight();
        } else {
            return "(" + getLeft() + " U" + i + " " + getRight() + ")";
        }
    }
}
