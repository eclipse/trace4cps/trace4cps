/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.trace4cps.analysis.mtl.MtlFormula;

public abstract class AbstractMTLformula implements MtlFormula {
    protected final List<MtlFormula> children = new ArrayList<MtlFormula>();

    public AbstractMTLformula(MtlFormula... c) {
        if (c != null) {
            for (MtlFormula f: c) {
                children.add(f);
            }
        }
    }

    @Override
    public final List<MtlFormula> getChildren() {
        return children;
    }
}
