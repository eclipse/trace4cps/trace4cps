/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl.impl;

import java.util.Map;

public class OpIExp implements IExp {
    private final Iop op;

    private final IExp left;

    private final IExp right;

    public OpIExp(IExp left, Iop op, IExp right) {
        this.left = left;
        this.op = op;
        this.right = right;
    }

    public double evaluate(Map<String, Long> valuation) {
        double v1 = left.evaluate(valuation);
        double v2 = right.evaluate(valuation);
        switch (op) {
            case PLUS:
                return v1 + v2;
            case MINUS:
                return v1 - v2;
            case MULT:
                return v1 * v2;
            case DIV:
                return v1 / v2;
            case MOD:
                return v1 % v2;
            default:
                throw new IllegalStateException("Unknown operator: " + op);
        }
    }

    public long evaluateLong(Map<String, Long> valuation) {
        long v1 = left.evaluateLong(valuation);
        long v2 = right.evaluateLong(valuation);
        switch (op) {
            case PLUS:
                return v1 + v2;
            case MINUS:
                return v1 - v2;
            case MULT:
                return v1 * v2;
            case DIV:
                return v1 / v2;
            case MOD:
                return v1 % v2;
            default:
                throw new IllegalStateException("Unknown operator: " + op);
        }
    }

    public boolean hasVariable() {
        return left.hasVariable() || right.hasVariable();
    }

    public boolean isNaturalNumber() {
        return left.isNaturalNumber() && right.isNaturalNumber();
    }

    @Override
    public String toString() {
        return "(" + left.toString() + op.toString() + right.toString() + ")";
    }
}
