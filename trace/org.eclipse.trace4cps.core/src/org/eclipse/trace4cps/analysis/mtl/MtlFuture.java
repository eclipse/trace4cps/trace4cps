/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.mtl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * A future for an asynchronous temporal-logic verification.
 */
public final class MtlFuture implements Future<List<MtlResult>> {
    private final Object lock = new Object();

    private final int total;

    private int numDone;

    private int numSatisfied = 0;

    private int numNeutral = 0;

    private int numNotSatisfied = 0;

    private boolean canceled = false;

    private long startTime;

    private long endTime;

    private final List<MtlResult> result = new ArrayList<MtlResult>();

    private int numErrors = 0;

    private final ExecutorService exec;

    MtlFuture(int size, ExecutorService exec) {
        this.total = size;
        this.exec = exec;
        startTime = System.nanoTime();
    }

    /**
     * @return the total number of properties in this future
     */
    public int getTotal() {
        synchronized (lock) {
            return total;
        }
    }

    /**
     * @return the number of properties that have been verified now
     */
    public int getNumDone() {
        synchronized (lock) {
            return numDone;
        }
    }

    /**
     * @return the number of properties with a {@link InformativePrefix.GOOD} result
     */
    public int getNumGood() {
        synchronized (lock) {
            return numSatisfied;
        }
    }

    /**
     * @return the number of properties with a {@link InformativePrefix.NON_INFORMATIVE} result
     */
    public int getNumNeutral() {
        synchronized (lock) {
            return numNeutral;
        }
    }

    /**
     * @return the number of properties with a {@link InformativePrefix.BAD} result
     */
    public int getNumBad() {
        synchronized (lock) {
            return numNotSatisfied;
        }
    }

    /**
     * @return the computation time spent up to now (stays constant when done)
     */
    public long getComputationTimeInMs() {
        synchronized (lock) {
            return (endTime - startTime) / 1000000;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        synchronized (lock) {
            if (!canceled) {
                canceled = true;
                exec.shutdownNow();
                return true;
            } else {
                return canceled;
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCancelled() {
        synchronized (lock) {
            return canceled;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDone() {
        synchronized (lock) {
            return total == numDone + numErrors;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MtlResult> get() throws InterruptedException, ExecutionException {
        // blocking method: wait until done
        synchronized (lock) {
            while (!isDone()) {
                lock.wait();
            }
            return result;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MtlResult> get(long timeout, TimeUnit unit)
            throws InterruptedException, ExecutionException, TimeoutException
    {
        throw new UnsupportedOperationException();
    }

    void addToResult(MtlFormula phi, InformativePrefix sat, ExplanationTable expl) throws MtlException {
        synchronized (lock) {
            if (isDone()) {
                throw new MtlException("I am already done!");
            }
            result.add(new MtlResult(phi, sat, expl));
            numDone++;
            // System.err.println("-- numDone = " + numDone);
            if (sat == InformativePrefix.GOOD) {
                numSatisfied++;
            } else if (sat == InformativePrefix.BAD) {
                numNotSatisfied++;
            } else if (sat == InformativePrefix.NON_INFORMATIVE) {
                numNeutral++;
            }
            if (isDone()) {
                endTime = System.nanoTime();
                // shutdown the executor service that has computed this future
                exec.shutdownNow();
            }
            lock.notifyAll();
        }
    }

    void addError(Throwable th) {
        synchronized (lock) {
            if (!isDone()) {
                numErrors++;
            }
            if (isDone()) {
                endTime = System.nanoTime();
                // shutdown the executor service that has computed this future
                exec.shutdownNow();
            }
            lock.notifyAll();
        }
    }
}
