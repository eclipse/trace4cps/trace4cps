/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.constraintgraph.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.trace4cps.analysis.constraintgraph.ConstraintConfig;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IClaimEvent;
import org.eclipse.trace4cps.core.IDependency;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.ITrace;

public class ConstraintGraph {

    // Ordered by timestamp, and end events before start events on same moment
    private final List<ConstraintGraphNode> nodes = new ArrayList<>();

    public ConstraintGraph(ITrace trace) {
        this(trace, ConstraintConfig.getDefault());
    }

    public ConstraintGraph(ITrace trace, ConstraintConfig config) {
        this(trace.getEvents(), trace.getDependencies(), config);
    }

    public ConstraintGraph(List<IEvent> events, ConstraintConfig config) {
        this(events, null, config);
    }

    /**
     * Creates the core constraint graph from a set of claims. Only intra-claim constraints are added, i.e., the claim
     * durations. Use {@link #applyOrderHeuristic(double)} and {@link #applyNonElasticityConstraint(Collection)} to add
     * more constraints.
     *
     * @param events the events for which to build a core constraint graph
     */
    public ConstraintGraph(List<IEvent> events, List<IDependency> deps, ConstraintConfig config) {
        if (events == null || config == null) {
            throw new IllegalArgumentException();
        }
        createNodeList(events, config.isAddSourceAndSink());
        if (config.isAddClaimLowerBoundConstraints() || config.isAddClaimUpperBoundConstraints()) {
            applyClaimDurations(config);
        }
        if (config.isApplyOrderingHeuristic()) {
            applyOrderHeuristic(config.getEpsilon());
        }
        if (config.isApplyNonElasticityHeuristic()) {
            applyNonElasticityConstraint(config.getFilter(), config.getPartitioningAttribute());
        }
        if (config.isUseDependencies()) {
            if (deps == null) {
                throw new IllegalArgumentException("dependencies not available");
            }
            applyDependencies(deps);
        }
        if (config.isAddSourceAndSink()) {
            addSourceAndSink();
        }
    }

    public final List<ConstraintGraphNode> getNodes() {
        return nodes;
    }

    public final int size() {
        return nodes.size();
    }

    public final int edgeSize() {
        int cnt = 0;
        for (ConstraintGraphNode n: nodes) {
            cnt += n.constraints().size();
        }
        return cnt;
    }

    private void createNodeList(List<IEvent> events, boolean addSourceAndSink) {
        int id = addSourceAndSink ? 1 : 0;
        for (IEvent event: events) {
            if (event instanceof IClaimEvent) {
                nodes.add(new ClaimNode(id, (IClaimEvent)event));
            } else {
                nodes.add(new EventNode(id, event));
            }
            id++;
        }
        // This is tricky: due to the Comparable interface implemented by
        // the ConstraintGraphNode we have that the nodes are ordered
        // by timestamp, and in case of ClaimNodes with the same time stamp:
        // end events before start events.
        Collections.sort(nodes);
    }

    private void applyClaimDurations(ConstraintConfig cfg) {
        // adds constraints w.r.t. the duration of claims
        for (int i = 0; i < nodes.size(); i++) {
            ConstraintGraphNode node = nodes.get(i);
            if (node instanceof ClaimNode && ((ClaimNode)node).isClaimStart()) {
                ClaimNode startNode = (ClaimNode)node;
                // find the end node:
                for (int j = findStartIndex(startNode, i); j < nodes.size(); j++) {
                    ConstraintGraphNode nj = nodes.get(j);
                    if (nj instanceof ClaimNode && ((ClaimNode)nj).isClaimStop()
                            && ((ClaimNode)nj).getClaim() == startNode.getClaim())
                    {
                        double duration = nj.timestamp() - startNode.timestamp();
                        if (cfg.isAddClaimLowerBoundConstraints()) {
                            startNode.addConstraint(nj, duration);
                        }
                        if (cfg.isAddClaimUpperBoundConstraints()) {
                            nj.addConstraint(startNode, -duration);
                        }
                        break;
                    }
                }
            }
        }
    }

    private int findStartIndex(ClaimNode n, int i) {
        IClaim c = n.getClaim();
        boolean zeroDuration = c.getStartTime().doubleValue() == c.getEndTime().doubleValue();
        if (zeroDuration) {
            // FIXME: this can be optimized by searching backwards in the
            // nodes list to the first node with timestamp equal to that of n
            return 0;
        } else {
            return i + 1;
        }
    }

    private void applyNonElasticityConstraint(Map<String, String> nonElasticGroupFilter,
            String nonElasticGroupPartitioning)
    {
        Collection<List<ConstraintGraphNode>> nonElasticCells = computeNonElasticGroups(nonElasticGroupFilter,
                nonElasticGroupPartitioning);
        for (List<ConstraintGraphNode> cell: nonElasticCells) {
            Collections.sort(cell); // to be sure: sort by timestamp
            // The inter-event distances from claims in this cell are fixed and
            // given by the current timing
            for (int i = 0; i < cell.size() - 1; i++) {
                ConstraintGraphNode ei = cell.get(i);
                ConstraintGraphNode ej = cell.get(i + 1);
                // add edges: i -> j and j -> i with duration of inter-event distance
                double duration = ej.timestamp() - ei.timestamp(); // >= 0 because ej is later
                ei.addConstraint(ej, duration);
                ej.addConstraint(ei, -duration);
            }
        }
    }

    private Collection<List<ConstraintGraphNode>> computeNonElasticGroups(Map<String, String> nonElasticGroupFilter,
            String nonElasticGroupPartitioning)
    {
        Map<String, List<ConstraintGraphNode>> neg = new HashMap<String, List<ConstraintGraphNode>>();
        for (ConstraintGraphNode ce: nodes) {
            EventNode eventNode = (EventNode)ce; // SrcSnk nodes not yet added!
            if (matches(eventNode, nonElasticGroupFilter)) {
                String pid = eventNode.getEvent().getAttributeValue(nonElasticGroupPartitioning);
                if (pid != null) { // skip claim events without partitioning attribute
                    List<ConstraintGraphNode> cell = neg.get(pid);
                    if (cell == null) {
                        cell = new ArrayList<ConstraintGraphNode>();
                        neg.put(pid, cell);
                    }
                    cell.add(ce);
                }
            }
        }
        return neg.values();
    }

    private static boolean matches(EventNode ce, Map<String, String> filter) {
        if (filter != null && filter.size() > 0) {
            for (Map.Entry<String, String> entry: filter.entrySet()) {
                String val = ce.getEvent().getAttributeValue(entry.getKey());
                if (!entry.getValue().equals(val)) {
                    return false;
                }
            }
        }
        return true;
    }

    private void applyDependencies(List<IDependency> deps) {
        Map<IEvent, Set<IEvent>> succs = new HashMap<>();
        for (IDependency dep: deps) {
            IEvent src = dep.getSrc();
            IEvent dst = dep.getDst();
            Set<IEvent> s = succs.get(src);
            if (s == null) {
                s = new HashSet<>();
                succs.put(src, s);
            }
            s.add(dst);
        }
        Map<IEvent, EventNode> eventMap = new HashMap<>();
        for (ConstraintGraphNode node: nodes) {
            if (node instanceof EventNode) {
                EventNode en = (EventNode)node;
                IEvent event = en.getEvent();
                eventMap.put(event, en);
            }
        }
        for (ConstraintGraphNode node: nodes) {
            if (node instanceof EventNode) {
                EventNode en = (EventNode)node;
                IEvent src = en.getEvent();
                Set<IEvent> ss = succs.get(src);
                if (ss != null) {
                    for (IEvent s: ss) {
                        EventNode dst = eventMap.get(s);
                        en.addConstraint(dst, 0);
                    }
                }
            }
        }
    }

    /**
     * Adds constraints between end event {@code e1} and start event {@code e2} with weight {@code 0} if and only if
     * {@code e1.timestamp <= e2.timestamp() <= e1.timestamp + epsilon}. Furthermore, the transitive reduction of the
     * implied constraints is taken.
     */
    private void applyOrderHeuristic(double epsilon) {
        if (epsilon < 0) {
            throw new IllegalArgumentException("epsilon must be at least 0");
        }
        // only end events of the front of finished tasks
        List<ClaimNode> f = new ArrayList<ClaimNode>();
        for (ConstraintGraphNode v: nodes) {
            if (v instanceof ClaimNode) {
                ClaimNode claimNode = (ClaimNode)v;
                if (claimNode.isClaimStart()) {
                    for (ClaimNode w: f) {
                        if (epsilon == Double.POSITIVE_INFINITY || (v.timestamp() - w.timestamp() <= epsilon)) {
                            w.addConstraint(v, 0);
                        }
                    }
                } else if (claimNode.isClaimStop()) {
                    Set<ConstraintGraphNode> toRemove = new HashSet<ConstraintGraphNode>();
                    for (ClaimNode w: f) {
                        // w is an end event, and so is v. Check whether w goes to the start event of v
                        for (Constraint succw: w.constraints()) {
                            ConstraintGraphNode s = succw.getDst();
                            if (s instanceof ClaimNode) {
                                if (((ClaimNode)s).isClaimStart()
                                        && ((ClaimNode)s).getClaim() == claimNode.getClaim())
                                {
                                    toRemove.add(w);
                                }
                            }
                        }
                        // removed end events from F that can never lead to a constraint anymore
                        // because the nodes are sorted by timestamp
                        if (epsilon != Double.POSITIVE_INFINITY && (v.timestamp() - w.timestamp() > epsilon)) {
                            toRemove.add(w);
                        }
                    }
                    for (ConstraintGraphNode k: toRemove) {
                        f.remove(k);
                    }
                    f.add(claimNode);
                }
            }
        }
    }

    private void addSourceAndSink() {
        ConstraintGraphNode src = new SrcSnkNode(0, true);
        ConstraintGraphNode snk = new SrcSnkNode(nodes.size() + 1, false);
        for (ConstraintGraphNode e: nodes) {
            e.addConstraint(snk, 0);
        }
        for (ConstraintGraphNode e: nodes) {
            src.addConstraint(e, 0);
        }
        // Add src and snk
        nodes.add(0, src);
        nodes.add(snk);
    }

    @Override
    public final String toString() {
        StringBuilder b = new StringBuilder("ConstraintGraph[size=");
        b.append(nodes.size()).append("]:\n");
        for (ConstraintGraphNode e: nodes) {
            for (Constraint edge: e.constraints()) {
                if (!edge.isClaimDurationConstraint()) {
                    b.append("\t\t").append(edge).append("\n");
                }
            }
        }
        return b.toString();
    }
}
