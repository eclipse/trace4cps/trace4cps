/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.constraintgraph.impl;

public class SrcSnkNode extends ConstraintGraphNode {
    private final boolean isSrc;

    public SrcSnkNode(int id, boolean isSrc) {
        super(id);
        this.isSrc = isSrc;
    }

    public boolean isSrc() {
        return isSrc;
    }

    public boolean isSnk() {
        return !isSrc;
    }

    @Override
    public double timestamp() {
        throw new IllegalStateException();
    }

    @Override
    public String toString() {
        return "SrcSnkNode[id=" + getId() + ", isSrc=" + isSrc + "]";
    }
}
