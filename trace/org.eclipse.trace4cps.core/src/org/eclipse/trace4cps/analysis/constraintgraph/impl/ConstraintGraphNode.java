/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.constraintgraph.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.trace4cps.core.IEvent;

/**
 * Implementation that wraps {@link IEvent} instances.
 */
public abstract class ConstraintGraphNode implements Comparable<ConstraintGraphNode> {
    private final List<Constraint> succ = new ArrayList<Constraint>();

    // id starts at 0
    private final int id;

    ConstraintGraphNode(int id) {
        this.id = id;
    }

    /**
     * ClaimEvents belong to a {@link ConstraintGraph} {@code cg}. The ids range from 0 to {@code cg.size()-1}, and
     * every ClaimEvent has an unique id within the ConstraintGraph. The ids can thus be used to index an array.
     * </p>
     * If the constraint graph has source and sink nodes, then the source has id 0, and the sink has id
     * {@code cg.size() - 1}.
     */
    public int getId() {
        return id;
    }

    /**
     * @return the timestamp of the underlying IEvent
     */
    public abstract double timestamp();

    /**
     * @return the outgoing {@link Constraint} edges from this event
     */
    public List<Constraint> constraints() {
        return succ;
    }

    void addConstraint(ConstraintGraphNode s, double w) {
        // just add the constraint to the graph; constraints with same src and dst
        // can happen because they might be caused by different things. Just don't
        // add real duplicates
        for (Constraint c: succ) {
            if (c.getDst() == s && c.weight() == w) {
                return;
            }
        }
        succ.add(new Constraint(this, s, w));
    }

    @Override
    public int compareTo(ConstraintGraphNode o) {
        double t0 = timestamp();
        double t1 = o.timestamp();
        return Double.compare(t0, t1);
    }
}
