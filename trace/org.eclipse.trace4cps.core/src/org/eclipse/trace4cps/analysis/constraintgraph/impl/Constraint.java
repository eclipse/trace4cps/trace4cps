/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.constraintgraph.impl;

/**
 * A constraint {@code e1 -- w --> e2} gives a lower bound of {@code w} time units between the occurrence of {@code e1}
 * and {@code e2}.
 * </p>
 * A constraint {@code e2 -- -w --> e1} gives an upper bound of {@code w} time units between the occurrence of
 * {@code e1} and {@code e2}.
 */
public class Constraint {
    private final ConstraintGraphNode src;

    private final ConstraintGraphNode dst;

    private double weight;

    Constraint(ConstraintGraphNode src, ConstraintGraphNode dst, double weight) {
        this.src = src;
        this.dst = dst;
        this.weight = weight;
    }

    public ConstraintGraphNode getSrc() {
        return src;
    }

    public ConstraintGraphNode getDst() {
        return dst;
    }

    public final boolean isClaimDurationConstraint() {
        if (src instanceof ClaimNode && dst instanceof ClaimNode) {
            ClaimNode e1 = (ClaimNode)src;
            ClaimNode e2 = (ClaimNode)dst;
            return ((e1.isClaimStart() && e2.isClaimStop() || (e2.isClaimStart() && e1.isClaimStop())))
                    && e1.getClaim() == e2.getClaim();
        }
        return false;
    }

    /**
     * @return the weight of this constraint (either positive or negative)
     */
    public double weight() {
        return weight;
    }

    @Override
    public final String toString() {
        return src + " ---[" + weight + "]--> " + dst;
    }
}
