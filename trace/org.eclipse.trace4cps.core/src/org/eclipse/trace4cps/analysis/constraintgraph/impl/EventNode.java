/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.constraintgraph.impl;

import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.impl.TraceHelper;

public class EventNode extends ConstraintGraphNode {
    private final IEvent event;

    public EventNode(int id, IEvent event) {
        super(id);
        this.event = event;
    }

    public IEvent getEvent() {
        return event;
    }

    @Override
    public double timestamp() {
        return event.getTimestamp().doubleValue();
    }

    @Override
    public String toString() {
        return "(" + TraceHelper.getValues(event, false) + ")";
    }
}
