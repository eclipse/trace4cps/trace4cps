/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.constraintgraph;

import java.util.Map;

import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IClaimEvent;
import org.eclipse.trace4cps.core.IEvent;

/**
 * A configuration type that is used for building a {@code ConstraintGraph} from a set of {@link IEvent} instances. The
 * {@code ConstraintGraph} is used by the critical-path analysis, and by the difference analysis methods, but the
 * {@link ConstraintConfig} only appears on the API of the critical-path analysis. There are three heuristics that add
 * timing constraints between {@link IEvent} instances:
 * <ul>
 * <li>Claim durations: adds constraints between the events of a single claim that model the duration of that
 * claim.</li>
 * <li>Ordering heuristic: add a timing constraint {@code >= 0} from an end event to a start event if and only if the
 * time gap between those is not negative (the start comes not earlier than the end), but not larger than a given
 * {@code epsilon}.</li>
 * <li>Non-elasticity heuristic: adds constraints between a set of events that fixes those exactly in time w.r.t. to
 * each other. An {@code id} attribute is specified to partition the events, and a filter then is used to filter the
 * events within a cell. The resulting set of events is fixed with timing constraints.</li>
 * </ul>
 */
public class ConstraintConfig {
    private boolean addClaimLowerBoundConstraints = true;

    private boolean addClaimUpperBoundConstraints = true;

    private boolean addSourceAndSink = false;

    private boolean applyOrderingHeuristic = false;

    private boolean useDependencies = false;

    private double epsilon = 0;

    private boolean applyNonElasticityHeuristic = false;

    private Map<String, String> filter = null;

    private String partitioning = null;

    private ConstraintConfig() {
    }

    /**
     * Default configuration:
     * <ul>
     * <li>Adds claim duration constraints</li>
     * <li>No additional source and sink nodes</li>
     * <li>No ordering heuristic</li>
     * <li>No elasticity heuristic</li>
     * </ul>
     *
     * @return the default configuration
     */
    public static ConstraintConfig getDefault() {
        return new ConstraintConfig();
    }

    /**
     * @return whether source and sink nodes are added
     */
    public boolean isAddSourceAndSink() {
        return addSourceAndSink;
    }

    /**
     * @param addSourceAndSink sets whether source and sink nodes are added
     */
    public void setAddSourceAndSink(boolean addSourceAndSink) {
        this.addSourceAndSink = addSourceAndSink;
    }

    /**
     * @return whether lower-bound constraints for claim durations are added between the two {@link IClaimEvent}
     *     instances that belong to the same {@link IClaim}
     */
    public boolean isAddClaimLowerBoundConstraints() {
        return addClaimLowerBoundConstraints;
    }

    /**
     * @return whether upper-bound constraints for claim durations are added between the two {@link IClaimEvent}
     *     instances that belong to the same {@link IClaim}
     */
    public boolean isAddClaimUpperBoundConstraints() {
        return addClaimUpperBoundConstraints;
    }

    /**
     * @param addClaimLowerBoundConstraints whether a constraint for lower bound on claim duration is added
     */
    public void setAddClaimLowerBoundConstraints(boolean addClaimLowerBoundConstraints) {
        this.addClaimLowerBoundConstraints = addClaimLowerBoundConstraints;
    }

    /**
     * @param addClaimLowerBoundConstraints whether a constraint for upper bound on claim duration is added
     */
    public void setAddClaimUpperBoundConstraints(boolean addClaimUpperBoundConstraints) {
        this.addClaimUpperBoundConstraints = addClaimUpperBoundConstraints;
    }

    public void setAddClaimDurations(boolean b) {
        this.addClaimLowerBoundConstraints = b;
        this.addClaimUpperBoundConstraints = b;
    }

    /**
     * @return whether the epsilon-ordering heuristic is applied to add timing constraints between events
     */
    public boolean isApplyOrderingHeuristic() {
        return applyOrderingHeuristic;
    }

    /**
     * @param epsilon enables the ordering heuristic with the given epsilon value
     */
    public void setApplyOrderingHeuristic(double epsilon) {
        this.applyOrderingHeuristic = true;
        this.epsilon = epsilon;
    }

    /**
     * @return the epsilon value for the ordering constrain (0 by default)
     */
    public double getEpsilon() {
        return epsilon;
    }

    /**
     * @return whether the non-elasticity heuristic is applied to add timing constraints between events
     */
    public boolean isApplyNonElasticityHeuristic() {
        return applyNonElasticityHeuristic;
    }

    /**
     * Enables the non-elasticity heuristic for adding constraints between events with the given parameters.
     *
     * @param filter the filter that is used to select a subset of events in a cell
     * @param partitioningAtt the attribute used to partition the events
     */
    public void setApplyNonElasticityHeuristic(Map<String, String> filter, String partitioningAtt) {
        this.applyNonElasticityHeuristic = true;
        this.filter = filter;
        this.partitioning = partitioningAtt;
    }

    /**
     * @return the filter for the non-elasticity heuristic
     */
    public Map<String, String> getFilter() {
        return filter;
    }

    /**
     * @return the attribute for the partitioning in the non-elasticity heuristic
     */
    public String getPartitioningAttribute() {
        return partitioning;
    }

    public boolean isUseDependencies() {
        return useDependencies;
    }

    public void setUseDependencies(boolean useDependencies) {
        this.useDependencies = useDependencies;
    }
}
