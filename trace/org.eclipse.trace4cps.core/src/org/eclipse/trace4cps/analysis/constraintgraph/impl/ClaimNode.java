/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.constraintgraph.impl;

import org.eclipse.trace4cps.core.ClaimEventType;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IClaimEvent;

public class ClaimNode extends EventNode {
    public ClaimNode(int id, IClaimEvent event) {
        super(id, event);
    }

    public IClaim getClaim() {
        return ((IClaimEvent)getEvent()).getClaim();
    }

    public boolean isClaimStart() {
        return ((IClaimEvent)getEvent()).getType() == ClaimEventType.START;
    }

    public boolean isClaimStop() {
        return ((IClaimEvent)getEvent()).getType() == ClaimEventType.END;
    }

    @Override
    public int compareTo(ConstraintGraphNode o) {
        int r = Double.compare(timestamp(), o.timestamp());
        if (r == 0 && o instanceof ClaimNode) {
            r = Boolean.compare(isClaimStart(), ((ClaimNode)o).isClaimStart());
        }
        return r;
    }
}
