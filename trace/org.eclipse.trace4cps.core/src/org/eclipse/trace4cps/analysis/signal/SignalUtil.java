/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.signal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.eclipse.trace4cps.analysis.signal.impl.EventSignalUtil;
import org.eclipse.trace4cps.analysis.signal.impl.LinearSignalFragment;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.IInterval;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.impl.Interval;
import org.eclipse.trace4cps.core.impl.TraceHelper;

/**
 * Utility methods for computing the Little's law signals of an {@link ITrace}. The signals here are all computed based
 * on the {@link IClaim} instances of an {@link ITrace}.
 */
public class SignalUtil {
    private SignalUtil() {
    }

    /**
     * Converts a time value from one unit to another.
     *
     * @param dst the destination unit
     * @param v the value
     * @param src the source unit
     * @return the value in the destination unit
     */
    public static double convert(TimeUnit dst, double v, TimeUnit src) {
        double w = v * dst.convert(1, src);
        if (w == 0d) {
            w = v / (src.convert(1, dst));
        }
        return w;
    }

    private static double getTraceWindowWidth(TimeUnit traceTimeUnit, double windowWidth, TimeUnit windowTimeUnit) {
        return convert(traceTimeUnit, windowWidth, windowTimeUnit);
    }

    /**
     * Creates a latency signal based on grouping of events by an {@code idAtt} attribute: all events with the same
     * value for this attribute are assumed to belong to the processing of a single object.
     *
     * @param trace the trace
     * @param idAttribute the attribute used for grouping of the events
     * @param scaleUnit the unit in which the latency is expressed (unit of the range of the function, e.g.,
     *     MILLISECOND)
     * @param windowWidth the width of the convolution window
     * @param windowUnit the time unit of the convolution window width
     * @return a signal for the latency of object processing
     */
    public static IPsop getLatency(ITrace trace, String idAttribute, TimeUnit scaleUnit, double windowWidth,
            TimeUnit windowUnit)
    {
        double scaleFactor = convert(scaleUnit, 1, trace.getTimeUnit());
        SignalModifier mod = new SignalModifier(scaleFactor, windowWidth, windowUnit);
        List<LinearSignalFragment> wip = createWipEventSignal(trace, idAttribute);
        List<LinearSignalFragment> tp = createTpEventSignal(trace, idAttribute);
        List<LinearSignalFragment> lat = EventSignalUtil.divide(wip, tp, 0d);
        if (mod.appliesConvolution()) {
            return applyConvolution(trace.getTimeUnit(), TraceHelper.getDomain(trace), mod, lat);
        } else {
            return EventSignalUtil.toIPsop(EventSignalUtil.scale(lat, mod.getScale()));
        }
    }

    /**
     * Creates a throughput signal based on grouping of events by an {@code idAtt} attribute: all events with the same
     * value for this attribute are assumed to belong to the processing of a single object.
     *
     * @param trace the trace
     * @param idAttribute the attribute used for grouping of the events
     * @param scaleUnit 1 / the unit in which the throughput is expressed (e.g., per SECOND)
     * @param windowWidth the width of the convolution window
     * @param windowUnit the time unit of the convolution window width
     * @return a signal for the throughput of object processing
     */
    public static IPsop getTP(ITrace trace, String idAtt, TimeUnit scaleUnit, double windowWidth, TimeUnit windowUnit) {
        double scaleFactor = convert(trace.getTimeUnit(), 1, scaleUnit);
        SignalModifier mod = new SignalModifier(scaleFactor, windowWidth, windowUnit);
        return modifySignal(trace.getTimeUnit(), TraceHelper.getDomain(trace), createTpEventSignal(trace, idAtt), mod);
    }

    /**
     * Creates a throughput signal based on events. Each event is assumed to represent the processing of a single
     * object. NOTE: a convolution must be specified, i.e., {@code windowWidth > 0}
     *
     * @param traceTimeUnit the time unit of the trace (and thus also of the events)
     * @param dom the domain to which to project the result to
     * @param events the events for the throughput signal
     * @param scaleUnit 1 / the unit in which the throughput is expressed (e.g., per SECOND)
     * @param windowWidth the width of the convolution window
     * @param windowUnit the time unit of the convolution window width
     * @return the event-based throughput signal
     */
    public static IPsop getTP(TimeUnit traceTimeUnit, IInterval dom, List<IEvent> events, TimeUnit scaleUnit,
            double windowWidth, TimeUnit windowUnit)
    {
        double scaleFactor = convert(traceTimeUnit, 1, scaleUnit);
        SignalModifier mod = new SignalModifier(scaleFactor, windowWidth, windowUnit);
        if (!mod.appliesConvolution()) {
            throw new IllegalArgumentException("instantaneous event-based tp not supported");
        }
        return modifySignal(traceTimeUnit, dom, createTpEventSignal(events), mod);
    }

    /**
     * Creates a work-in-progress signal based on grouping of events by an {@code idAtt} attribute: all events with the
     * same value for this attribute are assumed to belong to the processing of a single object.
     *
     * @param trace the trace
     * @param idAttribute the attribute used for grouping of the events
     * @param mod the modifier (see {@link SignalModifier#forWip(double, TimeUnit)})
     * @return a signal for the wip of object processing
     */
    public static IPsop getWip(ITrace trace, String idAtt, SignalModifier mod) {
        return modifySignal(trace.getTimeUnit(), TraceHelper.getDomain(trace), createWipEventSignal(trace, idAtt), mod);
    }

    /**
     * Creates a signal with the number of concurrent clients on a given resource.
     *
     * @param trace the trace
     * @param resource the resource
     * @param mod the signal modifier
     * @return a signal with the number of concurrent clients on a given resource
     */
    public static IPsop getResourceClients(ITrace trace, IResource resource, SignalModifier mod) {
        return modifySignal(trace.getTimeUnit(), TraceHelper.getDomain(trace),
                createResourceUtilizationSignal(trace, resource), mod);
    }

    /**
     * Creates a signal with the total claimed amount of a given resource.
     *
     * @param trace the trace
     * @param resource the resource
     * @param mod the signal modifier
     * @return a signal with the total claimed amount of a given resource
     */
    public static IPsop getResourceAmount(ITrace trace, IResource resource, SignalModifier mod) {
        return modifySignal(trace.getTimeUnit(), TraceHelper.getDomain(trace),
                createResourceAmountSignal(trace, resource), mod);
    }

    private static IPsop modifySignal(TimeUnit traceTimeUnit, IInterval dom, List<LinearSignalFragment> events,
            SignalModifier mod)
    {
        if (mod.appliesConvolution()) {
            return applyConvolution(traceTimeUnit, dom, mod, events);
        } else {
            return applyInstantaneous(dom, mod, events);
        }
    }

    private static IPsop applyInstantaneous(IInterval dom, SignalModifier mod, List<LinearSignalFragment> events) {
        List<LinearSignalFragment> inst = EventSignalUtil.computeInstantaneous(events, mod.getScale(), dom);
        return EventSignalUtil.toIPsop(inst);
    }

    private static IPsop applyConvolution(TimeUnit traceTimeUnit, IInterval tdom, SignalModifier mod,
            List<LinearSignalFragment> lsfs)
    {
        double scale = mod.getScale();
        double w = getTraceWindowWidth(traceTimeUnit, mod.getWindowWidth(), mod.getWindowTimeUnit());
        Interval dom = new Interval(tdom.lb(), false, tdom.ub().doubleValue() + w * 1.1, true);
        return EventSignalUtil.convoluteScaleAndProject(lsfs, scale, w, dom);
    }

    /**
     * Returns a list of constant fragments with value 1 (thus counting the number of objects that are being processed).
     * The fragments are ordered ascending by their start time.
     */
    private static List<LinearSignalFragment> createWipEventSignal(ITrace trace, String idAtt) {
        Map<String, double[]> timeStampsAndValue = createTimeStampValueMap(trace, idAtt);
        List<LinearSignalFragment> events = new ArrayList<>();
        for (Map.Entry<String, double[]> e: timeStampsAndValue.entrySet()) {
            double[] ts = e.getValue();
            events.add(new LinearSignalFragment(ts[0], ts[1], 1d, 1d));
        }
        EventSignalUtil.sortStartAsc(events);
        return events;
    }

    /**
     * Returns a list of constant fragments with value 1/(t1-t0), which is a measure for the items processed per
     * timeunit (throughput). The fragments are ordered ascending by their start time.
     */
    private static List<LinearSignalFragment> createTpEventSignal(ITrace trace, String idAtt) {
        Map<String, double[]> timeStampsAndValue = createTimeStampValueMap(trace, idAtt);
        List<LinearSignalFragment> events = new ArrayList<>();
        for (Map.Entry<String, double[]> e: timeStampsAndValue.entrySet()) {
            double[] ts = e.getValue();
            double val = 1d / (ts[1] - ts[0]);
            events.add(new LinearSignalFragment(ts[0], ts[1], val, val));
        }
        EventSignalUtil.sortStartAsc(events);
        return events;
    }

    /**
     * @param list a list of events, sorted ascending on their timestamp
     */
    private static List<LinearSignalFragment> createTpEventSignal(List<IEvent> list) {
        List<LinearSignalFragment> events = new ArrayList<>();
        for (IEvent e: list) {
            events.add(new LinearSignalFragment(e.getTimestamp().doubleValue(), 1d));
        }
        return events;
    }

    private static Map<String, double[]> createTimeStampValueMap(ITrace trace, String idAtt) {
        Map<String, double[]> timeStampsAndValue = new HashMap<>();
        for (IEvent e: trace.getEvents()) {
            if (e.getAttributeValue(idAtt) != null) {
                double[] vals = getOrCreate(idAtt, timeStampsAndValue, e);
                vals[0] = Math.min(vals[0], e.getTimestamp().doubleValue());
                vals[1] = Math.max(vals[1], e.getTimestamp().doubleValue());
            }
        }
        return timeStampsAndValue;
    }

    private static double[] getOrCreate(String idAtt, Map<String, double[]> timeStampsAndValue, IEvent e) {
        String key = createObjectKey(idAtt, e);
        double[] stamps = timeStampsAndValue.get(key);
        if (stamps == null) {
            stamps = new double[2];
            stamps[0] = Double.POSITIVE_INFINITY;
            stamps[1] = Double.NEGATIVE_INFINITY;
            timeStampsAndValue.put(key, stamps);
        }
        return stamps;
    }

    /**
     * Assumes that every claim has the idAtt attributes; if not, an exception is thrown
     */
    private static String createObjectKey(String idAtt, IEvent e) {
        String v = e.getAttributeValue(idAtt);
        Integer val = null;
        try {
            if (v == null) {
                throw new IllegalStateException("event " + e + " has no integer id attribute " + idAtt);
            }
            val = Integer.parseInt(v);
        } catch (NumberFormatException ex) {
            throw new IllegalStateException("event " + e + " has no integer id attribute " + idAtt);
        }
        return idAtt + "=" + val;
    }

    private static List<LinearSignalFragment> createResourceUtilizationSignal(ITrace trace, IResource resource) {
        List<LinearSignalFragment> events = new ArrayList<LinearSignalFragment>();
        for (IClaim c: trace.getClaims()) {
            if (c.getResource().equals(resource)) {
                events.add(
                        new LinearSignalFragment(c.getStartTime().doubleValue(), c.getEndTime().doubleValue(), 1d, 1d));
            }
        }
        EventSignalUtil.sortStartAsc(events);
        return events;
    }

    private static List<LinearSignalFragment> createResourceAmountSignal(ITrace trace, IResource resource) {
        List<LinearSignalFragment> events = new ArrayList<LinearSignalFragment>();
        for (IClaim c: trace.getClaims()) {
            if (c.getResource().equals(resource)) {
                double v = c.getAmount().doubleValue();
                if (v > 0d) {
                    events.add(new LinearSignalFragment(c.getStartTime().doubleValue(), c.getEndTime().doubleValue(), v,
                            v));
                }
            }
        }
        EventSignalUtil.sortStartAsc(events);
        return events;
    }
}
