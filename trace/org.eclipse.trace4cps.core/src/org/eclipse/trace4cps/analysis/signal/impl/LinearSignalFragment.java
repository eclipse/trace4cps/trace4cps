/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.signal.impl;

/**
 * A linear fragment to construct piecewise-linear signals. Immutable.
 */
public class LinearSignalFragment {
    private final double t0;

    private final double t1;

    private final double y0;

    private final double y1;

    /**
     * Constructs an instantaneaous fragment.
     */
    public LinearSignalFragment(double t, double y) {
        this.t0 = t;
        this.t1 = t;
        this.y0 = y;
        this.y1 = y;
    }

    public LinearSignalFragment(double t0, double t1, double y) {
        if (t1 < t0 || t0 == t1) {
            throw new IllegalArgumentException();
        }
        this.t0 = t0;
        this.t1 = t1;
        this.y0 = y;
        this.y1 = y;
    }

    public LinearSignalFragment(double t0, double t1, double y0, double y1) {
        if (t1 < t0 || t0 == t1) {
            throw new IllegalArgumentException();
        }
        this.t0 = t0;
        this.t1 = t1;
        this.y0 = y0;
        this.y1 = y1;
    }

    public double getT0() {
        return t0;
    }

    public double getT1() {
        return t1;
    }

    public double getY0() {
        return y0;
    }

    public double getY1() {
        return y1;
    }

    public boolean isConstant() {
        return y0 == y1;
    }

    public boolean isInstantaneousEvent() {
        return t0 == t1;
    }

    public double getWidth() {
        return t1 - t0;
    }

    public double getCoefficient() {
        if (isInstantaneousEvent()) {
            throw new IllegalStateException("instantaneous fragment has no coefficient");
        }
        if (isConstant()) {
            return 0;
        }
        return (y1 - y0) / (t1 - t0);
    }

    @Override
    public String toString() {
        return "LinearSignalFragment[t0=" + t0 + ", t1=" + t1 + ", y0=" + y0 + ", y1=" + y1 + "]";
    }
}
