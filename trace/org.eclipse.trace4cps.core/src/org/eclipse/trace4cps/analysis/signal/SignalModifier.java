/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.signal;

import java.util.concurrent.TimeUnit;

/**
 * A signal modifier to modify the scaling of the signal, and the applied convolution.
 */
public class SignalModifier {
    public static final SignalModifier NONE = new SignalModifier(1d);

    private final double scaleFactor;

    private final double windowWidth;

    private final TimeUnit windowTimeUnit;

    /**
     * Creates a new signal modifier without convolution.
     *
     * @param scaleFactor the scaling
     */
    public SignalModifier(double scaleFactor) {
        this.scaleFactor = scaleFactor;
        this.windowWidth = 0;
        this.windowTimeUnit = null;
    }

    /**
     * Creates a new signal modifier.
     *
     * @param scaleFactor the scale factor
     * @param windowWidth the width of the convolution window ({@code <= 0}) means no convolution)
     * @param windowTimeUnit the time unit of the convolution window width
     */
    public SignalModifier(double scaleFactor, double windowWidth, TimeUnit windowTimeUnit) {
        this.scaleFactor = scaleFactor;
        this.windowWidth = windowWidth;
        this.windowTimeUnit = windowTimeUnit;
    }

    /**
     * @return the scale factor to apply to the signal
     */
    public double getScale() {
        return scaleFactor;
    }

    /**
     * @return whether convolution needs to be applied to the signal
     */
    public boolean appliesConvolution() {
        return windowWidth > 0;
    }

    /**
     * @return the timeunit of the convolution window width
     */
    public TimeUnit getWindowTimeUnit() {
        return windowTimeUnit;
    }

    /**
     * @return the width of the convolution window
     */
    public double getWindowWidth() {
        return windowWidth;
    }
}
