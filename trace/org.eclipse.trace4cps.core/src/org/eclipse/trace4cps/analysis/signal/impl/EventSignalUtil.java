/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.signal.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.trace4cps.core.IInterval;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.impl.Interval;
import org.eclipse.trace4cps.core.impl.Psop;
import org.eclipse.trace4cps.core.impl.PsopFragment;

public final class EventSignalUtil {
    private EventSignalUtil() {
    }

    /**
     * Sorts ascending with respect to the start timestamp of the event signal instances.
     *
     * @param events a list of event signal instances
     */
    public static void sortStartAsc(List<LinearSignalFragment> events) {
        Collections.sort(events, new Comparator<LinearSignalFragment>() {
            @Override
            public int compare(LinearSignalFragment o1, LinearSignalFragment o2) {
                return Double.compare(o1.getT0(), o2.getT0());
            }
        });
    }

    /**
     * Converts a list of linear signal fragments to a Psop.
     */
    public static Psop toIPsop(List<LinearSignalFragment> events) {
        Psop r = new Psop();
        for (LinearSignalFragment e: events) {
            Interval i = new Interval(e.getT0(), false, e.getT1(), true);
            r.add(new PsopFragment(e.getY0(), e.getCoefficient(), 0d, i));
        }
        return r;
    }

    /**
     * @param eventSignal a list of possibly overlapping {@link LinearSignalFragment} instances; all must be constant
     * @param scale the scale factor of the result; e.g., using 60 gives the value per 60 time units
     * @param dom the domain for which to compute the result
     * @return a list of {@link LinearSignalFragment} instances that represents the sum of the given list of
     *     {@link LinearSignalFragment} instances
     */
    public static List<LinearSignalFragment> computeInstantaneous(List<LinearSignalFragment> eventSignal, double scale,
            IInterval dom)
    {
        List<Event> events = createEventList(eventSignal);
        List<LinearSignalFragment> r = createConstantSignal(scale, events);
        return setDomain(r, dom, true);
    }

    /**
     * @param events the events for which to compute the sliding-window average; these must all be constant!
     * @param scale the result is multiplied with this scale factor; e.g., using 60 gives the sliding window average per
     *     60 time units
     * @param w the width of the convolution window: larger means smoother graphs, 0 gives the instantaneous value
     * @param dom the domain of the result
     * @return the resulting psop
     */
    public static final IPsop convoluteScaleAndProject(List<LinearSignalFragment> events, double scale, double w,
            Interval dom)
    {
        List<LinearSignalFragment> cl = convolution(events, w);
        cl = scale(cl, scale);
        return toIPsop(setDomain(cl, dom, true));
    }

    /**
     * @param dividend must be constant fragments
     * @param divisor must be constant fragments
     */
    public static List<LinearSignalFragment> divide(List<LinearSignalFragment> dividend,
            List<LinearSignalFragment> divisor, double undefinedValue)
    {
        List<Event> l1 = createEventList(dividend);
        List<Event> l2 = createEventList(divisor);
        if (l1.isEmpty() || l2.isEmpty()) {
            throw new IllegalArgumentException("cannot divide with empty input");
        }
        Interval dom = new Interval(Math.max(l1.get(0).t, l2.get(0).t), false,
                Math.min(l1.get(l1.size() - 1).t, l2.get(l2.size() - 1).t), true);

        List<LinearSignalFragment> c1 = createConstantSignal(1d, l1);
        List<LinearSignalFragment> c2 = createConstantSignal(1d, l2);

        c1 = setDomain(c1, dom, true);
        c2 = setDomain(c2, dom, true);

        splitAtEvents(c1, l2);
        splitAtEvents(c2, l1);

        // all LinearSignalFragments are aligned now
        // compute l1 / l2
        List<LinearSignalFragment> result = new ArrayList<>();
        for (int i = 0; i < c1.size(); i++) {
            LinearSignalFragment f1 = c1.get(i);
            LinearSignalFragment f2 = c2.get(i);
            double v1 = f1.getY0(); // assumption: all constant fragments
            double v2 = f2.getY0();
            double v = undefinedValue;
            if (v2 != 0d) {
                v = v1 / v2;
            }
            result.add(new LinearSignalFragment(f1.getT0(), f1.getT1(), v, v));
        }
        return result;
    }

    private static void splitAtEvents(List<LinearSignalFragment> cl, List<Event> el) {
        int i = 0;
        for (Event e: el) {
            // find the next fragment that contains e.t
            while (cl.get(i).getT1() < e.t) {
                i++;
            }
            // Invariant: e.t <= cl.get(i).getT1()
            if (cl.get(i).getT0() < e.t && e.t < cl.get(i).getT1()) {
                doSplit(cl, i, e.t);
            }
        }
    }

    private static List<LinearSignalFragment> createConstantSignal(double scale, List<Event> events) {
        List<LinearSignalFragment> r = new ArrayList<>();
        Event prev = null;
        double value = 0d;
        for (Event e: events) {
            if (prev != null) {
                value += (prev.delta * scale);
                r.add(new LinearSignalFragment(prev.t, e.t, value, value));
            }
            prev = e;
        }
        return r;
    }

    private static List<Event> createEventList(List<LinearSignalFragment> eventSignal) {
        // precondition: all constant fragments
        List<Event> events = new ArrayList<Event>();
        for (LinearSignalFragment s: eventSignal) {
            if (s.isInstantaneousEvent()) {
                throw new UnsupportedOperationException("instantaneous events not suppprted");
            }
            events.add(new Event(s.getT0(), s.getY0()));
            events.add(new Event(s.getT1(), -s.getY0()));
        }
        Collections.sort(events);
        // merge events with the same timestamp:
        events = merge(events);
        return events;
    }

    /**
     * Scales the given linear signal fragments: multiplied by {@code c}
     *
     * @param fl the list of linear signal fragments.
     * @return the scaled signal
     */
    public static List<LinearSignalFragment> scale(List<LinearSignalFragment> fl, double c) {
        List<LinearSignalFragment> r = new ArrayList<LinearSignalFragment>();
        for (LinearSignalFragment f: fl) {
            r.add(new LinearSignalFragment(f.getT0(), f.getT1(), f.getY0() * c, f.getY1() * c));
        }
        return r;
    }

    private static List<LinearSignalFragment> convolution(List<LinearSignalFragment> fl, double w) {
        // checkAndPad(fl, false);
        List<LinearSignalFragment> result = new ArrayList<>();
        List<LinearSignalFragment> conv = new ArrayList<>();
        for (LinearSignalFragment f: fl) {
            conv.clear();
            convolutionWithBlock(conv, f, w);
            addTo(result, conv);
        }
        return result;
    }

    private static void addTo(List<LinearSignalFragment> result, List<LinearSignalFragment> l) {
        if (result.isEmpty()) {
            result.addAll(l);
            checkAndPad(result, true);
            return;
        }
        if (l == null || l.isEmpty()) {
            return;
        }
        // Invariant: result is padded and contains l
        padToContain(result, l.get(0).getT0(), l.get(l.size() - 1).getT1());

        // Add each fragment of l to the fragments of cl:
        // Assumption: fragments in cl and l are sorted w.r.t. t0, and are
        // non-overlapping.
        // This is guaranteed from the beginning as a single convolution result is added
        // Now guarantee that the following merge function preserves it:
        for (LinearSignalFragment f: l) {
            double t0cl = result.get(0).getT0();
            double t1cl = result.get(result.size() - 1).getT1();
            // There are six cases how f can overlap with cl
            if (f.getT1() <= t0cl) {
                result.add(0, f);
            } else if (f.getT0() >= t1cl) {
                result.add(f);
            } else { // some overlap
                split(result, f.getT0());
                split(result, f.getT1());
                int startIndex = findIndex(result, f.getT0());
                int endIndex = findIndex(result, f.getT1());
                if (startIndex == -1) {
                    startIndex = 0;
                }
                if (endIndex == -1) {
                    endIndex = result.size();
                }
                // now add the fragment to cl[startIndex] - cl[endIndex]
                double offset = f.getY0();
                if (f.getT0() < t0cl) {
                    offset += f.getCoefficient() * (t0cl - f.getT0());
                }
                for (int i = startIndex; i < endIndex; i++) {
                    LinearSignalFragment cf = result.remove(i);
                    double y0 = cf.getY0() + offset;
                    double y1 = cf.getY1() + offset + f.getCoefficient() * cf.getWidth();
                    result.add(i, new LinearSignalFragment(cf.getT0(), cf.getT1(), y0, y1));
                    offset += f.getCoefficient() * cf.getWidth();
                }
            }
        }
        checkAndPad(result, true);
    }

    private static void checkAndPad(List<LinearSignalFragment> fl, boolean pad) {
        // non-overlapping and consecutive
        int i = 0;
        int ub = fl.size() - 1;
        while (i < ub) {
            LinearSignalFragment f1 = fl.get(i);
            LinearSignalFragment f2 = fl.get(i + 1);
            if (f1.getT1() > f2.getT0()) {
                throw new IllegalStateException("not strictly sorted");
            }
            // if (f1.getT1() == f2.getT0() && f1.getY1() != f2.getY0()) {
            // throw new IllegalStateException("not consecutive");
            // }
            // pad if necessary:
            if (pad && f1.getT1() < f2.getT0()) {
                fl.add(i + 1, new LinearSignalFragment(f1.getT1(), f2.getT0(), 0d, 0d));
                i++;
                ub++;
            }
            i++;
        }
    }

    /**
     * Convolutes the given {@link ConstantSignalFragment} with a block function on domain [b0, b0 + w] with value 1/w.
     * The result is put into the given list of {@link LinearSignalFragment} instances to avoid allocation and
     * deallocation of many List instances.
     * </p>
     * By default, b0 = 0, such that this gives the average value over the last w time units. If b0 = - w/2 for
     * instance, then the average value around t with window width w is computed.
     *
     * @param r the result list
     * @param f the event signal to convolute
     * @param w the width of the block function
     */
    private static void convolutionWithBlock(List<LinearSignalFragment> r, LinearSignalFragment f, double w) {
        if (!f.isConstant()) {
            throw new IllegalStateException("convolution only supported for constant signal fragments");
        }
        final double b0 = 0;
        final double b1 = b0 + w;
        final double fVal = f.getY0();
        if (f.isInstantaneousEvent()) {
            // Scaled and translated Dirac delta function
            double y = fVal / w;
            r.add(new LinearSignalFragment(f.getT0() + b0, f.getT0() + b1, y, y));
        } else {
            double y = (Math.min(w, f.getWidth()) * fVal) / w;
            double[] ts = new double[4];
            ts[0] = f.getT0() + b0;
            ts[1] = f.getT0() + b1;
            ts[2] = f.getT1() + b0;
            ts[3] = f.getT1() + b1;
            Arrays.sort(ts);
            r.add(new LinearSignalFragment(ts[0], ts[1], 0d, y));
            if (ts[1] < ts[2]) {
                r.add(new LinearSignalFragment(ts[1], ts[2], y, y));
            }
            r.add(new LinearSignalFragment(ts[2], ts[3], y, 0d));
        }
    }

    private static void padToContain(List<LinearSignalFragment> cl, double t0, double t1) {
        if (t0 < cl.get(0).getT0()) {
            cl.add(new LinearSignalFragment(t0, cl.get(0).getT0(), 0d, 0d));
        }
        if (t1 > cl.get(cl.size() - 1).getT1()) {
            cl.add(new LinearSignalFragment(cl.get(cl.size() - 1).getT1(), t1, 0d, 0d));
        }
    }

    private static int findIndex(List<LinearSignalFragment> cl, double t) {
        for (int i = 0; i < cl.size(); i++) {
            if (cl.get(i).getT0() == t) {
                return i;
            }
        }
        return -1;
    }

    private static List<Event> merge(List<Event> events) {
        int i = 0;
        List<Event> r = new ArrayList<Event>();
        while (i < events.size()) {
            Event ei = events.get(i);
            int j = i + 1;
            double extraDelta = 0d;
            while (j < events.size() && events.get(j).t == ei.t) {
                extraDelta += events.get(j).delta;
                j++;
            }
            i = j - 1;
            r.add(new Event(ei.t, ei.delta + extraDelta));
            i++;
        }
        return r;
    }

    private static List<LinearSignalFragment> setDomain(List<LinearSignalFragment> ls, IInterval dom,
            boolean padWithZeros)
    {
        if (ls.isEmpty()) {
            throw new IllegalArgumentException("cannot set the domain of an empty signal");
        }
        // Pad first
        if (getTmin(ls) > dom.lb().doubleValue()) {
            double val = padWithZeros ? 0d : getYmin(ls);
            ls.add(0, new LinearSignalFragment(dom.lb().doubleValue(), getTmin(ls), val, val));
        }
        if (dom.ub().doubleValue() > getTmax(ls)) {
            double val = padWithZeros ? 0d : getYmax(ls);
            ls.add(new LinearSignalFragment(getTmax(ls), dom.ub().doubleValue(), val, val));
        }
        // Project later
        return projectTo(ls, dom.lb().doubleValue(), dom.ub().doubleValue());
    }

    private static List<LinearSignalFragment> projectTo(List<LinearSignalFragment> ls, double tmin, double tmax) {
        int i1 = split(ls, tmin);
        int i2 = split(ls, tmax);
//        if (i1 == -1) {
//            i1 = 0;
//        }
        if (i2 == -1) {
            i2 = ls.size() - 1;
        }
        return ls.subList(i1 + 1, i2 + 1);
    }

    private static int split(List<LinearSignalFragment> ls, double t) {
        for (int i = 0; i < ls.size(); i++) {
            if (ls.get(i).getT0() < t && t < ls.get(i).getT1()) {
                doSplit(ls, i, t);
                return i;
            }
        }
        return -1;
    }

    private static void doSplit(List<LinearSignalFragment> ls, int i, double t) {
        LinearSignalFragment f = ls.remove(i);
        double valueAtt = f.getY0() + (t - f.getT0()) * f.getCoefficient();
        LinearSignalFragment f1 = new LinearSignalFragment(f.getT0(), t, f.getY0(), valueAtt);
        LinearSignalFragment f2 = new LinearSignalFragment(t, f.getT1(), valueAtt, f.getY1());
        ls.add(i, f1);
        ls.add(i + 1, f2);
    }

    private static double getTmin(List<LinearSignalFragment> ls) {
        return ls.get(0).getT0();
    }

    private static double getYmin(List<LinearSignalFragment> ls) {
        return ls.get(0).getY0();
    }

    private static double getTmax(List<LinearSignalFragment> ls) {
        return ls.get(ls.size() - 1).getT1();
    }

    private static double getYmax(List<LinearSignalFragment> ls) {
        return ls.get(ls.size() - 1).getY1();
    }

    private static final class Event implements Comparable<Event> {
        private final double t;

        private final double delta;

        public Event(double t, double delta) {
            this.t = t;
            this.delta = delta;
        }

        @Override
        public int compareTo(Event o) {
            return Double.compare(t, o.t);
        }

        @Override
        public String toString() {
            return "Event [t=" + t + ", delta=" + delta + "]";
        }
    }
}
