/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.signal.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.trace4cps.analysis.stl.impl.STLUtil;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IInterval;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.IPsopFragment;
import org.eclipse.trace4cps.core.Shape;
import org.eclipse.trace4cps.core.impl.Claim;
import org.eclipse.trace4cps.core.impl.Interval;
import org.eclipse.trace4cps.core.impl.Psop;
import org.eclipse.trace4cps.core.impl.PsopFragment;
import org.eclipse.trace4cps.core.impl.Resource;

/**
 * A utility class with methods for {@link IPsop} instances.
 */
public class PsopHelper {
    private PsopHelper() {
    }

    public static int size(IPsop p) {
        return p.getFragments().size();
    }

    public static IPsop translate(IPsop p, double delta) {
        Psop p2 = new Psop();
        for (Map.Entry<String, String> e: p.getAttributes().entrySet()) {
            p2.setAttribute(e.getKey(), e.getValue());
        }
        for (IPsopFragment f: p.getFragments()) {
            double lb = BigDecimal.valueOf(f.dom().lb().doubleValue()).add(BigDecimal.valueOf(delta)).doubleValue();
            double ub = BigDecimal.valueOf(f.dom().ub().doubleValue()).add(BigDecimal.valueOf(delta)).doubleValue();
            if (lb < ub) { // rounding errors can cause lb >= ub; in that case we ignore the fragment as it
                           // was really small
                IInterval i2 = new Interval(lb, f.dom().isOpenLb(), ub, f.dom().isOpenUb());
                p2.add(new PsopFragment(f.getC(), f.getB(), f.getA(), i2));
            }
        }
        return p2;
    }

    public static IPsop max(IPsop p1, IPsop p2) {
        return minMax(p1, p2, true);
    }

    public static IPsop min(IPsop p1, IPsop p2) {
        return minMax(p1, p2, false);
    }

    private static IPsop minMax(IPsop p1, IPsop p2, boolean isMax) {
        IPsop f1 = copy(p1);
        IPsop f2 = copy(p2);
        IInterval d = Interval.intersect(dom(f1), dom(f2));
        if (d.isEmpty()) {
            return new Psop();
        }
        projectTo(f1, d);
        projectTo(f2, d);
        if (isMax) {
            return STLUtil.signalOr(f1, f2);
        } else {
            return STLUtil.signalAnd(f1, f2);
        }
    }

    public static IPsop add(IPsop p, double cnst) {
        Psop r = new Psop();
        for (IPsopFragment f: p.getFragments()) {
            double c = BigDecimal.valueOf(f.getC().doubleValue()).add(BigDecimal.valueOf(cnst)).doubleValue();
            r.add(new PsopFragment(c, f.getB(), f.getA(), f.dom()));
        }
        return r;
    }

    public static IPsop add(IPsop p1, IPsop p2) {
        return addSub(p1, p2, true);
    }

    public static IPsop sub(IPsop p1, IPsop p2) {
        return addSub(p1, p2, false);
    }

    private static IPsop addSub(IPsop p1, IPsop p2, boolean isAdd) {
        Psop r = new Psop();
        // Align the two position functions:
        IPsop g1 = PsopHelper.copy(p1);
        PsopHelper.alignWith(g1, p2);
        IPsop g2 = PsopHelper.copy(p2);
        PsopHelper.alignWith(g2, p1);
        // Invariant: g1 and g2 have exactly the same fragments w.r.t. time domain
        for (int i = 0; i < g1.getFragments().size(); i++) {
            IPsopFragment f1 = g1.getFragments().get(i);
            IPsopFragment f2 = g2.getFragments().get(i);
            if (isAdd) {
                double c = BigDecimal.valueOf(f1.getC().doubleValue()).add(BigDecimal.valueOf(f2.getC().doubleValue()))
                        .doubleValue();
                double b = BigDecimal.valueOf(f1.getB().doubleValue()).add(BigDecimal.valueOf(f2.getB().doubleValue()))
                        .doubleValue();
                double a = BigDecimal.valueOf(f1.getA().doubleValue()).add(BigDecimal.valueOf(f2.getA().doubleValue()))
                        .doubleValue();
                r.add(new PsopFragment(c, b, a, f1.dom()));
            } else {
                double c = BigDecimal.valueOf(f1.getC().doubleValue())
                        .subtract(BigDecimal.valueOf(f2.getC().doubleValue())).doubleValue();
                double b = BigDecimal.valueOf(f1.getB().doubleValue())
                        .subtract(BigDecimal.valueOf(f2.getB().doubleValue())).doubleValue();
                double a = BigDecimal.valueOf(f1.getA().doubleValue())
                        .subtract(BigDecimal.valueOf(f2.getA().doubleValue())).doubleValue();
                r.add(new PsopFragment(c, b, a, f1.dom()));
            }
        }
        return r;
    }

    public static IPsop mult(IPsop p, double cnst) {
        Psop r = new Psop();
        for (IPsopFragment f: p.getFragments()) {
            double c = BigDecimal.valueOf(f.getC().doubleValue()).multiply(BigDecimal.valueOf(cnst)).doubleValue();
            double b = BigDecimal.valueOf(f.getB().doubleValue()).multiply(BigDecimal.valueOf(cnst)).doubleValue();
            double a = BigDecimal.valueOf(f.getA().doubleValue()).multiply(BigDecimal.valueOf(cnst)).doubleValue();
            r.add(new PsopFragment(c, b, a, f.dom()));
        }
        return r;
    }

    public static Interval dom(IPsop p) {
        if (p.getFragments().isEmpty()) {
            throw new IllegalStateException("empty function has no domain");
        }
        Number lb = p.getFragments().get(0).dom().lb();
        Number ub = p.getFragments().get(p.getFragments().size() - 1).dom().ub();
        return new Interval(lb, false, ub, true);
    }

    public static IPsop createDerivativeOf(IPsop p) {
        Psop der = new Psop();
        der.setAttributes(p.getAttributes());
        for (IPsopFragment f: p.getFragments()) {
            IPsopFragment fn = new PsopFragment(f.getB(), 2 * f.getA().doubleValue(), 0d, f.dom());
            der.add(fn);
        }
        return der;
    }

    public static Number valueAt(IPsop p, Number t) {
        for (int i = 0; i < p.getFragments().size(); i++) {
            IPsopFragment f = p.getFragments().get(i);
            if (f.dom().contains(t) || (i == size(p) - 1 && t.doubleValue() == f.dom().ub().doubleValue())) {
                return valueAt(f, t);
            }
        }
        throw new IllegalArgumentException(t + " not in domain");
    }

    public static PsopFragment copy(IPsopFragment f) {
        return new PsopFragment(f.getC(), f.getB(), f.getA(), f.dom());
    }

    public static Psop copy(IPsop p) {
        Psop f = new Psop();
        f.setAttributes(p.getAttributes());
        for (IPsopFragment frag: p.getFragments()) {
            f.addAtEnd(copy(frag));
        }
        return f;
    }

    public static Number getDomainLowerBound(IPsop p) {
        if (size(p) == 0) {
            throw new IllegalStateException("function is empty");
        }
        return p.getFragments().get(0).dom().lb();
    }

    public static Number getDomainUpperBound(IPsop p) {
        if (size(p) == 0) {
            throw new IllegalStateException("function is empty");
        }
        return p.getFragments().get(p.getFragments().size() - 1).dom().ub();
    }

    public static Number getStartValue(IPsop p) {
        if (size(p) == 0) {
            throw new IllegalStateException("function is empty");
        }
        return p.getFragments().get(0).getC();
    }

    public static Number getMinValue(IPsop p) {
        if (size(p) == 0) {
            throw new IllegalStateException("function is empty");
        }
        MinMaxResult r = new MinMaxResult();
        for (IPsopFragment f: p.getFragments()) {
            r.combineWith(computeMinMax(f));
        }
        return r.getMin();
    }

    public static Number getMaxValue(IPsop p) {
        if (size(p) == 0) {
            throw new IllegalStateException("function is empty");
        }
        MinMaxResult r = new MinMaxResult();
        for (IPsopFragment f: p.getFragments()) {
            r.combineWith(computeMinMax(f));
        }
        return r.getMax();
    }

    /**
     * Possibly splits the fragments in the given Psop.
     */
    public static List<ShapeSegment> createMonotonicSegmentation(IPsop p) {
        int startIndex = 0;
        int endIndex = -1;
        int size = size(p);
        Shape previous = Shape.CONSTANT; // neutral; will never have value PARABOLA_*
        List<ShapeSegment> res = new ArrayList<>();
        int i = 0;
        while (i < size) {
            IPsopFragment frag = p.getFragments().get(i);
            Shape shape = frag.getShape();
            // First we check continuity: if not continuous: create a new segment
            if (i > 0 && nonContinuous(p.getFragments().get(i - 1), frag)) {
                // FIXME: add special case for stairs functions to increase efficiency
                res.add(new ShapeSegment(startIndex, endIndex, previous));
                if (shape == Shape.PARABOLA_CAP || shape == Shape.PARABOLA_CUP) {
                    double tZeroSlope = argZeroSlope(frag).doubleValue();
                    splitFragment(p.getFragments(), i, frag, tZeroSlope, valueAt(frag, tZeroSlope), 0d);
                    size++;
                    // Re-read the fragment (it has been split maybe):
                    frag = p.getFragments().get(i);
                    shape = frag.getShape();
                }
                previous = shape;
                endIndex++;
                startIndex = endIndex;
                i++;
            } else {
                if (shape == Shape.CONSTANT) {
                    endIndex++;
                    i++;
                } else if (shape == Shape.INCREASING) {
                    if (previous == Shape.CONSTANT || previous == Shape.INCREASING) {
                        previous = Shape.INCREASING;
                        endIndex++;
                    } else if (previous == Shape.DECREASING) {
                        res.add(new ShapeSegment(startIndex, endIndex, previous));
                        previous = Shape.INCREASING;
                        endIndex++;
                        startIndex = endIndex;
                    }
                    i++;
                } else if (shape == Shape.DECREASING) {
                    if (previous == Shape.CONSTANT || previous == Shape.DECREASING) {
                        previous = Shape.DECREASING;
                        endIndex++;
                    } else if (previous == Shape.INCREASING) {
                        res.add(new ShapeSegment(startIndex, endIndex, previous));
                        previous = Shape.DECREASING;
                        endIndex++;
                        startIndex = endIndex;
                    }
                    i++;
                } else if (shape == Shape.PARABOLA_CAP || shape == Shape.PARABOLA_CUP) {
                    double tZeroSlope = argZeroSlope(frag).doubleValue();
                    splitFragment(p.getFragments(), i, frag, tZeroSlope, valueAt(frag, tZeroSlope), 0d);
                    size++;
                    // don't increase i, the split fragments need to be considered again
                }
            }
        }
        // The last segment:
        res.add(new ShapeSegment(startIndex, endIndex, previous));
        return res;
    }

    private static boolean nonContinuous(IPsopFragment prev, IPsopFragment frag) {
        return Math.abs(frag.getC().doubleValue() - valueAt(prev, prev.dom().ub()).doubleValue()) > 1e-9;
    }

    private static void splitFragment(List<IPsopFragment> fragments, int fragIndex, IPsopFragment frag, Number xSplit) {
        Number c = valueAt(frag, xSplit);
        Number b = valueDerivativeAt(frag, xSplit);
        splitFragment(fragments, fragIndex, frag, xSplit, c, b);
    }

    private static void splitFragment(List<IPsopFragment> fragments, int fragIndex, IPsopFragment frag, Number xSplit,
            Number c, Number b)
    {
        if (xSplit.doubleValue() == frag.dom().lb().doubleValue()
                || xSplit.doubleValue() == frag.dom().ub().doubleValue())
        {
            return;
        }
        Number lb = frag.dom().lb();
        Number ub = frag.dom().ub();
        Interval d1 = new Interval(lb, false, xSplit, true);
        Interval d2 = new Interval(xSplit, false, ub, true);
        // Replace the frag with 2 new frags
        fragments.remove(fragIndex);
        fragments.add(fragIndex, new PsopFragment(c, b, frag.getA(), d2));
        fragments.add(fragIndex, new PsopFragment(frag.getC(), frag.getB(), frag.getA(), d1));
    }

    public static void projectTo(IPsop p, IInterval d) {
        if (size(p) == 0) {
            return;
        }
        if (p.getFragments().get(0).dom().lb().doubleValue() >= d.ub().doubleValue()) {
            p.getFragments().clear();
        } else if (p.getFragments().get(size(p) - 1).dom().ub().doubleValue() <= d.lb().doubleValue()) {
            p.getFragments().clear();
        } else {
            // we have overlap somewhere
            handleLowerBound(p, d);
            handleUpperBound(p, d);
        }
    }

    private static void handleLowerBound(IPsop p, IInterval d) {
        if (p.getFragments().get(0).dom().lb().doubleValue() >= d.lb().doubleValue()) {
            // no need to remove fragments from the front
            return;
        }
        int removeUpTo = -1;
        for (int i = 0; i < p.getFragments().size(); i++) {
            IPsopFragment frag = p.getFragments().get(i);
            if (frag.dom().contains(d.lb())) {
                // split if necessary;
                if (frag.dom().lb().doubleValue() != d.lb().doubleValue()) {
                    Number c = valueAt(frag, d.lb());
                    Number b = valueDerivativeAt(frag, d.lb());
                    p.getFragments().remove(i);
                    p.getFragments().add(i,
                            new PsopFragment(c, b, frag.getA(), new Interval(d.lb(), false, frag.dom().ub(), true)));
                }
                break;
            }
            removeUpTo++;
        }
        for (int i = 0; i <= removeUpTo; i++) {
            p.getFragments().remove(0);
        }
    }

    private static void handleUpperBound(IPsop p, IInterval d) {
        if (p.getFragments().get(size(p) - 1).dom().ub().doubleValue() <= d.ub().doubleValue()) {
            // no need to remove fragments from the back
            return;
        }
        int removeFrom = size(p);
        for (int i = 0; i < size(p); i++) {
            IPsopFragment frag = p.getFragments().get(i);
            if (frag.dom().contains(d.ub())) {
                // split if necessary;
                if (frag.dom().lb().doubleValue() != d.ub().doubleValue()) {
                    p.getFragments().remove(i);
                    p.getFragments().add(i, new PsopFragment(frag.getC(), frag.getB(), frag.getA(),
                            new Interval(frag.dom().lb(), false, d.ub(), true)));
                    removeFrom = i + 1;
                } else {
                    removeFrom = i;
                }
                break;
            }
        }
        int numToRemove = size(p) - removeFrom;
        for (int i = 0; i < numToRemove; i++) {
            p.getFragments().remove(removeFrom);
        }
    }

    /**
     * Aligns this IPsop with the given IPsop. The domain of the result function is the intersection of the domains of
     * this IPsop and the given IPsop. The fragments are split in such a way that they align with the fragments of the
     * given IPsop.
     *
     * @param p the IPsop to align
     * @param g the IPsop to align to
     */
    public static void alignWith(IPsop p, IPsop g) {
        projectTo(p, dom(g));
        IInterval dom = dom(p);
        // Split the fragments according to g:
        int index = 0;
        for (int i = 0; i < size(g) - 1; i++) {
            double tSplit = g.getFragments().get(i).dom().ub().doubleValue();
            if (tSplit > dom.lb().doubleValue() && tSplit < dom.ub().doubleValue()) {
                // Find the fragment that contains tSplit
                IPsopFragment frag = p.getFragments().get(index);
                while (!frag.dom().contains(tSplit)) {
                    index++;
                    frag = p.getFragments().get(index);
                }
                splitFragment(p.getFragments(), index, frag, tSplit);
            } else if (tSplit > dom.ub().doubleValue()) {
                // we are done
                break;
            }
        }
    }

    public static Number valueAt(IPsopFragment f, Number x) {
        if (!f.dom().contains(x) && x.doubleValue() != f.dom().ub().doubleValue()) {
            throw new IllegalArgumentException(x + " not in domain of this fragment");
        }

        if (f.dom().lb().doubleValue() == Double.NEGATIVE_INFINITY) {
            if (f.getA().doubleValue() != 0 || f.getB().doubleValue() != 0) {
                throw new IllegalStateException(); // we don't know now to handle this
            }
            // a constant fragment can be handled
            return f.getC();
        }
        double dt = x.doubleValue() - f.dom().lb().doubleValue();
        return f.getC().doubleValue() + f.getB().doubleValue() * dt + f.getA().doubleValue() * dt * dt;
    }

    public static Number valueDerivativeAt(IPsopFragment f, Number t) {
        double a = f.getA().doubleValue();
        double b = f.getB().doubleValue();

        if (!f.dom().contains(t) && t.doubleValue() != f.dom().ub().doubleValue()) {
            throw new IllegalArgumentException(t + " not in domain of this fragment");
        }
        if (f.dom().lb().doubleValue() == Double.NEGATIVE_INFINITY) {
            if (a != 0 || b != 0) {
                throw new IllegalStateException(); // we don't know now to handle this
            }
            // a constant fragment can be handled
            return 0d;
        }
        double dx = t.doubleValue() - f.dom().lb().doubleValue();
        return b + 2 * a * dx;
    }

    public static Number argZeroSlope(IPsopFragment f) {
        if (f.getShape() != Shape.PARABOLA_CAP && f.getShape() != Shape.PARABOLA_CUP) {
            throw new IllegalArgumentException("not a parabola shape");
        }
        double a = f.getA().doubleValue();
        double b = f.getB().doubleValue();
        double lb = f.dom().lb().doubleValue();
        // Compute the parameters of the quadratic equation a*x^2 + b*x + c
        return lb + (-b / (2 * a));
    }

    /**
     * @return the min and max (including arguments) of this fragment
     */
    public static MinMaxResult computeMinMax(IPsopFragment f) {
        double a = f.getA().doubleValue();
        double b = f.getB().doubleValue();
        double c = f.getC().doubleValue();
        double size = f.dom().ub().doubleValue() - f.dom().lb().doubleValue();
        // compute the values on the edges of the time domain:
        double v1 = c;
        double v2 = a * size * size + b * size + c;
        // Compute the value on the top/bottom of the parabola:
        if (f.getOrder() == 0) {
            return new MinMaxResult(v1);
        } else if (f.getOrder() == 1) {
            return new MinMaxResult(v1, v2);
        } else {
            double tTop = -b / (2 * a);
            if (0 <= tTop && tTop <= size) {
                double v3 = a * tTop * tTop + b * tTop + c;
                return new MinMaxResult(v1, v2, v3);
            } else {
                return new MinMaxResult(v1, v2);
            }
        }
    }

    /**
     * Requires that both fragments have the same time domain.
     * </p>
     * Empty list iff constant distance, or no intersections
     *
     * @param f1 fragment 1
     * @param f2 fragment 2
     * @return a list of domain points where the fragments intersect
     */
    public static List<Double> computeIntersections(IPsopFragment f1, IPsopFragment f2) {
        if (!f1.dom().equals(f2.dom())) {
            throw new IllegalArgumentException("fragments should have equal time domains");
        }

        // Compute the parameters of the quadratic equation a*x^2 + b*x + c
        double a = f1.getA().doubleValue() - f2.getA().doubleValue();
        double b = f1.getB().doubleValue() - f2.getB().doubleValue();
        double c = f1.getC().doubleValue() - f2.getC().doubleValue();

        if (a == 0 && b == 0) {
            return Collections.emptyList();
        } else if (a == 0) {
            double tSol = f1.dom().lb().doubleValue() + (-c / b);
            if (f1.dom().contains(tSol)) {
                return Collections.singletonList(tSol);
            } else {
                return Collections.emptyList();
            }
        } else {
            // FIXME: here are rounding errors...
            double t1 = f1.dom().lb().doubleValue() + ((-b - Math.sqrt((b * b) - (4 * a * c))) / (2 * a));
            double t2 = f1.dom().lb().doubleValue() + ((-b + Math.sqrt((b * b) - (4 * a * c))) / (2 * a));
            if (t2 < t1) {
                // just swap
                double tmp = t1;
                t1 = t2;
                t2 = tmp;
            }
            List<Double> r = new ArrayList<Double>();
            if (f1.dom().contains(t1)) {
                r.add(t1);
            }
            if (f1.dom().contains(t2) && !r.contains(t2)) {
                r.add(t2);
            }
            return r;
        }
    }

    public static List<IClaim> createClaimRepresentationOfSatisfaction(IPsop signal, String claimName) {
        List<IClaim> claims = new ArrayList<>();
        Resource r = new Resource(100, false);
        r.setAttribute("name", "STL dummy");

        double t0 = PsopHelper.getDomainLowerBound(signal).doubleValue();
        boolean sat = PsopHelper.getStartValue(signal).doubleValue() >= 0d;

        for (ShapeSegment seg: PsopHelper.createMonotonicSegmentation(signal)) {
            switch (seg.getShape()) {
                case CONSTANT:
                    double startT = signal.getFragments().get(seg.getBeginFragment()).dom().lb().doubleValue();
                    double value = signal.getFragments().get(seg.getBeginFragment()).getC().doubleValue();
                    if ((sat && value < 0d) || (!sat && value >= 0d)) {
                        createClaim(claims, claimName, r, t0, startT, sat);
                        t0 = startT;
                        sat = !sat;
                    }
                    break;
                case INCREASING:
                    if (!sat) {
                        // check whether we get sat
                        for (int i = seg.getBeginFragment(); i <= seg.getEndFragment(); i++) {
                            IPsopFragment frag = signal.getFragments().get(i);
                            double v1 = PsopHelper.valueAt(frag, frag.dom().ub()).doubleValue();
                            if (v1 >= 0d) {
                                double t1 = PsopHelper.argZeroValue(frag).doubleValue();
                                createClaim(claims, claimName, r, t0, t1, sat);
                                t0 = t1;
                                sat = !sat;
                                break;
                            }
                        }
                    }
                    break;
                case DECREASING:
                    if (sat) {
                        // check whether we get unsat
                        for (int i = seg.getBeginFragment(); i <= seg.getEndFragment(); i++) {
                            IPsopFragment frag = signal.getFragments().get(i);
                            double v1 = PsopHelper.valueAt(frag, frag.dom().ub()).doubleValue();
                            if (v1 < 0d) {
                                double t1 = PsopHelper.argZeroValue(frag).doubleValue();
                                createClaim(claims, claimName, r, t0, t1, sat);
                                t0 = t1;
                                sat = !sat;
                                break;
                            }
                        }
                    }
                    break;
                default:
                    throw new IllegalStateException();
            }
        }
        // add the last claim
        double t1 = PsopHelper.getDomainUpperBound(signal).doubleValue();
        createClaim(claims, claimName, r, t0, t1, sat);

        return claims;
    }

    private static Number argZeroValue(IPsopFragment f) {
        if (f.getShape() == Shape.CONSTANT) {
            if (f.getC().doubleValue() == 0d) {
                return f.dom().lb();
            } else {
                throw new IllegalArgumentException("not an increasing or decreasing shape");
            }
        }

        if (f.getShape() != Shape.INCREASING && f.getShape() != Shape.DECREASING) {
            throw new IllegalArgumentException("not an increasing or decreasing shape");
        }

        double lb = f.dom().lb().doubleValue();
        double a = f.getA().doubleValue();
        double b = f.getB().doubleValue();
        double c = f.getC().doubleValue();

        if (f.getOrder() == 1) {
            // bx + c = 0 => bx = -c => x = -c/b
            return lb + (-f.getC().doubleValue()) / f.getB().doubleValue();
        } else {
            // ax^2 + bx + c = 0 => x = -b +/- sqrt(b^2 - 4ac) / 2a
            double d = Math.sqrt((b * b) - (4 * a * c));
            double x1 = lb + (-b + d) / (2 * a);
            double x2 = lb + (-b - d) / (2 * a);
            if (f.dom().contains(x1)) {
                return x1;
            } else if (f.dom().contains(x2)) {
                return x2;
            } else {
                return Double.NaN;
            }
        }
    }

    private static void createClaim(List<IClaim> claims, String name, Resource r, double t0, double t1, boolean sat) {
        Claim c = new Claim(t0, t1, r, 1);
        c.setAttribute("phi", name);
        c.setAttribute("type", "STL");
        c.setAttribute("color", sat ? "dark_green" : "dark_red");
        c.setAttribute("sat", Boolean.toString(sat));
        claims.add(c);
    }

    public static final class MinMaxResult {
        private double min = Double.POSITIVE_INFINITY;

        private double max = Double.NEGATIVE_INFINITY;

        public MinMaxResult() {
        }

        private MinMaxResult(double x) {
            addToMin(x);
            addToMax(x);
        }

        private MinMaxResult(double x1, double x2) {
            addToMin(x1);
            addToMin(x2);
            addToMax(x1);
            addToMax(x2);
        }

        private MinMaxResult(double x1, double x2, double x3) {
            addToMin(x1);
            addToMin(x2);
            addToMin(x3);
            addToMax(x1);
            addToMax(x2);
            addToMax(x3);
        }

        private void addToMin(double x) {
            min = Math.min(min, x);
        }

        private void addToMax(double x) {
            max = Math.max(max, x);
        }

        public void combineWith(MinMaxResult r) {
            addToMin(r.min);
            addToMax(r.max);
        }

        public double getMin() {
            return min;
        }

        public double getMax() {
            return max;
        }
    }

    /**
     * Immutable. A block of consecutive indices of {@link PsopFragment} instances which, when considered as a whole,
     * have an INCREASING, DECREASING or CONSTANT shape. Begin and end indices are inclusive.
     */
    public static final class ShapeSegment {
        private final int beginFragment;

        private final int endFragment;

        private final Shape shape;

        public ShapeSegment(int start, int end, Shape shape) {
            this.beginFragment = start;
            this.endFragment = end;
            this.shape = shape;
            if (shape == Shape.PARABOLA_CAP || shape == Shape.PARABOLA_CUP) {
                throw new IllegalArgumentException("only monotonic shapes can be used");
            }
        }

        public int getBeginFragment() {
            return beginFragment;
        }

        public int getEndFragment() {
            return endFragment;
        }

        public Shape getShape() {
            return shape;
        }

        @Override
        public String toString() {
            return "ShapeSegment[" + beginFragment + "->" + endFragment + " : " + shape + "]";
        }
    }
}
