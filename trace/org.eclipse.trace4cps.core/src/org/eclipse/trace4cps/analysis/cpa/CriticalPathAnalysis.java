/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.cpa;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.trace4cps.analysis.constraintgraph.ConstraintConfig;
import org.eclipse.trace4cps.analysis.constraintgraph.impl.Constraint;
import org.eclipse.trace4cps.analysis.constraintgraph.impl.ConstraintGraph;
import org.eclipse.trace4cps.analysis.constraintgraph.impl.ConstraintGraphNode;
import org.eclipse.trace4cps.analysis.constraintgraph.impl.EventNode;
import org.eclipse.trace4cps.core.IAttributeAware;
import org.eclipse.trace4cps.core.IAttributeFilter;
import org.eclipse.trace4cps.core.IDependency;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.impl.Dependency;

/**
 * This class implements critical-path analysis based on the constraint graph representation of the {@link IEvent}
 * instances of an {@link ITrace}. References:
 * <ul>
 * <li>M. Hendriks and F. W. Vaandrager. Reconstructing critical paths from execution traces. In International
 * Conference on Embedded and Ubiquitous Computing (EUC 2012). IEEE Computer Society Press, 2012.</li>
 * <li>M. Hendriks, J. Verriet, T. Basten, B. Theelen, M. Brasse, L. Somers. Analyzing Execution Traces - Critical-Path
 * Analysis and Distance Analysis. International Journal on Software Tools for Technology Transfer, STTT. 19(4):487-510,
 * August 2017.</li>
 * </ul>
 */
public final class CriticalPathAnalysis {
    public static final String CPA_ATT_TYPE = "type";

    public static final String CPA_ATT_TYPE_CG = "cg";

    public static final String CPA_ATT_CRITICAL = "critical"; // boolean value

    public static final String CPA_ATT_CRITICAL_TRUE = "true";

    public static final String CPA_ATT_CRITICAL_FALSE = "false";

    private CriticalPathAnalysis() {
    }

    public static IAttributeFilter getCriticalDependencyFilter() {
        return new IAttributeFilter() {
            @Override
            public boolean include(IAttributeAware a) {
                return CPA_ATT_CRITICAL_TRUE.equals(a.getAttributeValue(CPA_ATT_CRITICAL));
            }
        };
    }

    /**
     * Default configuration for constraint graph: ordering heuristic with {@code epsilon = 0}.
     *
     * @param trace the {@link ITrace} whose events to analyze for a critical-path
     * @return the result
     * @throws PositiveCycleException if there are cyclic dependencies that make that critical-path analysis is not
     *     possible
     */
    public static CpaResult run(ITrace trace) throws PositiveCycleException {
        ConstraintConfig config = ConstraintConfig.getDefault();
        config.setAddSourceAndSink(true);
        config.setApplyOrderingHeuristic(0d);
        return run(trace, config);
    }

    /**
     * Manual configuration for the constraint graph of the critical-path analysis.
     *
     * @param trace the {@link ITrace} whose events to analyze for a critical-path
     * @param config the manual configuration for the analysis
     * @return the result
     * @throws PositiveCycleException if there are cyclic dependencies that make that critical-path analysis is not
     *     possible
     */
    public static CpaResult run(ITrace trace, ConstraintConfig config) throws PositiveCycleException {
        if (!config.isAddSourceAndSink()) {
            config.setAddSourceAndSink(true);
        }
        return applyBellmanFord(trace, config);
    }

    private static CpaResult applyBellmanFord(ITrace trace, ConstraintConfig config) throws PositiveCycleException {
        if (config.isUseDependencies() && config.isApplyOrderingHeuristic()) {
            throw new IllegalArgumentException("cannot use dependencies and ordering heuristic");
        }
        ConstraintGraph cg = new ConstraintGraph(trace, config);
        ConstraintGraphNode cgSrc = cg.getNodes().get(0);
        ConstraintGraphNode cgSnk = cg.getNodes().get(cg.getNodes().size() - 1);
        double[] dist = new double[cg.size()];
        @SuppressWarnings("unchecked")
        Set<Constraint>[] pre = new HashSet[cg.size()]; // FIXME: list of constraints
        initializeBFdata(cg, dist, pre, cgSrc);
        // relax edges at most |V| times
        for (int i = 0; i < cg.size(); i++) {
            // iterate over all edges: |E|
            boolean change = false;
            for (int j = 0; j < cg.size(); j++) {
                ConstraintGraphNode src = cg.getNodes().get(j);
                for (Constraint edge: src.constraints()) {
                    ConstraintGraphNode dst = edge.getDst();
                    if (dist[src.getId()] + edge.weight() > dist[dst.getId()]) {
                        dist[dst.getId()] = dist[src.getId()] + edge.weight();
                        pre[dst.getId()].clear();
                        pre[dst.getId()].add(edge);
                        change = true;
                    } else if (dist[src.getId()] + edge.weight() == dist[dst.getId()]) {
                        pre[dst.getId()].add(edge);
                        change = true;
                    }
                }
            }
            if (!change) {
                break;
            }
        }
        // Process BF result
        assertNoPositiveCycles(cg, dist);
        return computeResult(cgSrc, cgSnk, pre, cg);
    }

    private static void initializeBFdata(ConstraintGraph cg, double[] dist, Set<Constraint>[] pre,
            ConstraintGraphNode startNode)
    {
        for (int i = 0; i < cg.size(); i++) {
            pre[i] = new HashSet<Constraint>();
        }
        for (int i = 0; i < cg.size(); i++) {
            dist[i] = Double.NEGATIVE_INFINITY;
        }
        dist[startNode.getId()] = 0;
    }

    private static void assertNoPositiveCycles(ConstraintGraph cg, double[] dist) throws PositiveCycleException {
        for (ConstraintGraphNode ei: cg.getNodes()) {
            for (Constraint edge: ei.constraints()) {
                if (dist[ei.getId()] + edge.weight() > dist[edge.getDst().getId()]) {
                    throw new PositiveCycleException("graph contains a positive-weight cycle: " + ei);
                }
            }
        }
    }

    private static CpaResult computeResult(ConstraintGraphNode srcNode, ConstraintGraphNode snkNode,
            Collection<Constraint>[] pre, ConstraintGraph cg)
    {
        List<IDependency> criticalDeps = new ArrayList<>();
        List<ConstraintGraphNode> waiting = new ArrayList<>();
        waiting.add(cg.getNodes().get(cg.size() - 1)); // add the sink
        Set<ConstraintGraphNode> passed = new HashSet<>();
        while (!waiting.isEmpty()) {
            ConstraintGraphNode dst = waiting.remove(waiting.size() - 1);
            for (Constraint constraint: pre[dst.getId()]) {
                ConstraintGraphNode src = constraint.getSrc();
                if (!passed.contains(src)) {
                    waiting.add(src);
                }
                if (src instanceof EventNode && dst instanceof EventNode) {
                    IEvent srcEvent = ((EventNode)src).getEvent();
                    IEvent dstEvent = ((EventNode)dst).getEvent();
                    IDependency dep = new Dependency(srcEvent, dstEvent);
                    dep.setAttribute(CPA_ATT_CRITICAL, CPA_ATT_CRITICAL_TRUE);
                    criticalDeps.add(dep);
                }
            }
            passed.add(dst);
        }
        return new CpaResult(criticalDeps);
    }
}
