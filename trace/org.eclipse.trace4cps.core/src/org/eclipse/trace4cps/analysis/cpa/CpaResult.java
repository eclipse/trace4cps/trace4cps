/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.cpa;

import java.util.List;

import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IClaimEvent;
import org.eclipse.trace4cps.core.IDependency;

/**
 * The result of critical-path analysis.
 */
public final class CpaResult {
    private final List<IDependency> criticalDeps;

    public CpaResult(List<IDependency> criticalDeps) {
        this.criticalDeps = criticalDeps;
    }

    public List<IDependency> getCriticalDeps() {
        return criticalDeps;
    }

    public static boolean isNonAppDependency(IDependency dep, DependencyProvider p) {
        if (p == null || p.isApplicationDependency(dep.getSrc(), dep.getDst())) {
            return false;
        }
        // p != null && !p.isApplicationDependency
        // also ensure that this is not an intra-claim dependency
        if (dep.getSrc() instanceof IClaimEvent && dep.getDst() instanceof IClaimEvent) {
            IClaim src = ((IClaimEvent)dep.getSrc()).getClaim();
            IClaim dst = ((IClaimEvent)dep.getDst()).getClaim();
            if (src == dst) {
                return false;
            }
        }
        return true;
    }
}
