/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.analysis.cpa;

import org.eclipse.trace4cps.core.IEvent;

/**
 * This type can be used to refine the critical-path analysis by adding information on the logical dependencies between
 * claims that are due to the application.
 */
public interface DependencyProvider {
    /**
     * Indicates whether a dependency between two events is a logical/data/application dependency.
     *
     * @param src the source event
     * @param dst the destination event
     * @return whether a dependency from src to dst is due to the application
     */
    boolean isApplicationDependency(IEvent src, IEvent dst);
}
