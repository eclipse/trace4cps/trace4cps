/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * The central type of the TRACE data model.
 *
 *
 * <p>
 * <b>Caveat:</b> All computations in this library involving {@link Number} instances use {@link Number#doubleValue()}
 * and double arithmetic. This may cause rounding effects.
 * </p>
 */
public interface ITrace extends IAttributeAware {
    /**
     * @return the {@link TimeUnit} of the time stamps in this trace
     */
    TimeUnit getTimeUnit();

    /**
     * Specifies the time offset for the time stamps in this trace. All methods in the API that return time stamps,
     * return time stamps relative to this offset. Note that time stamp getters typically return a {@link Number}
     * instance, thereby allowing an arbitrary precision.
     *
     * @return the time offset as number of milliseconds since the epoch
     */
    Number getTimeOffset();

    /**
     * Gets the {@link IClaim} instances, ordered ascending w.r.t. their start timestamp.
     *
     * @return the claims
     */
    List<IClaim> getClaims();

    /**
     * Gets the {@link IResource} instances used by the {@link IClaim} instances.
     *
     * @return the {@link IResource} instances used by the {@link IClaim} instances
     */
    List<IResource> getResources();

    /**
     * Gets the {@link IEvent} instances, ordered ascending by their timestamp. This list includes the events that
     * belong to start and end of {@link IClaim} instances, i.e., the events obtained via {@link IClaim#getStartEvent()}
     * and {@link IClaim#getEndEvent()}. These "derived" events of the {@link IClaimEvent} type delegate the attribute
     * manipulation to their {@link IClaim} instance.
     *
     * @return the events
     */
    List<IEvent> getEvents();

    /**
     * Gets the {@link IDependency} instances.
     *
     * @return the dependencies
     */
    List<IDependency> getDependencies();

    /**
     * Gets the continuous signals.
     *
     * @return the signals
     */
    List<IPsop> getSignals();
}
