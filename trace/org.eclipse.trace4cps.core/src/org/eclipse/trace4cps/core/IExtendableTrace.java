/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core;

import java.util.Collection;

public interface IExtendableTrace {
    void addSignal(IPsop p);

    void addDependencies(Collection<IDependency> dependencies);

    void addEvents(Collection<IEvent> events);

    void addClaims(Collection<IClaim> claims);

    boolean hasExtension(TracePart part);

    void clearExtension(TracePart part);
}
