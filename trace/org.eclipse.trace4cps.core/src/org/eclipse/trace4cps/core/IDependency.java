/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core;

/**
 * A dependency between two {@link IEvent} instances. The semantics of a dependency is user-defined.
 */
public interface IDependency extends IAttributeAware {
    /**
     * @return the source event
     */
    IEvent getSrc();

    /**
     * @return the destination event
     */
    IEvent getDst();
}
