/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core;

import java.util.List;

import org.eclipse.trace4cps.core.impl.Psop;

/**
 * One of the central types of the TRACE data model.
 *
 *
 * <p>
 * A piecewise second-order polynomial, consisting of consecutive {@link IPsopFragment} instances.
 * </p>
 */
public interface IPsop extends IAttributeAware {
    /**
     * The fragments are ordered by their time-domain, are consecutive, and have no overlap.
     *
     * @return the fragments that define this {@link Psop}
     */
    List<IPsopFragment> getFragments();
}
