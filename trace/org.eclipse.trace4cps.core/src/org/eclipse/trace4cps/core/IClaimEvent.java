/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core;

/**
 * An {@link IClaimEvent} is a special event that is associated with an {@link IClaim} instance. Each {@link IClaim}
 * instance has exactly two {@link IClaimEvent} instances: one for its start and one for its end. The manipulation of
 * the attributes is delegated to the parent {@link IClaim} instance. That thus means that the {@link IClaim} and its
 * two {@link IClaimEvent} instances have exactly the same set of attributes.
 */
public interface IClaimEvent extends IEvent {
    /**
     * @return the type of this claim event
     */
    ClaimEventType getType();

    /**
     * @return the {@link IClaim} instance with which this event is associated
     */
    IClaim getClaim();
}
