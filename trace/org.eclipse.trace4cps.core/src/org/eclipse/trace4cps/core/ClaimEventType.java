/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core;

/**
 * The possible types of an {@link IClaimEvent} that is associated with a {@link IClaim} instance.
 */
public enum ClaimEventType {
    /**
     * The {@link IClaimEvent} models the start of the associated {@link IClaim}
     */
    START,
    /**
     * The {@link IClaimEvent} models the end of the associated {@link IClaim}
     */
    END
}
