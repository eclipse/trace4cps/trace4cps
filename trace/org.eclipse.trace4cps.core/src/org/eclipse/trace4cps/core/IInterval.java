/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core;

/**
 * This type represents a continuous interval (a subset of the real numbers) from {@link #lb()} to {@link #ub()}.
 * Whether the end values are included in the interval is given by {@link #isOpenLb()} and {@link #isOpenUb()}
 * respectively.
 */
public interface IInterval {
    /**
     * @return the lower bound of the interval
     */
    Number lb();

    /**
     * @return the upper bound of the interval
     */
    Number ub();

    /**
     * @return whether the lower bound is open
     */
    boolean isOpenLb();

    /**
     * @return whether the upper bound is open
     */
    boolean isOpenUb();

    /**
     * @return whether the interval is empty
     */
    boolean isEmpty();

    /**
     * @param v a given value
     * @return whether the given value is an element of the interval
     */
    boolean contains(Number v);
}
