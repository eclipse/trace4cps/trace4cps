/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.io;

/**
 * This type enumerates the possible dependencies between claims and events that can be specified.
 */
public enum DependencyType {
    CLAIM_START_CLAIM_START(0),
    CLAIM_START_CLAIM_END(1),
    CLAIM_END_CLAIM_START(2),
    CLAIM_END_CLAIM_END(3),
    EVENT_EVENT(4),
    CLAIM_START_EVENT(5),
    CLAIM_END_EVENT(6),
    EVENT_CLAIM_START(7),
    EVENT_CLAIM_END(8);

    private final int value;

    private DependencyType(int v) {
        value = v;
    }

    public static DependencyType typeFor(int v) {
        for (DependencyType t: DependencyType.values()) {
            if (t.value == v) {
                return t;
            }
        }
        return null;
    }

    public int getValue() {
        return value;
    }
}
