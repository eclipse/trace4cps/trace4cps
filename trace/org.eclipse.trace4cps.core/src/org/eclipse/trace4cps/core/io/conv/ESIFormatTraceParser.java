/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.io.conv;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.trace4cps.core.impl.Claim;
import org.eclipse.trace4cps.core.impl.Dependency;
import org.eclipse.trace4cps.core.impl.Resource;
import org.eclipse.trace4cps.core.impl.Trace;

public class ESIFormatTraceParser extends ESIFormatParser {

    // All the tags that are accepted in this parser
    protected static final String FORMAT_VERSION = "V0.1";

    protected static final String TRACEFORMATVERSION_TAG = "TraceFormatVersion"; // deprecated

    protected static final String TRACEVERSION_TAG = "TraceVersion";

    protected static final String TRACENAME_TAG = "TraceName";

    protected static final String TRACEABSOLUTESTARTTIME_TAG = "TraceAbsoluteStartTime"; // deprecated

    protected static final String TRACESTARTTIME_TAG = "TraceStartTime";

    protected static final String RESOURCE_TAG = "R";

    protected static final String CLAIM_TAG = "C";

    protected static final String DEPENDENCY_TAG = "D";

    protected HashMap<Integer, Resource> idResourceMap = new HashMap<>();

    protected HashMap<Integer, Claim> idClaimMap = new HashMap<>();

    private Set<Resource> resourceFullInstances = new HashSet<>();

    private Map<Resource, Double> resourceOffsets = new HashMap<>();

    private Resource getResource(Integer aId) {
        return idResourceMap.get(aId);
    }

    private void addResource(Resource r, int id) {
        idResourceMap.put(id, r);
    }

    protected boolean addClaim(Trace aTrace, Integer aId, Claim aClaim) {
        boolean result = false;
        if (!idClaimMap.containsKey(aId)) {
            aTrace.add(aClaim);
            idClaimMap.put(aId, aClaim);
            result = true;
        }
        return result;
    }

    protected Claim getClaim(Integer aId) {
        return idClaimMap.get(aId);
    }

    protected void addDependency(Trace aTrace, Dependency aDependency) {
        aTrace.add(aDependency);
    }

    /**
     * Parses an original trace file (which consists of a configuration file and a data file)
     *
     * @param traceFile The trace file to parse
     * @param aConfig The configuration to apply while parsing
     * @return The parsed trace
     * @throws IOException
     * @throws ParserException
     */
    public Trace parseTraceV0(File traceFile, Configuration aConfig) throws IOException, ParserException {
        Trace trace = new Trace();
        trace.setTimeUnit(aConfig.getTimeScaleUnit());
        trace.setOffset(0L);
        try (FileInputStream in = new FileInputStream(traceFile)) {
            parse(traceFile.getAbsolutePath(), in, aConfig, trace);
        }
        return trace;
    }

    private double getTimeMultiplier(Configuration aConfig) {
        return Math.pow(10.0, aConfig.getTimeScaleShift());
    }

    private void parse(String src, FileInputStream in, Configuration aConfig, Trace aTrace)
            throws IOException, ParserException
    {
        final double PARSE_TIME_MULTIPLIER = getTimeMultiplier(aConfig);

        int n_TRACEVERSION = 0;
        int n_CONFIGURATIONID = 0;
        int n_TRACENAME = 0;
        int n_TRACESTARTTIME = 0;

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
            String line = null;
            int linenr = 0;

            while ((n_TRACEVERSION == 0) && ((line = reader.readLine()) != null)) {
                linenr++;
                String[] tok = line.split(SEPARATOR, -1);
                if (tok.length >= 2) {
                    switch (tok[0]) {
                        case TRACEFORMATVERSION_TAG:
                            System.err.println(
                                    TRACEFORMATVERSION_TAG + " is deprecated; use " + TRACEVERSION_TAG + " instead");
                        case TRACEVERSION_TAG:
                            // aTrace.setTraceVersion(tok[1]);
                            if (!tok[1].equalsIgnoreCase(FORMAT_VERSION)) {
                                throw new ParserException("Trace version not supported " + tok[1], src, linenr);
                            }
                            n_TRACEVERSION++;
                            break;
                        default:
                            throw new ParserException("Trace version missing as first element", src, linenr);
                    }
                }
            }

            while ((line = reader.readLine()) != null) {
                linenr++;
                String[] tok = line.split(SEPARATOR, -1);
                if (tok.length >= 2) {
                    int index;
                    int contextId = 0;
                    Context context = null;

                    switch (tok[0]) {
                        case CONFIGURATIONID_TAG:
                            if (n_CONFIGURATIONID > 0) {
                                throw new ParserException("ConfigurationID allready declared", src, linenr);
                            }
                            // aTrace.setConfigurationID(tok[1]);
                            if (!aConfig.getConfigurationID().equalsIgnoreCase(tok[1])) {
                                throw new ParserException(
                                        "The Configuration required is notequal to actual Configuration", src, linenr);
                            }
                            n_CONFIGURATIONID++;
                            break;

                        case CONFIGURATION_SOURCE:
                            break;
                        case TRACENAME_TAG:
                            if (n_TRACENAME > 0) {
                                throw new ParserException("Trace name already declared", src, linenr);
                            }
                            aTrace.setAttribute(TRACENAME_TAG, tok[1]);
                            n_TRACENAME++;
                            break;

                        case TRACEABSOLUTESTARTTIME_TAG:
                            System.err.println(TRACEABSOLUTESTARTTIME_TAG + " is deprecated; use " + TRACESTARTTIME_TAG
                                    + " instead");
                        case TRACESTARTTIME_TAG:
                            if (n_TRACESTARTTIME > 0) {
                                throw new ParserException("TraceStartTime already declared", src, linenr);
                            }
//                        aTrace.setTraceStartTime(tok[1]);
                            n_TRACESTARTTIME++;
                            break;

                        case RESOURCE_TAG:
                            index = 1;
                            int rid;
                            try {
                                // Resource ID
                                rid = Integer.parseInt(tok[index]);
                            } catch (NumberFormatException e) {
                                throw new ParserException("ResourceID has illegal value " + tok[index], src, linenr);
                            }
                            if (idResourceMap.containsKey(rid)) {
                                throw new ParserException("Resource(ID) already exists " + tok[index], src, linenr);
                            }

                            // Resource Capacity
                            if (tok.length <= ++index) {
                                throw new ParserException("Resource Capacity expected for resource id " + rid, src,
                                        linenr);
                            }
                            double capacity;
                            try {
                                capacity = Double.parseDouble(tok[index]);
                            } catch (NumberFormatException e) {
                                throw new ParserException("Resource Capacity has illegal value " + tok[index], src,
                                        linenr);
                            }

                            // Resource Unit
                            if (tok.length <= ++index) {
                                throw new ParserException("Resource Unit expected for resource id " + rid, src, linenr);
                            }
                            String unit = tok[index];

                            // Resource Claim Type
                            if (tok.length <= ++index) {
                                throw new ParserException("Claim type expected for resource id " + rid, src, linenr);
                            }
                            Resource resource = null;
                            try {
                                int ict = Integer.parseInt(tok[index]);
                                ClaimType claimtype = ClaimType.get(ict);
                                if (claimtype == null) {
                                    throw new ParserException("Claim type has unknown value " + tok[index], src,
                                            linenr);
                                }
                                switch (claimtype) {
                                    case FULL:
                                        resource = new Resource(1, false);
                                        resourceFullInstances.add(resource);
                                        break;
                                    case AMOUNT:
                                        resource = new Resource(capacity, false);
                                        break;
                                    case AMOUNT_OFFSET:
                                        resource = new Resource(capacity, true);
                                        // Resource Offset
                                        if (tok.length <= ++index) {
                                            throw new ParserException("ResourceOffset expected for resource id " + rid,
                                                    src, linenr);
                                        }
                                        try {
                                            double offset = Double.parseDouble(tok[index]);
                                            resourceOffsets.put(resource, offset);
                                        } catch (NumberFormatException e) {
                                            throw new ParserException("ResourceOffset has illegal value " + tok[index],
                                                    src, linenr);
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            } catch (NumberFormatException e) {
                                throw new ParserException("ClaimType has illegal value " + tok[index], src, linenr);
                            }
                            resource.setAttribute("unit", unit);

                            // Resource Context
                            if (tok.length <= ++index) {
                                throw new ParserException("Context expected for resource id " + rid, src, linenr);
                            }
                            try {
                                contextId = Integer.parseInt(tok[index]);
                            } catch (NumberFormatException e) {
                                throw new ParserException("Context has illegal value " + tok[index], src, linenr);
                            }
                            context = aConfig.getContext(contextId);
                            if (context == null) {
                                throw new ParserException(
                                        "Resource context not defined in configuration: " + tok[index], src, linenr);
                            }
//                        resource.setContext(context);
                            if (tok.length <= index + context.getAttributes().size()) {
                                throw new ParserException(
                                        "Invalid number of context arguments: " + context.getContextName(), src,
                                        linenr);
                            }
//                        resource.newAttValMap(context.getAttributes().size());
                            for (Attribute att: context.getAttributes()) {
                                resource.setAttribute(att.getAttributeName(), tok[++index]);
                            }
                            addResource(resource, rid);
                            break;

                        case CLAIM_TAG:
                            index = 1;
                            // Claim id
                            int cid;
                            try {
                                cid = Integer.parseInt(tok[index]);
                            } catch (NumberFormatException e) {
                                throw new ParserException("ClaimID has illegal value " + tok[index], src, linenr);
                            }
                            if (idClaimMap.containsKey(cid)) {
                                throw new ParserException("Claim(ID) already exists " + tok[index], src, linenr);
                            }

                            // Claim resource
                            if (tok.length <= ++index) {
                                throw new ParserException("Resource expected for claim id " + cid, src, linenr);
                            }
                            Claim claim = null;
                            try {
                                rid = Integer.parseInt(tok[index]);
                            } catch (NumberFormatException e) {
                                throw new ParserException("ResourceID has illegal value " + tok[index], src, linenr);
                            }
                            resource = getResource(rid);

                            if (resource == null) {
                                throw new ParserException("Resource not known " + tok[index], src, linenr);
                            }

                            double amount = Double.NaN;
                            double offset = Double.NaN;

                            if (resourceFullInstances.contains(resource)) {
                                amount = 1d;
                            } else if (!resourceOffsets.keySet().contains(resource)) {
                                // Claim amount
                                if (tok.length <= ++index) {
                                    throw new ParserException("Amount expected for claim id " + cid, src, linenr);
                                }
                                try {
                                    amount = Double.parseDouble(tok[index]);
                                } catch (NumberFormatException e) {
                                    throw new ParserException("Amount has illegal value " + tok[index], src, linenr);
                                }
                            } else {
                                // Claim amount
                                if (tok.length <= ++index) {
                                    throw new ParserException("Amount expected for claim id " + cid, src, linenr);
                                }
                                try {
                                    amount = Double.parseDouble(tok[index]);
                                } catch (NumberFormatException e) {
                                    throw new ParserException("Aount has illegal value " + tok[index], src, linenr);
                                }
                                // Claim offset
                                if (tok.length <= ++index) {
                                    throw new ParserException("Offset expected for claim id " + cid, src, linenr);
                                }
                                try {
                                    offset = Double.parseDouble(tok[index]) + resourceOffsets.get(resource);
                                } catch (NumberFormatException e) {
                                    throw new ParserException("Offset has illegal value " + tok[index], src, linenr);
                                }
                            }

                            // Claim start time
                            double starttime;
                            if (tok.length <= ++index) {
                                throw new ParserException("Start time expected for claim id " + cid, src, linenr);
                            }
                            try {
                                starttime = PARSE_TIME_MULTIPLIER * Double.parseDouble(tok[index]);
                            } catch (NumberFormatException e) {
                                throw new ParserException("Start time has illegal value " + tok[index], src, linenr);
                            }

                            // Claim stop time
                            double stoptime;
                            if (tok.length <= ++index) {
                                throw new ParserException("Stop time expected for claim id " + cid, src, linenr);
                            }
                            try {
                                stoptime = PARSE_TIME_MULTIPLIER * Double.parseDouble(tok[index]);
                                // Alternative to avoid noise in LSBs at High values
                                // MathContext mathcontext = new MathContext(15, RoundingMode.HALF_UP);
                                // BigDecimal multiplicand = new BigDecimal( 1.0E12, mathcontext );
                                // BigDecimal bb = new BigDecimal( Double.parseDouble(tok[index]), mathcontext);
                                // stoptime = (bb.multiply(multiplicand)).longValue();
                            } catch (NumberFormatException e) {
                                throw new ParserException("Stop time has illegal value " + tok[index], src, linenr);
                            }
                            if (starttime > stoptime) {
                                throw new ParserException("Stop time is before start time for claim id " + cid, src,
                                        linenr);
                            }
                            if (Double.isNaN(offset)) {
                                claim = new Claim(starttime, stoptime, resource, amount);
                            } else {
                                claim = new Claim(starttime, stoptime, resource, offset, amount);
                            }

                            // Limitations Type
                            if (tok.length <= ++index) {
                                throw new ParserException("Limitations type expected for claim id " + cid, src, linenr);
                            }
                            try {
                                int ilt = Integer.parseInt(tok[index]);
                                LimitationsType lt = LimitationsType.get(ilt);
                                if (lt == null) {
                                    throw new ParserException("Period limitations type has unknown value " + tok[index],
                                            src, linenr);
                                }
                                if (ilt != LimitationsType.NONE_VALUE) {
//                                Limitations lim = factory.createLimitations();
                                    if ((ilt & LimitationsType.MIN_START_VALUE) != 0) {
                                        if (tok.length <= ++index) {
                                            throw new ParserException("MinStarttime expected for claim id " + cid, src,
                                                    linenr);
                                        }
//                                    try {
//                                        lim.setMinStartTime(Math
//                                            .round(PARSE_TIME_MULTIPLIER * Double.parseDouble(tok[index])));
//                                    } catch (NumberFormatException e) {
//                                        throw new ParserException(
//                                                "Claim limitation MinStarttime has illegal value "
//                                                        + tok[index],
//                                                src, linenr);
//                                    }
                                    }
                                    if ((ilt & LimitationsType.MAX_STOP_VALUE) != 0) {
                                        if (tok.length <= ++index) {
                                            throw new ParserException("MaxStoptime expected for Claim id " + cid, src,
                                                    linenr);
                                        }
//                                    try {
//                                        lim.setMaxStopTime(Math
//                                            .round(PARSE_TIME_MULTIPLIER * Double.parseDouble(tok[index])));
//                                    } catch (NumberFormatException e) {
//                                        throw new ParserException(
//                                                "Claim limitation MaxStoptime has illegal value "
//                                                        + tok[index],
//                                                src, linenr);
//                                    }
//                                    if (lim.isSetMinStartTime()) {
//                                        if (lim.getMinStartTime() > lim.getMaxStopTime()) {
//                                            throw new ParserException(
//                                                    "MaxStoptime is before MinStarttime for claim id " + cid,
//                                                    src, linenr);
//                                        }
//                                    }
                                    }
                                    if ((ilt & LimitationsType.MAX_DURATION_VALUE) != 0) {
                                        if (tok.length <= ++index) {
                                            throw new ParserException("MaxDuration expected for claim id " + cid, src,
                                                    linenr);
                                        }
                                        try {
                                            long max_duration = Math
                                                    .round(PARSE_TIME_MULTIPLIER * Double.parseDouble(tok[index]));
                                            if (max_duration < 0) {
                                                throw new ParserException(
                                                        "Negative value for claim limitation MaxDuration is not allowed for claim id "
                                                                + cid,
                                                        src, linenr);
                                            }
//                                        lim.setMaxDuration(max_duration);
                                        } catch (NumberFormatException e) {
                                            throw new ParserException(
                                                    "Claim limitation MaxDuration has illegal value " + tok[index], src,
                                                    linenr);
                                        }
                                    }
//                                claim.setLimitations(lim);
                                }
                            } catch (NumberFormatException e) {
                                throw new ParserException("Limitations type has illegal value " + tok[index], src,
                                        linenr);
                            }

                            // Claim context
                            if (tok.length <= ++index) {
                                throw new ParserException("Context expected for claim id " + cid, src, linenr);
                            }
                            try {
                                contextId = Integer.parseInt(tok[index]);
                            } catch (NumberFormatException e) {
                                throw new ParserException("Context has illegal value " + tok[index], src, linenr);
                            }
                            context = aConfig.getContext(contextId);
                            if (context == null) {
                                throw new ParserException("Claim context not defined in configuration: " + tok[index],
                                        src, linenr);
                            }
//                        claim.setContext(context);
                            if (tok.length <= index + context.getAttributes().size()) {
                                throw new ParserException(
                                        "Invalid number of context arguments: " + context.getContextName(), src,
                                        linenr);
                            }
                            for (Attribute att: context.getAttributes()) {
                                claim.setAttribute(att.getAttributeName(), tok[++index]);
                            }

                            addClaim(aTrace, cid, claim);
                            break;

                        case DEPENDENCY_TAG:
                            index = 1;

                            // Source ID
                            int sourceId = 0;
                            try {
                                sourceId = Integer.parseInt(tok[index]);
                            } catch (NumberFormatException e) {
                                throw new ParserException("Source ID has illegal value " + tok[index], src, linenr);
                            }
                            claim = getClaim(sourceId);
                            if (claim == null) {
                                throw new ParserException("Source claim not known" + sourceId, src, linenr);
                            }

                            // Sink ID
                            int sinkId = 0;
                            if (tok.length <= ++index) {
                                throw new ParserException("Sink ID expected ", src, linenr);
                            }
                            try {
                                sinkId = Integer.parseInt(tok[index]);
                            } catch (NumberFormatException e) {
                                throw new ParserException("Sink ID has illegal value " + tok[index], src, linenr);
                            }
                            claim = getClaim(sinkId);
                            if (claim == null) {
                                throw new ParserException("Sink claim not known" + sinkId, src, linenr);
                            }

                            // Dependency Type
                            if (tok.length <= ++index) {
                                throw new ParserException("Dependency type expected ", src, linenr);
                            }
                            int dependencyType;
                            try {
                                dependencyType = Integer.parseInt(tok[index]);
                            } catch (NumberFormatException e) {
                                throw new ParserException("Dependency type has illegal value " + tok[index], src,
                                        linenr);
                            }
                            DependencyType dType = DependencyType.get(dependencyType);
                            if (dType == null) {
                                throw new ParserException("Dependency type not known " + dependencyType, src, linenr);
                            }

                            // Dependency context
                            if (tok.length <= ++index) {
                                throw new ParserException("Context expected for dependency ", src, linenr);
                            }
                            try {
                                contextId = Integer.parseInt(tok[index]);
                            } catch (NumberFormatException e) {
                                throw new ParserException("Context has illegal value " + tok[index], src, linenr);
                            }
                            context = aConfig.getContext(contextId);
                            if (context == null) {
                                throw new ParserException(
                                        "Dependency context not defined in configuration: " + tok[index], src, linenr);
                            }
                            if (tok.length <= index + context.getAttributes().size()) {
                                throw new ParserException(
                                        "Invalid number of context arguments: " + context.getContextName(), src,
                                        linenr);
                            }

                            Claim srcClaim = getClaim(sourceId);
                            Claim dstClaim = getClaim(sinkId);
                            Dependency dependency = null;
                            switch (dType) {
                                case SOURCE_START2_SINK_START:
                                    dependency = new Dependency(srcClaim.getStartEvent(), dstClaim.getStartEvent());
                                    break;
                                case SOURCE_START2_SINK_STOP:
                                    dependency = new Dependency(srcClaim.getStartEvent(), dstClaim.getEndEvent());
                                    break;
                                case SOURCE_STOP2_SINK_START:
                                    dependency = new Dependency(srcClaim.getEndEvent(), dstClaim.getStartEvent());
                                    break;
                                case SOURCE_STOP2_SINK_STOP:
                                    dependency = new Dependency(srcClaim.getEndEvent(), dstClaim.getEndEvent());
                                    break;
                            }
                            for (Attribute att: context.getAttributes()) {
                                dependency.setAttribute(att.getAttributeName(), tok[++index]);
                            }

                            addDependency(aTrace, dependency);
                            break;

                        default:
                            throw new ParserException("Token not recognised " + tok[0], src, linenr);
                    }
                } else {
                    if (!tok[0].isEmpty()) {
                        throw new ParserException("Expected another token after " + tok[0], src, linenr);
                    }
                }
            }

            if (n_CONFIGURATIONID == 0) {
                throw new ParserException("Missing required " + CONFIGURATIONID_TAG, src, 0);
            }
        }
        return;
    }
}
