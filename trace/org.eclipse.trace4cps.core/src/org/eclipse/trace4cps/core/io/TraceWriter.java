/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.io;

import static org.eclipse.trace4cps.core.io.TraceReader.ATTRIBUTE_ASSIGN;
import static org.eclipse.trace4cps.core.io.TraceReader.ATTRIBUTE_ASSIGN_STR;
import static org.eclipse.trace4cps.core.io.TraceReader.ATTRIBUTE_DELIMITER;
import static org.eclipse.trace4cps.core.io.TraceReader.ATTRIBUTE_DELIMITER_STR;
import static org.eclipse.trace4cps.core.io.TraceReader.ATTRIBUTE_START;
import static org.eclipse.trace4cps.core.io.TraceReader.CLAIM_TAG;
import static org.eclipse.trace4cps.core.io.TraceReader.DEPENDENCY_TAG;
import static org.eclipse.trace4cps.core.io.TraceReader.EVENT_TAG;
import static org.eclipse.trace4cps.core.io.TraceReader.NEW_LINE;
import static org.eclipse.trace4cps.core.io.TraceReader.OFFSET_TAG;
import static org.eclipse.trace4cps.core.io.TraceReader.PSOP_FRAGMENT_TAG;
import static org.eclipse.trace4cps.core.io.TraceReader.PSOP_TAG;
import static org.eclipse.trace4cps.core.io.TraceReader.RESOURCE_TAG;
import static org.eclipse.trace4cps.core.io.TraceReader.SPACE;
import static org.eclipse.trace4cps.core.io.TraceReader.TIMEUNIT_TAG;
import static org.eclipse.trace4cps.core.io.TraceReader.TRACE_TAG;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.trace4cps.core.ClaimEventType;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IDependency;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.IPsopFragment;
import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.impl.ClaimEvent;

/**
 * Class used to write an {@link ITrace} instance to a file. Note that that the attribute keys and values are trimmed
 * with {@link String#trim()}. Furthermore, the time stamps ({@link Number} instances) are written as {@code double}
 * values, except for the {@link ITrace#getTimeOffset()}, which is written as a {@code long} value.
 */
public class TraceWriter {
    private final Map<IResource, Integer> resourceMap = new HashMap<>();

    private final Map<IClaim, Integer> claimMap = new HashMap<>();

    private final Map<IEvent, Integer> eventMap = new HashMap<>();

    private TraceWriter() {
    }

    /**
     * Writes a given {@link ITrace} to a given file.
     *
     * @param trace the trace to write
     * @param f the destination file
     * @throws IOException if an error occurs
     */
    public static void writeTrace(ITrace trace, File f) throws IOException {
        new TraceWriter().writeTraceInt(trace, f);
    }

    private void writeTraceInt(ITrace trace, File f) throws IOException {
        BufferedWriter w = new BufferedWriter(new FileWriter(f));
        try {
            writeTUandOffset(trace, w);
            writeTraceAtts(trace, w);
            writeResources(trace, w);
            writeClaims(trace, w);
            writeEvents(trace, w);
            writeDependencies(trace, w);
            writeSignals(trace, w);
        } finally {
            w.close();
        }
    }

    private void writeTUandOffset(ITrace trace, BufferedWriter w) throws IOException {
        w.write(TIMEUNIT_TAG);
        w.write(SPACE);
        w.write(trace.getTimeUnit().toString());
        w.write(NEW_LINE);
        w.write(OFFSET_TAG);
        w.write(SPACE);
        w.write(Long.toString(trace.getTimeOffset().longValue()));
        w.write(NEW_LINE);
    }

    private void writeTraceAtts(ITrace trace, BufferedWriter w) throws IOException {
        w.write(TRACE_TAG);
        w.write(SPACE);
        Map<String, String> atts = new HashMap<>();
        atts.putAll(trace.getAttributes());
        atts.remove(TraceReader.FILENAME_ATT);
        writeAttributes(atts, w);
    }

    private void writeResources(ITrace trace, BufferedWriter w) throws IOException {
        int id = 0;
        for (IResource r: trace.getResources()) {
            resourceMap.put(r, id);
            w.write(RESOURCE_TAG);
            w.write(SPACE);
            w.write(Integer.toString(id));
            w.write(SPACE);
            w.write(Double.toString(r.getCapacity().doubleValue()));
            w.write(SPACE);
            w.write(Boolean.toString(r.useOffset()));
            w.write(ATTRIBUTE_START);
            writeAttributes(r.getAttributes(), w);
            id++;
        }
    }

    private void writeClaims(ITrace trace, BufferedWriter w) throws IOException {
        int id = 0;
        for (IClaim c: trace.getClaims()) {
            claimMap.put(c, id);
            w.write(CLAIM_TAG);
            w.write(SPACE);
            w.write(Integer.toString(id));
            w.write(SPACE);
            w.write(Double.toString(c.getStartTime().doubleValue()));
            w.write(SPACE);
            w.write(Double.toString(c.getEndTime().doubleValue()));
            w.write(SPACE);
            Integer rid = resourceMap.get(c.getResource());
            if (rid == null) {
                throw new IOException("Claim " + c
                        + " refers to an IResource that is not part of the IResources of the ITrace instance");
            }
            w.write(Integer.toString(rid));
            if (c.getResource().useOffset()) {
                w.write(SPACE);
                w.write(Double.toString(c.getOffset().doubleValue()));
            }
            w.write(SPACE);
            w.write(Double.toString(c.getAmount().doubleValue()));
            w.write(ATTRIBUTE_START);
            writeAttributes(c.getAttributes(), w);
            id++;
        }
    }

    private void writeEvents(ITrace trace, BufferedWriter w) throws IOException {
        int id = 0;
        for (IEvent e: trace.getEvents()) {
            if (!(e instanceof ClaimEvent)) {
                eventMap.put(e, id);
                w.write(EVENT_TAG);
                w.write(SPACE);
                w.write(Integer.toString(id));
                w.write(SPACE);
                w.write(Double.toString(e.getTimestamp().doubleValue()));
                w.write(ATTRIBUTE_START);
                writeAttributes(e.getAttributes(), w);
                id++;
            }
        }
    }

    private void writeDependencies(ITrace trace, BufferedWriter w) throws IOException {
        int id = 0;
        for (IDependency d: trace.getDependencies()) {
            Integer[] typeAndIds = getTypeAndIds(d);
            if (typeAndIds[1] == null || typeAndIds[2] == null) {
                throw new IOException("Cannot resolve claim or event id in dependency " + d);
            }
            w.write(DEPENDENCY_TAG);
            w.write(SPACE);
            w.write(Integer.toString(id));
            w.write(SPACE);
            w.write(Integer.toString(typeAndIds[0]));
            w.write(SPACE);
            w.write(Integer.toString(typeAndIds[1]));
            w.write(SPACE);
            w.write(Integer.toString(typeAndIds[2]));
            w.write(ATTRIBUTE_START);
            writeAttributes(d.getAttributes(), w);
            id++;
        }
    }

    private Integer[] getTypeAndIds(IDependency dep) {
        Integer[] result = new Integer[3];
        IEvent src = dep.getSrc();
        IEvent dst = dep.getDst();
        boolean srcClaimEvent = src instanceof ClaimEvent;
        boolean dstClaimEvent = dst instanceof ClaimEvent;
        if (!srcClaimEvent && !dstClaimEvent) {
            result[0] = DependencyType.EVENT_EVENT.getValue();
            result[1] = eventMap.get(src);
            result[2] = eventMap.get(dst);
        } else if (!srcClaimEvent && dstClaimEvent) {
            ClaimEvent d = (ClaimEvent)dst;
            result[1] = eventMap.get(src);
            result[2] = claimMap.get(d.getClaim());
            if (d.getType() == ClaimEventType.START) {
                result[0] = DependencyType.EVENT_CLAIM_START.getValue();
            } else {
                result[0] = DependencyType.EVENT_CLAIM_END.getValue();
            }
        } else if (srcClaimEvent && !dstClaimEvent) { // srcClaimEvent && !dstClaimEvent
            ClaimEvent s = (ClaimEvent)src;
            result[1] = claimMap.get(s.getClaim());
            result[2] = eventMap.get(dst);
            if (s.getType() == ClaimEventType.START) {
                result[0] = DependencyType.CLAIM_START_EVENT.getValue();
            } else {
                result[0] = DependencyType.CLAIM_END_EVENT.getValue();
            }
        } else { // srcClaimEvent && dstClaimEvent
            ClaimEvent s = (ClaimEvent)src;
            ClaimEvent d = (ClaimEvent)dst;
            result[1] = claimMap.get(s.getClaim());
            result[2] = claimMap.get(d.getClaim());
            if (s.getType() == ClaimEventType.START && d.getType() == ClaimEventType.START) {
                result[0] = DependencyType.CLAIM_START_CLAIM_START.getValue();
            } else if (s.getType() == ClaimEventType.START && d.getType() == ClaimEventType.END) {
                result[0] = DependencyType.CLAIM_START_CLAIM_END.getValue();
            } else if (s.getType() == ClaimEventType.END && d.getType() == ClaimEventType.START) {
                result[0] = DependencyType.CLAIM_END_CLAIM_START.getValue();
            } else {
                result[0] = DependencyType.CLAIM_END_CLAIM_END.getValue();
            }
        }
        return result;
    }

    private void writeSignals(ITrace trace, BufferedWriter w) throws IOException {
        int id = 0;
        for (IPsop p: trace.getSignals()) {
            w.write(PSOP_TAG);
            w.write(SPACE);
            w.write(Integer.toString(id));
            w.write(ATTRIBUTE_START);
            writeAttributes(p.getAttributes(), w);
            writeFragments(p, id, w);
            id++;
        }
    }

    private void writeFragments(IPsop p, int signalId, BufferedWriter w) throws IOException {
        for (IPsopFragment f: p.getFragments()) {
            w.write(PSOP_FRAGMENT_TAG);
            w.write(SPACE);
            w.write(Integer.toString(signalId));
            w.write(SPACE);
            w.write(Double.toString(f.dom().lb().doubleValue()));
            w.write(SPACE);
            w.write(Double.toString(f.dom().ub().doubleValue()));
            w.write(SPACE);
            w.write(Double.toString(f.getC().doubleValue()));
            w.write(SPACE);
            w.write(Double.toString(f.getB().doubleValue()));
            w.write(SPACE);
            w.write(Double.toString(f.getA().doubleValue()));
            w.write(NEW_LINE);
        }
    }

    private void writeAttributes(Map<String, String> attributes, BufferedWriter w) throws IOException {
        int cnt = 0;
        for (Map.Entry<String, String> e: attributes.entrySet()) {
            cnt++;
            String key = escape(e.getKey()).trim();
            String val = escape(e.getValue()).trim();
            w.write(key);
            if (key.endsWith("\\")) {
                w.write(SPACE);
            }
            w.write(ATTRIBUTE_ASSIGN);
            w.write(val);
            if (val.endsWith("\\") && cnt < attributes.size()) {
                w.write(SPACE);
            }
            if (cnt < attributes.size()) {
                w.write(ATTRIBUTE_DELIMITER);
            }
        }
        w.write(NEW_LINE);
    }

    private String escape(String str) {
        return str.replace(ATTRIBUTE_DELIMITER_STR, "\\" + ATTRIBUTE_DELIMITER_STR).replace(ATTRIBUTE_ASSIGN_STR,
                "\\" + ATTRIBUTE_ASSIGN_STR);
    }
}
