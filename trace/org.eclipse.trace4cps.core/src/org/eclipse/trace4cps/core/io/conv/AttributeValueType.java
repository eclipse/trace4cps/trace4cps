/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.io.conv;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * A representation of the literals of the enumeration '<em><b>Attribute Value Type</b></em>', and utility methods for
 * working with them.
 *
 * @see nl.esi.trace.datamodel.TracePackage#getAttributeValueType()
 */
public enum AttributeValueType {
    /**
     * The '<em><b>String</b></em>' literal object.
     *
     * @see #STRING_VALUE
     */
    STRING(0, "String", "String"),

    /**
     * The '<em><b>Double</b></em>' literal object.
     *
     * @see #DOUBLE_VALUE
     */
    DOUBLE(1, "Double", "Double");

    /**
     * The '<em><b>String</b></em>' literal value.
     *
     * <p>
     * If the meaning of '<em><b>String</b></em>' literal object isn't clear, there really should be more of a
     * description here...
     * </p>
     *
     * @see #STRING
     * @model name="String"
     */
    public static final int STRING_VALUE = 0;

    /**
     * The '<em><b>Double</b></em>' literal value.
     *
     * <p>
     * If the meaning of '<em><b>Double</b></em>' literal object isn't clear, there really should be more of a
     * description here...
     * </p>
     *
     * @see #DOUBLE
     * @model name="Double"
     */
    public static final int DOUBLE_VALUE = 1;

    /**
     * An array of all the '<em><b>Attribute Value Type</b></em>' enumerators.
     */
    private static final AttributeValueType[] VALUES_ARRAY = new AttributeValueType[] {STRING, DOUBLE};

    /**
     * A public read-only list of all the '<em><b>Attribute Value Type</b></em>' enumerators.
     */
    public static final List<AttributeValueType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
     * Returns the '<em><b>Attribute Value Type</b></em>' literal with the specified literal value.
     */
    public static AttributeValueType get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            AttributeValueType result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Attribute Value Type</b></em>' literal with the specified name.
     */
    public static AttributeValueType getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            AttributeValueType result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Attribute Value Type</b></em>' literal with the specified integer value.
     */
    public static AttributeValueType get(int value) {
        switch (value) {
            case STRING_VALUE:
                return STRING;
            case DOUBLE_VALUE:
                return DOUBLE;
        }
        return null;
    }

    /**
     */
    private final int value;

    /**
     */
    private final String name;

    /**
     */
    private final String literal;

    /**
     * Only this class can construct instances.
     */
    private AttributeValueType(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    /**
     */
    public int getValue() {
        return value;
    }

    /**
     */
    public String getName() {
        return name;
    }

    /**
     */
    public String getLiteral() {
        return literal;
    }

    /**
     * Returns the literal value of the enumerator, which is its string representation.
     */
    @Override
    public String toString() {
        return literal;
    }
} // AttributeValueType
