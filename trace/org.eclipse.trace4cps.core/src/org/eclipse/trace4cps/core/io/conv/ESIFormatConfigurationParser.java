/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.io.conv;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class ESIFormatConfigurationParser extends ESIFormatParser {
    private static final String FORMAT_VERSION = "V0.1";

    private static final int MAX_TIMESCALESHIFT = 99;

    private static final String CONFIGURATIONFORMATVERSION_TAG = "ConfigurationFormatVersion"; // deprecated

    private static final String CONFIGURATIONVERSION_TAG = "ConfigurationVersion";

    private static final String CONFIGURATIONNAME_TAG = "ConfigurationName";

    private static final String TIMESCALESHIFT_TAG = "TimeScaleShift";

    private static final String TIMESCALEUNIT_TAG = "TimeScaleUnit";

    private static final String TIMEPRESENTATIONFORMAT_TAG = "TimePresentationFormat"; // deprecated

    private static final String TIMEDISPLAYFORMAT_TAG = "TimeDisplayFormat";

    private static final String ATTRIBUTE_TAG = "Attribute";

    private static final String CONTEXT_TAG = "Context";

    private static final String COLORDEF_TAG = "ColorDefinition";

    private static final String QUANTITY_TAG = "Quantity";

    private static final String FILTERINGVALUES_TAG = "FilteringValues";

    private static final String SCALE_CLAIMS_IN_ACTIVITY_VIEW_TAG = "ScaleClaimsActivityView";

    /**
     * Parses a Trace V0.x configuration file.
     *
     * @param configFile The config file to parse
     * @return The parsed config
     * @throws IOException
     * @throws ParserException
     */
    public Configuration parseTraceV0config(File configFile) throws IOException, ParserException {
        Configuration config = new Configuration();
        config.setConfigurationSource(configFile.getAbsolutePath());
        try (FileInputStream in = new FileInputStream(configFile)) {
            parse(configFile.getAbsolutePath(), in, config);
        }
        return config;
    }

    private void parse(String src, FileInputStream in, Configuration aConfig) throws IOException, ParserException {
        int n_CONFIGURATIONVERSION = 0;
        int n_CONFIGURATIONNAME = 0;
        int n_CONFIGURATIONID = 0;
        int n_TIMESCALESHIFT = 0;
        int n_TIMESCALEUNIT = 0;
        int n_TIMEDISPLAYFORMAT = 0;
        int n_SCALE_CLAIMS = 0;

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
            String line = null;
            int linenr = 0;

            while ((n_CONFIGURATIONVERSION == 0) && ((line = reader.readLine()) != null)) {
                linenr++;
                String[] tok = line.split(SEPARATOR, -1);
                if (tok.length >= 2) {
                    switch (tok[0]) {
                        case CONFIGURATIONFORMATVERSION_TAG:
                            System.err.println(CONFIGURATIONFORMATVERSION_TAG + " is deprecated; use "
                                    + CONFIGURATIONVERSION_TAG + " instead");
                        case CONFIGURATIONVERSION_TAG:
                            aConfig.setConfigurationVersion(tok[1]);
                            if (!tok[1].equalsIgnoreCase(FORMAT_VERSION)) {
                                throw new ParserException("Configuration version not supported " + tok[1], src, linenr);
                            }
                            n_CONFIGURATIONVERSION++;
                            break;
                        default:
                            throw new ParserException("Configuration version missing as first element", src, linenr);
                    }
                }
            }

            while ((line = reader.readLine()) != null) {
                linenr++;
                String[] tok = line.split(SEPARATOR, -1);
                if (tok.length >= 2) {
                    int index;
                    switch (tok[0]) {
                        case CONFIGURATIONNAME_TAG:
                            if (n_CONFIGURATIONNAME > 0) {
                                throw new ParserException(
                                        "Configuration name allready declared as " + aConfig.getConfigurationName(),
                                        src, linenr);
                            }
                            aConfig.setConfigurationName(tok[1]);
                            n_CONFIGURATIONNAME++;
                            break;

                        case CONFIGURATIONID_TAG:
                            if (n_CONFIGURATIONID > 0) {
                                throw new ParserException(
                                        "Configuration ID allready declared as " + aConfig.getConfigurationID(), src,
                                        linenr);
                            }
                            aConfig.setConfigurationID(tok[1]);
                            n_CONFIGURATIONID++;
                            break;

                        case TIMESCALESHIFT_TAG:
                            if (n_TIMESCALESHIFT > 0) {
                                throw new ParserException(
                                        "TimeScaleShift allready declared as " + aConfig.getTimeScaleShift(), src,
                                        linenr);
                            }
                            try {
                                int timeScaleShift = Integer.parseInt(tok[1]);
                                if (Math.abs(timeScaleShift) > MAX_TIMESCALESHIFT) {
                                    throw new ParserException("TimeScaleShift to large " + tok[1], src, linenr);
                                }
                                aConfig.setTimeScaleShift(timeScaleShift);
                                n_TIMESCALESHIFT++;
                            } catch (NumberFormatException e) {
                                throw new ParserException("TimeScaleShift has illegal value " + tok[1], src, linenr);
                            }
                            break;

                        case TIMESCALEUNIT_TAG:
                            if (n_TIMESCALEUNIT > 0) {
                                throw new ParserException(
                                        "TimeScaleUnit allready declared as " + aConfig.getTimeScaleUnit(), src,
                                        linenr);
                            }
                            if (!aConfig.setTimeScaleUnit(tok[1])) {
                                throw new ParserException(
                                        "TimeScaleUnit not valid: expected a java.util.concurrent.TimeUnit string "
                                                + aConfig.getTimeScaleUnit(),
                                        src, linenr);
                            }
                            n_TIMESCALEUNIT++;
                            break;

                        case TIMEPRESENTATIONFORMAT_TAG:
                            System.err.println(TIMEPRESENTATIONFORMAT_TAG + " is deprecated; use "
                                    + TIMEDISPLAYFORMAT_TAG + " instead");
                        case TIMEDISPLAYFORMAT_TAG:
                            if (n_TIMEDISPLAYFORMAT > 0) {
                                throw new ParserException(
                                        "TimeDisplayFormat allready declared as " + aConfig.getTimeDisplayFormat(), src,
                                        linenr);
                            }
                            aConfig.setTimeDisplayFormat(tok[1]);
                            n_TIMEDISPLAYFORMAT++;
                            try {
                                // Ignores the return value, just used to check if it throws an exception to
                                // verify the time display format is valid
                                String.format(aConfig.getTimeDisplayFormat(), 0.0);
                            } catch (Exception e) {
                                throw new ParserException("TimeDisplayFormat is illegal at: " + e.getMessage() + " in "
                                        + aConfig.getTimeDisplayFormat(), src, linenr);
                            }

                            break;

                        case SCALE_CLAIMS_IN_ACTIVITY_VIEW_TAG:
                            if (n_SCALE_CLAIMS > 0) {
                                throw new ParserException(
                                        SCALE_CLAIMS_IN_ACTIVITY_VIEW_TAG + " allready declared as "
                                                + aConfig.getScaleClaimsInActivityViewWithResourceAmount(),
                                        src, linenr);
                            }
                            aConfig.setScaleClaimsInActivityViewWithResourceAmount(Boolean.parseBoolean(tok[1]));
                            n_SCALE_CLAIMS++;
                            break;

                        case ATTRIBUTE_TAG:
                            int aid;
                            index = 1;
                            try {
                                aid = Integer.parseInt(tok[index]);
                                if (aConfig.getIdAttributeMap().containsKey(aid)) {
                                    throw new ParserException("Attribute(ID) already exists " + tok[index], src,
                                            linenr);
                                }
                                Attribute attribute = new Attribute();

                                if (tok.length <= ++index) {
                                    throw new ParserException(
                                            "Required Attribute name expected for Attribute id " + aid, src, linenr);
                                }
                                attribute.setAttributeName(tok[index]);

                                if (tok.length <= ++index) {
                                    throw new ParserException(
                                            "Required AttributeValue type expected for Attribute id " + aid, src,
                                            linenr);
                                }
                                AttributeValueType valuetype = AttributeValueType.get(tok[index]);
                                if (valuetype == null) {
                                    throw new ParserException(
                                            "Value type " + tok[index] + " is not recognized for Attribute id " + aid,
                                            src, linenr);
                                }
                                if (valuetype != AttributeValueType.STRING) {
                                    throw new ParserException(
                                            "Only AttributeValue type String is supported for Attribute id " + aid, src,
                                            linenr);
                                }
                                attribute.setAttributeValueType(valuetype);

                                for (++index; index < tok.length; index++) {
                                    AttributeUsageType usagetype = AttributeUsageType.get(tok[index]);
                                    if (usagetype == null) {
                                        throw new ParserException("Usage type " + tok[index]
                                                + " is not recognized for Attribute id " + aid, src, linenr);
                                    }
                                    if (!attribute.getAttributeUsages().contains(usagetype)) {
                                        attribute.getAttributeUsages().add(usagetype);
                                    }
                                }
                                aConfig.addAttribute(aid, attribute);
                            } catch (NumberFormatException e) {
                                throw new ParserException("AttributeID has illegal value " + tok[index], src, linenr);
                            }
                            break;

                        case CONTEXT_TAG:
                            index = 1;
                            int cid;
                            try {
                                cid = Integer.parseInt(tok[index]);
                                if (aConfig.getIdContextMap().containsKey(cid)) {
                                    throw new ParserException("Context(ID) already exists " + tok[index], src, linenr);
                                }
                                Context context = new Context();

                                if (tok.length <= ++index) {
                                    throw new ParserException("Required Context Name expected for Context id " + cid,
                                            src, linenr);
                                }
                                context.setContextName(tok[index]);

                                for (++index; index < tok.length; index++) {
                                    try {
                                        aid = Integer.parseInt(tok[index]);

                                        if (!aConfig.getIdAttributeMap().containsKey(aid)) {
                                            throw new ParserException(
                                                    "Referenced Attribute(ID) does not exist " + tok[index], src,
                                                    linenr);
                                        }
                                        context.getAttributes().add(aConfig.getAttribute(aid));
                                    } catch (NumberFormatException e) {
                                        throw new ParserException("Attribute reference has illegal value " + tok[index],
                                                src, linenr);
                                    }
                                }
                                aConfig.addContext(cid, context);
                            } catch (NumberFormatException e) {
                                throw new ParserException("ContextID has illegal value " + tok[index], src, linenr);
                            }
                            break;

                        case COLORDEF_TAG:
                            index = 1;
                            if (tok.length <= index) {
                                throw new ParserException("Required ColorName expected for ColorDefinition", src,
                                        linenr);
                            }
                            String colorDefName = tok[index];
                            if (tok.length <= ++index) {
                                throw new ParserException("Required Color expected for ColorDefinition", src, linenr);
                            }
                            Color colorDefColor = null;
                            try {
                                colorDefColor = Color.decode(tok[index]);
                            } catch (NumberFormatException e) {
                                throw new ParserException("Could not parse " + tok[index] + " as valid color", src,
                                        linenr);
                            }
                            aConfig.getColorDefinitionMap().put(colorDefName, colorDefColor);
                            break;

                        case QUANTITY_TAG:
                            throw new ParserException("Quantity not supported by the conversion", src, linenr);

                        case FILTERINGVALUES_TAG:
                            index = 1;
                            int afvid = Integer.parseInt(tok[index]);

                            if (!aConfig.getIdAttributeMap().containsKey(afvid)) {
                                throw new ParserException("Referenced Attribute(ID) does not exist " + tok[index], src,
                                        linenr);
                            }
                            Attribute attribute = aConfig.getAttribute(afvid);

                            if (tok.length <= ++index) {
                                throw new ParserException("At least 1 filtering value expected for FilteringValues",
                                        src, linenr);
                            }

                            while (index < tok.length) {
                                String filteringValue = tok[index++];
                                attribute.getAttributeFilteringValues().add(filteringValue.toCharArray());
                            }
                            break;

                        default:
                            throw new ParserException("Token not recognised " + tok[0], src, linenr);
                    }
                } else {
                    if (!tok[0].isEmpty()) {
                        throw new ParserException("Expected another token after " + tok[0], src, linenr);
                    }
                }
            }

            if (n_CONFIGURATIONID == 0) {
                throw new ParserException("Missing required " + CONFIGURATIONID_TAG, src, 0);
            }
            if (n_TIMESCALESHIFT == 0) {
                throw new ParserException("Missing required " + TIMESCALESHIFT_TAG, src, 0);
            }
            if (n_TIMESCALEUNIT == 0) {
                throw new ParserException("Missing required " + TIMESCALEUNIT_TAG, src, 0);
            }
            if (n_TIMEDISPLAYFORMAT == 0) {
                throw new ParserException("Missing required " + TIMEDISPLAYFORMAT_TAG, src, 0);
            }
        }
        return;
    }
}
