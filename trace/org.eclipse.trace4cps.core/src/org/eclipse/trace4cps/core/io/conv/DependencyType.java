/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.io.conv;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * A representation of the literals of the enumeration '<em><b>Dependency Type</b></em>', and utility methods for
 * working with them.
 *
 * @see nl.esi.trace.datamodel.TracePackage#getDependencyType()
 */
public enum DependencyType {
    /**
     * The '<em><b>Source Stop2 Sink Start</b></em>' literal object.
     *
     * @see #SOURCE_STOP2_SINK_START_VALUE
     */
    SOURCE_STOP2_SINK_START(0, "SourceStop2SinkStart", "SourceStop2SinkStart"),

    /**
     * The '<em><b>Source Stop2 Sink Stop</b></em>' literal object.
     *
     * @see #SOURCE_STOP2_SINK_STOP_VALUE
     */
    SOURCE_STOP2_SINK_STOP(1, "SourceStop2SinkStop", "SourceStop2SinkStop"),

    /**
     * The '<em><b>Source Start2 Sink Start</b></em>' literal object.
     *
     * @see #SOURCE_START2_SINK_START_VALUE
     */
    SOURCE_START2_SINK_START(2, "SourceStart2SinkStart", "SourceStart2SinkStart"),

    /**
     * The '<em><b>Source Start2 Sink Stop</b></em>' literal object.
     *
     * @see #SOURCE_START2_SINK_STOP_VALUE
     */
    SOURCE_START2_SINK_STOP(3, "SourceStart2SinkStop", "SourceStart2SinkStop");

    /**
     * The '<em><b>Source Stop2 Sink Start</b></em>' literal value.
     *
     * <p>
     * If the meaning of '<em><b>Source Stop2 Sink Start</b></em>' literal object isn't clear, there really should be
     * more of a description here...
     * </p>
     *
     * @see #SOURCE_STOP2_SINK_START
     * @model name="SourceStop2SinkStart"
     */
    public static final int SOURCE_STOP2_SINK_START_VALUE = 0;

    /**
     * The '<em><b>Source Stop2 Sink Stop</b></em>' literal value.
     *
     * <p>
     * If the meaning of '<em><b>Source Stop2 Sink Stop</b></em>' literal object isn't clear, there really should be
     * more of a description here...
     * </p>
     *
     * @see #SOURCE_STOP2_SINK_STOP
     * @model name="SourceStop2SinkStop"
     */
    public static final int SOURCE_STOP2_SINK_STOP_VALUE = 1;

    /**
     * The '<em><b>Source Start2 Sink Start</b></em>' literal value.
     *
     * <p>
     * If the meaning of '<em><b>Source Start2 Sink Start</b></em>' literal object isn't clear, there really should be
     * more of a description here...
     * </p>
     *
     * @see #SOURCE_START2_SINK_START
     * @model name="SourceStart2SinkStart"
     */
    public static final int SOURCE_START2_SINK_START_VALUE = 2;

    /**
     * The '<em><b>Source Start2 Sink Stop</b></em>' literal value.
     *
     * <p>
     * If the meaning of '<em><b>Source Start2 Sink Stop</b></em>' literal object isn't clear, there really should be
     * more of a description here...
     * </p>
     *
     * @see #SOURCE_START2_SINK_STOP
     * @model name="SourceStart2SinkStop"
     */
    public static final int SOURCE_START2_SINK_STOP_VALUE = 3;

    /**
     * An array of all the '<em><b>Dependency Type</b></em>' enumerators.
     */
    private static final DependencyType[] VALUES_ARRAY = new DependencyType[] {SOURCE_STOP2_SINK_START,
            SOURCE_STOP2_SINK_STOP, SOURCE_START2_SINK_START, SOURCE_START2_SINK_STOP};

    /**
     * A public read-only list of all the '<em><b>Dependency Type</b></em>' enumerators.
     */
    public static final List<DependencyType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
     * Returns the '<em><b>Dependency Type</b></em>' literal with the specified literal value.
     */
    public static DependencyType get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            DependencyType result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Dependency Type</b></em>' literal with the specified name.
     */
    public static DependencyType getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            DependencyType result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Dependency Type</b></em>' literal with the specified integer value.
     */
    public static DependencyType get(int value) {
        switch (value) {
            case SOURCE_STOP2_SINK_START_VALUE:
                return SOURCE_STOP2_SINK_START;
            case SOURCE_STOP2_SINK_STOP_VALUE:
                return SOURCE_STOP2_SINK_STOP;
            case SOURCE_START2_SINK_START_VALUE:
                return SOURCE_START2_SINK_START;
            case SOURCE_START2_SINK_STOP_VALUE:
                return SOURCE_START2_SINK_STOP;
        }
        return null;
    }

    /**
     */
    private final int value;

    /**
     */
    private final String name;

    /**
     */
    private final String literal;

    /**
     * Only this class can construct instances.
     */
    private DependencyType(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    /**
     */
    public int getValue() {
        return value;
    }

    /**
     */
    public String getName() {
        return name;
    }

    /**
     */
    public String getLiteral() {
        return literal;
    }

    /**
     * Returns the literal value of the enumerator, which is its string representation.
     */
    @Override
    public String toString() {
        return literal;
    }
} // DependencyType
