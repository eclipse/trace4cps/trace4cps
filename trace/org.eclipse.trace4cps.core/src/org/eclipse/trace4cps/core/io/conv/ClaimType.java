/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.io.conv;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * A representation of the literals of the enumeration '<em><b>Claim Type</b></em>', and utility methods for working
 * with them.
 *
 * @see nl.esi.trace.datamodel.TracePackage#getClaimType()
 */
public enum ClaimType {
    /**
     * The '<em><b>Full</b></em>' literal object.
     *
     * @see #FULL_VALUE
     */
    FULL(0, "Full", "Full"),

    /**
     * The '<em><b>Amount</b></em>' literal object.
     *
     * @see #AMOUNT_VALUE
     */
    AMOUNT(1, "Amount", "Amount"),

    /**
     * The '<em><b>Amount Offset</b></em>' literal object.
     *
     * @see #AMOUNT_OFFSET_VALUE
     */
    AMOUNT_OFFSET(2, "AmountOffset", "AmountOffset");

    /**
     * The '<em><b>Full</b></em>' literal value.
     *
     * <p>
     * If the meaning of '<em><b>Full</b></em>' literal object isn't clear, there really should be more of a description
     * here...
     * </p>
     *
     * @see #FULL
     * @model name="Full"
     */
    public static final int FULL_VALUE = 0;

    /**
     * The '<em><b>Amount</b></em>' literal value.
     *
     * <p>
     * If the meaning of '<em><b>Amount</b></em>' literal object isn't clear, there really should be more of a
     * description here...
     * </p>
     *
     * @see #AMOUNT
     * @model name="Amount"
     */
    public static final int AMOUNT_VALUE = 1;

    /**
     * The '<em><b>Amount Offset</b></em>' literal value.
     *
     * <p>
     * If the meaning of '<em><b>Amount Offset</b></em>' literal object isn't clear, there really should be more of a
     * description here...
     * </p>
     *
     * @see #AMOUNT_OFFSET
     * @model name="AmountOffset"
     */
    public static final int AMOUNT_OFFSET_VALUE = 2;

    /**
     * An array of all the '<em><b>Claim Type</b></em>' enumerators.
     */
    private static final ClaimType[] VALUES_ARRAY = new ClaimType[] {FULL, AMOUNT, AMOUNT_OFFSET};

    /**
     * A public read-only list of all the '<em><b>Claim Type</b></em>' enumerators.
     */
    public static final List<ClaimType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
     * Returns the '<em><b>Claim Type</b></em>' literal with the specified literal value.
     */
    public static ClaimType get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            ClaimType result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Claim Type</b></em>' literal with the specified name.
     */
    public static ClaimType getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            ClaimType result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Claim Type</b></em>' literal with the specified integer value.
     */
    public static ClaimType get(int value) {
        switch (value) {
            case FULL_VALUE:
                return FULL;
            case AMOUNT_VALUE:
                return AMOUNT;
            case AMOUNT_OFFSET_VALUE:
                return AMOUNT_OFFSET;
        }
        return null;
    }

    /**
     */
    private final int value;

    /**
     */
    private final String name;

    /**
     */
    private final String literal;

    /**
     * Only this class can construct instances.
     */
    private ClaimType(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    /**
     */
    public int getValue() {
        return value;
    }

    /**
     */
    public String getName() {
        return name;
    }

    /**
     */
    public String getLiteral() {
        return literal;
    }

    /**
     * Returns the literal value of the enumerator, which is its string representation.
     */
    @Override
    public String toString() {
        return literal;
    }
} // ClaimType
