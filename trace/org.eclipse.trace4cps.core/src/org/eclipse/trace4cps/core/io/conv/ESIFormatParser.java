/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.io.conv;

public abstract class ESIFormatParser {
    protected static final String CONFIGURATIONID_TAG = "ConfigurationID";

    protected static final String CONFIGURATION_SOURCE = "ConfigurationSource";

    // Fixed values for parsing
    protected static final String SEPARATOR = "\t";

    protected static final char TERMINATOR = '\n';
}
