/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.io;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.impl.Claim;
import org.eclipse.trace4cps.core.impl.Dependency;
import org.eclipse.trace4cps.core.impl.Event;
import org.eclipse.trace4cps.core.impl.Interval;
import org.eclipse.trace4cps.core.impl.Psop;
import org.eclipse.trace4cps.core.impl.PsopFragment;
import org.eclipse.trace4cps.core.impl.Resource;
import org.eclipse.trace4cps.core.impl.Trace;

/**
 * Class used to read an {@link ITrace} instance from its textual representation. Note that that the attribute keys and
 * values are trimmed with {@link String#trim()}. Furthermore, the time stamps ({@link Number} instances in the API) are
 * read as {@code double} values, except for the {@link ITrace#getTimeOffset()}, which is read as a {@code long} value.
 */
public class TraceReader {
    public static final String FILENAME_ATT = "name";

    private static final String WHITESPACE = "\\s+";

    static final char SPACE = ' ';

    static final char NEW_LINE = '\n';

    static final String TIMEUNIT_TAG = "TU";

    static final String OFFSET_TAG = "O";

    static final String TRACE_TAG = "T";

    static final String RESOURCE_TAG = "R";

    static final String DEPENDENCY_TAG = "D";

    static final String CLAIM_TAG = "C";

    static final String PSOP_TAG = "S";

    static final String PSOP_FRAGMENT_TAG = "F";

    static final String EVENT_TAG = "E";

    static final char ATTRIBUTE_START = ';';

    static final char ATTRIBUTE_DELIMITER = ',';

    static final String ATTRIBUTE_DELIMITER_STR = ",";

    static final char ATTRIBUTE_ASSIGN = '=';

    static final String ATTRIBUTE_ASSIGN_STR = "=";

    private static final String COMMENT_TAG = "#";

    private Trace trace;

    private int lineNumber;

    private final Map<Integer, IResource> resourceMap = new HashMap<>();

    private final Map<Integer, IClaim> claimMap = new HashMap<>();

    private final Map<Integer, Psop> signalMap = new HashMap<>();

    private final Map<Integer, Event> eventMap = new HashMap<>();

    private TraceReader() {
    }

    /**
     * Creates a {@link ITrace} from a string.
     *
     * @param s the textual representation of the trace
     * @return the corresponding {@link ITrace} instance
     * @throws ParseException if the input is not valid
     * @throws IOException if another error occurs
     */
    public static ITrace readTrace(String s) throws ParseException, IOException {
        return TraceReader.readTrace(new ByteArrayInputStream(s.getBytes()));
    }

    /**
     * Creates a {@link ITrace} from an input stream.
     *
     * @param in the stream with the textual representation of the trace
     * @return the corresponding {@link ITrace} instance
     * @throws ParseException if the input is not valid
     * @throws IOException if another error occurs
     */
    public static ITrace readTrace(InputStream in) throws ParseException, IOException {
        return new TraceReader().readTrace(new BufferedReader(new InputStreamReader(in)), null);
    }

    /**
     * Creates a {@link ITrace} from a file.
     *
     * @param f the file with the textual representation of the trace
     * @return the corresponding {@link ITrace} instance
     * @throws ParseException if the input is not valid
     * @throws IOException if another error occurs
     */
    public static ITrace readTrace(File f) throws ParseException, IOException {
        return new TraceReader().readTrace(new BufferedReader(new FileReader(f)), f.getName());
    }

    private ITrace readTrace(BufferedReader in, String name) throws ParseException, IOException {
        try {
            trace = new Trace();
            if (name != null) {
                trace.setAttribute(FILENAME_ATT, name); // overwritten by Trace attributes
            }
            String line = in.readLine();
            lineNumber = 1;
            while (line != null) {
                line = line.trim();
//                System.err.println("Parsing line " + lineNumber + " : " + line);
                if (!line.startsWith(COMMENT_TAG)) {
                    if (line.startsWith(TIMEUNIT_TAG)) {
                        parseTimeUnit(line);
                    } else if (line.startsWith(OFFSET_TAG)) {
                        parseOffset(line);
                    } else if (line.startsWith(RESOURCE_TAG)) {
                        parseResource(line);
                    } else if (line.startsWith(CLAIM_TAG)) {
                        parseClaim(line);
                    } else if (line.startsWith(EVENT_TAG)) {
                        parseEvent(line);
                    } else if (line.startsWith(DEPENDENCY_TAG)) {
                        parseDependency(line);
                    } else if (line.startsWith(PSOP_TAG)) {
                        parsePsop(line);
                    } else if (line.startsWith(PSOP_FRAGMENT_TAG)) {
                        parsePsopFragment(line);
                    } else if (line.startsWith(TRACE_TAG)) {
                        parseTrace(line);
                    } else if (!line.isEmpty()) {
                        throw new ParseException("Syntax error on line " + lineNumber);
                    }
                }
                line = in.readLine();
                lineNumber++;
            }
            validatePsops();
            return trace;
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                // Don't let this exception mask a potential other one
            }
        }
    }

    private void validatePsops() throws ParseException {
        for (IPsop p: signalMap.values()) {
            if (!p.getFragments().isEmpty()) {
                trace.add(p);
            }
        }
    }

    private void parseTimeUnit(String line) throws ParseException {
        final String msg = "Expected 'TU' <java.util.concurrent.TimeUnit> on line " + lineNumber;
        String[] tokens = line.split(WHITESPACE);
        if (tokens.length != 2) {
            throw new ParseException(msg);
        }
        try {
            trace.setTimeUnit(TimeUnit.valueOf(tokens[1]));
        } catch (IllegalArgumentException e) {
            throw new ParseException(msg, e);
        }
    }

    private void parseOffset(String line) throws ParseException {
        final String msg = "Expected 'O' <integer> on line " + lineNumber;
        String[] tokens = line.split(WHITESPACE);
        if (tokens.length != 2) {
            throw new ParseException(msg);
        }
        try {
            trace.setOffset(Long.parseLong(tokens[1]));
        } catch (NumberFormatException e) {
            throw new ParseException(msg, e);
        }
    }

    private void parseTrace(String line) throws ParseException {
        final String msg = "Expected 'T' <attributes>* on line " + lineNumber;
        int splitIndex = line.indexOf(TRACE_TAG);
        if (splitIndex < 0) {
            throw new ParseException(msg);
        }
        if (splitIndex + 1 < line.length()) {
            for (Map.Entry<String, String> e: parseAttributes(line.substring(splitIndex + 1)).entrySet()) {
                trace.setAttribute(e.getKey(), e.getValue());
            }
        }
    }

    private void parseResource(String line) throws ParseException {
        final String msg = "Expected 'R' <id> <capacity> <usesOffset> ';' <attributes>* on line " + lineNumber;
        int splitIndex = line.indexOf(ATTRIBUTE_START);
        if (splitIndex <= 0) {
            throw new ParseException(msg);
        }
        String part1 = line.substring(0, splitIndex);
        String part2 = splitIndex + 1 < line.length() ? line.substring(splitIndex + 1) : null;
        String[] res = part1.split(WHITESPACE);
        if (res.length != 4) {
            throw new ParseException(msg);
        }
        try {
            int id = Integer.parseInt(res[1]);
            double capacity = Double.parseDouble(res[2]);
            Boolean offset = Boolean.parseBoolean(res[3]);
            Resource r = new Resource(capacity, offset);
            r.setAttributes(parseAttributes(part2));
            if (resourceMap.containsKey(id)) {
                throw new ParseException("Non-unique resource id on line " + lineNumber);
            }
            resourceMap.put(id, r);
        } catch (NumberFormatException e) {
            throw new ParseException(msg, e);
        }
    }

    private void parseDependency(String line) throws ParseException {
        final String msg = "Expected 'D' <id> <type> <src> <dst> ';' <attributes>* on line " + lineNumber;
        int splitIndex = line.indexOf(ATTRIBUTE_START);
        if (splitIndex <= 0) {
            throw new ParseException(msg);
        }
        String part1 = line.substring(0, splitIndex);
        String part2 = splitIndex + 1 < line.length() ? line.substring(splitIndex + 1) : null;
        String[] res = part1.split(WHITESPACE);
        if (res.length != 5) {
            throw new ParseException(msg);
        }
        try {
            // id is not used
            int dependencyType = Integer.parseInt(res[2]);
            int sourceId = Integer.parseInt(res[3]);
            int dstId = Integer.parseInt(res[4]);
            Event[] events = getEvents(dependencyType, sourceId, dstId);
            if (events[0] == null) {
                throw new ParseException(
                        "Could not resolve dependency reference " + sourceId + " on line " + lineNumber);
            }
            if (events[1] == null) {
                throw new ParseException("Could not resolve dependency reference " + dstId + " on line " + lineNumber);
            }
            Dependency dep = new Dependency(events[0], events[1]);
            dep.setAttributes(parseAttributes(part2));
            trace.add(dep);
        } catch (NumberFormatException e) {
            throw new ParseException(msg, e);
        }
    }

    private Event[] getEvents(int dependencyType, int id1, int id2) throws ParseException {
        Event[] events = new Event[2];
        switch (DependencyType.typeFor(dependencyType)) {
            case EVENT_EVENT:
                events[0] = eventMap.get(id1);
                events[1] = eventMap.get(id2);
                break;
            case EVENT_CLAIM_START:
                events[0] = eventMap.get(id1);
                events[1] = (Event)claimMap.get(id2).getStartEvent();
                break;
            case EVENT_CLAIM_END:
                events[0] = eventMap.get(id1);
                events[1] = (Event)claimMap.get(id2).getEndEvent();
                break;
            case CLAIM_START_EVENT:
                events[0] = (Event)claimMap.get(id1).getStartEvent();
                events[1] = eventMap.get(id2);
                break;
            case CLAIM_END_EVENT:
                events[0] = (Event)claimMap.get(id1).getEndEvent();
                events[1] = eventMap.get(id2);
                break;
            case CLAIM_START_CLAIM_START:
                events[0] = (Event)claimMap.get(id1).getStartEvent();
                events[1] = (Event)claimMap.get(id2).getStartEvent();
                break;
            case CLAIM_START_CLAIM_END:
                events[0] = (Event)claimMap.get(id1).getStartEvent();
                events[1] = (Event)claimMap.get(id2).getEndEvent();
                break;
            case CLAIM_END_CLAIM_START:
                events[0] = (Event)claimMap.get(id1).getEndEvent();
                events[1] = (Event)claimMap.get(id2).getStartEvent();
                break;
            case CLAIM_END_CLAIM_END:
                events[0] = (Event)claimMap.get(id1).getEndEvent();
                events[1] = (Event)claimMap.get(id2).getEndEvent();
                break;
            default:
                throw new ParseException("Unknown dependency type " + dependencyType + " in line " + lineNumber);
        }
        return events;
    }

    private void parsePsop(String line) throws ParseException {
        final String msg = "Expected 'S' <id> ';' <attributes>* on line " + lineNumber;
        int splitIndex = line.indexOf(ATTRIBUTE_START);
        if (splitIndex <= 0) {
            throw new ParseException(msg);
        }
        String part1 = line.substring(0, splitIndex);
        String part2 = splitIndex + 1 < line.length() ? line.substring(splitIndex + 1) : null;
        String[] res = part1.split(WHITESPACE);
        if (res.length != 2) {
            throw new ParseException(msg);
        }
        try {
            int id = Integer.parseInt(res[1]);
            Psop p = new Psop();
            p.setAttributes(parseAttributes(part2));
            if (signalMap.containsKey(id)) {
                throw new ParseException("Non-unique signal id on line " + lineNumber);
            }
            signalMap.put(id, p);
        } catch (NumberFormatException e) {
            throw new ParseException(msg, e);
        }
    }

    private void parsePsopFragment(String line) throws ParseException {
        final String msg = "Expected 'F' <id> <t0> <t1> <c> <b> <a> on line " + lineNumber;
        String[] res = line.split(WHITESPACE);
        if (res.length < 5) {
            throw new ParseException(msg);
        }
        try {
            int id = Integer.parseInt(res[1]);
            double t0 = Double.parseDouble(res[2]);
            double t1 = Double.parseDouble(res[3]);
            double c = Double.parseDouble(res[4]);
            double b = 0;
            double a = 0;
            if (res.length >= 6) {
                b = Double.parseDouble(res[5]);
            }
            if (res.length == 7) {
                a = Double.parseDouble(res[6]);
            } else {
                throw new ParseException(msg);
            }
            Psop p = signalMap.get(id);
            if (p == null) {
                throw new ParseException("Unknown signal reference " + id + " on line " + lineNumber);
            }
            if (t0 > t1) {
                throw new ParseException("Invalid time interval on line " + lineNumber);
            } else if (t0 < t1) {
                ensureConsecutive(p, t0);
                p.add(new PsopFragment(c, b, a, new Interval(t0, false, t1, true)));
            } // don't add a fragment with equal timestamps
        } catch (NumberFormatException e) {
            throw new ParseException(msg, e);
        }
    }

    private void ensureConsecutive(Psop p, double t0) throws ParseException {
        if (p.getFragments().size() > 0) {
            double endOfPreviousTime = p.getFragments().get(p.getFragments().size() - 1).dom().ub().doubleValue();
            if (endOfPreviousTime != t0) {
                throw new ParseException("Non-consecutive fragment on line " + lineNumber
                        + ", end of previous fragment: " + endOfPreviousTime);
            }
        }
    }

    private void parseEvent(String line) throws ParseException {
        final String msg = "Expected 'E' <id> <t> ';' <attributes>* on line " + lineNumber;
        int splitIndex = line.indexOf(ATTRIBUTE_START);
        if (splitIndex <= 0) {
            throw new ParseException(msg);
        }
        String part1 = line.substring(0, splitIndex);
        String part2 = splitIndex + 1 < line.length() ? line.substring(splitIndex + 1) : null;
        String[] res = part1.split(WHITESPACE);
        if (res.length != 3) {
            throw new ParseException(msg);
        }
        try {
            int id = Integer.parseInt(res[1]);
            double t = Double.parseDouble(res[2]);
            Event event = new Event(t);
            event.setAttributes(parseAttributes(part2));
            if (eventMap.containsKey(id)) {
                throw new ParseException("Non-unique event id on line " + lineNumber);
            }
            eventMap.put(id, event);
            trace.add(event);
        } catch (NumberFormatException e) {
            throw new ParseException(msg, e);
        }
    }

    private void parseClaim(String line) throws ParseException {
        final String msg = "Expected 'C' <id> <t0> <t1> <resourceId> <offset>? <amount> ';' <attributes>* on line "
                + lineNumber;
        int splitIndex = line.indexOf(ATTRIBUTE_START);
        if (splitIndex <= 0) {
            throw new ParseException(msg);
        }
        String part1 = line.substring(0, splitIndex);
        String part2 = splitIndex + 1 < line.length() ? line.substring(splitIndex + 1) : null;
        String[] res = part1.split(WHITESPACE);
        if (res.length != 6 && res.length != 7) {
            throw new ParseException(msg);
        }
        try {
            int id = Integer.parseInt(res[1]);
            double t0 = Double.parseDouble(res[2]);
            double t1 = Double.parseDouble(res[3]);
            int resId = Integer.parseInt(res[4]);
            double offset = 0;
            double amount;
            if (res.length == 6) {
                amount = Double.parseDouble(res[5]);
            } else {
                offset = Double.parseDouble(res[5]);
                amount = Double.parseDouble(res[6]);
            }
            IResource resource = resourceMap.get(resId);
            if (resource == null) {
                throw new ParseException("unknown resource reference " + resId + " in line " + lineNumber);
            }
            if (resource.useOffset() && res.length == 6) {
                throw new ParseException("unspecified offset on line " + lineNumber);
            }
            if (!resource.useOffset() && res.length == 7) {
                throw new ParseException("superfluous offset on line " + lineNumber);
            }
            Claim c;
            if (t0 > t1 || offset + amount > resource.getCapacity().doubleValue()) {
                throw new ParseException("Invalid claim declaration on line " + lineNumber);
            }
            if (resource.useOffset()) {
                c = new Claim(t0, t1, resource, offset, amount);
            } else {
                c = new Claim(t0, t1, resource, amount);
            }
            c.setAttributes(parseAttributes(part2));
            if (claimMap.containsKey(id)) {
                throw new ParseException("Non-unique claim id on line " + lineNumber);
            }
            claimMap.put(id, c);
            trace.add(c);
        } catch (NumberFormatException e) {
            throw new ParseException(msg, e);
        }
    }

    private Map<String, String> parseAttributes(String line) throws ParseException {
        Map<String, String> m = new HashMap<>();
        if (line != null) {
            int i = 0;
            while (i < line.length()) {
                StringBuilder key = new StringBuilder();
                StringBuilder value = new StringBuilder();
                i = readKey(line, i, key);
                i = readValue(line, i, value);
                m.put(key.toString().trim(), value.toString().trim()); // TODO: remove escaped characters
            }
        }
        return m;
    }

    private int readKey(String line, int i, StringBuilder b) throws ParseException {
        int j = firstUnescaped(line, i, ATTRIBUTE_ASSIGN);
        if (j > i) {
            String key = line.substring(i, j).trim().replace("\\,", ",").replace("\\=", "=");
            b.append(key.trim());
            return j + 1;
        } else {
            throw new ParseException("Failed to parse attributes in line " + lineNumber);
        }
    }

    private int readValue(String line, int i, StringBuilder b) {
        int j = firstUnescaped(line, i, ATTRIBUTE_DELIMITER);
        if (j < 0) { // last attribute value
            j = line.length();
        }
        String value = line.substring(i, j).trim().replace("\\,", ",").replace("\\=", "=");
        b.append(value.trim());
        return j + 1;
    }

    private int firstUnescaped(String line, int fromIndex, char c) {
        while (fromIndex < line.length()) {
            int r = line.indexOf(c, fromIndex);
            if (r >= 0) {
                if (r > 0 && line.charAt(r - 1) == '\\') {
                    fromIndex = r + 1;
                } else {
                    return r;
                }
            } else {
                return -1;
            }
        }
        return -1;
    }
}
