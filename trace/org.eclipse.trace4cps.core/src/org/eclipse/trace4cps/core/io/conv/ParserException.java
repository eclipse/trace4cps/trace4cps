/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.io.conv;

public class ParserException extends Exception {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private final String msg;

    private final int linenr;

    private final String source;

    public ParserException(String aMsg, int aLinenr) {
        this(aMsg, "<unspecified>", aLinenr);
    }

    public ParserException(String aMsg, String source, int aLinenr) {
        msg = aMsg;
        this.source = source;
        linenr = aLinenr;
    }

    @Override
    public String getMessage() {
        return msg;
    }

    public String getSource() {
        return source;
    }

    public int getLinenr() {
        return linenr;
    }
}
