/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.io.conv;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.eclipse.trace4cps.core.impl.Trace;
import org.eclipse.trace4cps.core.io.TraceWriter;

/**
 * Can be invoked on the command line:
 * {@code java -classpath org.eclipse.trace4cps.core-?.?.?.jar org.eclipse.trace4cps.core.io.conv.TraceConverter <config file> <trace file>}
 */
public class TraceConverter {
    /**
     * Private constructor prevents object creation.
     */
    private TraceConverter() {
        // Empty
    }

    public static void main(String... args) {
        checkArgs(args);
        File configFile = new File(args[0]);
        checkFile(configFile);
        File traceFile = new File(args[1]);
        checkFile(traceFile);

        try {
            File out = new File(traceFile.getParent(), traceFile.getName() + ".etf");
            convert(configFile, traceFile, out);
        } catch (IOException | ParserException e) {
            e.printStackTrace();
        }
    }

    public static void convert(File configFile, File traceFile, File outputFile) throws IOException, ParserException {
        Configuration config = new ESIFormatConfigurationParser().parseTraceV0config(configFile);
        Trace trace = new ESIFormatTraceParser().parseTraceV0(traceFile, config);
        TraceWriter.writeTrace(trace, outputFile);
    }

    private static void checkFile(File f) {
        if (!f.canRead()) {
            System.out.println("Cannot read " + f.getAbsolutePath());
            System.exit(1);
        }
    }

    private static void checkArgs(String... args) {
        if (args.length != 2) {
            System.out.println(
                    "Usage: java -classpath org.eclipse.trace4cps.core-?.?.?.jar org.eclipse.trace4cps.core.io.conv.TraceConverter <config file> <trace file>");
            System.out.println("Given args: " + Arrays.toString(args));
            System.exit(1);
        }
    }
}
