/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.io.conv;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

/**
 * <!-- begin-user-doc --> An implementation of the model object ' <em><b>Configuration</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link nl.esi.trace.model.ganttchart.Configuration#getConfigurationVersion <em> Configuration Version</em>}</li>
 * <li>{@link nl.esi.trace.model.ganttchart.Configuration#getConfigurationSource <em> Configuration Source</em>}</li>
 * <li>{@link nl.esi.trace.model.ganttchart.Configuration#getConfigurationName <em> Configuration Name</em>}</li>
 * <li>{@link nl.esi.trace.model.ganttchart.Configuration#getConfigurationID <em> Configuration ID</em>}</li>
 * <li>{@link nl.esi.trace.model.ganttchart.Configuration#getTimeScaleShift <em>Time Scale Shift</em>}</li>
 * <li>{@link nl.esi.trace.model.ganttchart.Configuration#getTimeScaleUnit <em>Time Scale Unit</em>}</li>
 * <li>{@link nl.esi.trace.model.ganttchart.Configuration#getTimeResolutionShift <em> Time Resolution Shift</em>}</li>
 * <li>{@link nl.esi.trace.model.ganttchart.Configuration#getTimeDisplayFormat <em>Time Display Format</em>}</li>
 * <li>{@link nl.esi.trace.model.ganttchart.Configuration#getAttributes <em>Attributes </em>}</li>
 * <li>{@link nl.esi.trace.model.ganttchart.Configuration#getIdAttributeMap <em>Id Attribute Map</em>}</li>
 * <li>{@link nl.esi.trace.model.ganttchart.Configuration#getContexts <em>Contexts </em>}</li>
 * <li>{@link nl.esi.trace.model.ganttchart.Configuration#getIdContextMap <em>Id Context Map</em>}</li>
 * </ul>
 * </p>
 */
public class Configuration extends Object {
    /**
     * The default value of the '{@link #getConfigurationVersion() <em>Configuration Version</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     *
     * @see #getConfigurationVersion()
     */
    protected static final String CONFIGURATION_VERSION_DEFAULT = null;

    /**
     * The cached value of the '{@link #getConfigurationVersion() <em>Configuration Version</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     *
     * @see #getConfigurationVersion()
     */
    protected String configurationVersion = CONFIGURATION_VERSION_DEFAULT;

    /**
     * The default value of the '{@link #getConfigurationSource() <em>Configuration Source</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     *
     * @see #getConfigurationSource()
     */
    protected static final String CONFIGURATION_SOURCE_DEFAULT = null;

    /**
     * The cached value of the '{@link #getConfigurationSource() <em>Configuration Source</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     *
     * @see #getConfigurationSource()
     */
    protected String configurationSource = CONFIGURATION_SOURCE_DEFAULT;

    /**
     * The default value of the '{@link #getConfigurationName() <em>Configuration Name</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     *
     * @see #getConfigurationName()
     */
    protected static final String CONFIGURATION_NAME_DEFAULT = null;

    /**
     * The cached value of the '{@link #getConfigurationName() <em>Configuration Name</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     *
     * @see #getConfigurationName()
     */
    protected String configurationName = CONFIGURATION_NAME_DEFAULT;

    /**
     * The default value of the '{@link #getConfigurationID() <em>Configuration ID</em>}' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     *
     * @see #getConfigurationID()
     */
    protected static final String CONFIGURATION_ID_DEFAULT = null;

    /**
     * The cached value of the '{@link #getConfigurationID() <em>Configuration ID</em>}' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     *
     * @see #getConfigurationID()
     */
    protected String configurationID = CONFIGURATION_ID_DEFAULT;

    /**
     * The default value of the '{@link #getTimeScaleShift() <em>Time Scale Shift</em>}' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     *
     * @see #getTimeScaleShift()
     */
    protected static final int TIME_SCALE_SHIFT_DEFAULT = 0;

    /**
     * The cached value of the '{@link #getTimeScaleShift() <em>Time Scale Shift</em>}' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     *
     * @see #getTimeScaleShift()
     */
    protected int timeScaleShift = TIME_SCALE_SHIFT_DEFAULT;

    /**
     * The default value of the '{@link #getTimeScaleUnit() <em>Time Scale Unit</em>}' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     *
     * @see #getTimeScaleUnit()
     */
    protected static final String TIME_SCALE_UNIT_DEFAULT = "s";

    /**
     * The cached value of the '{@link #getTimeScaleUnit() <em>Time Scale Unit</em>}' attribute. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @see #getTimeScaleUnit()
     */
    protected TimeUnit timeScaleUnit = TimeUnit.SECONDS;

    /**
     * The default value of the '{@link #getTimeResolutionShift() <em>Time Resolution Shift</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     *
     * @see #getTimeResolutionShift()
     */
    protected static final int TIME_RESOLUTION_SHIFT_DEFAULT = 3;

    /**
     * The cached value of the '{@link #getTimeResolutionShift() <em>Time Resolution Shift</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     *
     * @see #getTimeResolutionShift()
     */
    protected int timeResolutionShift = TIME_RESOLUTION_SHIFT_DEFAULT;

    /**
     * The default value of the '{@link #getTimeDisplayFormat() <em>Time Display Format</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     *
     * @see #getTimeDisplayFormat()
     */
    protected static final String TIME_DISPLAY_FORMAT_DEFAULT = "%.5g";

    /**
     * The cached value of the '{@link #getTimeDisplayFormat() <em>Time Display Format</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     *
     * @see #getTimeDisplayFormat()
     */
    protected String timeDisplayFormat = TIME_DISPLAY_FORMAT_DEFAULT;

    /**
     * The cached value of the '{@link #getAttributes() <em>Attributes</em>}' containment reference list. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     *
     * @see #getAttributes()
     */
    protected ArrayList<Attribute> attributes;

    /**
     * The cached value of the '{@link #getIdAttributeMap() <em>Id Attribute Map</em>}' map. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @see #getIdAttributeMap()
     */
    protected HashMap<Integer, Object> idAttributeMap;

    protected HashMap<Object, Integer> attributeIdMap;

    /**
     * The cached value of the '{@link #getContexts() <em>Contexts</em>}' containment reference list. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     *
     * @see #getContexts()
     */
    protected ArrayList<Context> contexts;

    /**
     * The cached value of the '{@link #getIdContextMap() <em>Id Context Map</em>}' map. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     *
     * @see #getIdContextMap()
     */
    protected HashMap<Integer, Object> idContextMap;

    protected HashMap<Object, Integer> contextIdMap;

    protected HashMap<String, Color> colorDefinitionMap;

    protected HashMap<Integer, Object> idQuantityMap;

    private boolean scaleClaimsInActivityViewWithResourceAmount = true;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    protected Configuration() {
        super();
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public String getConfigurationID() {
        return configurationID;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public void setConfigurationID(String newConfigurationID) {
        configurationID = newConfigurationID;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public String getConfigurationName() {
        return configurationName;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public void setConfigurationName(String newConfigurationName) {
        configurationName = newConfigurationName;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public String getConfigurationVersion() {
        return configurationVersion;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public void setConfigurationVersion(String newConfigurationVersion) {
        configurationVersion = newConfigurationVersion;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public String getConfigurationSource() {
        return configurationSource;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public void setConfigurationSource(String newConfigurationSource) {
        configurationSource = newConfigurationSource;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public int getTimeScaleShift() {
        return timeScaleShift;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public void setTimeScaleShift(int newTimeScaleShift) {
        timeScaleShift = newTimeScaleShift;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public TimeUnit getTimeScaleUnit() {
        return timeScaleUnit;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public boolean setTimeScaleUnit(String newTimeScaleUnit) {
        try {
            timeScaleUnit = TimeUnit.valueOf(newTimeScaleUnit);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public int getTimeResolutionShift() {
        return timeResolutionShift;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     *
     * @generated
     */
    public void setTimeResolutionShift(int newTimeResolutionShift) {
        timeResolutionShift = newTimeResolutionShift;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public String getTimeDisplayFormat() {
        return timeDisplayFormat;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public void setTimeDisplayFormat(String newTimeDisplayFormat) {
        timeDisplayFormat = newTimeDisplayFormat;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public ArrayList<Attribute> getAttributes() {
        if (attributes == null) {
            attributes = new ArrayList<Attribute>();
        }
        return attributes;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public HashMap<Integer, Object> getIdAttributeMap() {
        if (idAttributeMap == null) {
            idAttributeMap = new HashMap<Integer, Object>();
        }
        return idAttributeMap;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public HashMap<Object, Integer> getAttributeIdMap() {
        if (attributeIdMap == null) {
            attributeIdMap = new HashMap<Object, Integer>();
        }
        return attributeIdMap;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public ArrayList<Context> getContexts() {
        if (contexts == null) {
            contexts = new ArrayList<Context>();
        }
        return contexts;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public HashMap<Integer, Object> getIdContextMap() {
        if (idContextMap == null) {
            idContextMap = new HashMap<Integer, Object>();
        }
        return idContextMap;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public HashMap<Object, Integer> getContextIdMap() {
        if (contextIdMap == null) {
            contextIdMap = new HashMap<Object, Integer>();
        }
        return contextIdMap;
    }

    public HashMap<String, Color> getColorDefinitionMap() {
        if (colorDefinitionMap == null) {
            colorDefinitionMap = new HashMap<String, Color>();
        }
        return colorDefinitionMap;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public HashMap<Integer, Object> getIdQuantityMap() {
        if (idQuantityMap == null) {
            idQuantityMap = new HashMap<Integer, Object>();
        }
        return idQuantityMap;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public boolean addAttribute(Integer aId, Attribute aAttribute) {
        boolean result = false;
        if (!getIdAttributeMap().containsKey(aId)) {
            aAttribute.setAttributeID(aId);
            if (getAttributes().add(aAttribute)) {
                getIdAttributeMap().put(aId, aAttribute);
                getAttributeIdMap().put(aAttribute, aId);
                result = true;
            }
        }
        return result;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public Attribute getAttribute(Integer aId) {
        return (Attribute)getIdAttributeMap().get(aId);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public boolean addContext(Integer aId, Context aContext) {
        boolean result = false;
        if (!getIdContextMap().containsKey(aId)) {
            aContext.setContextID(aId);
            if (getContexts().add(aContext)) {
                getIdContextMap().put(aId, aContext);
                getContextIdMap().put(aContext, aId);
                result = true;
            }
        }
        return result;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    public Context getContext(Integer aId) {
        return (Context)getIdContextMap().get(aId);
    }

    public boolean getScaleClaimsInActivityViewWithResourceAmount() {
        return scaleClaimsInActivityViewWithResourceAmount;
    }

    public void setScaleClaimsInActivityViewWithResourceAmount(boolean v) {
        this.scaleClaimsInActivityViewWithResourceAmount = v;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     */
    @Override
    public String toString() {
        StringBuffer result = new StringBuffer();
        result.append("Configuration:\n");
        result.append("   configurationVersion: " + configurationVersion + "\n");
        result.append("   configurationSource : " + configurationSource + "\n");
        result.append("   configurationName   : " + configurationName + "\n");
        result.append("   configurationID     : " + configurationID + "\n");
        result.append("   timeScaleShift      : " + timeScaleShift + "\n");
        result.append("   timeScaleUnit       : " + timeScaleUnit + "\n");
        result.append("   timeDisplayFormat   : " + timeDisplayFormat + "\n");
        if (attributes == null) {
            result.append("   no attributes\n");
        } else {
            for (Iterator<Attribute> ia = attributes.iterator(); ia.hasNext();) {
                result.append("   attribute: " + ia.next().toString() + "\n");
            }
        }
        if (contexts == null) {
            result.append("   no contexts\n");
        } else {
            for (Iterator<Context> ic = contexts.iterator(); ic.hasNext();) {
                Context cc = ic.next();
                result.append("   context  : " + cc.toString());
            }
        }

        return result.toString();
    }
} // Configuration
