/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.io.conv;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * A representation of the literals of the enumeration '<em><b>Period Limitations Type</b></em>', and utility methods
 * for working with them.
 */
public enum LimitationsType {
    /**
     * The '<em><b>None</b></em>' literal object.
     *
     * @see #NONE_VALUE
     */
    NONE(0, "None", "None"),
    /**
     * The '<em><b>Min Start</b></em>' literal object.
     *
     * @see #MIN_START_VALUE
     */
    MIN_START(1, "MinStart", "MinStart"),
    /**
     * The '<em><b>Max Stop</b></em>' literal object.
     *
     * @see #MAX_STOP_VALUE
     */
    MAX_STOP(2, "MaxStop", "MaxStop"),
    /**
     * The '<em><b>Min Start Max Stop</b></em>' literal object.
     *
     * @see #MIN_START_MAX_STOP_VALUE
     */
    MIN_START_MAX_STOP(3, "MinStart_MaxStop", "MinStart_MaxStop"),
    /**
     * The '<em><b>Max Duration</b></em>' literal object.
     *
     * @see #MAX_DURATION_VALUE
     */
    MAX_DURATION(4, "MaxDuration", "MaxDuration"),
    /**
     * The '<em><b>Min Start Max Duration</b></em>' literal object.
     *
     * @see #MIN_START_MAX_DURATION_VALUE
     */
    MIN_START_MAX_DURATION(5, "MinStart_MaxDuration", "MinStart_MaxDuration"),
    /**
     * The '<em><b>Max Stop Max Duration</b></em>' literal object.
     *
     * @see #MAX_STOP_MAX_DURATION_VALUE
     */
    MAX_STOP_MAX_DURATION(6, "MaxStop_MaxDuration", "MaxStop_MaxDuration"),
    /**
     * The '<em><b>Min Start Max Stop Max Duration</b></em>' literal object.
     *
     * @see #MIN_START_MAX_STOP_MAX_DURATION_VALUE
     */
    MIN_START_MAX_STOP_MAX_DURATION(7, "MinStart_MaxStop_MaxDuration", "MinStart_MaxStop_MaxDuration");

    /**
     * The '<em><b>None</b></em>' literal value.
     *
     * <p>
     * If the meaning of '<em><b>None</b></em>' literal object isn't clear, there really should be more of a description
     * here...
     * </p>
     *
     * @see #NONE
     * @model name="None"
     */
    public static final int NONE_VALUE = 0;

    /**
     * The '<em><b>Min Start</b></em>' literal value.
     *
     * <p>
     * If the meaning of '<em><b>Min Start</b></em>' literal object isn't clear, there really should be more of a
     * description here...
     * </p>
     *
     * @see #MIN_START
     * @model name="MinStart"
     */
    public static final int MIN_START_VALUE = 1;

    /**
     * The '<em><b>Max Stop</b></em>' literal value.
     *
     * <p>
     * If the meaning of '<em><b>Max Stop</b></em>' literal object isn't clear, there really should be more of a
     * description here...
     * </p>
     *
     * @see #MAX_STOP
     * @model name="MaxStop"
     */
    public static final int MAX_STOP_VALUE = 2;

    /**
     * The '<em><b>Min Start Max Stop</b></em>' literal value.
     *
     * <p>
     * If the meaning of '<em><b>Min Start Max Stop</b></em>' literal object isn't clear, there really should be more of
     * a description here...
     * </p>
     *
     * @see #MIN_START_MAX_STOP
     * @model name="MinStart_MaxStop"
     */
    public static final int MIN_START_MAX_STOP_VALUE = 3;

    /**
     * The '<em><b>Max Duration</b></em>' literal value.
     *
     * <p>
     * If the meaning of '<em><b>Max Duration</b></em>' literal object isn't clear, there really should be more of a
     * description here...
     * </p>
     *
     * @see #MAX_DURATION
     * @model name="MaxDuration"
     */
    public static final int MAX_DURATION_VALUE = 4;

    /**
     * The '<em><b>Min Start Max Duration</b></em>' literal value.
     *
     * <p>
     * If the meaning of '<em><b>Min Start Max Duration</b></em>' literal object isn't clear, there really should be
     * more of a description here...
     * </p>
     *
     * @see #MIN_START_MAX_DURATION
     * @model name="MinStart_MaxDuration"
     */
    public static final int MIN_START_MAX_DURATION_VALUE = 5;

    /**
     * The '<em><b>Max Stop Max Duration</b></em>' literal value.
     *
     * <p>
     * If the meaning of '<em><b>Max Stop Max Duration</b></em>' literal object isn't clear, there really should be more
     * of a description here...
     * </p>
     *
     * @see #MAX_STOP_MAX_DURATION
     * @model name="MaxStop_MaxDuration"
     */
    public static final int MAX_STOP_MAX_DURATION_VALUE = 6;

    /**
     * The '<em><b>Min Start Max Stop Max Duration</b></em>' literal value.
     *
     * <p>
     * If the meaning of '<em><b>Min Start Max Stop Max Duration</b></em>' literal object isn't clear, there really
     * should be more of a description here...
     * </p>
     *
     * @see #MIN_START_MAX_STOP_MAX_DURATION
     * @model name="MinStart_MaxStop_MaxDuration"
     */
    public static final int MIN_START_MAX_STOP_MAX_DURATION_VALUE = 7;

    /**
     * An array of all the '<em><b>Period Limitations Type</b></em>' enumerators.
     */
    private static final LimitationsType[] VALUES_ARRAY = new LimitationsType[] {NONE, MIN_START, MAX_STOP,
            MIN_START_MAX_STOP, MAX_DURATION, MIN_START_MAX_DURATION, MAX_STOP_MAX_DURATION,
            MIN_START_MAX_STOP_MAX_DURATION};

    /**
     * A public read-only list of all the '<em><b>Period Limitations Type</b></em>' enumerators.
     */
    public static final List<LimitationsType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
     * Returns the '<em><b>Period Limitations Type</b></em>' literal with the specified literal value.
     */
    public static LimitationsType get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            LimitationsType result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Period Limitations Type</b></em>' literal with the specified name.
     */
    public static LimitationsType getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            LimitationsType result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Period Limitations Type</b></em>' literal with the specified integer value.
     */
    public static LimitationsType get(int value) {
        switch (value) {
            case NONE_VALUE:
                return NONE;
            case MIN_START_VALUE:
                return MIN_START;
            case MAX_STOP_VALUE:
                return MAX_STOP;
            case MIN_START_MAX_STOP_VALUE:
                return MIN_START_MAX_STOP;
            case MAX_DURATION_VALUE:
                return MAX_DURATION;
            case MIN_START_MAX_DURATION_VALUE:
                return MIN_START_MAX_DURATION;
            case MAX_STOP_MAX_DURATION_VALUE:
                return MAX_STOP_MAX_DURATION;
            case MIN_START_MAX_STOP_MAX_DURATION_VALUE:
                return MIN_START_MAX_STOP_MAX_DURATION;
        }
        return null;
    }

    /**
     */
    private final int value;

    /**
     */
    private final String name;

    /**
     */
    private final String literal;

    /**
     * Only this class can construct instances.
     */
    private LimitationsType(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    /**
     */
    public int getValue() {
        return value;
    }

    /**
     */
    public String getName() {
        return name;
    }

    /**
     */
    public String getLiteral() {
        return literal;
    }

    /**
     * Returns the literal value of the enumerator, which is its string representation.
     */
    @Override
    public String toString() {
        return literal;
    }
} // PeriodLimitationsType
