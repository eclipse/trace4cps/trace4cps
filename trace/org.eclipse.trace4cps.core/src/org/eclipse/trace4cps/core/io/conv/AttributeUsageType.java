/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.io.conv;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * A representation of the literals of the enumeration '<em><b>Attribute Usage Type</b></em>', and utility methods for
 * working with them.
 *
 * @see nl.esi.trace.model.TracePackage#getAttributeUsageType()
 */
public enum AttributeUsageType {
    /**
     * The '<em><b>Describing</b></em>' literal object.
     *
     * @see #DESCRIBING_VALUE
     */
    DESCRIBING(1, "Describing", "Describing"),

    /**
     * The '<em><b>Labeling</b></em>' literal object.
     *
     * @see #LABELING_VALUE
     */
    LABELING(2, "Labeling", "Labeling"),

    /**
     * The '<em><b>Filtering</b></em>' literal object.
     *
     * @see #FILTERING_VALUE
     */
    FILTERING(3, "Filtering", "Filtering"),

    /**
     * The '<em><b>Grouping</b></em>' literal object.
     *
     * @see #GROUPING_VALUE
     */
    GROUPING(4, "Grouping", "Grouping"),

    /**
     * The '<em><b>Coloring</b></em>' literal object.
     *
     * @see #COLORING_VALUE
     */
    COLORING(5, "Coloring", "Coloring");

    /**
     * The '<em><b>Describing</b></em>' literal value.
     *
     * <p>
     * If the meaning of '<em><b>Describing</b></em>' literal object isn't clear, there really should be more of a
     * description here...
     * </p>
     *
     * @see #DESCRIBING
     * @model name="Describing"
     */
    public static final int DESCRIBING_VALUE = 1;

    /**
     * The '<em><b>Labeling</b></em>' literal value.
     *
     * <p>
     * If the meaning of '<em><b>Labeling</b></em>' literal object isn't clear, there really should be more of a
     * description here...
     * </p>
     *
     * @see #LABELING
     * @model name="Labeling"
     */
    public static final int LABELING_VALUE = 2;

    /**
     * The '<em><b>Filtering</b></em>' literal value.
     *
     * <p>
     * If the meaning of '<em><b>Filtering</b></em>' literal object isn't clear, there really should be more of a
     * description here...
     * </p>
     *
     * @see #FILTERING
     * @model name="Filtering"
     */
    public static final int FILTERING_VALUE = 3;

    /**
     * The '<em><b>Grouping</b></em>' literal value.
     *
     * <p>
     * If the meaning of '<em><b>Grouping</b></em>' literal object isn't clear, there really should be more of a
     * description here...
     * </p>
     *
     * @see #GROUPING
     * @model name="Grouping"
     */
    public static final int GROUPING_VALUE = 4;

    /**
     * The '<em><b>Coloring</b></em>' literal value.
     *
     * <p>
     * If the meaning of '<em><b>Coloring</b></em>' literal object isn't clear, there really should be more of a
     * description here...
     * </p>
     *
     * @see #COLORING
     * @model name="Coloring"
     */
    public static final int COLORING_VALUE = 5;

    /**
     * An array of all the '<em><b>Attribute Usage Type</b></em>' enumerators.
     */
    private static final AttributeUsageType[] VALUES_ARRAY = new AttributeUsageType[] {DESCRIBING, LABELING, FILTERING,
            GROUPING, COLORING};

    /**
     * A public read-only list of all the '<em><b>Attribute Usage Type</b></em>' enumerators.
     */
    public static final List<AttributeUsageType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
     * Returns the '<em><b>Attribute Usage Type</b></em>' literal with the specified literal value.
     */
    public static AttributeUsageType get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            AttributeUsageType result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Attribute Usage Type</b></em>' literal with the specified name.
     */
    public static AttributeUsageType getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            AttributeUsageType result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Attribute Usage Type</b></em>' literal with the specified integer value.
     */
    public static AttributeUsageType get(int value) {
        switch (value) {
            case DESCRIBING_VALUE:
                return DESCRIBING;
            case LABELING_VALUE:
                return LABELING;
            case FILTERING_VALUE:
                return FILTERING;
            case GROUPING_VALUE:
                return GROUPING;
            case COLORING_VALUE:
                return COLORING;
        }
        return null;
    }

    /**
     */
    private final int value;

    /**
     */
    private final String name;

    /**
     */
    private final String literal;

    /**
     * Only this class can construct instances.
     */
    private AttributeUsageType(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    /**
     */
    public int getValue() {
        return value;
    }

    /**
     */
    public String getName() {
        return name;
    }

    /**
     */
    public String getLiteral() {
        return literal;
    }

    /**
     * Returns the literal value of the enumerator, which is its string representation.
     */
    @Override
    public String toString() {
        return literal;
    }
} // AttributeUsageType
