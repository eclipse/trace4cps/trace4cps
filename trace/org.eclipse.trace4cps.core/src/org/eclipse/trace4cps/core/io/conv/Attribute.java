/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.io.conv;

import java.util.ArrayList;
import java.util.List;

/**
 * An implementation of the model object '<em><b>Attribute</b></em>'.
 *
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link nl.esi.trace.model.ganttchart.Attribute#getAttributeID <em>Attribute ID</em>}</li>
 * <li>{@link nl.esi.trace.model.ganttchart.Attribute#getAttributeName <em>Attribute Name</em>}</li>
 * <li>{@link nl.esi.trace.model.ganttchart.Attribute#getAttributeValueType <em>Attribute Value Type</em>}</li>
 * <li>{@link nl.esi.trace.datamodel Attribute#isAttributeInternal <em>Attribute Internal</em>}</li>
 * <li>{@link nl.esi.trace.model.ganttchart.Attribute#getAttributeUsages <em>Attribute Usages</em>}</li>
 * </ul>
 * </p>
 */
public class Attribute extends Object {
    /**
     * The default value of the '{@link #getAttributeID() <em>Attribute ID</em>}' attribute.
     *
     * @see #getAttributeID()
     */
    protected static final int ATTRIBUTE_ID_DEFAULT = 0;

    /**
     * The cached value of the '{@link #getAttributeID() <em>Attribute ID</em>}' attribute.
     *
     * @see #getAttributeID()
     */
    protected int attributeID = ATTRIBUTE_ID_DEFAULT;

    /**
     * The default value of the '{@link #getAttributeName() <em>Attribute Name</em>}' attribute.
     *
     * @see #getAttributeName()
     */
    protected static final String ATTRIBUTE_NAME_DEFAULT = null;

    /**
     * The cached value of the '{@link #getAttributeName() <em>Attribute Name</em>}' attribute.
     *
     * @see #getAttributeName()
     */
    protected String attributeName = ATTRIBUTE_NAME_DEFAULT;

    /**
     * The default value of the '{@link #getAttributeValueType() <em>Attribute Value Type</em>}' attribute.
     *
     * @see #getAttributeValueType()
     */
    protected static final AttributeValueType ATTRIBUTE_VALUE_TYPE_DEFAULT = AttributeValueType.STRING;

    /**
     * The cached value of the '{@link #getAttributeValueType() <em>Attribute Value Type</em>}' attribute.
     *
     * @see #getAttributeValueType()
     */
    protected AttributeValueType attributeValueType = ATTRIBUTE_VALUE_TYPE_DEFAULT;

    /**
     * The default value of the '{@link #isAttributeInternal() <em>Attribute Internal</em>}' attribute.
     *
     * @see #isAttributeInternal()
     */
    protected static final boolean ATTRIBUTE_INTERNAL_DEFAULT = false;

    /**
     * The cached value of the '{@link #isAttributeInternal() <em>Attribute Internal</em>}' attribute.
     *
     * @see #isAttributeInternal()
     */
    protected boolean attributeInternal = ATTRIBUTE_INTERNAL_DEFAULT;

    /**
     * The cached value of the '{@link #getAttributeUsages() <em>Attribute Usages</em>}' attribute list.
     *
     * @see #getAttributeUsages()
     */
    protected ArrayList<AttributeUsageType> attributeUsages;

    /**
     * The cached value of the '{@link #getAttributeFilteringValues() <em>Filtering Values</em>}' attribute list.
     *
     * @see #getAttributeFilteringValue()
     */
    protected ArrayList<char[]> attributeFilteringValues = new ArrayList<char[]>();

    /**
     */
    public Attribute() {
        super();
    }

    /**
     */
    public int getAttributeID() {
        return attributeID;
    }

    /**
     */
    public void setAttributeID(int newAttributeID) {
        attributeID = newAttributeID;
    }

    /**
     */
    public String getAttributeName() {
        return attributeName;
    }

    /**
     */
    public void setAttributeName(String newAttributeName) {
        attributeName = newAttributeName;
    }

    /**
     */
    public AttributeValueType getAttributeValueType() {
        return attributeValueType;
    }

    /**
     */
    public void setAttributeValueType(AttributeValueType newAttributeValueType) {
        attributeValueType = newAttributeValueType == null ? ATTRIBUTE_VALUE_TYPE_DEFAULT : newAttributeValueType;
    }

    /**
     */
    public boolean isAttributeInternal() {
        return attributeInternal;
    }

    /**
     */
    public void setAttributeInternal(boolean newAttributeInternal) {
        attributeInternal = newAttributeInternal;
    }

    /**
     */
    public List<AttributeUsageType> getAttributeUsages() {
        if (attributeUsages == null) {
            attributeUsages = new ArrayList<AttributeUsageType>();
        }
        return attributeUsages;
    }

    /**
     */
    public ArrayList<char[]> getAttributeFilteringValues() {
        return attributeFilteringValues;
    }

    /**
     */
    public void setAttributeFilteringValues(ArrayList<char[]> filteringValue) {
        this.attributeFilteringValues = filteringValue;
    }

    /**
     */
    @Override
    public String toString() {
        StringBuffer result = new StringBuffer('(');
        result.append("[ID: " + attributeID);
        result.append(", Name: " + attributeName);
        result.append(", ValueType: " + attributeValueType);
        result.append(", Internal: " + attributeInternal);
        result.append(", Usages: " + attributeUsages);
        result.append(']');
        return result.toString();
    }
} // Attribute
