/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.io.conv;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * An implementation of the model object '<em><b>Context</b></em>'.
 *
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link nl.esi.trace.model.ganttchart.Context#getContextID <em>Context ID</em>}</li>
 * <li>{@link nl.esi.trace.model.ganttchart.Context#getContextName <em>Context Name</em>}</li>
 * <li>{@link nl.esi.trace.model.ganttchart.Context#getAttributes <em>Attributes</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Context extends Object {
    /**
     * The default value of the '{@link #getContextID() <em>Context ID</em>}' attribute.
     *
     * @see #getContextID()
     */
    protected static final int CONTEXT_ID_DEFAULT = 0;

    /**
     * The cached value of the '{@link #getContextID() <em>Context ID</em>}' attribute.
     *
     * @see #getContextID()
     */
    protected int contextID = CONTEXT_ID_DEFAULT;

    /**
     * The default value of the '{@link #getContextName() <em>Context Name</em>}' attribute.
     *
     * @see #getContextName()
     */
    protected static final String CONTEXT_NAME_DEFAULT = null;

    /**
     * The cached value of the '{@link #getContextName() <em>Context Name</em>}' attribute.
     *
     * @see #getContextName()
     */
    protected String contextName = CONTEXT_NAME_DEFAULT;

    /**
     * The cached value of the '{@link #getAttributes() <em>Attributes</em>}' reference list.
     *
     * @see #getAttributes()
     */
    protected ArrayList<Attribute> attributes;

    /**
     */
    public Context() {
        super();
    }

    /**
     */
    public int getContextID() {
        return contextID;
    }

    /**
     */
    public void setContextID(int newContextID) {
        contextID = newContextID;
    }

    /**
     */
    public String getContextName() {
        return contextName;
    }

    /**
     */
    public void setContextName(String newContextName) {
        contextName = newContextName;
    }

    /**
     */
    public ArrayList<Attribute> getAttributes() {
        if (attributes == null) {
            attributes = new ArrayList<Attribute>();
        }
        return attributes;
    }

    /**
     */
    @Override
    public String toString() {
        StringBuffer result = new StringBuffer();
        result.append("[Context ID: " + contextID + ", Context Name: " + contextName + " attributeIDs: ");
        if (attributes == null) {
            result.append("null");
        } else {
            for (Iterator<Attribute> ia = attributes.iterator(); ia.hasNext();) {
                result.append(ia.next().getAttributeID() + (ia.hasNext() ? ", " : ""));
            }
        }
        result.append("]\n");
        return result.toString();
    }
} // Context
