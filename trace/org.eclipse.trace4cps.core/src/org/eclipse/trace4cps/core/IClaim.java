/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core;

/**
 * One of the central types of the TRACE data model.
 *
 * <p>
 * A {@link IClaim} models the start and end time of a claim on a certain amount of some {@link IResource}. Every
 * {@link IClaim} instance is associated with two {@link IClaimEvent} instances: one for its start and one for its end.
 * </p>
 */
public interface IClaim extends IAttributeAware {
    /**
     * @return the start time of the claim
     */
    Number getStartTime();

    /**
     * @return the end time of the claim
     */
    Number getEndTime();

    /**
     * @return the claimed resource
     */
    IResource getResource();

    /**
     * @return the claimed amount
     */
    Number getAmount();

    /**
     * @return the offset of the claimed amount, only meaningful if the resource uses an offset:
     *     {@link IResource#useOffset()}
     */
    Number getOffset();

    /**
     * @return the start event associated with this claim
     */
    IClaimEvent getStartEvent();

    /**
     * @return the end event associated with this claim
     */
    IClaimEvent getEndEvent();
}
