/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core;

import java.util.Map;

/**
 * Top-level interface for many types of the TRACE data model. Implementations provide a set of {@code String}
 * attributes that can be read and written through this interface.
 */
public interface IAttributeAware {
    /**
     * Gets the attribute value with the specific key value
     *
     * @param key the key
     * @return the attribute value
     */
    String getAttributeValue(String key);

    /**
     * Gets the full attribute map, typically unmodifiable.
     *
     * @return the attribute values
     */
    Map<String, String> getAttributes();

    /**
     * Sets a new attribute value, or overwrites an existing one.
     *
     * @param key the key
     * @param value the attribute value
     */
    void setAttribute(String key, String value);

    /**
     * Removes all attributes.
     */
    void clearAttributes();
}
