/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IClaimEvent;
import org.eclipse.trace4cps.core.IDependency;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.ITrace;

/**
 * Default implementation of the {@link ITrace} type.
 */
public class Trace extends AttributeAware implements ITrace {
    private List<IClaim> claims = new ArrayList<>();

    private Set<IResource> resources = new HashSet<>();

    private List<IEvent> events = new ArrayList<>();

    private List<IDependency> dependencies = new ArrayList<>();

    private List<IPsop> signals = new ArrayList<>();

    private TimeUnit traceTimeUnit = TimeUnit.SECONDS;

    private Number timeOffset = 0L;

    private boolean sorted = false;

    /**
     * {@inheritDoc}
     */
    @Override
    public Number getTimeOffset() {
        return timeOffset;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TimeUnit getTimeUnit() {
        return traceTimeUnit;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<IClaim> getClaims() {
        if (!sorted) {
            sortAll();
        }
        return Collections.unmodifiableList(claims);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<IResource> getResources() {
        List<IResource> r = new ArrayList<>();
        r.addAll(resources);
        return Collections.unmodifiableList(r);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<IEvent> getEvents() {
        if (!sorted) {
            sortAll();
        }
        return Collections.unmodifiableList(events);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<IDependency> getDependencies() {
        return Collections.unmodifiableList(dependencies);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<IPsop> getSignals() {
        return Collections.unmodifiableList(signals);
    }

    public void setTimeUnit(TimeUnit tu) {
        this.traceTimeUnit = tu;
    }

    public void setOffset(Number offset) {
        this.timeOffset = offset;
    }

    public void add(IClaim claim) {
        claims.add(claim);
        events.add(claim.getStartEvent());
        events.add(claim.getEndEvent());
        resources.add(claim.getResource());
        sorted = false;
    }

    public void add(IEvent event) {
        if (event instanceof IClaimEvent) {
            throw new IllegalArgumentException("IClaimEvent instances are added automatically via add(IClaim)");
        }
        events.add(event);
        sorted = false;
    }

    public void add(IDependency dependency) {
        dependencies.add(dependency);
    }

    public void add(IPsop psop) {
        signals.add(psop);
    }

    public void sortAll() {
        if (!sorted) {
            Collections.sort(events, new Comparator<IEvent>() {
                @Override
                public int compare(IEvent o1, IEvent o2) {
                    return Double.compare(o1.getTimestamp().doubleValue(), o2.getTimestamp().doubleValue());
                }
            });
            Collections.sort(claims, new Comparator<IClaim>() {
                @Override
                public int compare(IClaim o1, IClaim o2) {
                    return Double.compare(o1.getStartTime().doubleValue(), o2.getStartTime().doubleValue());
                }
            });
            sorted = true;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Trace[#events=" + events.size() + ", #claims=" + claims.size() + ", #dependencies="
                + dependencies.size() + ", #signals=" + signals.size() + "]";
    }
}
