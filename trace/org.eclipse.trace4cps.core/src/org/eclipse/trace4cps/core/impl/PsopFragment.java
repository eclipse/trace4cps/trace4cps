/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.impl;

import org.eclipse.trace4cps.core.IInterval;
import org.eclipse.trace4cps.core.IPsopFragment;
import org.eclipse.trace4cps.core.Shape;

/**
 * Default implementation of the {@link IPsopFragment} type, immutable.
 */
public final class PsopFragment implements IPsopFragment {
    private final Number c;

    private final Number b;

    private final Number a;

    private final IInterval dom;

    // The order and shape are derived
    private final int order;

    private final Shape shape;

    public PsopFragment(Number c, Number b, Number a, IInterval timeDomain) {
        this(c, b, a, timeDomain, null);
    }

    private PsopFragment(Number c, Number b, Number a, IInterval timeDomain, Shape shape) {
        if (c == null || b == null || a == null) {
            throw new IllegalArgumentException("coefficients must not be null");
        }
        if (timeDomain == null || timeDomain.isEmpty()) {
            throw new IllegalArgumentException("cannot create fragment with empty domain");
        }
        if (!(!timeDomain.isOpenLb() && timeDomain.isOpenUb())) {
            throw new IllegalArgumentException("cannot create fragment with non [lb, ub) time domain");
        }
        this.c = c;
        this.b = b;
        this.a = a;
        if (a.doubleValue() != 0) {
            order = 2;
        } else if (b.doubleValue() != 0) {
            order = 1;
        } else {
            order = 0;
        }
        this.dom = timeDomain;
        if (shape == null) {
            this.shape = computeShape();
        } else {
            this.shape = shape;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Number getA() {
        return a;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Number getB() {
        return b;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Number getC() {
        return c;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Shape getShape() {
        return shape;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getOrder() {
        return order;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IInterval dom() {
        return dom;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "PsopFragment[dom=" + dom + ", c=" + c + ", b=" + b + ", a=" + a + ", shape=" + shape + "]";
    }

    private Shape computeShape() {
        if (order == 0) {
            return Shape.CONSTANT;
        } else if (order == 1) {
            if (b.doubleValue() > 0) {
                return Shape.INCREASING;
            } else {
                return Shape.DECREASING;
            }
        } else if (order == 2) {
            return compute2ndOrderShape();
        }
        throw new IllegalStateException();
    }

    private Shape compute2ndOrderShape() {
        // check whether we have a point with 0 derivative in the time domain:
        // Compute the parameters of the quadratic equation a*x^2 + b*x + c
        double tTop = -b.doubleValue() / (2 * a.doubleValue());
        if (dom.lb().doubleValue() < dom.lb().doubleValue() + tTop
                && dom.lb().doubleValue() + tTop < dom.ub().doubleValue())
        {
            return compute2ndOrderShapeTopInSegment(tTop);
        } else {
            // check whether increasing or decreasing
            double dt = dom.ub().doubleValue() - dom.lb().doubleValue();
            double xEnd = c.doubleValue() + b.doubleValue() * dt + a.doubleValue() * dt * dt;
            if (c.doubleValue() < xEnd) {
                return Shape.INCREASING;
            } else {
                return Shape.DECREASING;
            }
        }
    }

    private Shape compute2ndOrderShapeTopInSegment(double tTop) {
        double dt = dom.ub().doubleValue() - dom.lb().doubleValue();
        double xTop = c.doubleValue() + b.doubleValue() * tTop + a.doubleValue() * tTop * tTop;
        double xEnd = c.doubleValue() + b.doubleValue() * dt + a.doubleValue() * dt * dt;
        if (c.doubleValue() < xTop && xTop > xEnd) {
            return Shape.PARABOLA_CAP;
        } else if (c.doubleValue() > xTop && xTop < xEnd) {
            return Shape.PARABOLA_CUP;
        } else if (c.doubleValue() <= xTop && xTop <= xEnd) { // rounding errors...
            return Shape.INCREASING;
        } else if (c.doubleValue() >= xTop && xTop >= xEnd) { // rounding errors ...
            return Shape.DECREASING;
        }
        throw new IllegalStateException();
    }
}
