/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.impl;

import org.eclipse.trace4cps.core.IInterval;

/**
 * Default implementation of the {@link IInterval} type, immutable.
 */
public final class Interval implements IInterval {
    private final Number lb;

    private final Number ub;

    private final boolean openLb;

    private final boolean openUb;

    /**
     * Constructs a left-closed right-open interval
     *
     * @param lb the lower bound
     * @param ub the upper bound
     */
    public Interval(Number lb, Number ub) {
        this(lb, false, ub, true);
    }

    public Interval(Number lb, boolean openLb, Number ub, boolean openUb) {
        this.lb = lb;
        this.openLb = openLb;
        this.ub = ub;
        this.openUb = openUb;
    }

    /**
     * @return the trivial positive interval: {@code [0, Infty)}
     */
    public static Interval trivial() {
        return new Interval(0d, false, Double.POSITIVE_INFINITY, true);
    }

    public boolean isTrivial() {
        return lb.doubleValue() == 0d && !openLb && ub.doubleValue() == Double.POSITIVE_INFINITY;
    }

    public boolean strictlySmallerThan(Number ts) {
        if (ub.doubleValue() < 0d) {
            return false;
        }
        if (openUb) {
            return ts.doubleValue() >= ub.doubleValue();
        }
        return ts.doubleValue() > ub.doubleValue();
    }

    public boolean strictlyLargerThan(Number ts) {
        if (openLb) {
            return ts.doubleValue() <= lb.doubleValue();
        }
        return ts.doubleValue() < lb.doubleValue();
    }

    @Override
    public boolean contains(Number ts) {
        if (ts.doubleValue() < lb.doubleValue() || (ts.doubleValue() <= lb.doubleValue() && openLb)) {
            return false;
        }
        // remaining: finite upper bound:
        if (openUb) {
            return ts.doubleValue() < ub.doubleValue();
        } else {
            return ts.doubleValue() <= ub.doubleValue();
        }
    }

    @Override
    public Number lb() {
        return lb;
    }

    @Override
    public Number ub() {
        return ub;
    }

    @Override
    public boolean isOpenLb() {
        return openLb;
    }

    @Override
    public boolean isOpenUb() {
        return openUb;
    }

    @Override
    public boolean isEmpty() {
        return lb.doubleValue() > ub.doubleValue() || (lb.doubleValue() == ub.doubleValue() && (openLb || openUb));
    }

    @Override
    public String toString() {
        String l = openLb ? "(" : "[";
        String r = openUb ? ")" : "]";
        return l + lb + "," + ub + r;
    }

    public static Interval intersect(Interval i1, Interval i2) {
        Number lb, ub;
        boolean openLb, openUb;
        // LB
        if (i1.lb.doubleValue() < i2.lb.doubleValue()) {
            lb = i2.lb;
            openLb = i2.openLb;
        } else if (i1.lb.doubleValue() == i2.lb.doubleValue()) {
            lb = i1.lb;
            openLb = i1.openLb || i2.openLb;
        } else {
            lb = i1.lb;
            openLb = i1.openLb;
        }
        // UB
        if (i1.ub.doubleValue() < i2.ub.doubleValue()) {
            ub = i1.ub;
            openUb = i1.openUb;
        } else if (i1.ub.doubleValue() == i2.ub.doubleValue()) {
            ub = i1.ub;
            openUb = i1.openUb || i2.openUb;
        } else {
            ub = i2.ub;
            openUb = i2.openUb;
        }
        return new Interval(lb, openLb, ub, openUb);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((lb == null) ? 0 : lb.hashCode());
        result = prime * result + (openLb ? 1231 : 1237);
        result = prime * result + (openUb ? 1231 : 1237);
        result = prime * result + ((ub == null) ? 0 : ub.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Interval other = (Interval)obj;
        if (lb == null) {
            if (other.lb != null) {
                return false;
            }
        } else if (!lb.equals(other.lb)) {
            return false;
        }
        if (openLb != other.openLb) {
            return false;
        }
        if (openUb != other.openUb) {
            return false;
        }
        if (ub == null) {
            if (other.ub != null) {
                return false;
            }
        } else if (!ub.equals(other.ub)) {
            return false;
        }
        return true;
    }
}
