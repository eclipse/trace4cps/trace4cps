/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.impl;

import java.util.Map;

import org.eclipse.trace4cps.core.ClaimEventType;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IClaimEvent;
import org.eclipse.trace4cps.core.IResource;

/**
 * Default implementation of the {@link IClaim} type. The constructors create new instances for the start and end
 * events.
 */
public class Claim extends AttributeAware implements IClaim {
    private final Number startTime;

    private final Number endTime;

    private final IResource resource;

    private final Number offset;

    private final Number amount;

    private final ClaimEvent startEvent;

    private final ClaimEvent stopEvent;

    public Claim(double t0, double t1, IResource r, Number amount) {
        this(t0, t1, r, Double.NaN, amount, null);
    }

    public Claim(double t0, double t1, IResource r, Number amount, Map<String, String> attMap) {
        this(t0, t1, r, Double.NaN, amount, attMap);
    }

    public Claim(Number t0, Number t1, IResource r, Number offset, Number amount) {
        this(t0, t1, r, offset, amount, null);
    }

    public Claim(Number t0, Number t1, IResource r, Number offset, Number amount, Map<String, String> attMap) {
        this.startTime = t0;
        this.endTime = t1;
        this.resource = r;
        if ((r.useOffset() && Double.isNaN(offset.doubleValue()))
                || (!r.useOffset() && !Double.isNaN(offset.doubleValue())))
        {
            throw new IllegalArgumentException("offset must be specified if and only if the resource uses an offset");
        }
        this.offset = offset;
        this.amount = amount;
        if (startTime.doubleValue() > endTime.doubleValue()) {
            throw new IllegalArgumentException("start time cannot be larger than end time");
        }
        if (offset.doubleValue() + amount.doubleValue() > r.getCapacity().doubleValue()) {
            throw new IllegalArgumentException("offset + amount cannot be larger than resource capacity");
        }
        setAttributes(attMap);
        this.startEvent = new ClaimEvent(startTime, this, ClaimEventType.START);
        this.stopEvent = new ClaimEvent(endTime, this, ClaimEventType.END);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Number getStartTime() {
        return startTime;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Number getEndTime() {
        return endTime;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IResource getResource() {
        return resource;
    }

    @Override
    public Number getOffset() {
        return offset;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Number getAmount() {
        return amount;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IClaimEvent getStartEvent() {
        return startEvent;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IClaimEvent getEndEvent() {
        return stopEvent;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Claim[" + startTime + "-" + endTime + ", amount=" + amount + ", offset=" + offset + ", resoure="
                + resource + ", attributes=" + getAttributes() + "]";
    }
}
