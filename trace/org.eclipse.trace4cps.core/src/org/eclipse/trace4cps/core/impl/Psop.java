/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.trace4cps.analysis.signal.impl.PsopHelper;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.IPsopFragment;

/**
 * Default implementation of the {@link IPsop} type.
 */
public final class Psop extends AttributeAware implements IPsop {
    private final List<IPsopFragment> fragments = new ArrayList<>();

    /**
     * {@inheritDoc}
     */
    @Override
    public List<IPsopFragment> getFragments() {
        return fragments;
    }

    /**
     * @param f the fragment to add
     * @throws TraceException if the fragment is not consecutive to the last fragment
     */
    public void add(IPsopFragment f) {
        addAtEnd(f);
    }

    public void addAtBegin(IPsopFragment f) {
        if (!fragments.isEmpty()) {
            IPsopFragment lf = fragments.get(0);
            check(f, lf);
        }
        fragments.add(0, f);
    }

    public void addAtEnd(IPsopFragment f) {
        if (!fragments.isEmpty()) {
            IPsopFragment last = fragments.get(fragments.size() - 1);
            check(last, f);
            // merge fragments if equal constant values
            if (f.getA().doubleValue() == 0d && f.getB().doubleValue() == 0d && last.getA().doubleValue() == 0d
                    && last.getB().doubleValue() == 0d && f.getC().doubleValue() == last.getC().doubleValue())
            {
                fragments.remove(fragments.size() - 1);
                fragments.add(new PsopFragment(f.getC(), f.getB(), f.getA(),
                        new Interval(last.dom().lb(), false, f.dom().ub(), true)));
            } else {
                fragments.add(f);
            }
        } else {
            fragments.add(f);
        }
    }

    /**
     * Merges fragments with the same parameters thereby reducing the size of the psop.
     */
    public void merge() {
        int i = 0;
        while (i < fragments.size() - 1) {
            IPsopFragment f0 = fragments.get(i);
            IPsopFragment f1 = fragments.get(i + 1);
            if (mergeable(f0, f1)) {
                fragments.remove(i);
                fragments.remove(i);
                fragments.add(i, new PsopFragment(f0.getC(), f0.getB(), f0.getA(),
                        new Interval(f0.dom().lb(), false, f1.dom().ub(), true)));
                // don't increase i; check if the new fragment and the next fragment can be
                // merged
            } else {
                i++;
            }
        }
    }

    private boolean mergeable(IPsopFragment f0, IPsopFragment f1) {
        double x = f0.dom().ub().doubleValue(); // equal to f1.dom().lb()
        return f0.getA().doubleValue() == f1.getA().doubleValue()
                && Math.abs(PsopHelper.valueAt(f0, x).doubleValue() - f1.getC().doubleValue()) < 1e-12
                && Math.abs(PsopHelper.valueDerivativeAt(f0, x).doubleValue() - f1.getB().doubleValue()) < 1e-12;
    }

    private void check(IPsopFragment f1, IPsopFragment f2) {
        if (f1.dom().ub().doubleValue() != f2.dom().lb().doubleValue()) {
            throw new IllegalArgumentException("non-consecutive fragments");
        }
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder("Psop[attributes=" + getAttributes());
        b.append(", fragments:\n");
        for (IPsopFragment f: fragments) {
            b.append("\t").append(f.toString()).append("\n");
        }
        b.append("]\n");
        return b.toString();
    }
}
