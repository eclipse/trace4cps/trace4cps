/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.impl;

import java.util.Map;

import org.eclipse.trace4cps.analysis.mtl.AtomicProposition;
import org.eclipse.trace4cps.core.ClaimEventType;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IClaimEvent;

/**
 * Default implementation of the {@link IClaimEvent}.
 */
public class ClaimEvent extends Event implements IClaimEvent {
    private final IClaim claim;

    private final ClaimEventType type;

    public ClaimEvent(Number t, IClaim claim, ClaimEventType type) {
        super(t);
        this.claim = claim;
        this.type = type;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IClaim getClaim() {
        return claim;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ClaimEventType getType() {
        return type;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, String> getAttributes() {
        return claim.getAttributes();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAttributeValue(String key) {
        return claim.getAttributeValue(key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAttribute(String key, String value) {
        claim.setAttribute(key, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean satisfies(AtomicProposition p) {
        if (p.getType() != null && p.getType() != type) {
            return false;
        }
        // no type specified, or type matches
        return TraceHelper.matches(p, this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "ClaimEvent[t=" + getTimestamp() + ", type=" + type + ", attributes=" + getAttributes() + "]";
    }
}
