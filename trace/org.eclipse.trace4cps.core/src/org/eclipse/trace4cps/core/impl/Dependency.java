/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.impl;

import org.eclipse.trace4cps.core.IDependency;
import org.eclipse.trace4cps.core.IEvent;

/**
 * Default implementation of the {@link IDependency} type.
 */
public class Dependency extends AttributeAware implements IDependency {
    private final IEvent src;

    private final IEvent dst;

    public Dependency(IEvent src, IEvent dst) {
        this.src = src;
        this.dst = dst;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IEvent getSrc() {
        return src;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IEvent getDst() {
        return dst;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Dependency[src=" + src + ", dst=" + dst + ", attributes=" + getAttributes() + "]";
    }
}
