/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.impl;

import org.eclipse.trace4cps.core.IResource;

/**
 * Default implementation of the {@link IResource} type.
 */
public class Resource extends AttributeAware implements IResource {
    private Number capacity;

    private final boolean useOffset;

    /**
     * @param capacity the resources capacity
     * @param useOffset whether the resource uses an offset
     */
    public Resource(Number capacity, boolean useOffset) {
        if (capacity.doubleValue() <= 0d) {
            throw new IllegalArgumentException("capacity must be > 0");
        }
        this.capacity = capacity;
        this.useOffset = useOffset;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Number getCapacity() {
        return capacity;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean useOffset() {
        return useOffset;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Resource[capacity=" + capacity + ", usesOffset=" + useOffset + ", attributes=" + getAttributes() + "]";
    }
}
