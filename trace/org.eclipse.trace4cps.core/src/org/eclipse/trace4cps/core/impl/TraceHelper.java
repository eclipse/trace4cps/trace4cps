/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.trace4cps.analysis.signal.impl.PsopHelper;
import org.eclipse.trace4cps.core.ClaimEventType;
import org.eclipse.trace4cps.core.IAttributeAware;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IClaimEvent;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.IInterval;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.ITrace;

/**
 * A utility class for doing all kinds of things with traces.
 */
public class TraceHelper {
    private TraceHelper() {
    }

    public static Set<String> getAttributeValues(ITrace trace, String att) {
        Set<String> result = new HashSet<>();
        getAttributeValues(trace.getClaims(), att, result);
        getAttributeValues(trace.getResources(), att, result);
        getAttributeValues(trace.getEvents(), att, result);
        getAttributeValues(trace.getDependencies(), att, result);
        getAttributeValues(trace.getSignals(), att, result);
        return result;
    }

    public static Set<String> getAttributeValues(Collection<? extends IAttributeAware> list, String att) {
        Set<String> result = new HashSet<>();
        getAttributeValues(list, att, result);
        return result;
    }

    private static void getAttributeValues(Collection<? extends IAttributeAware> list, String att, Set<String> result) {
        for (IAttributeAware a: list) {
            String val = a.getAttributeValue(att);
            if (val != null) {
                result.add(val);
            }
        }
    }

    public static Set<String> getAttributeNames(Collection<? extends IAttributeAware> list) {
        Set<String> result = new HashSet<>();
        getAttributeNames(list, result);
        return result;
    }

    public static Set<String> getAttributeNames(ITrace trace) {
        Set<String> result = new HashSet<>();
        getAttributeNames(trace.getClaims(), result);
        getAttributeNames(trace.getResources(), result);
        getAttributeNames(trace.getEvents(), result);
        getAttributeNames(trace.getDependencies(), result);
        getAttributeNames(trace.getSignals(), result);
        return result;
    }

    private static void getAttributeNames(Collection<? extends IAttributeAware> list, Set<String> result) {
        for (IAttributeAware a: list) {
            result.addAll(a.getAttributes().keySet());
        }
    }

    public static boolean hasNonClaimEvents(ITrace trace) {
        for (IEvent e: trace.getEvents()) {
            if (!(e instanceof IClaimEvent)) {
                return true;
            }
        }
        return false;
    }

    public static List<IEvent> getEvents(ITrace trace, boolean includeClaimEvents) {
        List<IEvent> r = new ArrayList<>();
        for (IEvent e: trace.getEvents()) {
            if (!(e instanceof IClaimEvent) || includeClaimEvents) {
                r.add(e);
            }
        }
        return r;
    }

    public static List<IAttributeAware> getClaimsAndEvents(ITrace trace, boolean includeClaimEvents) {
        List<IAttributeAware> r = new ArrayList<>();
        r.addAll(trace.getClaims());
        for (IEvent e: trace.getEvents()) {
            if (!(e instanceof IClaimEvent) || includeClaimEvents) {
                r.add(e);
            }
        }
        return r;
    }

    public static Map<String, String> toMap(String... args) {
        Map<String, String> r = new HashMap<>();
        if (args != null) {
            for (int i = 0; i < args.length - 1; i += 2) {
                r.put(args[i], args[i + 1]);
            }
        }
        return r;
    }

    public static IInterval getDomain(ITrace trace) {
        double min = Double.POSITIVE_INFINITY;
        double max = Double.NEGATIVE_INFINITY;
        for (IClaim c: trace.getClaims()) {
            min = Math.min(min, c.getStartTime().doubleValue());
            max = Math.max(max, c.getEndTime().doubleValue());
        }
        for (IEvent e: trace.getEvents()) {
            min = Math.min(min, e.getTimestamp().doubleValue());
            max = Math.max(max, e.getTimestamp().doubleValue());
        }
        for (IPsop p: trace.getSignals()) {
            min = Math.min(min, PsopHelper.getDomainLowerBound(p).doubleValue());
            max = Math.max(max, PsopHelper.getDomainUpperBound(p).doubleValue());
        }
        return new Interval(min, false, max, false);
    }

    public static <T extends IAttributeAware> T get(Collection<T> l, String key, String value) {
        List<T> r = TraceHelper.filter(l, key, value);
        if (r.isEmpty()) {
            return null;
        }
        return r.get(0);
    }

    /**
     * ] * @return true iff all attributes of a1 are also present in a2
     */
    public static boolean matches(IAttributeAware a1, IAttributeAware a2) {
        return matches(a1.getAttributes(), a2.getAttributes());
    }

    /**
     * @return whether every filter entry is present in the attributes
     */
    public static boolean matches(Map<String, String> filter, Map<String, String> attributes) {
        for (Map.Entry<String, String> e: filter.entrySet()) {
            String attVal = attributes.get(e.getKey());
            if (!e.getValue().equals(attVal)) {
                return false;
            }
        }
        return true;
    }

    public static <T extends IAttributeAware> List<T> filter(Collection<T> l, Map<String, String> filter) {
        List<T> result = new ArrayList<>();
        for (T aa: l) {
            if (matches(filter, aa.getAttributes())) {
                result.add(aa);
            }
        }
        return result;
    }

    public static <T extends IAttributeAware> List<T> filter(Collection<T> l, String key, String value) {
        return filter(l, Collections.singletonMap(key, value));
    }

    public static String represent(IEvent event, boolean includeAttNames, boolean includeAmountOffset) {
        if (event == null) {
            return "<null>";
        }
        StringBuilder b = new StringBuilder();
        getValues(b, event, includeAttNames);
        if (event instanceof IClaimEvent) {
            IClaimEvent claimEvent = (IClaimEvent)event;
            b.append(",res={");
            IResource resource = claimEvent.getClaim().getResource();
            getValues(b, resource, includeAttNames);
            if (includeAmountOffset) {
                b.append(",amount=").append(claimEvent.getClaim().getAmount());
                if (resource.useOffset()) {
                    b.append("offset=").append(claimEvent.getClaim().getOffset());
                }
            }
            b.append("}");
            if (claimEvent.getType() == ClaimEventType.START) {
                b.append("S");
            } else {
                b.append("E");
            }
        }
        return b.toString();
    }

    public static String getValues(IAttributeAware aa, boolean includeAttName) {
        return getValues(aa, aa.getAttributes().keySet(), includeAttName);
    }

    public static String getValues(IAttributeAware aa, Collection<String> atts, boolean includeAttName) {
        StringBuilder b = new StringBuilder();
        getValues(b, aa, atts, includeAttName);
        return b.toString();
    }

    private static void getValues(StringBuilder b, IAttributeAware aa, boolean includeAttName) {
        getValues(b, aa, aa.getAttributes().keySet(), includeAttName);
    }

    private static void getValues(StringBuilder b, IAttributeAware aa, Collection<String> atts,
            boolean includeAttName)
    {
        List<String> keys = new ArrayList<>();
        if (atts != null && atts.size() > 0) {
            keys.addAll(atts);
        } else {
            keys.addAll(aa.getAttributes().keySet());
        }
        Collections.sort(keys);
        boolean addSeparator = false;
        for (Iterator<String> it = keys.iterator(); it.hasNext();) {
            String key = it.next();
            String val = aa.getAttributeValue(key);
            if (includeAttName) {
                if (addSeparator) {
                    b.append(",");
                }
                addSeparator = true;
                b.append(key).append("=").append(val);
            } else if (val != null) {
                if (addSeparator) {
                    b.append(",");
                }
                addSeparator = true;
                b.append(val);
            }
        }
    }
}
