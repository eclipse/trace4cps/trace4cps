/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.eclipse.trace4cps.core.IAttributeAware;
import org.eclipse.trace4cps.core.IAttributeFilter;
import org.eclipse.trace4cps.core.IClaim;
import org.eclipse.trace4cps.core.IClaimEvent;
import org.eclipse.trace4cps.core.IDependency;
import org.eclipse.trace4cps.core.IEvent;
import org.eclipse.trace4cps.core.IExtendableTrace;
import org.eclipse.trace4cps.core.IFilteredTrace;
import org.eclipse.trace4cps.core.IPsop;
import org.eclipse.trace4cps.core.IResource;
import org.eclipse.trace4cps.core.ITrace;
import org.eclipse.trace4cps.core.TracePart;

/**
 * Default implementation of the {@link IFilteredTrace} and {@link IExtendableTrace} types.
 */
public class ModifiableTrace implements IFilteredTrace, IExtendableTrace {
    private final List<IAttributeFilter> claimFilters = new ArrayList<>();

    private final List<IAttributeFilter> resourceFilters = new ArrayList<>();

    private final List<IAttributeFilter> eventFilters = new ArrayList<>();

    private final List<IAttributeFilter> signalFilters = new ArrayList<>();

    private final List<IAttributeFilter> dependencyFilters = new ArrayList<>();

    private final ITrace wrapped;

    private List<IClaim> claims;

    private List<IResource> resources;

    private List<IEvent> events;

    private List<IDependency> dependencies;

    private List<IPsop> signals;

    private List<IDependency> extDependencies = new ArrayList<>();

    private List<IPsop> extSignals = new ArrayList<>();

    private List<IEvent> extEvents = new ArrayList<>();

    private List<IClaim> extClaims = new ArrayList<>();

    private List<IResource> extResources = new ArrayList<>();

    private static final Comparator<IEvent> EVENT_COMP = new Comparator<IEvent>() {
        @Override
        public int compare(IEvent e1, IEvent e2) {
            return Double.compare(e1.getTimestamp().doubleValue(), e2.getTimestamp().doubleValue());
        }
    };

    /**
     * Wraps the given trace with this filtering facade.
     *
     * @param trace the wrapped trace
     */
    public ModifiableTrace(ITrace trace) {
        this.wrapped = trace;
        clearFilter(TracePart.ALL);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addDependencies(Collection<IDependency> dependencies) {
        extDependencies.addAll(dependencies);
        recalculate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addSignal(IPsop p) {
        extSignals.add(p);
        recalculate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addEvents(Collection<IEvent> events) {
        extEvents.addAll(events);
        recalculate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addClaims(Collection<IClaim> claims) {
        extClaims.addAll(claims);
        for (IClaim claim: claims) {
            extEvents.add(claim.getStartEvent());
            extEvents.add(claim.getEndEvent());
            if (!extResources.contains(claim.getResource())) {
                extResources.add(claim.getResource());
            }
        }
        recalculate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasExtension(TracePart part) {
        switch (part) {
            case ALL:
                return !extSignals.isEmpty() || !extDependencies.isEmpty() || !extEvents.isEmpty();
            case SIGNAL:
                return !extSignals.isEmpty();
            case DEPENDENCY:
                return !extDependencies.isEmpty();
            case EVENT:
                return !extEvents.isEmpty();
            case CLAIM:
                return !extClaims.isEmpty();
            default:
                return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clearExtension(TracePart part) {
        switch (part) {
            case ALL:
                extSignals.clear();
                extDependencies.clear();
                extEvents.clear();
                extClaims.clear();
                extResources.clear();
                break;
            case SIGNAL:
                extSignals.clear();
                break;
            case DEPENDENCY:
                extDependencies.clear();
                break;
            case EVENT:
                // do not remove the claim events from the extClaims
                extEvents.clear();
                for (IClaim c: extClaims) {
                    extEvents.add(c.getStartEvent());
                    extEvents.add(c.getEndEvent());
                }
                Collections.sort(extEvents, EVENT_COMP);
                break;
            case CLAIM:
                extResources.clear();
                for (IClaim c: extClaims) {
                    extEvents.remove(c.getStartEvent());
                    extEvents.remove(c.getEndEvent());
                }
                extClaims.clear();
                break;
            default:
                throw new IllegalStateException();
        }
        recalculate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clearFilter(TracePart type) {
        if (type == TracePart.ALL) {
            clearInt(TracePart.CLAIM);
            clearInt(TracePart.EVENT);
            clearInt(TracePart.RESOURCE);
            clearInt(TracePart.DEPENDENCY);
            clearInt(TracePart.SIGNAL);
        } else {
            clearInt(type);
        }
        recalculate();
    }

    private void clearInt(TracePart type) {
        switch (type) {
            case CLAIM:
                claimFilters.clear();
                break;
            case EVENT:
                eventFilters.clear();
                break;
            case RESOURCE:
                resourceFilters.clear();
                break;
            case DEPENDENCY:
                dependencyFilters.clear();
                break;
            case SIGNAL:
                signalFilters.clear();
                break;
            case ALL:
                throw new IllegalStateException("should not be called");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addFilter(TracePart type, IAttributeFilter filter) {
        addFilter(type, false, filter);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addFilterAndRecalculate(TracePart type, IAttributeFilter filter) {
        addFilter(type, true, filter);
    }

    private void addFilter(TracePart type, boolean recalc, IAttributeFilter filter) {
        switch (type) {
            case CLAIM:
                addFilter(claimFilters, filter);
                break;
            case RESOURCE:
                addFilter(resourceFilters, filter);
                break;
            case EVENT:
                addFilter(eventFilters, filter);
                break;
            case SIGNAL:
                addFilter(signalFilters, filter);
                break;
            case DEPENDENCY:
                addFilter(dependencyFilters, filter);
                break;
            case ALL:
                addFilter(claimFilters, filter);
                addFilter(resourceFilters, filter);
                addFilter(eventFilters, filter);
                addFilter(signalFilters, filter);
                addFilter(dependencyFilters, filter);
                break;
        }
        if (recalc) {
            recalculate();
        }
    }

    private void addFilter(List<IAttributeFilter> l, IAttributeFilter filter) {
        if (filter != null) {
            l.add(filter);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void recalculate() {
        // Order matters here: res -> claims -> events -> deps
        recalcResources();
        recalcClaims();
        recalcEvents();
        recalcDependencies();
        recalcSignals();
    }

    private void recalcSignals() {
        signals = new ArrayList<>();
        signals.addAll(wrapped.getSignals());
        signals.addAll(extSignals);
        // Then filter if needed
        if (!signalFilters.isEmpty()) {
            signals = recalc(signals, signalFilters, null);
        }
    }

    private void recalcDependencies() {
        dependencies = new ArrayList<>();
        dependencies.addAll(wrapped.getDependencies());
        dependencies.addAll(extDependencies);
        // Then filter if needed
        if (!dependencyFilters.isEmpty() || !eventFilters.isEmpty() || !claimFilters.isEmpty()
                || !resourceFilters.isEmpty())
        {
            dependencies = recalc(dependencies, dependencyFilters, new InclusionConstraint<IDependency>() {
                @Override
                boolean include(IDependency a) {
                    return events.contains(a.getSrc()) && events.contains(a.getDst());
                }
            });
        }
    }

    private void recalcEvents() {
        events = new ArrayList<>();
        events.addAll(extEvents);
        events.addAll(wrapped.getEvents());
        Collections.sort(events, EVENT_COMP);
        // Filter if needed
        if (!eventFilters.isEmpty() || !claimFilters.isEmpty() || !resourceFilters.isEmpty()) {
            events = recalc(events, eventFilters, new InclusionConstraint<IEvent>() {
                @Override
                boolean include(IEvent a) {
                    if (a instanceof IClaimEvent) {
                        return claims.contains(((IClaimEvent)a).getClaim());
                    }
                    return true;
                }
            });
        }
    }

    private void recalcClaims() {
        claims = new ArrayList<>();
        claims.addAll(extClaims);
        claims.addAll(wrapped.getClaims());
        // Filter if needed
        if (!claimFilters.isEmpty() || !resourceFilters.isEmpty()) {
            claims = recalc(claims, claimFilters, new InclusionConstraint<IClaim>() {
                @Override
                boolean include(IClaim c) {
                    return resources.contains(c.getResource());
                }
            });
        }
    }

    private void recalcResources() {
        resources = new ArrayList<>();
        resources.addAll(extResources);
        resources.addAll(wrapped.getResources());
        // Filter if needed
        if (!resourceFilters.isEmpty()) {
            resources = recalc(resources, resourceFilters, null);
        }
    }

    private abstract class InclusionConstraint<T extends IAttributeAware> {
        abstract boolean include(T a);
    }

    private <T extends IAttributeAware> List<T> recalc(List<T> origList, List<IAttributeFilter> filters,
            InclusionConstraint<T> constraint)
    {
        List<T> filteredList = new ArrayList<>();
        for (T r: origList) {
            if (matches(r, filters) && (constraint == null || constraint.include(r))) {
                filteredList.add(r);
            }
        }
        return filteredList;
    }

    /**
     * Here the conjunction of all filters is taken.
     */
    private boolean matches(IAttributeAware a, List<IAttributeFilter> filters) {
        if (filters.isEmpty()) {
            return true;
        }
        for (IAttributeFilter f: filters) {
            if (!f.include(a)) {
                return false;
            }
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Number getTimeOffset() {
        return wrapped.getTimeOffset();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TimeUnit getTimeUnit() {
        return wrapped.getTimeUnit();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<IClaim> getClaims() {
        return claims;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<IResource> getResources() {
        return resources;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<IEvent> getEvents() {
        return events;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<IDependency> getDependencies() {
        return dependencies;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<IPsop> getSignals() {
        return signals;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAttributeValue(String key) {
        return wrapped.getAttributeValue(key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, String> getAttributes() {
        return wrapped.getAttributes();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAttribute(String key, String value) {
        wrapped.setAttribute(key, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clearAttributes() {
        wrapped.clearAttributes();
    }
}
