/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.trace4cps.core.IAttributeAware;

/**
 * Default implementation of the {@link IAttributeAware} type. This class can be subclassed to implement the various
 * parts of the TRACE data model.
 */
public class AttributeAware implements IAttributeAware {
    private final Map<String, String> attributes = new HashMap<>();

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, String> getAttributes() {
        return Collections.unmodifiableMap(attributes);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAttributeValue(String key) {
        return attributes.get(key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAttribute(String key, String value) {
        attributes.put(key, removeNewlines(value));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clearAttributes() {
        attributes.clear();
    }

    /**
     * Replaces the existing attributes with a new set.
     *
     * @param atts the new set of attributes to replace the ecisting ones
     */
    public void setAttributes(Map<String, String> atts) {
        attributes.clear();
        if (atts != null) {
            for (Map.Entry<String, String> e: atts.entrySet()) {
                setAttribute(e.getKey(), e.getValue());
            }
        }
    }

    private String removeNewlines(String s) {
        return s.replace('\n', ' ');
    }

    @Override
    public String toString() {
        return "AttributeAware[" + attributes.toString() + "]";
    }
}
