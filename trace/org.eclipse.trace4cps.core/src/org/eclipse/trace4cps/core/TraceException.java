/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core;

/**
 * The top-level exception in the TRACE library.
 */
public class TraceException extends Exception {
    private static final long serialVersionUID = 1L;

    public TraceException() {
    }

    public TraceException(String msg) {
        super(msg);
    }

    public TraceException(Throwable cause) {
        super(cause);
    }

    public TraceException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
