/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core;

/**
 * Encodes a second-order polynomial {@code f(t)} on the time domain {@link #dom()}. The time domain is a left-closed,
 * right-open interval: {@code [t0, t1)}. For all {@code t} in {@code dom}, we have
 * {@code f(t) = a*(t-t_0)^2 + b * (t - t_0) + c}.
 */
public interface IPsopFragment {
    /**
     * @return the second-order coefficient
     */
    Number getA();

    /**
     * @return the first-order coefficient
     */
    Number getB();

    /**
     * @return the zeroth-order coefficient
     */
    Number getC();

    /**
     * @return the time domain of this fragment: left closed and right open
     */
    IInterval dom();

    /**
     * @return the shape of this fragment
     */
    Shape getShape();

    /**
     * @return the order of this fragment (0, 1 or 2)
     */
    int getOrder();
}
