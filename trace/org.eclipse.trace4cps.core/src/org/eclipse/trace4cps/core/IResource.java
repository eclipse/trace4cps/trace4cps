/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core;

/**
 * One of the central types of the TRACE data model.
 *
 * <p>
 * An {@link IResource} has a positive capacity and indicates whether claims on it use an offset or not. This offset can
 * be used, for instance, to model memory fragmentation.
 * </p>
 */
public interface IResource extends IAttributeAware {
    /**
     * @return the capacity of the resource
     */
    Number getCapacity();

    /**
     * @return whether the resource uses an offset
     */
    boolean useOffset();
}
