/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core;

import org.eclipse.trace4cps.core.impl.ModifiableTrace;

/**
 * This type adds filtering methods to the {@link ITrace} interface. The default implementation {@link ModifiableTrace}
 * can be wrapped around any {@link ITrace} instance.
 */
public interface IFilteredTrace extends ITrace {
    /**
     * Adds a filter for a specific part of an {@link ITrace}. The filter is added to the already existing filters. The
     * conjunction of all {@link IAttributeFilter#include(IAttributeAware)} is taken for whether an element is included
     * </p>
     * The result is not recalculated, because this can be a relatively expensive operation and more filters might be
     * added. An explicit call to {@link #recalculate()} is needed for the new filters to take effect.
     *
     * @param type the part of the trace to which the filter applies
     * @param filter a new filter element to add
     */
    <T extends IAttributeFilter> void addFilter(TracePart type, T filter);

    /**
     * Adds a filter for a specific part of an {@link ITrace}. The filter is added to the already existing filters. The
     * conjunction of all {@link IAttributeFilter#include(IAttributeAware)} is taken for whether an element is included
     * </p>
     * The result is not recalculated.
     *
     * @param type the part of the trace to which the filter applies
     * @param filter a new filter element to add
     */
    <T extends IAttributeFilter> void addFilterAndRecalculate(TracePart type, T filter);

    /**
     * Recalculates the result of the currently defined filters.
     */
    void recalculate();

    /**
     * Clears the filters for the given parts of the trace.
     *
     * @param type the type of filters to clear
     */
    void clearFilter(TracePart type);
}
