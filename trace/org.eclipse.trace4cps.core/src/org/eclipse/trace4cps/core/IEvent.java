/*
 * Copyright (c) 2021, 2025 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.eclipse.trace4cps.core;

import org.eclipse.trace4cps.analysis.mtl.State;

/**
 * One of the central types of the TRACE data model.
 *
 * <p>
 * An {@link IEvent} has a timestamp (also see {@link ITrace}) and a number of attributes. Furthermore, an
 * {@link IEvent} extends the {@link State} type, which enables temporal-logic checking on sequences of events.
 * </p>
 */
public interface IEvent extends State {
    /**
     * @return the timestamp of the event
     */
    Number getTimestamp();
}
